

WITH DateTable
AS
(
  SELECT CAST('8-1-2016' AS Date) AS [DATE]
  UNION ALL
  SELECT DATEADD(dd,
   1, [DATE])
  FROM DateTable
  WHERE DATEADD(dd, 1, [DATE]) < cast(getdate() as Date)
)
SELECT dt.[DATE],
       D.ID,
	   D.Name as Retailer,
		coalesce((select RiskScore
		from DealerRiskScoreHistory
		where (cast(dt.[Date] as date) >= cast(StartDate as date) and cast(dt.[Date] as date) < (case when cast(EndDate as date) is null then cast(getdate() as date) else cast(EndDate as date) end))
		and DealerID = D.ID),0) as 'RiskScore',
		C.CategoryName as Category,
		T.TMF as [3moFunded],
		FL.Funded,
		FL.LeasesFunded,
		FL.Utilization,
		FL.FundedApproval,
		SL.Submissions,
		SL.Approved,
		SL.Flagged,
		SL.Denied,
		SL.ApprovalAmount,
		SL.ApprovalAmount/nullif(SL.Approved,0) as AvgApp,
		SL.Income/nullif(SL.Submissions,0) as AvgInc,
		case when FL.Funded is not null OR SL.Submissions is not null then 'Yes' else 'No' end as Active,
		cast(D.CreationDate as Date) as CreationDate
		INTO ##biRankHistory
FROM [DateTable] as dt cross join Dealers as D
	join DealerCategories as DC on D.ID = DC.DealerID
	Join Categories as C on C.ID = DC.CategoryID
	left outer join
		(SELECT
			cast(L.FundedDate as date) as FDate,
			D.Name as Name,
			D.ID as ID,
			SUM(L.FundedAmount)	as Funded,
			COUNT(case when L.FundedAmount>0 then 1 else null end) as LeasesFunded,
			SUM([FundedAmount])/SUM(case when ApprovalAmount is null then QualifiedAmount else ApprovalAmount end) as Utilization,
			SUM(case when ApprovalAmount is null then QualifiedAmount else ApprovalAmount end) as FundedApproval
		FROM	
			Dealers as D join Loans as L on L.DealerID = D.ID
		GROUP BY
			cast(L.FundedDate as date),
			D.Name,
			D.ID) as FL on FL.ID = D.ID and FL.FDate = dt.[Date]
		left outer join
		(SELECT
			cast(L.CreationDate as date) as FDate,
			D.ID as ID,
			COUNT(L.ID) as Submissions,
			COUNT(case when L.statusID in (7,11) then 1 else null end) as Denied,
			COUNT(case when L.statusID in (1,2,8) then 1 else null end) as Flagged,
			COUNT(case when L.statusID not in (7,11,1,2,8) then 1 else null end) as Approved,
			SUM(ApprovalAmount) as ApprovalAmount,
			SUM(C.MonthlyIncome) as Income
		FROM	
			Dealers as D join Loans as L on L.DealerID = D.ID
				join ApplicantJob as C on C.ApplicantID = L.ApplicantID
		GROUP BY
			cast(L.CreationDate as date),
			D.ID) as SL on SL.ID = D.ID and SL.FDate = dt.[Date]
		left outer join
		(SELECT
			DealerID, 
			sum(FundedAmount) as TMF
		FROM Loans
		WHERE datediff(dd,[FundedDate],getdate()) < 90
		GROUP BY DealerID) as T on T.DealerID = D.ID
WHERE
	D.CancelDate is null
	and D.CreationDate <= dt.[Date]
	--and (T.TMF > 200000 OR (T.TMF > 100000 and RiskScore < 3) OR (T.TMF > 10000 and T.TMF <= 100000 and RiskScore < 1.5))
	--and RiskScore < 5

ORDER BY
	dt.[DATE] asc

Select
	ID,
	RiskScore*Funded as WA
	INTO #waHistory
from ##biRankHistory

Select
	Case when C.CategoryName in ('Appliance','Other','Cell Phones','Exercise Equipment','Bikes','Medical Supplies','Golf')
		then 'Other'
		else C.CategoryName end as 'Category Name',
	sum(B.LeasesFunded) as Leases,
	sum(B.Funded) as Funded,
	avg(B.RiskScore) as 'Avg. RS',
	(sum(B.Funded * B.RiskScore))/sum(B.Funded) as 'Weighted Avg. RS'
	--,sum(H.WA)/sum(B.Funded) as 'Weighted Avg. RS'
from
	##biRankHistory as B left outer join DealerCategories as DC on B.ID = DC.DealerID
		left outer join Categories as C on C.ID = DC.CategoryID
		--left outer join #waHistory as H on H.ID = B.ID
where
	B.[Date] between '8-21-16' and '8-27-16'
	and C.CategoryName not in ('Test','Demo')
Group by
	Case when C.CategoryName in ('Appliance','Other','Cell Phones','Exercise Equipment','Bikes','Medical Supplies','Golf')
		then 'Other'
		else C.CategoryName end
order by
	[Leases] desc