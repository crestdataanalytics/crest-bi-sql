/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
	     Max([ModifiedDate]),
		[DealerID]
      ,[FundingMatrixCategoryID]
      ,[LoanLimit]

  FROM [DealerManage].[dbo].[DealerHistory]
  group by
  [DealerID]
      ,[FundingMatrixCategoryID]
      ,[LoanLimit]	
order by max(modifieddate) asc

SELECT
	Max(DealerID),
	LoanLimit,
	ModifiedDate
From
	DealerHistory
where LoanLimit is not null
group by
	LoanLimit,
	ModifiedDate
order by
	Max(DealerID) asc