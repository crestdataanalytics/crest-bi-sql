SELECT
	L.DealerID,
	D.Name,
	D.RiskScore as 'BI Rank',
	D.LoanLimit,
	count(L.ID) as 'Submissions',
	avg(FundedAmount) as 'Avg. Funded',
	sum(J.MonthlyIncome)/count(L.ID) as 'Avg Stated Income',
	count(case when J.MonthlyIncome >= 7500 then 1 else null end)*1.0/count(L.ID) as '% over 7,500'
From
	Loans as L join ApplicantJob as J on L.ApplicantID = J.ApplicantID
		join Dealers as D on D.ID = L.DealerID
where
	cast(FundedDate as Date) > '8-2-16'
group by
	L.DealerID,
	D.Name,
	D.RiskScore,
	D.LoanLimit
having
	Count(L.ID) > 2 and
	(count(case when J.MonthlyIncome >= 7500 then 1 else null end)*1.0/count(L.ID)) > 0.25
order by
	[% over 7,500] desc
