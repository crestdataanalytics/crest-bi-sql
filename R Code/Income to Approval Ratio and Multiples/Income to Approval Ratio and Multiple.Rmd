---
title: "Multiple - Income to Approval Ratio"
author: "Trang Le"
date: "August 2, 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
The purpose of this analysis is to investigate the relationship between the Income to Approval ratio and the big M Multiple. 
We would like to eventually apply a rank to each category in the relationship (if any)

```{r getData}

readRDS("Crest.RDS")

```

