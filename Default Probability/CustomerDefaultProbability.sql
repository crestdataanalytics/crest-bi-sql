SELECT
	distinct L.[Customer ID],
	CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
	L.[Total Paid]/L.[Funded Amount] as Multiple,
	L.LeaseQueueKey,
	--R.[Retailer ID],
	(exp(
				(	
				-3.38722737
				+
				coalesce((Case when R.Category = 'Furniture' then -0.59352816
				when R.Category = 'Jewelry' then 0.10014578
				when R.Category = 'Mattress' then -0.6974649
				else 0.04627665 end),0)
				+
				coalesce((Case when R.Region = 'NE+FL' then 0.36842137
				when R.Region = 'CENTRAL' then 0.23960365
				when R.Region = 'WEST' then 0.04372482 
				else 0 end),0)
				+
				coalesce((Case when C.Age between 25 and 35 then -0.08849128
				when C.Age between 35 and 45 then -0.03597492
				when C.Age between 45 and 130 then -0.23098591 
				else 0 end),0)
				+
				coalesce((Case when PD.IDVResult = 'D1' then -0.58751116
				when PD.IDVResult = 'D10' then -1.51183217
				when PD.IDVResult = 'D11' then -12.42763105
				when PD.IDVResult = 'D13' then -0.57193736 
				when PD.IDVResult = 'D14' then -1.32448866
				when PD.IDVResult = 'D16' then -2.23585564
				when PD.IDVResult = 'D17' then -2.35251570
				when PD.IDVResult = 'D18' then -1.33222062
				when PD.IDVResult = 'D19' then -3.07827867
				when PD.IDVResult = 'D4' then -11.89686189
				when PD.IDVResult = 'D6' then -1.79865452
				when PD.IDVResult = 'D8' then -2.44117966 
				else 0 end),0)
				+
				coalesce((Case when PD.IDVCode = '10' then 0.50928614
				when PD.IDVResult = '20' then 1.07487556
				when PD.IDVResult = '30' then 2.26382190
				when PD.IDVResult = '40' then 1.83543867 
				when PD.IDVResult = '50' then 1.51877395
				else 0 end),-10.33744948)
				+
				coalesce((Case when PD.BAVPass2 = 'true' then -0.19393331 
				else 0 end),0.87531723)
				+
				coalesce((Case when PD.DR = 'A2' then -0.31361043
				when PD.DR = 'D1' then 1.31361043
				when PD.DR = 'D2' then 1.47930491
				when PD.DR = 'D3' then 0.84536563
				when PD.DR = 'D4' then 0.6761938
				when PD.DR = 'D5' then 1.24081581 
				else 0 end),1.41017536)
				+
				coalesce((Case when PD.IDABucket = 'D2' then 0.21588040 
				else 0 end),-0.71823385)
				+
				coalesce((Case when PD.ClarityAction = 'Deny' then 0.72247108
				when PD.ClarityAction = 'Exception' then 0.91644209 
				else 0 end),1.16343655)
				+
				coalesce((Case when PD.PropertyOwner = 'Y' then -0.48823354 
				else 0 end),0.22108090)
			)
		)
	/(1+exp(
				(	
				-3.38722737
				+
				coalesce((Case when R.Category = 'Furniture' then -0.59352816
				when R.Category = 'Jewelry' then 0.10014578
				when R.Category = 'Mattress' then -0.6974649
				else 0.04627665 end),0)
				+
				coalesce((Case when R.Region = 'NE+FL' then 0.36842137
				when R.Region = 'CENTRAL' then 0.23960365
				when R.Region = 'WEST' then 0.04372482 
				else 0 end),0)
				+
				coalesce((Case when C.Age between 25 and 35 then -0.08849128
				when C.Age between 35 and 45 then -0.03597492
				when C.Age between 45 and 130 then -0.23098591 
				else 0 end),0)
				+
				coalesce((Case when PD.IDVResult = 'D1' then -0.58751116
				when PD.IDVResult = 'D10' then -1.51183217
				when PD.IDVResult = 'D11' then -12.42763105
				when PD.IDVResult = 'D13' then -0.57193736 
				when PD.IDVResult = 'D14' then -1.32448866
				when PD.IDVResult = 'D15' then -1.50499940
				when PD.IDVResult = 'D16' then -2.23585564
				when PD.IDVResult = 'D17' then -2.35251570
				when PD.IDVResult = 'D18' then -1.33222062
				when PD.IDVResult = 'D19' then -3.07827867
				when PD.IDVResult = 'D4' then -11.89686189
				when PD.IDVResult = 'D6' then -1.79865452
				when PD.IDVResult = 'D8' then -2.44117966 
				else 0 end),0)
				+
				coalesce((Case when PD.IDVCode = '10' then 0.50928614
				when PD.IDVResult = '20' then 1.07487556
				when PD.IDVResult = '30' then 2.26382190
				when PD.IDVResult = '40' then 1.83543867 
				when PD.IDVResult = '50' then 1.51877395
				else 0 end),-10.33744948)
				+
				coalesce((Case when PD.BAVPass2 = 'true' then -0.19393331 
				else 0 end),0.87531723)
				+
				coalesce((Case when PD.DR = 'A2' then -0.31361043
				when PD.DR = 'D1' then 1.31361043
				when PD.DR = 'D2' then 1.47930491
				when PD.DR = 'D3' then 0.84536563
				when PD.DR = 'D4' then 0.6761938
				when PD.DR = 'D5' then 1.24081581 
				else 0 end),1.41017536)
				+
				coalesce((Case when PD.IDABucket = 'D2' then 0.21588040 
				else 0 end),-0.71823385)
				+
				coalesce((Case when PD.ClarityAction = 'Deny' then 0.72247108
				when PD.ClarityAction = 'Exception' then 0.91644209 
				else 0 end),1.16343655)
				+
				coalesce((Case when PD.PropertyOwner = 'Y' then -0.48823354 
				else 0 end),0.22108090)
			)
		)))*10 as DefaultProbability
FROM
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
	join Customers as C on C.[Customer ID] = L.[Customer ID]
	left outer join
		(Select
			pad.SubjectID
			,max(case when padv.VariableID = 7216 then padv.VariableValue else null end) as 'IDVResult'
			,max(case when padv.VariableID = 7765 then padv.VariableValue else null end) as 'IDVCode'
			,max(case when padv.VariableID = 6342 then padv.VariableValue else null end) as 'BAVPass2'
			,max(case when padv.VariableID = 8164 then padv.VariableValue else null end) as 'DR'
			,max(case when padv.VariableID = 7910 then padv.VariableValue else null end) as 'IDABucket'
			,max(case when padv.VariableID = 246 then padv.VariableValue else null end) as 'ClarityAction'
			,max(case when padv.VariableID = 13084 then padv.VariableValue else null end) as 'PropertyOwner'
		from [ProviderAnalysis].dbo.DocumentInfos as pad 
			join [ProviderAnalysis].dbo.DocumentVariables as padv on
			pad.DocumentID = padv.DocumentID
			join [ProviderAnalysis].dbo.Variables as pav on
			padv.VariableID = pav.VariableID
			join [ProviderAnalysis].dbo.Providers as pap on
			pad.providerID = pap.providerId
			left outer join [Crest Business Intelligence].[dbo].[Leases] as L on L.[Customer ID] = pad.SubjectID
		--WHERE L.[Retailer ID] = 11377
		--WHERE L.[Customer ID] = 1153180
		GROUP BY
			pad.SubjectID) as PD on PD.SubjectID = L.[Customer ID]
WHERE
	--R.[Retailer ID] = 11138 and
	L.[LeaseQueueKey] not in (7,11)
	and L.[Funded Date] between '12-1-15' and '12-5-15'