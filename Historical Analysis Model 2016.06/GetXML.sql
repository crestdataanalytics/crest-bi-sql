if (select OBJECT_ID('[dbo].[udf_GetXMLNode]')) IS NOT NULL
DROP FUNCTION [dbo].[udf_GetXMLNodeV2]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_GetXMLNode]    Script Date: 6/8/2016 6:43:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[udf_GetXMLNode](@strPath varchar(1000), @strXML varchar(max))
returns varchar(max)
as
begin
	declare @strNodeValue varchar(255), @strCurrentPath varchar(1000), @strCurrentPathEnd varchar(1000), @intStartPoint int
	if patindex(replace(replace(@strPath,'<','%<'),'>','>%'),@strXML)>0 
	
	Begin 
		while patindex(replace(replace(@strPath,'<','%<'),'>','>%'),@strXML)>0
		Begin
			set @strCurrentPath = left(@strPath,patindex('%>%',@strPath))
			set @strCurrentPathEnd = replace(@strCurrentPath, '<','</')
			set @intStartPoint = patindex('%'+@strCurrentPath+'%'+@strCurrentPathEnd+'%',@strXML)
			set @strXML = replace(
								substring(
										@strXML,@intStartPoint,patindex
																	(
																	'%'+@strCurrentPathEnd+'%',
																	@strXML
																	)-@intStartPoint
									),@strCurrentPath,'') 
			set @strPath = replace(@strPath,@strCurrentPath,'')
		End
		--// All that is left is the value
		set @strNodeValue = @strXML
	End
	Else --// Tag not found, return NULL
	Begin
		set @strNodeValue = 'Missing'
	End
		
	return @strNodeValue
end

GO


