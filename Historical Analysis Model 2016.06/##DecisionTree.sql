set rowcount 0

--// 2016.06.28 DJS RepeatCustomer added filter and StatusID in (5) for Paid, i.e. good loans

--// Global Table
IF OBJECT_ID('tempdb..##DecisionTree') IS NOT NULL
	drop table ##DecisionTree
go 

Select DataSet.* 
	,[dbo].[udf_GetRecommendedApprovalPercentage](DataSet.RAPNode) as RAP
into ##DecisionTree	
from 
(
	SELECT 
		Applicants.[ID] as ApplicantID
		,[MainDealerID]
		,datediff(year,Applicants.DateOfBirth,ProviderData.CreatedDate) as Age
		,RetailerScores.MaxLimit
		,ProviderData.BAVResult
		,ProviderData.BAVCode
		,ProviderData.IDABucket
		,ProviderData.BAVPass
		,ProviderData.[HRI4]
		,ProviderData.CRA
		,ProviderData.[FollowUp2]
		,ProviderData.GlobalResult
		,ProviderData.BankAccountClosed
		,case when 	RepeatCustomer.ApplicantID is not null then 'Yes' else 'No' end as RepeatCustomer
		,RetailerScores.RegionName
		,Applicants.OpenedTime
		,RetailerScores.ProductCategory
		,[dbo].udf_GetRecommendedApprovalPercentageNode(ProviderData.BAVResult,ProviderData.BAVCode,ProviderData.IDABucket,ProviderData.BAVPass,ProviderData.BAVPass2,ProviderData.[HRI4],ProviderData.CRA,ProviderData.[FollowUp2],ProviderData.GlobalResult,ProviderData.BankAccountClosed,case when 	RepeatCustomer.ApplicantID is not null then 'Yes' else 'No' end,coalesce(RetailerScores.RegionName,''),coalesce(RetailerScores.ProductCategory,'')) as RAPNode
	 FROM Applicants
		left outer join (select ApplicantID, max(case when StatusID in (5) then 1 else 0 end) as CustomerType from Loans where FundedDate is not null group by ApplicantID having Count(*)>1) as RepeatCustomer on Applicants.[ID]=RepeatCustomer.ApplicantID
		left outer join ##RetailerScores as RetailerScores on Applicants.MainDealerID=RetailerScores.DealerID
		inner join ##ProviderData as ProviderData with (nolock) on Applicants.id=ProviderData.ApplicantID
) as DataSet



Go


select * from ##DecisionTree where RAPNode is null  and bavresult is not null order by OpenedTime

