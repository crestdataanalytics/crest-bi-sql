
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--// Global Table
IF OBJECT_ID('tempdb..##Leases') IS NOT NULL
drop table ##Leases

IF OBJECT_ID('tempdb..#LoanTransactions') IS NOT NULL
drop table #LoanTransactions


IF OBJECT_ID('tempdb..#RetailerFinancePlans') IS NOT NULL
drop table #RetailerFinancePlans


set nocount on

SELECT 
	LoanID,
	PaymentDate,
	CASE WHEN Amount > 0 THEN Amount ELSE 0 END AS AmountCharged,
	CASE WHEN Amount > 0 AND ReturnDate IS NOT NULL THEN Amount ELSE 0 END AS AmountReturned,
	CASE WHEN Amount < 0 AND ReturnDate IS NULL THEN Amount*-1 ELSE 0 END AS AmountRefunded,
	CAST(CASE WHEN ReturnDate IS NULL THEN 0 ELSE 1 END AS BIT) AS Returned
INTO #LoanTransactions
FROM dbo.LoanPayments		
WHERE DeletedDate IS NULL
	AND Amount <> 0
	AND NOT(Amount < 0 AND ReturnDate IS NOT NULL);

SELECT
	DealerFinancePlans.DealerID,
	FinancePlans.ID AS FinancePlanID,
	ROW_NUMBER() OVER (PARTITION BY DealerFinancePlans.DealerID ORDER BY FinancePlans.CreatedDate) AS PlanOrder
INTO #RetailerFinancePlans
FROM FinancePlans
	JOIN DealerFinancePlans
		ON DealerFinancePlans.FinancePlanID = FinancePlans.ID;

WITH PaymentSchedule AS
(
	SELECT 
		LoanID, 
		DueDate, 
		DueAmount,
		ROW_NUMBER() OVER (PARTITION BY LoanID ORDER BY DueDate, CreatedDate, ID) AS InvoiceNumber
	FROM dbo.LoanPaymentSchedule
	WHERE ScheduleEntryDesc NOT IN ('Initial Payment')
)
SELECT LoanID, DueDate, DueAmount
INTO #FirstBill
FROM PaymentSchedule
WHERE InvoiceNumber = 1;

WITH LoanIncomeInfo AS
(
	SELECT 
		LoanID,
		SUM(CASE WHEN AmountCharged > 0  THEN 1 ELSE 0 END) AS ChargeCount,
		SUM(CASE WHEN AmountReturned > 0 THEN 1 ELSE 0 END) AS ReturnCount,
		SUM(CASE WHEN AmountRefunded > 0 THEN 1 ELSE 0 END) AS RefundCount,
		SUM(AmountCharged)  AS AmountCharged,
		SUM(AmountRefunded) AS AmountReturned,
		SUM(AmountReturned) AS AmountRefunded	
	FROM #LoanTransactions
	GROUP BY LoanID
),
LoanFees AS
(
	SELECT 
		LoanID,
		SUM(FeeAmount) AS TotalFees,
		SUM(CASE WHEN WaivedDate IS NOT NULL THEN FeeAmount ELSE 0 END) AS FeesWavied
	FROM dbo.LoanFees
	GROUP BY LoanID
),
Leases AS
(
	SELECT
		Loans.ID AS [Lease ID],
		ISNULL(LoanOwnerID, 2) AS [Owner ID],
		Dealers.ID AS [Retailer ID],
		Loans.ApplicantID AS [Customer ID],
		Loans.CoApplicantID AS [Co-Applicant Customer ID],
		CAST(Loans.CreationDate AS DATE) AS [Submitted Date],
		Loans.ApprovalDate AS [Approval Date],
		CASE WHEN FundedDate IS NULL 
			 THEN NULL
			 WHEN ISNULL(ReceiptOfGoodDate, FundedDate) > ISNULL(#FirstBill.DueDate, Loans.FirstPaymentDate) 
			 THEN DATEADD(DAY, -10, ISNULL(#FirstBill.DueDate, Loans.FirstPaymentDate))
			 ELSE ISNULL(ReceiptOfGoodDate, FundedDate) END AS AgreementDate,
		CAST(FundedDate AS DATE) AS [Funded Date],
		Loans.StatusID AS LeaseQueueKey,
		DisplayID AS [Agreement Number],
		ROW_NUMBER() OVER (PARTITION BY Loans.ApplicantID ORDER BY Loans.CreationDate) AS [Sequence Number],
		QualifiedAmount AS [Qualified Amount],
		ISNULL(ApprovalAmount, QualifiedAmount) AS [Approval Amount],
		ISNULL(Loans.ApplicationFee, 40) AS [Application Fee],
		Loans.Amount AS [Financed Amount],
		CAST(CASE WHEN AutoApprovedFlag >=1 THEN 1 ELSE 0 END AS BIT) AS [Auto Approved],
		CASE WHEN FundedDate IS NOT NULL
			 THEN ISNULL(FundedAmount, Amount - ISNULL(DiscountCollected, Amount * 0.06) - ISNULL(Loans.ApplicationFee, 40))
			 ELSE 0 END AS [Funded Amount],
		TotalNote AS [Total Note],	
		ISNULL(ChargeCount, 0) AS [Payments Charged],
		ISNULL(ReturnCount, 0) AS [Payments Returned],
		ISNULL(RefundCount, 0) AS [Refunds Issued],		
		CASE WHEN OutcomeName IN ('PaidInFull', 'PaidPending') THEN 'Yes'
			 WHEN OutcomeName IN ('PartiallyPaid', 'PartialPaidPending') THEN 'Partially'
			 WHEN OutcomeName IN ('NotPaid') THEN 'No'
			 WHEN OutcomeName IN ('NotYetDue', 'NotFunded') THEN 'N/A'
			 ELSE 'Unknown' END
			 AS [Met First Scheduled Payment],
		ISNULL(LoanIncomeInfo.AmountCharged, 0) AS [Amount Charged],
		ISNULL(LoanIncomeInfo.AmountReturned, 0) AS [Amount Returned],
		ISNULL(LoanIncomeInfo.AmountRefunded, 0) AS [Amount Refunded],
		ISNULL(PastDueAmount, 0) AS [Past Due Balance],
		ISNULL(TotalFees, 0) AS [Fees Applied],
		ISNULL(FeesWavied, 0) AS [Fees Waived],
		COALESCE
		(
			Loans.PlanID,
			#RetailerFinancePlans.FinancePlanID,
			1
		) AS FinancePlanKey,
		IsDemoAccount,
		CASE WHEN Loans.StatusID = 7 THEN Loans.DeniedDate
			 WHEN Loans.StatusID = 11 THEN Loans.CanceledDate
			 WHEN Loans.StatusID IN (9,12,13,14,20,22) THEN Loans.ChargedOffDate
			 WHEN Loans.StatusID = 5 THEN Loans.PaidOffDate
			 ELSE NULL END AS CloseDate,
		CASE WHEN NumDaysLate > 0 THEN NumDaysLate ELSE 0 END AS DaysLate
	FROM dbo.Loans
		JOIN dbo.Dealers
			ON Loans.DealerID = Dealers.ID			
		LEFT JOIN dbo.LoanOwnerLoans
			ON Loans.ID = LoanOwnerLoans.LoanID
		LEFT JOIN dbo.FirstScheduledPaymentDetails
			ON Loans.ID = FirstScheduledPaymentDetails.LoanID
			AND IsCurrentOutcome = 1
		LEFT JOIN dbo.FirstScheduledPaymentOutcome
			ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
		LEFT JOIN LoanIncomeInfo
			ON Loans.ID = LoanIncomeInfo.LoanID
		LEFT JOIN LoanFees
			ON Loans.ID = LoanFees.LoanID	
		LEFT JOIN #FirstBill
			ON Loans.ID = #FirstBill.LoanID	
		LEFT JOIN #RetailerFinancePlans
			ON Dealers.ID = #RetailerFinancePlans.DealerID
			AND #RetailerFinancePlans.PlanOrder = 1		
)
SELECT 
	[Lease ID],
	[Owner ID],
	[Retailer ID],
	[Customer ID],
	[Co-Applicant Customer ID],
	[Submitted Date],
	[Approval Date],
	AgreementDate AS [Agreement Date],
	[Funded Date],
	CloseDate AS [Close Date],
	LeaseQueueKey,
	FinancePlanKey,
	[Agreement Number],
	[Sequence Number],
	[Qualified Amount],
	[Approval Amount],
	[Application Fee],
	[Financed Amount],
	[Auto Approved],
	[Funded Amount],
	[Total Note],	
	[Payments Charged],
	[Payments Returned],
	[Refunds Issued],
	[Met First Scheduled Payment],
	[Amount Charged],
	[Amount Returned],
	[Amount Refunded],
	[Past Due Balance],
	[Fees Applied],
	[Fees Waived],
	[Funded Amount] * 1.25 AS [Expected Cash],
	DaysLate AS [Days Late],
	([Amount Charged]-[Amount Returned]-[Amount Refunded]) as [Total Paid]
into ##Leases
FROM Leases
WHERE IsDemoAccount = 0

DROP TABLE #LoanTransactions;
DROP TABLE #RetailerFinancePlans;
DROP TABLE #FirstBill;

Go
