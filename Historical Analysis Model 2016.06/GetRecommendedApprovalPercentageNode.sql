if (select OBJECT_ID('[dbo].[udf_GetRecommendedApprovalPercentageNode]')) IS NOT NULL
DROP FUNCTION [dbo].udf_GetRecommendedApprovalPercentageNode
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].udf_GetRecommendedApprovalPercentageNode(
	@strBAVResult varchar(10),
	@strBAVCode varchar(10),
	@strIDABucket varchar(10),
	@strBAVPass varchar(10),
	@strBAVPass2 varchar(25),
	@strHRI4 varchar(10),
	@strCRA varchar(10),
	@strFollowUp2 varchar(10),
	@strGlobalResult varchar(10),
	@strBankAccountClosed varchar(10),
	@strRepeatCustomer varchar(10),
	@strRegion varchar(25),
	@strProductCategory varchar(25)
	)
returns int
as
begin
	declare @intReturnValue int
	set @intReturnValue = Null
	if @strRegion is null set @strRegion=''

	if @strBAVPass = ''
	Begin --// Begin @@strBAVPass = ''
		if @strProductCategory <> 'Jewelry'
			Begin --// Begin @strProductCategory <> 'Jewelry'
				if @intReturnValue is Null and @strRepeatCustomer = 'Yes'  set @intReturnValue=1
				if @intReturnValue is Null and @strRepeatCustomer = 'No' and @strFollowUp2<>'G' set @intReturnValue=2
				if @intReturnValue is Null and @strRepeatCustomer = 'No' and @strFollowUp2='G' set @intReturnValue=3
			End   --// End @strProductCategory <> 'Jewelry'
		Else 
			Begin --// Begin @strProductCategory = 'Jewelry'
				if @intReturnValue is Null set @intReturnValue=4
			End   --// End @strProductCategory = 'Jewelry'
	End --// End @@strBAVPass = ''
		
	if @strBAVPass = 'Y'
	Begin --// Begin @@strBAVPass = 'Y'
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 = ''and @strBankAccountClosed = 'No' and @strBAVResult= 'D3'  set @intReturnValue = 5
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 = ''and @strBankAccountClosed = 'No' and @strBAVResult<>'D3'  set @intReturnValue = 6
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 = ''and @strBankAccountClosed <>'No'  set @intReturnValue = 7
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strCRA = 'A2' and @strRegion <>'CENTRAL'  set @intReturnValue = 8
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strCRA = 'A2' and @strRegion = 'CENTRAL'  set @intReturnValue = 9
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strCRA <>'A2'  set @intReturnValue = 10
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory = 'Jewelry'  set @intReturnValue = 11
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed <>'No'  set @intReturnValue = 12
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 <>''and @strProductCategory = 'Jewelry'  set @intReturnValue = 13
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 <>''and @strProductCategory = 'Jewelry'  set @intReturnValue = 14
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strProductCategory <>'Cell Phones'  set @intReturnValue = 15
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 = ''and @strProductCategory = 'Jewelry'  set @intReturnValue = 16
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 = ''and @strProductCategory <>'Jewelry'  set @intReturnValue = 17
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult= 'D3'  set @intReturnValue = 18
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode<>'P01' and @strCRA = 'D8'  set @intReturnValue = 19
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode<>'P01' and @strCRA <>'D8' and @strBankAccountClosed = 'Yes'  set @intReturnValue = 20
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode<>'P01' and @strCRA <>'D8' and @strBankAccountClosed = 'No' set @intReturnValue = 21
		if @intReturnValue is Null and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode= 'P01'  set @intReturnValue = 22
		if @intReturnValue is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory = 'Jewelry'  set @intReturnValue = 23
		if @intReturnValue is Null and @strBAVCode= 'XD00' and @strRepeatCustomer <>'No'  set @intReturnValue = 24
		if @intReturnValue is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strProductCategory = 'Jewelry' set @intReturnValue = 25
		if @intReturnValue is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strProductCategory <>'Jewelry' and @strRegion <>'CENTRAL'  set @intReturnValue = 26
		if @intReturnValue is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strProductCategory <>'Jewelry' and @strRegion = 'CENTRAL'  set @intReturnValue = 27
		if @intReturnValue is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strRegion <>'CENTRAL'  set @intReturnValue = 28
		if @intReturnValue is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strRegion = 'CENTRAL'  set @intReturnValue = 29
	End --// End @@strBAVPass = 'Y'

	if @strBAVPass = 'N'
	Begin --// Begin @@strBAVPass = 'N'
		if @intReturnValue is Null and @strBAVCode<>'XT00' and  upper(@strBAVPass2) = 'FALSE'  set @intReturnValue = 30
		if @intReturnValue is Null and @strBAVCode<>'XT00' and  upper(@strBAVPass2) = 'TRUE'  set @intReturnValue = 31
		if @intReturnValue is Null and @strBAVCode= 'XT00'  set @intReturnValue = 32
	End --// End @@strBAVPass = 'N'
















	return @intReturnValue
end

GO
