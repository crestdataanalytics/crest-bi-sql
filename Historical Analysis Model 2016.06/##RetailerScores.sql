

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--// Global Table
IF OBJECT_ID('tempdb..##RetailerScores') IS NOT NULL
drop table ##RetailerScores


select 
##Retailers.[Retailer ID] as DealerID,
CategoryLimits.Name,
##Retailers.[Current Status],
(
	(
		(case 
			when [3 mo. FPD $] >=.60 then 0 
			when [3 mo. FPD $] >=.40 then 1
			when [3 mo. FPD $] >=.30 then 2
			when [3 mo. FPD $] >=.20 then 3
			when [3 mo. FPD $] >=.10 then 4
			else 5
		end * 3)
		+  
		(
		case 
		when [3 mo. Charge-Off %] >= .30 Then 0
		when [3 mo. Charge-Off %] >= .25 Then 1
		when [3 mo. Charge-Off %] >= .20 Then 2
		when [3 mo. Charge-Off %] >= .15 Then 3
		when [3 mo. Charge-Off %] >= .10 Then 4
		when [3 mo. Charge-Off %] >= .00 Then 5
		else 5
		End*4
		)
		+
		(
		case 
		when [3 mo. Paid %] >= .30 Then 5
		when [3 mo. Paid %] >= .25 Then 4
		when [3 mo. Paid %] >= .20 Then 3
		when [3 mo. Paid %] >= .15 Then 2
		when [3 mo. Paid %] >= .10 Then 1
		when [3 mo. Paid %] >= .00 Then 0
		else 5
		End*5
		)
		+
		(
		case 
		when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
		when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
		when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
		when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
		when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
		when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
		else 0
		End*6
		)
		+
		(
		case 
		when [Quarterly FPD % Increase] >= 2.10 Then 0
		when [Quarterly FPD % Increase] >= 1.80 Then 1
		when [Quarterly FPD % Increase] >= 1.50 Then 2
		when [Quarterly FPD % Increase] >= 1.20 Then 3
		when [Quarterly FPD % Increase] >= .90 Then 4
		when [Quarterly FPD % Increase] >= .00 Then 5
		else 5
		End*2
		)
	)/100.00 
)*(coalesce(CategoryLimits.MaxLimit,2500)) as MaxLimit
,coalesce(CategoryLimits.CategoryName,'') as ProductCategory
,coalesce(##Retailers.RegionName,'') as RegionName
into ##RetailerScores
from ##Retailers 
left outer join 
(
	SELECT 
		Dealers.ID as DealerID
		,Dealers.[Name]
		,ProductLimits.CategoryName as CategoryName
		,ProductLimits.MaxLimit
	 FROM Dealers 
		left outer join DealerCategories on Dealers.id=DealerCategories.DealerID
		left outer join Categories on Categories.ID = DealerCategories.CategoryID
		left outer join ##ProductLimits as ProductLimits on coalesce(nullif(Categories.CategoryName,''),'All Other') = ProductLimits.CategoryName
) as CategoryLimits on ##Retailers.[Retailer ID] = CategoryLimits.DealerID


Go

select * from ##RetailerScores




