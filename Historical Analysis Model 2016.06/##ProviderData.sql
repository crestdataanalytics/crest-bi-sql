set rowcount 0

--// 2016-06-08 08:36:33.652
--// Global Table

IF OBJECT_ID('tempdb..##ProviderData') IS NOT NULL
drop table ##ProviderData


--// Provider Data

select
	ApprovalProviderResponseLog.ApplicantID
	,ApprovalProviderResponseLog.CreatedDate as CreatedDate
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/Code)'))as BAVCode
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/CustomDecision/Bucket)')) as BAVResult 	
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/GlobalDecision/IDABucket)')) as IDABucket
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/CustomDecision/Result)')) as BAVPass
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/Pass)')) as BAVPass2
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[4]/Code)')) as HRI4
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/GlobalDecision/CRABucket)')) as CRA
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/IDVSegment/PotentialFollowupActions/Action[2]/Code)')) as FollowUp2
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/GlobalDecision/Result)')) as GlobalResult	
	,case when convert(int,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(DataxResponse/Response/Detail/DebitReportSegment/ReportedInput/AccountClosures/Summary/DaysSinceFirst)')))>0 then 'Yes' else 'No' end as BankAccountClosed
	into ##ProviderData
  FROM CrestWarehouse.dbo.ApprovalProviderResponseLog as ApprovalProviderResponseLog where ApprovalProviderSettingsID in (2) --// Just DataX
  and ID in (select max(id) from CrestWarehouse.dbo.ApprovalProviderResponseLog where ApprovalProviderSettingsID in (2) group by ApplicantID)
  
Go

