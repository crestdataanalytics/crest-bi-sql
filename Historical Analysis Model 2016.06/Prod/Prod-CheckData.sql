USE [DealerManage]
Go

set TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
Go
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

declare @ApplicantID int

--///////////////////////////////////
--// SET APPLICANT ID
--///////////////////////////////////
set @ApplicantID = 1337196
--///////////////////////////////////


--///////////////////////////////////
--// Just To Be Safe...
--///////////////////////////////////
SET NOCOUNT ON
SET ARITHABORT OFF 

Declare @intCrystalBallNode int --// Node in the decision tree used
Declare @intCrystalBallApprovedAmount numeric(14,2)  --// Recommended Amount based on retailer performance and the risk factors
Declare @intCrystalBallRecommendedApprovalPercentage numeric(14,2)  --// Recommended Approval Percentage to be applied to the Maximum Limit
Declare @intUseResult Int
Declare @intApprovalProviderResultType int
Declare @DecFactor as numeric(14,2)
Declare @intDealerID int
Declare @ApprovedAmount numeric(14,2) = 1000.00
--///////////////////////////////////
--// Variables for the Decision Tree
--///////////////////////////////////
Declare @strBAVResult varchar(10), @strBAVCode varchar(10), @strIDABucket varchar(10), @strBAVPass varchar(10),	@strBAVPass2 varchar(25), @strHRI4 varchar(10), @strCRA varchar(10), @strFollowUp2 varchar(10),	@strGlobalResult varchar(10), @strBankAccountClosed varchar(10), @strRepeatCustomer varchar(10), @strRegion varchar(25), @strProductCategory varchar(25),@intMaxLimit int

set nocount on
select @intDealerID=MainDealerID from applicants where id = @ApplicantID


	--//////////////////////////////////
	--// Begin #ProductLimits
	--//////////////////////////////////
	Begin
		--// Global Table
		IF OBJECT_ID('tempdb..#ProductLimits') IS NOT NULL 	drop table #ProductLimits

		create table #ProductLimits(CategoryName varchar(255), LowerLimit integer, Average integer, MaxLimit int)
		insert into #ProductLimits values('Bikes',200,1100,2500)
		insert into #ProductLimits values('Medical Supplies',200,1100,2500)
		insert into #ProductLimits values('Golf',200,1100,2500)
		insert into #ProductLimits values('Exercise Equipment',200,1100,2500)
		insert into #ProductLimits values('Appliance/Electronics/Video',200,3500,5000)
		insert into #ProductLimits values('Appliance',200,3500,5000)
		insert into #ProductLimits values('Jewelry',200,1800,1800)
		insert into #ProductLimits values('Furniture',200,5000,5000)
		insert into #ProductLimits values('Mattress',200,5000,5000)
		insert into #ProductLimits values('Car Audio',200,2500,5000)
		insert into #ProductLimits values('Tires/Rims or Tires / Rims',200,2500,5000)
		insert into #ProductLimits values('All Other',200,1250,2500)
	End 
	--//////////////////////////////////
	--// End #ProductLimits
	--//////////////////////////////////


	--//////////////////////////////////
	--// Begin LoanData
	--//////////////////////////////////
	Begin
		--// Temp Table
		IF OBJECT_ID('tempdb..#Leases') IS NOT NULL drop table #Leases

		IF OBJECT_ID('tempdb..#firstbill') IS NOT NULL drop table #firstbill

		IF OBJECT_ID('tempdb..#LoanTransactions') IS NOT NULL drop table #LoanTransactions

		IF OBJECT_ID('tempdb..#RetailerFinancePlans') IS NOT NULL drop table #RetailerFinancePlans

		SELECT 
			LoanID,
			PaymentDate,
			CASE WHEN Amount > 0 THEN Amount ELSE 0 END AS AmountCharged,
			CASE WHEN Amount > 0 AND ReturnDate IS NOT NULL THEN Amount ELSE 0 END AS AmountReturned,
			CASE WHEN Amount < 0 AND ReturnDate IS NULL THEN Amount*-1 ELSE 0 END AS AmountRefunded,
			CAST(CASE WHEN ReturnDate IS NULL THEN 0 ELSE 1 END AS BIT) AS Returned
		INTO #LoanTransactions
		FROM dbo.LoanPayments		
		WHERE DeletedDate IS NULL
			AND Amount <> 0
			AND NOT(Amount < 0 AND ReturnDate IS NOT NULL)
			AND LoanPayments.LoanID in (select Id from Loans where DealerID=@intDealerID)

		SELECT
			DealerFinancePlans.DealerID,
			FinancePlans.ID AS FinancePlanID,
			ROW_NUMBER() OVER (PARTITION BY DealerFinancePlans.DealerID ORDER BY FinancePlans.CreatedDate) AS PlanOrder
		INTO #RetailerFinancePlans
		FROM FinancePlans
			JOIN DealerFinancePlans
				ON DealerFinancePlans.FinancePlanID = FinancePlans.ID
		where DealerFinancePlans.DealerID=@intDealerID;


		WITH PaymentSchedule AS
		(
			SELECT 
				LoanID, 
				DueDate, 
				DueAmount,
				ROW_NUMBER() OVER (PARTITION BY LoanID ORDER BY DueDate, CreatedDate, ID) AS InvoiceNumber
			FROM dbo.LoanPaymentSchedule
			WHERE ScheduleEntryDesc NOT IN ('Initial Payment') and LoanPaymentSchedule.LoanID in (select Id from Loans where DealerID=@intDealerID)
		)
		SELECT LoanID, DueDate, DueAmount
		INTO #FirstBill
		FROM PaymentSchedule
		WHERE InvoiceNumber = 1;


		WITH LoanIncomeInfo AS
		(
			SELECT 
				LoanID,
				SUM(CASE WHEN AmountCharged > 0  THEN 1 ELSE 0 END) AS ChargeCount,
				SUM(CASE WHEN AmountReturned > 0 THEN 1 ELSE 0 END) AS ReturnCount,
				SUM(CASE WHEN AmountRefunded > 0 THEN 1 ELSE 0 END) AS RefundCount,
				SUM(AmountCharged)  AS AmountCharged,
				SUM(AmountRefunded) AS AmountReturned,
				SUM(AmountReturned) AS AmountRefunded	
			FROM #LoanTransactions
			GROUP BY LoanID
		),
		LoanFees AS
		(
			SELECT 
				LoanID,
				SUM(FeeAmount) AS TotalFees,
				SUM(CASE WHEN WaivedDate IS NOT NULL THEN FeeAmount ELSE 0 END) AS FeesWavied
			FROM dbo.LoanFees where loanid in (select Id from Loans where DealerID=@intDealerID)
			GROUP BY LoanID
		),
		Leases AS
		(
			SELECT
				Loans.ID AS [Lease ID],
				ISNULL(LoanOwnerID, 2) AS [Owner ID],
				Dealers.ID AS [Retailer ID],
				Loans.ApplicantID AS [Customer ID],
				Loans.CoApplicantID AS [Co-Applicant Customer ID],
				CAST(Loans.CreationDate AS DATE) AS [Submitted Date],
				Loans.ApprovalDate AS [Approval Date],
				CASE WHEN FundedDate IS NULL 
					 THEN NULL
					 WHEN ISNULL(ReceiptOfGoodDate, FundedDate) > ISNULL(#FirstBill.DueDate, Loans.FirstPaymentDate) 
					 THEN DATEADD(DAY, -10, ISNULL(#FirstBill.DueDate, Loans.FirstPaymentDate))
					 ELSE ISNULL(ReceiptOfGoodDate, FundedDate) END AS AgreementDate,
				CAST(FundedDate AS DATE) AS [Funded Date],
				Loans.StatusID AS LeaseQueueKey,
				DisplayID AS [Agreement Number],
				ROW_NUMBER() OVER (PARTITION BY Loans.ApplicantID ORDER BY Loans.CreationDate) AS [Sequence Number],
				QualifiedAmount AS [Qualified Amount],
				ISNULL(ApprovalAmount, QualifiedAmount) AS [Approval Amount],
				ISNULL(Loans.ApplicationFee, 40) AS [Application Fee],
				Loans.Amount AS [Financed Amount],
				CAST(CASE WHEN AutoApprovedFlag >=1 THEN 1 ELSE 0 END AS BIT) AS [Auto Approved],
				CASE WHEN FundedDate IS NOT NULL
					 THEN ISNULL(FundedAmount, Amount - ISNULL(DiscountCollected, Amount * 0.06) - ISNULL(Loans.ApplicationFee, 40))
					 ELSE 0 END AS [Funded Amount],
				TotalNote AS [Total Note],	
				ISNULL(ChargeCount, 0) AS [Payments Charged],
				ISNULL(ReturnCount, 0) AS [Payments Returned],
				ISNULL(RefundCount, 0) AS [Refunds Issued],		
				CASE WHEN OutcomeName IN ('PaidInFull', 'PaidPending') THEN 'Yes'
					 WHEN OutcomeName IN ('PartiallyPaid', 'PartialPaidPending') THEN 'Partially'
					 WHEN OutcomeName IN ('NotPaid') THEN 'No'
					 WHEN OutcomeName IN ('NotYetDue', 'NotFunded') THEN 'N/A'
					 ELSE 'Unknown' END
					 AS [Met First Scheduled Payment],
				ISNULL(LoanIncomeInfo.AmountCharged, 0) AS [Amount Charged],
				ISNULL(LoanIncomeInfo.AmountReturned, 0) AS [Amount Returned],
				ISNULL(LoanIncomeInfo.AmountRefunded, 0) AS [Amount Refunded],
				ISNULL(PastDueAmount, 0) AS [Past Due Balance],
				ISNULL(TotalFees, 0) AS [Fees Applied],
				ISNULL(FeesWavied, 0) AS [Fees Waived],
				COALESCE
				(
					Loans.PlanID,
					#RetailerFinancePlans.FinancePlanID,
					1
				) AS FinancePlanKey,
				IsDemoAccount,
				CASE WHEN Loans.StatusID = 7 THEN Loans.DeniedDate
					 WHEN Loans.StatusID = 11 THEN Loans.CanceledDate
					 WHEN Loans.StatusID IN (9,12,13,14,20,22) THEN Loans.ChargedOffDate
					 WHEN Loans.StatusID = 5 THEN Loans.PaidOffDate
					 ELSE NULL END AS CloseDate,
				CASE WHEN NumDaysLate > 0 THEN NumDaysLate ELSE 0 END AS DaysLate
			FROM dbo.Loans
				JOIN dbo.Dealers
					ON Loans.DealerID = Dealers.ID			
				LEFT JOIN dbo.LoanOwnerLoans
					ON Loans.ID = LoanOwnerLoans.LoanID
				LEFT JOIN dbo.FirstScheduledPaymentDetails
					ON Loans.ID = FirstScheduledPaymentDetails.LoanID
					AND IsCurrentOutcome = 1
				LEFT JOIN dbo.FirstScheduledPaymentOutcome
					ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
				LEFT JOIN LoanIncomeInfo
					ON Loans.ID = LoanIncomeInfo.LoanID
				LEFT JOIN LoanFees
					ON Loans.ID = LoanFees.LoanID	
				LEFT JOIN #FirstBill
					ON Loans.ID = #FirstBill.LoanID	
				LEFT JOIN #RetailerFinancePlans
					ON Dealers.ID = #RetailerFinancePlans.DealerID
					AND #RetailerFinancePlans.PlanOrder = 1		
			Where Loans.ID in (select Id from Loans where DealerID=@intDealerID)
		)
		SELECT 
			[Lease ID],
			[Owner ID],
			[Retailer ID],
			[Customer ID],
			[Co-Applicant Customer ID],
			[Submitted Date],
			[Approval Date],
			AgreementDate AS [Agreement Date],
			[Funded Date],
			CloseDate AS [Close Date],
			LeaseQueueKey,
			FinancePlanKey,
			[Agreement Number],
			[Sequence Number],
			[Qualified Amount],
			[Approval Amount],
			[Application Fee],
			[Financed Amount],
			[Auto Approved],
			[Funded Amount],
			[Total Note],	
			[Payments Charged],
			[Payments Returned],
			[Refunds Issued],
			[Met First Scheduled Payment],
			[Amount Charged],
			[Amount Returned],
			[Amount Refunded],
			[Past Due Balance],
			[Fees Applied],
			[Fees Waived],
			[Funded Amount] * 1.25 AS [Expected Cash],
			DaysLate AS [Days Late],
			([Amount Charged]-[Amount Returned]-[Amount Refunded]) as [Total Paid]
		into #Leases
		FROM Leases

		DROP TABLE #LoanTransactions;
		DROP TABLE #RetailerFinancePlans;
		DROP TABLE #FirstBill;

	End 
	--//////////////////////////////////
	--// End LoanData
	--//////////////////////////////////


	--//////////////////////////////////
	--// Begin Retailer Table
	--//////////////////////////////////
	Begin
		IF OBJECT_ID('tempdb..#Retailers') IS NOT NULL drop table #Retailers

		select		
			R.[Retailer Name],	
			R.[Retailer ID],	
			R.[State],	
			R.RegionName,
			sum(case when datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end) as '3 mo. Funded',	
			isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end),0)*1.0/	
				nullif(sum(case when datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. FPD $',
			isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end),0)*1.0/	
				nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
			isnull(nullif(count(case when [LeaseQueueKey] = 6 then 1 else null end),0)*1.0/	
				nullif(count(case when [Close Date] is null then 1 else null end),0),0) as '3 mo. Bounced %',
			isnull(nullif(count(case when [LeaseQueueKey] = 12 and datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0)*1.0/	
				nullif(count(case when datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0),0) as '3 mo. Charge-Off %',
			isnull(nullif(count(case when [LeaseQueueKey] = 5 and datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0)*1.0/	
				nullif(count(case when datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0),0) as '3 mo. Paid %',
			isnull(nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Total Paid] else 0 end),0)*1.0/	
				nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. Closing Multiple',
			isnull(nullif(sum(case when [Close Date] is not null then [Total Paid] else 0 end),0)*1.0/	
				nullif(sum(case when [Close Date] is not null then [Funded Amount] else 0 end),0),0) as 'Overall Closing Multiple',
			isnull(nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Total Paid] else 0 end),0)*1.0/	
				nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Funded Amount] else 0 end),0),0) as 'One Year Multiple',
			count(case when datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end) as '3 mo Volume',	
			count(case when datediff(mm,[Funded Date],getdate()) <= 6 then 1 else null end) as '6 mo Volume',	
			count(case when datediff(mm,[Funded Date],getdate()) <= 12 then 1 else null end) as '12 mo Volume',	
			isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/	
				 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
			isnull(nullif(count(case when datediff(mm,[Submitted Date],getdate()) <= 3 and [LeaseQueueKey] not in (7,11) then 1 else null end),0)*1.0/	
				 nullif(count(case when datediff(mm,[Submitted Date],getdate()) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate'
			into #Retailers
		from	
			(
			SELECT 
			Dealers.ID AS [Retailer ID],
			LTRIM(RTRIM(DealerGroups.DealerGroupName)) AS [Retailer Group Name],
			LTRIM(RTRIM(Dealers.Name)) AS [Retailer Name],
			StateID AS [State],
			Regions.RegionName,
			ApplicationFee AS [Application Fee],
			Ranking AS [Ranking (24 Month)],
			Ranking18Month AS [Ranking (18 Month)],
			Ranking10Month AS [Ranking (10 Month)],
			DealerStatuses.[Status] AS [Current Status]
		FROM dbo.Dealers
			JOIN dbo.DealerStatuses
				ON Dealers.StatusID = DealerStatuses.ID
			LEFT JOIN dbo.DealerGroups
				ON Dealers.DealerGroupID = DealerGroups.ID
			LEFT JOIN dbo.[Addresses]
				ON Dealers.AddressID = Addresses.ID
			left outer join States on addresses.StateID=states.ID
			left outer join regions on states.RegionID=REgions.RegionID
			where Dealers.ID=@intDealerID 
			) as R join #Leases as L on R.[Retailer ID] = L.[Retailer ID]
		group by	
			R.[Retailer Name], R.[Retailer ID], R.[State], R.RegionName
	End 
	--//////////////////////////////////
	--// End Retailer Table
	--//////////////////////////////////

	--//////////////////////////////////
	--// Begin RetailerScores Table 
	--//////////////////////////////////
	Begin
		IF OBJECT_ID('tempdb..#RetailerScores') IS NOT NULL drop table #RetailerScores


		select 
		#Retailers.[Retailer ID] as DealerID,
		(
			(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end * 3)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*4
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*5
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*6
				)
				+
				(
				case 
				when [Quarterly FPD % Increase] >= 2.10 Then 0
				when [Quarterly FPD % Increase] >= 1.80 Then 1
				when [Quarterly FPD % Increase] >= 1.50 Then 2
				when [Quarterly FPD % Increase] >= 1.20 Then 3
				when [Quarterly FPD % Increase] >= .90 Then 4
				when [Quarterly FPD % Increase] >= .00 Then 5
				else 5
				End*2
				)
			)/100.00 
		)*(coalesce(CategoryLimits.MaxLimit,2500)) as MaxLimit
		,coalesce(CategoryLimits.CategoryName,'') as ProductCategory
		,coalesce(#Retailers.RegionName,'') as RegionName
		into #RetailerScores
		from #Retailers 
		left outer join 
		(
			SELECT 
				Dealers.ID as DealerID
				,Dealers.[Name]
				,ProductLimits.CategoryName as CategoryName
				,ProductLimits.MaxLimit
			 FROM Dealers 
				left outer join DealerCategories on Dealers.id=DealerCategories.DealerID
				left outer join Categories on Categories.ID = DealerCategories.CategoryID
				left outer join #ProductLimits as ProductLimits on coalesce(nullif(Categories.CategoryName,''),'All Other') = ProductLimits.CategoryName
			Where Dealers.ID = @intDealerId	
		) as CategoryLimits on #Retailers.[Retailer ID] = CategoryLimits.DealerID

select * from #Retailers

select * from #RetailerScores

	End 
	--//////////////////////////////////
	--// End RetailerScores Table
	--//////////////////////////////////

	--//////////////////////////////////
	--// Begin ProviderData Table
	--//////////////////////////////////
	Begin
		IF OBJECT_ID('tempdb..#ProviderData') IS NOT NULL drop table #ProviderData

		--// Provider Data
		select top 1 
			ApprovalProviderResponseLog.ApplicantID
			,ApprovalProviderResponseLog.CreatedDate as CreatedDate
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/Code)'))as BAVCode
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/CustomDecision/Bucket)')) as BAVResult 	
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/GlobalDecision/IDABucket)')) as IDABucket
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/CustomDecision/Result)')) as BAVPass
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/BAVSegment/Pass)')) as BAVPass2
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[4]/Code)')) as HRI4
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/GlobalDecision/CRABucket)')) as CRA
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/IDVSegment/PotentialFollowupActions/Action[2]/Code)')) as FollowUp2
			,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/DataxResponse/Response/Detail/GlobalDecision/Result)')) as GlobalResult	
			,case when convert(int,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(DataxResponse/Response/Detail/DebitReportSegment/ReportedInput/AccountClosures/Summary/DaysSinceFirst)')))>0 then 'Yes' else 'No' end as BankAccountClosed
			into #ProviderData
		  FROM CrestWarehouse.dbo.ApprovalProviderResponseLog as ApprovalProviderResponseLog where ApprovalProviderSettingsID in (2) --// Just DataX
		  and ApplicantID=@ApplicantID
		  order by ApprovalProviderResponseLog.id desc
  
	End 
select * from #ProviderData
	--//////////////////////////////////
	--// End ProviderData Table 
	--//////////////////////////////////



	--//////////////////////////////////
	--// Begin DecisionTree Variables
	--//////////////////////////////////
	Begin
		SELECT 
			@intMaxLimit = RetailerScores.MaxLimit
			,@strBAVResult=ProviderData.BAVResult
			,@strBAVCode=ProviderData.BAVCode
			,@strIDABucket=ProviderData.IDABucket
			,@strBAVPass=ProviderData.BAVPass
			,@strBAVPass2=ProviderData.BAVPass2
			,@strHRI4=ProviderData.[HRI4]
			,@strCRA=ProviderData.CRA
			,@strFollowUp2=ProviderData.[FollowUp2]
			,@strGlobalResult=ProviderData.GlobalResult
			,@strBankAccountClosed=ProviderData.BankAccountClosed
			,@strRepeatCustomer = case when 	RepeatCustomer.ApplicantID is not null then 'Yes' else 'No' end
			,@strRegion=RetailerScores.RegionName
			,@strProductCategory=RetailerScores.ProductCategory
			FROM Applicants
			left outer join (select ApplicantID from Loans where FundedDate is not null and ApplicantID=@ApplicantID group by ApplicantID having Count(*)>1) as RepeatCustomer on Applicants.[ID]=RepeatCustomer.ApplicantID
			left outer join #RetailerScores as RetailerScores on Applicants.MainDealerID=RetailerScores.DealerID
			inner join #ProviderData as ProviderData with (nolock) on Applicants.id=ProviderData.ApplicantID
		Where Applicants.ID = @ApplicantID
	End
	--//////////////////////////////////
	--// End DecisionTree Variables
	--//////////////////////////////////

	
	--//////////////////////////////////
	--// Begin Decision Tree Traversal
	--//////////////////////////////////
	Begin
		if @strBAVPass = ''
		Begin --// Begin @@strBAVPass = ''
			if @strProductCategory <> 'Jewelry'
				Begin --// Begin @strProductCategory <> 'Jewelry'
					if @intCrystalBallNode is Null and @strRepeatCustomer = 'Yes'  set @intCrystalBallNode=1
					if @intCrystalBallNode is Null and @strRepeatCustomer = 'No' and @strFollowUp2<>'G' set @intCrystalBallNode=2
					if @intCrystalBallNode is Null and @strRepeatCustomer = 'No' and @strFollowUp2='G' set @intCrystalBallNode=3
				End   --// End @strProductCategory <> 'Jewelry'
			Else 
				Begin --// Begin @strProductCategory = 'Jewelry'
					if @intCrystalBallNode is Null set @intCrystalBallNode=4
				End   --// End @strProductCategory = 'Jewelry'
		End --// End @@strBAVPass = ''
		
		if @strBAVPass = 'Y'
		Begin --// Begin @@strBAVPass = 'Y'
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 = ''and @strBankAccountClosed = 'No' and @strBAVResult= 'D3'  set @intCrystalBallNode = 5
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 = ''and @strBankAccountClosed = 'No' and @strBAVResult<>'D3'  set @intCrystalBallNode = 6
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 = ''and @strBankAccountClosed <>'No'  set @intCrystalBallNode = 7
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strCRA = 'A2' and @strRegion <>'CENTRAL'  set @intCrystalBallNode = 8
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strCRA = 'A2' and @strRegion = 'CENTRAL'  set @intCrystalBallNode = 9
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strCRA <>'A2'  set @intCrystalBallNode = 10
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strHRI4 <>''and @strProductCategory = 'Jewelry'  set @intCrystalBallNode = 11
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed <>'No'  set @intCrystalBallNode = 12
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 <>''and @strProductCategory = 'Jewelry'  set @intCrystalBallNode = 13
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 <>''and @strProductCategory = 'Jewelry'  set @intCrystalBallNode = 14
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 <>''and @strProductCategory <>'Jewelry' and @strProductCategory <>'Cell Phones'  set @intCrystalBallNode = 15
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 = ''and @strProductCategory = 'Jewelry'  set @intCrystalBallNode = 16
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strBankAccountClosed = 'No' and @strHRI4 = ''and @strProductCategory <>'Jewelry'  set @intCrystalBallNode = 17
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult= 'D3'  set @intCrystalBallNode = 18
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode<>'P01' and @strCRA = 'D8'  set @intCrystalBallNode = 19
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode<>'P01' and @strCRA <>'D8' and @strBankAccountClosed = 'Yes'  set @intCrystalBallNode = 20
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode<>'P01' and @strCRA <>'D8' and @strBankAccountClosed = 'No' set @intCrystalBallNode = 21
			if @intCrystalBallNode is Null and @strRepeatCustomer = 'Yes' and @strProductCategory <>'Jewelry' and @strBAVResult<>'D3' and @strBAVCode= 'P01'  set @intCrystalBallNode = 22
			if @intCrystalBallNode is Null and @strBAVCode<>'XD00' and @strRepeatCustomer = 'Yes' and @strProductCategory = 'Jewelry'  set @intCrystalBallNode = 23
			if @intCrystalBallNode is Null and @strBAVCode= 'XD00' and @strRepeatCustomer <>'No'  set @intCrystalBallNode = 24
			if @intCrystalBallNode is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strProductCategory = 'Jewelry' set @intCrystalBallNode = 25
			if @intCrystalBallNode is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strProductCategory <>'Jewelry' and @strRegion <>'CENTRAL'  set @intCrystalBallNode = 26
			if @intCrystalBallNode is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket = 'A2' and @strProductCategory <>'Jewelry' and @strRegion = 'CENTRAL'  set @intCrystalBallNode = 27
			if @intCrystalBallNode is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strRegion <>'CENTRAL'  set @intCrystalBallNode = 28
			if @intCrystalBallNode is Null and @strBAVCode= 'XD00' and @strRepeatCustomer = 'No' and @strIDABucket <>'A2' and @strRegion = 'CENTRAL'  set @intCrystalBallNode = 29
		End --// End @@strBAVPass = 'Y'

		if @strBAVPass = 'N'
		Begin --// Begin @@strBAVPass = 'N'
			if @intCrystalBallNode is Null and @strBAVCode<>'XT00' and  upper(@strBAVPass2) = 'FALSE'  set @intCrystalBallNode = 30
			if @intCrystalBallNode is Null and @strBAVCode<>'XT00' and  upper(@strBAVPass2) = 'TRUE'  set @intCrystalBallNode = 31
			if @intCrystalBallNode is Null and @strBAVCode= 'XT00'  set @intCrystalBallNode = 32
		End --// End @@strBAVPass = 'N'
	End
	--//////////////////////////////////
	--// End Decision Tree Traversal
	--//////////////////////////////////


	--//////////////////////////////////
	--// Begin translation from node to percentage, can be migrated to a table
	--//////////////////////////////////
	Begin
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 1  set @intCrystalBallRecommendedApprovalPercentage = .90
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 2  set @intCrystalBallRecommendedApprovalPercentage = .65
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 3  set @intCrystalBallRecommendedApprovalPercentage = .70
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 4  set @intCrystalBallRecommendedApprovalPercentage = .25
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 5  set @intCrystalBallRecommendedApprovalPercentage = .35
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 6  set @intCrystalBallRecommendedApprovalPercentage = .40
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 7  set @intCrystalBallRecommendedApprovalPercentage = .20
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 8  set @intCrystalBallRecommendedApprovalPercentage = .30
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 9  set @intCrystalBallRecommendedApprovalPercentage = .40
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 10  set @intCrystalBallRecommendedApprovalPercentage = .20
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 11  set @intCrystalBallRecommendedApprovalPercentage = .10
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 12  set @intCrystalBallRecommendedApprovalPercentage = .30
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 13  set @intCrystalBallRecommendedApprovalPercentage = .20
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 14  set @intCrystalBallRecommendedApprovalPercentage = .15
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 15  set @intCrystalBallRecommendedApprovalPercentage = .50
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 16  set @intCrystalBallRecommendedApprovalPercentage = .35
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 17  set @intCrystalBallRecommendedApprovalPercentage = .65
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 18  set @intCrystalBallRecommendedApprovalPercentage = .90
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 19  set @intCrystalBallRecommendedApprovalPercentage = .85
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 20  set @intCrystalBallRecommendedApprovalPercentage = .85
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 21  set @intCrystalBallRecommendedApprovalPercentage = 1.00
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 22  set @intCrystalBallRecommendedApprovalPercentage = 1.00
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 23  set @intCrystalBallRecommendedApprovalPercentage = .75
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 24  set @intCrystalBallRecommendedApprovalPercentage = .85
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 25  set @intCrystalBallRecommendedApprovalPercentage = .25
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 26  set @intCrystalBallRecommendedApprovalPercentage = .55
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 27  set @intCrystalBallRecommendedApprovalPercentage = .65
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 28  set @intCrystalBallRecommendedApprovalPercentage = .15
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 29  set @intCrystalBallRecommendedApprovalPercentage = .30
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 30  set @intCrystalBallRecommendedApprovalPercentage = .65
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 31  set @intCrystalBallRecommendedApprovalPercentage = .15
		if @intCrystalBallRecommendedApprovalPercentage is null and @intCrystalBallNode= 32  set @intCrystalBallRecommendedApprovalPercentage = .30
		--// Default Value
		if @intCrystalBallRecommendedApprovalPercentage is null set @intCrystalBallRecommendedApprovalPercentage = 0
	End
	--//////////////////////////////////
	--// End translation from node to amount, can be migrated to a table
	--//////////////////////////////////

	--//////////////////////////////////
	--// Begin Use Result Logic
	--//////////////////////////////////
	Begin
		if @ApplicantID%20=0 
		Begin
			set @intUseResult = 0
			---/ set @intUseResult = 1
		End
		Else
		Begin
			set @intUseResult = 0
		End
	End
	--//////////////////////////////////
	--// End Use Result Logic
	--//////////////////////////////////

	--//////////////////////////////////
	--// Begin Final Result Logic
	--//////////////////////////////////
	Begin
		set @intCrystalBallApprovedAmount = @intMaxLimit*@intCrystalBallRecommendedApprovalPercentage
		set @intApprovalProviderResultType = 1 --// Default To Approved

		if @intCrystalBallApprovedAmount<=200
		Begin
			set @intCrystalBallApprovedAmount = 200
			if @strProductCategory='Jewelry' 
			Begin
				set @intApprovalProviderResultType = 2 --// Denied
			End 
		End


		--// Check for 0 Approved Amount
		if @ApprovedAmount = 0 set @ApprovedAmount = null
		
		--// Set Factor After Minimum
		set @DecFactor = (@intCrystalBallApprovedAmount/@ApprovedAmount)-1.00

		if @DecFactor is null set @DecFactor = 0

		--// Pending if we cannot calc for some reason
		if @intCrystalBallRecommendedApprovalPercentage=0 set @intApprovalProviderResultType = 3

		--// Value is getting stored in the XML as an integer
		set @intCrystalBallRecommendedApprovalPercentage=@intCrystalBallRecommendedApprovalPercentage*100.00
	
	End
	--//////////////////////////////////
	--// End Final Result Logic
	--//////////////////////////////////


	SELECT @ApplicantID as ApplicantID, @intUseResult AS UseResult, @intApprovalProviderResultType AS ApprovalProviderResultType, @intCrystalBallApprovedAmount AS ApprovedAmount, @intCrystalBallRecommendedApprovalPercentage AS ApprovalScore, @intCrystalBallNode as ModelNode, @DecFactor as Factor




