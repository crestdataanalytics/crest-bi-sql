  @echo off
    SETLOCAL EnableDelayedExpansion

    for /f "skip=1 tokens=1-6 delims= " %%a in ('wmic path Win32_LocalTime Get Day^,Hour^,Minute^,Month^,Second^,Year /Format:table') do (
        IF NOT "%%~f"=="" (
            set /a FormattedDate=10000 * %%f + 100 * %%d + %%a
            set FormattedDate=!FormattedDate:~-6,2!!FormattedDate:~-4,2!!FormattedDate:~-2,2!
        )
    )

    echo %FormattedDate%
    PAUSE



sqlcmd -S CFSQL -i "C:\Users\dsewell\Documents\BitBucket\Crest-BI-SQL\Historical Analysis Model 2016.06\Prod\Prod-RunLiveBulk.sql" -o "C:\Users\dsewell\Documents\BitBucket\Crest-BI-SQL\Historical Analysis Model 2016.06\Prod\DailyFile_%FormattedDate%.txt"