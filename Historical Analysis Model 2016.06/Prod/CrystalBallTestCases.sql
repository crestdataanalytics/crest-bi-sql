
select * from 
(
    select top 3 'ApproveWithDealerOverride' as TestCase, ApplicantID, (select max(loans.id) from loans where loans.applicantid=##TestResults.applicantid) as LeaseID,UseResult,ApprovalProviderResultType,ApprovedAmount,ApprovalScore,Factor
    from ##TestResults where applicantid in (select loans.ApplicantID from loans where DealerID in (select id from  Dealers where [FundingMatrixCategoryID] is not null))
    and useresult=1
    and ApprovalProviderResultType=1
    order by applicantID desc
)
as ApprovedWithDealerOverride

union all

select * from 
(
    select top 3 'DeniedWithDealerOverride' as TestCase, ApplicantID, (select max(loans.id) from loans where loans.applicantid=##TestResults.applicantid) as LeaseID,UseResult,ApprovalProviderResultType,ApprovedAmount,ApprovalScore,Factor
    from ##TestResults where applicantid in (select loans.ApplicantID from loans where DealerID in (select id from  Dealers where [FundingMatrixCategoryID] is not null))
    and useresult=1
    and ApprovalProviderResultType=5
    order by applicantID desc
)
as DeniedWithDealerOverride
union all

select * from 
(
    select top 3 'ApprovedWithoutDealerOverride' as TestCase, ApplicantID, (select max(loans.id) from loans where loans.applicantid=##TestResults.applicantid) as LeaseID,UseResult,ApprovalProviderResultType,ApprovedAmount,ApprovalScore,Factor
    from ##TestResults where applicantid in (select loans.ApplicantID from loans where DealerID in (select id from  Dealers where [FundingMatrixCategoryID] is null))
    and useresult=1
    and ApprovalProviderResultType=1
    order by applicantID desc
)
as ApprovedWithoutDealerOverride

union all

select * from 
(
    select top 3 'DeniedWithoutDealerOverride' as TestCase, ApplicantID, (select max(loans.id) from loans where loans.applicantid=##TestResults.applicantid) as LeaseID,UseResult,ApprovalProviderResultType,ApprovedAmount,ApprovalScore,Factor
    from ##TestResults where applicantid in (select loans.ApplicantID from loans where DealerID in (select id from  Dealers where [FundingMatrixCategoryID] is null))
    and useresult=1
    and ApprovalProviderResultType=5
    order by applicantID desc
)
as DeniedWithoutDealerOverride

union all

select * from 
(
    select top 3 'PendedWithoutDealerOverrideNoUseResult' as TestCase, ApplicantID, (select max(loans.id) from loans where loans.applicantid=##TestResults.applicantid) as LeaseID,UseResult,ApprovalProviderResultType,ApprovedAmount,ApprovalScore,Factor
    from ##TestResults where applicantid in (select loans.ApplicantID from loans where DealerID in (select id from  Dealers where [FundingMatrixCategoryID] is null))
    and useresult=0
    and ApprovalProviderResultType=2
    order by applicantID desc
)
as PendedWithoutDealerOverrideNoUseResult

union all

select * from 
(
    select top 3 'DeniedWithoutDealerOverrideNoUseResult' as TestCase, ApplicantID, (select max(loans.id) from loans where loans.applicantid=##TestResults.applicantid) as LeaseID,UseResult,ApprovalProviderResultType,ApprovedAmount,ApprovalScore,Factor
    from ##TestResults where applicantid in (select loans.ApplicantID from loans where DealerID in (select id from  Dealers where [FundingMatrixCategoryID] is null))
    and useresult=0
    and ApprovalProviderResultType=5
    order by applicantID desc
)
as DeniedWithoutDealerOverrideNoUseResult

union all

select * from 
(
    select top 3 'ApprovedWithoutDealerOverrideNoUseResult' as TestCase, ApplicantID, (select max(loans.id) from loans where loans.applicantid=##TestResults.applicantid) as LeaseID,UseResult,ApprovalProviderResultType,ApprovedAmount,ApprovalScore,Factor
    from ##TestResults where applicantid in (select loans.ApplicantID from loans where DealerID in (select id from  Dealers where [FundingMatrixCategoryID] is null))
    and useresult=0
    and ApprovalProviderResultType=1
    order by applicantID desc
)
as ApprovedWithoutDealerOverrideNoUseResult

