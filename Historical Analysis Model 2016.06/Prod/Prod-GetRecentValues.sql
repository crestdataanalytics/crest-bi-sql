SELECT TOP 30000
	ApplicantID
	,CreatedDate
	,Decision
	,Exception
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovalScore)'))as ApprovalScore
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovedAmount)')) as ApprovedAmount 	
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ModelNode)')) as ModelNode
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/Factor)')) as Factor
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovalProviderResultType)')) as ApprovalProviderResultType
	,convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/UseResult)')) as UseResult
into ##MyData
	 from dbo.ApprovalProviderResponseLog (nolock)
WHERE ApprovalProviderResponseLog.ApprovalProviderSettingsID = 16 
--and CreatedDate between '06/23/2016' and '06/24/2016' 
--and convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ModelNode)'))='0'
ORDER BY ApprovalProviderResponseLog.ID DESC



