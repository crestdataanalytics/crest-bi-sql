select
	R.[Retailer Name],
	R.[Retailer ID],
	R.[State],
	sum(case when datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end) as '3 mo. Funded',	
	isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 then [Funded Amount] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end),0),0) as '3 mo. FPD $',
	isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 then 1 else null end),0)*1.0/	
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
	isnull(nullif(count(case when [LeaseQueueKey] = 6 and datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/	
		nullif(count(case when [Close Date] is null and datediff(mm,[Funded Date],getdate())  <= 3 then 1 else null end),0),0) as '3 mo. Bounced %',
	isnull(nullif(count(case when [LeaseQueueKey] = 12 and datediff(mm,[Funded Date],getdate()) between 5 and 8 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(mm,[Funded Date],getdate()) between 5 and 8 then 1 else null end),0),0) as '3 mo. Charge-Off %',
	isnull(nullif(count(case when [LeaseQueueKey] = 5 and datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0),0) as '3 mo. Paid %',
	isnull(nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. Closing Multiple',
	isnull(nullif(sum(case when [Close Date] is not null then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when [Close Date] is not null then [Funded Amount] else 0 end),0),0) as 'Overall Closing Multiple',
	isnull(nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Funded Amount] else 0 end),0),0) as 'One Year Multiple',
	count(case when datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end) as '3 mo Volume',	
	count(case when datediff(mm,[Funded Date],getdate()) <= 6 then 1 else null end) as '6 mo Volume',	
	count(case when datediff(mm,[Funded Date],getdate()) <= 12 then 1 else null end) as '12 mo Volume',	
	isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
	isnull(nullif(count(case when datediff(mm,[Submitted Date],getdate()) <= 3 and [LeaseQueueKey] not in (7,11) then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Submitted Date],getdate()) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate',
	(isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end),0)*1.0/
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0))-
	 (isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0)) as 'Quarterly FPD vs Volume %',
	sum(case when datediff(mm,[Funded Date],getdate()) >= 12 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],getdate()) = 11 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],getdate()) = 10 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],getdate()) = 9 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],getdate()) = 8 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],getdate()) = 7 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],getdate()) = 6 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],getdate()) = 5 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],getdate()) = 4 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],getdate()) = 3 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],getdate()) = 2 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],getdate()) = 1 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],getdate()) = 0 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.06
		else 0
		end)/nullif(count([Lease ID]),0) as 'Actual vs. Target'
from
	Retailers as R 
	join Leases as L on R.[Retailer ID] = L.[Retailer ID]
where
	R.[Current Status] in ('Active') 
group by
	R.[Retailer Name], R.[Retailer ID], R.[State]

If OBJECT_ID('tempdb..##RetailerMax') is not null
drop table ##RetailerMax

select
##TempRetailer.[Retailer ID] as RetailerID,
(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*4
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*3
				)
				+
				(
				case
				when [3 mo. Bounced %] >= .45 Then 0
				when [3 mo. Bounced %] >= .30 Then 1
				when [3 mo. Bounced %] >= .20 Then 2
				when [3 mo. Bounced %] >= .10 Then 3
				when [3 mo. Bounced %] >= .05 Then 4
				when [3 mo. Bounced %] >= 0 Then 5
				else 5
				End*2
				)
				+
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/100.00 'Max Approval Score'
			into ##RetailerMax
			from ##TempRetailer