/*
select 
case when convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))<1000 then
	convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))
	when convert(int,round(convert(numeric(14,2),ApprovedAmount),-2)) between 3000 and 5000 then
	3000
	else 
	convert(int,round(convert(numeric(14,2),ApprovedAmount),-3))
end
as ApprovalGroup
,count(*) as Approvals
,format(sum(convert(numeric(14,2),ApprovedAmount)),'C') as CBAmountSum
,format(avg(convert(numeric(14,2),ApprovedAmount)),'C') as CBAmountAvg
,format(sum(convert(numeric(14,2),ApprovedAmount)/(1.00+convert(numeric(14,2),Factor))),'C') as CMAmountSum
,format(avg(convert(numeric(14,2),ApprovedAmount)/(1.00+convert(numeric(14,2),Factor))),'C') as CMAmountAvg
from ##MyData where ApprovalProviderResultType=1 and convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))>=700
group by 
case when convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))<1000 then
	convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))
	when convert(int,round(convert(numeric(14,2),ApprovedAmount),-2)) between 3000 and 5000 then
	3000
	else 
	convert(int,round(convert(numeric(14,2),ApprovedAmount),-3))
end
order by 
case when convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))<1000 then
	convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))
	when convert(int,round(convert(numeric(14,2),ApprovedAmount),-2)) between 3000 and 5000 then
	3000
	else 
	convert(int,round(convert(numeric(14,2),ApprovedAmount),-3))
end
-- select * from  ##MyData where isnumeric(factor)=1
*/




select 
case 
	when round(convert(int,ApprovalScore),-1) between 10 and 30 then 'High Risk'
	when round(convert(int,ApprovalScore),-1) between 31 and 60 then 'Med Risk'
	when round(convert(int,ApprovalScore),-1) between 61 and 100 then 'Low Risk'
	else 'Deny' end 
as ApprovalScore
,count(*) as Approvals
,format(sum(convert(numeric(14,2),ApprovedAmount)),'C') as CBAmountSum
,format(avg(convert(numeric(14,2),ApprovedAmount)),'C') as CBAmountAvg
,format(sum(convert(numeric(14,2),ApprovedAmount)/(1.00+convert(numeric(14,2),Factor))),'C') as CMAmountSum
,format(avg(convert(numeric(14,2),ApprovedAmount)/(1.00+convert(numeric(14,2),Factor))),'C') as CMAmountAvg
from ##MyData where ApprovalProviderResultType=1  and convert(int,round(convert(numeric(14,2),ApprovedAmount),-2))>=500
group by 
case 
	when round(convert(int,ApprovalScore),-1) between 10 and 30 then 'High Risk'
	when round(convert(int,ApprovalScore),-1) between 31 and 60 then 'Med Risk'
	when round(convert(int,ApprovalScore),-1) between 61 and 100 then 'Low Risk'
	else 'Deny' end 
order by 
case 
	when round(convert(int,ApprovalScore),-1) between 10 and 30 then 'High Risk'
	when round(convert(int,ApprovalScore),-1) between 31 and 60 then 'Med Risk'
	when round(convert(int,ApprovalScore),-1) between 61 and 100 then 'Low Risk'
	else 'Deny' end 
	
	
	select ModelNode,count(*)  from ##MyData where ApprovedAmount<>'' and convert(numeric(14,2),ApprovedAmount)>500  group by ModelNode