USE [DealerManage]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_GetCrystalBallDealerScore]    Script Date: 7/31/2016 11:23:53 PM ******/
DROP FUNCTION [dbo].[udf_GetCrystalBallDealerScore]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_GetCrystalBallDealerScore]    Script Date: 7/31/2016 11:23:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[udf_GetCrystalBallDealerScore] 
(
    @DealerID INTEGER
)
RETURNS DECIMAL(18,2)
AS
Begin
Declare @ReturnValue decimal(18,2)
select @ReturnValue = 
			(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*4
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*3
				)
				+
				(
				case
				when [3 mo. Bounced %] >= .45 Then 0
				when [3 mo. Bounced %] >= .30 Then 1
				when [3 mo. Bounced %] >= .20 Then 2
				when [3 mo. Bounced %] >= .10 Then 3
				when [3 mo. Bounced %] >= .05 Then 4
				when [3 mo. Bounced %] >= 0 Then 5
				else 5
				End*2
				)
				+
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/10.0
from 
(
	select
		R.Name as [Retailer Name],
		R.ID as [Retailer ID],
		sum(case when datediff(dd,[FundedDate],getdate()) < 90 then [FundedAmount] else 0 end) as '3 mo. Funded',	
		isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[FundedDate],getdate()) >= 1 and datediff(mm,[FundedDate],getdate()) < 4 then [FundedAmount] else 0 end),0)*1.0/	
			nullif(sum(case when datediff(mm,[FundedDate],getdate()) >= 1 and datediff(mm,[FundedDate],getdate()) < 4 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [FundedAmount] else 0 end),0),0) as '3 mo. FPD $',
		isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[FundedDate],getdate()) >= 1 and datediff(mm,[FundedDate],getdate()) < 4 then 1 else null end),0)*1.0/	
			nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[FundedDate],getdate()) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
		isnull(nullif(count(case when L.StatusID = 6 and datediff(mm,[FundedDate],getdate()) <= 3 then 1 else null end),0)*1.0/	
			nullif(count(case when [Close Date] is null and datediff(mm,[FundedDate],getdate())  <= 3 then 1 else null end),0),0) as '3 mo. Bounced %',
		isnull(nullif(count(case when L.StatusID = 12 and datediff(mm,[FundedDate],getdate()) between 5 and 8 then 1 else null end),0)*1.0/	
			nullif(count(case when datediff(mm,[FundedDate],getdate()) between 5 and 8 then 1 else null end),0),0) as '3 mo. Charge-Off %',
		isnull(nullif(count(case when L.StatusID = 5 and datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0)*1.0/	
			nullif(count(case when datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0),0) as '3 mo. Paid %',
		isnull(nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then AmountPaid else 0 end),0)*1.0/	
			nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [FundedAmount] else 0 end),0),0) as '3 mo. Closing Multiple',
		isnull(nullif(sum(case when [Close Date] is not null then AmountPaid else 0 end),0)*1.0/	
			nullif(sum(case when [Close Date] is not null then [FundedAmount] else 0 end),0),0) as 'Overall Closing Multiple',
		isnull(nullif(sum(case when datediff(mm,[FundedDate],getdate()) between 12 and 24 then AmountPaid else 0 end),0)*1.0/	
			nullif(sum(case when datediff(mm,[FundedDate],getdate()) between 12 and 24 then [FundedAmount] else 0 end),0),0) as 'One Year Multiple',
		count(case when datediff(mm,[FundedDate],getdate()) <=3 then 1 else null end) as '3 mo Volume',	
		count(case when datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as '6 mo Volume',	
		count(case when datediff(mm,[FundedDate],getdate()) <= 12 then 1 else null end) as '12 mo Volume',	
		isnull(nullif(count(case when datediff(mm,[FundedDate],getdate()) <= 3 then 1 else null end),0)*1.0/	
			 nullif(count(case when datediff(mm,[FundedDate],getdate()) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
		isnull(nullif(count(case when datediff(mm,L.CreationDate,getdate()) <= 3 and L.StatusID not in (7,11) then 1 else null end),0)*1.0/	
			 nullif(count(case when datediff(mm,L.CreationDate,getdate()) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate',
		(isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[FundedDate],getdate()) <=3 then 1 else null end),0)*1.0/
			nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[FundedDate],getdate()) between 4 and 7 then 1 else null end),0),0))-
		 (isnull(nullif(count(case when datediff(mm,[FundedDate],getdate()) <= 3 then 1 else null end),0)*1.0/
			 nullif(count(case when datediff(mm,[FundedDate],getdate()) between 4 and 7  then 1 else null end),0),0)) as 'Quarterly FPD vs Volume %',
		sum(case when datediff(mm,[FundedDate],getdate()) >= 12 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 1.25
			when datediff(mm,[FundedDate],getdate()) = 11 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 1.21
			when datediff(mm,[FundedDate],getdate()) = 10 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 1.15
			when datediff(mm,[FundedDate],getdate()) = 9 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 1.09
			when datediff(mm,[FundedDate],getdate()) = 8 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 1.02
			when datediff(mm,[FundedDate],getdate()) = 7 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.95
			when datediff(mm,[FundedDate],getdate()) = 6 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.88
			when datediff(mm,[FundedDate],getdate()) = 5 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.79
			when datediff(mm,[FundedDate],getdate()) = 4 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.71
			when datediff(mm,[FundedDate],getdate()) = 3 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.62
			when datediff(mm,[FundedDate],getdate()) = 2 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.44
			when datediff(mm,[FundedDate],getdate()) = 1 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.25
			when datediff(mm,[FundedDate],getdate()) = 0 then coalesce((AmountPaid)/nullif(([FundedAmount]),0),0) - 0.06
			else 0
			end)/nullif(count(L.ID),0) as 'Actual vs. Target'
	from
		Dealers as R 
		inner join Loans as L on R.ID = L.DealerID
		inner join 
			(select Loans.ID, 
					CASE
				WHEN Loans.StatusID = 7
					THEN Loans.DeniedDate
				WHEN Loans.StatusID = 11
					THEN Loans.CanceledDate
				WHEN Loans.StatusID IN (9,12,13,14,20,22)
					THEN Loans.ChargedOffDate
				WHEN Loans.StatusID = 5
					THEN Loans.PaidOffDate
				ELSE NULL
			END AS [Close Date] from Loans) as LoanClose on L.ID=LoanClose.ID
		left outer join
			(Select Loans.ID,
		 		CASE
				WHEN OutcomeName IN ('PaidInFull', 'PaidPending')
					THEN 'Yes'
				WHEN OutcomeName IN ('PartiallyPaid', 'PartialPaidPending')
					THEN 'Partially'
				WHEN OutcomeName IN ('NotPaid')
					THEN 'No'
				WHEN OutcomeName IN ('NotYetDue', 'NotFunded')
					THEN 'N/A'
				ELSE 'Unknown'
			END AS [Met First Scheduled Payment]
			From Loans
				LEFT OUTER JOIN dbo.FirstScheduledPaymentDetails
					ON Loans.ID = FirstScheduledPaymentDetails.LoanID AND FirstScheduledPaymentDetails.IsCurrentOutcome = 1
				LEFT OUTER JOIN dbo.FirstScheduledPaymentOutcome
					ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
			) as PaymentMet on L.ID = PaymentMet.ID
		left outer join 
				(
				SELECT
				LoanID,
				RentPaid + FeesPaid AS AmountPaid
				FROM 
				(
					SELECT 
						LoanID,
						SUM(FeesCharged)
							- SUM(FeesReturned)
							- SUM(FeesRefunded) AS FeesPaid,
						SUM(RentCharged)
							- SUM(RentReturned)
							- SUM(RentRefunded) AS RentPaid
					FROM
						(
 							SELECT 
								LoanID,
								CASE WHEN Amount > 0 THEN FeeAmount ELSE 0 END AS FeesCharged,
								CASE WHEN Amount > 0 AND ReturnDate IS NOT NULL THEN FeeAmount ELSE 0 END AS FeesReturned,
								CASE WHEN Amount < 0 THEN FeeAmount * -1 ELSE 0 END AS FeesRefunded,
								CASE WHEN Amount > 0 THEN RentAmount ELSE 0 END AS RentCharged,
								CASE WHEN Amount > 0 AND ReturnDate IS NOT NULL THEN RentAmount ELSE 0 END AS RentReturned,
								CASE WHEN Amount < 0 THEN RentAmount * -1 ELSE 0 END AS RentRefunded
							FROM dbo.vw_LoanPayments AS LoanPayments
							WHERE DeletedDate IS NULL
								AND Amount <> 0
								AND TypeID <> 9
								AND NOT(Amount < 0 AND ReturnDate IS NOT NULL)
						) AS LoanTransactions
						GROUP BY LoanID
			) As TransactionSummary
			) as AmountPaid on L.ID=AmountPaid.LoanID

	where
		R.StatusID = 1 --// Active
		and R.ID = @DealerID
	group by
		R.Name, R.ID
) as Retailers


	return @ReturnValue
End
GO


