/*
drop table ##TempRetailer
drop table ##RetailerMax
*/

Declare @intMonth int
set @intMonth = 1
declare @count int
set @count = 0
declare @d1 date
set @d1 = dateadd(month,@count,getdate())

IF OBJECT_ID('tempdb..##RankHistory') IS NOT NULL drop table ##RankHistory
create table ##RankHistory(TStamp date,Category varchar(25), RetailerName varchar(75))

WHILE @intMonth<4
BEGIN
IF OBJECT_ID('tempdb..##TempRetailer') IS NOT NULL
	drop table ##TempRetailer  
    select
	R.[Retailer Name],
	R.[Retailer ID],
	R.[State],
	sum(case when datediff(dd,[Funded Date],@d1) < 90 then [Funded Amount] else 0 end) as '3 mo. Funded',	
	isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) >= 1 and datediff(mm,[Funded Date],@d1) < 4 then [Funded Amount] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],@d1) < 4 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end),0),0) as '3 mo. FPD $',
	isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) >= 1 and datediff(mm,[Funded Date],@d1) < 4 then 1 else null end),0)*1.0/	
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
	isnull(nullif(count(case when [LeaseQueueKey] = 6 and datediff(mm,[Funded Date],@d1) <= 3 then 1 else null end),0)*1.0/	
		nullif(count(case when [Close Date] is null and datediff(mm,[Funded Date],@d1)  <= 3 then 1 else null end),0),0) as '3 mo. Bounced %',
	isnull(nullif(count(case when [LeaseQueueKey] = 12 and datediff(mm,[Funded Date],@d1) between 5 and 8 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(mm,[Funded Date],@d1) between 5 and 8 then 1 else null end),0),0) as '3 mo. Charge-Off %',
	isnull(nullif(count(case when [LeaseQueueKey] = 5 and datediff(dd,[Close Date],@d1) <= 90 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(dd,[Close Date],@d1) <= 90 then 1 else null end),0),0) as '3 mo. Paid %',
	isnull(nullif(sum(case when datediff(dd,[Close Date],@d1) < 90 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(dd,[Close Date],@d1) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. Closing Multiple',
	isnull(nullif(sum(case when [Close Date] is not null then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when [Close Date] is not null then [Funded Amount] else 0 end),0),0) as 'Overall Closing Multiple',
	isnull(nullif(sum(case when datediff(mm,[Funded Date],@d1) between 12 and 24 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],@d1) between 12 and 24 then [Funded Amount] else 0 end),0),0) as 'One Year Multiple',
	count(case when datediff(mm,[Funded Date],@d1) <=3 then 1 else null end) as '3 mo Volume',	
	count(case when datediff(mm,[Funded Date],@d1) <= 6 then 1 else null end) as '6 mo Volume',	
	count(case when datediff(mm,[Funded Date],@d1) <= 12 then 1 else null end) as '12 mo Volume',	
	isnull(nullif(count(case when datediff(mm,[Funded Date],@d1) <= 3 then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Funded Date],@d1) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
	isnull(nullif(count(case when datediff(mm,[Submitted Date],@d1) <= 3 and [LeaseQueueKey] not in (7,11) then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Submitted Date],@d1) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate',
	(isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) <=3 then 1 else null end),0)*1.0/
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) between 4 and 7 then 1 else null end),0),0))-
	 (isnull(nullif(count(case when datediff(mm,[Funded Date],@d1) <= 3 then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Funded Date],@d1) between 4 and 7  then 1 else null end),0),0)) as 'Quarterly FPD vs Volume %',
	sum(case when datediff(mm,[Funded Date],@d1) >= 12 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],@d1) = 11 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],@d1) = 10 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],@d1) = 9 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],@d1) = 8 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],@d1) = 7 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],@d1) = 6 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],@d1) = 5 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],@d1) = 4 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],@d1) = 3 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],@d1) = 2 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],@d1) = 1 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],@d1) = 0 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.06
		else 0
		end)/nullif(count([Lease ID]),0) as 'Actual vs. Target'
	into ##TempRetailer
from
	CFREPORTS.[Crest Business Intelligence].dbo.Retailers as R left outer join CFREPORTS.[Crest Business Intelligence].dbo.Leases as L on R.[Retailer ID] = L.[Retailer ID]
--where
	--R.[Current Status] in ('Active') 
group by
	R.[Retailer Name], R.[Retailer ID], R.[State]
order by
	[One Year Multiple] asc,
	r.[Retailer Name]


	IF OBJECT_ID('tempdb..##RetailerMax') IS NOT NULL
	drop table ##RetailerMax 
select
##TempRetailer.[Retailer ID] as RetailerID,
[3 mo. FPD $],
[3 mo. Charge-Off %],
[3 mo. Paid %],
coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) as 'Multiple',
[Actual vs. Target],
[3 mo. Bounced %],
[Quarterly FPD vs Volume %],
(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*4
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*3
				)
				+
				(
				case
				when [3 mo. Bounced %] >= .45 Then 0
				when [3 mo. Bounced %] >= .30 Then 1
				when [3 mo. Bounced %] >= .20 Then 2
				when [3 mo. Bounced %] >= .10 Then 3
				when [3 mo. Bounced %] >= .05 Then 4
				when [3 mo. Bounced %] >= 0 Then 5
				else 5
				End*2
				)
				+
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/100.00 'Max Approval Score'
			into ##RetailerMax
			from ##TempRetailer

declare @sDate as date, @sDate2 as date
set @sDate = Convert(date, dateadd(day, -365, @d1))
set @sDate2 = Convert(date, @d1)

IF OBJECT_ID('tempdb..##RankingTemp') IS NOT NULL
drop table ##RankingTemp

select
	@d1 as 'Timestamp',
	R.Category,
	R.[Retailer Name],
	R.[Retailer ID],
	R.[Current Status],
	convert(date,R.[Creation Date]) as 'Creation Date',
	R.[Ranking (10 month)],
	RM.[Max Approval Score]*10 as 'Score',
	RM.[3 mo. FPD $],
	RM.[3 mo. Charge-Off %],
	RM.[3 mo. Paid %],
	RM.Multiple,
	RM.[Actual vs. Target],
	RM.[3 mo. Bounced %],
	RM.[Quarterly FPD vs Volume %],
	count(case when [Funded Amount] > 0 then 1 else null end)*1.0/count(case when [LeaseQueueKey] not in (7,11) then 1 else null end) as 'Conversion Rate'	
	into ##RankingTemp
from
	Leases as L left join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
	left outer join ##RetailerMax as RM on R.[Retailer ID] = RM.RetailerID
where
	([Funded Date] between @sDate and @sDate2) and [Funded Amount]>0
	--and RM.[Max Approval Score] < 0.5
GROUP BY
	R.Category,
	R.[Retailer Name],
	R.[Retailer ID],
	R.[Current Status],
	convert(date,R.[Creation Date]),
	R.[Ranking (10 month)],
	RM.[Max Approval Score]*10,
	RM.[3 mo. FPD $],
	RM.[3 mo. Charge-Off %],
	RM.[3 mo. Paid %],
	RM.Multiple,
	RM.[Actual vs. Target],
	RM.[3 mo. Bounced %],
	RM.[Quarterly FPD vs Volume %]
ORDER BY
	RM.[Max Approval Score]*10 desc

Insert into ##RankHistory(TStamp,Category,RetailerName) --,RetailerID,CurrentStatus,CreateDate,Ranking10Month,MaxApprovalScore,3moFPD,3moChargeOff,3moPaid,Multiple,AvsT,3moBounced,QuarterlyFPDvsVolume)
select [TimeStamp],Category,[Retailer Name] from ##RankingTemp

    set @intMonth = @intMonth + 1
	set @count = @count - 1
END