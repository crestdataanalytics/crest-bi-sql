USE [DealerManage]
GO

DROP PROCEDURE [dbo].[up_Perform_CaptureDealerRiskScoreChanges]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[up_Perform_CaptureDealerRiskScoreChanges] 
AS
BEGIN

	SET XACT_ABORT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	BEGIN TRANSACTION;

	DECLARE @Now DATE = GETDATE();
	
	WITH LoanTransactions AS
	(
		SELECT 
			LoanID,
			CASE WHEN Amount > 0 THEN FeeAmount ELSE 0 END AS FeesCharged,
			CASE WHEN Amount > 0 AND ReturnDate IS NOT NULL THEN FeeAmount ELSE 0 END AS FeesReturned,
			CASE WHEN Amount < 0 THEN FeeAmount * -1 ELSE 0 END AS FeesRefunded,
			CASE WHEN Amount > 0 THEN RentAmount ELSE 0 END AS RentCharged,
			CASE WHEN Amount > 0 AND ReturnDate IS NOT NULL THEN RentAmount ELSE 0 END AS RentReturned,
			CASE WHEN Amount < 0 THEN RentAmount * -1 ELSE 0 END AS RentRefunded
		FROM dbo.vw_LoanPayments AS LoanPayments
		WHERE DeletedDate IS NULL
		AND Amount <> 0
		AND TypeID <> 9
		AND NOT(Amount < 0 AND ReturnDate IS NOT NULL)
	),
	TransactionSummary AS
	(
		SELECT 
			LoanID,
			SUM(FeesCharged)
			- SUM(FeesReturned)
			- SUM(FeesRefunded) AS FeesPaid,
			SUM(RentCharged)
			- SUM(RentReturned)
			- SUM(RentRefunded) AS RentPaid
		FROM LoanTransactions
		GROUP BY LoanID
	),
	AmountPaid AS
	(
		SELECT
			LoanID,
			RentPaid
			+ FeesPaid AS AmountPaid
		FROM TransactionSummary
	),
	Loans AS
	(
		SELECT
			ID AS LoanID,
			ApplicantID,
			DealerID,
			FundedDate,
			FundedAmount,
			StatusID,
			CreationDate,
			CASE
				WHEN Loans.StatusID = 7
					THEN CONVERT(DATE, Loans.DeniedDate)
				WHEN Loans.StatusID = 11
					THEN CONVERT(DATE, Loans.CanceledDate)
				WHEN Loans.StatusID IN (9,12,13,14,20,22)
					THEN CONVERT(DATE, Loans.ChargedOffDate)
				WHEN Loans.StatusID = 5
					THEN CONVERT(DATE, Loans.PaidOffDate)
				ELSE NULL
			END AS [Close Date],
			CASE
				WHEN OutcomeName IN ('PaidInFull', 'PaidPending')
					THEN 'Yes'
				WHEN OutcomeName IN ('PartiallyPaid', 'PartialPaidPending')
					THEN 'Partially'
				WHEN OutcomeName IN ('NotPaid')
					THEN 'No'
				WHEN OutcomeName IN ('NotYetDue', 'NotFunded')
					THEN 'N/A'
				ELSE 'Unknown'
			END AS [Met First Scheduled Payment],
			AmountPaid.AmountPaid
		FROM dbo.Loans
		LEFT JOIN dbo.FirstScheduledPaymentDetails
			ON Loans.ID = FirstScheduledPaymentDetails.LoanID AND FirstScheduledPaymentDetails.IsCurrentOutcome = 1
		LEFT JOIN dbo.FirstScheduledPaymentOutcome
			ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
		LEFT JOIN AmountPaid
			ON AmountPaid.LoanID = Loans.ID
	),
	Retailers AS
	(
		SELECT
			Dealers.Name AS [Retailer Name],
			Dealers.ID AS [Retailer ID],
			SUM(
				CASE
					WHEN DATEDIFF(dd, [FundedDate], @Now) < 90
						THEN [FundedAmount]
					ELSE 0
				END
			) AS '3 mo. Funded',	
			ISNULL(
				NULLIF(
					SUM(
						CASE
							WHEN [Met First Scheduled Payment] = 'No'
							AND DATEDIFF(mm, [FundedDate], @Now) >= 1
							AND DATEDIFF(mm, [FundedDate], @Now) < 4
								THEN [FundedAmount]
							ELSE 0
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					SUM(
						CASE
							WHEN DATEDIFF(mm, [FundedDate], @Now) >= 1
							AND DATEDIFF(mm, [FundedDate], @Now) < 4
							AND [Met First Scheduled Payment] IN ('No','Yes','Partially')
								THEN [FundedAmount]
							ELSE 0
						END
					)
				, 0)
			, 0) AS '3 mo. FPD $',
			ISNULL(
				NULLIF(
					COUNT(
						CASE
							WHEN [Met First Scheduled Payment] = 'No'
							AND DATEDIFF(mm, [FundedDate], @Now) >= 1
							AND DATEDIFF(mm, [FundedDate], @Now) < 4
								THEN 1
							ELSE NULL
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					COUNT(
						CASE
							WHEN [Met First Scheduled Payment] = 'No'
							AND DATEDIFF(mm, [FundedDate], @Now) BETWEEN 4 AND 7
								THEN 1
							ELSE NULL
						END
					)
				, 0)
			, 0) AS 'Quarterly FPD % Increase',
			ISNULL(
				NULLIF(
					COUNT(
						CASE
							WHEN Loans.StatusID = 6
							AND DATEDIFF(mm, [FundedDate], @Now) <= 3
								THEN 1
							ELSE NULL
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					COUNT(
						CASE
							WHEN [Close Date] IS NULL
							AND DATEDIFF(mm, [FundedDate], @Now)  <= 3
								THEN 1
							ELSE NULL
						END
					)
				, 0)
			, 0) AS '3 mo. Bounced %',
			ISNULL(
				NULLIF(
					COUNT(
						CASE
							WHEN Loans.StatusID = 12
							AND DATEDIFF(mm, [FundedDate], @Now) BETWEEN 5 AND 8
								THEN 1
							ELSE NULL
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					COUNT(
						CASE
							WHEN DATEDIFF(mm, [FundedDate], @Now) BETWEEN 5 AND 8
								THEN 1
							ELSE NULL
						END
					)
				, 0)
			, 0) AS '3 mo. Charge-Off %',
			ISNULL(
				NULLIF(
					COUNT(
						CASE
							WHEN Loans.StatusID = 5
							AND DATEDIFF(dd, [Close Date], @Now) <= 90
								THEN 1
							ELSE NULL
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					COUNT(
						CASE
							WHEN DATEDIFF(dd, [Close Date], @Now) <= 90
								THEN 1
							ELSE NULL
						END
					)
				, 0)
			, 0) AS '3 mo. Paid %',
			ISNULL(
				NULLIF(
					SUM(
						CASE
							WHEN DATEDIFF(dd, [Close Date], @Now) < 90
								THEN AmountPaid
							ELSE 0
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					SUM(
						CASE
							WHEN DATEDIFF(dd, [Close Date], @Now) < 90
								THEN [FundedAmount]
							ELSE 0
						END
					)
				, 0)
			, 0) AS '3 mo. Closing Multiple',
			ISNULL(
				NULLIF(
					SUM(
						CASE
							WHEN [Close Date] IS NOT NULL
								THEN AmountPaid
							ELSE 0
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					SUM(
						CASE
							WHEN [Close Date] is not NULL
								THEN [FundedAmount]
							ELSE 0
						END
					)
				, 0)
			, 0) AS 'Overall Closing Multiple',
			ISNULL(
				NULLIF(
					SUM(
						CASE
							WHEN DATEDIFF(mm, [FundedDate], @Now) BETWEEN 12 AND 24
								THEN AmountPaid
							ELSE 0
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					SUM(
						CASE
							WHEN DATEDIFF(mm, [FundedDate], @Now) BETWEEN 12 AND 24
								THEN [FundedAmount]
							ELSE 0
						END
					)
				, 0)
			, 0) AS 'One Year Multiple',
			COUNT(
				CASE
					WHEN DATEDIFF(mm, [FundedDate], @Now) <=3
						THEN 1
					ELSE NULL
				END
			) AS '3 mo Volume',	
			COUNT(
				CASE
					WHEN DATEDIFF(mm, [FundedDate], @Now) <= 6
						THEN 1
					ELSE NULL
				END
			) AS '6 mo Volume',	
			COUNT(
				CASE
					WHEN DATEDIFF(mm, [FundedDate], @Now) <= 12
						THEN 1
					ELSE NULL
				END
			) AS '12 mo Volume',	
			ISNULL(
				NULLIF(
					COUNT(
						CASE
							WHEN DATEDIFF(mm, [FundedDate], @Now) <= 3
								THEN 1
							ELSE NULL
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					COUNT(
						 CASE
							 WHEN DATEDIFF(mm, [FundedDate], @Now) BETWEEN 4 AND 7
								THEN 1
							 ELSE NULL
						 END
					)
				, 0)
			, 0) AS 'Quarterly Volume Increase',
			ISNULL(
				NULLIF(
					COUNT(
						CASE
							WHEN DATEDIFF(mm, Loans.CreationDate, @Now) <= 3
							AND Loans.StatusID not in (7,11)
								THEN 1
							ELSE NULL
						END
					)
				, 0) * 1.0
				/	
				NULLIF(
					COUNT(
						CASE
							WHEN DATEDIFF(mm, Loans.CreationDate, @Now) <=3
							THEN 1
							ELSE NULL
						END
					)
				, 0)
			, 0) AS '3 mo. Approval Rate',
			(
				ISNULL(
					NULLIF(
						COUNT(
							CASE
								WHEN [Met First Scheduled Payment] = 'No'
								AND DATEDIFF(mm, [FundedDate], @Now) <=3
									THEN 1
								ELSE NULL
							END
						)
					, 0) * 1.0
					/
					NULLIF(
						COUNT(
							CASE
								WHEN [Met First Scheduled Payment] = 'No'
								AND DATEDIFF(mm, [FundedDate], @Now) BETWEEN 4 AND 7
									THEN 1
								ELSE NULL
							END
						)
					,0)
				, 0)
			)
			-
			(
				ISNULL(
					NULLIF(
						COUNT(
							 CASE
								 WHEN DATEDIFF(mm, [FundedDate], @Now) <= 3
									THEN 1
								 ELSE NULL
							 END
						)
					, 0) * 1.0
					/
					 NULLIF(
						COUNT(
							CASE
								WHEN DATEDIFF(mm, [FundedDate], @Now) BETWEEN 4 AND 7
									THEN 1
								ELSE NULL
							END
						)
					, 0)
				, 0)
			) AS 'Quarterly FPD vs Volume %',
			SUM(
				CASE
					WHEN DATEDIFF(mm, [FundedDate], @Now) >= 12 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 1.25
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 11 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 1.21
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 10 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 1.15
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 9 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 1.09
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 8 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 1.02
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 7 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.95
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 6 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.88
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 5 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.79
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 4 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.71
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 3 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.62
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 2 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.44
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 1 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.25
					WHEN DATEDIFF(mm, [FundedDate], @Now) = 0 THEN COALESCE((AmountPaid)/NULLIF(([FundedAmount]), 0), 0) - 0.06
					ELSE 0
				END
			)
			/
			NULLIF(
				COUNT(Loans.LoanID)
			, 0) AS 'Actual vs. Target'
		FROM Dealers
		LEFT JOIN Loans
			ON Loans.DealerID = Dealers.ID
		GROUP BY Dealers.Name, Dealers.ID
	),
	RetailerRiskScores AS
	(
		SELECT
			Retailers.[Retailer ID],
			(
				(
					CASE 
						WHEN [3 mo. FPD $] >= 0.60 THEN 0 
						WHEN [3 mo. FPD $] >= 0.40 THEN 1
						WHEN [3 mo. FPD $] >= 0.30 THEN 2
						WHEN [3 mo. FPD $] >= 0.20 THEN 3
						WHEN [3 mo. FPD $] >= 0.10 THEN 4
						ELSE 5
					END * 3
				)
				+  
				(
					CASE 
						WHEN [3 mo. Charge-Off %] >= 0.30 THEN 0
						WHEN [3 mo. Charge-Off %] >= 0.25 THEN 1
						WHEN [3 mo. Charge-Off %] >= 0.20 THEN 2
						WHEN [3 mo. Charge-Off %] >= 0.15 THEN 3
						WHEN [3 mo. Charge-Off %] >= 0.10 THEN 4
						WHEN [3 mo. Charge-Off %] >= 0.00 THEN 5
						ELSE 5
					END * 3
				)
				+
				(
					CASE 
						WHEN [3 mo. Paid %] >= 0.30 THEN 5
						WHEN [3 mo. Paid %] >= 0.25 THEN 4
						WHEN [3 mo. Paid %] >= 0.20 THEN 3
						WHEN [3 mo. Paid %] >= 0.15 THEN 2
						WHEN [3 mo. Paid %] >= 0.10 THEN 1
						WHEN [3 mo. Paid %] >= 0.00 THEN 0
						ELSE 5
					END * 3
				)
				+
				(
					CASE
						WHEN COALESCE(NULLIF([One Year Multiple], 0), [Overall Closing Multiple]) >= 1.30 THEN 5
						WHEN COALESCE(NULLIF([One Year Multiple], 0), [Overall Closing Multiple]) >= 1.25 THEN 4
						WHEN COALESCE(NULLIF([One Year Multiple], 0), [Overall Closing Multiple]) >= 1.15 THEN 3
						WHEN COALESCE(NULLIF([One Year Multiple], 0), [Overall Closing Multiple]) >= 1.00 THEN 2
						WHEN COALESCE(NULLIF([One Year Multiple], 0), [Overall Closing Multiple]) >= 0.70 THEN 1
						WHEN COALESCE(NULLIF([One Year Multiple], 0), [Overall Closing Multiple]) >= 0.00 THEN 0
						ELSE 0
					END * 4
				)
				+
				(
					CASE
						WHEN [Actual vs. Target] >= 0.15 THEN 5
						WHEN [Actual vs. Target] >= 0.05 THEN 4
						WHEN [Actual vs. Target] >= 0.00 THEN 3
						WHEN [Actual vs. Target] >= -0.05 THEN 2
						WHEN [Actual vs. Target] >= -0.10 THEN 1
						WHEN [Actual vs. Target] >= -0.15 THEN 0
						ELSE 0
					END * 3
				)
				+
				(
					CASE
						WHEN [3 mo. Bounced %] >= 0.45 THEN 0
						WHEN [3 mo. Bounced %] >= 0.30 THEN 1
						WHEN [3 mo. Bounced %] >= 0.20 THEN 2
						WHEN [3 mo. Bounced %] >= 0.10 THEN 3
						WHEN [3 mo. Bounced %] >= 0.05 THEN 4
						WHEN [3 mo. Bounced %] >= 0.00 THEN 5
						ELSE 5
					END * 2
				)
				+
				(
					CASE 
						WHEN [Quarterly FPD vs Volume %] >= 0.30 THEN 0
						WHEN [Quarterly FPD vs Volume %] >= 0.20 THEN 1
						WHEN [Quarterly FPD vs Volume %] >= 0.00 THEN 2
						WHEN [Quarterly FPD vs Volume %] >= -0.20 THEN 3
						WHEN [Quarterly FPD vs Volume %] >= -0.40 THEN 4
						WHEN [Quarterly FPD vs Volume %] >= -0.80 THEN 5
						ELSE 5
					END * 2
				)
			) / 10.0 AS [Dealer Risk Score]
		FROM Retailers
	)
	UPDATE dbo.Dealers
	SET Dealers.RiskScore = RetailerRiskScores.[Dealer Risk Score]
	FROM RetailerRiskScores
	WHERE Dealers.ID = RetailerRiskScores.[Retailer ID]


	-- INSERT NEW RISK SCORE RECORDS

	INSERT INTO dbo.DealerRiskScoreHistory
	(
		DealerID,
		StartDate,
		EndDate,
		RiskScore
	)
	SELECT
		Dealers.ID AS DealerID,
		GETDATE(),
		NULL,
		Dealers.RiskScore
	FROM dbo.Dealers
	LEFT JOIN dbo.DealerRiskScoreHistory
		ON DealerRiskScoreHistory.DealerID = Dealers.ID			
	WHERE DealerRiskScoreHistory.DealerID IS NULL


	-- UPDATE EXISTING RISK SCORE RECORDS

	---- FIND RETAILERS WHO HAD THEIR SCORE CHANGE SINCE YESTERDAY
	SELECT
		ID AS DealerID
	INTO #ChangedRetailers
	FROM dbo.Dealers
	INNER JOIN dbo.DealerRiskScoreHistory
		ON Dealers.ID = DealerRiskScoreHistory.DealerID
			AND DealerRiskScoreHistory.EndDate IS NULL
	WHERE Dealers.RiskScore <> ISNULL(DealerRiskScoreHistory.RiskScore, 0)

	---- UPDATE HISTORY ENDDATES FOR RETAILERS WHO HAD THEIR SCORE CHANGE SINCE YESTERDAY
	UPDATE dbo.DealerRiskScoreHistory
	SET EndDate = GETDATE()
	FROM #ChangedRetailers
	WHERE DealerRiskScoreHistory.DealerID = #ChangedRetailers.DealerID
		AND DealerRiskScoreHistory.EndDate IS NULL

	---- INSERT HISTORY RECORDS FOR RETAILERS WHO HAD THEIR SCORE CHANGE SINCE YESTERDAY
	INSERT INTO dbo.DealerRiskScoreHistory
	(
		DealerID,
		StartDate,
		EndDate,
		RiskScore
	)
	SELECT
		Dealers.ID AS DealerID,
		GETDATE(),
		NULL,
		Dealers.RiskScore
	FROM dbo.Dealers
	INNER JOIN #ChangedRetailers
		ON #ChangedRetailers.DealerID = Dealers.ID

	DROP TABLE #ChangedRetailers;

	COMMIT TRANSACTION;

END

GO


