set nocount on
go 
--// Global Table
IF OBJECT_ID('tempdb..##ProductLimits') IS NOT NULL
drop table ##ProductLimits

create table ##ProductLimits(CategoryName varchar(255), LowerLimit integer, Average integer, MaxLimit int)

insert into ##ProductLimits values('Bikes',200,1100,2500)
insert into ##ProductLimits values('Medical Supplies',200,1100,2500)
insert into ##ProductLimits values('Golf',200,1100,2500)
insert into ##ProductLimits values('Exercise Equipment',200,1100,2500)
insert into ##ProductLimits values('Appliance/Electronics/Video',200,3500,5000)
insert into ##ProductLimits values('Appliance',200,3500,5000)
insert into ##ProductLimits values('Jewelry',200,1800,1800)
insert into ##ProductLimits values('Furniture',200,5000,5000)
insert into ##ProductLimits values('Mattress',200,5000,5000)
insert into ##ProductLimits values('Car Audio',200,2500,5000)
insert into ##ProductLimits values('Tires/Rims or Tires / Rims',200,2500,5000)
insert into ##ProductLimits values('All Other',200,1250,2500)

/*
SELECT 
	Dealers.ID as DealerID
    ,Dealers.[Name]
    ,Categories.[CategoryName]
	,ProductLimits.*
 FROM Dealers 
	inner join DealerCategories on Dealers.id=DealerCategories.DealerID
	inner join Categories on Categories.ID = DealerCategories.CategoryID
	inner join ##ProductLimits as ProductLimits on Categories.CategoryName = ProductLimits.CategoryName
*/
Go

