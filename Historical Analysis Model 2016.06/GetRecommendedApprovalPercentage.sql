if (select OBJECT_ID('[dbo].[udf_GetRecommendedApprovalPercentage]')) IS NOT NULL
DROP FUNCTION [dbo].udf_GetRecommendedApprovalPercentage
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].udf_GetRecommendedApprovalPercentage(
	@intCrystalBallNode int)
returns numeric(14,2)
as
begin
	declare @decReturnValue numeric(14,2)
		if @decReturnValue is null and @intCrystalBallNode= 1  set @decReturnValue = .90
		if @decReturnValue is null and @intCrystalBallNode= 2  set @decReturnValue = .65
		if @decReturnValue is null and @intCrystalBallNode= 3  set @decReturnValue = .70
		if @decReturnValue is null and @intCrystalBallNode= 4  set @decReturnValue = .25
		if @decReturnValue is null and @intCrystalBallNode= 5  set @decReturnValue = .35
		if @decReturnValue is null and @intCrystalBallNode= 6  set @decReturnValue = .40
		if @decReturnValue is null and @intCrystalBallNode= 7  set @decReturnValue = .20
		if @decReturnValue is null and @intCrystalBallNode= 8  set @decReturnValue = .30
		if @decReturnValue is null and @intCrystalBallNode= 9  set @decReturnValue = .40
		if @decReturnValue is null and @intCrystalBallNode= 10  set @decReturnValue = .20
		if @decReturnValue is null and @intCrystalBallNode= 11  set @decReturnValue = .10
		if @decReturnValue is null and @intCrystalBallNode= 12  set @decReturnValue = .30
		if @decReturnValue is null and @intCrystalBallNode= 13  set @decReturnValue = .20
		if @decReturnValue is null and @intCrystalBallNode= 14  set @decReturnValue = .15
		if @decReturnValue is null and @intCrystalBallNode= 15  set @decReturnValue = .50
		if @decReturnValue is null and @intCrystalBallNode= 16  set @decReturnValue = .35
		if @decReturnValue is null and @intCrystalBallNode= 17  set @decReturnValue = .65
		if @decReturnValue is null and @intCrystalBallNode= 18  set @decReturnValue = .90
		if @decReturnValue is null and @intCrystalBallNode= 19  set @decReturnValue = .85
		if @decReturnValue is null and @intCrystalBallNode= 20  set @decReturnValue = .85
		if @decReturnValue is null and @intCrystalBallNode= 21  set @decReturnValue = 1.00
		if @decReturnValue is null and @intCrystalBallNode= 22  set @decReturnValue = 1.00
		if @decReturnValue is null and @intCrystalBallNode= 23  set @decReturnValue = .75
		if @decReturnValue is null and @intCrystalBallNode= 24  set @decReturnValue = .85
		if @decReturnValue is null and @intCrystalBallNode= 25  set @decReturnValue = .25
		if @decReturnValue is null and @intCrystalBallNode= 26  set @decReturnValue = .55
		if @decReturnValue is null and @intCrystalBallNode= 27  set @decReturnValue = .65
		if @decReturnValue is null and @intCrystalBallNode= 28  set @decReturnValue = .15
		if @decReturnValue is null and @intCrystalBallNode= 29  set @decReturnValue = .30
		if @decReturnValue is null and @intCrystalBallNode= 30  set @decReturnValue = .65
		if @decReturnValue is null and @intCrystalBallNode= 31  set @decReturnValue = .15
		if @decReturnValue is null and @intCrystalBallNode= 32  set @decReturnValue = .30
		--// Default Value
		if @decReturnValue is null set @decReturnValue = 0
	return @decReturnValue
end

GO
