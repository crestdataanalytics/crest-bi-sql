
		SELECT 
			Loans.ID, 
			Loans.StatusID, 
			Loans.Amount AS FaceValue, 
			CASE WHEN FundedDate IS NOT NULL 
				 THEN ISNULL(FundedAmount, Loans.Amount - ISNULL(Loans.DiscountCollected, Loans.Amount * 0.06) - ISNULL(Loans.ApplicationFee, 40))
				 ELSE NULL END AS FundedAmount,
			ISNULL(Loans.ApplicationFee, 40) AS ApplicationFee, 
			ISNULL(Loans.DiscountCollected, Loans.Amount * 0.06) AS Discount, 
			Loans.DealerRanking,
			States.RegionId
		FROM dbo.Loans
			JOIN dbo.LoanStatuses ls  ON ls.ID = Loans.StatusID
			JOIN dbo.Dealers ON Loans.DealerID = Dealers.ID AND Dealers.IsDemoAccount=0
			JOIN dbo.Applicants ON Loans.ApplicantID = Applicants.ID
			JOIN dbo.Addresses ON Dealers.AddressID = Addresses.ID
			JOIN dbo.LoanOwnerLoans ON Loans.ID = LoanOwnerLoans.LoanID
			LEFT JOIN dbo.Users ON Users.ID = Dealers.DealerRepID
			LEFT JOIN dbo.States ON Addresses.StateID = States.ID
		WHERE 
			FundedDate between '04/01/2016' and '04/02/2016'
			AND Loans.StatusID IN (4, 5, 6, 12, 13, 14, 22, 21, 20, 10, 9)


		SELECT 
			avg(Loans.DealerRanking) as AverageDealerRanking
		FROM dbo.Loans
			JOIN dbo.LoanStatuses ls  ON ls.ID = Loans.StatusID
			JOIN dbo.Dealers ON Loans.DealerID = Dealers.ID AND Dealers.IsDemoAccount=0
			JOIN dbo.Applicants ON Loans.ApplicantID = Applicants.ID
			JOIN dbo.Addresses ON Dealers.AddressID = Addresses.ID
			JOIN dbo.LoanOwnerLoans ON Loans.ID = LoanOwnerLoans.LoanID
			LEFT JOIN dbo.Users ON Users.ID = Dealers.DealerRepID
			LEFT JOIN dbo.States ON Addresses.StateID = States.ID
		WHERE 
			FundedDate between '04/01/2016' and '04/02/2016'
			AND Loans.StatusID IN (4, 5, 6, 12, 13, 14, 22, 21, 20, 10, 9)

