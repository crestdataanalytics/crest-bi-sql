'///////////////////////////////////////////////////////
' Purpose: 
'		Create HTML File with all data for a given LoadID
' Inputs: 
'		LoanID, FileName
' Returns: 
'		Creates File
'///////////////////////////////////////////////////////
Option Explicit
'On Error Resume Next
Dim strLoanID, strFileName

If Wscript.Arguments.Count=1 Then 
	strLoanID = Wscript.Arguments.Item(0) 
Else 	
	strLoanID = "1013769"
End If	

If Wscript.Arguments.Count=2 Then 
	strFileName = Wscript.Arguments.Item(1) 
Else 	
	strFileName = "C:\Temp\Temp.htm"
End If	


Main strLoanID, strFileName

Function Main(intLoanID, strFileName)
	Dim strConnection, strQuery, strHTML
	
	strConnection = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CrestFinancial_20160424;Data Source=."
	strQuery = "execute LoanSearch @LoanID=" + CStr(intLoanID)
	
	strHTML = SQLToHTML(strConnection, strQuery)
	strHTML = Replace(strHTML, "  ", "<BR>")
	
	CreateHTMLFile strHTML, strFileName
		
	Main = 0
End Function

Sub CreateHTMLFile(strHTML, strFileName)
	Dim objFSO
	Dim objFile
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFile = objFSO.OpenTextFile(strFileName, 2, True)

   	objFile.Write(strHTML)

	Set objFSO = Nothing	
	Set objFile = Nothing
End Sub

Function SQLToHTML(strConnection, strQuery)
Dim strHeader, strFooter, strHTML, objConnection, objRecordSet
	strHeader = "<!DOCTYPE html PUBLIC ""-//W3C//DTD HTML 4.01//EN"" ""http://www.w3.org/TR/html4/strict.dtd""><html><head><meta content=""text/html; charset=ISO-8859-1"" http-equiv=""content-type""><title>Data</title></head><body><table style=""text-align: left; background-color: rgb(239, 239, 239);"" border=""1"" cellpadding=""5"" cellspacing=""0""><tbody> "
	strFooter = "</tbody></table><span style=""padding: 0px;""></span></body></html>"

	strHTML = strHeader
	
	'// DB Connect
	Set objConnection = CreateObject("ADODB.Connection")
	objConnection.ConnectionString = strConnection
	objConnection.Open
	Set objRecordSet = objConnection.Execute(strQuery)
	

	Do While Not objRecordSet Is Nothing
		If objRecordSet.Fields.Count>0 Then 
			strHTML = strHTML & GetInnerHTML(objRecordSet)
		End If			
		Set objRecordSet = objRecordSet.NextRecordset()		
	Loop	

	'//Debug.WriteLine strHTML
	
	SQLToHTML = strHTML & strFooter
'//	Debug.WriteLine strHTML
End Function


Function GetInnerHTML(objRecordSet)
	Dim strInnerHTML, strRowStart, strRowEnd, strFirstRowCellStart, strFirstRowCellEnd, strCellStart, strCellEnd, strHighlightedCellStart, objField
	strInnerHTML = ""
	'// Rows
	strRowStart = "<tr>"
	strRowEnd = "</tr>"

	'// Columns
	strFirstRowCellStart = "<th style=""background-color: rgb(204, 204, 204);width: 20px;"">"
	strFirstRowCellEnd = "</th>"	
	
	strCellStart = "<td style=""white-space:wrap"">"
	'strCellStart = "<td style=""white-space:nowrap"">"
	strCellEnd = "<br></td>"
	strHighlightedCellStart = "<td style=""white-space: wrap; background-color: rgb(255, 255, 204);"">"
	'strHighlightedCellStart = "<td style=""white-space: nowrap; background-color: rgb(255, 255, 204);"">"

	'// Only set values if record is found
	if objRecordset.EOF=False and objRecordset.BOF=False Then
		strInnerHTML = strInnerHTML & strRowStart
		for each objField in objRecordset.Fields
			strInnerHTML = strInnerHTML & strFirstRowCellStart & "  " & objField.Name & "  " &  strFirstRowCellEnd
		Next
		strInnerHTML = strInnerHTML & strRowEnd
		
		While (Not objRecordset.EOF)
			strInnerHTML = strInnerHTML & strRowStart
			for each objField in objRecordset.Fields
				If left(objField.Value,2)="//" Then
					strInnerHTML = strInnerHTML & strHighlightedCellStart & objField.Value & strCellEnd
				Else
					strInnerHTML = strInnerHTML & strCellStart & objField.Value & strCellEnd
				End If
			Next
			strInnerHTML = strInnerHTML & strRowEnd
			objRecordset.MoveNext
		Wend
	Else 
		strInnerHTML =""
		Exit Function
	End If		
	GetInnerHTML = strInnerHTML
	
End Function	
