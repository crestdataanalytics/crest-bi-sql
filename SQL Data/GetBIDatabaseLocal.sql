USE [Crest Business Intelligence]
GO

If exists(select * from sys.tables where name='Retailers') 
	DROP TABLE [dbo].[Retailers]
GO
If exists(select * from sys.tables where name='Owners') 
	DROP TABLE [dbo].[Owners]
GO
If exists(select * from sys.tables where name='Leases') 
	DROP TABLE [dbo].[Leases]
GO
If exists(select * from sys.tables where name='Lease Queues') 
	DROP TABLE [dbo].[Lease Queues]
GO
If exists(select * from sys.tables where name='Finance Plans') 
	DROP TABLE [dbo].[Finance Plans]
GO
If exists(select * from sys.tables where name='Dates') 
	DROP TABLE [dbo].[Dates]
GO
If exists(select * from sys.tables where name='Customers') 
	DROP TABLE [dbo].[Customers]
GO
select * into [dbo].[Customers] from [CFREPORTS].[Crest Business Intelligence].[dbo].[Customers]
select * into [dbo].[Dates] from [CFREPORTS].[Crest Business Intelligence].[dbo].[Dates]
select * into [dbo].[Finance Plans] from [CFREPORTS].[Crest Business Intelligence].[dbo].[Finance Plans]
select * into [dbo].[Lease Queues] from [CFREPORTS].[Crest Business Intelligence].[dbo].[Lease Queues]
select * into [dbo].[Leases] from [CFREPORTS].[Crest Business Intelligence].[dbo].[Leases]
select * into [dbo].[Owners] from [CFREPORTS].[Crest Business Intelligence].[dbo].[Owners]
select * into [dbo].[Retailers] from [CFREPORTS].[Crest Business Intelligence].[dbo].[Retailers]


if exists(select * from sys.key_constraints where object_Name(parent_object_id)='Customers' and name = 'PK_Customers')
	alter table [Customers] drop constraint PK_Customers
alter table [Customers] add constraint PK_Customers Primary Key ([Customer ID])

if exists(select * from sys.key_constraints where object_Name(parent_object_id)='Dates' and name = 'PK_Dates')
	alter table [Dates] drop constraint PK_Dates
alter table [Dates] add constraint PK_Dates Primary Key ([Date])

if exists(select * from sys.key_constraints where object_Name(parent_object_id)='Finance Plans' and name = 'PK_FinancePlans')
	alter table [Finance Plans] drop constraint PK_FinancePlans
alter table [Finance Plans] add constraint PK_FinancePlans Primary Key ([FinancePlanKey])

if exists(select * from sys.key_constraints where object_Name(parent_object_id)='Lease Queues' and name = 'PK_LeaseQueues')
	alter table [Lease Queues] drop constraint PK_LeaseQueues
alter table [Lease Queues] add constraint PK_LeaseQueues Primary Key ([LeaseQueueKey])

if exists(select * from sys.key_constraints where object_Name(parent_object_id)='Leases' and name = 'PK_Lease')
	alter table [Leases] drop constraint PK_Lease
alter table [Leases] add constraint PK_Lease Primary Key ([Lease ID])

if exists(select * from sys.key_constraints where object_Name(parent_object_id)='Owners' and name = 'PK_Owners')
	alter table [Owners] drop constraint PK_Owners
alter table [Owners] add constraint PK_Owners Primary Key ([Owner ID])

if exists(select * from sys.key_constraints where object_Name(parent_object_id)='Retailers' and name = 'PK_Retailers')
	alter table [Retailers] drop constraint PK_Retailers
alter table [Retailers] add constraint PK_Retailers Primary Key ([Retailer ID])

