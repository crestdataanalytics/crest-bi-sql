if exists(select * from INFORMATION_SCHEMA.ROUTINES where Specific_Name='LoanSearch') 
	Drop procedure LoanSearch
Go

create procedure LoanSearch @LoanID varchar(255)
as

set nocount on
--// Session Specific
IF OBJECT_ID('tempdb..#HoldTable') IS NOT NULL
drop table #HoldTable

--declare @LoanID varchar(255)
--set @LoanID = '621505'

declare @SQLCommand nvarchar(4000)
declare @CurrentTable varchar(4000)


select Table_Name as TableName, 'if exists(Select * from [' + [TABLE_CATALOG] + '].[' + Table_Schema + '].[' + Table_Name + '] where LoanID=' + @LoanID + ')'+ ' Select ''' + Table_Name + ''' as Table_Name,* from [' + [TABLE_CATALOG] + '].[' + Table_Schema + '].[' + Table_Name + '] where LoanID=' + @LoanID  as SQLCommand
into #HoldTable
from [INFORMATION_SCHEMA].[COLUMNS] where [COLUMN_NAME] = 'LoanID' order by [TABLE_NAME] asc

while (Select count(*) from #HoldTable)>0
Begin
	Select top 1 @CurrentTable=TableName, @SQLCommand=SQLCommand from #HoldTable order by TableName asc
	execute sp_executesql @statement = @SQLCommand
	delete from #HoldTable where TableName=@CurrentTable
End


