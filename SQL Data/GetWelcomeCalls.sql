
--// Global Table
IF OBJECT_ID('tempdb..##WelcomeCalls') IS NOT NULL
drop table ##WelcomeCalls


select 
	LoanID, 
	case when WelcomeCalls>0 then 1 else 0 end as WelcomeCallSuccess,
	Result,
	Seconds
into ##WelcomeCalls
from
    (
    select LoanID, count(*) as WelcomeCalls, Result, Seconds
    from CrestFinancial_20160424.dbo.WelcomeCallUploads 
    where Result in ('Answering Machine Left Message','machine','answered')
    group by LoanId, Result, Seconds
    ) as WelcomeCalls

select * from ##WelcomeCalls