--// Global Table
IF OBJECT_ID('tempdb..##VariableData') IS NOT NULL
drop table ##VariableData

select '1234 Global' as MyField into ##VariableData

--// Session Specific
IF OBJECT_ID('tempdb..#VariableData') IS NOT NULL
drop table #VariableData

select '1234 Local' as MyField into #VariableData

select * from ##VariableData
select * from #VariableData

