----------------------------

------------------------------------------------------------------------------------------------------------------------------------
-- Existing Data and Rate of Growth Evaluation Code
------------------------------------------------------------------------------------------------------------------------------------
--USE CrestWarehouse

--DECLARE @CutOffDate DATE = DATEADD(WEEK,-1,GETDATE());	-- Get a 1 week (168 hour) sample of row count growth as a representative sample of how fast the table is growing.
--SELECT  @CutOffDate AS [Cut Off Date];

--DECLARE @TotalRowCount INT = (SELECT COUNT(*) AS [RowCount]    FROM UserActions WITH(NOLOCK)); -- Get the current  row count.
--SELECT  @TotalRowCount AS [Current Row Count];
--DECLARE @1HourRowCount INT = (SELECT count(*) as [OldRowCount] FROM UserActions WITH(NOLOCK)   -- Get the week ago row count.
--                              WHERE CreatedAt < @CutOffDate);

--SELECT @1HourRowCount = (@TotalRowCount - @1HourRowCount)/168;
--SELECT @1HourRowCount    AS [Average Row Count Growth Per Hour];   -- This is the average row count growth per 1 hour.
--SELECT @1HourRowCount/60 AS [Average Row Count Growth Per Minute]; -- This is the average row count growth per 1 minute.
--GO
--return -- Safety code.
------------------------------------------------------------------------------------------------------------------------------------
-- Row Deletion Code
------------------------------------------------------------------------------------------------------------------------------------
USE CrestAnalytics
-- 104,473,732 Total Rows on M/01/23/2017/15:16.
--  25,058,352   Old Rows on M/01/23/2017/15:16 (6 months old).
--      20,961 Rows/Hour   added in the previous week (168 hours). 
--         349 Rows/Minute added per minute in the previous week (168 hours). 
--******************************************************************************************
-- GOAL: Get row deletions caught up in approximately one week for all data older than 6 months.
-- 25,058,352 Old    Rows/1 Week (10080 minutes per week) = ~2,485 rows per minute. 
--        349 Growth Rows/1 Minute 
-- 2,485 rows/minute + 349 rows per/minute ~ 3,000 rows/minute to be deleted.
--------------------
BEGIN TRY
	WHILE(1=1)
	BEGIN
		DECLARE @CutOffDate DATE = '2017-07-14'--DATEADD(M,-6,GETDATE()); -- 6 Months Ago.
		
		DELETE TOP(4000) FROM [CrestAnalytics].[dbo].[XMLShred_Data]
			WHERE CreatedDate > @CutoffDate;

		WAITFOR DELAY '00:00:00.001'; -- Delay between loop executions.		
	END
END TRY
---------------------------------------------------------------------------------------------
BEGIN CATCH
	DECLARE   @Message VARCHAR(MAX) = ERROR_MESSAGE();
	SELECT    @Message;
	PRINT     @Message;
	RAISERROR(@Message, 25, 0) WITH LOG;
END CATCH