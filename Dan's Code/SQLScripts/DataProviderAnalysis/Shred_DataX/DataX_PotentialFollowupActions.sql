SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-01'
				AND DocumentInfos.ProviderID = 3
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentVariables.VariableID,
			DocumentIDs.GenerationDate,
			CAST(DocumentVariables.VariableValue AS varchar(1)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		Variables.VariableID IN (7715,8113,8137,7768,7960)
			

--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	7715	/DataxResponse/Response/Detail/IDVSegment/PotentialFollowupActions/Action[1]/Code
--3	DataX	8113	/DataxResponse/Response/Detail/IDVSegment/PotentialFollowupActions/Action/Code
--3	DataX	8137	/DataxResponse/Response/Detail/IDVSegment/PotentialFollowupActions/Action[3]/Code
--3	DataX	7768	/DataxResponse/Response/Detail/IDVSegment/PotentialFollowupActions/Action[2]/Code
--3	DataX	7960	/DataxResponse/Response/Detail/IDVSegment/PotentialFollowupActions/Action[4]/Code