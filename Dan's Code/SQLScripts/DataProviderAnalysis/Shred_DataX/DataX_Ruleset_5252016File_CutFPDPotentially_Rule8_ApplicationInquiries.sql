SET Transaction Isolation Level Read Uncommitted;

SELECT		top 1 ID AS APRLID,
			ApplicantID,
			CHARINDEX('ApplicationInquiries>%60',ResponseXML),
			ResponseXML,
			SUBSTRING(ResponseXML, CHARINDEX('ApplicationInquiries>%<Total days="60"%</ApplicationInquiries>',ResponseXML), 100) AS ApplicationInquiries_60
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog 
WHERE		CreatedDate > '2016-08-01'
			AND ApprovalProviderSettingsID = 2
			AND ResponseXML LIKE '%ApplicationInquiries>%<Total days="60"%</ApplicationInquiries>%'
			

--WorkPhoneByBankAccount days="120"

--SELECT		top 1 ID AS APRLID,
--			ApplicantID,
--			CHARINDEX('WorkPhoneByBankAccount days="120"',ResponseXML)
--FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog 
--WHERE		ApprovalProviderSettingsID = 2
--			AND CreatedDate > '2016-08-01'
--			AND ResponseXML LIKE '%WorkPhoneByBankAccount days="120"%'
