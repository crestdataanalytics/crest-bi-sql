
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-09'
				AND DocumentInfos.ProviderID = 3
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(50)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID IN (7480,7817,7471,7787,7501)



--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	7480	/DataxResponse/Response/Detail/IDVSegment/ChronologyHistories/History[1]/Address/StreetAddress1
--3	DataX	7817	/DataxResponse/Response/Detail/IDVSegment/ChronologyHistories/History[1]/DateFirstSeen/Year
--3	DataX	7471	/DataxResponse/Response/Detail/IDVSegment/ChronologyHistories/History[1]/DateFirstSeen/Month
--3	DataX	7787	/DataxResponse/Response/Detail/IDVSegment/ChronologyHistories/History[1]/DateLastSeen/Month
--3	DataX	7501	/DataxResponse/Response/Detail/IDVSegment/ChronologyHistories/History[1]/DateLastSeen/Year


