SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-01'
				AND DocumentInfos.ProviderID = 3
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentVariables.VariableID,
			DocumentIDs.GenerationDate,
			CAST(DocumentVariables.VariableValue AS varchar(100)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		Variables.VariableID = 47436
			

--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	47436	/DataxResponse/Response/Detail/BAVSegment/DataXCode