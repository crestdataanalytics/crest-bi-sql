SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-01'
				AND DocumentInfos.ProviderID = 3
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentVariables.VariableID,
			DocumentIDs.GenerationDate,
			CAST(DocumentVariables.VariableValue AS varchar(2)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		Variables.VariableID IN (45983,7961,8170,7792,8711,8202,8079,12258,8171,39492,7928,7716,8114,7962,8696,39490,7505,8172,7823)
			

--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	45983	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[18]/Code
--3	DataX	7961	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[10]/Code
--3	DataX	8170	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[11]/Code
--3	DataX	7792	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[2]/Code
--3	DataX	8711	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[14]/Code
--3	DataX	8202	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[6]/Code
--3	DataX	8079	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[12]/Code
--3	DataX	12258	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[15]/Code
--3	DataX	8171	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[9]/Code
--3	DataX	39492	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[17]/Code
--3	DataX	7928	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[8]/Code
--3	DataX	7716	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[3]/Code
--3	DataX	8114	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[7]/Code
--3	DataX	7962	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[5]/Code
--3	DataX	8696	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[13]/Code
--3	DataX	39490	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[16]/Code
--3	DataX	7505	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[1]/Code
--3	DataX	8172	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator/Code
--3	DataX	7823	/DataxResponse/Response/Detail/IDVSegment/RiskIndicators/Indicator[4]/Code