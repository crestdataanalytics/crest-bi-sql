SET Transaction Isolation Level Read Uncommitted;

SELECT		DISTINCT DocumentInfos.ProviderID,
			Providers.ProviderName,
			Variables.VariableID,
			Variables.VariableName
FROM		DocumentInfos
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
INNER JOIN	Providers ON Providers.ProviderID = DocumentInfos.ProviderID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentInfos.ProviderID = 10
			AND DocumentInfos.GenerationDate >= '2016-12-16'
			--AND DocumentVariables.VariableID = 39756
			AND Variables.VariableName LIKE '%previousinquiries%'



--Let's me know the different variable values for a given VariableID
SELECT		COUNT(DocumentVariables.VariableValue) AS Total,
			DocumentVariables.VariableValue
FROM		DocumentVariables
INNER JOIN	DocumentInfos ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
WHERE		DocumentVariables.VariableID = 45993 and DocumentInfos.GenerationDate >= '2016-10-01'
GROUP BY	DocumentVariables.VariableValue

--SELECT		DocumentInfos.SubjectID,
--			DocumentInfos.GenerationDate,
--			DocumentInfos.DocumentID,
--			DocumentVariables.VariableValue
--FROM		DocumentVariables
--INNER JOIN	DocumentInfos ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
--WHERE		DocumentVariables.VariableID = 7805 and DocumentInfos.GenerationDate >= '2016-10-01' and DocumentVariables.VariableValue = 5



--ProviderID	ProviderName
--2	Neustar
--3	DataX
--4	ClarityBank
--5	ClarityUnbanked
--6	PurePredictive
--7	Clarity
--8	MicroBuilt
--9	FactorTrust
--10	TeletrackCore
--11	EBureau
--12	GIACT
--13	DragnetValidation
--14	CrystalBall
--15	CrestKiller
--16	DragnetInsight
