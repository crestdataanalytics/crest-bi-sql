SET Transaction Isolation Level Read Uncommitted;

WITH
		DocumentIDs AS
		(
			SELECT		DocumentInfos.DocumentID,
						DocumentInfos.ProviderID,
						DocumentInfos.SubjectID,
						DocumentInfos.GenerationDate
			FROM		DocumentInfos
			WHERE		DocumentInfos.GenerationDate >= '2015-12-04 17:07:08.063'
						AND DocumentInfos.ProviderID = 3
		),
		
		Variables6250 AS
		(
			SELECT		DocumentInfos.DocumentID,
						DocumentVariables.VariableID,
						cast(DocumentVariables.VariableValue as varchar(20)) AS VariableValue,
						Providers.ProviderName
			FROM		DocumentInfos
			LEFT JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
			LEFT JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
			LEFT JOIN	Providers ON Providers.ProviderID = DocumentInfos.ProviderID
			WHERE		DocumentInfos.GenerationDate >= '2015-12-04 17:07:08.063'
						AND DocumentInfos.ProviderID = 3
						AND Variables.VariableID = 6250
		)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ProviderID,
			DocumentIDs.SubjectID AS ApplicantID,
			DocumentIDs.GenerationDate,
			Variables6250.VariableID,
			Variables6250.VariableValue,
			Variables6250.ProviderName,
			ROW_NUMBER() OVER (PARTITION BY DocumentIDs.SubjectID ORDER BY DocumentIDs.SubjectID,DocumentIDs.GenerationDate DESC) AS SequenceNumber
FROM		DocumentIDs
LEFT JOIN	Variables6250 ON Variables6250.DocumentID = DocumentIDs.DocumentID
ORDER BY	DocumentIDs.DocumentID ASC