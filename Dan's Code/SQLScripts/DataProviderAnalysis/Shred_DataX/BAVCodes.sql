SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-09-01'
				AND DocumentInfos.ProviderID = 3
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentVariables.VariableID,
			DocumentIDs.GenerationDate,
			CAST(DocumentVariables.VariableValue AS varchar(5)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		Variables.VariableID = 6250 
--and ApplicantID = 1434627
			

--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	6250	/DataxResponse/Response/Detail/BAVSegment/Code