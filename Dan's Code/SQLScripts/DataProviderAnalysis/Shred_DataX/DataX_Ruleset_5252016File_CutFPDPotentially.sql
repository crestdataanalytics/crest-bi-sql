--DataX rules to reduce FPD ... from 5/25 file we sent them
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-08-01'
				AND DocumentInfos.ProviderID = 3
)


SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(10)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID IN (8107,--	/DataxResponse/Response/Detail/DebitReportSegment/Score/Value --rule 1/rule5
			8026,--	/DataxResponse/Response/Detail/DebitReportSegment/ReportedInput/Inquiries/Summary/DaysSinceLast rule 1
			8185,--	/DataxResponse/Response/Detail/IDAIDSASegment/ScoreAction/Score rule 2/rule4/rule11
			7165,--	/DataxResponse/Response/Detail/IDVSegment/NameAddressPhone/Type rule 2
			47436,--/DataxResponse/Response/Detail/BAVSegment/DataXCode rule 3/rule4/rule6/rule11
			47435,--/DataxResponse/Response/Detail/BAVSegment/MBCode rule 3/rule4/rule6/rule11
			7816,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/WorkPhoneByBankAccount[8] rule5
			7765,--	/DataxResponse/Response/Detail/IDVSegment/ComprehensiveVerificationIndex rule6
			6433,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/TimeSinceLastInquiry/days rule7/rule9
			6258,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/DaysSinceLastAch rule7
			8021,--	/DataxResponse/Response/Detail/IDAIDSASegment/ScoreAction/Status  rule8
			6412,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/ApplicationInquiries/Total[6] rule8
			7367,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/BankAccountABA[8] rule9 
			6405,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/LastPaymentDisposition rule9
			7826,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[8]/Code rule10
			7853,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[5]/Code rule10
			7973,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[10]/Code rule10
			7495,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[2]/Code rule10
			7827,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator/Code rule10
			7828,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[4]/Code rule10
			7885,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[3]/Code rule10
			9531,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[11]/Code rule10 
			47487,--/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[12]/Code rule10
			7888,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[6]/Code rule10 
			7977,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[9]/Code rule10
			6489,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[1]/Code rule10
			7854,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[7]/Code rule10
			6352,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/FirstPaymentDefaults rule10
			7805,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/Address[9] rule10
			7442--/DataxResponse/Response/Detail/IDVSegment/NameAddressPhone/Summary rule11
			)






SELECT		DISTINCT DocumentVariables.VariableID, -- this query gives dictionary of variable name to variable id
			Variables.VariableName
FROM		DocumentVariables
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID IN (8107,--	/DataxResponse/Response/Detail/DebitReportSegment/Score/Value --rule 1/rule5
			8026,--	/DataxResponse/Response/Detail/DebitReportSegment/ReportedInput/Inquiries/Summary/DaysSinceLast rule 1
			8185,--	/DataxResponse/Response/Detail/IDAIDSASegment/ScoreAction/Score rule 2/rule4/rule11
			7165,--	/DataxResponse/Response/Detail/IDVSegment/NameAddressPhone/Type rule 2
			47436,--/DataxResponse/Response/Detail/BAVSegment/DataXCode rule 3/rule4/rule6/rule11
			47435,--/DataxResponse/Response/Detail/BAVSegment/MBCode rule 3/rule4/rule6/rule11
			7816,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/WorkPhoneByBankAccount[8] rule5
			7765,--	/DataxResponse/Response/Detail/IDVSegment/ComprehensiveVerificationIndex rule6
			6433,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/TimeSinceLastInquiry/days rule7/rule9
			6258,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/DaysSinceLastAch rule7
			8021,--	/DataxResponse/Response/Detail/IDAIDSASegment/ScoreAction/Status  rule8
			6412,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/ApplicationInquiries/Total[6] rule8
			7367,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/BankAccountABA[8] rule9
			6405,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/LastPaymentDisposition rule9
			7826,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[8]/Code rule10
			7853,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[5]/Code rule10
			7973,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[10]/Code rule10
			7495,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[2]/Code rule10
			7827,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator/Code rule10
			7828,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[4]/Code rule10
			7885,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[3]/Code rule10
			9531,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[11]/Code rule10 
			47487,--/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[12]/Code rule10
			7888,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[6]/Code rule10 
			7977,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[9]/Code rule10
			6489,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[1]/Code rule10
			7854,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[7]/Code rule10
			6352,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/FirstPaymentDefaults rule10
			7805,--	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/Address[9] rule10
			7442--/DataxResponse/Response/Detail/IDVSegment/NameAddressPhone/Summary rule11
					)





--ProviderID	ProviderName	VariableID	VariableName
	--DataX	8107	/DataxResponse/Response/Detail/DebitReportSegment/Score/Value --rule 1/rule5
	--DataX	8026	/DataxResponse/Response/Detail/DebitReportSegment/ReportedInput/Inquiries/Summary/DaysSinceLast rule 1
	--DataX	8185	/DataxResponse/Response/Detail/IDAIDSASegment/ScoreAction/Score rule 2/rule4/rule11
	--DataX	7165	/DataxResponse/Response/Detail/IDVSegment/NameAddressPhone/Type rule 2
	--DataX	47436	/DataxResponse/Response/Detail/BAVSegment/DataXCode rule 3/rule4/rule6/rule11
	--DataX	47435	/DataxResponse/Response/Detail/BAVSegment/MBCode rule 3/rule4/rule6/rule11
	--DataX 7816	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/WorkPhoneByBankAccount[8] rule5
	--DataX	7765	/DataxResponse/Response/Detail/IDVSegment/ComprehensiveVerificationIndex rule6
	--DataX	6433	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/TimeSinceLastInquiry/days rule7/rule9
	--DataX	6258	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/DaysSinceLastAch rule7
	--DataX	8021	/DataxResponse/Response/Detail/IDAIDSASegment/ScoreAction/Status  rule8
	--DataX	6412	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/ApplicationInquiries/Total[6] rule8
	--DataX 7367	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/BankAccountABA[8] rule9
	--DataX	6405	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/LastPaymentDisposition rule9
	--DataX	7826	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[8]/Code rule10
	--DataX	7853	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[5]/Code rule10
	--DataX	7973	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[10]/Code rule10
	--DataX	7495	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[2]/Code rule10
	--DataX	7827	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator/Code rule10
	--DataX	7828	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[4]/Code rule10
	--DataX	7885	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[3]/Code rule10
	--DataX	9531	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[11]/Code rule10 
	--DataX	47487	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[12]/Code rule10
	--DataX	7888	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[6]/Code rule10 
	--DataX	7977	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[9]/Code rule10
	--DataX	6489	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[1]/Code rule10
	--DataX	7854	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[7]/Code rule10
	--DataX	6352	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/AllTransactionsSummary/FirstPaymentDefaults rule10
	--DataX 7805	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/UniqueIdentifiersSummary/Address[9] rule10
	--DataX	7442	/DataxResponse/Response/Detail/IDVSegment/NameAddressPhone/Summary rule11