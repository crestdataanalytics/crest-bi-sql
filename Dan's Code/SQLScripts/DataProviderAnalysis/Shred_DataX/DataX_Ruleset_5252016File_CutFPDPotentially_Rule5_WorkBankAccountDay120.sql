SET Transaction Isolation Level Read Uncommitted;

SELECT		ID AS APRLID,
			ApplicantID,
			SUBSTRING(ResponseXML, CHARINDEX('WorkPhoneByBankAccount days="120"',ResponseXML) + 34, 1) AS WorkPhoneByBankAccountDays_120
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog 
WHERE		CreatedDate > '2016-08-01'
			AND ApprovalProviderSettingsID = 2
			AND ResponseXML LIKE '%WorkPhoneByBankAccount days="120"%'
			

--WorkPhoneByBankAccount days="120"

--SELECT		top 1 ID AS APRLID,
--			ApplicantID,
--			CHARINDEX('WorkPhoneByBankAccount days="120"',ResponseXML)
--FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog 
--WHERE		ApprovalProviderSettingsID = 2
--			AND CreatedDate > '2016-08-01'
--			AND ResponseXML LIKE '%WorkPhoneByBankAccount days="120"%'
