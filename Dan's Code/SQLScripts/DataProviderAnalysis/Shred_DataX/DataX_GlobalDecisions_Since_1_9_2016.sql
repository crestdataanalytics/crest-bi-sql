
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-01'
				AND DocumentInfos.ProviderID = 3
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(1)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID = 7266 --IN (7214,8164,7910,7710,7266,7410)



--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	7214	/DataxResponse/Response/Detail/GlobalDecision/CRABucket
--3	DataX	8164	/DataxResponse/Response/Detail/GlobalDecision/DRBucket
--3	DataX	7910	/DataxResponse/Response/Detail/GlobalDecision/IDABucket
--3	DataX	7710	/DataxResponse/Response/Detail/GlobalDecision/IDVBucket
--3	DataX	7266	/DataxResponse/Response/Detail/GlobalDecision/Result
--3	DataX	7410	/DataxResponse/Response/Detail/GlobalDecision/BAVBucket

--4620 is not included b/c no values are present there for the global decision ... just the 7266