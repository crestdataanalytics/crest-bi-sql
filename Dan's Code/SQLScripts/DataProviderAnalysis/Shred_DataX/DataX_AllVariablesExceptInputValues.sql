
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-09'
				AND DocumentInfos.ProviderID = 3
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(10)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID NOT IN (7838,8004,7861,7912,8167,7351,7998,7503,7451,7766,7268,7878,7947,7269,7979,7426,7512,8199,7896,7217,7945,7164,8037,7350,7879,
											7473,7441,7839,7919,7897,7997,7820,7946,8132,7877,7482,7860,7980,8103)



--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	7214	/DataxResponse/Response/Detail/GlobalDecision/CRABucket
--3	DataX	8164	/DataxResponse/Response/Detail/GlobalDecision/DRBucket
--3	DataX	7910	/DataxResponse/Response/Detail/GlobalDecision/IDABucket
--3	DataX	7710	/DataxResponse/Response/Detail/GlobalDecision/IDVBucket
--3	DataX	7266	/DataxResponse/Response/Detail/GlobalDecision/Result
--3	DataX	7410	/DataxResponse/Response/Detail/GlobalDecision/BAVBucket

--4620 is not included b/c no values are present there for the global decision ... just the 7266