SET Transaction Isolation Level Read Uncommitted;

DECLARE @cutoffdate AS DATETIME
SELECT @cutoffdate = '11/01/2015';

WITH Leases AS
(
	SELECT
		Loans.ID,
		Loans.DisplayID,
		Loans.DealerID,
		Loans.FundedAmount,
		Loans.CreationDate AS SubmitDate,
		Loans.AutoApprovedFlag, 
		Loans.ApprovalAmount,
		CAST(Loans.ApprovalDate AS DATE) AS ApprovedDate,
		CAST(Loans.FundedDate AS DATE) AS FundedDate,
		Loans.Amount AS LeaseAmount,
		CASE
			WHEN DataXResponseDescription = 'Denied for not enough bank information'
				THEN 1
			ELSE 0
		END AS XD00Denial,
		EligibilityResultRequirements.EligibilityResultID,
		EligibilityRequirements.RequirementName,
		EligibilityRequirements.RequirementDescription
	FROM vw_loans_reporting Loans
	LEFT JOIN AutoApprovalProcessResponses Responses ON Responses.ApplicantID = Loans.ApplicantID
	LEFT JOIN EligibilityResults ON EligibilityResults.LoanID = Loans.ID
	LEFT JOIN EligibilityResultRequirements ON EligibilityResultRequirements.EligibilityResultID = EligibilityResults.EligibilityResultID
	LEFT JOIN EligibilityRequirements ON EligibilityRequirements.EligibilityRequirementID = EligibilityResultRequirements.EligibilityRequirementID
	WHERE Loans.CreationDate > @cutoffdate AND NOT EXISTS(SELECT 1 FROM vw_loans_reporting ll WHERE ll.ApplicantID = Loans.ApplicantID AND ll.StatusID = 21)
		--and Loans.ID >= 1500927
),

LeasesFlagged AS 
(
	SELECT
		t.DisplayID,
		t.DealerID,
		t.FundedAmount,
		t.SubmitDate, 
		t.AutoApprovedFlag,
		t.ApprovedDate,
		t.FundedDate,
		t.LeaseAmount,
		t.ApprovalAmount,
		CAST(p.FirstPaymentDate AS DATE) AS FirstPaymentDate,
		CASE 
			WHEN t.ApprovedDate IS NOT NULL AND t.AutoApprovedFlag > 0
				THEN 'AUTO'
			WHEN t.ApprovedDate IS NOT NULL AND t.AutoApprovedFlag = 0
				THEN 'MANUAL'
			WHEN t.ApprovedDate IS NULL
				THEN 'NONE'
		END AS ApprovalType,
		CASE
			WHEN FirstScheduledPaymentOutcomeID IN (3,4)
				THEN 1
			ELSE 0
		END AS FPDOutcome,
		CASE
			WHEN FirstScheduledPaymentOutcomeID IN (3,4,5)
				THEN 1
			ELSE 0
		END AS FPDEligible,
		XD00Denial,
		CASE
			WHEN XD00Denial = 1 AND t.ApprovedDate IS NOT NULL
				THEN 1
			ELSE 0
		END AS XD00DenialApproved,
		t.EligibilityResultID,
		t.RequirementName,
		t.RequirementDescription
	FROM Leases t
	LEFT JOIN
	(
		SELECT
			LoanID,
			MIN(DueDate) AS FirstPaymentDate
		FROM LoanPaymentSchedule lp WITH(NOLOCK)
		INNER JOIN Leases t
			ON t.ID = lp.LoanID
		WHERE lp.ScheduleEntryDesc <> 'Initial Payment'
		GROUP BY LoanID
	) p ON p.LoanID = t.ID
	LEFT JOIN FirstScheduledPaymentDetails fpd
		ON fpd.LoanID = t.ID AND IsCurrentOutCome = 1
)

SELECT
	year(SubmitDate) as [year],
	month(SubmitDate) as [month],
	--day(SubmitDate) as [day],
	--datepart(hour,SubmitDate) as [hour],
	--CAST(SubmitDate AS DATE) AS [Date],
	Count(*) AS SubmitCount,
	SUM
	(
		CASE
			WHEN ApprovalType = 'AUTO' AND ApprovedDate IS NOT NULL
				THEN 1
			ELSE 0
		END
	) AS AutoApprovedCount,
	SUM
	(
		CASE 
			WHEN LeasesFlagged.FundedDate IS NOT NULL AND LeasesFlagged.FundedAmount > 0
				THEN 1
			ELSE 0
		END
	) AS FundedCount,
	SUM
	(
		CASE
			WHEN ApprovalType = 'MANUAL' AND ApprovedDate IS NOT NULL
				THEN 1
			ELSE 0
		END
	) AS ManualApprovedCount,
	cast(SUM
	(
		CASE
			WHEN ApprovalType = 'AUTO' AND ApprovedDate IS NOT NULL
				THEN 1
			ELSE 0
		END
	) as dec) / Count(*) * 100.0 as AutoApprovedPercent,
	SUM(ApprovalAmount) / SUM
	(
		CASE
			WHEN ApprovalType = 'AUTO' AND ApprovedDate IS NOT NULL
				THEN 1
			ELSE 0
		END
	) AS AverageApprovalAmount,
	cast(SUM
	(
		CASE
			WHEN ApprovalType = 'AUTO' AND ApprovedDate IS NOT NULL AND RequirementName IS NULL
				THEN 1
			ELSE 0
		END
	) as dec) / cast(sum(case when requirementname is null then 1 else 0 end) as decimal) * 100.0 AS AdjustedAutoApprovalPercent
FROM LeasesFlagged
GROUP BY year(SubmitDate),
	month(SubmitDate) 
	--datepart(hour,SubmitDate)
ORDER BY 1,2,3
