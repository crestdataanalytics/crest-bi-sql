
    SELECT		ApplicantID,
				ActiveCount,
				ClosedCount
    FROM
		(SELECT		ApplicantID,
					COUNT(CASE WHEN StatusID in (3,4,6) THEN 1 ELSE null END) AS ActiveCount,
					COUNT(CASE WHEN StatusID in (5,9,12,13,14,16,22) THEN 1 ELSE null END) AS ClosedCount
		FROM		Loans
		WHERE		Loans.LoanOwnerID IN (1,2,4,5,7)
		GROUP BY	ApplicantID
		) AS A
    WHERE		(ActiveCount > 0
				AND ClosedCount > 0) 
				OR (ClosedCount > 1)
    