SET Transaction Isolation Level Read Uncommitted;

--gets the ApplicantIDs
WITH ApplicantIDs AS
(
	SELECT		DISTINCT ApplicantID
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		CreatedDate >= '2016-11-06'
				AND ApprovalProviderSettingsID = 6
)

SELECT		Loans.ID AS LoanID,
			Loans.DisplayID,
			Loans.CreationDate,
			Loans.ApplicantID,
			Loans.ApprovalAmount,
			AutoApprovedFlag
FROM		Loans
INNER JOIN	ApplicantIDs ON ApplicantIDs.ApplicantID = Loans.ApplicantID
WHERE		Loans.LoanOwnerID IN (1,2,4,5,7)
--WHERE		Loans.ApplicantID = 1338751

--select * from CrestWarehouse.dbo.ApprovalProviderResponseLog where applicantid = 1338751 and CreatedDate >= '2016-11-15'