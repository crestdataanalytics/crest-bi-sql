
SET Transaction Isolation Level Read Uncommitted;



SELECT		LoanPaymentSchedule.LoanID,
			LoanPaymentSchedule.ScheduleEntryDesc, 
			LoanPaymentSchedule.DueAmount,
			CAST(LoanPaymentSchedule.DueDate AS DATE) DueDate,
			LoanPaymentSchedule.AmountPaid,
			LoanPaymentSchedule.PaidStatus,
			LoanPaymentSchedule.ID
FROM		LoanPaymentSchedule
LEFT JOIN	Loans ON Loans.ID = LoanPaymentSchedule.LoanID
WHERE		Loans.LoanOwnerID IN (1,2,4,5,7)
			AND YEAR(CreatedDate) < 2016