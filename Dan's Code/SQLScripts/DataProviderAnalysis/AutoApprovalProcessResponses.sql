

SET Transaction Isolation Level Read Uncommitted;


SELECT			AutoApprovalProcessResponses.ApplicantID,
				AutoApprovalProcessResponses.ClarityTracking AS TrackingID,
				AutoApprovalProcessResponses.ApprovalPath
FROM			AutoApprovalProcessResponses
--where			AutoApprovalProcessResponses.ApprovalPath = 'Full'