SET Transaction Isolation Level Read Uncommitted;

SELECT		Dealers.ID,
			REPLACE(REPLACE(Name, ',',''),'"','') Name,
			CAST(Dealers.CreationDate AS DATE) CreationDate,
			Ranking10Month,
			Dealers.IsDemoAccount,
			Dealers.IsDeleted,
			CAST(CancelDate AS DATE) CancelDate,
			CASE	WHEN Ranking10Month < -9 THEN 1
					WHEN Ranking10Month IS NULL THEN 0
					ELSE 0 END AS ThresholdBiggerThan1,
			Dealers.DealerRepID,
			REPLACE(Users.FirstName,',','') AS RetailerRepFirstName,
			REPLACE(Users.LastName,',','') AS RetailerRepLastName
			, DealerGroups.ID AS RetailerGroupsID
			, REPLACE(DealerGroups.DealerGroupName,',','') AS RetailerGroupName
			,Dealers.RiskScore
FROM		Dealers
INNER JOIN	Addresses ON Dealers.AddressID = Addresses.ID
LEFT JOIN	DealerReps ON Dealers.DealerRepID = DealerReps.ID 
LEFT JOIN	Users ON Users.ID = Dealers.DealerRepID
LEFT JOIN	DealerGroups ON Dealers.DealerGroupID = DealerGroups.ID
WHERE		Invalid = 0 --this refers to something not on the dealers table
--			and dealers.id = 9566
--			and Dealers.DealerRepID = 79137
--			IsDemoAccount = 0
--			AND IsDeleted = 0
--			AND CancelDate IS NULL