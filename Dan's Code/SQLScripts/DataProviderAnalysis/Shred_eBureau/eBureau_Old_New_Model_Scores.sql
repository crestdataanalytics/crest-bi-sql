
SET Transaction Isolation Level Read Uncommitted;

DECLARE @startdate Date
SET @startdate = '2016-01-01';

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > @startdate
				AND DocumentInfos.ProviderID = 11
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(50)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID IN (39800,45953,39752,39791,39756,45950,39751,39778,45955)
			AND DocumentVariables.VariableValue IS NOT NULL
			--Sand DocumentIDs.ApplicantID = 1302723


--ProviderID	ProviderName	VariableID	VariableName
--11	EBureau	39800	/root/transaction/output/score/name
--11	EBureau	39752	/root/transaction/output/score/adjusted
--11	EBureau	39791	/root/transaction/output/score/score

--11	EBureau	39756	/root/transaction/output/score[1]/score
--11	EBureau	39751	/root/transaction/output/score[1]/name
--11	EBureau	39778	/root/transaction/output/score[1]/adjusted

--11	EBureau	45953	/root/transaction/output/score[2]/adjusted
--11	EBureau	45950	/root/transaction/output/score[2]/score
--11	EBureau	45955	/root/transaction/output/score[2]/name

--must look at the three 'names' together ... the greater VariableID is the older score