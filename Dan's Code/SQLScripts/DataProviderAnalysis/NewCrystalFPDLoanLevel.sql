WITH PAY AS
(
	SELECT
		LoanID,
		cast(PaymentDate as date) as PayDate,
		cast(ReturnDate as Date) as ReturnDate,
		ROW_NUMBER() OVER(PARTITION BY LoanID ORDER BY ID ASC) as Rn
	FROM
		vw_loanPayments
	WHERE
		DeletedDate is null
		and typeID <> 9
),
PAYT AS
(
	SELECT
		LoanID,
		cast(PaymentDate as date) as PayDate,
		ROW_NUMBER() OVER(PARTITION BY LoanID ORDER BY ID ASC) as Rn
	FROM
		vw_loanPayments
	WHERE
		DeletedDate is null
		and ReturnDate is null
		and typeID <> 9
),
LD AS(
SELECT
	L.ID as LoanID,
	L.ApplicantID,
	L.StatusID,
	P.PayDate as FirstTriedPayment,
	P.ReturnDate,
	PT.PayDate as FirstSuccessfulPayment,
	cast(L.FundedDate as date) as FundedDate,
	cast(L.CreationDate as date) as CreationDate,
	L.FundedAmount,
	case when P.ReturnDate is null then 0
		when P.ReturnDate is not null and P.PayDate = PT.PayDate then 0
		else 1 end as FPD,
	case when P.ReturnDate is null then 0
		when P.ReturnDate is not null and datediff(dd,P.PayDate,PT.PayDate)<=2 then 0
		else 1 end as FPDtwo,
	case when P.ReturnDate is null then 0
		when P.ReturnDate is not null and datediff(dd,P.PayDate,PT.PayDate)<=5 then 0
		else 1 end as FPDthree,
	case when P.ReturnDate is null then 0
		when P.ReturnDate is not null and datediff(dd,P.PayDate,PT.PayDate)<=7 then 0
		else 1 end as FPDfour,
	case when P.ReturnDate is not null then 1 else 0 end as OldFPD
FROM
	Loans as L
	LEFT JOIN PAY as P
		on L.ID = P.LoanID and P.Rn = 1
	LEFT JOIN PAYT as PT
		on L.ID = PT.LoanID and PT.Rn = 1

WHERE
	(datediff(dd,P.PayDate,Getdate())>7
	OR datediff(dd,PT.PayDate,Getdate())>7)
	and L.FundedDate is not null
	and L.FundedAmount > 0
	and L.CreationDate >= '1-1-15'
	and L.CreationDate < cast(dateadd(d,-14,getdate()) as Date)
	and L.LoanOwnerID IN (1,2,4,5,7)
	and L.StatusID not in (10)
)

SELECT *
	
FROM
	LD



