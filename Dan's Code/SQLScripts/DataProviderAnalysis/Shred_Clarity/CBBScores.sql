
SET Transaction Isolation Level Read Uncommitted;

DECLARE @startdate Date
SET @startdate = '2016-01-01';

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > @startdate
				AND DocumentInfos.ProviderID = 7
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(3)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID IN (45711,44855)
			AND DocumentVariables.VariableValue IS NOT NULL


--ProviderID	ProviderName	VariableID	VariableName
--7	Clarity	45711	/xml-response/clear-bank-behavior/cbb-score2
--7	Clarity	44855	/xml-response/clear-bank-behavior/cbb-score