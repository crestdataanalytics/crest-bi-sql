
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-01'
				AND DocumentInfos.ProviderID = 7
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(150)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID = 246 --IN (45110,72,676,9781,9879,9699,1470,79,466,9895,246)
			AND DocumentVariables.VariableValue IS NOT NULL



--ProviderID	ProviderName	VariableID	VariableName
--7	Clarity	45110	/xml-response/clear-bank-behavior/deny-descriptions
--7	Clarity	72	/xml-response/clear-bank/supplier-bank/deny-descriptions
--7	Clarity	676	/xml-response/deny-descriptions
--7	Clarity	9781	/xml-response/clear-tradeline/inquiry-cluster-tradeline/deny-descriptions
--7	Clarity	9879	/xml-response/clear-tradeline/supplier-tradeline/deny-descriptions
--7	Clarity	9699	/xml-response/clear-tradeline/deny-descriptions
--7	Clarity	1470	/xml-response/inquiry/deny-descriptions
--7	Clarity	79	/xml-response/clear-bank/deny-descriptions
--7	Clarity	466	/xml-response/clear-products-request/deny-descriptions
--7	Clarity	9895	/xml-response/clear-fraud/deny-descriptions
--7	Clarity	246	/xml-response/action