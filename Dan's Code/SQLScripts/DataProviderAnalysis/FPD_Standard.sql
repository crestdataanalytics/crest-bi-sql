SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

WITH ApplicantFirstExchangeLeases AS
(
	SELECT
		ApplicantID,
		CreationDate,
		AutoApprovedFlag,
		ApprovalUserID,
		FundedDate,
		FirstScheduledPaymentDetails.NonInitialAmountPaidToDate,
		FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID,
		FirstScheduledPaymentDetails.FirstScheduledAmount,
		FirstScheduledPaymentDetails.FirstScheduledPaymentDueDate,
		ROW_NUMBER() OVER (PARTITION BY ApplicantID ORDER BY CreationDate ASC) AS LeaseSequence
	FROM dbo.Loans
	LEFT JOIN dbo.FirstScheduledPaymentDetails 
		ON Loans.ID = FirstScheduledPaymentDetails.LoanID and FirstScheduledPaymentDetails.IsCurrentOutcome = 1
	WHERE StatusID = 21 and Loans.LoanOwnerID IN (1,2,4,5,7)
),
ManuallyApprovedRevisedLeases AS
(
	SELECT
		Loans.ID,
		Loans.StatusID,
		Loans.AutoApprovedFlag AS APF,
		Loans.ApprovalUserID AS APU,
		ApplicantFirstExchangeLeases.AutoApprovedFlag AS EX_APF,
		ApplicantFirstExchangeLeases.ApprovalUserID AS EX_APU,
		ApplicantFirstExchangeLeases.CreationDate,
		ApplicantFirstExchangeLeases.FundedDate,
		ApplicantFirstExchangeLeases.NonInitialAmountPaidToDate,
		ApplicantFirstExchangeLeases.FirstScheduledPaymentOutcomeID,
		ApplicantFirstExchangeLeases.FirstScheduledAmount,
		ApplicantFirstExchangeLeases.FirstScheduledPaymentDueDate
	FROM dbo.Loans
	LEFT JOIN ApplicantFirstExchangeLeases
		ON ApplicantFirstExchangeLeases.ApplicantID = Loans.ApplicantID
		AND ApplicantFirstExchangeLeases.LeaseSequence = 1
	WHERE Loans.AutoApprovedFlag = 0
		AND ApplicantFirstExchangeLeases.AutoApprovedFlag = 0
		AND Loans.StatusID NOT IN (7,10,11,20,21)
		and Loans.LoanOwnerID IN (1,2,4,5,7)
),
AutoApprovedRevisedLeases AS
(
	SELECT
		Loans.ID,
		Loans.StatusID,
		Loans.AutoApprovedFlag AS APF,
		Loans.ApprovalUserID AS APU,
		ApplicantFirstExchangeLeases.AutoApprovedFlag AS EX_APF,
		ApplicantFirstExchangeLeases.ApprovalUserID AS EX_APU,
		ApplicantFirstExchangeLeases.CreationDate,
		ApplicantFirstExchangeLeases.FundedDate,
		ApplicantFirstExchangeLeases.NonInitialAmountPaidToDate,
		ApplicantFirstExchangeLeases.FirstScheduledPaymentOutcomeID,
		ApplicantFirstExchangeLeases.FirstScheduledAmount,
		ApplicantFirstExchangeLeases.FirstScheduledPaymentDueDate
	FROM dbo.Loans
	LEFT JOIN ApplicantFirstExchangeLeases
		ON ApplicantFirstExchangeLeases.ApplicantID = Loans.ApplicantID
		AND ApplicantFirstExchangeLeases.LeaseSequence = 1
	WHERE Loans.AutoApprovedFlag = 0
		AND ApplicantFirstExchangeLeases.AutoApprovedFlag IN (1,2,3)
		AND Loans.StatusID NOT IN (7,10,11,20,21)
		and Loans.LoanOwnerID IN (1,2,4,5,7)
),
ManuallyUnderwrittenLeasesExchangeOverride AS
(
	SELECT
		Loans.ID,
		Loans.ApplicantID,
		CASE
			WHEN ManuallyApprovedRevisedLeases.FundedDate IS NOT NULL THEN 1
			WHEN AutoApprovedRevisedLeases.FundedDate IS NOT NULL THEN 1
			ELSE 0
		END AS ExchangedLease,
		Coalesce(ManuallyApprovedRevisedLeases.EX_APU, AutoApprovedRevisedLeases.EX_APU, Loans.ApprovalUserID) as ApprovalUserID,
		Coalesce(ManuallyApprovedRevisedLeases.CreationDate, AutoApprovedRevisedLeases.CreationDate, Loans.CreationDate) as CreationDate,
		Coalesce(ManuallyApprovedRevisedLeases.FundedDate, Loans.FundedDate) as FundedDate,
		Coalesce(ManuallyApprovedRevisedLeases.EX_APF, AutoApprovedRevisedLeases.EX_APF, Loans.AutoApprovedFlag) as AutoApprovedFlag,
		CASE
			WHEN Coalesce(ManuallyApprovedRevisedLeases.FirstScheduledPaymentOutcomeID, AutoApprovedRevisedLeases.FirstScheduledPaymentOutcomeID) = 6 THEN FirstScheduledPaymentDetails.NonInitialAmountPaidToDate
			ELSE Coalesce(ManuallyApprovedRevisedLeases.NonInitialAmountPaidToDate, AutoApprovedRevisedLeases.NonInitialAmountPaidToDate, FirstScheduledPaymentDetails.NonInitialAmountPaidToDate) 
		END AS NonInitialAmountPaidToDate,
		CASE
			WHEN Coalesce(ManuallyApprovedRevisedLeases.FirstScheduledPaymentOutcomeID, AutoApprovedRevisedLeases.FirstScheduledPaymentOutcomeID) = 6 THEN FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID
			ELSE Coalesce(ManuallyApprovedRevisedLeases.FirstScheduledPaymentOutcomeID, AutoApprovedRevisedLeases.FirstScheduledPaymentOutcomeID, FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID) 
		END AS FirstScheduledPaymentOutcomeID,
		CASE
			WHEN Coalesce(ManuallyApprovedRevisedLeases.FirstScheduledPaymentOutcomeID, AutoApprovedRevisedLeases.FirstScheduledPaymentOutcomeID) = 6 THEN FirstScheduledPaymentDetails.FirstScheduledPaymentDueDate
			ELSE Coalesce(ManuallyApprovedRevisedLeases.FirstScheduledPaymentDueDate, AutoApprovedRevisedLeases.FirstScheduledPaymentDueDate, FirstScheduledPaymentDetails.FirstScheduledPaymentDueDate) 
		END AS FirstScheduledPaymentDueDate,
		CASE
			WHEN Coalesce(ManuallyApprovedRevisedLeases.FirstScheduledPaymentOutcomeID, AutoApprovedRevisedLeases.FirstScheduledPaymentOutcomeID) = 6 THEN FirstScheduledPaymentDetails.FirstScheduledAmount
			ELSE Coalesce(ManuallyApprovedRevisedLeases.FirstScheduledAmount, AutoApprovedRevisedLeases.FirstScheduledAmount, FirstScheduledPaymentDetails.FirstScheduledAmount)
		END AS FirstScheduledAmount
	FROM dbo.Loans
	LEFT JOIN ManuallyApprovedRevisedLeases
		ON ManuallyApprovedRevisedLeases.ID = Loans.ID
	LEFT JOIN AutoApprovedRevisedLeases
		ON AutoApprovedRevisedLeases.ID = Loans.ID
	LEFT JOIN dbo.FirstScheduledPaymentDetails 
		ON Loans.ID = FirstScheduledPaymentDetails.LoanID and FirstScheduledPaymentDetails.IsCurrentOutcome = 1
	WHERE Loans.StatusID IN (4,5,6,12,13,14,16,22)
			and Loans.LoanOwnerID IN (1,2,4,5,7)
),
LeaseData AS
(
	SELECT
		CONCAT(Users.FirstName,' ',Users.LastName) AS Rep,
		Loans.ID,
		Loans.ApplicantID,
		Loans.DisplayID,
		ManuallyUnderwrittenLeasesExchangeOverride.CreationDate,
		ManuallyUnderwrittenLeasesExchangeOverride.FundedDate,
		ManuallyUnderwrittenLeasesExchangeOverride.AutoApprovedFlag,
		ManuallyUnderwrittenLeasesExchangeOverride.ExchangedLease,
		ManuallyUnderwrittenLeasesExchangeOverride.NonInitialAmountPaidToDate,
		ManuallyUnderwrittenLeasesExchangeOverride.FirstScheduledPaymentOutcomeID,
		ManuallyUnderwrittenLeasesExchangeOverride.FirstScheduledPaymentDueDate,
		ManuallyUnderwrittenLeasesExchangeOverride.FirstScheduledAmount,
		Loans.StatusID,
		Loans.FundedAmount,
		Loans.DealerID,
		Loans.ApprovalAmount
	FROM dbo.Loans
	LEFT JOIN ManuallyUnderwrittenLeasesExchangeOverride
		ON ManuallyUnderwrittenLeasesExchangeOverride.ID = Loans.ID
	LEFT JOIN vw_Users AS Users
		ON ManuallyUnderwrittenLeasesExchangeOverride.ApprovalUserID = Users.ID
	WHERE  Loans.LoanOwnerID IN (1,2,4,5,7)
),
DealerMainCategories AS
(
	SELECT
		DealerCategories.DealerID,
		DealerCategories.CategoryID,
		Categories.CategoryName,
		ROW_NUMBER() OVER (PARTITION BY DealerID ORDER BY RankOrder ASC) AS RankOrder
	FROM dbo.DealerCategories
	LEFT JOIN dbo.Categories
		ON Categories.ID = DealerCategories.CategoryID
),
AJ AS
(
	SELECT
		AJ.ApplicantID,
		PPT.[Period] as PayPeriod,
		ROW_NUMBER() OVER (PARTITION BY AJ.ApplicantID ORDER BY AJ.ID DESC) AS RankOrder
	FROM ApplicantJob as AJ
	LEFT JOIN PayPeriodTypes as PPT
		ON AJ.PayPeriodTypeID = PPT.ID
)


SELECT
	LeaseData.ID AS LoanID,
	LeaseData.ApplicantID,
	LeaseData.DisplayID,
	LeaseData.CreationDate,
	LeaseData.FundedDate,
	LeaseData.StatusID,
	LeaseData.FundedAmount,
	LeaseData.ApprovalAmount,
	LeaseData.AutoApprovedFlag,
	LeaseData.ExchangedLease,
	LeaseData.NonInitialAmountPaidToDate,
	LeaseData.FirstScheduledPaymentOutcomeID,
	LeaseData.FirstScheduledPaymentDueDate,
	LeaseData.FirstScheduledAmount,
	DealerMainCategories.CategoryName,
	LeaseData.DealerID,
	LeaseData.Rep AS ApprovalUser,
	CONCAT(Users.FirstName,' ',Users.LastName) AS SalesRep,
	Dealers.RiskScore,
	Dealers.Name,
	AJ.PayPeriod,
	LoanStatuses.[Status]
FROM LeaseData
INNER JOIN Dealers
	ON Dealers.ID = LeaseData.DealerID
	AND Dealers.IsDemoAccount = 0
INNER JOIN LoanStatuses ON LoanStatuses.ID = LeaseData.StatusID
LEFT JOIN vw_Users AS Users
	ON Users.ID = Dealers.DealerRepID
LEFT JOIN DealerMainCategories
	ON DealerMainCategories.DealerID = Dealers.ID
	AND DealerMainCategories.RankOrder = 1
LEFT JOIN AJ
	ON AJ.ApplicantID = LeaseData.ApplicantID and AJ.RankOrder = 1
WHERE
	LeaseData.FundedAmount > 0
	AND LeaseData.FundedDate IS NOT NULL
	AND LeaseData.StatusID NOT IN (10,20)
	--AND DATEDIFF(DAY, LeaseData.FirstScheduledPaymentDueDate, GETDATE()) >= 7
	--and Leasedata.ApplicantID = 1039891
	--and Dealers.DealerRepID = 54950
	--and Dealers.Name like '%metropcs%'
	--and DealerMainCategories.CategoryName = 'Cell Phones'
	--and LeaseData.applicantid = 1439453


--SELECT
--	concat(datepart(year,A.CreationDate),datepart(week,A.CreationDate)) as WeekNum,
--	max(cast(A.CreationDate as date)) as WeekVintage,
--	--A.PayPeriod,
--	A.CategoryName,
--	--CONVERT(CHAR(7),A.CreationDate,120) as Vintage,
--	--A.AutoApprovedFlag,
--	--A.CategoryName,
--	--A.RiskScore,
--	--A.Name,
--	--count(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID = 3 then 1 else null end)*1.0/
--	--	count(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end) as 'FPD%',
--	--sum(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (3) then A.FundedAmount end)/
--	--	sum(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (3,4,5) then A.FundedAmount end) as 'FPD$',
--	--sum(case when A.FirstScheduledPaymentOutcomeID = 3 then A.FundedAmount end)*1.0/sum(case when A.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then A.FundedAmount end) as 'procFPD$',
--	count(distinct A.ID) as CountAll,
--	count(case when A.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as CountComeDue,
--	count(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as CountComeDueSevenDays,
--	count(case when A.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as CountProcessing,
--	count(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as CountPaid,
--	count(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as CountFPDd,
--	sum(A.FundedAmount) as FundedAll,
--	sum(case when A.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then A.FundedAmount else 0 end) as FundedComeDue,
--	sum(case when A.FirstScheduledPaymentOutcomeID in (7,8) then A.FundedAmount else 0 end) as FundedProcessing,
--	sum(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (4,5) then A.FundedAmount else 0 end) as FundedPaid,
--	sum(case when DATEDIFF(DAY, A.FirstScheduledPaymentDueDate, GETDATE()) >= 7 and A.FirstScheduledPaymentOutcomeID in (3) then A.FundedAmount else 0 end) as FundedFPDd,
--	count(case when A.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end)*1.0/count(distinct A.ID) as ReadPercent
--FROM
--	A
--WHERE 
--	A.AutoApprovedFlag = 0
--GROUP BY
--	--A.Name
--	--A.Riskscore
--	concat(datepart(year,A.CreationDate),datepart(week,A.CreationDate)),
--	A.PayPeriod
--	--A.CategoryName
--	--A.CategoryName
--	--A.AutoApprovedFlag
----	CONVERT(CHAR(7),A.CreationDate,120)
--ORDER BY
--	max(cast(A.CreationDate as date)) asc	


