

SET Transaction Isolation Level Read Uncommitted;


SELECT	Loans.CreationDate, 
		Loans.ApplicantID, 
		Loans.ID AS LoanID, 
		Loans.FundedDate,
		Loans.ChargedOffDate, 
		Loans.Amount,  
		Loans.ChargedOffAmount, 
		Loans.StatusID, 
		REPLACE(Loans.DisplayID, '-','') AS ID,  
		Loans.PaidOffDate, 
		Loans.DealerID, 
		Loans.FundedAmount, 
		Loans.LoanTermID, 
		Loans.AutoApprovedFlag, 
		Loans.ApprovalDate, 
		Loans.OriginationDate,
		LoanTerms.Months, 
		Dealers.ID as RetailerID,  
		CASE 
			WHEN Loans.AutoApprovedFlag = 1 THEN 1 
			WHEN Loans.AutoApprovedFlag = 0 THEN 0
		ELSE 0 END AS IsAutoApproved,
		'' AS LeadCampaignName, 
		'' AS LeadPurchasedIndicator,
		'' AS LeadProviderName, 
		'' AS LeadPrice, 
		'' AS RequestedLoanAmount, 
		'' AS FundingApprovedIndicator, 
		LoanStatuses.[Status],
		CASE
				WHEN	Loans.Amount = 0 THEN 0 
				WHEN	Loans.Amount != 0
						AND LoanStatuses.[Status] IN ('Approved',
													'ApprovedAwaitingDelivery',
													'ApprovedWithDocs',
													'ApprovedWithPayment',
													'Awaiting Signed Docs',
													'Denied',
													'Pending',
													'Pending Review',
													'Cancelled',
													'Flagged',
													'Ready For Funding') THEN 0
				ELSE	Loans.FundedAmount END AS MerchantPaidAmount, 
		Categories.CategoryName AS CollateralType,
		CASE
				WHEN	Loans.Amount = 0 THEN 0	
				WHEN	Loans.Amount != 0
						AND LoanStatuses.[Status] IN ('Approved',
													'ApprovedAwaitingDelivery',
													'ApprovedWithDocs',
													'ApprovedWithPayment',
													'Awaiting Signed Docs',
													'Denied',
													'Pending',
													'Pending Review',
													'Cancelled',
													'Flagged',
													'Ready For Funding') THEN 0
					ELSE Loans.Amount 
					END AS FundedLoanAmount, 
		'' AS FirstDueDate,
		'' AS SecondDueDate, 
		'' AS ThirdDueDate,
		'' AS FirstPaymentDefault,
		'' AS FirstPaymentFatalReturnIndicator,
		'' AS NinetyDayDelinquency, 
		'' AS SecondPaymentDefault,
		'' AS ThirdPaymentDefault,
		'' AS ChargeOffAmount, 
		'' AS CollectionIndicator, 
		'' as RecoveryIndicator,
		'' as AmountRecovered,
		Loans.FirstPaymentDate,
		Loans.CoapplicantID
FROM		Loans
LEFT JOIN	LoanTerms ON LoanTerms.ID = Loans.LoanTermID
INNER JOIN	Dealers ON Loans.DealerID = Dealers.ID
LEFT JOIN	DealerCategories ON Dealers.ID = DealerCategories.DealerID
LEFT JOIN	Categories ON DealerCategories.CategoryID = Categories.ID	
LEFT JOIN	LoanStatuses on Loans.StatusID = LoanStatuses.[ID]
WHERE		Loans.LoanOwnerID IN (1,2,4,5,7)
--where Loans.ID <= 1378571


