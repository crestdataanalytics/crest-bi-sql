SET Transaction Isolation Level Read Uncommitted;

SELECT		ApplicantJob.ApplicantID,
			ApplicantJob.MonthlyIncome,
			CASE
				WHEN PayPeriodTypes.Period = 'Monthly' THEN 'MONTHLY'
				WHEN PayPeriodTypes.Period = 'Semi-Monthly' THEN 'SEMI_MONTHLY'
				WHEN PayPeriodTypes.Period = 'Bi-Weekly' THEN 'BI_WEEKLY'
				WHEN PayPeriodTypes.Period = 'Weekly' THEN 'WEEKLY'
				ELSE '' END AS PayPeriod,
			ApplicantJob.ID AS ApplicantJobID,
			ApplicantJob.LastPayDate,
			ApplicantJob.NextPayDate,
			ApplicantJob.HireDate
FROM		ApplicantJob
LEFT JOIN	PayPeriodTypes on ApplicantJob.PayPeriodTypeID = PayPeriodTypes.ID

--this checks the different type of pay periods we have
--SELECT count(PayPeriodTypes.ID), PayPeriodTypes.Period
--From PayPeriodTypes
--Group by PayPeriodTypes.Period