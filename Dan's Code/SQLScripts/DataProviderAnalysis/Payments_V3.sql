SET Transaction Isolation Level Read Uncommitted;

SELECT		LoanID,
			PaymentDate,
			ReturnDate,
			RentAmount,
			SalesTaxAmount,
			CASE WHEN 
				RentAmount >= 0 THEN RentAmount + FeeAmount END AS Amount,
			FeeAmount,
			BankAccountID,
			DeletedDate,
			LoanPaymentTypes.[Type],
			vw_LoanPayments.ID AS PaymentID
FROM		vw_LoanPayments
INNER JOIN	LoanPaymentTypes ON LoanPaymentTypes.ID = vw_LoanPayments.TypeID
INNER JOIN	vw_loans_reporting ON vw_LoanPayments.LoanID = vw_loans_reporting.ID
WHERE		vw_loans_reporting.LoanOwnerID IN (1,2,4,5,7)