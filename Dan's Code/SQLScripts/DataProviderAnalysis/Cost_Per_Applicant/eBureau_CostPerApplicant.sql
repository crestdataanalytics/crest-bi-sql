
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2015-01-01'
				AND DocumentInfos.ProviderID = 11
),

Inquiry AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS Inquiry
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (39780)
)


SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.GenerationDate,
			Inquiry.Inquiry
FROM		DocumentIDs
LEFT JOIN	Inquiry ON Inquiry.DocumentID = DocumentIDs.DocumentID


--ProviderID	ProviderName	VariableID	VariableName
--11	EBureau	39780	/root/transaction/status/uuid