
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2016-01-01'
				AND DocumentInfos.GenerationDate < '2017-01-01'
				AND DocumentInfos.ProviderID = 9
),

ChexAdvisor AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS ChexAdvisor
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (45402)
),

RiskLogic AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS RiskLogic
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (13765)
),

AccountLogicPlus AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS AccountLogicPlus
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (13043)
),

RiskAndCompliance AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS RiskAndCompliance
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (13238)
),

IDLogic AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS IDLogic
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (13053)
)


SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.GenerationDate,
			ChexAdvisor.ChexAdvisor,
			RiskLogic.RiskLogic,
			AccountLogicPlus.AccountLogicPlus,
			RiskAndCompliance.RiskAndCompliance,
			IDLogic.IDLogic
FROM		DocumentIDs
LEFT JOIN	ChexAdvisor ON ChexAdvisor.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	RiskLogic ON RiskLogic.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	AccountLogicPlus ON AccountLogicPlus.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	RiskAndCompliance ON RiskAndCompliance.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	IDLogic ON IDLogic.DocumentID = DocumentIDs.DocumentID


--ProviderID	ProviderName	VariableID	VariableName
--9	FactorTrust	13238	/ApplicationResponse/ApplicationInfo/ClientTransactionID | TOTAL INQUIRIES and Risk and compliance
--9	FactorTrust	45402	/ApplicationResponse/ApplicationInfo/ChexAdvisor/Privacy/MsgID | CHEXADVISOR
--9	FactorTrust	13765	/ApplicationResponse/ApplicationInfo/RiskLogic/Scores/Score[1]/Value | RISKLOGIC
--9	FactorTrust	13043	/ApplicationResponse/ApplicationInfo/DDAPlus/DDAPlusResponse | Account Logic +  
--9	FactorTrust	13053	/ApplicationResponse/ApplicationInfo/IDV/AddressisCurrent | IDLogic