
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2016-01-01'
				AND DocumentInfos.GenerationDate < '2017-01-01'
				AND DocumentInfos.ProviderID = 3
),

IdentityVerification AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS IdentityVerification
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (7476)
),

BAVNotXD00 AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS BAVNotXD00
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (6250)
				AND DocumentVariables.VariableValue != 'XD00'
),

BAVXD00 AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS BAVXD00
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (6250)
				AND DocumentVariables.VariableValue = 'XD00'
),

CreditReportTotal AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS CreditReportTotal
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (7448)
),

CreditReportNull AS
(
	SELECT		COUNT(DocumentIDs.DocumentID) AS TotalDocumentIDs,
				DocumentIDs.DocumentID,
				1 AS CreditReportNull
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (6489,7495,7826,7827,7828,7977,9531,7973,7853,7854,7885,7888)
				AND DocumentVariables.VariableValue = 'C100'
	GROUP BY	DocumentIDs.DocumentID
),

DebitReport AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS DebitReport
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (8107)
),

IDScoreAction AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS IDScoreAction
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (8021)
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.GenerationDate,
			DocumentIDs.ApplicantID,
			IdentityVerification.IdentityVerification,
			BAVNotXD00.BAVNotXD00,
			BAVXD00.BAVXD00,
			CreditReportTotal.CreditReportTotal,
			CreditReportNull.CreditReportNull,
			DebitReport.DebitReport,
			IDScoreAction.IDScoreAction
FROM		DocumentIDs
LEFT JOIN	IdentityVerification ON IdentityVerification.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	BAVNotXD00 ON BAVNotXD00.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	BAVXD00 ON BAVXD00.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	CreditReportTotal ON CreditReportTotal.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	CreditReportNull ON CreditReportNull.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	DebitReport ON DebitReport.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	IDScoreAction ON IDScoreAction.DocumentID = DocumentIDs.DocumentID


--ProviderID	ProviderName	VariableID	VariableName
--3	DataX	5020	/DataxResponse/TransactionId  -- Total Inquiries
--3	DataX	7476	/DataxResponse/Response/Detail/IDVSegment/Status --Identity Verification
--3	DataX	6250	/DataxResponse/Response/Detail/BAVSegment/Code -- Bank Account Verification
--CreditReportNull
	--3	DataX	6489	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[1]/Code
	--3	DataX	7495	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[2]/Code
	--3	DataX	7826	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[8]/Code
	--3	DataX	7827	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator/Code
	--3	DataX	7828	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[4]/Code
	--3	DataX	7977	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[9]/Code
	--3	DataX	9531	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[11]/Code
	--3	DataX	7973	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[10]/Code
	--3	DataX	7853	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[5]/Code
	--3	DataX	7854	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[7]/Code
	--3	DataX	7885	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[3]/Code
	--3	DataX	7888	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/Report/ConsumerSummarySegment/Indicators/IndicatorSummary/Indicator[6]/Code
--3	DataX	7448	/DataxResponse/Response/Detail/CRAInquirySegment/CRAResponse/TransactionID -- total CreditReport
--3	DataX	8107	/DataxResponse/Response/Detail/DebitReportSegment/Score/Value  -- DebitReport
--3	DataX	8021	/DataxResponse/Response/Detail/IDAIDSASegment/ScoreAction/Status  -- IDScoreAction

