	SET Transaction Isolation Level Read Uncommitted;
	
	SELECT		CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2014-01-01'
				AND DocumentInfos.GenerationDate < '2017-01-01'