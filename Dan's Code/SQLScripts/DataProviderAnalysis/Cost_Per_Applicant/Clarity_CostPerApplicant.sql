--need to account for clear bank and clear bank behavior
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2016-01-01'
				AND DocumentInfos.GenerationDate < '2017-01-01'
				AND DocumentInfos.ProviderID = 7
),

ClearFraud AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS ClearFraud
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (9697)	
),

ClearBank AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS ClearBank
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (103)	
),


ClearBankBehavior AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS ClearBankBehavior
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (44727)	
),

ClearTradeline AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS ClearTradeline
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (9791)	
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.GenerationDate,
			ClearFraud.ClearFraud,
			ClearBank.ClearBank,
			ClearBankBehavior.ClearBankBehavior,
			ClearTradeline.ClearTradeline
FROM		DocumentIDs
LEFT JOIN	ClearFraud ON ClearFraud.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearBankBehavior ON ClearBankBehavior.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearTradeline ON ClearTradeline.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearBank ON ClearBank.DocumentID = DocumentIDs.DocumentID



--ProviderID	ProviderName	VariableID	VariableName
--7	Clarity	1811	/xml-response/tracking-number --tracking number
--7	Clarity	9697	/xml-response/clear-fraud/action --Clear-Fraud
--7	Clarity	44727	/xml-response/clear-bank-behavior/action --clear bank behavior
--7	Clarity	39485	/xml-response/inquiry/product-date --product date
--7	Clarity	9791	/xml-response/clear-tradeline/action  -- clear tradeline
--7	Clarity	103	/xml-response/clear-bank/action -- clear bank

