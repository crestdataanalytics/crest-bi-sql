
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2016-01-01'
				AND DocumentInfos.GenerationDate < '2017-01-01'
				AND DocumentInfos.ProviderID = 8
),

BAV AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS BAV
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (12010)	
),

FraudVerify AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS FraudVerify
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (11965)	
),

iPredict AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS iPredict
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (12027)	
)


SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.GenerationDate,
			BAV.BAV,
			FraudVerify.FraudVerify,
			iPredict.iPredict
FROM		DocumentIDs
LEFT JOIN	BAV ON BAV.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	FraudVerify ON FraudVerify.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	iPredict ON iPredict.DocumentID = DocumentIDs.DocumentID


--ProviderID	ProviderName	VariableID	VariableName
--8	MicroBuilt	12027	/MBCLVRs_Type/MsgRsHdr/RqUID | total inquiries and iPredict
--8	MicroBuilt	12010	/MBCLVRs_Type/RESPONSE/CONTENT/SERVICEDETAILS/BANK-INFORMATION/BANKACCOUNTSUMMARY/score | BAV
--8	MicroBuilt	11965	/MBCLVRs_Type/RESPONSE/CONTENT/SERVICEDETAILS/IDV/score | Fraud Verify