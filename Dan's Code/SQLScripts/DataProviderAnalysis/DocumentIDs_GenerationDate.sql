SET Transaction Isolation Level Read Uncommitted;

SELECT		DocumentInfos.DocumentID,
			DocumentInfos.ProviderID,
			Providers.ProviderName,
			DocumentInfos.SubjectID AS ApplicantID,
			DocumentInfos.GenerationDate
FROM		DocumentInfos
INNER JOIN	Providers ON Providers.ProviderID = DocumentInfos.ProviderID