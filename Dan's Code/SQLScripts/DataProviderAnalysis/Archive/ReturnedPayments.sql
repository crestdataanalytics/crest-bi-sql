SET Transaction Isolation Level Read Uncommitted;

SELECT		ISNULL(LoanPayments.SystemCode, '(unknown)') AS ProcessorName,
			CAST(LoanPayments.PaymentDate AS DATE) AS PaymentDate,
			LoanPayments.Returned,	
			CASE WHEN LoanPayments.Returned = 0 THEN '' ELSE ISNULL(WellsFargoReturns.ReturnCode, '') END AS ReturnCode
			--COUNT(LoanPayments.ID) AS PaymentCount,
			--SUM(LoanPayments.Amount) AS TotalAmount	
FROM		LoanPayments	
LEFT JOIN	WellsFargoReturns ON LoanPayments.ID = WellsFargoReturns.PaymentID
WHERE		LoanPayments.IsDeleted = 0
			AND LoanPayments.TypeID NOT IN (1,3)
			AND LoanPayments.SystemCode IN ('ACHWORKS','GOTOBILLING')
--GROUP BY	SystemCode,
--			CAST(PaymentDate AS DATE),
--			Returned,	
--			CASE WHEN Returned = 0 THEN '' ELSE ISNULL(ReturnCode, '') END
ORDER BY	PaymentDate DESC

