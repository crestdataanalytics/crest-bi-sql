
DECLARE @startdate date,
		@enddate date
SET @startdate = '2015-01-01';
SET @enddate = '2016-01-01';
WITH LastYearTotal AS
(
	SELECT		COUNT(*) AS LastYearTotal,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		CreationDate >= @startdate
				AND CreationDate < @enddate
				AND FundedDate IS NOT NULL
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
),

LastYearNDBO AS
(
	SELECT		COUNT(*) AS LastYearNDBO,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		CreationDate >= @startdate
				AND CreationDate < @enddate
				AND FundedDate IS NOT NULL
				AND PaidOffDate <= NinetyDaysEndDate
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
				AND PaidOffDate IS NOT NULL
				AND BuyOutType = 90
),

LastYear4000Total AS
(
	SELECT		COUNT(*) AS LastYear4000Total,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		CreationDate >= @startdate
				AND CreationDate < @enddate
				AND FundedDate IS NOT NULL
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
				AND ApprovalAmount >= 4000
),

LastYear4000NDBO AS
(
	SELECT		COUNT(*) AS LastYear4000NDBO,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		CreationDate >= @startdate
				AND CreationDate < @enddate
				AND FundedDate IS NOT NULL
				AND PaidOffDate <= NinetyDaysEndDate
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
				AND PaidOffDate IS NOT NULL
				AND BuyOutType = 90
				AND ApprovalAmount >= 4000
),

TotalTotal AS
(
	SELECT		COUNT(*) AS Total,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		FundedDate IS NOT NULL
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
),

TotalNDBO AS
(
	SELECT		COUNT(*) AS TotalNDBO,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		FundedDate IS NOT NULL
				AND PaidOffDate <= NinetyDaysEndDate
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
				AND PaidOffDate IS NOT NULL
				AND BuyOutType = 90
),

Total4000Total AS
(
	SELECT		COUNT(*) AS Total4000Total,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		FundedDate IS NOT NULL
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
				AND ApprovalAmount >= 4000
),

Total4000NDBO AS
(
	SELECT		COUNT(*) AS Total4000NDBO,
				'LastYear' AS LastYear
	FROM		Loans
	INNER JOIN	LoanStatuses ON LoanStatuses.ID = Loans.StatusID
	WHERE		FundedDate IS NOT NULL
				AND LoanStatuses.[Status] NOT IN ('Migrated','DealerReturn','Cancelled')
				AND PaidOffDate IS NOT NULL
				AND PaidOffDate <= NinetyDaysEndDate
				AND BuyOutType = 90
				AND ApprovalAmount >= 4000
)

SELECT		LastYearTotal.LastYearTotal,
			LastYearNDBO.LastYearNDBO,
			CAST(LastYearNDBO.LastYearNDBO AS decimal) / LastYearTotal.LastYearTotal * 100 AS NDBO_Total_Percent,
			LastYear4000Total.LastYear4000Total,
			LastYear4000NDBO.LastYear4000NDBO,
			CAST(LastYear4000NDBO.LastYear4000NDBO AS decimal) / LastYear4000Total.LastYear4000Total * 100 AS NDBO_4000_Percent,
			TotalTotal.Total,
			TotalNDBO.TotalNDBO,
			CAST(TotalNDBO.TotalNDBO AS decimal) / TotalTotal.Total * 100 AS NDBO_TotalPortfolio_Percent,
			Total4000Total.Total4000Total,
			Total4000NDBO.Total4000NDBO,
			CAST(Total4000NDBO.Total4000NDBO AS decimal) / Total4000Total.Total4000Total * 100 AS NDBO_4000Portfolio_Percent
FROM		LastYearTotal
INNER JOIN	LastYearNDBO ON LastYearTotal.LastYear = LastYearNDBO.LastYear
INNER JOIN	LastYear4000Total ON LastYear4000Total.LastYear = LastYearNDBO.LastYear
INNER JOIN	LastYear4000NDBO ON LastYear4000NDBO.LastYear = LastYearNDBO.LastYear
INNER JOIN	TotalTotal ON TotalTotal.LastYear = LastYearTotal.LastYear
INNER JOIN	TotalNDBO ON TotalNDBO.LastYear = LastYearTotal.LastYear
INNER JOIN	Total4000Total ON Total4000Total.LastYear = LastYearTotal.LastYear
INNER JOIN	Total4000NDBO ON Total4000NDBO.LastYear = LastYearTotal.LastYear
