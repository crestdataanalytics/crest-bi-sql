select distinct AutoApprovalProcessResponses.ApplicantID, 
		vw_loans_reporting.DisplayID,
		case when ClarityResponse = 1 then 'Approved'
		when ClarityResponse = 2 then 'Denied'
		end as ClarityResponse,
		ClarityResponseDescription,
		LoanStatuses.[Status],
		cast(ClarityRequestDate as date) ClarityRequestDate,
		cast(vw_loans_reporting.ApprovalDate as date) ApprovalDate,
		vw_loans_reporting.AutoApprovedFlag,
		cast(vw_loans_reporting.FundedDate as date) FundedDate,
		cast(vw_loans_reporting.ChargedOffDate as date) ChargedOffDate,
		cast(vw_loans_reporting.PaidOffDate as date) PaidOffDate,
		cast(AmountPaid.FirstPaymentDate AS DATE) FirstPaymentDate,
		cast(FirstReturnedPayment.EffectiveDate AS DATE) FirstReturnedPaymentDate,
		ReturnedPayments.ReturnCode
from AutoApprovalProcessResponses
inner join vw_loans_reporting on vw_loans_reporting.ApplicantID = AutoApprovalProcessResponses.ApplicantID
inner join LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
LEFT JOIN	(SELECT		LoanPayments.LoanID, sum(LoanPayments.Amount) AS TotalAmountPaid, MIN(PaymentDate) AS FirstPaymentDate, 
						MIN(LoanPayments.ID) as PaymentID
			FROM		LoanPayments
			INNER JOIN	LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
			WHERE		PaymentDate is not null and IsDeleted = 0
						AND LoanPaymentTypes.[Type] NOT IN ('Cash','Initial Payment', 'Paid By Dealer','Refund')
						AND LoanPayments.Returned = 0
			GROUP BY	LoanID
			)			AmountPaid on vw_loans_reporting.ID = AmountPaid.LoanID
LEFT JOIN	
			(SELECT		MIN(PaymentID) AS PaymentID, MIN(EffectiveDate) AS EffectiveDate, LoanID
			FROM		WellsFargoReturns
			GROUP BY	LoanID
			)			FirstReturnedPayment ON FirstReturnedPayment.LoanID = vw_loans_reporting.ID
LEFT JOIN	
			(SELECT		PaymentID, EffectiveDate, LoanID, ReturnCode,
						case when ReturnCode IN ('R02','R05', 'R07', 'R08', 'R10', 'R16') then 1
						else 0 end as FirstPaymentFatalReturnIndicator,
						1 as IsReturned
			FROM		WellsFargoReturns	
			)			ReturnedPayments on ReturnedPayments.PaymentID = FirstReturnedPayment.PaymentID
where	AutoApprovalProcessResponses.ClarityResponse is not null
		and AutoApprovalProcessResponses.clarityrequestdate is not null
		--and ClarityResponseDescription like '%Primary Account Has No Information%'
		and LoanStatuses.[Status] != 'Cancelled'
order by ClarityRequestDate asc
