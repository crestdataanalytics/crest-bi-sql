
--Dean says that we should include all of the data fields that DataX asked for and just put a blank for what we don't have.
--we need to make sure that the address and phone number that are being used are the ones registered at time of application
SET Transaction Isolation Level Read Uncommitted;
Declare @startDate date,
		@endDate date
SET @startDate = '01/01/2015';
SET	@endDate = '12/31/2015';
	

SELECT	distinct cast(ApprovalProviderResponseLog.CreatedDate as date) as InquiryDate, 
		Dealers.ID as RetailerID,
		--vw_loans_reporting.PaidOffDate, --shouldn't be in the actual data we send
		replace(vw_loans_reporting.DisplayID, '-','') as ID, 
		case	
			when ApprovalProviderResponseLog.TrackingID IS NOT NULL  
				then cast(ApprovalProviderResponseLog.TrackingID as varchar) 
			else '' end AS TransactionID, 
		vw_loans_reporting.ApplicantID,
		Applicants.SocialSecurityNumber as SSN, Applicants.FirstName as NameFirst, Applicants.MiddleInitial as NameMiddle,
		Applicants.LastName as NameLast,
		replace(Addresses.StreetLine1, ',', '') as Street1, 
		replace(Addresses.StreetLine2, ',', '') as Street2,
		Addresses.City, Addresses.StateID as [State], Addresses.PostalCode as Zip, Applicants.HomePhone as PhoneHome, 
		Applicants.CellPhone as PhoneCell, ApplicantJob.PhoneNumber as PhoneWork, ApplicantEmailAddresses.EmailAddress as Email, 
		cast(Applicants.DateOfBirth as date) as DOB,
		'' as IPAddress, Applicants.DriversLicenseNumber as DriverLicenseNumber, Applicants.DriversLicenseStateID as DriverLicenseState, 
		ApplicantJob.EmployerName as WorkName, 
		replace(replace(WorkAddresses.StreetLine1, ',', ''), '*','') as WorkStreet1, 
		replace(replace(WorkAddresses.StreetLine2, ',', ''), '*','') as WorkStreet2, 
		WorkAddresses.City as WorkCity, 
		WorkAddresses.StateID as WorkState, 
		WorkAddresses.PostalCode as WorkZip,
		cast(ApplicantJob.HireDate as date) as HireDate, '' as BankName,
		BankAccounts.RoutingNumber as BankABA, BankAccounts.AccountNumber as BankAcctNumber, 
		case
			when PayPeriodTypes.Period = 'Monthly' then 'MONTHLY'
			when PayPeriodTypes.Period = 'Semi-Monthly' then 'SEMI_MONTHLY'
			when PayPeriodTypes.Period = 'Bi-Weekly' then 'BI_WEEKLY'
			when PayPeriodTypes.Period = 'Weekly' then 'WEEKLY'
			else '' end as PayPeriod,
		ApplicantJob.MonthlyIncome,
		LoanTerms.Months,
		case 
			when vw_loans_reporting.ApprovalDate IS NOT NULL then 1
			else 0 end as IsApproved,
		case 
			when vw_loans_reporting.ApprovalDate IS NULL then 1
			else 0 end as IsDenied,
		case 
			when vw_loans_reporting.AutoApprovedFlag = 1 then 1 
			when vw_loans_reporting.AutoApprovedFlag = 0 then 0
		else 0 end as IsAutoApproved,
		'' as LeadCampaignName, AmountPaid.TotalAmountPaid as CumulativeAmountCollected, '' as LeadPurchasedIndicator,
		'' as LeadProviderName, '' as LeadPrice, '' as RequestedLoanAmount, 
		'' as FundingApprovedIndicator, --DataX says this has to do with a lead that was purchased
		case 
			when	vw_loans_reporting.FundedDate is not null and LoanStatuses.[Status] != 'Canceled' then 1
			else 0 end as FundedIndicator,
		case when vw_loans_reporting.Amount = 0 then 0 else vw_loans_reporting.FundedAmount end as MerchantPaidAmount,
		Categories.CategoryName as CollateralType,
		vw_loans_reporting.Amount as FundedLoanAmount, cast(LoanPaymentFrequency.FirstDueDate as date) as FirstDueDate,
		'' as SecondDueDate, '' as ThirdDueDate,
		case
			when	TotalLeases.TotalLeases > 1 then 1
			else 0 end as ReturningCustomerIndicator,
		case
			when	TotalLeases.TotalLeases >= 1 then TotalLeases.TotalLeases
			else 0 end as NumberOfLoansWithLenders, 
		case
			when ReturnedPayments.IsReturned = 1 THEN 1
			ELSE 0 END AS FirstPaymentDefault,
		case
			when ReturnedPayments.FirstPaymentFatalReturnIndicator = 1 THEN 1
			ELSE 0 END AS FirstPaymentFatalReturnIndicator,
		case
			when datediff(day, LoanPaymentFrequency.FirstDueDate, AmountPaid.FirstPaymentDate) >= 90 then 1
			when datediff(day, LoanPaymentFrequency.FirstDueDate, AmountPaid.FirstPaymentDate) < 90 then 0
			end as NinetyDayDelinquency,
		'' as SecondPaymentDefault, '' as ThirdPaymentDefault,
		case
			when vw_loans_reporting.ChargedOffDate IS NOT NULL and LoanStatuses.[Status] = 'Charged Off' then 1
			ELSE 0 END AS ChargeOffIndicator,
		case
			when vw_loans_reporting.ChargedOffDate IS NOT NULL and LoanStatuses.[Status] = 'Charged Off' then vw_loans_reporting.ChargedOffAmount
			END AS ChargeOffAmount,
		case
			when LoanStatuses.[Status] = 'Bounced' AND vw_loans_reporting.NumDaysLate > 0 AND vw_loans_reporting.NumDaysLate < 121 then 1
			else 0 end as CollectionIndicator,
		'' as RecoveryIndicator,
		'' as AmountRecovered
FROM		(SELECT		ApplicantID, vw_loans_reporting.ID, FundedDate, ChargedOffDate, Amount, NumDaysLate, ChargedOffAmount,
						CreationDate, StatusID, DisplayID, PaidOffDate, DealerID, FundedAmount, LoanTermID, AutoApprovedFlag, ApprovalDate
			FROM		vw_loans_reporting					
			INNER JOIN	LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
			WHERE		((vw_loans_reporting.CreationDate >= @startDate and vw_loans_reporting.CreationDate <= @endDate) 
						OR (vw_loans_reporting.UpdatedDate >= @startDate and vw_loans_reporting.UpdatedDate <= @endDate))
			) vw_loans_reporting
LEFT JOIN	LoanTerms on LoanTerms.ID = vw_loans_reporting.LoanTermID
INNER JOIN	Dealers on vw_loans_reporting.DealerID = Dealers.ID
INNER JOIN	(SELECT		ApplicantID, CreatedDate, TrackingID, InquiryID, Decision
			FROM		[CrestWareHouse].[dbo].[ApprovalProviderResponseLog]
			WHERE		ApprovalProviderSettingsID = 1 
						AND Decision IS NOT NULL
						AND TrackingID IS NOT NULL
						AND ApprovalProviderResponseLog.CreatedDate >= @startDate
						AND ApprovalProviderResponseLog.CreatedDate <= @endDate) 
			AS ApprovalProviderResponseLog on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
INNER JOIN	(select count(DealerID) as CountDealerID, DealerID, max(categoryID) as CategoryID 
			from DealerCategories
			group by DealerID) as DealerCategories on Dealers.ID = DealerCategories.DealerID
LEFT JOIN Categories on DealerCategories.CategoryID = Categories.ID	
LEFT JOIN	(SELECT		ApplicantID, COUNT(ApplicantID) as TotalLeases
			FROM		vw_loans_reporting
			INNER JOIN	LoanStatuses on vw_loans_reporting.StatusID = LoanStatuses.ID
			WHERE		FundedDate IS NOT NULL
						AND LoanStatuses.[Status] != 'Canceled'
			GROUP BY	ApplicantID
			)	as TotalLeases on vw_loans_reporting.ApplicantID = TotalLeases.ApplicantID	
LEFT JOIN	Applicants on vw_loans_reporting.ApplicantID = Applicants.ID
LEFT JOIN	(SELECT		min(ID) as ID, count(ApplicantID) as CountApplicantID, ApplicantID
			FROM		ApplicantPhone
			GROUP BY	ApplicantID
			)			as PhoneNumber on PhoneNumber.ApplicantID = vw_loans_reporting.ApplicantID
LEFT JOIN	ApplicantPhone on ApplicantPhone.ID = PhoneNumber.ID
LEFT JOIN	(SELECT		ApplicantID, count(ApplicantID) as CountApplicantID, min(AddressID) as AddressID
			FROM		ApplicantAddresses 
			GROUP BY	ApplicantID
			) as ApplicantAddresses on vw_loans_reporting.ApplicantID = ApplicantAddresses.ApplicantID
LEFT JOIN	Addresses on Addresses.ID = ApplicantAddresses.AddressID
LEFT JOIN	(SELECT		min(ID) as ID, ApplicantID, count(ApplicantID) as CountApplicantID
			FROM		ApplicantJob
			GROUP BY	ApplicantID
			) as FirstApplicantJob on FirstApplicantJob.ApplicantID = vw_loans_reporting.ApplicantID
LEFT JOIN	(SELECT		ID, AddressID, ApplicantID, EmployerName, JobTitle, HireDate, ShiftInformation, MonthlyIncome, SupervisorName,
						PhoneNumber, IsPrimary, PayPeriodTypeID, IncomeTypeID, LastPayDate, NextPayDate 
			FROM		ApplicantJob
			) as ApplicantJob on FirstApplicantJob.ID = ApplicantJob.ID 
LEFT JOIN	(SELECT		ID, StreetLine1, StreetLine2, City, StateID, PostalCode
			FROM		Addresses
			) as WorkAddresses on WorkAddresses.ID = Addresses.ID
LEFT JOIN	LoanStatuses on vw_loans_reporting.StatusID = LoanStatuses.[ID]
LEFT JOIN	PayPeriodTypes on ApplicantJob.PayPeriodTypeID = PayPeriodTypes.ID
LEFT JOIN	(SELECT		ApplicantID, count(ApplicantID) as CountApplicantID, min(BankAccountID) as BankAccountID
			FROM		ApplicantBankAccounts
			GROUP BY	ApplicantID
			) as ApplicantBankAccounts on vw_loans_reporting.ApplicantID = ApplicantBankAccounts.ApplicantID
LEFT JOIN	(SELECT		ID, BankName, AccountNumber, RoutingNumber, OpenDate, IsPrimaryAccount, IsClosed, AccountType, NotAuthorized, IsDeleted
			FROM		BankAccounts
			WHERE		AccountNumber IS NOT NULL and RoutingNumber IS NOT NULL)
			BankAccounts on ApplicantBankAccounts.BankAccountID = BankAccounts.ID 
LEFT JOIN	(SELECT		LoanPayments.LoanID, sum(LoanPayments.Amount) AS TotalAmountPaid, MIN(PaymentDate) AS FirstPaymentDate, 
						MIN(LoanPayments.ID) as PaymentID
			FROM		LoanPayments
			INNER JOIN	LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
			WHERE		PaymentDate is not null and IsDeleted = 0
					AND LoanPaymentTypes.[Type] NOT IN ('Cash','Initial Payment', 'Paid By Dealer','Refund')
			GROUP BY	LoanID) AmountPaid on vw_loans_reporting.ID = AmountPaid.LoanID
LEFT JOIN 
			(SELECT		LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
			FROM		LoanPaymentSchedule
			WHERE		ScheduleEntryDesc != 'Initial Payment'
			GROUP BY	LoanID) LoanPaymentFrequency on vw_loans_reporting.ID = LoanPaymentFrequency.LoanID			
LEFT JOIN	
			(SELECT		PaymentID, processeddate,
						case when ReturnCode IN ('R02','R05', 'R07', 'R08', 'R10', 'R16') then 1
						else 0 end as FirstPaymentFatalReturnIndicator,
						1 as IsReturned
			FROM		WellsFargoReturns	
			) ReturnedPayments on ReturnedPayments.PaymentID = AmountPaid.PaymentID
LEFT JOIN	(SELECT		min(ID) as ID, ApplicantID, count(ApplicantID) as CountApplicantID
			FROM		ApplicantEmailAddresses
			GROUP BY	ApplicantID
			) as ApplicantEmailAddressesID on vw_loans_reporting.ApplicantID = ApplicantEmailAddressesID.ApplicantID
LEFT JOIN	ApplicantEmailAddresses on ApplicantEmailAddressesID.ID = ApplicantEmailAddresses.ID
WHERE	ApprovalProviderResponseLog.CreatedDate is not null
		AND Applicants.IsDeleted = 0
		--and LoanStatuses.status != 'Cancelled'
		--and loanstatuses.status = 'Denied'
		--and ApprovalProviderResponseLog.applicantid = 541779
		--and  vw_loans_reporting.ApplicantID = 527871
		--or vw_loans_reporting.CoApplicantID = 541779

--this checks how many loans should be in the results

--Declare @startDate date,
--		@endDate date
--SET @startDate = '02/01/2013'
--SET	@endDate = '12/31/2013'
--select distinct applicantid
--from [CrestWareHouse].[dbo].[ApprovalProviderResponseLog]
--WHERE		ApprovalProviderSettingsID = 2 
--			AND Decision IS NOT NULL 
--			and TrackingID is not null
--			AND CreatedDate >= @startDate
--			AND CreatedDate <= @endDate
----and applicantid = 554230








--select --count(distinct applicantid) asdf
--distinct loans.applicantid
--from loans
--inner join [CrestWareHouse].[dbo].[ApprovalProviderResponseLog] on ApprovalProviderResponseLog.ApplicantID = loans.applicantID
--where year(ApprovalProviderResponseLog.CreatedDate) = 2013 and month(ApprovalProviderResponseLog.CreatedDate) = 3
--and loans.statusid not in (11) 
--and ApprovalProviderResponseLog.Decision is not null



----gives me a glance to look at dates on the other tables.
--select * 
--from [CrestWareHouse].[dbo].[ApprovalProviderResponseLog] 
----inner join 
----			(select applicantID
----			from loans
----			where coapplicantID is null)
----			Loans on [CrestWareHouse].[dbo].[ApprovalProviderResponseLog].ApplicantID = loans.ApplicantID
--where [CrestWareHouse].[dbo].[ApprovalProviderResponseLog].applicantID = 550527
--and approvalprovidersettingsid = 2

--select * from vw_loans_reporting where applicantid = 550527
--select * from vw_loans_reporting where coapplicantid = 550527
--select * from Applicants where id = 550527