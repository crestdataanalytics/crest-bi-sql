SET Transaction Isolation Level Read Uncommitted;

SELECT		--ApplicantID, CreatedDate
			COUNT (ApplicantID) as Total,  
			Decision, 
			SmartSettings
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 1
			AND CreatedDate >= '10/02/2015' -- the time we changed generic to 100%
			AND CreatedDate < '10/03/2015' 
			--AND cast(CreatedDate as date) = cast(getdate() as date)
GROUP BY	Decision,SmartSettings