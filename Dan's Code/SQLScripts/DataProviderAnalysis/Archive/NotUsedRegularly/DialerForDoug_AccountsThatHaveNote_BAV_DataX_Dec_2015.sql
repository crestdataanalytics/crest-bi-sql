SET Transaction Isolation Level Read Uncommitted;	
	
	
	SELECT		Notes.LoanID,
				Loans.DisplayID
	FROM		Notes
	INNER JOIN	Loans ON Notes.LoanID = Loans.ID
	WHERE		Notes.CreationDate >= '2016-01-11 1:10:00'
				AND Notes.NoteContent like 'VERIFY AND CORRECT ALL INFORMATION on this customers account when customer calls in!'