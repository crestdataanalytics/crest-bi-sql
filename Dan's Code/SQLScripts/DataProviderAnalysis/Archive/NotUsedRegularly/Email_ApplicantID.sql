SET Transaction Isolation Level Read Uncommitted;


SELECT		ID AS EmailID,
			ApplicantID,
			EmailAddress,
			IsPrimary,
			Invalid,
			StrikeIronVerified
FROM		ApplicantEmailAddresses