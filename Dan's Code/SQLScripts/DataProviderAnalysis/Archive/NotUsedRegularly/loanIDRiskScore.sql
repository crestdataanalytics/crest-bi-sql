SET Transaction Isolation Level Read Uncommitted;

SELECT		ROW_NUMBER() OVER (Partition by vw_loans_reporting.ID ORDER BY ApprovalProviderResponseLog.CreatedDate DESC) AS RowNumber,
			vw_loans_reporting.ID,
			vw_loans_reporting.DealerID,
			CAST(ApprovalProviderResponseLog.CreatedDate AS DATE) CreatedDate,
			ApprovalProviderResponseLog.RiskScore
FROM		vw_loans_reporting
INNER JOIN	LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
INNER JOIN	Dealers on Dealers.ID = vw_loans_reporting.DealerID
LEFT JOIN	(SELECT		ApplicantID, CreatedDate, TrackingID, InquiryID, Decision, RiskScore
			FROM		[CrestWareHouse].[dbo].[ApprovalProviderResponseLog]
			WHERE		ApprovalProviderSettingsID = 6
						AND Decision IS NOT NULL
						AND ApprovalProviderResponseLog.CreatedDate >= '02/10/2015'
						AND ApprovalProviderResponseLog.CreatedDate < getdate()) 
			AS ApprovalProviderResponseLog on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
WHERE		((vw_loans_reporting.CreationDate >= '02/10/2015' and vw_loans_reporting.CreationDate < getdate()) 
			OR (vw_loans_reporting.UpdatedDate >= '02/10/2015' and vw_loans_reporting.UpdatedDate < getdate()))
			AND ApprovalProviderResponseLog.RiskScore IS NOT NULL
			AND vw_loans_reporting.FundedDate IS NOT NULL
			AND LoanStatuses.[Status] IN ('Bankrupt13','Bankrupt7','Bounced','Charged Off','Funded','Paid','Returned')