SET Transaction Isolation Level Read Uncommitted;

WITH TotalSubmits AS
(
	SELECT		CAST(Loans.CreationDate AS DATE) AS SumbitDate, COUNT(*) AS TotalSubmits
	FROM		Loans
	INNER JOIN	Dealers ON Loans.DealerID = Dealers.ID
	WHERE		Dealers.IsDemoAccount = 0
	GROUP BY	CAST(Loans.CreationDate AS DATE)
),

AutoApproved AS
(
	SELECT		CAST(Loans.CreationDate AS DATE) AS SumbitDate, COUNT(*) AS AutoApproved
	FROM		Loans
	INNER JOIN	Dealers ON Loans.DealerID = Dealers.ID
	WHERE		Dealers.IsDemoAccount = 0
				AND Loans.AutoApprovedFlag = 1
				AND Loans.ApprovalDate IS NOT NULL
	GROUP BY	CAST(Loans.CreationDate AS DATE)
),

ManuallyApproved AS
(
	SELECT		CAST(Loans.CreationDate AS DATE) AS SumbitDate, COUNT(*) AS ManuallyApproved
	FROM		Loans
	INNER JOIN	Dealers ON Loans.DealerID = Dealers.ID
	WHERE		Dealers.IsDemoAccount = 0
				AND (Loans.AutoApprovedFlag = 0 OR Loans.AutoApprovedFlag IS NULL)
				AND Loans.ApprovalDate IS NOT NULL
	GROUP BY	CAST(Loans.CreationDate AS DATE)
),

Pending AS
(
	SELECT		CAST(Loans.CreationDate AS DATE) AS SumbitDate, COUNT(*) AS Pending
	FROM		Loans
	INNER JOIN	Dealers ON Loans.DealerID = Dealers.ID
	WHERE		Dealers.IsDemoAccount = 0
				AND Loans.AutoApprovedFlag = 0 
				AND Loans.ApprovalDate IS NULL
				AND Loans.DeniedDate IS NULL
	GROUP BY	CAST(Loans.CreationDate AS DATE)
),

Denied AS
(
	SELECT		CAST(Loans.CreationDate AS DATE) AS SumbitDate, COUNT(*) AS Denied
	FROM		Loans
	INNER JOIN	Dealers ON Loans.DealerID = Dealers.ID
	WHERE		Dealers.IsDemoAccount = 0
				AND Loans.DeniedDate IS NOT NULL
	GROUP BY	CAST(Loans.CreationDate AS DATE)
)

SELECT		TotalSubmits.SumbitDate,
			TotalSubmits.TotalSubmits,
			AutoApproved.AutoApproved,
			CAST(AutoApproved.AutoApproved AS decimal) / TotalSubmits.TotalSubmits * 100 AS [AutoApproved%],
			ManuallyApproved.ManuallyApproved,
			CAST(ManuallyApproved.ManuallyApproved AS decimal) / TotalSubmits.TotalSubmits * 100 AS [ManuallyApproved%],
			Pending.Pending,
			CAST(Pending.Pending AS decimal) / TotalSubmits.TotalSubmits * 100 AS [Pending%],
			Denied.Denied,
			CAST(Denied.Denied AS decimal) / TotalSubmits.TotalSubmits * 100 AS [Denied%]
FROM		TotalSubmits
LEFT JOIN	AutoApproved ON AutoApproved.SumbitDate = TotalSubmits.SumbitDate
LEFT JOIN	ManuallyApproved ON ManuallyApproved.SumbitDate = TotalSubmits.SumbitDate
LEFT JOIN	Pending ON Pending.SumbitDate = TotalSubmits.SumbitDate
LEFT JOIN	Denied ON Denied.SumbitDate = TotalSubmits.SumbitDate
WHERE		TotalSubmits.SumbitDate >= '2015-11-01'
ORDER BY	1 ASC