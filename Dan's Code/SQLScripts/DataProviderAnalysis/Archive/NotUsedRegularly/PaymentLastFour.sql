SET Transaction Isolation Level Read Uncommitted;

SELECT		LoanPayments.LoanID,
			LoanPayments.ID AS PaymentID,
			LoanPayments.PaymentMethodLastFour
FROM		LoanPayments
