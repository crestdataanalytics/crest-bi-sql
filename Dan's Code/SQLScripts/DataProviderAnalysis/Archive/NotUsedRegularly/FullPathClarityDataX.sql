
SET Transaction Isolation Level Read Uncommitted;

SELECT			DISTINCT --AutoApprovalProcessResponses.ApplicantID,
				AutoApprovalProcessResponses.ClarityTracking,
				ClarityResponse,
				--CAST(AutoApprovalProcessResponses.ClarityRequestDate AS DATE) InquiryDate,
				--AutoApprovalProcessResponses.DataXTransactionID,
				DataXResponse
				--count(ApplicantID)
  FROM			AutoApprovalProcessResponses
  INNER JOIN	Loans ON Loans.ApplicantID = AutoApprovalProcessResponses.ApplicantID
  INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
  WHERE			AutoApprovalProcessResponses.ApprovalPath = 'Full'
				AND AutoApprovalProcessResponses.ClarityRequestDate >= '01-01-2015'
				AND AutoApprovalProcessResponses.ClarityTracking IS NOT NULL
				AND AutoApprovalProcessResponses.DataXTransactionID IS NOT NULL
				AND Dealers.IsDemoAccount = 0
				AND CAST(AutoApprovalProcessResponses.ClarityRequestDate AS DATE) = CAST(AutoApprovalProcessResponses.DataXRequestDate AS DATE)
				AND ClarityResponse IN (1,2,4,5)
				AND DataXResponse IN (1,2,4,5)

SELECT			count(DISTINCT AutoApprovalProcessResponses.ApplicantID)
  FROM			AutoApprovalProcessResponses
  INNER JOIN	Loans ON Loans.ApplicantID = AutoApprovalProcessResponses.ApplicantID
  INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
  WHERE			AutoApprovalProcessResponses.ApprovalPath = 'Full'
				AND AutoApprovalProcessResponses.ClarityRequestDate >= '01-01-2015'
				AND AutoApprovalProcessResponses.ClarityTracking IS NOT NULL
				AND AutoApprovalProcessResponses.DataXTransactionID IS NOT NULL
				AND Dealers.IsDemoAccount = 0
				AND CAST(AutoApprovalProcessResponses.ClarityRequestDate AS DATE) = CAST(AutoApprovalProcessResponses.DataXRequestDate AS DATE)
				AND ClarityResponse IN (1,2,4,5)
				AND DataXResponse IN (1,2,4,5)
				--AUDITING
				AND AutoApprovalProcessResponses.ClarityRequestDate < '2015-12-11 07:18:29' --the time stamp i pulled my data
				--AND ClarityResponse = 1
				--AND ClarityResponse IN (2,4,5)
				--AND DataXResponse = 1
				--AND DataXResponse IN (2,4,5)

--THIS AUDITS COUNTS FOR A GIVEN DATE
SELECT			count(DISTINCT AutoApprovalProcessResponses.ApplicantID)
  FROM			AutoApprovalProcessResponses
  INNER JOIN	Loans ON Loans.ApplicantID = AutoApprovalProcessResponses.ApplicantID
  INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
  WHERE			AutoApprovalProcessResponses.ApprovalPath = 'Full'
				AND AutoApprovalProcessResponses.ClarityRequestDate >= '01-01-2015'
				AND AutoApprovalProcessResponses.ClarityTracking IS NOT NULL
				AND AutoApprovalProcessResponses.DataXTransactionID IS NOT NULL
				AND Dealers.IsDemoAccount = 0
				AND CAST(AutoApprovalProcessResponses.ClarityRequestDate AS DATE) = CAST(AutoApprovalProcessResponses.DataXRequestDate AS DATE)
				AND ClarityResponse IN (1,2,4,5)
				AND DataXResponse IN (1,2,4,5)
				--AUDITING
				AND CAST(AutoApprovalProcessResponses.ClarityRequestDate AS DATE) = '12-10-2015'