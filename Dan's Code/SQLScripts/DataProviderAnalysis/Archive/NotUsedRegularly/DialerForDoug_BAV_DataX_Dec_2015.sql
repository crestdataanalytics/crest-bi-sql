SET Transaction Isolation Level Read Uncommitted;


WITH
		MostRecentLease AS
		(
			SELECT		Loans.ID AS LoanID,
						Loans.DisplayID,
						Loans.NumDaysLate,
						Loans.ApplicantID,
						ROW_NUMBER() OVER (PARTITION BY Loans.ID ORDER BY Loans.CreationDate DESC) AS SequenceNumber
			FROM		Loans
			WHERE		Loans.CreationDate >= '2015-12-04 17:07:08.063'
			--order by applicantid, creationdate desc
		),
		MostRecentApplicantAddresses AS
		(
			SELECT		ApplicantAddresses.ApplicantID,
						ApplicantAddresses.AddressID,
						ROW_NUMBER() OVER (PARTITION BY ApplicantAddresses.ApplicantID ORDER BY ApplicantAddresses.AddressID DESC) AS SequenceNumber
			FROM		ApplicantAddresses
		)


SELECT		MostRecentLease.LoanID AS [Account Number 1],
			MostRecentLease.DisplayID AS [Account Number 2],
			Applicants.ID AS [Account Number 3],
			REPLACE(Applicants.FirstName,',','') AS FirstName,
			REPLACE(Applicants.LastName,',','') AS LastName,
			Applicants.HomePhone AS [Phone Number 1],
			Applicants.CellPhone AS [Phone Number 2],
			CASE WHEN Languages.Lang IS NULL THEN 'English' ELSE Languages.Lang END AS Group1,
			CASE WHEN MostRecentLease.NumDaysLate IS NULL THEN 0 ELSE MostRecentLease.NumDaysLate END AS NumDaysLate,
			Addresses.StateId,
			Teams.Name AS TeamName
			--CASE WHEN Teams.Name IS NULL THEN 'NOT ASSIGNED' ELSE Teams.Name END AS TeamName
FROM		Applicants
LEFT JOIN	Languages ON Applicants.LanguageID = Languages.ID
INNER JOIN	MostRecentLease ON MostRecentLease.ApplicantID = Applicants.ID
LEFT JOIN	MostRecentApplicantAddresses ON Applicants.ID = MostRecentApplicantAddresses.ApplicantID
LEFT JOIN	Addresses ON MostRecentApplicantAddresses.AddressID = Addresses.ID
LEFT JOIN	TeamUsers ON Applicants.AssignedUserID = TeamUsers.UserID
LEFT JOIN	Teams ON TeamUsers.TeamID = Teams.TeamID
WHERE		MostRecentApplicantAddresses.SequenceNumber = 1
			--and MostRecentLease.LoanID = 795586
--WHERE displayid = '729496-1'