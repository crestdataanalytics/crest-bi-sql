SET Transaction Isolation Level Read Uncommitted;

SELECT			YEAR(PaymentDate) Year,
				MONTH(PaymentDate) Month,
				DAY(PaymentDate) Day,
				COUNT(LoanPayments.ID) Count
FROM			LoanPayments
INNER JOIN		LoanPaymentTypes ON LoanPayments.TypeID = LoanPaymentTypes.ID
WHERE			PaymentDate is not null 
				AND IsDeleted = 0
				AND LoanPaymentTypes.[Type] NOT IN ('Initial Payment','Paid By Dealer','Refund')
				AND PaymentDate > '09/30/2015'
GROUP BY		YEAR(PaymentDate),MONTH(PaymentDate),DAY(PaymentDate)
ORDER BY		1,2,3