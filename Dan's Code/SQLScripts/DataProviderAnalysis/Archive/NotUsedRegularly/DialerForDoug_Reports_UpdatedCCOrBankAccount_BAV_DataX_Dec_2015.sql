SET Transaction Isolation Level Read Uncommitted;


SELECT		Loans.ID AS LoanID,
			Loans.DisplayID,
			CAST(Loans.CreationDate AS DATE) AS CreationDate,
			CAST(Notes.CreationDate AS DATE) AS NoteCreationDate
			--LeaseNotes.NoteContent --this is so Doug can audit the notes, but leaving it off for spinning data
FROM		Notes
INNER JOIN	Loans ON Loans.ID = Notes.LoanID
WHERE		Notes.CreationDate > '2014-12-01'
			AND Loans.FundedDate IS NOT NULL
			AND (Notes.NoteContent like '%New Bank Account:%'
			OR Notes.NoteContent like '%New Credit Card created%')
			AND Notes.CreationDate >= Loans.FundedDate
ORDER BY	Notes.CreationDate ASC