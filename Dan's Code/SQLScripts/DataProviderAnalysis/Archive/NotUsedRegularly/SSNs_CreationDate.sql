
select Loans.ID AS LoanID,
		Loans.ApplicantID,
		cast(Loans.CreationDate as date) CreationDate,
		cast(Loans.FundedDate as date) FundedDate,
		Applicants.SocialSecurityNumber
from	Loans
INNER JOIN	Applicants ON Applicants.ID = Loans.ApplicantID