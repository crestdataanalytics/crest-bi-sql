SET Transaction Isolation Level Read Uncommitted;

;WITH MultipleCategoryDealerIDs AS
(
	SELECT		COUNT(Dealers.ID) TotalCategories,
				Dealers.ID AS DealerID
	FROM		Dealers
	LEFT JOIN	DealerCategories on DealerCategories.DealerID = dealers.ID
	LEFT JOIN	Categories on DealerCategories.CategoryID = Categories.ID
	GROUP BY	Dealers.ID
),

TotalInquiries AS
(
	SELECT		YEAR(Loans.CreationDate) AS [Year],
				MONTH(Loans.CreationDate) AS [Month],
				DAY(Loans.CreationDate) AS [Day],
				COUNT(Loans.ID) TotalInquiries
	FROM		Loans 
	INNER JOIN	Applicants ON Applicants.ID = Loans.ApplicantID
	INNER JOIN	MultipleCategoryDealerIDs ON MultipleCategoryDealerIDs.DealerID = Loans.DealerID
	WHERE		Applicants.MainDealerID = 1337
				AND Loans.CreationDate > '2015-11-01'
				AND MultipleCategoryDealerIDs.TotalCategories < 2
	GROUP BY	YEAR(Loans.CreationDate),
				MONTH(Loans.CreationDate),
				DAY(Loans.CreationDate)
),

TrackCategories AS
(
	SELECT		YEAR(Loans.CreationDate) AS [Year],
				MONTH(Loans.CreationDate) AS [Month],
				DAY(Loans.CreationDate) AS [Day],
				COUNT(Loans.ID) SubTotal,
				Categories.CategoryName AS CategoryName
	FROM		Loans 
	INNER JOIN	Applicants ON Applicants.ID = Loans.ApplicantID
	LEFT JOIN	DealerCategories ON DealerCategories.DealerID = Loans.DealerID
	LEFT JOIN	Categories ON Categories.ID = DealerCategories.CategoryID
	INNER JOIN	MultipleCategoryDealerIDs ON MultipleCategoryDealerIDs.DealerID = Loans.DealerID
	WHERE		Applicants.MainDealerID = 1337
				AND Loans.CreationDate > '2015-11-01'
				AND MultipleCategoryDealerIDs.TotalCategories < 2
	GROUP BY	YEAR(Loans.CreationDate),
				MONTH(Loans.CreationDate),
				DAY(Loans.CreationDate),Categories.CategoryName
)

SELECT		TotalInquiries.[Year],
			TotalInquiries.[Month],
			TotalInquiries.[Day],
			TotalInquiries.TotalInquiries,
			TrackCategories.SubTotal,
			CAST(TrackCategories.SubTotal AS decimal) / TotalInquiries.TotalInquiries AS CategoryPercent,
			TrackCategories.CategoryName
FROM		TotalInquiries
LEFT JOIN	TrackCategories ON TrackCategories.[Day] = TotalInquiries.[Day]
			AND TrackCategories.[Month] = TotalInquiries.[Month]
			AND TrackCategories.[Year] = TotalInquiries.[Year]
ORDER BY	1 DESC, 2 DESC, 3 DESC



--with multiple categories
;WITH

TotalInquiries AS
(
	SELECT		YEAR(Loans.CreationDate) AS [Year],
				MONTH(Loans.CreationDate) AS [Month],
				DAY(Loans.CreationDate) AS [Day],
				COUNT(Loans.ID) TotalInquiries
	FROM		Loans 
	INNER JOIN	Applicants ON Applicants.ID = Loans.ApplicantID
	WHERE		Applicants.MainDealerID = 1337
				AND Loans.CreationDate > '2015-11-01'
	GROUP BY	YEAR(Loans.CreationDate),
				MONTH(Loans.CreationDate),
				DAY(Loans.CreationDate)
),

TrackCategories AS
(
	SELECT		YEAR(Loans.CreationDate) AS [Year],
				MONTH(Loans.CreationDate) AS [Month],
				DAY(Loans.CreationDate) AS [Day],
				COUNT(Loans.ID) SubTotal,
				Categories.CategoryName AS CategoryName
	FROM		Loans 
	INNER JOIN	Applicants ON Applicants.ID = Loans.ApplicantID
	LEFT JOIN	DealerCategories ON DealerCategories.DealerID = Loans.DealerID
	LEFT JOIN	Categories ON Categories.ID = DealerCategories.CategoryID
	WHERE		Applicants.MainDealerID = 1337
				AND Loans.CreationDate > '2015-11-01'
	GROUP BY	YEAR(Loans.CreationDate),
				MONTH(Loans.CreationDate),
				DAY(Loans.CreationDate),Categories.CategoryName
)

SELECT		TotalInquiries.[Year],
			TotalInquiries.[Month],
			TotalInquiries.[Day],
			TotalInquiries.TotalInquiries,
			TrackCategories.SubTotal,
			CAST(TrackCategories.SubTotal AS decimal) / TotalInquiries.TotalInquiries AS CategoryPercent,
			TrackCategories.CategoryName
FROM		TotalInquiries
LEFT JOIN	TrackCategories ON TrackCategories.[Day] = TotalInquiries.[Day]
			AND TrackCategories.[Month] = TotalInquiries.[Month]
			AND TrackCategories.[Year] = TotalInquiries.[Year]
ORDER BY	1 DESC, 2 DESC, 3 DESC














