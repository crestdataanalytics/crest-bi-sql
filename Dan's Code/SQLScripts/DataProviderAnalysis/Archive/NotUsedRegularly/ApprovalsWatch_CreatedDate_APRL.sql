SET Transaction Isolation Level Read Uncommitted;
Declare @startDate date,
		@endDate date
SET @startDate = '11/01/2015';
SET	@endDate = '1/01/2017';

WITH	Clarity AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS ClarityCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 1
				AND ApprovalProviderResponseLog.Decision NOT IN ('Exception','')
				AND ApprovalProviderResponseLog.Decision IS NOT NULL
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

ClarityApproved AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS ClarityApprovedCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 1
				AND ApprovalProviderResponseLog.Decision IN ('Approved','Approve')
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

DataX AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS DataXCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 2
				AND ApprovalProviderResponseLog.Decision NOT IN ('Exception','')
				AND ApprovalProviderResponseLog.Decision IS NOT NULL
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

DataXApproved AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS DataXApprovedCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 2
				AND ApprovalProviderResponseLog.Decision IN ('Approved','Approve')
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

MicroBilt AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS MicroBiltCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 3
				AND ApprovalProviderResponseLog.Decision NOT IN ('Exception','')
				AND ApprovalProviderResponseLog.Decision IS NOT NULL
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

MicroBiltApproved AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS MicroBiltApprovedCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 3
				AND ApprovalProviderResponseLog.Decision IN ('Approved','Approve')
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

FactorTrust AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS FactorTrustCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 4
				AND ApprovalProviderResponseLog.Decision NOT IN ('Exception','')
				AND ApprovalProviderResponseLog.Decision IS NOT NULL
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

FactorTrustApproved AS
(
	SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [Year],
				MONTH(ApprovalProviderResponseLog.CreatedDate) AS [Month],
				DAY(ApprovalProviderResponseLog.CreatedDate) AS [Day],
				COUNT(ApprovalProviderResponseLog.ApplicantID) AS FactorTrustApprovedCount
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
				AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 4
				AND ApprovalProviderResponseLog.Decision IN ('Approved','Approve')
	GROUP BY	YEAR(ApprovalProviderResponseLog.CreatedDate),
				MONTH(ApprovalProviderResponseLog.CreatedDate),
				DAY(ApprovalProviderResponseLog.CreatedDate)
),

--Unique ApplicantID groupings

APRLIDs AS
(
	SELECT		ApprovalProviderResponseLog.ID AS APRLID,
				ApprovalProviderResponseLog.CreatedDate,
				ApprovalProviderResponseLog.ApplicantID,
				ApprovalProviderResponseLog.Decision,
				ROW_NUMBER() OVER(PARTITION BY ApprovalProviderResponseLog.ApplicantID ORDER BY ApprovalProviderResponseLog.CreatedDate ASC) AS SequenceNumber
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	ORDER BY	ApplicantID, CreatedDate ASC
)




SELECT		CAST((CAST(Clarity.[Month] AS VARCHAR) + '-' + CAST(Clarity.[Day] AS VARCHAR) + '-' + CAST(Clarity.[Year] AS VARCHAR)) AS DATE) AS CreatedDate,
			Clarity.ClarityCount,
			ClarityApproved.ClarityApprovedCount,
			CAST(ClarityApproved.ClarityApprovedCount AS DEC) / Clarity.ClarityCount * 100 AS ClarityApprovedPercent,
			DataX.DataXCount,
			DataXApproved.DataXApprovedCount,
			CAST(DataXApproved.DataXApprovedCount AS DEC) / DataX.DataXCount * 100 AS DataXApprovedPercent,
			MicroBilt.MicroBiltCount,
			MicroBiltApproved.MicroBiltApprovedCount,
			CAST(MicroBiltApproved.MicroBiltApprovedCount AS DEC) / MicroBilt.MicroBiltCount * 100 AS MicroBiltApprovedPercent,
			FactorTrust.FactorTrustCount,
			FactorTrustApproved.FactorTrustApprovedCount,
			CAST(FactorTrustApproved.FactorTrustApprovedCount AS DEC) / FactorTrust.FactorTrustCount * 100 AS FactorTrustApprovedPercent
FROM		Clarity
LEFT JOIN	DataX ON DataX.[Year] = Clarity.[Year] AND DataX.[Month] = Clarity.[Month] AND DataX.[Day] = Clarity.[Day] 
LEFT JOIN	MicroBilt ON MicroBilt.[Year] = Clarity.[Year] AND MicroBilt.[Month] = Clarity.[Month] AND MicroBilt.[Day] = Clarity.[Day]
LEFT JOIN	FactorTrust ON FactorTrust.[Year] = Clarity.[Year] AND FactorTrust.[Month] = Clarity.[Month] AND FactorTrust.[Day] = Clarity.[Day]
LEFT JOIN	ClarityApproved ON ClarityApproved.[Year] = Clarity.[Year] AND ClarityApproved.[Month] = Clarity.[Month] AND ClarityApproved.[Day] = Clarity.[Day]
LEFT JOIN	DataXApproved ON DataXApproved.[Year] = Clarity.[Year] AND DataXApproved.[Month] = Clarity.[Month] AND DataXApproved.[Day] = Clarity.[Day]
LEFT JOIN	MicroBiltApproved ON MicroBiltApproved.[Year] = Clarity.[Year] AND MicroBiltApproved.[Month] = Clarity.[Month] AND MicroBiltApproved.[Day] = Clarity.[Day]
LEFT JOIN	FactorTrustApproved ON FactorTrustApproved.[Year] = Clarity.[Year] AND FactorTrustApproved.[Month] = Clarity.[Month] AND FactorTrustApproved.[Day] = Clarity.[Day]
ORDER BY	1 ASC




