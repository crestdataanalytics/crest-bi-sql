SET Transaction Isolation Level Read Uncommitted;

SELECT		DocumentInfos.DocumentID,
			DocumentInfos.ProviderID,
			Providers.ProviderName
FROM		DocumentInfos
INNER JOIN	Providers ON Providers.ProviderID = DocumentInfos.ProviderID