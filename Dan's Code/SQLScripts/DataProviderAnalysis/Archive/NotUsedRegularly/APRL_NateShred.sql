
-- this is meant to get the "global" decision from the provider on the request and a unique transaction id
-- will need to later get the things that cause HardKillDenied in the code
SET Transaction Isolation Level Read Uncommitted;

SELECT		DocumentInfos.DocumentID,
			DocumentInfos.ProviderID,
			DocumentInfos.SubjectID,
			DocumentInfos.SubjectID AS ApplicantID,
			DocumentInfos.GenerationDate,
			Providers.ProviderID,
			Providers.ProviderName,
			Variables.VariableID,
			Variables.VariableName,
			DocumentVariables.VariableValue
FROM		DocumentInfos
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
INNER JOIN	Providers ON Providers.ProviderID = DocumentInfos.ProviderID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		YEAR(DocumentInfos.GenerationDate) >= 2009 AND YEAR(DocumentInfos.GenerationDate) <= 2014
			AND Variables.VariableID IN (246, --clarity decision
											1811, -- clarity tracking-number
											5020, --datax transactionID
											5023, --datax trackID
											7266, -- datax decision
											12026, --microbilt decision
											12027, -- microbilt "tracking number"
											13238, -- factorTrust transactionID
											13054, -- factorTrust decision
											39791, -- eBureau risk score ... below 35 is denied
											39780 -- transaction id
											)

----let's me know all outcomes for a given VariableID
--SELECT		COUNT(DocumentVariables.VariableValue) AS Total,
--			DocumentVariables.VariableValue
--FROM		DocumentVariables
--WHERE		DocumentVariables.VariableID = 13054
--GROUP BY	DocumentVariables.VariableValue