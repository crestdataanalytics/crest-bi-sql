--//FPD based on Creation Date and 7 day buffer for returned payments

declare @date1 as date
declare @date2 as date

set @date1 = '9-1-16'
set @date2 = '10-1-16'
--set @date2 = dateadd(d,1,getdate())

SELECT
	--concat(U.FirstName,' ',U.LastName),
	--D.StatusID,
	--C.CategoryName,
	--L.ID,
	L.Rep,
	count(FSPo.FirstScheduledPaymentOutcomeID) as TotalLeases,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'oFPD$',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'pFPD$',
	--count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'procFPD$'
FROM
	(
		SELECT
			concat(U.FirstName,' ',U.LastName) as Rep,
			L.ID,
			L.ApplicantID,
			--L.CreationDate,
			UD.CreationDate,
			UD.FundedDate,
			L.StatusID,
			L.FundedAmount,
			L.DealerID
		FROM
			Loans as L
			left outer join
			(SELECT
				L.ID,
				--L.ApprovalUserID,
				L.ApplicantID,
				--RL.ApprovalUserID,
				--L.CreationDate,
				--RL.CreationDate as CD,
				Case when RL.ApprovalUserID is null then L.ApprovalUserID else RL.ApprovalUserID end as ApprovalUserID,
				Case when RL.CreationDate is null then L.CreationDate else RL.CreationDate end as CreationDate,
				Case when RL.FundedDate is null then L.FundedDate else RL.FundedDate end as FundedDate
			FROM
				Loans as L
				left outer join
				(SELECT
					L.ID,
					L.StatusID,
					L.AutoApprovedFlag as APF,
					L.ApprovalUserID as APU,
					EX.AutoApprovedFlag,
					EX.ApprovalUserID as ApprovalUserID,
					EX.CreationDate,
					EX.FundedDate
				FROM
					Loans as L
					left outer join
						(SELECT
							ApplicantID,
							min(CreationDate) as CreationDate,
							min(AutoApprovedFlag) as AutoApprovedFlag,
							min(ApprovalUserID) as ApprovalUserID,
							min(FundedDate) as FundedDate
						FROM
							Loans
						WHERE StatusID = 21
						GROUP BY
							ApplicantID) as EX on EX.ApplicantID = L.ApplicantID
				WHERE
					L.AutoApprovedFlag = 0
					and EX.AutoApprovedFlag = 0
					and L.StatusID not in (7,10,11,20,21)) as RL on RL.ID = L.ID
			WHERE
				L.StatusID in (4,6,12,13,14)
				and L.AutoApprovedFlag = 0) as UD on UD.ID = L.ID
			join vw_Users as U on UD.ApprovalUserID = U.ID and U.RoleList like '%Approvals%'
		WHERE
			L.AutoApprovedFlag = 0
			--and L.FundedDate > '10-10-16'
			and L.ID not in 
				(SELECT
					L.ID
				FROM
					Loans as L
					left outer join
						(SELECT
							ApplicantID,
							min(CreationDate) as CreationDate,
							min(AutoApprovedFlag) as AutoApprovedFlag,
							min(ApprovalUserID) as ApprovalUserID
						FROM
							Loans
						WHERE StatusID = 21
						GROUP BY
							ApplicantID) as EX on EX.ApplicantID = L.ApplicantID
				WHERE
					L.AutoApprovedFlag = 0
					and EX.AutoApprovedFlag in (1,2)
					and L.StatusID not in (7,10,11,20,21))
	) as L
	join
	(SELECT
		L.ID,
		Case when datediff(mm,EX.CreationDate,L.CreationDate) = 0 then 1 else 0 end as ExcludeRevised,
		Case when EX.NonInitialAmountPaidToDate > 0 then 5 else FSPO.FirstScheduledPaymentOutcomeID end as FirstScheduledPaymentOutcomeID
	FROM
		Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
		left outer join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
		left outer join
			(SELECT
				L.ApplicantID,
				L.CreationDate,
				FSP.EvaluationDate,
				FSP.NonInitialAmountPaidToDate
			FROM
				Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
				join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
			WHERE
				L.StatusID = 21) as EX on EX.ApplicantID = L.ApplicantID and EX.EvaluationDate = FSP.EvaluationDate
	WHERE
		L.FundedAmount > 0
		and FSP.IsCurrentOutcome = 1
		--and L.ApplicantID = 1099063
		and L.StatusID not in (21)
		) as FSPO on FSPO.ID = L.ID --and FSPO.ExcludeRevised = 0
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	--left outer join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID 
	left outer join Dealers as D on D.ID = L.DealerID
	--left outer join
	--	(SELECT
	--		DealerID,
	--		min(CategoryID) as CategoryID,
	--		min(rankOrder) as RO
	--	FROM
	--		DealerCategories
	--	GROUP BY
	--		DealerID) as DC on DC.DealerID = D.ID
	--join Categories as C on C.ID = DC.CategoryID
WHERE
	L.FundedAmount > 0
	and FSP.IsCurrentOutcome = 1
	and L.CreationDate > @date1
	and L.CreationDate < @date2
	--and L.AutoApprovedFlag = 0
	and D.IsDemoAccount = 0
	--and C.CategoryName is null
	and datediff(dd,FSP.FirstScheduledPaymentDueDate,getdate()) >= 7
	and L.FundedDate is not null
	and L.StatusID not in (10,20)
GROUP BY
	L.Rep
	--L.ID
	--D.StatusiD,
	--C.CategoryName
	--concat(U.FirstName,' ',U.LastName)
ORDER BY
	count(FSPo.FirstScheduledPaymentOutcomeID) desc

/*
--//FPD based on Funded Date and 7 day buffer for returned payments

SELECT
	--concat(U.FirstName,' ',U.LastName),
	--D.StatusID,
	--C.CategoryName,
	count(FSPo.FirstScheduledPaymentOutcomeID) as TotalLeases,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
	count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
FROM
	Loans as L 
	join 
	(SELECT
		L.ID,
		case when EX.NonInitialAmountPaidToDate > 0 then 5 else FSPO.FirstScheduledPaymentOutcomeID end as FirstScheduledPaymentOutcomeID
	FROM
		Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
		join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
		left outer join
			(SELECT
				L.ApplicantID,
				FSP.EvaluationDate,
				FSP.NonInitialAmountPaidToDate
			FROM
				Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
				join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
			WHERE
				L.StatusID = 21
				and FSP.NonInitialAmountPaidToDate >0) as EX on EX.ApplicantID = L.ApplicantID and EX.EvaluationDate = FSP.EvaluationDate
	WHERE
		L.FundedAmount > 0
		and FSP.IsCurrentOutcome = 1
		--and L.ApplicantID = 1099063
		and L.StatusID not in (21)) as FSPO on FSPO.ID = L.ID
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	--join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	--join LoanStatuses as LS on LS.ID = L.StatusID
	--left outer join vw_users as U on U.ID = L.ApprovalUserID and RoleList like '%Approvals%'
	left outer join Dealers as D on D.ID = L.DealerID
	--left outer join DealerCategories as DC on DC.DealerID = D.ID --and DC.RankOrder = 1
	left outer join
		(SELECT
			DealerID,
			CategoryID,
			min(rankOrder) as RO
		FROM
			DealerCategories
		GROUP BY
			DealerID,
			CategoryID) as DC on DC.DealerID = D.ID
	left outer join Categories as C on C.ID = DC.CategoryID
WHERE
	L.FundedAmount > 0
	and FSP.IsCurrentOutcome = 1
	and L.FundedDate > @date1
	and L.FundedDate < @date2
	--and L.AutoApprovedFlag = 0
	and D.IsDemoAccount = 0
	and L.FundedDate is not null
	and L.StatusID not in (10,20)
	--and C.CategoryName is null
	and datediff(dd,cast(FSP.FirstScheduledPaymentDueDate as date),getdate()) > 7
--GROUP BY
	--D.StatusiD,
	--C.CategoryName
	--concat(U.FirstName,' ',U.LastName)

--//FPD RAW Numbers on Funded Date
SELECT
	--concat(U.FirstName,' ',U.LastName),
	--D.StatusID,
	--C.CategoryName,
	count(FSPo.FirstScheduledPaymentOutcomeID) as TotalLeases,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	count(case when FSPo.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
	count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
FROM
	Loans as L 
	join 
	(SELECT
		L.ID,
		case when EX.NonInitialAmountPaidToDate > 0 then 5 else FSPO.FirstScheduledPaymentOutcomeID end as FirstScheduledPaymentOutcomeID
	FROM
		Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
		join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
		left outer join
			(SELECT
				L.ApplicantID,
				FSP.EvaluationDate,
				FSP.NonInitialAmountPaidToDate
			FROM
				Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
				join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
			WHERE
				L.StatusID = 21
				and FSP.NonInitialAmountPaidToDate >0) as EX on EX.ApplicantID = L.ApplicantID and EX.EvaluationDate = FSP.EvaluationDate
	WHERE
		L.FundedAmount > 0
		and FSP.IsCurrentOutcome = 1
		--and L.ApplicantID = 1099063
		and L.StatusID not in (21)) as FSPO on FSPO.ID = L.ID
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	--join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	--join LoanStatuses as LS on LS.ID = L.StatusID
	--left outer join vw_users as U on U.ID = L.ApprovalUserID and RoleList like '%Approvals%'
	left outer join Dealers as D on D.ID = L.DealerID
	--left outer join DealerCategories as DC on DC.DealerID = D.ID --and DC.RankOrder = 1
	--left outer join
	--	(SELECT
	--		DealerID,
	--		CategoryID,
	--		min(rankOrder) as RO
	--	FROM
	--		DealerCategories
	--	GROUP BY
	--		DealerID,
	--		CategoryID) as DC on DC.DealerID = D.ID
	--left outer join Categories as C on C.ID = DC.CategoryID
WHERE
	L.FundedAmount > 0
	and FSP.IsCurrentOutcome = 1
	and L.CreationDate > @date1
	and L.CreationDate < @date2
	--and L.AutoApprovedFlag = 0
	and D.IsDemoAccount = 0
	and L.FundedDate is not null
	and L.StatusID not in (10,20)
	--and C.CategoryName is null
	--and datediff(dd,FSP.FirstScheduledPaymentDueDate,getdate()) > 7
--GROUP BY
	--D.StatusiD,
	--C.CategoryName
	--concat(U.FirstName,' ',U.LastName)
*/
