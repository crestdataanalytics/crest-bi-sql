/****** Script for SelectTopNRows command from SSMS  ******/
SELECT		WelcomeCallUploads.ID AS WelcomeCallUploadsID,
			LoanID,
			PhoneNumber,
			Result,
			CallDate,
			ImportDate,
			ImportStatus,
			Seconds,
			[Description],
			[Status]
FROM		WelcomeCallUploads
INNER JOIN	WelcomeCallStatuses ON WelcomeCallStatuses.ID = WelcomeCallUploads.ImportStatus
WHERE		CallDate >= '2016-01-01'