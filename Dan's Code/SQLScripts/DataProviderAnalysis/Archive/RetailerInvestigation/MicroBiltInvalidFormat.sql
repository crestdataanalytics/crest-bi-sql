SET Transaction Isolation Level Read Uncommitted;

SELECT		--count(distinct applicantID)
			ApprovalProviderResponseLog.CreatedDate,
			ApprovalProviderResponseLog.ApprovalProviderSettingsID,
			--vw_loans_reporting.DealerID,
			ApprovalProviderResponseLog.TrackingID
From		CrestWarehouse.dbo.ApprovalProviderResponseLog
--INNER JOIN	vw_loans_reporting ON ApprovalProviderResponseLog.ApplicantID = vw_loans_reporting.ApplicantID
WHERE		DenyDescription = 'Invalid format. One or more data fields contain invalid data. The <hint> value inside the error block contains which element(s) have invalid data.'
ORDER BY	CreatedDate DESC