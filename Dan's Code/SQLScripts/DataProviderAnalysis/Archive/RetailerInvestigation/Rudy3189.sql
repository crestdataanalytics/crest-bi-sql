
SET Transaction Isolation Level Read Uncommitted;

SELECT	MONTH(CreationDate) Month, 
		YEAR(CreationDate) Year, 
		COUNT(ID) Total, 
		COUNT(ApprovalDate) TotalApproved,
		COUNT(FundedDate) Funded
FROM vw_loans_reporting
WHERE DealerID = 3189
--and month(CreationDate) in (8,9) and year(CreationDate) = 2015
and vw_loans_reporting.StatusID != 11
and year(creationdate) = 2015
GROUP BY month(CreationDate), year(CreationDate)
ORDER BY 2,1 DESC

SELECT	CAST(creationdate AS DATE) CreationDate, 
		COUNT(ID) Total, 
		COUNT(ApprovalDate) TotalApproved,
		COUNT(FundedDate) Funded
FROM	vw_loans_reporting
WHERE	DealerID = 3189
		AND vw_loans_reporting.StatusID != 11
GROUP BY CAST(creationdate AS DATE)
ORDER BY 1 DESC

SELECT	month(CreationDate) Month, 
		year(CreationDate) year,
		count(DenyDescription),
		DenyDescription
From CrestWarehouse.dbo.ApprovalProviderResponseLog
inner join vw_loans_reporting on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
where DealerID = 3189
and vw_loans_reporting.StatusID != 11
and month(CreationDate) in (9,10) and year(CreationDate) = 2015
and Decision in ('Denied','Deny')
and ApprovalProviderResponseLog.ApplicantID not in (1039081,1039165,1040003,1040654,1044737)
group by month(CreationDate), 
		year(CreationDate),
		DenyDescription
order by 1,2,3

SELECT *
FROM Loans
WHERE DealerID = 3189
		AND cast(CreationDate as date) = '2015-10-14'
		and ApprovalDate is not null


--SELECT	month(CreationDate) Month, 
--		year(CreationDate) year,
--		count(DenyDescription),
--		DenyDescription
--From CrestWarehouse.dbo.ApprovalProviderResponseLog
--inner join vw_loans_reporting on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
--where DealerID = 3189
--and vw_loans_reporting.StatusID != 11
--and month(CreationDate) in (10) and year(CreationDate) = 2015 and day(CreationDate) in (12,13,14)
--and Decision in ('Denied','Deny')
--and ApprovalProviderResponseLog.ApplicantID not in (1039081,1039165,1040003,1040654,1044737)
--group by month(CreationDate), 
--		year(CreationDate),
--		DenyDescription
--order by 1,2,3

