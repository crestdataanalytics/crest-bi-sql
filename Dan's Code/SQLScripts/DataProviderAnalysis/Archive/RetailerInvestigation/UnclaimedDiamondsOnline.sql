--add auto approved vs manual approved on this.
SET Transaction Isolation Level Read Uncommitted;

SELECT	MONTH(CreationDate) Month, 
		YEAR(CreationDate) Year, 
		COUNT(ID) Total, 
		COUNT(ApprovalDate) TotalApproved,
		COUNT(FundedDate) Funded
FROM vw_loans_reporting
WHERE DealerID = 8573
--and month(CreationDate) in (8,9) and year(CreationDate) = 2015
and vw_loans_reporting.StatusID != 11
GROUP BY month(CreationDate), year(CreationDate)
ORDER BY 2 desc,1 DESC

SELECT	CAST(creationdate AS DATE) CreationDate, 
		COUNT(ID) Total, 
		COUNT(ApprovalDate) TotalApproved,
		COUNT(FundedDate) Funded
FROM	vw_loans_reporting
WHERE	DealerID = 8573
		AND vw_loans_reporting.StatusID != 11
		AND vw_loans_reporting.ApplicantID not in (1039081,1039165,1040003,1040654,1044737)  --these are Demo accounts that Sulby ID'd
GROUP BY CAST(creationdate AS DATE)
ORDER BY 1 DESC

