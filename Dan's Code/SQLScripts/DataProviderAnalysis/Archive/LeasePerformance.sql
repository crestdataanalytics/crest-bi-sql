SELECT Dealers.ID AS RetailerID, Dealers.Name AS RetailerName, Categories.CategoryName AS RetailerCategory, Addresses.StateID as State,
cast(vw_loans_reporting.CreationDate AS date) AS LeaseCreationDate, vw_loans_reporting.OriginationDate, 
vw_loans_reporting.ID AS LoanID, vw_loans_reporting.DisplayID AS DisplayID, 
vw_loans_reporting.Amount AS FinancedAmount, vw_loans_reporting.FundedAmount AS MerchantPaidAmount,
cASt(vw_loans_reporting.FundedDate AS Date) FundedDate,
vw_loans_reporting.TotalNote, cASt(vw_loans_reporting.NinetyDaysEndDate AS Date) AS NinetyDaysEndDate,
cASt(vw_loans_reporting.ChargedOffDate AS date) AS ChargedOffDate,
cASt(vw_loans_reporting.PaidOffDate AS date) AS PaidOffDate, LoanStatuses.[Status], vw_loans_reporting.AutoApprovedFlag AS IsAutoApproved,
ISNULL(12,LoanTerms.Months) AS TermOfLeaseInMonths,
case
when LoanPayments.Returned = 0 
and LoanPayments.IsDeleted = 0
and LoanPayments.HasBeenReturned = 0 
and LoanPaymentTypes.[Type] not in ('Initial Payment','Migrated')
and vw_loans_reporting.FundedDate is not null
then LoanPayments.Amount end as ClearedPaymentAmount,
case
when LoanPayments.Returned = '' 
and LoanPayments.IsDeleted = ''
and LoanPayments.HasBeenReturned = '' 
and LoanPaymentTypes.[Type] not in ('Initial Payment','Migrated')
and vw_loans_reporting.FundedDate is not null
then LoanPayments.PaymentDate end as ClearedPaymentDate,
FirstDueDate.FirstDueDate

From vw_loans_reporting
LEFT JOIN LoanTerms on vw_loans_reporting.LoanTermID = LoanTerms.ID
INNER JOIN Dealers on Dealers.ID = vw_loans_reporting.DealerID
inner join DealerCategories on Dealers.ID = DealerCategories.DealerID
INNER JOIN Categories on DealerCategories.CategoryID = Categories.ID
INNER JOIN Applicants on Applicants.ID = vw_loans_reporting.ApplicantID
LEFT JOIN Addresses on Applicants.AddressID = Addresses.ID
INNER JOIN LoanStatuses on vw_loans_reporting.StatusID = LoanStatuses.ID
LEFT JOIN LoanPayments on LoanPayments.LoanID = vw_loans_reporting.ID
LEFT JOIN LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
LEFT JOIN
(SELECT  LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
FROM    LoanPaymentSchedule
WHERE ScheduleEntryDesc not in ('Initial Payment', '')
GROUP BY LoanID) FirstDueDate
on FirstDueDate.LoanID = vw_loans_reporting.ID

WHERE vw_loans_reporting.OriginationDate >= '01/01/2012' 
and vw_loans_reporting.OriginationDate < '01/01/2013'
and LoanPayments.IsDeleted = 0
and LoanPayments.Returned = 0
and LoanPayments.ReturnDate is NULL
and LoanStatuses.Status != 'Canceled'
