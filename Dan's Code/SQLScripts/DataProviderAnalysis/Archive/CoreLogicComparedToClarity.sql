
select distinct ApprovalProviderResponseLog.ApplicantID, ApprovalProviderResponseLog.CreatedDate, 
	case 
		when ApprovalProviderResponseLog.Decision = 'Approve' then 'Approved'
		when ApprovalProviderResponseLog.Decision = 'Deny' then 'Denied'
		else ApprovalProviderResponseLog.Decision end as CoreLogicDecision, 
	case 
		when ClarityResponseLog.Decision = 'Approve' then 'Approved'
		when ClarityResponseLog.Decision = 'Deny' then 'Denied'
		else ClarityResponseLog.Decision end as ClarityDecision,
	case 
		when DataXResponseLog.Decision = 'Approve' then 'Approved'
		when DataXResponseLog.Decision = 'Deny' then 'Denied'
		else DataXResponseLog.Decision end as DataXDecision,
	case 
		when MicroBiltResponseLog.Decision = 'Approve' then 'Approved'
		when MicroBiltResponseLog.Decision = 'Deny' then 'Denied'
		else MicroBiltResponseLog.Decision end as MicroBiltDecision,
	case 
		when FactorTrustResponseLog.Decision = 'Approve' then 'Approved'
		when FactorTrustResponseLog.Decision = 'Deny' then 'Denied'
		else FactorTrustResponseLog.Decision end as FactorTrustDecision,
	ApprovalProviderResponseLog.RiskScore as RiskScore,
	case when AutoApprovalProcessResponses.ApprovalPath = 'FULL' then 1 else 0 end as ApprovalPathIsFull
from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
left join AutoApprovalProcessResponses on AutoApprovalProcessResponses.ApplicantID = ApprovalProviderResponseLog.ApplicantID

left join
		(Select distinct ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 1 and ApprovalProviderResponseLog.createddate > '05/05/2015'
		and Decision is not null) as ClarityResponseLog
			on ClarityResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID

left join
		(Select distinct ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 2 and ApprovalProviderResponseLog.createddate > '05/05/2015'
		and Decision is not null) as DataXResponseLog
			on DataXResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID

left join
		(Select distinct ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 3 and ApprovalProviderResponseLog.createddate > '05/05/2015'
		and Decision is not null) as MicroBiltResponseLog
			on MicroBiltResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID

left join
		(Select distinct ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 4 and ApprovalProviderResponseLog.createddate > '05/05/2015'
		and Decision is not null) as FactorTrustResponseLog
			on FactorTrustResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID
							
where ApprovalProviderResponseLog.createddate > '05/05/2015'
	and ApprovalProviderResponseLog.ApprovalProviderSettingsID = 8 
	--and AutoApprovalProcessResponses.ApprovalPath = 'FULL'
	--and ApprovalProviderResponseLog.Decision = 'Denied' 
	--and ApprovalProviderResponseLog.RiskScore >=500
	--and ApprovalProviderResponseLog.createddate > '05/05/2015'
	--and ClarityResponseLog.createddate > '05/05/2015'
	--and DataXResponseLog.createddate > '05/05/2015'
	--and MicroBiltResponseLog.createddate > '05/05/2015'
	--and FactorTrustResponseLog.createddate > '05/05/2015'
order by ApprovalProviderResponseLog.CreatedDate desc

select count(applicantID)
from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
where --ApprovalProviderResponseLog.createddate > '05/05/2015'
 ApprovalProviderResponseLog.ApprovalProviderSettingsID = 8
