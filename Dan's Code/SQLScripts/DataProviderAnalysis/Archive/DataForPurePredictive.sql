

select vw_loans_reporting.ID as UniqueID, vw_loans_reporting.ApplicantID, vw_loans_reporting.DealerID,
	[CrestWarehouse].[dbo].[ApprovalProviderResponseLog].ResponseXML as 'ResponseXML(Clarity)'
--select count(vw_loans_reporting.id)
from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] with (NOLOCK)
inner join vw_loans_reporting with (NOLOCK) on vw_loans_reporting.applicantID = [CrestWarehouse].[dbo].[ApprovalProviderResponseLog].ApplicantID
inner join ApprovalProviderSettings with (NOLOCK) on ApprovalProviderSettings.ID = [CrestWarehouse].[dbo].[ApprovalProviderResponseLog].ApprovalProviderSettingsID
where [CrestWarehouse].[dbo].[ApprovalProviderResponseLog].ResponseXML is not null and
	ApprovalProviderSettings.ProviderName = 'Clarity' 
	and [CrestWarehouse].[dbo].[ApprovalProviderResponseLog].CreatedDate between '01/01/2015' and getdate()
	
	and not (
				vw_loans_reporting.id % 91 = 0 and 
				[CrestWarehouse].[dbo].[ApprovalProviderResponseLog].CreatedDate  between '11/01/2014' and getdate()
			)
order by vw_loans_reporting.ApplicantID desc