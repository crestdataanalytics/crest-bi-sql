--the problem with this is the decision that is coming through.  approve and approved need to be merged....same with denied and deny.
select *--vw_loans_reporting.DisplayID --count(vw_loans_reporting.DisplayID) as countID
from vw_loans_reporting
left join
		(select ApprovalProviderResponseLog.ApplicantID, count(ApprovalProviderResponseLog.ApplicantID) as countAID, count(decision) as countAD, 
		Case Decision 
			when 'Approve' then 'Approved'
			when 'Approved' then 'Approved'
			when 'Deny' then 'Denied'
			when 'Denied' then 'Denied'
			ELSE 'NULL'
			END as ClarityDecision 
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as DataXDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 2 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as DataX
				on DataX.applicantid = ApprovalProviderResponseLog.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as MicroBiltDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 3 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as MicroBilt
				on MicroBilt.applicantid = ApprovalProviderResponseLog.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as FactorTrustDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 4 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as FactorTrust
				on FactorTrust.applicantid = ApprovalProviderResponseLog.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as eBureauDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 6 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as eBureau
				on eBureau.applicantid = ApprovalProviderResponseLog.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as ClarityBankDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 7 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as ClarityBank
				on ClarityBank.applicantid = ApprovalProviderResponseLog.applicantid

		where ApprovalProviderSettingsID = 1 and CreatedDate is not null and Decision is not null
		group by ApprovalProviderResponseLog.applicantid, ApprovalProviderResponseLog.decision) as Clarity
		on Clarity.applicantid = vw_loans_reporting.applicantid
left join
		(
		select  vw_loans_reporting.id, count(vw_loans_reporting.id) as countID,sum(LoanPaymentFeesOwed.Amount) as TotalFeesOwed
        from vw_loans_reporting 
        inner join LoanPaymentFeesOwed on LoanPaymentFeesOwed.loanid = vw_loans_reporting.id
        where LoanPaymentFeesOwed.id is not null and LoanPaymentFeesOwed.feetypeid = 2 and LoanPaymentFeesOwed.waiveddate is null
		group by LoanPaymentFeesOwed.Amount,vw_loans_reporting.id) as Fees
        on fees.id = vw_loans_reporting.id
left join
		(select vw_loans_reporting.DisplayID, sum(LoanPayments.Amount) as TotalPaid
		from LoanPayments
		inner join LoanPaymentTypes on LoanPaymentTypes.ID = LoanPayments.TypeID
		inner join vw_loans_reporting on vw_loans_reporting.ID = LoanPayments.LoanID
		where LoanPaymentTypes.Type not in ('Refund','Migrated', 'Initial Payment') and LoanPayments.HasBeenReturned = 0 
			and LoanPayments.IsDeleted = 0 and vw_loans_reporting.ID = LoanPayments.LoanID
			and vw_loans_reporting.DealerID not in (4232, 3205)
		group by vw_loans_reporting.DisplayID) as TotalPaid
		on TotalPaid.DisplayID = vw_loans_reporting.DisplayID
--look at duplicates here...the number of total accounts increased with the Delinquent join by about 70 accounts
--left join
--		(select vw_loans_reporting.ApplicantID, 1 as IsDelinquent
--		from vw_loans_reporting
--		where vw_loans_reporting.StatusID in (6,12, 16)) as Delinquent
--		on Delinquent.ApplicantID = vw_loans_reporting.ApplicantID
where vw_loans_reporting.DealerID not in (4232, 3205) and vw_loans_reporting.DisplayID = '504660-1'--these are the retailer id's for 'crest financial flooring' for retailers
--group by vw_loans_reporting.DisplayID
--order by countID desc 
	