SET Transaction Isolation Level Read Uncommitted;


	SELECT	DocumentID
	INTO	#tempTable
	FROM	DocumentInfos
	WHERE	GenerationDate > '2015-12-01'


--clarity
	SELECT		COUNT(VariableValue) AS ClarityDenyDescriptionsCount,
				VariableValue AS ClarityDenyDescriptions
	FROM		DocumentVariables
	INNER JOIN	#tempTable ON #tempTable.DocumentID = DocumentVariables.DocumentID
	WHERE		VariableID IN (9895,9699,79) --clear bank, clear tradeline, clear fraud
				AND VariableValue != ''
	GROUP BY	VariableValue


	--data x
	SELECT		COUNT(VariableValue) AS DataXDenyCodesCount,
				VariableValue AS DataXDenyCodes,
				Variables.VariableName
	FROM		DocumentVariables
	INNER JOIN	#tempTable ON #tempTable.DocumentID = DocumentVariables.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (7214,8164,7910,7710,7410)
				AND VariableValue LIKE 'D%'
	GROUP BY	VariableValue,Variables.VariableName

--microbilt
	SELECT		COUNT(VariableValue) AS MicroBiltDenyDescriptionsCount,
				Variables.VariableName,
				VariableValue AS MicroBiltDenyDescriptions
	FROM		DocumentVariables
	INNER JOIN	#tempTable ON #tempTable.DocumentID = DocumentVariables.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (39711,39715,39719,12202,39712,39716,39720,12150,12206,39714,39718,39722,12197,12177,39713,39717,39721,12191)
				AND VariableValue != ''
	GROUP BY	VariableValue,Variables.VariableName


--factor trust
	SELECT		COUNT(VariableValue) AS FactorTrustDenyDescriptionsCount,
				VariableValue AS FactorTrustDenyDescriptions
	FROM		DocumentVariables
	INNER JOIN	#tempTable ON #tempTable.DocumentID = DocumentVariables.DocumentID
	WHERE		VariableID = 13375
				AND VariableValue != ''
	GROUP BY	VariableValue


	DROP TABLE #tempTable