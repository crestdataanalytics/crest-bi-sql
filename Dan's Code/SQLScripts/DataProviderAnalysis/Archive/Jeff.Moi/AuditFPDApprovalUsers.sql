--this helps to ensure we have the right funded loan count with a first payment due
SELECT		COUNT(DISTINCT Loans.ID) as TotalLeasesWithFirstPaymentDue
			--Distinct Loans.ID as LoanID
FROM		Loans
INNER JOIN Dealers on Dealers.id = Loans.DealerID
INNER JOIN (SELECT LoanID,DueDate,ScheduleEntryDesc
			From LoanPaymentSchedule
			Where LoanPaymentSchedule.ScheduleEntryDesc != 'Initial Payment'
			) as LoanPaymentSchedule on LoanPaymentSchedule.LoanID = Loans.ID
INNER JOIN	Users ON Users.ID = Loans.ApprovalUserID
WHERE		Loans.FundedDate is not null 
			AND Dealers.IsDemoAccount = 0 
			AND LoanPaymentSchedule.DueDate <= GETDATE()
			--and month(fundeddate) = 11
			--and year(FundedDate) = 2015
			AND FundedDate >= '2014-01-01' 
			AND FundedDate <= '2015-12-09 14:35:20'-- the most recent timestamp on my dfV3Leases data frame
			--and loans.ID in (1046903, 1012483, 1012378,  943522,  917613,  880210,  879095,  870632,  837697,  841709,  826629,  802720,  830720,  814390,  794108,
			--  781542,  785147,  787480,  756946,  734231,591132,  812485,  591920) -- these are leases where the ApprovalUserID is NULL

--this audits the amount per vintage per approval user per retailer id
SELECT		SUM(DISTINCT Loans.FundedAmount) as TotalFunded
			--Distinct Loans.ID as LoanID
FROM		Loans
INNER JOIN dealers on dealers.id = loans.dealerid
INNER JOIN (SELECT LoanID,DueDate,ScheduleEntryDesc
			From LoanPaymentSchedule
			Where LoanPaymentSchedule.ScheduleEntryDesc != 'Initial Payment'
			) as LoanPaymentSchedule on LoanPaymentSchedule.LoanID = Loans.ID
INNER JOIN	Users ON Users.ID = Loans.ApprovalUserID
WHERE		Loans.FundedDate is not null 
			AND Dealers.IsDemoAccount = 0 
			AND LoanPaymentSchedule.DueDate <= GETDATE()
			--and month(fundeddate) = 11
			--and year(FundedDate) = 2015
			AND FundedDate >= '2014-01-01' 
			AND FundedDate <= '2015-12-09 14:35:20'-- the most recent timestamp on my dfV3Leases data frame
			AND Loans.ApprovalUserID = 17179
			AND Loans.DealerID = 5026
			AND MONTH(Loans.FundedDate) = 11
			AND YEAR(Loans.FundedDate) = 2015
			--and loans.ID in (1046903, 1012483, 1012378,  943522,  917613,  880210,  879095,  870632,  837697,  841709,  826629,  802720,  830720,  814390,  794108,
			--  781542,  785147,  787480,  756946,  734231,591132,  812485,  591920) -- these are leases where the ApprovalUserID is NULL