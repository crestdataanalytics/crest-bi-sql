
  SET Transaction Isolation Level Read Uncommitted;

  SELECT		Loans.DisplayID,
				CAST(Loans.FundedDate AS Date) AS FundedDate,
				CAST(FirstScheduledPaymentDetails.FirstScheduledPaymentDueDate AS Date) AS FirstScheduledPaymentDueDate,
				FirstScheduledPaymentOutcome.OutcomeName,
				FirstScheduledPaymentOutcome.IsFinalOutcome AS CrestFPD,--if '1' then FPD
				FirstScheduledPaymentDetails.IsCurrentOutcome
  FROM			FirstScheduledPaymentDetails
  INNER JOIN	FirstScheduledPaymentOutcome ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
  INNER JOIN	Loans ON Loans.ID = FirstScheduledPaymentDetails.LoanID
  Where			FirstScheduledPaymentOutcome.IsFinalOutcome = 1 and FirstScheduledPaymentDetails.IsCurrentOutcome = 1
				AND FundedDate >= '2016-08-01'
				
  --order by LoanID

  --select		count(FirstScheduledPaymentOutcome.OutcomeName),FirstScheduledPaymentOutcome.OutcomeName
  --FROM			FirstScheduledPaymentDetails
  --INNER JOIN	FirstScheduledPaymentOutcome ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
  --INNER JOIN	Loans ON Loans.ID = FirstScheduledPaymentDetails.LoanID
  --Where			FirstScheduledPaymentOutcome.IsFinalOutcome = 1 and 
  --FirstScheduledPaymentDetails.IsCurrentOutcome = 1
  --group by FirstScheduledPaymentOutcome.OutcomeName