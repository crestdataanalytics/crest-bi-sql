
SET Transaction Isolation Level Read Uncommitted;



SELECT		LoanPaymentSchedule.LoanID,
			LoanPaymentSchedule.ScheduleEntryDesc, 
			LoanPaymentSchedule.DueAmount,
			CAST(LoanPaymentSchedule.DueDate AS DATE) DueDate,
			LoanPaymentSchedule.AmountPaid,
			LoanPaymentSchedule.PaidStatus,
			LoanPaymentSchedule.ID
FROM		LoanPaymentSchedule