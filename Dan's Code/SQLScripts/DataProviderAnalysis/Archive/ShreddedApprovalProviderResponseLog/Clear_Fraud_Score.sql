
SET Transaction Isolation Level Read Uncommitted;

SELECT		DocumentVariables.DocumentID,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(20)) AS VariableValue
FROM		ProviderAnalysis.dbo.Variables
INNER JOIN	ProviderAnalysis.dbo.DocumentVariables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		Variables.VariableID IN (10022,1811) --10022 == Fraud Score, 1811 == TrackingID