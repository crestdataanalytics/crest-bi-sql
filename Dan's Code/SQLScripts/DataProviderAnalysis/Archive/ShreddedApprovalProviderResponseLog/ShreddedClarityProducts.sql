SET Transaction Isolation Level Read Uncommitted;

SELECT		DocumentInfos.DocumentID,
			DocumentInfos.SubjectID AS ApplicantID,
			DocumentInfos.GenerationDate AS InquiryDate,
			DocumentVariables.VariableID,
			DocumentVariables.VariableValue,
			Variables.VariableName
FROM		[ProviderAnalysis].[dbo].[DocumentInfos]
INNER JOIN	ProviderAnalysis.dbo.DocumentVariables on DocumentInfos.DocumentID = DocumentVariables.DocumentID
INNER JOIN	ProviderAnalysis.dbo.Variables on DocumentVariables.VariableID = Variables.VariableID
WHERE		DocumentVariables.VariableID IN (9697,9791,103) --9697 = Clear Fraud | 9791 = Clear Tradeline | 103 = Clear Bank