SET Transaction Isolation Level Read Uncommitted;


SELECT		DocumentVariables.DocumentID,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(1000)) VariableValue
FROM		ProviderAnalysis.dbo.DocumentVariables
WHERE		DocumentVariables.VariableID IN (13320,13043,13053,13765,13238) 
--13320 = Risk & Compliance | 13043 = AccountLogic Plus | 13053 = IDLogic | 13765 = RiskLogic | 13238 = TrackingID

