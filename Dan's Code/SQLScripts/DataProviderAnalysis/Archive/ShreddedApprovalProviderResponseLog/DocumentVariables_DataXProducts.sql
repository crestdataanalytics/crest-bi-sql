SET Transaction Isolation Level Read Uncommitted;

DECLARE @startDate date,
		@endDate date
SET		@startDate = '1/01/2016';
SET		@endDate = '1/01/2017';

--7789 = IDV | 6355 = BAV | 7414 = CRAInquirySegment | 8059 = DebitReportSegment | 8091 = IDAIDSASegment

WITH
		DocumentIDs AS
		(
			SELECT		DocumentInfos.DocumentID,
						DocumentInfos.ProviderID,
						DocumentInfos.SubjectID,
						DocumentInfos.GenerationDate
			FROM		DocumentInfos
			WHERE		DocumentInfos.GenerationDate >= @startDate
						AND DocumentInfos.GenerationDate < @endDate
						AND DocumentInfos.ProviderID = 3
		),

		IDV AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS IDVVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS IDVVariableValue,
						'0.25' AS IDVPrice
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 7789
		),

		BAV AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS BAVVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS BAVVariableValue,
						'0.30' AS BAVPrice  --.05 per no hit
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 6355
		),

		CRAInquiry AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS CRAInquiryVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS CRAInquiryVariableValue,
						'0.80' AS CRAInquiryPrice  
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 7414
		),

		DebitReport AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS DebitReportVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS DebitReportVariableValue,
						'0.65' AS DebitReportPrice  
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 8059
		),

		IDAIDSA AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS IDAIDSAVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS IDAIDSAVariableValue,
						'0.65' AS IDAIDSAPrice  
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 8091
		)


SELECT		DocumentIDs.DocumentID,
			DocumentIDs.GenerationDate,
			DocumentIDs.ProviderID,
			DocumentIDs.SubjectID AS ApplicantID,
			IDV.IDVPrice,
			IDV.IDVVariableID,
			IDV.IDVVariableValue,
			BAV.BAVPrice,
			--CASE WHEN BAV.BAVPrice IS NULL THEN '.05' ELSE BAV.BAVPrice END AS BAVPrice,
			BAV.BAVVariableID,
			BAV.BAVVariableValue,
			CRAInquiry.CRAInquiryPrice,
			CRAInquiry.CRAInquiryVariableID,
			CRAInquiry.CRAInquiryVariableValue,
			IDAIDSA.IDAIDSAPrice,
			IDAIDSA.IDAIDSAVariableID,
			IDAIDSA.IDAIDSAVariableValue
FROM		DocumentIDs
LEFT JOIN	IDV ON IDV.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	BAV ON BAV.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	CRAInquiry ON CRAInquiry.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	DebitReport ON DebitReport.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	IDAIDSA ON IDAIDSA.DocumentID = DocumentIDs.DocumentID



