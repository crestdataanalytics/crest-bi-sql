SET Transaction Isolation Level Read Uncommitted;

DECLARE @startDate date,
		@endDate date
SET		@startDate = '1/01/2009';
SET		@endDate = '1/01/2016';

--need to find value that is populated the whole way through or combine variables.
WITH
		DocumentIDs AS
		(
			SELECT		DocumentInfos.DocumentID,
						DocumentInfos.ProviderID,
						DocumentInfos.SubjectID,
						DocumentInfos.GenerationDate
			FROM		DocumentInfos
			WHERE		DocumentInfos.GenerationDate >= @startDate
						AND DocumentInfos.GenerationDate < @endDate
						AND DocumentInfos.ProviderID = 8
		),

		FraudVerify AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS FraudVerifyVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS FraudVerifyVariableValue,
						'0.39' AS FraudVerifyPrice
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 11965
		),

		iPredict AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS iPredictVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS iPredictVariableValue,
						'0.54' AS iPredictPrice  
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 12082
		),

		BAVmicrobilt AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS BAVmicrobiltVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS BAVmicrobiltVariableValue,
						'0.60' AS BAVmicrobiltPrice  
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 12010
		),

		[UID] AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS UIDVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS UIDVariableValue  
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 12027
		)


SELECT		DocumentIDs.DocumentID,
			DocumentIDs.GenerationDate,
			DocumentIDs.ProviderID,
			DocumentIDs.SubjectID AS ApplicantID,
			FraudVerify.FraudVerifyPrice,
			FraudVerify.FraudVerifyVariableID,
			FraudVerify.FraudVerifyVariableValue,
			iPredict.iPredictPrice,
			iPredict.iPredictVariableID,
			iPredict.iPredictVariableValue,
			BAVmicrobilt.BAVmicrobiltPrice,
			BAVmicrobilt.BAVmicrobiltVariableID,
			BAVmicrobilt.BAVmicrobiltVariableValue,
			[UID].UIDVariableID,
			[UID].UIDVariableValue
FROM		DocumentIDs
LEFT JOIN	FraudVerify ON FraudVerify.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	iPredict ON iPredict.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	BAVmicrobilt ON BAVmicrobilt.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	[UID] ON [UID].DocumentID = DocumentIDs.DocumentID

--microbilt 
--11965 - IDV (FraudVerify) 0.39| 12082 - iPredict PRBC 0.54| 12010 - BAVmicrobilt 0.60 | UID - 12027

