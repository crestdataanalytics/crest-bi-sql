SET Transaction Isolation Level Read Uncommitted;
Declare @startDate date,
		@endDate date
SET @startDate = '12/5/2015';
SET	@endDate = '1/01/2016';


SELECT		DocumentVariables.DocumentID,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(1000)) VariableValue,
			DocumentInfos.GenerationDate,
			DocumentInfos.ProviderID,
			DocumentInfos.SubjectID AS ApplicantID
FROM		ProviderAnalysis.dbo.DocumentVariables
INNER JOIN	ProviderAnalysis.dbo.DocumentInfos ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
WHERE		DocumentVariables.VariableID IN (44855,1811) -- 44855 cbb score | 1811 tracking id
			AND DocumentInfos.GenerationDate >= @startDate
			AND DocumentInfos.GenerationDate < @endDate
			AND ProviderID = 7
ORDER BY	GenerationDate DESC


select top 1 *
from ProviderAnalysis.dbo.Variables
where VariableName like '%xml-response/tracking-number%'