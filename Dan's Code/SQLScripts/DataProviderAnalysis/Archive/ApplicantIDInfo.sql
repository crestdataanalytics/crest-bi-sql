SET Transaction Isolation Level Read Uncommitted;

SELECT		Addresses.ID AS AddressID,
			ApplicantPhone.ID AS PhoneID,
			ApplicantJob.ID AS ApplicantJobID,
			ApplicantEmailAddresses.ID AS ApplicantEmailAddressID,
			ApplicantBankAccounts.BankAccountID AS ApplicantBankAccountID,
			Applicants.ID AS ApplicantID,
			CAST(DateOfBirth AS DATE) DateOfBirth,
			CAST(Applicants.SocialSecurityNumber AS VARCHAR(20)) AS SSN, 
			REPLACE(Applicants.FirstName,',','') AS NameFirst, 
			REPLACE(Applicants.MiddleInitial,',','') AS NameMiddle,
			REPLACE(Applicants.LastName,',','') AS NameLast,
			REPLACE(REPLACE(Addresses.StreetLine1, ',', ''), '*','') as Street1, 
			REPLACE(REPLACE(Addresses.StreetLine2, ',', ''), '*','') as Street2,
			REPLACE(Addresses.City, ',','') AS City,
			Addresses.StateID as [State], 
			Addresses.PostalCode as Zip, 
			ApplicantPhone.PhoneNumber AS Phone,
			CASE WHEN ApplicantPhone.PhoneNumber = Applicants.CellPhone THEN 1 ELSE 0 END AS IsPhoneCell,
			ApplicantJob.PhoneNumber as PhoneWork, 
			REPLACE(ApplicantEmailAddresses.EmailAddress,',','') as Email, 
			cast(Applicants.DateOfBirth as date) as DOB,
			'' as IPAddress, 
			REPLACE(Applicants.DriversLicenseNumber,',','') as DriverLicenseNumber, 
			Applicants.DriversLicenseStateID as DriverLicenseState,
			cast(ApplicantJob.HireDate as date) as HireDate, 
			'' as BankName,
			CAST(BankAccounts.RoutingNumber AS VARCHAR(50)) as BankABA, 
			CAST(BankAccounts.AccountNumber AS VARCHAR(100)) as BankAcctNumber, 
			ApplicantJob.EmployerName AS WorkName,
			ApplicantJob.MonthlyIncome
FROM		Applicants
LEFT JOIN	ApplicantAddresses ON Applicants.ID = ApplicantAddresses.ApplicantID
LEFT JOIN	Addresses ON Addresses.ID = ApplicantAddresses.AddressID
LEFT JOIN	ApplicantPhone ON ApplicantPhone.ApplicantID = Applicants.ID
LEFT JOIN	ApplicantJob ON ApplicantJob.ApplicantID = Applicants.ID
LEFT JOIN	ApplicantEmailAddresses ON ApplicantEmailAddresses.ApplicantID = Applicants.ID
LEFT JOIN	ApplicantBankAccounts ON ApplicantBankAccounts.ApplicantID = Applicants.ID
LEFT JOIN	BankAccounts ON BankAccounts.ID = ApplicantBankAccounts.BankAccountID
LEFT JOIN	Loans ON Applicants.ID = Loans.ApplicantID
WHERE		Loans.CreationDate >= '2016-05-25'
