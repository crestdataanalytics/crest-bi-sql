SELECT		Distinct Top 5000 Applicants.ID as ApplicantID, 
			Applicants.EmailAddress, 
			Applicants.FirstName,
			Applicants.LastName,
			vw_loans_reporting.CreationDate,
			CASE WHEN ReturnedPayments.IsReturned = 1 THEN 1 ELSE 0 END AS FirstPaymentDefault,
			CASE WHEN LoanStatuses.[Status] != 'Cancelled' AND vw_loans_reporting.FundedDate IS NOT NULL THEN 1 ElSE 0 END AS IsFunded,
			CASE WHEN vw_loans_reporting.ChargedOffDate IS NOT NULL AND LoanStatuses.[Status] = 'Charged Off' THEN 1 ELSE 0 END AS IsChargedOff
FROM		Applicants
INNER JOIN	vw_loans_reporting ON vw_loans_reporting.ApplicantID = Applicants.ID
INNER JOIN	LoanStatuses ON LoanStatuses.ID = vw_loans_reporting.StatusID
LEFT JOIN	(SELECT		LoanPayments.LoanID, sum(LoanPayments.Amount) AS TotalAmountPaid, MIN(PaymentDate) AS FirstPaymentDate, 
						MIN(LoanPayments.ID) as PaymentID
			FROM		LoanPayments
			INNER JOIN	LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
			WHERE		PaymentDate is not null and IsDeleted = 0
						AND LoanPaymentTypes.[Type] NOT IN ('Cash','Initial Payment', 'Paid By Dealer','Refund')
			GROUP BY	LoanID) AmountPaid on vw_loans_reporting.ID = AmountPaid.LoanID		
LEFT JOIN	
			(SELECT PaymentID, processeddate,
					case	when ReturnCode IN ('R02','R05', 'R07', 'R08', 'R10', 'R16') then 1
							else 0 end as FirstPaymentFatalReturnIndicator,
					1 as IsReturned
			FROM	WellsFargoReturns	
			) ReturnedPayments on ReturnedPayments.PaymentID = AmountPaid.PaymentID
WHERE		vw_loans_reporting.CreationDate <= '05/31/2015'
			AND vw_loans_reporting.CreationDate >= '02/10/2015'
			AND Applicants.SocialSecurityNumber IS NOT NULL
			AND Applicants.SocialSecurityNumber NOT LIKE '%[a-z]%'
			AND Applicants.SocialSecurityNumber NOT LIKE '%/%'
			AND Applicants.SocialSecurityNumber NOT LIKE '%+%'
			AND (RIGHT(CAST(Applicants.SocialSecurityNumber AS INT), 2)) <= 5
			AND vw_loans_reporting.DisplayID like '%-1'
ORDER BY	vw_loans_reporting.CreationDate DESC
