
	select vw_loans_reporting.DisplayID, ApprovalProviderSettings.ProviderName, 
		cast(ApprovalProviderResponseLog.CreatedDate as date) as CreatedDate, 
		ApprovalProviderResponseLog.Decision, cast(ApprovalProviderResponseLog.RiskScore as int) as RiskScore, 
		LoanStatuses.Status as LoanStatus, vw_loans_reporting.Amount as FundedAmount,	cast(vw_loans_reporting.FundedDate as date) as FundedDate, 
		cast(vw_loans_reporting.CreationDate as date) as CreationDate, cast(vw_loans_reporting.ApprovalDate as date) as ApprovalDate, 
		vw_loans_reporting.FundedAmount as AmountToRetailer, vw_loans_reporting.TotalNote, 
		cast(vw_loans_reporting.ChargedOffDate as date) as ChargedOffDate,
		cast(FirstPayment.FirstPaymentDate as date) as FirstPaymentDate, cast(DueDate.FirstDueDate as date) as FirstDueDate,
		case when
			FirstPayment.FirstPaymentDate <= getdate() and FirstPayment.FirstPaymentDate is not null then 1 else 0 end as HasFirstPaymentDue,
		case when
			(DueDate.FirstDueDate >= FirstPayment.FirstPaymentDate) and (DueDate.FirstDueDate is not null) and 
			(FirstPayment.FirstPaymentDate is not null) then 1 else 0 end as FirstPaymentOnTime,
		case when
			(DueDate.FirstDueDate < FirstPayment.FirstPaymentDate) and (DueDate.FirstDueDate is not null) and 
			(FirstPayment.FirstPaymentDate is not null) then 1 else 0 end as FirstPaymentLate,
		case when 
			vw_loans_reporting.AutoApprovedFlag = 1 then 1 else 0 end as AutoApprovedFlag, 
		Fees.TotalNSFFeesAmount, LoanStatuses.[Status],
		case
			when LoanStatuses.[Status] in ('Bounced', 'Charged Off') then 1 else 0 end as IsDelinquent
	--select count(vw_loans_reporting.DisplayID)
	from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog	
	inner join vw_loans_reporting on ApprovalProviderResponseLog.applicantid = vw_loans_reporting.applicantid
	inner join ApprovalProviderSettings on	ApprovalProviderResponseLog.ApprovalProviderSettingsID = ApprovalProviderSettings.ID
	inner join LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
	left join 
			(select count(LoanID) as countLoanID, LoanID, sum(Amount) TotalNSFFeesAmount
			from LoanPaymentFeesOwed
			where ID is not null and FeeTypeID = 2 and WaivedDate is null
			group by LoanID) Fees
			on vw_loans_reporting.ID = Fees.LoanID
	left join 
			(SELECT  LoanID , MIN(PaymentDate) AS FirstPaymentDate
			FROM    LoanPayments
			INNER JOIN LoanPaymentTypes on LoanPaymentTypes.ID = LoanPayments.TypeID
			WHERE   Returned = 0 AND DeletedDate IS NULL and LoanPaymentTypes.Type != 'Initial Payment'
			GROUP BY LoanID) FirstPayment
			on FirstPayment.LoanID = vw_loans_reporting.ID
	left join 
			(SELECT  LoanID , MIN(DueDate) AS FirstDueDate
			FROM    LoanPaymentSchedule
			WHERE ScheduleEntryDesc != 'Initial Payment'
			GROUP BY LoanID) DueDate
			on DueDate.LoanID = vw_loans_reporting.ID
	where approvalprovidersettingsid = 6 and (cast(ApprovalProviderResponseLog.createddate as date) between '02/26/2015' and getdate())
		and (cast(vw_loans_reporting.CreationDate as date) between '02/26/2015' and getdate())
		--and riskscore >= 993 
		--and vw_loans_reporting.DisplayID = '873285-1'	
		--and FundedDate is not null
		--and vw_loans_reporting.chargedoffdate is not null
		--and month(ApprovalProviderResponseLog.createddate) = 2 and year(ApprovalProviderResponseLog.createddate) = 2015
		--and vw_loans_reporting.AutoApprovedFlag =1
		--and ApprovalProviderResponseLog.Decision = 'Approved'
		--and vw_loans_reporting.FundedDate is not null
		--and ApprovalProviderResponseLog.Decision = 'Denied'
		--and vw_loans_reporting.DisplayID = '865252-1'
		--and ApprovalProviderResponseLog.CreatedDate >= vw_loans_reporting.CreationDate
		--and aprl.Decision = 'Approved' and l.approvaldate >= aprl.createddate and l.AutoApprovedFlag = 1 and l.fundeddate is not null
		--and l.statusid = 4 and (lpfo.id is null or (lpfo.id is not null and lpfo.feetypeid !=2)) 
		--and riskscore > 35
		--and LoanStatuses.status = 'Funded'
	order by vw_loans_reporting.ApplicantID

	select count(vw_loans_reporting.DisplayID)--, vw_loans_reporting.creationDate,ApprovalProviderResponseLog.CreatedDate
	from vw_loans_reporting
	inner join [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog 
		on ApprovalProviderResponseLog.applicantid = vw_loans_reporting.applicantid
	where approvalprovidersettingsid = 6 and vw_loans_reporting.CreationDate between '02/26/2015' and getdate()
		and ApprovalProviderResponseLog.CreatedDate between '02/26/2015' and getdate()
	--and (cast(vw_loans_reporting.CreationDate as date) between '02/26/2015' and getdate())
	--order by displayid