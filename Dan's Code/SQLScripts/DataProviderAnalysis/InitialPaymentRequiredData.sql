WITH FundedLeases AS
(
	SELECT		vw_loans_reporting.ID AS LoanID,
				vw_loans_reporting.CreationDate,
				vw_loans_reporting.FundedDate,
				vw_loans_reporting.FundedAmount,
				vw_loans_reporting.DealerID AS RetailerID
	FROM		vw_loans_reporting
	INNER JOIN	Dealers ON Dealers.ID = vw_loans_reporting.DealerID
	WHERE		Dealers.IsDemoAccount = 0
				AND vw_loans_reporting.CreationDate >= '2015-06-01'
				AND vw_loans_reporting.FundedDate IS NOT NULL
				AND vw_loans_reporting.FundedAmount > 0		
				AND vw_loans_reporting.StatusID NOT IN (10,20)	
				AND vw_loans_reporting.LoanOwnerID != 3				
),

InitialPaymentLoanIDs AS
(
	SELECT		LoanID,
				LoanPaymentTypes.[Type],
				1 AS InitialPaymentFlag
	FROM		vw_LoanPayments
	INNER JOIN	LoanPaymentTypes ON LoanPaymentTypes.ID = vw_LoanPayments.TypeID
	WHERE		LoanPaymentTypes.[Type] = 'Initial Payment'
),

FPDandEligibility AS
(	
	  SELECT		FirstScheduledPaymentDetails.LoanID,
					FirstScheduledPaymentOutcome.IsFinalOutcome AS CrestFPD
	  FROM			FirstScheduledPaymentDetails
	  INNER JOIN	FirstScheduledPaymentOutcome ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
	  INNER JOIN	Loans ON Loans.ID = FirstScheduledPaymentDetails.LoanID
	  Where			FirstScheduledPaymentOutcome.IsFinalOutcome = 1 and FirstScheduledPaymentDetails.IsCurrentOutcome = 1 AND FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID IN (3,4,5)
)

SELECT		DISTINCT FundedLeases.LoanID,
			FundedLeases.RetailerID,
			FundedLeases.CreationDate,
			FundedLeases.FundedDate,
			CAST(DATEADD(m, DATEDIFF(m, 0, FundedLeases.FundedDate), 0) AS Date) AS FundedVintage,
			InitialPaymentLoanIDs.InitialPaymentFlag,
			FPDandEligibility.CrestFPD,
			FundedLeases.FundedAmount,
			CASE WHEN InitialPaymentLoanIDs.InitialPaymentFlag = 1 THEN FundedLeases.FundedAmount END AS IPRAmount,
			CASE WHEN FPDandEligibility.CrestFPD = 1 THEN FundedLeases.FundedAmount END AS CrestFPDAmount
FROM		FundedLeases
LEFT JOIN	InitialPaymentLoanIDs ON InitialPaymentLoanIDs.LoanID = FundedLeases.LoanID
LEFT JOIN	FPDandEligibility ON FPDandEligibility.LoanID = FundedLeases.LoanID
