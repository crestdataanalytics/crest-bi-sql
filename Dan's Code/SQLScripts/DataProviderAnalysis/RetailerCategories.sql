SET Transaction Isolation Level Read Uncommitted;


SELECT		DealerCategories.DealerID AS RetailerID,
			Categories.CategoryName,
			DealerCategories.RankOrder
FROM		DealerCategories
INNER JOIN	Categories ON Categories.ID = DealerCategories.CategoryID