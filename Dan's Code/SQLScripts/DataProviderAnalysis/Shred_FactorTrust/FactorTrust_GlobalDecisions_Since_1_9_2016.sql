
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2015-01-01'
				AND DocumentInfos.ProviderID = 9
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(150)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID = 13054 --IN (13054,13405,13411,39193,13375,22183)



--ProviderID	ProviderName	VariableID	VariableName
--9	FactorTrust	13405	/ApplicationResponse/ApplicationInfo/ResponseCodes/Response[2]/Description
--9	FactorTrust	13411	/ApplicationResponse/ApplicationInfo/ResponseCodes/Response[1]/Description
--9	FactorTrust	39193	/ApplicationResponse/ApplicationInfo/ResponseCodes/Response[4]/Description
--9	FactorTrust	13375	/ApplicationResponse/ApplicationInfo/ResponseCodes/Response/Description
--9	FactorTrust	22183	/ApplicationResponse/ApplicationInfo/ResponseCodes/Response[3]/Description
--9	FactorTrust	13054	/ApplicationResponse/ApplicationInfo/TransactionStatus