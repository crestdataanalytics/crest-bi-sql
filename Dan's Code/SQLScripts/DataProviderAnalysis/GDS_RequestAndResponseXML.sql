SET Transaction Isolation Level Read Uncommitted;

--gets the response xml
SELECT		ID AS ApprovalProviderResponseLogID,
			ApprovalProviderSettingsID,
			ApplicantID,
			CreatedDate,
			ResponseXML
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		CreatedDate >= '2016-9-01'
			AND ApprovalProviderSettingsID IN (1,2,6)


--gets the request xml
SELECT		ID AS ApprovalProviderResponseLogID,
			ApprovalProviderSettingsID,
			ApplicantID,
			CreatedDate,
			RequestXML
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		CreatedDate >= '2016-9-01'
			AND ApprovalProviderSettingsID = 2
