SET Transaction Isolation Level Read Uncommitted;

SELECT	WellsFargoReturns.LoanID,
		WellsFargoReturns.ID,
		WellsFargoReturns.PaymentID,
		WellsFargoReturns.ReturnCode,
		CASE	WHEN	WellsFargoReturns.ReturnCode IN ('R02','R05', 'R07', 'R08', 'R10', 'R16') THEN 1
		ELSE	0 
		END AS	FirstPaymentFatalReturnIndicator
FROM	WellsFargoReturns
INNER JOIN Loans on Loans.ID = WellsFargoReturns.LoanID
WHERE	Loans.LoanOwnerID IN (1,2,4,5,7)
--WHERE	PaymentID = 491003