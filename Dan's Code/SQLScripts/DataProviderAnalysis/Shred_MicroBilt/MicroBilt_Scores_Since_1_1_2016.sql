
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-01-01'
				AND DocumentInfos.ProviderID = 8
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(50)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID IN (12018,12099,12031)

--ProviderID	ProviderName	VariableID	VariableName
--8	MicroBuilt	12018	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/SCORES/score[1]
--8	MicroBuilt	12099	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/SCORES/score[2]
--8	MicroBuilt	12031	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/SCORES/score