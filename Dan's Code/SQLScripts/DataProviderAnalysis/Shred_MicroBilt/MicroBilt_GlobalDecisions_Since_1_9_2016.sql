
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2015-01-01'
				AND DocumentInfos.ProviderID = 8
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(50)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableID = 11954 --IN (39712,12197,39715,39719,12150,12206,39713,39717,39721,11954)



--ProviderID	ProviderName	VariableID	VariableName
--8	MicroBuilt	39712	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[4]
--8	MicroBuilt	12197	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[2]
--8	MicroBuilt	39715	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[5]
--8	MicroBuilt	39719	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[8]
--8	MicroBuilt	12150	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason
--8	MicroBuilt	12206	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[1]
--8	MicroBuilt	39713	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[3]
--8	MicroBuilt	39717	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[6]
--8	MicroBuilt	39721	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/REASONS/reason[7]
--8	MicroBuilt	11954	/MBCLVRs_Type/RESPONSE/CONTENT/DECISION/decision