

WITH P AS
(
	SELECT
		LoanID,
		SUM(ISNULL(RentAmount,0))+SUM(ISNULL(FeeAmount,0)) as Paid
	FROM
		vw_loanPayments
	WHERE
		ReturnDate is null
		and DeletedDate is null
		and PaymentDate <= @maxDate
	GROUP BY
		LoanID
),
FP AS
(
	SELECT
		distinct LoanID,
		min(PaymentDate) as FirstPayDate
	FROM
		(SELECT
			LoanID,
			PaymentDate,
			SUM(ISNULL(RentAmount,0))+SUM(ISNULL(FeeAmount,0)) as Paid
		FROM
			vw_loanPayments
		WHERE
			ReturnDate is null
			and DeletedDate is null
			and TypeID not in (6,9,10)
			and PaymentDate <= @maxDate
		GROUP BY
			LoanID,
			PaymentDate) as A
	GROUP BY
		LoanID
),
FDD AS
(
		SELECT
			LoanID,
			min(DueDate) as FirstDueDate
		FROM
			VW_LoanPaymentSchedule
		WHERE
			DueAmount > 0
			and DueAmount is not null
			and ScheduleEntryDesc not in ('Initial Payment')
		GROUP BY
			LoanID

)
,L AS
(
	SELECT
		L.ID as LoanID,
		L.DealerID,
		L.FundedAmount,
		L.FundedDate,
		datediff(dd,L.FundedDate,FDD.FirstDueDate) as DaysToFirstDueDate,
		P.Paid as TotalPaid,
		coalesce(P.Paid/L.FundedAmount,0) as Multiple,
		case when FP.FirstPayDate is null then 0 else FP.FirstPayDate end as FirstPayDate,
		case when L.StatusID in (12,13,14) then 1 else 0 end as ChargeOff,
		case when FP.FirstPayDate is null then L.FundedAmount else 0 end as NoPayAmount,
		case when FP.FirstPayDate is null then 1 else 0 end as NoPayFlag,
		datediff(dd,FDD.FirstDueDate,getdate()) as DaysSinceFirstDueDate
	FROM
		Loans as L
		LEFT JOIN Dealers as D
			on L.ID = D.ID and D.IsDemoAccount = 0
		LEFT JOIN P	
			on P.LoanID = L.ID
		LEFT JOIN FP
			on FP.LoanID = L.ID
		LEFT JOIN FDD
			on FDD.LoanID = L.ID
	WHERE
		datediff(mm,L.FundedDate,Getdate())<=18
		and datediff(dd,FDD.FirstDueDate,getdate())>=7
		and FDD.FirstDueDate is not null
		and L.StatusID not in (10,20,21)
		and L.FundedAmount > 0
		and L.FundedDate is not null
		and L.CreationDate <= @maxDate
)
,CO as
(
	SELECT
		DealerID,
		SUM(FundedAmount) as TotalFunded,
		SUM(TotalPaid) as TotalPaid,
		SUM(case when ChargeOff = 1 then FundedAmount end)/nullif(SUM(FundedAmount),0) as ChargeOffPercent,
		SUM(case when ChargeOff = 1 and DaysSinceFirstDueDate>120 then TotalPaid end)/nullif(SUM(Case when ChargeOff = 1 and DaysSinceFirstDueDate>120 then FundedAmount end),0) as ChargeOffMultiple,
		SUM(case when ChargeOff = 1 and NoPayFlag = 1 and DaysSinceFirstDueDate>120 then FundedAmount end)/
			nullif(SUM(Case when ChargeOff in (1,0) and DaysSinceFirstDueDate>120  then FundedAmount end),0) as ChargeOffNoPay,
		SUM(case when NoPayFlag = 1 and (DaysSinceFirstDueDate between 15 and 44) then FundedAmount end)/
			nullif(SUM(Case when (DaysSinceFirstDueDate between 15 and 44)  then FundedAmount end),0) as NPO,
		SUM(case when NoPayFlag = 1 and (DaysSinceFirstDueDate between 45 and 89) then FundedAmount end)/
			nullif(SUM(Case when (DaysSinceFirstDueDate between 45 and 89)  then FundedAmount end),0) as NPT,
		SUM(case when NoPayFlag = 1 and (DaysSinceFirstDueDate between 90 and 120) then FundedAmount end)/
			nullif(SUM(Case when (DaysSinceFirstDueDate between 90 and 120)  then FundedAmount end),0) as NPTR,
		SUM(case when NoPayFlag = 1 and (DaysSinceFirstDueDate between 15 and 120) then FundedAmount end)/
			nullif(SUM(Case when (DaysSinceFirstDueDate between 15 and 120)  then FundedAmount end),0) as NPOverall,
		(SUM(case when (DaysSinceFirstDueDate between 15 and 44) then FundedAmount end)/
			SUM(case when (DaysSinceFirstDueDate between 15 and 120) then FundedAmount end))*10 as NPOF,
		(SUM(case when (DaysSinceFirstDueDate between 45 and 89) then FundedAmount end)/
			SUM(case when (DaysSinceFirstDueDate between 15 and 120) then FundedAmount end))*10 as NPTF,
		(SUM(case when (DaysSinceFirstDueDate between 90 and 120) then FundedAmount end)/
			SUM(case when (DaysSinceFirstDueDate between 15 and 120) then FundedAmount end))*10 as NPTRF
	FROM
		L
	GROUP BY
		DealerID
),
DIST AS
(
	SELECT
		DealerID,
		TotalFunded,
		TotalPaid,
		NPO,
		NPT,
		NPTR,
		NPOverall,
		(NPOF/(NPOF+NPTF+NPTRF+5))*10 as NPOF,
		(NPTF/(NPOF+NPTF+NPTRF+5))*10 as NPTF,
		(NPTRF/(NPOF+NPTF+NPTRF+5))*10 as NPTRF,
		(5/(NPOF+NPTF+NPTRF+5))*10 as NPOverallF
	FROM
		CO
),
SCORE AS
(
	SELECT
		DealerID,
		TotalFunded,
		TotalPaid,
		(coalesce(NPOverall,0)/.1)*NPOverallF as NPOverall,
		(coalesce(NPO,0)/.1)*NPOF as NPO,
		(coalesce(NPT,0)/.1)*NPTF as NPT,
		(coalesce(NPTR,0)/.1)*NPTRF as NPTR
	FROM
		DIST
),
NORM AS
(
	SELECT
		DealerID,
		case when NPO>4 then 4 else NPO end as NPO,
		case when NPT>3 then 3 else NPT end as NPT,
		case when NPTR>3 then 3 else NPTR end as NPTR,
		case when NPOverall>5 then 5 end as NPOverall
	FROM
		SCORE
),
MethodOne AS
(
	SELECT
		DealerID,
		SUM(
			coalesce(NPO,0)
			+coalesce(NPT,0)
			+coalesce(NPTR,0)
			+coalesce(NPOverall,0)
			)/1 as FraudScore
	FROM
		NORM 
	GROUP BY
		DealerID
)


SELECT
	MO.DealerID,
	case when MO.FraudScore > 10 then 10 else MO.FraudScore end as FraudScore
FROM
	MethodOne as MO
