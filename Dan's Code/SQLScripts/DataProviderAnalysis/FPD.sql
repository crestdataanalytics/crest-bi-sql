
  SET Transaction Isolation Level Read Uncommitted;

  SELECT		FirstScheduledPaymentDetails.FirstScheduledPaymentDetailsID,
				FirstScheduledPaymentDetails.LoanID,
				Loans.DisplayID,
				FirstScheduledPaymentDetails.EvaluationDate,
				FirstScheduledPaymentDetails.FirstScheduledPaymentDueDate,
				FirstScheduledPaymentDetails.FirstScheduledAmount,
				FirstScheduledPaymentDetails.NonInitialAmountPaidToDate,
				FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID,
				FirstScheduledPaymentOutcome.OutcomeName,
				FirstScheduledPaymentOutcome.IsFinalOutcome AS CrestFPD,
				FirstScheduledPaymentDetails.IsCurrentOutcome
  FROM			FirstScheduledPaymentDetails
  INNER JOIN	FirstScheduledPaymentOutcome ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
  INNER JOIN	Loans ON Loans.ID = FirstScheduledPaymentDetails.LoanID
  Where			FirstScheduledPaymentOutcome.IsFinalOutcome = 1 and FirstScheduledPaymentDetails.IsCurrentOutcome = 1
				AND Loans.LoanOwnerID IN (1,2,4,5,7)
	 --Loans.ID = 1574484
  --order by LoanID

  --select creationdate from loans where loans.id = 1574484

  --select		count(FirstScheduledPaymentOutcome.OutcomeName),FirstScheduledPaymentOutcome.OutcomeName
  --FROM			FirstScheduledPaymentDetails
  --INNER JOIN	FirstScheduledPaymentOutcome ON FirstScheduledPaymentDetails.FirstScheduledPaymentOutcomeID = FirstScheduledPaymentOutcome.FirstScheduledPaymentOutcomeID
  --INNER JOIN	Loans ON Loans.ID = FirstScheduledPaymentDetails.LoanID
  --Where			FirstScheduledPaymentOutcome.IsFinalOutcome = 1 and 
  --FirstScheduledPaymentDetails.IsCurrentOutcome = 1
  --group by FirstScheduledPaymentOutcome.OutcomeName