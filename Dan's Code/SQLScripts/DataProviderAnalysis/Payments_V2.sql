SET Transaction Isolation Level Read Uncommitted;

SELECT		LoanPayments.LoanID,
			LoanPayments.Amount, 
			LoanPayments.PaymentDate,
			LoanPayments.ID AS PaymentID, 
			LoanPayments.ReturnDate,
			CASE	WHEN	LoanPayments.ReturnDate IS NOT NULL THEN 1
					ELSE	0 
					END AS	IsReturned,
			LoanPaymentTypes.[Type],
			LoanPayments.DeletedDate,
			LoanPayments.BankAccountID,
			LoanPayments.CreditCardID									
FROM		LoanPayments
INNER JOIN	LoanPaymentTypes ON LoanPayments.TypeID = LoanPaymentTypes.ID
INNER JOIN	Loans ON Loans.ID = LoanPayments.LoanID 
WHERE		Loans.LoanOwnerID != 3
--WHERE		PaymentDate is not null 
--			AND LoanPayments.DeletedDate IS NULL
--			AND LoanPaymentTypes.[Type] NOT IN ('Initial Payment','Paid By Dealer','Refund')
			--AND LoanPayments.PaymentDate >= @startDate 
			--AND LoanPayments.PaymentDate < @endDate