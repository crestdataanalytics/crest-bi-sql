SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

With LeaseData as
(
	SELECT
		ApplicantID,
		DisplayID,
		ID as LeaseID,
		StatusID,
		CASE WHEN datediff(dd,FundedDate,PaidOffDate) <= 90 then 'Yes' else 'No' end as SAC,
		ROW_NUMBER() OVER (PARTITION BY ApplicantID ORDER BY CreationDate DESC) AS LeaseSequence
	FROM
		Loans
	WHERE
		CreationDate > '6-29-16'
),
C as
(
	SELECT
		ID AS ApprovalProviderResponseLogID,
		--ResponseXML,
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovalScore)')) as CBScore,
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApplicantID)')) as ApplicantID,
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovedAmount)')) as ApprovedAmount,
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/Factor)')) as Factor,
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovalProviderResultType)')) as Response,
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ModelNode)')) as Node,
		cast(CreatedDate as date) as SubmitDate,
		ROW_NUMBER() OVER (PARTITION BY ApplicantID ORDER BY CreatedDate DESC) AS DataSequence
	FROM crestwarehouse.dbo.approvalproviderresponselog 
	WHERE
		cast(createddate as date) > '6-29-16' and approvalprovidersettingsid = 16 and
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovalScore)')) <> '' and
		convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/Factor)')) not in ('0.00','1.00')
)

SELECT
	LeaseData.*,
	(CASE 
        WHEN C.node = 1 THEN 1.00 
        WHEN C.node = 2 THEN 1.00 
        WHEN C.node = 3 THEN 1.00 
        WHEN C.node = 4 THEN 0.10 
        WHEN C.node = 5 THEN 0.35 
        WHEN C.node = 6 THEN 1.00 
        WHEN C.node = 7 THEN 0.15 
        WHEN C.node = 8 THEN 0.47 
        WHEN C.node = 9 THEN 1.00 
        WHEN C.node = 10 THEN 0.29 
        WHEN C.node = 11 THEN 0.10 
        WHEN C.node = 12 THEN 0.13 
        WHEN C.node = 13 THEN 0.10 
        WHEN C.node = 14 THEN 0.20 
        WHEN C.node = 15 THEN 0.59 
        WHEN C.node = 16 THEN 1.00 
        WHEN C.node = 17 THEN 0.31 
        WHEN C.node = 18 THEN 0.96 
        WHEN C.node = 19 THEN 1.00 
        WHEN C.node = 20 THEN 1.00 
        WHEN C.node = 21 THEN 1.00 
        WHEN C.node = 22 THEN 1.00 
        WHEN C.node = 23 THEN 1.00 
        WHEN C.node = 24 THEN 0.98 
        WHEN C.node = 25 THEN 0.10 
        WHEN C.node = 26 THEN 0.93 
        WHEN C.node = 27 THEN 0.99 
        WHEN C.node = 28 THEN 0.16 
        WHEN C.node = 29 THEN 0.11 
        WHEN C.node = 30 THEN 0.88 
        WHEN C.node = 31 THEN 0.14 
        WHEN C.node = 32 THEN 0.12 
    END)*100 as NewScore,
	C.*
FROM
	LeaseData join C on LeaseData.ApplicantID = C.ApplicantID and LeaseData.LeaseSequence = 1 and C.DataSequence = 1
--WHERE
	--LeaseData.StatusID in (4,5,6,12,13,14,16,22)

/*
SELECT top 100
	L.*
FROM
(SELECT
		ApplicantID,
		DisplayID,
		ID as LeaseID,
		StatusID,
		CASE WHEN datediff(dd,FundedDate,PaidOffDate) <= 90 then 'Yes' else 'No' end as SAC,
		ROW_NUMBER() OVER (PARTITION BY ApplicantID ORDER BY CreationDate DESC) AS LeaseSequence
	FROM
		Loans) as L
WHERE L.LeaseSequence =1
*/