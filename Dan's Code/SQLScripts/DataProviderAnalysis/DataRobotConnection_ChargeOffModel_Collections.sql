WITH P AS
(
	SELECT
		LoanID,
		cast(PaymentDate as Date) as PaymentDate,
		cast(ReturnDate as Date) as ReturnDate,
		SUM(ISNULL(RentAmount,0))+SUM(ISNULL(FeeAmount,0)) as TotalPaid
	FROM
		vw_LoanPayments
	WHERE
		DeletedDate is null
		and TypeID <> 9
	GROUP BY
		LoanID,
		cast(PaymentDate as Date),
		cast(ReturnDate as Date)
),
APP AS
(
	SELECT
		A.ID as ApplicantID,
		datediff(yy,A.DateOfBirth,getdate()) as Age,
		AD.City,
		AD.PostalCode,
		AD.StateID as State
	FROM
		Applicants as A
		LEFT JOIN Addresses as AD
			on A.AddressID = AD.ID
),
PAY AS
(
	SELECT
		LoanID,
		cast(PaymentDate as date) as PayDate,
		cast(ReturnDate as Date) as ReturnDate,
		ROW_NUMBER() OVER(PARTITION BY LoanID ORDER BY ID ASC) as Rn
	FROM
		vw_loanPayments
	WHERE
		DeletedDate is null
		and typeID <> 9
),
PAYT AS
(
	SELECT
		LoanID,
		cast(PaymentDate as date) as PayDate,
		ROW_NUMBER() OVER(PARTITION BY LoanID ORDER BY ID ASC) as Rn
	FROM
		vw_loanPayments
	WHERE
		DeletedDate is null
		and ReturnDate is null
		and typeID <> 9
)

SELECT
	L.ID as LoanID,
	case when L.StatusID in (12,13,14) then 1 else 0 end as COflag,
	L.FundedAmount,
	nullif(L.FundedAmount/isnull(L.ApprovalAmount,0),0) as Utilization,
	APP.Age,
	APP.City,
	APP.PostalCode,
	APP.State,
	case when P.ReturnDate is null then 0
		when P.ReturnDate is not null and PAY.PayDate = PT.PayDate then 0
		else 1 end as FPD,
	count(case when datediff(dd,L.FundedDate,P.PaymentDate)<=30 and P.ReturnDate is not null then 1 else null end) as MonthOneReturnCount,
	count(case when datediff(dd,L.FundedDate,P.PaymentDate)<=30 and P.ReturnDate is null then 1 else null end) as MonthOneClearedCount,
	coalesce(sum(case when datediff(dd,L.FundedDate,P.PaymentDate)<=30 and P.ReturnDate is null then P.TotalPaid end),0) as MonthOnePaid
FROM
	Loans as L
	LEFT JOIN Dealers as D
		on D.ID = L.DealerID and D.IsDemoAccount = 0
	LEFT JOIN P
		on L.ID = P.LoanID
	LEFT JOIN APP
		on APP.ApplicantID = L.ApplicantID
	LEFT JOIN PAY
		on L.ID = PAY.LoanID and PAY.Rn = 1
	LEFT JOIN PAYT as PT
		on L.ID = PT.LoanID and PT.Rn = 1
WHERE
	L.FundedDate is not null
	and L.FundedAmount > 0
	and L.ApprovalAmount > 0
	and L.StatusID in (4,6)
	and L.LoanOwnerID IN (1,2,4,5,7)
	and L.FundedDate between dateadd(dd,-180,getdate()) and dateadd(dd,-30,getdate())
GROUP BY
	L.ID,
	case when L.StatusID in (12,13,14) then 1 else 0 end,
	L.FundedAmount,
	nullif(L.FundedAmount/isnull(L.ApprovalAmount,0),0),
	APP.Age,
	APP.City,
	APP.PostalCode,
	APP.State,
	case when P.ReturnDate is null then 0
		when P.ReturnDate is not null and PAY.PayDate = PT.PayDate then 0
		else 1 end