
WITH 

LeaseTotal AS
(
	SELECT		Loans.ID AS LoanID,
				Loans.DisplayID,
				Loans.ApplicantID	
	FROM		Loans
	INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
	WHERE		Loans.CreationDate >= '2016-05-01'
				AND Loans.CreationDate < '2016-06-01'
				AND NOT EXISTS(SELECT 1 FROM vw_loans_reporting ll WHERE ll.ApplicantID = Loans.ApplicantID AND ll.StatusID = 21)
				AND Dealers.IsDemoAccount = 0
),

ebTotal AS
(
	SELECT		ApplicantID AS ebApplicantID
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	WHERE		CreatedDate >= '2016-05-01'
				AND CreatedDate < '2016-06-01'
				AND ApprovalProviderSettingsID = 6
)

SELECT		*
FROM		LeaseTotal
LEFT JOIN	ebTotal ON ebTotal.ebApplicantID = LeaseTotal.ApplicantID
WHERE		ebApplicantID IS NULL
			--and displayid = '1238294-1'