
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2016-12-28'
				AND DocumentInfos.ProviderID = 8
),

TotalInquiries AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS TotalInquiries
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (12027)
),

BAV AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS BAV
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (12010)	
				AND DocumentVariables.VariableValue IS NOT NULL
),

FraudVerify AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS FraudVerify
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (11965)	
				AND DocumentVariables.VariableValue IS NOT NULL
),

iPredict AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS iPredict
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (12027)	
				AND DocumentVariables.VariableValue IS NOT NULL
)


SELECT		YEAR(DocumentIDs.GenerationDate) AS [Year],
			MONTH(DocumentIDs.GenerationDate) AS [Month],
			SUM(TotalInquiries.TotalInquiries) AS TotalInquiries,
			SUM(BAV.BAV) AS BAV,
			SUM(FraudVerify.FraudVerify) AS FraudVerify,
			SUM(iPredict.iPredict) AS iPredict
FROM		DocumentIDs
INNER JOIN	TotalInquiries ON TotalInquiries.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	BAV ON BAV.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	FraudVerify ON FraudVerify.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	iPredict ON iPredict.DocumentID = DocumentIDs.DocumentID
GROUP BY	YEAR(DocumentIDs.GenerationDate),
			MONTH(DocumentIDs.GenerationDate)
ORDER BY	1,2 ASC


--ProviderID	ProviderName	VariableID	VariableName
--8	MicroBuilt	12027	/MBCLVRs_Type/MsgRsHdr/RqUID | total inquiries and iPredict
--8	MicroBuilt	12010	/MBCLVRs_Type/RESPONSE/CONTENT/SERVICEDETAILS/BANK-INFORMATION/BANKACCOUNTSUMMARY/score | BAV
--8	MicroBuilt	11965	/MBCLVRs_Type/RESPONSE/CONTENT/SERVICEDETAILS/IDV/score | Fraud Verify