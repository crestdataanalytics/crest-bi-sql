
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2016-05-01'
				AND DocumentInfos.ProviderID = 7
),

TotalInquiries AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS TotalInquiries
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (1811)
),

ClearFraud AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS TotalClearFraud
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (9697)	
				AND DocumentVariables.VariableValue IS NOT NULL
),

ClearBankBehavior AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS TotalClearBankBehavior
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (44727)	
				AND DocumentVariables.VariableValue IS NOT NULL
),

ClearTradeline AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS TotalClearTradeline
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (9791)	
				AND DocumentVariables.VariableValue IS NOT NULL
)

SELECT		YEAR(DocumentIDs.GenerationDate) AS [Year],
			MONTH(DocumentIDs.GenerationDate) AS [Month],
			SUM(TotalInquiries.TotalInquiries) AS TotalInquiries,
			SUM(ClearFraud.TotalClearFraud) AS TotalClearFraud,
			SUM(ClearBankBehavior.TotalClearBankBehavior) AS TotalClearBankBehavior,
			SUM(ClearTradeline.TotalClearTradeline) AS TotalClearTradeline
FROM		DocumentIDs
INNER JOIN	TotalInquiries ON TotalInquiries.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearFraud ON ClearFraud.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearBankBehavior ON ClearBankBehavior.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearTradeline ON ClearTradeline.DocumentID = DocumentIDs.DocumentID
GROUP BY	YEAR(DocumentIDs.GenerationDate),
			MONTH(DocumentIDs.GenerationDate)
ORDER BY	1,2 ASC


--ProviderID	ProviderName	VariableID	VariableName
--7	Clarity	1811	/xml-response/tracking-number --tracking number
--7	Clarity	9697	/xml-response/clear-fraud/action --Clear-Fraud
--7	Clarity	44727	/xml-response/clear-bank-behavior/action --clear bank behavior
--7	Clarity	39485	/xml-response/inquiry/product-date --product date
--7	Clarity	9791	/xml-response/clear-tradeline/action  -- clear tradeline

