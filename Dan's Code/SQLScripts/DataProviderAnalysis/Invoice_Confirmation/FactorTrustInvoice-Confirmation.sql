
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2016-05-01'
				--AND DocumentInfos.GenerationDate < '2015-11-01'
				AND DocumentInfos.ProviderID = 9
),

TotalInquiries AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS TotalInquiries
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	WHERE		DocumentVariables.VariableID IN (13238)
),

ChexAdvisor AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS ChexAdvisor
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	WHERE		DocumentVariables.VariableID IN (45402)
				AND DocumentVariables.VariableValue IS NOT NULL
),

RiskLogic AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS RiskLogic
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	WHERE		DocumentVariables.VariableID IN (13765)
				AND DocumentVariables.VariableValue IS NOT NULL
),

AccountLogicPlus AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS AccountLogicPlus
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	WHERE		DocumentVariables.VariableID IN (13043)
				AND DocumentVariables.VariableValue IS NOT NULL
),

RiskAndCompliance AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS RiskAndCompliance
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	WHERE		DocumentVariables.VariableID IN (13238)
				AND DocumentVariables.VariableValue IS NOT NULL
),

IDLogic AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS IDLogic
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	WHERE		DocumentVariables.VariableID IN (13053)
				AND DocumentVariables.VariableValue IS NOT NULL
)


SELECT		YEAR(DocumentIDs.GenerationDate) AS [Year],
			MONTH(DocumentIDs.GenerationDate) AS [Month],
			SUM(TotalInquiries.TotalInquiries) AS TotalInquiries,
			SUM(ChexAdvisor.ChexAdvisor) AS ChexAdvisor,
			SUM(RiskLogic.RiskLogic) AS RiskLogic,
			SUM(AccountLogicPlus.AccountLogicPlus) AS AccountLogicPlus,
			SUM(RiskAndCompliance.RiskAndCompliance) AS RiskAndCompliance,
			SUM(IDLogic.IDLogic) AS IDLogic
FROM		DocumentIDs
INNER JOIN	TotalInquiries ON TotalInquiries.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ChexAdvisor ON ChexAdvisor.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	RiskLogic ON RiskLogic.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	AccountLogicPlus ON AccountLogicPlus.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	RiskAndCompliance ON RiskAndCompliance.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	IDLogic ON IDLogic.DocumentID = DocumentIDs.DocumentID
GROUP BY	YEAR(DocumentIDs.GenerationDate),
			MONTH(DocumentIDs.GenerationDate)
ORDER BY	1,2 ASC


--ProviderID	ProviderName	VariableID	VariableName
--9	FactorTrust	13238	/ApplicationResponse/ApplicationInfo/ClientTransactionID | TOTAL INQUIRIES and Risk and compliance
--9	FactorTrust	45402	/ApplicationResponse/ApplicationInfo/ChexAdvisor/Privacy/MsgID | CHEXADVISOR
--9	FactorTrust	13765	/ApplicationResponse/ApplicationInfo/RiskLogic/Scores/Score[1]/Value | RISKLOGIC
--9	FactorTrust	13043	/ApplicationResponse/ApplicationInfo/DDAPlus/DDAPlusResponse | Account Logic +  
--9	FactorTrust	13053	/ApplicationResponse/ApplicationInfo/IDV/AddressisCurrent | IDLogic