
SET Transaction Isolation Level Read Uncommitted;

WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,				
				CAST(DocumentInfos.GenerationDate AS DATE) AS GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate >= '2015-02-1'
				AND DocumentInfos.ProviderID = 11
),

TotalInquiries AS
(
	SELECT		DocumentIDs.DocumentID,
				1 AS TotalInquiries
	FROM		DocumentIDs
	INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
	INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
	WHERE		DocumentVariables.VariableID IN (39780)
)


SELECT		--DocumentIDs.DocumentID,
			--TotalInquiries.TotalInquiries
			YEAR(DocumentIDs.GenerationDate) AS [Year],
			MONTH(DocumentIDs.GenerationDate) AS [Month],
			SUM(TotalInquiries.TotalInquiries) AS TotalInquiries
FROM		DocumentIDs
LEFT JOIN	TotalInquiries ON TotalInquiries.DocumentID = DocumentIDs.DocumentID
GROUP BY	YEAR(DocumentIDs.GenerationDate),
			MONTH(DocumentIDs.GenerationDate)
ORDER BY	1,2 ASC


--ProviderID	ProviderName	VariableID	VariableName
--11	EBureau	39780	/root/transaction/status/uuid