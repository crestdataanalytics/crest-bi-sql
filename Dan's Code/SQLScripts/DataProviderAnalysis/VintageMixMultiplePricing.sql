WITH P AS
(
	SELECT
		LoanID,
		SUM(ISNULL(RentAmount,0))+SUM(ISNULL(FeeAmount,0)) as TotalPaid
	FROM
		vw_loanPayments
	WHERE
		DeletedDate is null
		and ReturnDate is null
	GROUP BY
		LoanID
),
PP AS
(
	SELECT
		LoanID,
		CAST(PaymentDate as Date) as PayDate,
		SUM(ISNULL(RentAmount,0))+SUM(ISNULL(FeeAmount,0)) as TotalPaid
	FROM
		vw_loanPayments
	WHERE
		DeletedDate is null
		and ReturnDate is null
	GROUP BY
		LoanID,
		CAST(PaymentDate as Date)
),
A AS
(
	SELECT
		L.ID,
		ISNULL(SUM(Case when datediff(dd,L.FundedDate,PP.PayDate)<=90 then PP.TotalPaid end),0) as NDP,
		SUM(coalesce(PP.TotalPaid,0)) as TotalPaid
	FROM
		Loans as L
		LEFT JOIN Dealers as D
			on L.DealerID = D.ID and D.ISDemoAccount = 0
		LEFT JOIN PP
			on PP.LoanID = L.ID
	WHERE
		FundedDate is not null
		and FundedAmount > 0
		and L.StatusID not in (10,20,21)
		and L.LoanOwnerID IN (1,2,4,5,7)
		and L.FundedDate > '1-1-15'
	GROUP BY
		L.ID
)


SELECT
	CONVERT(CHAR(7),L.FundedDate,120) as Vintage,
	SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate)<=90 then L.FundedAmount else 0 end)*1.0/
		SUM(L.FundedAmount) as SACmix,
	SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate) between 91 and 364 then L.FundedAmount else 0 end)*1.0/
		SUM(L.FundedAmount) as EBOmix,
	SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate)>=365 then L.FundedAmount else 0 end)*1.0/
		SUM(L.FundedAmount) as PIFmix,
	SUM(case when L.StatusID in (12,13,14) and A.NDP>0 then L.FundedAmount else 0 end)*1.0/
		SUM(L.FundedAmount) as COmix,
	SUM(case when L.StatusID in (12,13,14) and A.NDP<=0 then L.FundedAmount else 0 end)*1.0/
		SUM(L.FundedAmount) as NDNPmix,
	SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate)<=90 then P.TotalPaid else 0 end)*1.0/
		nullif(SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate)<=90 then L.FundedAmount else 0 end),0) as SACmult,
	SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate) between 91 and 364 then P.TotalPaid else 0 end)*1.0/
		nullif(SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate) between 91 and 364 then L.FundedAmount else 0 end),0) as EBOmult,
	SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate)>=365 then P.TotalPaid else 0 end)*1.0/
		nullif(SUM(case when datediff(dd,L.FundedDate,L.PaidOffDate)>=365 then L.FundedAmount else 0 end),0) as PIFmult,
	SUM(case when L.StatusID in (12,13,14) and A.NDP>0 then P.TotalPaid else 0 end)*1.0/
		nullif(SUM(case when L.StatusID in (12,13,14) and A.NDP>0 then L.FundedAmount else 0 end),0) as COmult,
	SUM(case when L.StatusID in (12,13,14) and A.NDP<=0 then P.TotalPaid else 0 end)*1.0/
		nullif(SUM(case when L.StatusID in (12,13,14) and A.NDP<=0 then L.FundedAmount else 0 end),0) as NDNPmult,
	SUM(FP.CrestPercentage*L.FundedAmount)/SUM(L.FundedAmount) as Discount,
	SUM(FP.TotalNotePercent*L.FundedAmount)/SUM(L.FundedAmount) as PricingMultiple,
	SUM(FP.NinetyDayPayoffPercent*L.FundedAmount)/SUM(L.FundedAmount) as SACpremium,
	SUM(L.ApplicationFee*L.FundedAmount)/SUM(L.FundedAmount) as AppFee,
	SUM(case when L.StatusID in (12,13,14) then P.TotalPaid else 0 end)*1.0/
		nullif(SUM(case when L.StatusID in (12,13,14) then L.FundedAmount else 0 end),0) as COmultlessNDNP,
	SUM(P.TotalPaid)*1.0/
		SUM(L.FundedAmount) as Multiple,
	SUM(case when A.NDP>0 then P.TotalPaid else 0 end)*1.0/
		SUM(case when A.NDP>0 then L.FundedAmount else 0 end) as MultipleWithoutNDNP
FROM
	Loans as L
	LEFT JOIN Dealers as D
		on L.DealerID = D.ID and D.IsDemoAccount = 0
	LEFT JOIN P
		on P.LoanID = L.ID
	LEFT JOIN FinancePlans as FP
		on FP.ID = L.PlanID
	LEFT JOIN A
		on A.ID = L.ID
WHERE
	L.FundedDate is not null
	and L.FundedAmount > 0
	and L.StatusID not in (10,20,21)
	and L.LoanOwnerID IN (1,2,4,5,7)
	and L.FundedDate > '1-1-15'
	and D.ID = 3888
GROUP BY
	CONVERT(CHAR(7),L.FundedDate,120)
ORDER BY
	CONVERT(CHAR(7),L.FundedDate,120) asc