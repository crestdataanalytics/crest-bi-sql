SET Transaction Isolation Level Read Uncommitted;
--need to refactor how i am pulling this data
WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-09-01'
				AND DocumentInfos.ProviderID = 13--16
)

SELECT		TOP 10 DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentVariables.VariableID,
			DocumentIDs.GenerationDate,
			CAST(DocumentVariables.VariableValue AS varchar(5)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentVariables.VariableValue IS NOT NULL
			

