SET Transaction Isolation Level Read Uncommitted;
--there was a time where the ResponseXML on the APRL wasn't logging but the trackingID was ... that is why this shred 
	--doesn't go all the way back
--Should bring this up with data team
WITH DocumentIDs AS
(
	SELECT		DocumentInfos.DocumentID,
				DocumentInfos.GenerationDate,
				DocumentInfos.SubjectID AS ApplicantID,
				DocumentInfos.ProviderID
	FROM		DocumentInfos
	WHERE		DocumentInfos.GenerationDate > '2016-09-01'
				AND DocumentInfos.ProviderID = 16
)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ApplicantID,
			DocumentIDs.ProviderID,
			DocumentVariables.VariableID,
			DocumentIDs.GenerationDate,
			CAST(DocumentVariables.VariableValue AS varchar(100)) AS VariableValue
FROM		DocumentIDs
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		Variables.VariableID = 47395 --all of the VariableIDs below yield the same VariableValue on each DocID
order by DocumentIDs.DocumentID

--ProviderID	ProviderName	VariableID	VariableName
--16	DragnetInsight	47377	/TotalInsightResponse/CurrentAddressResponse/TransactionCode
--16	DragnetInsight	47394	/TotalInsightResponse/FuturePerformanceResponse/TransactionCode
--16	DragnetInsight	47395	/TotalInsightResponse/TransactionCode
--16	DragnetInsight	47397	/TotalInsightResponse/VerifyIdentityResponse/TransactionCode
--16	DragnetInsight	47407	/TotalInsightResponse/StatisticsResponse/TransactionCode
--16	DragnetInsight	47419	/TotalInsightResponse/OfacResponse/TransactionCode
--16	DragnetInsight	47432	/TotalInsightResponse/FirstDataConfidenceResponse/TransactionCode
			

