SET Transaction Isolation Level Read Uncommitted;

SELECT		Loans.ID AS LoanID,
			Loans.StatusID,
			LoanStatuses.[Status]
FROM		Loans
INNER JOIN	LoanStatuses ON Loans.StatusID = LoanStatuses.ID
WHERE		Loans.LoanOwnerID IN (1,2,4,5,7)