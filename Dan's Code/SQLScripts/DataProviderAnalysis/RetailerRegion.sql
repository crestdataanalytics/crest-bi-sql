SET Transaction Isolation Level Read Uncommitted;

SELECT		Dealers.ID AS DealerID,
			Addresses.StateID AS [State],
			CASE WHEN Addresses.StateID in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
				WHEN Addresses.StateID in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
				WHEN Addresses.StateID in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
				WHEN Addresses.StateID in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
				ELSE 'NCT' END AS Region
FROM		Dealers
LEFT JOIN	Addresses ON Dealers.AddressID = Addresses.ID
