

SET Transaction Isolation Level Read Uncommitted;
Declare @startDate date,
		@endDate date
SET @startDate = '1/01/2016';
SET	@endDate = '1/01/2017';

SELECT		ApprovalProviderResponseLog.ID AS ApprovalProviderResponseLogID,
			ApprovalProviderResponseLog.ApplicantID,
			ApprovalProviderResponseLog.CreatedDate, 
			ApprovalProviderResponseLog.Decision, 
			ApprovalProviderResponseLog.RiskScore,
			ApprovalProviderResponseLog.ApprovalProviderSettingsID
FROM		CrestWareHouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderResponseLog.CreatedDate >= @startDate
			AND ApprovalProviderResponseLog.CreatedDate < @endDate


