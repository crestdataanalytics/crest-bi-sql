SET Transaction Isolation Level Read Uncommitted;

DECLARE @startdate Date
SET @startdate = '2016-01-01';

WITH
		DocumentIDs AS
		(
			SELECT		DocumentInfos.DocumentID,
						DocumentInfos.ProviderID,
						DocumentInfos.SubjectID,
						DocumentInfos.GenerationDate
			FROM		DocumentInfos
			WHERE		DocumentInfos.GenerationDate >= @startdate
						AND DocumentInfos.ProviderID = 12
		)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.ProviderID,
			DocumentIDs.SubjectID AS ApplicantID,
			DocumentIDs.GenerationDate,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(10)) AS VariableValue,
			Providers.ProviderName
FROM		DocumentIDs
LEFT JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	Providers ON Providers.ProviderID = DocumentIDs.ProviderID
WHERE		DocumentVariables.VariableID = 45709

--ProviderID	ProviderName	VariableID	VariableName
--12	GIACT	45709	/GiactResult/AccountResponseCode