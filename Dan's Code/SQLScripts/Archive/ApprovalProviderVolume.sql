--this gets me the numbers of accounts approved and denied
select Year(ClarityRequestDate) as Year, Month(ClarityRequestDate) as Month, 
	--count(FactorTrustResponse) as CountFactorTrustResponses 
	count(ClarityResponse) as CountClarityResponses 
	--count(DataXResponse) as CountDataXResponses
	--count(MicrobiltResponse) as CountMicrobiltResponses
from AutoApprovalProcessResponses 
where Year(ClarityRequestDate) >= 2014
	--and ClarityResponse=1 
	--and DataXResponse=1 
	--and MicrobiltResponse=1 
	--and FactorTrustResponse = 1   
 group by Year(ClarityRequestDate), Month(ClarityRequestDate)
 order by 1, 2

--this gets me the amounts funded
 select Year(CreationDate) as Year, Month(CreationDate) as Month,
	sum(fundedAmount) as MicrobiltAmount
 from vw_Loans_Reporting
 inner join LoanStatuses on vw_Loans_Reporting.StatusID = LoanStatuses.ID
 where ApplicantID in (
		select applicantID
		from AutoApprovalProcessResponses 
		where Year(ClarityRequestDate) >= 2014
		and ClarityResponse=2
		and DataXResponse=2 
		and MicrobiltResponse=2 
		and FactorTrustResponse = 2
		)
and year(CreationDate) >=2014
group by Year(CreationDate), Month(CreationDate)
order by 1,2