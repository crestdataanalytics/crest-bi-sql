
SET Transaction Isolation Level Read Uncommitted;

SELECT		--COUNT(vw_loans_reporting.ID) AS TotalClosedAccounts
			DISTINCT REPLACE(vw_loans_reporting.DisplayID, '-','') AS ID,
			vw_loans_reporting.DealerID,
			CAST(vw_loans_reporting.OriginationDate AS DATE) AS OriginationDate,
			CAST(vw_loans_reporting.PaidOffDate AS DATE) AS PaidOffDate,
			CAST(vw_loans_reporting.ChargedOffDate AS DATE) AS ChargedOffDate,
			vw_loans_reporting.InvoiceAmount,
			CASE
				WHEN LoanStatuses.[Status] = 'Paid' THEN 1 ELSE 0 END AS IsPaid,
			CASE
				WHEN LoanStatuses.[Status] = 'Charged Off' THEN 1 ELSE 0 END AS IsChargedOff,
			CASE
				WHEN LoanStatuses.[Status] IN ('Bankrupt13','Bankrupt7') THEN 1 ELSE 0 END AS IsBankrupt,
			CASE
				WHEN LoanStatuses.[Status] IN ('Returned') THEN 1 ELSE 0 END AS IsReturned
FROM		vw_loans_reporting
INNER JOIN	LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
WHERE		--LoanStatuses.[Status] = 'Paid'
			LoanStatuses.[Status] IN ('Bankrupt13','Bankrupt7','Charged Off','Paid','Returned')
			AND vw_loans_reporting.CreationDate >= '01/01/2013'
			AND vw_loans_reporting.FundedDate IS NOT NULL
			--AND vw_loans_reporting.DisplayID = '633489-1'