

SELECT		TOP 100
			CAST(vw_loans_reporting.CreationDate AS DATE) CreationDate,
			vw_loans_reporting.ApplicantID,
			vw_loans_reporting.DisplayID,
			LoanStatuses.[Status],
			ApplicantJob.MonthlyIncome
FROM		vw_loans_reporting
INNER JOIN	ApplicantJob on vw_loans_reporting.ApplicantID = ApplicantJob.ApplicantID
INNER JOIN	LoanStatuses ON vw_loans_reporting.StatusID = LoanStatuses.ID
INNER JOIN	Notes ON Notes.LoanID = vw_loans_reporting.ID
WHERE		ApplicantJob.MonthlyIncome >= 15000
			--AND vw_loans_reporting.AutoApprovedFlag = 1
			AND Notes.NoteContent like '%Flagged%'
			--AND LoanStatuses.[Status] = 'Denied'
ORDER BY	vw_loans_reporting.CreationDate DESC