/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct TOP 1000 
		--LoanHistory.StatusID,
		vw_loans_reporting.ID as LoanID,
		ApprovalProviderResponseLog.[ID]
      ,ApprovalProviderResponseLog.[ApplicantID]
	  ,vw_loans_reporting.applicantid
      ,[ApprovalProviderSettingsID]
      ,[CreatedByUserID]
      ,[CreatedDate]
      ,[Decision]
      ,[DenyCode]
      ,[DenyDescription]
      ,[Exception]
      ,[ExceptionDescription]
      ,[RequestXML]
      ,[ResponseXML]
      ,[TrackingID]
      ,[URI]
      ,[RiskScore]
      ,[ResponseTime]
      ,[InquiryID]
      ,[SmartSettings]
      ,[ApprovalProviderControlID]
  FROM [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
  inner join vw_loans_reporting on ApprovalProviderResponseLog.ApplicantID = vw_loans_reporting.ApplicantID
  --left join LoanHistory on vw_loans_reporting.ID = LoanHistory.LoanID
  --left join Notes on Notes.LoanID = vw_loans_reporting.ID
	where vw_loans_reporting.ApplicantID = 1031962 


	--ApprovalProviderResponseLog.approvalprovidersettingsid in (10)
	and ApprovalProviderResponseLog.CreatedDate >= '09/03/2015'

	--and LoanHistory.StatusID != 1
	--and decision in ('Denied','Deny')
	--and requestxml not like '%<control-file-name>furniture</control-file-name>%'
	--and Notes.NoteContent like '%Pending Review%'
	order by ApprovalProviderResponseLog.ApplicantID, ApprovalProviderResponseLog.CreatedDate