SET Transaction Isolation Level Read Uncommitted;

SELECT		LoanID, PaymentMethodLastFour
From		LoanPayments
Where		PaymentDate in ('10/30/2015','10/16/2015')