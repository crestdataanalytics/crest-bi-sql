/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  WFLoanPaymentSchedule.[ID],
		DisplayID
      ,[LoanID]
      ,[ScheduleEntryDesc]
      ,[DueAmount]
      ,[DueDate]
      ,[CreatedDate]
      ,WFLoanPaymentSchedule.[CreatedByID]
      ,[IsDeleted]
      ,[DeletedByUserID]
      ,[DeletedDate]
      ,[DeletedByUsername]
      ,[ProcessedDate]
      ,[PaymentID]
      ,[BankAccount]
      ,[RoutingNumber]
      ,[IsPreview]
      ,[CPRiskLevel]
      ,[CPRiskReason]
      ,[CPProcessDate]
      ,[CPReleaseDate]
      ,[CPReleaseUserID]
      ,[CPReleaseUsername]
      ,[NoReceipt]
      ,[BankAccountOwnerType]
      ,[AccountType]
      ,WFLoanPaymentSchedule.[PaymentSystem]
  FROM [DealerManage].[dbo].[WFLoanPaymentSchedule]
  inner join vw_loans_reporting on vw_loans_reporting.ID = WFLoanPaymentSchedule.LoanID
  where CPRiskLevel = 'high'
	 and DueDate >= '09/21/2015'
	 and DueDate < '09/22/2015'
--  and DueDate > getdate()
--  and vw_loans_reporting.DisplayID in ('732700-1',
--'710602-2',
--'779239-1',
--'783116-1',
--'815557-1',
--'853295-1',
--'832898-2',
--'783482-2',
--'888557-1',
--'897599-1',
--'905735-1',
--'906310-1',
--'911907-1',
--'919638-1',
--'929690-1',
--'932093-1',
--'940456-1',
--'945034-1',
--'591194-3'
--)

--  select count(loanid) Total, CPRiskReason
--  from WFLoanPaymentSchedule
--  where CPRiskLevel = 'high'
--	 and DueDate >= '09/15/2015'
--	 and DueDate < '09/16/2015'
--group by CPRiskReason

--select count(loanid) Total, CPRiskReason
--  from WFLoanPaymentSchedule
--  where CPRiskLevel = 'high'
--	 and DueDate >= '09/16/2015'
--	 and DueDate < '09/17/2015'
--group by CPRiskReason

select count(loanid) Total, CPRiskReason
  from WFLoanPaymentSchedule
  where 
		--CPRiskLevel = 'high'
	  DueDate >= '09/18/2015'
	 and DueDate < '09/23/2015'
group by CPRiskReason