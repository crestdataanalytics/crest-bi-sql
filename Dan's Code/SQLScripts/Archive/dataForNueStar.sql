select distinct vw_loans_reporting.ApplicantID,
	Applicants.FirstName, Applicants.LastName, Applicants.CellPhone, Applicants.HomePhone, Addresses.StreetLine1, 
	Addresses.StreetLine2, Addresses.City, States.Name, Addresses.PostalCode, 
	ApplicantEmailAddresses.EmailAddress, cast(vw_loans_reporting.CreationDate as date) as CreationDate, 
	case 
		when LoanStatuses.Status in ('Bounced','Bankrupt13','Bankrupt7') then 'Delinquent'
		when LoanStatuses.Status in ('Awaiting Signed Docs','ApprovedAwaitingDelivery','ApprovedWithPayment','ApprovedWithDocs') then 'Approved'
		when LoanStatuses.Status in ('Pending','Flagged') then 'Denied'
		when LoanStatuses.Status in ('Ready For Funding','Funded') then 'Funded'
		else LoanStatuses.Status end as LoanStatus,
	vw_loans_reporting.FundedAmount as FinancedAmount, vw_loans_reporting.TotalNote
from vw_loans_reporting
inner join Applicants on Applicants.ID = vw_loans_reporting.ApplicantID
inner join ApplicantAddresses on Applicants.ID = ApplicantAddresses.ApplicantID
inner join Addresses on Applicants.AddressID = Addresses.ID 
inner join States on States.ID = Addresses.StateID
inner join ApplicantEmailAddresses on vw_loans_reporting.ApplicantID = ApplicantEmailAddresses.ApplicantID
inner join LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID

where vw_loans_reporting.CreationDate >= '01/10/2015'
 and vw_loans_reporting.ApplicantID%6 = 0
 and ApplicantEmailAddresses.IsPrimary = 1 and ApplicantEmailAddresses.Invalid = 0
 and (Applicants.CellPhone is not null or Applicants.HomePhone is not null)
 and (Applicants.CellPhone not in ('0','0000000000') or Applicants.HomePhone not in ('0','0000000000'))
 and Addresses.Invalid = 0
 and Applicants.IsDeleted = 0
 and LoanStatuses.Status not in ('Returned','Exchange','DealerReturn','Cancelled')
 --and vw_loans_reporting.ApplicantID = '852378'

 --group by LoanStatuses.Status

 

