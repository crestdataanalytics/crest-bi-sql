
select distinct ApprovalProviderResponseLog.ApplicantID, cast(ApprovalProviderResponseLog.CreatedDate as date) as CreatedDate, 
	case 
		when ApprovalProviderResponseLog.Decision = 'Approve' then 'Approved'
		when ApprovalProviderResponseLog.Decision in ('Deny','HardKillDenied','SoftKillDenied') then 'Denied'
		when ApprovalProviderResponseLog.Decision in ('Exception', '') then 'NULL'
		else ApprovalProviderResponseLog.Decision end as ClarityDecision, 
	case 
		when CoreLogicResponseLog.Decision = 'Approve' then 'Approved'
		when CoreLogicResponseLog.Decision in ('Deny','HardKillDenied','SoftKillDenied') then 'Denied'
		when CoreLogicResponseLog.Decision in ('Exception', '') then 'NULL'
		else CoreLogicResponseLog.Decision end as CoreLogicDecision,
	case 
		when DataXResponseLog.Decision = 'Approve' then 'Approved'
		when DataXResponseLog.Decision in ('Deny','HardKillDenied','SoftKillDenied') then 'Denied'
		when DataXResponseLog.Decision in ('Exception', '') then 'NULL'
		else DataXResponseLog.Decision end as DataXDecision,
	case 
		when MicroBiltResponseLog.Decision = 'Approve' then 'Approved'
		when MicroBiltResponseLog.Decision in ('Deny','HardKillDenied','SoftKillDenied') then 'Denied'
		when MicroBiltResponseLog.Decision in ('Exception', '') then 'NULL'
		else MicroBiltResponseLog.Decision end as MicroBiltDecision,
	case 
		when FactorTrustResponseLog.Decision = 'Approve' then 'Approved'
		when FactorTrustResponseLog.Decision in ('Deny','HardKillDenied','SoftKillDenied') then 'Denied'
		when FactorTrustResponseLog.Decision in ('Exception', '') then 'NULL'
		else FactorTrustResponseLog.Decision end as FactorTrustDecision,
	CoreLogicResponseLog.RiskScore as CoreLogicRiskScore,
	case when AutoApprovalProcessResponses.ApprovalPath = 'FULL' then 1 else 0 end as ApprovalPathIsFull
--select count(ApprovalProviderResponseLog.ApplicantID) as count,  ApprovalProviderResponseLog.ApplicantID
from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
left join AutoApprovalProcessResponses on AutoApprovalProcessResponses.ApplicantID = ApprovalProviderResponseLog.ApplicantID

left join
		(Select ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 8 and ApprovalProviderResponseLog.createddate > '01/01/2012'
		and Decision is not null) as CoreLogicResponseLog
			on (CoreLogicResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID)
			and (cast(CoreLogicResponseLog.CreatedDate as date) = cast(ApprovalProviderResponseLog.CreatedDate as date))

left join
		(Select ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 2 and ApprovalProviderResponseLog.createddate > '01/01/2012'
		and Decision is not null) as DataXResponseLog
		on (DataXResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID)
		and (cast(DataXResponseLog.CreatedDate as date) = cast(ApprovalProviderResponseLog.CreatedDate as date))

left join
		(Select ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 3 and ApprovalProviderResponseLog.createddate > '01/01/2012'
		and Decision is not null) as MicroBiltResponseLog
		on (MicroBiltResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID)
		and (cast(MicroBiltResponseLog.CreatedDate as date) = cast(ApprovalProviderResponseLog.CreatedDate as date))

left join
		(Select ApplicantID, ApprovalProviderSettingsID, CreatedDate, Decision, RiskScore
		from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
		where ApprovalProviderSettingsID = 4 and ApprovalProviderResponseLog.createddate > '01/01/2012'
		and Decision is not null) as FactorTrustResponseLog
		on (FactorTrustResponseLog.ApplicantID = ApprovalProviderResponseLog.ApplicantID)
		and (cast(FactorTrustResponseLog.CreatedDate as date) = cast(ApprovalProviderResponseLog.CreatedDate as date))
							
where ApprovalProviderResponseLog.createddate > '01/01/2012'
	and ApprovalProviderResponseLog.ApprovalProviderSettingsID = 1
	--and  CoreLogicResponseLog.Decision is not null
	--and AutoApprovalProcessResponses.ApprovalPath = 'FULL'
	--and ApprovalProviderResponseLog.applicantid = '868499'
--group by ApprovalProviderResponseLog.ApplicantID
order by CreatedDate desc
--order by count desc

select count(distinct applicantID)
from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
where ApprovalProviderResponseLog.createddate > '01/01/2012'
 and ApprovalProviderResponseLog.ApprovalProviderSettingsID = 1

