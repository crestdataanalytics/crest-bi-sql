SET Transaction Isolation Level Read Uncommitted;

SET Transaction Isolation Level Read Uncommitted;

--name, dob, ssn
SELECT		DISTINCT Applicants.ID AS ApplicantID,
			REPLACE(Applicants.FirstName,',','') AS FirstName, 
			REPLACE(Applicants.LastName,',','') AS LastName,
			CAST(Applicants.DateOfBirth as date) as DOB,
			CAST(Applicants.SocialSecurityNumber AS VARCHAR(20)) AS SSN
FROM		Applicants
INNER JOIN	Loans ON Loans.ApplicantID = Applicants.ID
WHERE		Loans.CreationDate >= '2014-01-01'




--phone
SELECT		DISTINCT Applicants.ID AS ApplicantID,		
			ApplicantPhone.ID AS PhoneNumberID,	
			ApplicantPhone.PhoneNumber,
			CASE WHEN ApplicantPhone.PhoneNumber = Applicants.CellPhone THEN 1 ELSE 0 END AS MobileNumber
FROM		Applicants
INNER JOIN	Loans ON Loans.ApplicantID = Applicants.ID
LEFT JOIN	ApplicantPhone ON ApplicantPhone.ApplicantID = Applicants.ID
WHERE		Loans.CreationDate >= '2014-01-01'
			AND PhoneNumber IS NOT NULL


--address
SELECT		DISTINCT Applicants.ID AS ApplicantID,
			REPLACE(REPLACE(Addresses.StreetLine1, ',', ''), '*','') as Street1, 
			REPLACE(REPLACE(Addresses.StreetLine2, ',', ''), '*','') as Street2, 
			REPLACE(Addresses.City, ',','') AS City,
			Addresses.StateID as [State], 
			Addresses.PostalCode as Zip, 
			Addresses.ID AS AddressID
FROM		Applicants
INNER JOIN	Loans ON Loans.ApplicantID = Applicants.ID
LEFT JOIN	ApplicantAddresses ON Applicants.ID = ApplicantAddresses.ApplicantID
LEFT JOIN	Addresses ON Addresses.ID = ApplicantAddresses.AddressID
WHERE		Loans.CreationDate >= '2014-01-01'
			AND Addresses.Invalid = 0


--email
SELECT		DISTINCT Applicants.ID AS ApplicantID,
			ApplicantEmailAddresses.ID AS ApplicantEmailAddressID,
			REPLACE(ApplicantEmailAddresses.EmailAddress,',','') as EmailAddress
FROM		Applicants
INNER JOIN	Loans ON Loans.ApplicantID = Applicants.ID
LEFT JOIN	ApplicantEmailAddresses ON ApplicantEmailAddresses.ApplicantID = Applicants.ID
WHERE		Loans.CreationDate >= '2014-01-01'
