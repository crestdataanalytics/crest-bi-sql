SET Transaction Isolation Level Read Uncommitted;

SELECT		Dealers.ID AS DealerID,
			REPLACE(REPLACE(Dealers.Name, ',', ''), '*','') AS DealerName,
			REPLACE(REPLACE(Addresses.StreetLine1, ',', ''), '*','') as Street1, 
			REPLACE(REPLACE(Addresses.StreetLine2, ',', ''), '*','') as Street2,
			Addresses.City,
			Addresses.StateID AS [State],
			Addresses.PostalCode,
			Loans.ID AS LoanID,
			Loans.DisplayID
FROM		Dealers
INNER JOIN	Addresses ON Dealers.AddressID = Addresses.ID
INNER JOIN	Loans ON Loans.DealerID = Dealers.ID
