
--select count(vw_loans_reporting.DisplayID)
select Dealers.ID as RetailerID, vw_loans_reporting.DisplayID as ApplicantID, vw_loans_reporting.ID as LoanID, vw_loans_reporting.CreationDate,
	Applicants.FirstName, Applicants.LastName, cast(Applicants.DateOfBirth as date) as DateOfBirth, Applicants.SocialSecurityNumber,
	Applicants.HomePhone, Applicants.CellPhone, Applicants.EmailAddress, 
	replace(Addresses.StreetLine1, ',', '') as StreetLine1, 
	replace(Addresses.StreetLine2, ',', '') as StreetLine2, 
	Addresses.City, Addresses.StateID as [State], Addresses.PostalCode, TotalMonthlyIncome.TotalMonthlyIncome,
	BankAccounts.AccountNumber as BankAccountNumber, BankAccounts.RoutingNumber, cast(ApplicantJob.HireDate as date) as HireDate, 
	ApplicantJob.PhoneNumber as EmployerPhoneNumber, cast(ApplicantJob.LastPayDate as date) as LastPayDate, 
	cast(ApplicantJob.NextPayDate as date) as NextPayDate, PayPeriodTypes.Period as PayPeriod, 
	ApprovalProviderResponseLog.TrackingID as ClarityTrackingNumber, vw_loans_reporting.Amount as FinancedAmount,
	case when vw_loans_reporting.Amount = 0 then 0 else vw_loans_reporting.FundedAmount end as MerchantPaidAmount, 
	Categories.CategoryName as CollateralType, PrincipleAmountOwed.TermOfLeaseInMonths,
	case 
		when vw_loans_reporting.AutoApprovedFlag = 1 then 1 
		when vw_loans_reporting.AutoApprovedFlag = 0 then 0
		else 0 end as IsAutoApproved,
	case 
		when (vw_loans_reporting.FundedDate is not null and PrincipleAmountOwed.PaymentDate is not null and LoanPaymentFrequency.FirstDueDate is not null) and 
			((LoanPaymentFrequency.FirstDueDate >= PrincipleAmountOwed.PaymentDate) or (LoanPaymentFrequency.FirstDueDate >= getdate()))
			then 1
		when (vw_loans_reporting.FundedDate is not null and PrincipleAmountOwed.PaymentDate is not null and LoanPaymentFrequency.FirstDueDate is not null) and 
			((LoanPaymentFrequency.FirstDueDate < PrincipleAmountOwed.PaymentDate) or (LoanPaymentFrequency.FirstDueDate < getdate()))
			then 0
		end as FirstPaymentOnTime,
	case
		when datediff(day, LoanPaymentFrequency.FirstDueDate, PrincipleAmountOwed.PaymentDate) >= 90 then 1
		when datediff(day, LoanPaymentFrequency.FirstDueDate, PrincipleAmountOwed.PaymentDate) < 90 then 0
		end as NinetyDayDelinquency,
	case 
		when PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid > 0 and vw_loans_reporting.StatusID not in (4,5,12) 
			 and vw_loans_reporting.FundedDate is not null then 1 
		else 0 end as AccountDelinquent,
	case
		when vw_loans_reporting.StatusID = 4 then 1 
		when PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid <= 0 and vw_loans_reporting.StatusID not in (5,12)	
			 and vw_loans_reporting.FundedDate is not null then 1
		else 0 end as AccountCurrent,
	case 
		when vw_loans_reporting.StatusID = 12 then 1 else 0 end as AccountChargedOff,
	case 
		when vw_loans_reporting.StatusID = 5 then 1 else 0 end as AccountPaidOff
			
from vw_loans_reporting
inner join 
	(Select count(ApplicantID) as CountApplicantID, ApplicantID, max(CreatedDate) as CreatedDate, max(TrackingID) as TrackingID
	From [CrestWarehouse].[dbo].[ApprovalProviderResponseLog]
	Where ApprovalProviderSettingsID = 1
	group by ApplicantID) as ApprovalProviderResponseLog on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
left join LoanTerms on vw_loans_reporting.LoanTermID = LoanTerms.ID
inner join Dealers on Dealers.ID = vw_loans_reporting.DealerID
inner join (
	select count(DealerID) as CountDealerID, DealerID, max(categoryID) as CategoryID 
	from DealerCategories
	group by DealerID) as DealerCategories on Dealers.ID = DealerCategories.DealerID
inner join Categories on DealerCategories.CategoryID = Categories.ID
inner join Applicants on Applicants.ID = vw_loans_reporting.ApplicantID
inner join ApplicantBankAccounts on Applicants.ID = ApplicantBankAccounts.ApplicantID
inner join BankAccounts on ApplicantBankAccounts.BankAccountID = BankAccounts.ID
inner join Addresses on Addresses.ID = Applicants.AddressID
inner join LoanStatuses on vw_loans_reporting.StatusID = LoanStatuses.ID
left join ApplicantJob on ApplicantJob.ApplicantID = Applicants.ID
inner join PayPeriodTypes on PayPeriodTypes.ID = ApplicantJob.PayPeriodTypeID
inner join 
	(Select ApplicantID, Sum(MonthlyIncome) as TotalMonthlyIncome 
	from ApplicantJob
	group by ApplicantID) as TotalMonthlyIncome
	on TotalMonthlyIncome.ApplicantID = vw_loans_reporting.ApplicantID
left join 
	(SELECT  LoanID , sum(Amount) AS AmountPaid, count(Amount) AS NumberOfPaymentsMade
     FROM    LoanPayments
     WHERE   PaymentDate is not null and IsDeleted = 0 and HasBeenReturned = 0
     GROUP BY LoanID) AmountPaid
	on AmountPaid.LoanID = vw_loans_reporting.ID
left join
	(SELECT vw_loans_reporting.ID as LoanID, FirstPayment.PaymentDate, ISNULL(12,LoanTerms.Months) as TermOfLeaseInMonths,
	case
		when dateadd(mm, ISNULL(12,LoanTerms.Months),FirstPayment.PaymentDate) >= getdate() then
			(vw_loans_reporting.TotalNote / ISNULL(12,LoanTerms.Months) * datediff(month, FirstPayment.PaymentDate, getdate()))
		when dateadd(mm, ISNULL(12,LoanTerms.Months),FirstPayment.PaymentDate) < getdate() then
			vw_loans_reporting.TotalNote
		else NULL end as AmountOwed
	FROM vw_loans_reporting
	left join 
		(SELECT  LoanID , MIN(PaymentDate) AS PaymentDate
		FROM    LoanPayments
		left join LoanPaymentTypes on LoanPaymentTypes.ID = LoanPayments.TypeID
		WHERE   LoanPayments.Returned = 0 AND LoanPayments.DeletedDate IS NULL and LoanPaymentTypes.[Type] != 'Initial Payment'
		GROUP BY LoanID) FirstPayment on FirstPayment.LoanID = vw_loans_reporting.ID
	left join LoanTerms on LoanTerms.ID = vw_loans_reporting.LoanTermID
	) PrincipleAmountOwed
	on PrincipleAmountOwed.LoanID = vw_loans_reporting.ID
left join
	(SELECT  LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
     FROM    LoanPaymentSchedule
	 WHERE ScheduleEntryDesc != 'Initial Payment'
     GROUP BY LoanID) LoanPaymentFrequency
	on LoanPaymentFrequency.LoanID = vw_loans_reporting.ID 
where vw_loans_reporting.CreationDate >= '01/01/2014' and vw_loans_reporting.CreationDate <= '02/28/2015'
	and BankAccounts.IsPrimaryAccount = 1
	and ApplicantJob.IsPrimary = 1
	--and LoanStatuses.[Status] != 'Canceled'
	
order by vw_loans_reporting.CreationDate Asc
--and vw_loans_reporting.DisplayID = '868499-1'
--and PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid > 0 and vw_loans_reporting.StatusID not in (5,12)
--and PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid <= 0
--and vw_loans_reporting.NumDaysLate >0 and PrincipleAmountOwed.AmountOwed <= AmountPaid.AmountPaid and vw_loans_reporting.StatusID != 5
--and vw_loans_reporting.ChargedOffDate is not null
--and vw_loans_reporting.EarlyPayout = 1
--and vw_loans_reporting.PaidOffDate <= vw_loans_reporting.NinetyDaysEndDate and vw_loans_reporting.StatusID = 5