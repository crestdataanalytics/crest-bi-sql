select Dealers.ID as RetailerID, Dealers.ID as RetailerName, Categories.CategoryName as RetailerCategory,
	vw_loans_reporting.DisplayID, cast(vw_loans_reporting.CreationDate as date) as LeaseCreationDate, vw_loans_reporting.OriginationDate,
	vw_loans_reporting.ID as LoanID, Addresses.StateID as [State], vw_loans_reporting.Amount as FinancedAmount,
	vw_loans_reporting.FundedAmount as MerchantPaidAmount, cast(vw_loans_reporting.FundedDate as Date) FundedDate, vw_loans_reporting.EarlyPayout,
	vw_loans_reporting.TotalNote,
	cast(vw_loans_reporting.NinetyDaysEndDate as date) NinetyDaysEndDate, --cast(vw_loans_reporting.FirstPaymentDate as date) as FirstPaymentDueDate, 
	case 
		when vw_loans_reporting.NumDaysLate < 0 then 0
		when vw_loans_reporting.ChargedOffDate is not null then 0
		when vw_loans_reporting.PaidOffDate is not null then 0
		when vw_loans_reporting.StatusID in (4,5,12) then 0
		when PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid <= 0 and vw_loans_reporting.StatusID not in (5,12) then 0
		else vw_loans_reporting.NumDaysLate end as NumDaysLate, 
	cast(PrincipleAmountOwed.PaymentDate as date) as FirstPaymentDate, vw_loans_reporting.Balance,
	vw_loans_reporting.ChargedOffDate, 
	case when vw_loans_reporting.PaidOffDate is not null then 1 else 0 end as AccountIsPaidOff,
	case
		when vw_loans_reporting.statusID = 12 then 'ChargedOff'
		when vw_loans_reporting.statusID = 5 then 'PaidOff'
		else '' end as LoanStatus,
	dateadd(mm, isnull(LoanTerms.Months, 12),cast(PrincipleAmountOwed.PaymentDate as date)) as EndOfLeaseTermDate,
	case 
		when vw_loans_reporting.AutoApprovedFlag = 1 then 1 
		when vw_loans_reporting.AutoApprovedFlag = 0 then 0
		else 0 end as IsAutoApproved,
	case 
		when LoanPaymentFrequency.FirstDueDate >= PrincipleAmountOwed.PaymentDate then 0 
		when LoanPaymentFrequency.FirstDueDate >= getdate() and PrincipleAmountOwed.PaymentDate is not null then 0
		else 1 end as AccountIsFPD, 
	case when
		vw_loans_reporting.PaidOffDate <= vw_loans_reporting.NinetyDaysEndDate and vw_loans_reporting.StatusID = 5 then 1 else 0 end as PaidOffInNinetyDays,
	case when
		PrincipleAmountOwed.AmountOwed > AmountPaid.AmountPaid and vw_loans_reporting.StatusID not in (5,12)
			then PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid else 0 end as AmountPastDue,
	Fees.Fees as FeesOwed, TotalPaid.TotalPaid,
	case
		when TotalPaid.TotalPaid > vw_loans_reporting.TotalNote then TotalPaid.TotalPaid - vw_loans_reporting.TotalNote
			else 0 end as FeesPaid,
	case
		when vw_loans_reporting.TotalNote - AmountPaid.AmountPaid > 0 then vw_loans_reporting.TotalNote - AmountPaid.AmountPaid
			else 0 end as UnPaidNote, 
	PrincipleAmountOwed.TermOfLeaseInMonths,
	case when
		AmountPaid.NumberOfPaymentsMade is null then 0 else AmountPaid.NumberOfPaymentsMade end as NumberOfPaymentsMade,
	case 
		when NumberOfPaymentsOwed.NumberOfPaymentsOwed is null then 0
		when vw_loans_reporting.ChargedOffDate is not null then 0
		when vw_loans_reporting.PaidOffDate is not null then 0
		else NumberOfPaymentsOwed.NumberOfPaymentsOwed end as NumberOfPaymentsOwed,
	case
		when vw_loans_reporting.StatusID = 4 then 1 
		when PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid <= 0 and vw_loans_reporting.StatusID not in (5,12) then 1
		else 0 end as AccountIsCurrent,
	case 
		when PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid > 0 and vw_loans_reporting.StatusID not in (4,5,12) then 1 
		else 0 end as AccountIsDelinquent,
	case 
		when vw_loans_reporting.StatusID = 12 then 1 else 0 end as AccountIsChargedOff,
	case
		when vw_loans_reporting.StatusID = 5 and  TotalPaid.TotalPaid <= vw_loans_reporting.Amount then 1
		else 0 end as AccountIsPaidEarlyIn90Days,
	case
		when vw_loans_reporting.StatusID = 5 and AmountPaid.AmountPaid < vw_loans_reporting.TotalNote 
			and AmountPaid.AmountPaid > vw_loans_reporting.Amount then 1 
		else 0 end as AccountIsPaidEarlyAfter90Days,
	case
		when vw_loans_reporting.StatusID = 5 and vw_loans_reporting.PaidOffDate > vw_loans_reporting.NinetyDaysEndDate
			and AmountPaid.AmountPaid >= vw_loans_reporting.TotalNote then 1 
		else 0 end as AccountIsPaidOnSchedule,
	case
		when vw_loans_reporting.StatusID = 4 and dateadd(mm, PrincipleAmountOwed.TermOfLeaseInMonths,PrincipleAmountOwed.PaymentDate) > getdate() and
			AmountPaid.AmountPaid > (vw_loans_reporting.TotalNote / PrincipleAmountOwed.TermOfLeaseInMonths * 
									datediff(month, PrincipleAmountOwed.PaymentDate, getdate())) then 1 
		else 0 end as AccountIsCurrentPaidAhead,
	case
		when vw_loans_reporting.StatusID = 4 and dateadd(mm, PrincipleAmountOwed.TermOfLeaseInMonths,PrincipleAmountOwed.PaymentDate) > getdate() and
			AmountPaid.AmountPaid > PrincipleAmountOwed.AmountOwed then AmountPaid.AmountPaid - PrincipleAmountOwed.AmountOwed
		else 0 end as AmountPaidAheadWhileCurrent,
	case when Categories.CategoryName = 'Furniture' then 1 else 0 end as CategoryIsFurniture,
	case when Categories.CategoryName = 'Mattress' then 1 else 0 end as CategoryIsMattress,
	case when Categories.CategoryName = 'Tires/Rims' then 1 else 0 end as CategoryIsTiresRims,
	case when Categories.CategoryName = 'Jewelry' then 1 else 0 end as CategoryIsJewelry,
	case when Categories.CategoryName = 'Appliance/Electronics/Video' then 1 else 0 end as CategoryIsApplianceElectronicsVideo,
	case 
		when LoanPaymentFrequency.LoanPaymentFrequency in ('Loan Payment (Monthly SYS)', 'Loan Payment (Monthly SYS2)', 
			'Loan Payment (For 90Days)','Loan Payment (Monthly)') then 'Monthly'
		when LoanPaymentFrequency.LoanPaymentFrequency in ('Initial Payment', '') then NULL
		when LoanPaymentFrequency.LoanPaymentFrequency = 'Loan Payment (Biweekly)' then 'Biweekly'
		when LoanPaymentFrequency.LoanPaymentFrequency = 'Loan Payment (Twice Monthly)' then 'Semi-Monthly'
		when LoanPaymentFrequency.LoanPaymentFrequency = 'Loan Payment (Weekly)' then 'Weekly'
		else LoanPaymentFrequency.LoanPaymentFrequency end as LoanPaymentFrequency,

	PayPeriodTypes.Period as JobPayPeriod,
	case
		when LoanPayments.Returned = 0 
			and LoanPayments.IsDeleted = 0
			and LoanPayments.HasBeenReturned = 0 
			and LoanPaymentTypes.[Type] not in ('Initial Payment','Migrated')
			and vw_Loans_reporting.FundedDate is not null
			then LoanPayments.Amount
		else NULL end as ClearedPaymentAmount,
	case
		when LoanPayments.Returned = '' 
			and LoanPayments.IsDeleted = ''
			and LoanPayments.HasBeenReturned = '' 
			and LoanPaymentTypes.[Type] not in ('Initial Payment','Migrated')
			and vw_Loans_reporting.FundedDate is not null
			then LoanPayments.PaymentDate
		else NULL end as ClearedPaymentDate
		
from vw_loans_reporting
left join LoanTerms on vw_loans_reporting.LoanTermID = LoanTerms.ID
inner join Dealers on Dealers.ID = vw_loans_reporting.DealerID
inner join DealerCategories on Dealers.ID = DealerCategories.DealerID
inner join Categories on DealerCategories.CategoryID = Categories.ID
inner join Applicants on Applicants.ID = vw_loans_reporting.ApplicantID
inner join Addresses on Addresses.ID = Applicants.AddressID
inner join LoanStatuses on vw_loans_reporting.StatusID = LoanStatuses.ID
left join ApplicantJob on ApplicantJob.ApplicantID = Applicants.ID
inner join PayPeriodTypes on PayPeriodTypes.ID = ApplicantJob.PayPeriodTypeID
left join LoanPayments on LoanPayments.LoanID = vw_loans_reporting.ID
left join LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
left join 
	(SELECT  LoanID , sum(Amount) AS AmountPaid, count(Amount) AS NumberOfPaymentsMade
     FROM    LoanPayments
     WHERE   PaymentDate is not null and IsDeleted = 0 and HasBeenReturned = 0
     GROUP BY LoanID) AmountPaid
	on AmountPaid.LoanID = vw_loans_reporting.ID
left join
	(SELECT vw_loans_reporting.ID as LoanID, FirstPayment.PaymentDate, ISNULL(12,LoanTerms.Months) as TermOfLeaseInMonths,
	case
		when dateadd(mm, ISNULL(12,LoanTerms.Months),FirstPayment.PaymentDate) >= getdate() then
			(vw_loans_reporting.TotalNote / ISNULL(12,LoanTerms.Months) * datediff(month, FirstPayment.PaymentDate, getdate()))
		when dateadd(mm, ISNULL(12,LoanTerms.Months),FirstPayment.PaymentDate) < getdate() then
			vw_loans_reporting.TotalNote
		else NULL end as AmountOwed
	FROM vw_loans_reporting
	left join 
		(SELECT  LoanID , MIN(PaymentDate) AS PaymentDate
		FROM    LoanPayments
		WHERE   Returned = 0 AND DeletedDate IS NULL
		GROUP BY LoanID) FirstPayment on FirstPayment.LoanID = vw_loans_reporting.ID
	left join LoanTerms on LoanTerms.ID = vw_loans_reporting.LoanTermID
	) PrincipleAmountOwed
	on PrincipleAmountOwed.LoanID = vw_loans_reporting.ID
left join
	(SELECT  LoanID , count(DueAmount) AS NumberOfPaymentsOwed
     FROM    LoanPaymentSchedule
     WHERE   PaidStatus in ('Not Yet Due','Due')
     GROUP BY LoanID) NumberOfPaymentsOwed
	on NumberOfPaymentsOwed.LoanID = vw_loans_reporting.ID
left join
	(SELECT  LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
     FROM    LoanPaymentSchedule
	 WHERE ScheduleEntryDesc != 'Initial Payment'
     GROUP BY LoanID) LoanPaymentFrequency
	on LoanPaymentFrequency.LoanID = vw_loans_reporting.ID
--get fees in here
left join
	(select LoanID, sum(Amount) as Fees
	from LoanPaymentFeesOwed
	where WaivedDate is null
	group by LoanID	
	) as Fees
	on Fees.LoanID = vw_loans_reporting.ID
left join
	(select LoanID, sum(Amount) as TotalPaid
	from LoanPayments
	where Returned = 0 and isDeleted = 0 and PaymentDate is not null
	group by LoanID	
	) as TotalPaid
	on TotalPaid.LoanID = vw_loans_reporting.ID 
where --datepart(year, vw_loans_reporting.CreationDate) >= 2013 and 
	vw_loans_reporting.CreationDate >= '01/10/2015' and 
	vw_loans_reporting.FundedDate is not null 
--and vw_loans_reporting.DisplayID = '501267-2'
--and vw_loans_reporting.DisplayID = '868499-1'
--and PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid > 0 and vw_loans_reporting.StatusID not in (5,12)
--and PrincipleAmountOwed.AmountOwed - AmountPaid.AmountPaid <= 0
--and vw_loans_reporting.NumDaysLate >0 and PrincipleAmountOwed.AmountOwed <= AmountPaid.AmountPaid and vw_loans_reporting.StatusID != 5
--and vw_loans_reporting.ChargedOffDate is not null
--and vw_loans_reporting.EarlyPayout = 1
--and vw_loans_reporting.PaidOffDate <= vw_loans_reporting.NinetyDaysEndDate and vw_loans_reporting.StatusID = 5