
SET Transaction Isolation Level Read Uncommitted;

SELECT WFLoanPaymentSchedule.[ID] AS WFLoanPaymentScheduleID
      ,WFLoanPaymentSchedule.[LoanID]
      ,[DueDate]
      ,[ProcessedDate]
      ,[PaymentID]
      ,[BankAccount]
	  ,CPReleaseDate
      ,[RoutingNumber]
      ,[CPRiskLevel]
	  ,[AccountType]
	  ,LoanPayments.ReturnDate
  FROM [DealerManage].[dbo].[WFLoanPaymentSchedule]
  LEFT JOIN	LoanPayments ON LoanPayments.ID = WFLoanPaymentSchedule.PaymentID 
  WHERE YEAR(duedate) = 2015 
		AND WFLoanPaymentSchedule.DeletedDate IS NULL 
		AND WFLoanPaymentSchedule.CPRiskLevel = 'high'
		AND CPReleaseDate IS NOT NULL
  order by duedate desc

  --this goes to the ALLRETURNED FILE  PROBABLY DONT NEED THIS
  SELECT	LoanID,
			PaymentDate,
			ReturnDate
  FROM		LoanPayments
  WHERE		YEAR(PaymentDate) = 2015


  --select * from loanpayments where PaymentDate >= '2015-12-01' and ReturnDate is not null

  select count(id) from LoanPayments where DeletedDate is null and PaymentDate >= '2015-01-02' and PaymentDate < '2015-001-03' and ReturnDate is not null
--  select month(duedate) month,
--		day(duedate) day ,
--		count(cprisklevel),
--		cprisklevel
--From WFLoanPaymentSchedule
--where cprisklevel = 'high' and year(duedate) = 2015
--group by month(duedate),
--		day(duedate),
--		cprisklevel
--order by 1,2,3 asc

