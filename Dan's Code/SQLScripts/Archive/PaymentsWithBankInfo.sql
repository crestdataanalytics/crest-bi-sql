
SET Transaction Isolation Level Read Uncommitted;

SELECT		vw_loans_reporting.ID AS LoanID,
			WFLoanPaymentSchedule.PaymentID,
			CAST(LoanPayments.PaymentDate AS DATE) ProcessedDate,
			WFLoanPaymentSchedule.BankAccount,
			WFLoanPaymentSchedule.RoutingNumber,
			WellsFargoReturns.ReturnReason,
			WellsFargoReturns.ReturnCode,
			FirstDraft.FirstDraft,
			LoanPayments.Returned

FROM		vw_loans_reporting
INNER JOIN	LoanPayments ON vw_loans_reporting.ID = LoanPayments.LoanID
LEFT JOIN	WFLoanPaymentSchedule ON LoanPayments.ID = WFLoanPaymentSchedule.PaymentID
INNER JOIN	LoanStatuses ON vw_loans_reporting.StatusID = LoanStatuses.ID
LEFT JOIN	(SELECT		MIN(LoanPayments.ID) PaymentID, LoanPayments.LoanID, 1 AS FirstDraft
			FROM		LoanPayments
			INNER JOIN	LoanPaymentTypes ON LoanPaymentTypes.ID = LoanPayments.TypeID
			WHERE		LoanPayments.IsDeleted = 0
						AND LoanPaymentTypes.[Type] IN ('ACH','Credit Card')
			GROUP BY	LoanPayments.LoanID
			)	AS FirstDraft ON LoanPayments.ID = FirstDraft.PaymentID
INNER JOIN	LoanPaymentTypes ON LoanPaymentTypes.ID = LoanPayments.TypeID
LEFT JOIN	WellsFargoReturns ON WellsFargoReturns.PaymentID = WFLoanPaymentSchedule.PaymentID

WHERE		LoanPayments.PaymentDate >= '01/01/2014'
			AND LoanPayments.PaymentDate < '09/01/2015'
			AND WFLoanPaymentSchedule.IsDeleted = 0
			AND LoanPaymentTypes.[Type] IN ('ACH','Credit Card')
			AND LoanStatuses.[Status] != 'Cancelled'

			--and WFLoanPaymentSchedule.LoanID = 1000000
			and month(LoanPayments.PaymentDate) = 7
			and year(LoanPayments.PaymentDate) = 2015
			--and FirstDraft.FirstDraft = 1
			--and LoanPayments.Returned = 1
			--and WellsFargoReturns.ReturnCode is not null
			