--select top 10 * from CrestWarehouse.dbo.approvalproviderresponselog where approvalprovidersettingsid = 14 and createddate >= '2016-09-29'
SET Transaction Isolation Level Read Uncommitted;

SELECT		ID AS ApprovalProviderResponseLogID,
			ApprovalProviderSettingsID,
			ApplicantID,
			TrackingID AS ValidationGUID,
			RiskScore AS ValidationScore
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 14
			