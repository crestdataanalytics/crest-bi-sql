SET Transaction Isolation Level Read Uncommitted;

SELECT		DISTINCT top 10 DocumentInfos.ProviderID,
			Providers.ProviderName,
			Variables.VariableID,
			Variables.VariableName
FROM		DocumentInfos
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
INNER JOIN	Providers ON Providers.ProviderID = DocumentInfos.ProviderID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentInfos.ProviderID = 11
			AND YEAR(DocumentInfos.GenerationDate) >= 2015
			AND Variables.VariableName LIKE '%/root/transaction/status/uuid%'