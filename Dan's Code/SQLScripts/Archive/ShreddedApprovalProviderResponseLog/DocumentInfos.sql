SET Transaction Isolation Level Read Uncommitted;
Declare @startDate date,
		@endDate date
SET @startDate = '1/01/2015';
SET	@endDate = '1/01/2016';

SELECT		DocumentInfos.DocumentID,
			DocumentInfos.GenerationDate,
			DocumentInfos.ProviderID,
			DocumentInfos.SubjectID AS ApplicantID
FROM		ProviderAnalysis.dbo.DocumentInfos
WHERE		DocumentInfos.GenerationDate >= @startDate
			AND DocumentInfos.GenerationDate < @endDate