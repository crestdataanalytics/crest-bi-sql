SET Transaction Isolation Level Read Uncommitted;


SELECT		DocumentVariables.DocumentID,
			DocumentVariables.VariableID,
			CAST(DocumentVariables.VariableValue AS varchar(1000)) VariableValue
FROM		ProviderAnalysis.dbo.DocumentVariables
--WHERE		DocumentVariables.VariableID IN (9697,9791,103) --9697 = Clear Fraud | 9791 = Clear Tradeline | 103 = Clear Bank
