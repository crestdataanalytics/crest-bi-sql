SET Transaction Isolation Level Read Uncommitted;

SELECT		DISTINCT DocumentInfos.ProviderID,
			Providers.ProviderName,
			Variables.VariableName,
			Variables.VariableID
FROM		DocumentInfos
INNER JOIN	DocumentVariables ON DocumentVariables.DocumentID = DocumentInfos.DocumentID
INNER JOIN	Providers ON Providers.ProviderID = DocumentInfos.ProviderID
INNER JOIN	Variables ON Variables.VariableID = DocumentVariables.VariableID
WHERE		DocumentInfos.ProviderID = 9
			AND YEAR(DocumentInfos.GenerationDate) >= 2015
