SET Transaction Isolation Level Read Uncommitted;


--9697 = Clear Fraud | 9791 = Clear Tradeline | 103 = Clear Bank | 1811 = Tracking-Number | 44727 = ClearBankBehavior
DECLARE @startDate date,
		@endDate date
SET		@startDate = '1/01/2016';
SET		@endDate = '1/01/2017';

WITH
		DocumentIDs AS
		(
			SELECT		DocumentInfos.DocumentID,
						DocumentInfos.ProviderID,
						DocumentInfos.SubjectID,
						DocumentInfos.GenerationDate
			FROM		DocumentInfos
			WHERE		DocumentInfos.GenerationDate >= @startDate
						AND DocumentInfos.GenerationDate < @endDate
						AND DocumentInfos.ProviderID = 7
		),

		ClearFraud AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS ClearFraudVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS ClearFraudVariableValue,
						'0.65' AS ClearFraudPrice
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 9697
		),

		ClearTradeline AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS ClearTradelineVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS ClearTradelineVariableValue,
						'0.47' AS ClearTradelinePrice
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 9791
		),

		ClearBank AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS ClearBankVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS ClearBankVariableValue,
						'0.65' AS ClearBankPrice
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 103
		),

		ClarityTrackingNumber AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS ClarityTrackingNumberVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS ClarityTrackingNumberVariableValue
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 1811
		),

		ClearBankBehavior AS
		(
			SELECT		DocumentVariables.DocumentID,
						DocumentVariables.VariableID AS ClearBankBehaviorVariableID,
						CAST(DocumentVariables.VariableValue AS varchar(100)) AS ClearBankBehaviorVariableValue,
						'0.65' AS ClearBankBehaviorPrice
			FROM		DocumentVariables
			INNER JOIN	DocumentIDs ON DocumentVariables.DocumentID = DocumentIDs.DocumentID
			WHERE		DocumentVariables.VariableID = 44727
		)

SELECT		DocumentIDs.DocumentID,
			DocumentIDs.GenerationDate,
			DocumentIDs.ProviderID,
			DocumentIDs.SubjectID AS ApplicantID,
			ClarityTrackingNumber.ClarityTrackingNumberVariableID,
			ClarityTrackingNumber.ClarityTrackingNumberVariableValue,
			ClearBank.ClearBankPrice,
			ClearBank.ClearBankVariableID,
			ClearBank.ClearBankVariableValue,
			ClearBankBehavior.ClearBankBehaviorPrice,
			ClearBankBehavior.ClearBankBehaviorVariableID,
			ClearBankBehavior.ClearBankBehaviorVariableValue,
			ClearFraud.ClearFraudPrice,
			ClearFraud.ClearFraudVariableID,
			ClearFraud.ClearFraudVariableValue,
			ClearTradeline.ClearTradelinePrice,
			ClearTradeline.ClearTradelineVariableID,
			ClearTradeline.ClearTradelineVariableValue
FROM		DocumentIDs
LEFT JOIN	ClarityTrackingNumber ON ClarityTrackingNumber.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearBank ON ClearBank.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearBankBehavior ON ClearBankBehavior.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearFraud ON ClearFraud.DocumentID = DocumentIDs.DocumentID
LEFT JOIN	ClearTradeline ON ClearTradeline.DocumentID = DocumentIDs.DocumentID


