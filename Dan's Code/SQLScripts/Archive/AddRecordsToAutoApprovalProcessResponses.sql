--Query to update missing records on AutoApprovalProcessResponses

SET Transaction Isolation Level Read Uncommitted;

WITH	
		MissingIDs AS
		(
			SELECT		DISTINCT ApprovalProviderResponseLog.ApplicantID
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			LEFT JOIN	AutoApprovalProcessResponses ON AutoApprovalProcessResponses.ApplicantID = ApprovalProviderResponseLog.ApplicantID
			WHERE		AutoApprovalProcessResponses.ApplicantID IS NULL
		),
		ApprovalProviderResponseLogRecords AS
		(
			SELECT		ROW_NUMBER()	OVER (PARTITION BY ApprovalProviderResponseLog.ApplicantID,ApprovalProviderResponseLog.ApprovalProviderSettingsID
										ORDER BY ApprovalProviderResponseLog.ApplicantID, ApprovalProviderResponseLog.CreatedDate DESC) AS SequenceNumber,
						ApprovalProviderResponseLog.ApplicantID,
						ApprovalProviderResponseLog.ApprovalProviderSettingsID,
						ApprovalProviderResponseLog.CreatedDate,
						ApprovalProviderResponseLog.Decision,
						ApprovalProviderResponseLog.DenyDescription,
						ApprovalProviderResponseLog.ExceptionDescription,
						ApprovalProviderResponseLog.TrackingID, --Clarity
						ApprovalProviderResponseLog.URI, -- MicroBilt
						ApprovalProviderResponseLog.InquiryID -- Datax
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			INNER JOIN	MissingIDs ON MissingIDs.ApplicantID = ApprovalProviderResponseLog.ApplicantID			
		),
		ClarityRecords AS
		(
			SELECT		ApplicantID,
						TrackingID AS ClarityTracking,
						CASE	WHEN Decision IN ('Approve','Approved') THEN 1
								WHEN Decision IN ('Deny','Denied') THEN 2
								WHEN Decision = 'Exception' THEN 3
								WHEN Decision = 'SoftKillDenied' THEN 4
								WHEN Decision = 'HardKillDenied' THEN 5
								ELSE NULL END AS ClarityResponse,
						CASE	WHEN Decision NOT IN ('Approve','Approved','Exception') THEN DenyDescription
								WHEN Decision = 'Exception' THEN ExceptionDescription
								ELSE NULL END AS ClarityResponseDescription,
						CreatedDate AS ClarityRequestDate
			FROM		ApprovalProviderResponseLogRecords
			WHERE		SequenceNumber = 1
						AND ApprovalProviderSettingsID = 1
		),
		DataXRecords AS
		(
			SELECT		ApplicantID,
						NULL AS DataXTransactionID,  -- this is null on the ApprovalProviderResponseLog table
						CASE	WHEN Decision IN ('Approve','Approved') THEN 1
								WHEN Decision IN ('Deny','Denied') THEN 2
								WHEN Decision = 'Exception' THEN 3
								WHEN Decision = 'SoftKillDenied' THEN 4
								WHEN Decision = 'HardKillDenied' THEN 5
								ELSE NULL END AS DataXResponse,
						CASE	WHEN Decision NOT IN ('Approve','Approved','Exception') THEN DenyDescription
								WHEN Decision = 'Exception' THEN ExceptionDescription
								ELSE NULL END AS DataXResponseDescription,
						InquiryID AS DataXInquiryID,
						CreatedDate AS DataXRequestDate
			FROM		ApprovalProviderResponseLogRecords
			WHERE		SequenceNumber = 1
						AND ApprovalProviderSettingsID = 2
		)

		SELECT		MissingIDs.ApplicantID,
					ClarityRecords.ClarityTracking,
					ClarityRecords.ClarityResponse,
					ClarityRecords.ClarityResponseDescription,
					ClarityRecords.ClarityRequestDate,
					DataXRecords.DataXTransactionID,
					DataXRecords.DataXResponse,
					DataXRecords.DataXResponseDescription,
					DataXRecords.DataXRequestDate,
					NULL AS MicroBiltRequestUID,
					NULL AS MicroBiltResponse,
					NULL AS MicroBiltResponseDescription,
					NULL AS MicroBiltRequestDate,
					NULL AS CreditsUsageHistoryID,
					DataXRecords.DataXInquiryID AS DataXInquiryID,
					NULL AS FactorTrustClientTransactionID,
					NULL AS FactorTrustResponse,
					NULL AS FactorTrustResponseDescription,
					NULL AS FactorTrustRequestDate,
					NULL AS ApprovalPath,
					NULL AS EBureauHardDenied,
					NULL AS EBureauRequestDate,
					NULL AS TeletrackCoreClientTransactionID,
					NULL AS TeletrackCoreResponse,
					NULL AS TeletrackCoreResponseDescription,
					NULL AS TeletrackCoreRequestDate,
					NULL AS NeustarTransactionID,
					NULL AS NeustarRequestDate,
					NULL AS NeustarResponse
		INTO		#NewRecords
		FROM		MissingIDs
		LEFT JOIN	ClarityRecords ON ClarityRecords.ApplicantID = MissingIDs.ApplicantID
		LEFT JOIN	DataXRecords ON DataXRecords.ApplicantID = MissingIDs.ApplicantID

		SELECT * FROM #NewRecords

		--INSERT INTO AutoApprovalProcessResponses
		--(		
		--	ApplicantID,
		--	  ClarityTracking,
		--	  ClarityResponse,
		--	  ClarityResponseDescription,
		--	  ClarityRequestDate,
		--	  DataXTransactionID,
		--	  DataXResponse,
		--	  DataXResponseDescription,
		--	  DataXRequestDate,
		--	  MicroBiltRequestUID,
		--	  MicroBiltResponse,
		--	  MicroBiltResponseDescription,
		--	  MicroBiltRequestDate,
		--	  CreditsUsageHistoryID,
		--	  DataXInquiryID,
		--	  FactorTrustClientTransactionID,
		--	  FactorTrustResponse,
		--	  FactorTrustResponseDescription,
		--	  FactorTrustRequestDate,
		--	  ApprovalPath,
		--	  EBureauHardDenied,
		--	  EBureauRequestDate,
		--	  TeletrackCoreClientTransactionID,
		--	  TeletrackCoreResponse,
		--	  TeletrackCoreResponseDescription,
		--	  TeletrackCoreRequestDate,
		--	  NeustarTransactionID,
		--	  NeustarRequestDate,
		--	  NeustarResponse
		--)	

		DROP TABLE #NewRecords
		


		--***NOTES***
		--SELECT * FROM MissingIDs -- ************there are 48 records here
		--SELECT	DISTINCT ApprovalProviderSettingsID	-- **************this shows that the only ApprovalProviderSettingsIDs are 1,2,5 ****PurePredictive(5) isn't on the AutoApprovalProcessResponses			
		--FROM ApprovalProviderResponseLogRecords