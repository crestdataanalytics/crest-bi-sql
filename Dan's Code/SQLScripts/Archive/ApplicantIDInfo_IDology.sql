SET Transaction Isolation Level Read Uncommitted;

SET Transaction Isolation Level Read Uncommitted;

SELECT		Addresses.ID AS AddressID,
			ApplicantPhone.ID AS PhoneID,
			Applicants.ID AS ApplicantID,
			REPLACE(Applicants.FirstName,',','') AS NameFirst, 
			REPLACE(Applicants.LastName,',','') AS NameLast,
			REPLACE(REPLACE(Addresses.StreetLine1, ',', ''), '*','') as Street1, 
			REPLACE(REPLACE(Addresses.StreetLine2, ',', ''), '*','') as Street2, 
			REPLACE(Addresses.City, ',','') AS City,
			Addresses.StateID as [State], 
			Addresses.PostalCode as Zip, 
			CAST(Applicants.DateOfBirth as date) as DOB,
			CAST(Applicants.SocialSecurityNumber AS VARCHAR(20)) AS SSN, 			
			ApplicantPhone.PhoneNumber AS Phone,
			CASE WHEN ApplicantPhone.PhoneNumber = Applicants.CellPhone THEN 1 ELSE 0 END AS MobileNumber, 
			REPLACE(Applicants.DriversLicenseNumber,',','') as DriverLicenseNumber, 
			Applicants.DriversLicenseStateID as DriverLicenseState
FROM		Applicants
INNER JOIN	Loans ON Loans.ApplicantID = Applicants.ID
LEFT JOIN	ApplicantAddresses ON Applicants.ID = ApplicantAddresses.ApplicantID
LEFT JOIN	Addresses ON Addresses.ID = ApplicantAddresses.AddressID
LEFT JOIN	ApplicantPhone ON ApplicantPhone.ApplicantID = Applicants.ID
WHERE		Loans.CreationDate >= '2016-02-20'
