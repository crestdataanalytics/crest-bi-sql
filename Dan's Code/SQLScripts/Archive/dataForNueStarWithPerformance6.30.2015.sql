select distinct vw_loans_reporting.ApplicantID, vw_loans_reporting.ID as LoanID, vw_loans_reporting.DisplayID,
	Applicants.FirstName, Applicants.LastName, 
	vw_loans_reporting.FundedAmount as FinancedAmount, vw_loans_reporting.TotalNote, vw_loans_reporting.FundedDate, 
	cast(vw_loans_reporting.CreationDate as date) CreationDate,
	case
			when ReturnedPayments.IsReturned = 1 THEN 1
			ELSE 0 END AS FirstPaymentDefault
from vw_loans_reporting
inner join Applicants on Applicants.ID = vw_loans_reporting.ApplicantID
inner join ApplicantAddresses on Applicants.ID = ApplicantAddresses.ApplicantID
inner join Addresses on Applicants.AddressID = Addresses.ID 
inner join States on States.ID = Addresses.StateID
inner join ApplicantEmailAddresses on vw_loans_reporting.ApplicantID = ApplicantEmailAddresses.ApplicantID
inner join LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
LEFT JOIN	(SELECT		LoanPayments.LoanID, sum(LoanPayments.Amount) AS TotalAmountPaid, MIN(PaymentDate) AS FirstPaymentDate, 
						MIN(LoanPayments.ID) as PaymentID
			FROM		LoanPayments
			INNER JOIN	LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
			WHERE		PaymentDate is not null and IsDeleted = 0
						AND LoanPaymentTypes.[Type] NOT IN ('Cash','Initial Payment', 'Paid By Dealer','Refund')
			GROUP BY	LoanID) AmountPaid on vw_loans_reporting.ID = AmountPaid.LoanID
LEFT JOIN	
			(SELECT PaymentID, processeddate,
					case when ReturnCode IN ('R02','R05', 'R07', 'R08', 'R10', 'R16') then 1
					else 0 end as FirstPaymentFatalReturnIndicator,
					1 as IsReturned
			FROM	WellsFargoReturns	
			) ReturnedPayments on ReturnedPayments.PaymentID = AmountPaid.PaymentID
LEFT JOIN
			(SELECT  LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
			FROM    LoanPaymentSchedule
			WHERE ScheduleEntryDesc not in ('Initial Payment', '')
			GROUP BY LoanID) FirstDueDate
			on FirstDueDate.LoanID = vw_loans_reporting.ID

where vw_loans_reporting.CreationDate >= '01/10/2015'
 --and vw_loans_reporting.ApplicantID%6 = 0
 and ApplicantEmailAddresses.IsPrimary = 1 and ApplicantEmailAddresses.Invalid = 0
 and (Applicants.CellPhone is not null or Applicants.HomePhone is not null)
 and (Applicants.CellPhone not in ('0','0000000000') or Applicants.HomePhone not in ('0','0000000000'))
 and Addresses.Invalid = 0
 and Applicants.IsDeleted = 0
 and LoanStatuses.Status not in ('Returned','Exchange','DealerReturn','Cancelled')
 --and vw_loans_reporting.ApplicantID = '852378'

 --group by LoanStatuses.Status

 

