SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @StartDate AS DATE = '03/1/2015'
DECLARE @EndDate AS DATE = '03/31/2015'

SELECT @EndDate = DATEADD(dd,1,@EndDate)
SELECT 
	-- This is a rare scenario, where wf schedule DueDate is not found in given date range --
	CASE 
	WHEN A.SheduledDate IS NOT NULL THEN A.SheduledDate
	WHEN B.CollectedDate IS NOT NULL THEN B.CollectedDate
	WHEN C.FundedDate IS NOT NULL THEN C.FundedDate
	WHEN D.ReturnedDate IS NOT NULL THEN D.ReturnedDate
	END AS [Date], 
	A.Scheduled, B.Collected, C.Funded, D.Returned FROM
	
-- Scheduled Payments
(SELECT DATEADD(DAY, DATEDIFF(DAY, 0, wf.DueDate), 0) AS 'SheduledDate', SUM(wf.DueAmount) as 'Scheduled' 
FROM WFLoanPaymentSchedule AS wf
WHERE wf.DueDate >= @StartDate AND wf.DueDate < @EndDate AND wf.IsPreview = 0 And wf.IsDeleted=0
GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, wf.DueDate), 0) ) A 

FULL JOIN 

-- Payments Collected
(SELECT DATEADD(DAY, DATEDIFF(DAY, 0, lp.PaymentDate), 0) AS 'CollectedDate', SUM(lp.Amount) as 'Collected' 
FROM LoanPayments AS lp
	--inner join Loans on Loans.ID = lp.LoanID  --NOT IN ACTUAL CASH FLOW REPORT BUT SHOULD BE
	--inner join LoanStatuses on Loans.StatusID = LoanStatuses.ID  --NOT IN ACTUAL CASH FLOW REPORT BUT SHOULD BE
WHERE lp.PaymentDate >= @StartDate AND lp.PaymentDate < @EndDate and IsDeleted=0
	--and LoanStatuses.Status not in ('Cancelled')  --NOT IN ACTUAL CASH FLOW REPORT BUT SHOULD BE

GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, lp.PaymentDate), 0) ) B on A.SheduledDate=B.CollectedDate

FULL JOIN

-- Payments Funded
(SELECT DATEADD(DAY, DATEDIFF(DAY, 0, l.FundedDate) , 0) AS 'FundedDate', SUM(l.FundedAmount) as 'Funded' 
FROM Loans AS l 
WHERE l.FundedDate >= @StartDate AND l.FundedDate < @EndDate 
GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, l.FundedDate), 0) ) C ON A.SheduledDate= C.FundedDate

FULL JOIN

-- Payments Returned
(SELECT DATEADD(DAY, DATEDIFF(DAY, 0, lp.ReturnDate), 0) AS 'ReturnedDate', SUM(lp.Amount) as 'Returned' 
FROM LoanPayments AS lp
WHERE lp.ReturnDate >= @StartDate AND lp.ReturnDate < @EndDate AND lp.Returned = 1 and IsDeleted=0
GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, lp.ReturnDate), 0) )D ON A.SheduledDate=D.ReturnedDate
ORDER BY A.SheduledDate

