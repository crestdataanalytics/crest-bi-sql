/****** Script for SelectTopNRows command from SSMS  ******/
SELECT	[ID] AS RetailerID
      ,[FundedCount]
      ,[FundedPercent]
      ,[FundedAmount]
      ,[FaceValue]
      ,[Ranking] -- log of ranking column on dealers, not the 10 month or 18 month
      ,[Weight]
      ,[ActiveAccounts]
      ,[ActivePercent]
      ,[Loans40Days]
      ,[Loans85Days]
      ,[Bounced85Days]
      ,[Clarity40Days]
      ,[Clarity85Days]
      ,[ClarityBounced85Days]
      ,[AutoApprovedSubmitted40Days]
      ,[Submitted85DaysNoPayment]
      ,[AutoApprovedCount]
      ,[90DayCustomers]
      ,[ChargedOffCount]
      ,[BouncedCount]
      ,[TotalCollected]
      ,[TotalCostOfMoney]
      ,[MonthsBack]
      ,[Bounced40Days]
      ,CAST([EndDate] AS DATE) AS EndDate
	  ,MONTH(EndDate) AS [Month]
	  ,YEAR(EndDate) AS [Year]
  FROM [CrestWarehouse].[dbo].[DealerRankingHistory]
  --where id = 1173 and enddate >= '2016-08-01' and ranking is not null
  --order by enddate
  