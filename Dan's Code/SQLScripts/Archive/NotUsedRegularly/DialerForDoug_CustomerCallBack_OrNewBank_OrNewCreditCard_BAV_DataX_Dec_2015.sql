SET Transaction Isolation Level Read Uncommitted;

WITH
		--LeasesWithNotes AS
		--(
		--	SELECT		Notes.LoanID,
		--				CAST(Notes.NoteContent AS varchar(50)) NoteContent,
		--				Notes.CreationDate,
		--				Loans.DisplayID,
		--				Loans.FundedDate
		--	FROM		Notes
		--	INNER JOIN	Loans ON Notes.LoanID = Loans.ID
		--	WHERE		Notes.CreationDate >= '2016-01-11 1:10:00'
		--				AND Notes.NoteContent like 'VERIFY AND CORRECT ALL INFORMATION on this customers account when customer calls in!'
		--	--order by applicantid, creationdate desc
		--),

		LeaseNotes AS
		(
			SELECT		Notes.LoanID,
						CAST(Notes.NoteContent AS varchar(50)) NoteContent,
						Notes.CreationDate,
						Loans.DisplayID,
						Loans.FundedDate
			FROM		Notes
			INNER JOIN	Loans ON Loans.ID = Notes.LoanID
			WHERE		Notes.CreationDate > '2016-01-11 1:10:00'
		)

		SELECT		LeaseNotes.LoanID,
					LeaseNotes.DisplayID,
					LeaseNotes.CreationDate
					--LeaseNotes.NoteContent --this is so Doug can audit the notes, but leaving it off for spinning data
		FROM		LeaseNotes
		--INNER JOIN	LeaseNotes ON LeaseNotes.LoanID = LeasesWithNotes.LoanID
		WHERE		(LOWER(LeaseNotes.NoteContent) LIKE '%call%'
					OR LOWER(LeaseNotes.NoteContent) LIKE '%inbound%'
					OR LOWER(LeaseNotes.NoteContent) LIKE '%verified%'
					OR LOWER(LeaseNotes.NoteContent) LIKE '%Payment Promise%'
					--OR LeaseNotes.NoteContent LIKE '%call%' -- doug will let me know if this is needed
					OR LeaseNotes.NoteContent LIKE '%New Bank Account:%'
					OR LeaseNotes.NoteContent LIKE '%New Credit Card created%')
					AND LeaseNotes.FundedDate <= LeaseNotes.CreationDate


		--select * from loans where displayid = '1136321-1'