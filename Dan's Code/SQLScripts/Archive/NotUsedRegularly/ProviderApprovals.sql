
WITH TOTAL AS
(
	SELECT		CAST(Loans.CreationDate AS date) AS CreationDate,
				Loans.ID AS LoanID,
				Loans.ApplicantID			
	FROM		Loans
	INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
	WHERE		CAST(Loans.CreationDate AS date) >= '2016-05-25'
				AND NOT EXISTS(SELECT 1 FROM vw_loans_reporting ll WHERE ll.ApplicantID = Loans.ApplicantID AND ll.StatusID = 21)
				AND Dealers.IsDemoAccount = 0
				--AND StatusID NOT IN (11,21)
	--GROUP BY	CAST(Loans.CreationDate AS date)
),

DataXApproval AS
(
	SELECT		CAST(CreatedDate AS date) AS CreatedDate,
				ApprovalProviderResponseLog.ApplicantID
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	LEFT JOIN	Loans ON Loans.ApplicantID = ApprovalProviderResponseLog.ApplicantID
	INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID	
	WHERE		ApprovalProviderSettingsID = 2
				AND CAST(CreatedDate AS date) >= '2016-05-25'
				AND Decision IN ('Approve','Approved')
				AND Dealers.IsDemoAccount = 0
	--GROUP BY	CAST(CreatedDate AS date)
),

Clarity AS
(
	SELECT		CAST(CreatedDate AS date) AS CreatedDate,
				ApprovalProviderResponseLog.ApplicantID
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	LEFT JOIN	Loans ON Loans.ApplicantID = ApprovalProviderResponseLog.ApplicantID
	INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
	WHERE		ApprovalProviderSettingsID = 1
				AND CAST(CreatedDate AS date) >= '2016-05-25'
				AND Decision IN ('Approve','Approved')
				AND Dealers.IsDemoAccount = 0
				AND NOT EXISTS(SELECT 1 FROM DataXApproval  WHERE DataXApproval.ApplicantID = ApprovalProviderResponseLog.ApplicantID AND ApprovalProviderSettingsID=1)
	--GROUP BY	CAST(CreatedDate AS date)
),

Microbilt AS
(
	SELECT		CAST(CreatedDate AS date) AS CreatedDate,
				ApprovalProviderResponseLog.ApplicantID
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	LEFT JOIN	Loans ON Loans.ApplicantID = ApprovalProviderResponseLog.ApplicantID
	INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
	WHERE		ApprovalProviderSettingsID = 3
				AND CAST(CreatedDate AS date) >= '2016-05-25'
				AND Decision IN ('Approve','Approved')
				AND NOT EXISTS(SELECT 1 FROM vw_loans_reporting ll WHERE ll.ApplicantID = Loans.ApplicantID AND ll.StatusID = 21)
				AND Dealers.IsDemoAccount = 0
				AND NOT EXISTS(SELECT 1 FROM DataXApproval  WHERE DataXApproval.ApplicantID = ApprovalProviderResponseLog.ApplicantID AND ApprovalProviderSettingsID=1)
	--GROUP BY	CAST(CreatedDate AS date)
),

FactorTrust AS
(
	SELECT		CAST(CreatedDate AS date) AS CreatedDate,
				ApprovalProviderResponseLog.ApplicantID
	FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
	LEFT JOIN	Loans ON Loans.ApplicantID = ApprovalProviderResponseLog.ApplicantID
	INNER JOIN	Dealers ON Dealers.ID = Loans.DealerID
	WHERE		ApprovalProviderSettingsID = 4
				AND CAST(CreatedDate AS date) >= '2016-05-25'
				AND Decision IN ('Approve','Approved')
				AND NOT EXISTS(SELECT 1 FROM vw_loans_reporting ll WHERE ll.ApplicantID = Loans.ApplicantID AND ll.StatusID = 21)
				AND Dealers.IsDemoAccount = 0
				AND NOT EXISTS(SELECT 1 FROM DataXApproval  WHERE DataXApproval.ApplicantID = ApprovalProviderResponseLog.ApplicantID AND ApprovalProviderSettingsID=1)
	--GROUP BY	CAST(CreatedDate AS date)
)

SELECT		Total.CreationDate,
			COUNT(DISTINCT Total.ApplicantID) AS Total,
			COUNT(DISTINCT DataXApproval.ApplicantID) AS DataXApproval,
			COUNT(DISTINCT Clarity.ApplicantID) AS ClarityApproval,
			COUNT(DISTINCT Microbilt.ApplicantID) AS MicrobiltApproval,
			COUNT(DISTINCT FactorTrust.ApplicantID) AS FactorTrustApproval
			--Total.CurrentTotalApps,
			--DataXApproval.DataXTotal,
			--CAST(DataXApproval.DataXTotal AS decimal) / total.CurrentTotalApps * 100 AS DataXApprovalPercent,
			--Clarity.ClarityTotal,
			--CAST(Clarity.ClarityTotal AS decimal) / total.CurrentTotalApps * 100 AS ClarityApprovalPercent,
			--Microbilt.MicroBiltTotal,
			--CAST(Microbilt.MicroBiltTotal AS decimal) / total.CurrentTotalApps * 100 AS MicroBiltApprovalPercent,
			--FactorTrust.FactorTrustTotal,
			--CAST(FactorTrust.FactorTrustTotal AS decimal) / total.CurrentTotalApps * 100 AS FactorTrustApprovalPercent
FROM		TOTAL
LEFT JOIN	DataXApproval ON TOTAL.CreationDate = DataXApproval.CreatedDate and total.ApplicantID = DataXApproval.ApplicantID
LEFT JOIN	Clarity ON TOTAL.CreationDate = Clarity.CreatedDate and total.ApplicantID = Clarity.ApplicantID
LEFT JOIN	Microbilt ON TOTAL.CreationDate = Microbilt.CreatedDate and total.ApplicantID = Microbilt.ApplicantID
LEFT JOIN	FactorTrust ON TOTAL.CreationDate = FactorTrust.CreatedDate and total.ApplicantID = FactorTrust.ApplicantID
GROUP BY	Total.CreationDate