SET Transaction Isolation Level Read Uncommitted;

SELECT		Loans.DisplayID,
			LoanPaymentSchedule.DueDate,
			LoanPayments.PaymentDate,
			LoanPayments.ReturnDate	
FROM		Loans
LEFT JOIN	LoanPaymentSchedule ON LoanPaymentSchedule.LoanID = Loans.ID
LEFT JOIN	LoanPayments ON LoanPayments.LoanID = Loans.ID
INNER JOIN	LoanPaymentTypes ON LoanPayments.TypeID = LoanPaymentTypes.ID
WHERE		LoanPaymentSchedule.ScheduleEntryDesc != 'Initial Payment'
			AND LoanPaymentSchedule.IsPreview = 0
			AND LoanPayments.PaymentDate is not null and IsDeleted = 0
			AND LoanPaymentTypes.[Type] NOT IN ('Initial Payment','Paid By Dealer','Refund')
			AND Loans.ID = 924212
			--AND LoanPaymentSchedule.DueDate = LoanPayments.PaymentDate
ORDER BY	Loans.ID,
			LoanPaymentSchedule.DueDate,
			LoanPayments.PaymentDate

select *
from LoanPaymentSchedule
where loanid = 924212
order by DueDate