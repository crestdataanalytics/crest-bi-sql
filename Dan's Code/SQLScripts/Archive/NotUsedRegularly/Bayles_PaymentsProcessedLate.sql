SELECT		YEAR(WFLoanPaymentSchedule.DueDate) AS [Year],
			MONTH(WFLoanPaymentSchedule.DueDate) AS [Month],
			COUNT(WFLoanPaymentSchedule.LoanID)	AS Total		
FROM		WFLoanPaymentSchedule
LEFT JOIN	LoanPayments ON WFLoanPaymentSchedule.ID = LoanPayments.ScheduledLoanPaymentID
WHERE		LoanPayments.DeletedDate IS NULL
			AND YEAR(WFLoanPaymentSchedule.DueDate) >= 2015
			AND WFLoanPaymentSchedule.DueDate < '2016-06-15'
			AND WFLoanPaymentSchedule.DueAmount >= 0
			AND WFLoanPaymentSchedule.DeletedDate IS NULL
			AND LoanPayments.ID IS NOT NULL
			AND CAST(WFLoanPaymentSchedule.DueDate AS date) < CAST(LoanPayments.PaymentDate AS date)
GROUP BY	YEAR(WFLoanPaymentSchedule.DueDate),
			MONTH(WFLoanPaymentSchedule.DueDate)
ORDER BY	1,2