SET Transaction Isolation Level Read Uncommitted;

Declare @startDate date,
		@endDate date
SET @startDate = '01/01/2015';
SET	@endDate = '12/01/2015';

SELECT		PaymentPromises.LoanID,
			cast(PaymentPromises.PromiseDate AS DATE) AS PromiseDate,
			PaymentPromises.CreationDate
FROM		PaymentPromises
WHERE		PaymentPromises.CreationDate >= @startDate
			AND PaymentPromises.CreationDate < @endDate