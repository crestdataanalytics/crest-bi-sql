SET Transaction Isolation Level Read Uncommitted;


WITH

		TotalInquiries AS
		(
			SELECT		YEAR(CreationDate) AS [Year],
						MONTH(CreationDate) AS [Month],
						DAY(CreationDate) AS [Day],
						DATEPART(HOUR,CreationDate) AS [Hour],
						COUNT(ID) TotalInquiries,
						1 AS Total

			FROM		Loans
			WHERE		CreationDate >= '2015-12-01'
			GROUP BY	YEAR(CreationDate),
						MONTH(CreationDate),
						DAY(CreationDate),
						DATEPART(HOUR,CreationDate)
		),

		AutoApproved AS
		(
			SELECT		YEAR(CreationDate) AS [Year],
						MONTH(CreationDate) AS [Month],
						DAY(CreationDate) AS [Day],
						DATEPART(HOUR,CreationDate) AS [Hour],
						COUNT(ID) AutoApprovedTotal,
						1 AS Total
			FROM		Loans
			WHERE		CreationDate >= '2015-12-01'
						AND AutoApprovedFlag = 1
						AND ApprovalDate IS NOT NULL
			GROUP BY	YEAR(CreationDate),
						MONTH(CreationDate),
						DAY(CreationDate),
						DATEPART(HOUR,CreationDate)
		),

		ManuallyApproved AS
		(
			SELECT		YEAR(CreationDate) AS [Year],
						MONTH(CreationDate) AS [Month],
						DAY(CreationDate) AS [Day],
						DATEPART(HOUR,CreationDate) AS [Hour],
						COUNT(ID) ManuallyApprovedTotal,
						1 AS Total
			FROM		Loans
			WHERE		CreationDate >= '2015-12-01'
						AND AutoApprovedFlag = 0
						AND ApprovalDate IS NOT NULL
			GROUP BY	YEAR(CreationDate),
						MONTH(CreationDate),
						DAY(CreationDate),
						DATEPART(HOUR,CreationDate)
		),
		
		DataXLooks AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) DataXLooksTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 2
						AND CreatedDate >= '2015-12-01'
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		),

		DataXYes AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) DataXYesTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 2
						AND CreatedDate >= '2015-12-01'
						AND Decision IN ('Approve','Approved')
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		),

		ClarityLooks AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) ClarityLooksTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 1
						AND CreatedDate >= '2015-12-01'
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		),

		ClarityYes AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) ClarityYesTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 1
						AND CreatedDate >= '2015-12-01'
						AND Decision IN ('Approve','Approved')
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		),

		MicroBiltLooks AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) MicroBiltLooksTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 3
						AND CreatedDate >= '2015-12-01'
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		),

		MicroBiltYes AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) MicroBiltYesTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 3
						AND CreatedDate >= '2015-12-01'
						AND Decision IN ('Approve','Approved')
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		),

		FactorTrustLooks AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) FactorTrustLooksTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 4
						AND CreatedDate >= '2015-12-01'
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		),

		FactorTrustYes AS
		(
			SELECT		YEAR(CreatedDate) AS [Year],
						MONTH(CreatedDate) AS [Month],
						DAY(CreatedDate) AS [Day],
						DATEPART(HOUR,CreatedDate) AS [Hour],
						COUNT(ApplicantID) FactorTrustYesTotal,
						1 AS Total
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 4
						AND CreatedDate >= '2015-12-01'
						AND Decision IN ('Approve','Approved')
			GROUP BY	YEAR(CreatedDate),
						MONTH(CreatedDate),
						DAY(CreatedDate),
						DATEPART(HOUR,CreatedDate)
		)
		SELECT		TotalInquiries.Year,
					TotalInquiries.Month,
					TotalInquiries.Day,
					TotalInquiries.Hour,
					TotalInquiries.TotalInquiries,
					CAST(AutoApproved.AutoApprovedTotal AS decimal) + CAST(ISNULL(ManuallyApproved.ManuallyApprovedTotal,0) AS decimal) AS TotalApproved,
					AutoApproved.AutoApprovedTotal,
					CAST(AutoApproved.AutoApprovedTotal AS decimal) / (AutoApproved.AutoApprovedTotal + CAST(ISNULL(ManuallyApproved.ManuallyApprovedTotal,0) AS decimal)) AS AutoApprovedPercent,
					ManuallyApproved.ManuallyApprovedTotal,
					CAST(ManuallyApproved.ManuallyApprovedTotal AS decimal) / TotalInquiries.TotalInquiries AS ManuallyApprovedPercent,
					DataXLooks.DataXLooksTotal,
					DataXYes.DataXYesTotal,
					ClarityLooks.ClarityLooksTotal,
					ClarityYes.ClarityYesTotal,
					MicroBiltLooks.MicroBiltLooksTotal,
					MicroBiltYes.MicroBiltYesTotal,
					FactorTrustLooks.FactorTrustLooksTotal,
					FactorTrustYes.FactorTrustYesTotal
		FROM		TotalInquiries
		LEFT JOIN	AutoApproved ON AutoApproved.Day = TotalInquiries.Day
								AND AutoApproved.Month = TotalInquiries.Month
								AND AutoApproved.Year = TotalInquiries.Year
								AND AutoApproved.Hour = TotalInquiries.Hour
		LEFT JOIN	ManuallyApproved ON ManuallyApproved.Day = TotalInquiries.Day
								AND ManuallyApproved.Month = TotalInquiries.Month
								AND ManuallyApproved.Year = TotalInquiries.Year
								AND ManuallyApproved.Hour = TotalInquiries.Hour
		LEFT JOIN	DataXYes ON DataXYes.Day = TotalInquiries.Day
								AND DataXYes.Month = TotalInquiries.Month
								AND DataXYes.Year = TotalInquiries.Year
								AND DataXYes.Hour = TotalInquiries.Hour
		LEFT JOIN	ClarityYes ON ClarityYes.Day = TotalInquiries.Day
								AND ClarityYes.Month = TotalInquiries.Month
								AND ClarityYes.Year = TotalInquiries.Year
								AND ClarityYes.Hour = TotalInquiries.Hour
		LEFT JOIN	ClarityLooks ON ClarityLooks.Day = TotalInquiries.Day
								AND ClarityLooks.Month = TotalInquiries.Month
								AND ClarityLooks.Year = TotalInquiries.Year
								AND ClarityLooks.Hour = TotalInquiries.Hour
		LEFT JOIN	DataXLooks ON DataXLooks.Day = TotalInquiries.Day
								AND DataXLooks.Month = TotalInquiries.Month
								AND DataXLooks.Year = TotalInquiries.Year
								AND DataXLooks.Hour = TotalInquiries.Hour
		LEFT JOIN	MicroBiltYes ON MicroBiltYes.Day = TotalInquiries.Day
								AND MicroBiltYes.Month = TotalInquiries.Month
								AND MicroBiltYes.Year = TotalInquiries.Year
								AND MicroBiltYes.Hour = TotalInquiries.Hour
		LEFT JOIN	MicroBiltLooks ON MicroBiltLooks.Day = TotalInquiries.Day
								AND MicroBiltLooks.Month = TotalInquiries.Month
								AND MicroBiltLooks.Year = TotalInquiries.Year
								AND MicroBiltLooks.Hour = TotalInquiries.Hour
		LEFT JOIN	FactorTrustYes ON FactorTrustYes.Day = TotalInquiries.Day
								AND FactorTrustYes.Month = TotalInquiries.Month
								AND FactorTrustYes.Year = TotalInquiries.Year
								AND FactorTrustYes.Hour = TotalInquiries.Hour
		LEFT JOIN	FactorTrustLooks ON FactorTrustLooks.Day = TotalInquiries.Day
								AND FactorTrustLooks.Month = TotalInquiries.Month
								AND FactorTrustLooks.Year = TotalInquiries.Year
								AND FactorTrustLooks.Hour = TotalInquiries.Hour
		ORDER BY	1 DESC,2 DESC,3 DESC,4 DESC
