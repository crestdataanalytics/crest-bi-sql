DECLARE @cutoffdate as datetime
select @cutoffdate = '11/1/2015'


;with GardnerWhiteDealerIDs AS
(
	SELECT ID AS DealerID 
	FROM Dealers 
	WHERE Name LIKE '%Gardner%'
)


select l.ID, l.DisplayID, l.DealerID, l.FundedAmount, CAST(l.CreationDate as date) as SubmitDate, l.AutoApprovedFlag, 
CAST(l.ApprovalDate as Date) as ApprovedDate,
CAST(l.FundedDate as date) as FundedDate, l.Amount as LeaseAmount
into #templ
from vw_loans_reporting l
inner join GardnerWhiteDealerIDs ON GardnerWhiteDealerIDs.DealerID = l.DealerID
where l.CreationDate > @cutoffdate


;with FirstPayments as
(
    SELECT ROW_NUMBER() OVER(PARTITION BY LoanID ORDER BY PaymentDate) as RowNum, LoanID, PaymentDate, ReturnDate, 
    CASE WHEN ReturnDate IS NOT NULL THEN 1 ELSE 0 END as FPD,
    CASE WHEN PaymentDate <= CAST(GetDate() as Date) THEN 1 ELSE 0 END as PaymentsCharged,
    CASE WHEN PaymentDate <= CAST(DateAdd(dd, -7, GetDate()) as Date) THEN 1 ELSE 0 END as PastGrace
    FROM LoanPayments lp with(nolock)
    INNER JOIN #templ t on t.ID=lp.LoanID and lp.TypeID <> 9
)
select t.DisplayID, t.DealerID, t.FundedAmount, t.SubmitDate, 
t.AutoApprovedFlag, t.ApprovedDate, t.FundedDate, t.LeaseAmount,
CAST(p.FirstPaymentDate as date) as FirstPaymentDate,
CAST(PaymentDate as date) as PaymentDate,
CAST(ReturnDate as date) as ReturnDate,
CASE WHEN t.ApprovedDate is not null AND t.AutoApprovedFlag > 0 THEN 'AUTO'
     WHEN t.ApprovedDate is not null AND t.AutoApprovedFlag = 0 THEN 'MANUAL'
     WHEN t.ApprovedDate is null THEN 'NONE'
END as ApprovalType,
CASE WHEN FirstPaymentDate < CAST(GetDate() as Date) THEN 'DUE' ELSE null END as FirstPaymentDue,
PaymentsCharged,
PastGrace,
FPD
into #tempp
 from #templ t
left join (
    SELECT LoanID, MIN(DueDate) as FirstPaymentDate
    FROM LoanPaymentSchedule lp with(nolock)
    INNER JOIN #templ t on t.ID=lp.LoanID
    WHERE lp.ScheduleEntryDesc <> 'Initial Payment'
    GROUP BY LoanID
) p on p.LoanID=t.ID
left join FirstPayments fp on fp.RowNum=1 and fp.LoanID=t.ID

select CAST(SubmitDate as Date) as [Date],
Count(*) as SubmitCount,
SUM(CASE WHEN FundedDate is not null and FundedDate < CAST(GetDate() as Date) THEN 1 ELSE 0 END) As FundedCount,
SUM(CASE WHEN ApprovalType = 'AUTO' and ApprovedDate is not null THEN 1 ELSE 0 END) as AutoApprovedCount,
SUM(CASE WHEN ApprovalType = 'MANUAL' and ApprovedDate is not null THEN 1 ELSE 0 END) as ManualApprovedCount,
SUM(ISNULL(PaymentsCharged, 0)) as FirstPaymentDue,
SUM(ISNULL(PastGrace, 0)) as PastGraceCount,
SUM(ISNULL(FPD, 0)) as FPDCount
from #tempp
group by CAST(SubmitDate as Date)
order by 1

drop table #templ
drop table #tempp

