SET Transaction Isolation Level Read Uncommitted;

SELECT		WFLoanPaymentSchedule.LoanID,
			Loans.DisplayID,
			WFLoanPaymentSchedule.ID,
			LoanPayments.ScheduledLoanPaymentID,
			CASE WHEN LoanPayments.LoanID IS NULL THEN 0 ELSE 1 END AS LoanPaymentRecordPresent,
			WFLoanPaymentSchedule.ID AS WFLoanPaymentScheduleID,
			CAST(WFLoanPaymentSchedule.DueDate AS date) AS WFLPSDueDate,
			WFLoanPaymentSchedule.DueAmount AS WFLPSDueAmount,
			CAST(LoanPayments.PaymentDate AS date) AS PaymentDate,
			LoanPayments.Amount AS PaymentAmount,
			WFLoanPaymentSchedule.ProcessedDate,
			CASE WHEN CAST(WFLoanPaymentSchedule.DueDate AS date) < CAST(LoanPayments.PaymentDate AS date) THEN 1 ELSE 0 END AS ProcessedLate,
			LoanPaymentSchedule.DueDate AS LPSDueDate
FROM		WFLoanPaymentSchedule
INNER JOIN	Loans ON WFLoanPaymentSchedule.LoanID = Loans.ID
LEFT JOIN	LoanPayments ON WFLoanPaymentSchedule.ID = LoanPayments.ScheduledLoanPaymentID
LEFT JOIN	LoanPaymentSchedule ON WFLoanPaymentSchedule.LoanPaymentScheduleItemID = LoanPaymentSchedule.ID
WHERE		LoanPayments.DeletedDate IS NULL
			AND YEAR(WFLoanPaymentSchedule.DueDate) >= 2016
			AND WFLoanPaymentSchedule.DueDate < '2016-06-15'
			AND WFLoanPaymentSchedule.DueAmount >= 0
			AND WFLoanPaymentSchedule.DeletedDate IS NULL
			--AND LoanPayments.ScheduledLoanPaymentID = 6241310
			--AND Loans.ID = 922330
			--AND CAST(WFLoanPaymentSchedule.DueDate AS date) != CAST(LoanPaymentSchedule.DueDate AS date)
ORDER BY	WFLoanPaymentSchedule.DueDate ASC

--select *
--from LoanPayments
--where LoanID = 922330
--order by PaymentDate 

--select *
--from LoanPayments
--where ScheduledLoanPaymentID = 6241310

select *
from WFLoanPaymentSchedule
where ID = 9682637

select *
from LoanPaymentSchedule
where LoanID = 780813
order by DueDate 