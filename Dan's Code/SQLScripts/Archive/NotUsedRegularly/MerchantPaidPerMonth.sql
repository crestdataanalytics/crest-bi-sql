SET Transaction Isolation Level Read Uncommitted;

select year(fundeddate)
	, month(fundeddate)
	,sum(fundedamount)
from loans
inner join dealers on dealers.id = loans.dealerid
inner join loanStatuses on LoanStatuses.ID = Loans.StatusID
where FundedDate >= '10-01-2014'
	and approvaluserid = 0
	and dealers.IsDeleted = 0
	and dealers.IsDemoAccount = 0
	and FundedDate is not null
	and loanStatuses.[Status] in ('Bankrupt13','Bankrupt7','Bounced','Charged Off','Funded','Paid','Returned')
	and Loans.Amount != 0
	and Loans.FundedAmount != 0
group by month(fundeddate),
		year(fundeddate)
order by year(fundeddate), month(fundeddate)  asc


select year(fundeddate)
	, month(fundeddate)
	,sum(loanpayments.amount)
from loans
inner join dealers on dealers.id = loans.dealerid
inner join loanStatuses on LoanStatuses.ID = Loans.StatusID
inner join loanpayments on loanpayments.loanid = loans.id
INNER JOIN	LoanPaymentTypes ON LoanPayments.TypeID = LoanPaymentTypes.ID
where FundedDate >= '10-01-2014'
	and approvaluserid = 0
	and dealers.IsDeleted = 0
	and dealers.IsDemoAccount = 0
	and FundedDate is not null
	and loanStatuses.[Status] in ('Bankrupt13','Bankrupt7','Bounced','Charged Off','Funded','Paid','Returned')
	and Loans.Amount != 0
	and Loans.FundedAmount != 0
	and loanpayments.isdeleted = 0
	and loanPayments.returndate is null
	AND LoanPaymentTypes.[Type] NOT IN ('Initial Payment','Paid By Dealer','Refund')
group by month(fundeddate),
		year(fundeddate)
order by year(fundeddate), month(fundeddate)  asc








select year(fundeddate)
	, month(fundeddate)
	,count(Loans.ID)
from loans
inner join dealers on dealers.id = loans.dealerid
inner join loanStatuses on LoanStatuses.ID = Loans.StatusID
where approvaluserid = 0
	and FundedDate >= '10-01-2014'
	and dealers.IsDeleted = 0
	and dealers.IsDemoAccount = 0
	and FundedDate is not null
	and loanStatuses.[Status] in ('Bankrupt13','Bankrupt7','Bounced','Charged Off','Funded','Paid','Returned')
	and Loans.Amount != 0
	and Loans.FundedAmount != 0
group by month(fundeddate),
		year(fundeddate)
order by year(fundeddate), month(fundeddate)  asc

select ID as LoanID,
		cast(FundedDate as date) FundedDate
From	Loans
Where month(fundeddate) = 10
	and year(fundeddate) = 2014
