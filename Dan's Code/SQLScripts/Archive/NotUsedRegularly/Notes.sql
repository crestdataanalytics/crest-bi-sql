SET Transaction Isolation Level Read Uncommitted;

Declare @startDate date,
		@endDate date
SET @startDate = '10/01/2015';
SET	@endDate = '11/01/2015';

SELECT		Notes.LoanID,
			Notes.CreatedByID,
			REPLACE(Users.FirstName,',','') AS FirstName,
			REPLACE(Users.LastName,',','') AS LastName,
			Notes.CreationDate,
			Notes.WasContacted,
			REPLACE(REPLACE(REPLACE(CAST(Notes.NoteContent AS VARCHAR(5000)), ',',''),CHAR(10),' '),CHAR(13),' ') AS NoteContent,
			Notes.ID
			--select count(LoanID) 3.3MM notes
FROM		Notes
INNER JOIN	Users ON Notes.CreatedByID = Users.ID
WHERE		Notes.CreationDate >= @startDate
			AND Notes.CreationDate < @endDate
			--AND Notes.LoanID = 872785