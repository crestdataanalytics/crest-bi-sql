
SET Transaction Isolation Level Read Uncommitted;

select CAST(l.CreationDate as Date) as [Date],
ID AS LoanID, replace(DisplayID,'-','') as ID
into #tempd
from vw_loans_reporting l
where cast(l.CreationDate as date) >= '11/1/2015' --and cast(l.CreationDate as date) < '12/21/2015'


select t.*
from #tempd t
order by 1

drop table #tempd


select CAST(l.CreationDate as Date) as [Date],
Count(*) as SubmitCount,
SUM(CASE WHEN l.AutoApprovedFlag >= 1 and l.ApprovalDate is not null THEN 1 ELSE 0 END) as AutoApprovedCount,
SUM(CASE WHEN l.AutoApprovedFlag = 0 and l.ApprovalDate is not null THEN 1 ELSE 0 END) as ManualApprovedCount
into #tempd
from vw_loans_reporting l
where l.CreationDate >= '11/1/2015'
group by CAST(l.CreationDate as Date)

select t.*
from #tempd t
order by 1

drop table #tempd


