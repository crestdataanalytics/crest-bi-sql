

SELECT		ID AS ApprovalProviderResponseLogID,
			ApplicantID,
			CreatedDate,
			Decision,
			DenyDescription
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderResponseLog.Decision = 'HardKillDenied'
			AND ApprovalProviderResponseLog.ApprovalProviderSettingsID = 2
			AND ApprovalProviderResponseLog.CreatedDate > '2016-01-1'
			AND DenyDescription LIKE '%Denied for high risk account%'
--group by	DenyDescription
ORDER BY	CreatedDate ASC

