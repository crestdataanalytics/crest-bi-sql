select vw_loans_reporting.DisplayID, LoanStatuses.[Status], TotalMonthlyIncome.TotalMonthlyIncome, cast(vw_loans_reporting.FundedDate as Date) FundedDate, 
	vw_loans_reporting.FundedAmount
from vw_loans_reporting
inner join 
	(Select ApplicantID, Sum(MonthlyIncome) as TotalMonthlyIncome
	from ApplicantJob
	group by ApplicantID) as TotalMonthlyIncome
	on TotalMonthlyIncome.ApplicantID = vw_loans_reporting.ApplicantID
inner join LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
where vw_loans_reporting.FundedDate is not null
and ApplicantJob.Mont

select count(vw_loans_reporting.DisplayID)
from vw_loans_reporting
where vw_loans_reporting.FundedDate is not null and exists (select 1 from ApplicantJob where ApplicantJob.ApplicantID = vw_loans_reporting.ApplicantID)
