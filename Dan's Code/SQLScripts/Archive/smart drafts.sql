USE DealerManage
GO

WITH LeasePayments
AS
(
	--Ad-Hoc CreditCard failures
	SELECT 
		[LoanPaymentTransactions].CreatedDate AS PaymentDate,
		[LoanPaymentTransactions].LoanID,
		NULL AS PaidAmount,
		'Manual Credit Card' AS PaymentMethod,
		LoanHistory.StatusID AS LoanStatusID,
		1 AS Returned
	FROM CrestWarehouse.[dbo].[LoanPaymentTransactions]
		JOIN dbo.PaymentProcessors
			ON LoanPaymentTransactions.PaymentProcessorID = PaymentProcessors.ID
			AND PaymentProcessorType = 2    --credit cards
		LEFT JOIN dbo.LoanHistory
			ON LoanPaymentTransactions.LoanID = LoanHistory.LoanID
			AND LoanPaymentTransactions.CreatedDate BETWEEN LoanHistory.StartDate AND ISNULL(EndDate, CONVERT(DATETIME, '2099-12-31'))
	WHERE Response <> 1     --1 = success

	UNION ALL

	SELECT 
		PaymentDate,
		LoanID,
		Amount AS PaidAmount,
		[Type] AS PaymentMethod,
		LoanStatusID,
		Returned
	FROM dbo.LoanPayments
		JOIN dbo.LoanPaymentTypes
			ON LoanPayments.TypeID = LoanPaymentTypes.ID
	WHERE IsDeleted = 0
		AND Amount > 0
)
SELECT 
	DisplayID,	
	DueDate,
	DueAmount,
	PaymentDate,	
	PaidAmount,
	PaymentMethod,
	LoanStatuses.[Status] AS LeaseStatus,
	Returned
FROM dbo.WFLoanPaymentSchedule
	JOIN LeasePayments
		ON WFLoanPaymentSchedule.LoanID = LeasePayments.LoanID
		AND LeasePayments.PaymentDate >= DATEADD(HOUR, -72, DueDate)
		AND LeasePayments.PaymentDate < DueDate
	JOIN dbo.vw_loans_reporting
		ON LeasePayments.LoanID = vw_loans_reporting.ID
	LEFT JOIN dbo.LoanStatuses
		ON LeasePayments.LoanStatusID = LoanStatuses.ID
WHERE WFLoanPaymentSchedule.IsDeleted = 0
	AND DueDate < GETDATE()
