
with EBTotal as
(
	select year(createddate) as [year],
		month(createddate) as [month],
		count(applicantid) as EBtotalapps
	from CrestWarehouse.dbo.approvalproviderresponselog
	where approvalprovidersettingsid = 6
	group by year(createddate),
		month(createddate)
),

clarityTotal as 
(
	select year(createddate) as [year],
		month(createddate) as [month],
		count(applicantid) as Claritytotalapps
	from CrestWarehouse.dbo.approvalproviderresponselog
	where approvalprovidersettingsid = 1
		and createddate >= '2014-01-01'
	group by year(createddate),
		month(createddate)
)

select EBTotal.[year],
	EBTotal.[month],
	EBTotal.EBtotalapps,
	Claritytotalapps 
from EBTotal
left join clarityTotal on EBTotal.[year] = clarityTotal.[year] and EBTotal.[month] = clarityTotal.[month]
order by 1,2
