SELECT		YEAR(ApprovalProviderResponseLog.CreatedDate) AS [year],
			MONTH(ApprovalProviderResponseLog.CreatedDate) AS [month],
			Count(Distinct ApprovalProviderResponseLog.ApplicantID) as TotalApplicants, 
			ApprovalProviderSettings.ProviderName
FROM		[CrestWareHouse].[dbo].[ApprovalProviderResponseLog] AS ApprovalProviderResponseLog
INNER JOIN	ApprovalProviderSettings ON ApprovalProviderResponseLog.ApprovalProviderSettingsID = ApprovalProviderSettings.ID
WHERE		ApprovalProviderResponseLog.CreatedDate >= '01/01/2015'
			AND ApprovalProviderResponseLog.Decision IS NOT NULL
GROUP BY	ApprovalProviderSettings.ProviderName,
			YEAR(ApprovalProviderResponseLog.CreatedDate),
			MONTH(ApprovalProviderResponseLog.CreatedDate)  
ORDER BY	ApprovalProviderSettings.ProviderName DESC,
			YEAR(ApprovalProviderResponseLog.CreatedDate) DESC,
			MONTH(ApprovalProviderResponseLog.CreatedDate) ASC