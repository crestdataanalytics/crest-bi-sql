
select top 1000 ApprovalProviderResponseLog.ApplicantID, vw_Loans_reporting.DisplayID, vw_Loans_reporting.ID, 
	ApprovalProviderResponseLog.CreatedDate,
	ApprovalProviderResponseLog.Decision, LoanStatuses.[Status] as CurrentStatus, 
	LoanStatusHistory.[Status] as LoanHistoryStatusID,
	LoanStatusHistory.StartDate, LoanStatusHistory.EndDate
from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog
	inner join vw_Loans_reporting on ApprovalProviderResponseLog.ApplicantID = vw_Loans_reporting.ApplicantID
	inner join LoanStatuses on LoanStatuses.ID = vw_Loans_reporting.StatusID
	inner join 
		(Select LoanHistory.LoanID, LoanStatuses.[Status], LoanHistory.StartDate, LoanHistory.EndDate
		From LoanHistory 
		Inner Join LoanStatuses on LoanHistory.StatusID = LoanStatuses.ID) LoanStatusHistory 
		on LoanStatusHistory.LoanID = vw_Loans_reporting.ID
where ApprovalProviderResponseLog.ApprovalProviderSettingsID = 6
	and ApprovalProviderResponseLog.Decision = 'Denied'
	and LoanStatuses.[Status] not in ('Denied','Cancelled')
	and vw_Loans_reporting.CreationDate >= '02/10/2015'
	and vw_Loans_reporting.DisplayID like ('%-1%')
order by ApprovalProviderResponseLog.CreatedDate Desc

