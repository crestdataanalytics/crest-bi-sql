/****** Script for SelectTopNRows command from SSMS  ******/
SELECT		MONTH(AutoApprovalProcessResponses.ClarityRequestDate) AS month,
			YEAR(AutoApprovalProcessResponses.ClarityRequestDate) AS year,	
			AutoApprovalProcessResponses.ApplicantID,
			AutoApprovalProcessResponses.ClarityResponseDescription,
			AutoApprovalProcessResponses.ClarityResponse,
			CASE 
					WHEN LoanStatuses.ID IN (3,24,18,23,17) THEN 'Approved'
					WHEN LoanStatuses.ID IN (13,14) THEN 'Bankrupt'
					WHEN LoanStatuses.[Status] = 'Collections' THEN 'Bounced'
					WHEN LoanStatuses.ID IN (2,1) THEN 'Pending'
					WHEN LoanStatuses.ID = 15 THEN 'Funded'
					ELSE LoanStatuses.[Status]
					END AS Status,
			CAST(vw_loans_reporting.FundedDate AS DATE) FundedDate,
			CASE WHEN ReturnedPayments.IsReturned = 1 THEN 1 ELSE 0 END AS FirstPaymentDefault,
			vw_loans_reporting.DealerID
FROM		AutoApprovalProcessResponses
LEFT JOIN	vw_loans_reporting ON vw_loans_reporting.ApplicantID = AutoApprovalProcessResponses.ApplicantID 
LEFT JOIN	LoanStatuses ON vw_loans_reporting.StatusID = LoanStatuses.ID
LEFT JOIN	(SELECT		LoanPayments.LoanID, sum(LoanPayments.Amount) AS TotalAmountPaid, MIN(PaymentDate) AS FirstPaymentDate, 
						MIN(LoanPayments.ID) as PaymentID
			FROM		LoanPayments
			INNER JOIN	LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
			WHERE		PaymentDate is not null and IsDeleted = 0
					AND LoanPaymentTypes.[Type] NOT IN ('Cash','Initial Payment', 'Paid By Dealer','Refund')
			GROUP BY	LoanID) AmountPaid on vw_loans_reporting.ID = AmountPaid.LoanID
LEFT JOIN 
			(SELECT		LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
			FROM		LoanPaymentSchedule
			WHERE		ScheduleEntryDesc != 'Initial Payment'
			GROUP BY	LoanID) LoanPaymentFrequency on vw_loans_reporting.ID = LoanPaymentFrequency.LoanID			
LEFT JOIN	
			(SELECT		PaymentID, processeddate,
						case when ReturnCode IN ('R02','R05', 'R07', 'R08', 'R10', 'R16') then 1
						else 0 end as FirstPaymentFatalReturnIndicator,
						1 as IsReturned
			FROM		WellsFargoReturns	
			) ReturnedPayments on ReturnedPayments.PaymentID = AmountPaid.PaymentID
WHERE		--AutoApprovalProcessResponses.ClarityResponse = 2
			--AND 
			AutoApprovalProcessResponses.ClarityResponseDescription in ('Primary account is high risk.','Primary account may be high risk',
			'Primary account may be high risk.')
			AND	CAST(vw_loans_reporting.CreationDate AS DATE) = CAST(AutoApprovalProcessResponses.ClarityRequestDate AS DATE)

--GROUP BY	MONTH(AutoApprovalProcessResponses.ClarityRequestDate), YEAR(AutoApprovalProcessResponses.ClarityRequestDate),
--			LoanStatuses.[Status]
--ORDER BY	1,2

SELECT		count(AutoApprovalProcessResponses.ApplicantID)
FROM		AutoApprovalProcessResponses
--INNER JOIN	vw_loans_reporting ON 
WHERE		AutoApprovalProcessResponses.ClarityResponse = 2
			AND AutoApprovalProcessResponses.ClarityResponseDescription in ('Primary account is high risk.','Primary account may be high risk',
			'Primary account may be high risk.')