DECLARE @StartDate AS DATE = '01/01/2009'
DECLARE @EndDate AS DATE = '01/01/2014'

SELECT 
	Loans.ID AS LoanID,
	DisplayID,
	Loans.StatusID,
	AutoApprovalProcessResponses.ApprovalPath,
	LoanStatuses.[Status] AS LeaseStatus,
	DealerID AS RetailerID,
	CASE WHEN IsPlanB = 1 THEN 'Y' ELSE 'N' END AS Platinum,
	FundedDate,
	Amount AS FaceValue,
	ISNULL(FundedAmount, Amount-ISNULL(DiscountCollected,Amount*0.06)-ISNULL(Loans.ApplicationFee,40)) AS FundedAmount,
	TotalNote,
	PaidOffDate,
	ChargedOffDate,
	CASE ClarityResponse WHEN 1 THEN 'Approved' WHEN 2 THEN 'Denied' ELSE 'None' END AS ClarityResponse, 
	CASE DataXResponse WHEN 1 THEN 'Approved' WHEN 2 THEN 'Denied' ELSE 'None' END AS DataXResponse, 
	CASE MicroBiltResponse WHEN 1 THEN 'Approved' WHEN 2 THEN 'Denied' ELSE 'None' END AS MicroBiltResponse, 
	CASE FactorTrustResponse WHEN 1 THEN 'Approved' WHEN 2 THEN 'Denied' ELSE 'None' END AS FactorTrustResponse, 
	LoanOwners.Name AS OwnerName
INTO #FundedLoans
FROM dbo.Loans
	JOIN dbo.Dealers
		ON Loans.DealerID = Dealers.ID
		AND Dealers.IsDemoAccount = 0
	JOIN dbo.LoanStatuses
		ON Loans.StatusID = LoanStatuses.ID
	JOIN dbo.LoanOwners
		ON CASE WHEN Loans.[Owner] = 0 THEN 2 ELSE Loans.[Owner] END = LoanOwners.ID
	LEFT JOIN dbo.AutoApprovalProcessResponses
		ON Loans.ApplicantID = AutoApprovalProcessResponses.ApplicantID
WHERE FundedDate BETWEEN @StartDate AND @EndDate and Loans.DealerID != 4232; --this dealerID is Crest Financial Flooring (used for retailer loans)

WITH OriginationActions AS
(
	SELECT
		#FundedLoans.*,
		NULL AS PaymentAmount,
		'ORIGINATION' AS PaymentStatus,
		FundedDate AS TransactionDate,
		NULL AS ReturnedDate,
		1 AS TransactionOrder
	FROM #FundedLoans
	WHERE LeaseStatus NOT IN ('Cancelled')
),
PaymentActions AS
(
	SELECT 
		#FundedLoans.*,
		Amount AS PaymentAmount,
		'CLEARED' AS PaymentStatus, 
		PaymentDate as TransactionDate,
		NULL AS ReturnedDate,
		CASE WHEN 
			StatusID IN (12,13,14,9,5) 
			AND (CAST(PaymentDate AS DATE) > ChargedOffdate OR CAST(PaymentDate AS DATE) > PaidOffDate) 
			THEN 6 
			WHEN PaymentDate <= DATEADD(dd,94,FundedDate) 
			THEN 2 
			ELSE 4
			END AS TransactionOrder
	FROM #FundedLoans 
		JOIN dbo.LoanPayments
			ON #FundedLoans.LoanID = LoanPayments.LoanID
	WHERE LeaseStatus NOT IN ('Cancelled') 
		AND Returned = 0
		AND IsDeleted = 0
),
ReturnedPayments AS
(
	SELECT 
		#FundedLoans.*,
		Amount AS PaymentAmount,
		'RETURNED' AS PaymentStatus, 
		PaymentDate as TransactionDate,
		ReturnDate AS ReturnedDate,
		CASE WHEN 
			StatusID IN (12,13,14,9,5) 
			AND (CAST(PaymentDate AS DATE) > ChargedOffdate OR CAST(PaymentDate AS DATE) > PaidOffDate) 
			THEN 6 
			WHEN PaymentDate <= DATEADD(dd,94,FundedDate) 
			THEN 2 
			ELSE 4
			END AS TransactionOrder
	FROM #FundedLoans 
		JOIN dbo.LoanPayments
			ON #FundedLoans.LoanID = LoanPayments.LoanID
	WHERE LeaseStatus NOT IN ('Cancelled') 
		AND Returned = 1
		AND IsDeleted = 0
),
TransitionActions AS
(
	SELECT 
		#FundedLoans.*,
		NULL AS PaymentAmount,
		'90 DAY TRANSITION' AS PaymentStatus,
		DATEADD(dd,94,FundedDate) as TransactionDate,
		NULL AS ReturnedDate, 
		3 AS TransactionOrder
	FROM #FundedLoans
	WHERE LeaseStatus NOT IN ('Cancelled') 
		AND (PaidOffDate IS NULL OR PaidOffDate >= DATEADD(dd,94,FundedDate))
		AND (ChargedOffDate IS NULL OR ChargedOffDate >= DATEADD(dd,94,FundedDate))
),
ChargeOffActions AS
(
	SELECT 
		#FundedLoans.*,
		NULL AS PaymentAmount,
		'CHARGED OFF' AS PaymentStatus,
		ChargedOffDate as TransactionDate,
		NULL AS ReturnedDate, 
		5 AS TransactionOrder
	FROM #FundedLoans
	WHERE LeaseStatus NOT IN ('Charged Off', 'Bankrupt13', 'Bankrupt7', 'Returned') 
),
PaidActions AS
(
	SELECT 
		#FundedLoans.*,
		NULL AS PaymentAmount, 
		'CLOSED' as PaymentStatus,  
		PaidOffDate as TransactionDate,
		NULL AS ReturnedDate, 
		5 AS TransactionOrder
	FROM #FundedLoans
	WHERE LeaseStatus IN ('Paid','Settled')
),
CombinedActions AS
(
	SELECT * FROM OriginationActions
	UNION ALL
	SELECT * FROM PaymentActions
	UNION ALL
	SELECT * FROM ReturnedPayments
	UNION ALL
	SELECT * FROM TransitionActions
	UNION ALL
	SELECT * FROM ChargeOffActions
	UNION ALL
	SELECT * FROM PaidActions
)
SELECT 
	DisplayID AS [AgreementNumber],
	LeaseStatus AS [Status],
	ApprovalPath,
	RetailerID, 
	[Platinum],	
	cast(FundedDate as date) FundedDate,
	FaceValue,
	FundedAmount,
	TotalNote,
	PaymentAmount,
	PaymentStatus,
	cast(TransactionDate as date) TransactionDate,
	cast(ReturnedDate as date) ReturnedDate,
	MAX(CASE WHEN PaymentStatus IN ('CLEARED', 'RETURNED', 'CCLOSED', 'ORIGINATION') THEN cast(TransactionDate as date) ELSE NULL END) 
		OVER (PARTITION BY LoanID ORDER BY LoanID) AS LastTransactionDate,
	CASE WHEN StatusID IN (12,13,14,9) THEN cast(ChargedOffDate as date) ELSE cast(PaidOffDate as date) END AS PaidOffDate,
	CASE WHEN StatusID IN (12,13,14,9) THEN cast(ChargedOffDate as date) ELSE NULL END AS ChargedOffDate,
	TransactionOrder,
	ClarityResponse,
	DataXResponse,
	MicroBiltResponse,
	FactorTrustResponse
	--OwnerName 
FROM CombinedActions
--where displayid = '542033-1'
--and month(TransactionDate) = 8
ORDER BY DisplayID, TransactionOrder, TransactionDate

DROP TABLE #FundedLoans