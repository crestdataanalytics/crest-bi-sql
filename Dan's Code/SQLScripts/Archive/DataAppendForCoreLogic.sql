select top 20000 vw_loans_reporting.DisplayID, vw_loans_reporting.ID as LoanID, 
	vw_loans_reporting.CreationDate, Applicants.SocialSecurityNumber, Applicants.FirstName, Applicants.LastName,
	LoanPaymentFrequency.FirstDueDate,PrincipleAmountOwed.PaymentDate,
	Applicants.DateOfBirth, 
	replace(Addresses.StreetLine1, ',', '') as StreetLine1, 
	replace(Addresses.StreetLine2, ',', '') as StreetLine2, 
	Addresses.City, Addresses.StateID as State, Addresses.PostalCode,
	Applicants.CellPhone, Applicants.HomePhone, 
	case 
		when Applicants.EmailAddress is not null and Applicants.InvalidEmail = 0 then Applicants.EmailAddress
		else null end as EmailAddress,
	case 
		when LoanPaymentFrequency.FirstDueDate < PrincipleAmountOwed.PaymentDate then 1 
		when LoanPaymentFrequency.FirstDueDate < getdate() and PrincipleAmountOwed.PaymentDate is null then 1
		else 0 end as FPD, 
	
	case 
		when vw_loans_reporting.StatusID = 12 then 1 else 0 end as AccountChargedOff

from vw_loans_reporting
left join LoanTerms on vw_loans_reporting.LoanTermID = LoanTerms.ID
inner join Dealers on Dealers.ID = vw_loans_reporting.DealerID
inner join DealerCategories on Dealers.ID = DealerCategories.DealerID
inner join Categories on DealerCategories.CategoryID = Categories.ID
inner join Applicants on Applicants.ID = vw_loans_reporting.ApplicantID
inner join Addresses on Addresses.ID = Applicants.AddressID
inner join LoanStatuses on vw_loans_reporting.StatusID = LoanStatuses.ID
left join ApplicantJob on ApplicantJob.ApplicantID = Applicants.ID
inner join PayPeriodTypes on PayPeriodTypes.ID = ApplicantJob.PayPeriodTypeID
left join
	(SELECT  LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
     FROM    LoanPaymentSchedule
	 WHERE ScheduleEntryDesc != 'Initial Payment'
     GROUP BY LoanID) LoanPaymentFrequency
	on LoanPaymentFrequency.LoanID = vw_loans_reporting.ID
left join
	(SELECT vw_loans_reporting.ID as LoanID, FirstPayment.PaymentDate, ISNULL(12,LoanTerms.Months) as TermOfLeaseInMonths,
	case
		when dateadd(mm, ISNULL(12,LoanTerms.Months),FirstPayment.PaymentDate) >= getdate() then
			(vw_loans_reporting.TotalNote / ISNULL(12,LoanTerms.Months) * datediff(month, FirstPayment.PaymentDate, getdate()))
		when dateadd(mm, ISNULL(12,LoanTerms.Months),FirstPayment.PaymentDate) < getdate() then
			vw_loans_reporting.TotalNote
		else NULL end as AmountOwed
	FROM vw_loans_reporting
	left join 
		(SELECT  LoanID , MIN(PaymentDate) AS PaymentDate
		FROM    LoanPayments
		WHERE   Returned = 0 AND DeletedDate IS NULL
		GROUP BY LoanID) FirstPayment on FirstPayment.LoanID = vw_loans_reporting.ID
	left join LoanTerms on LoanTerms.ID = vw_loans_reporting.LoanTermID
	) PrincipleAmountOwed
	on PrincipleAmountOwed.LoanID = vw_loans_reporting.ID
where vw_loans_reporting.CreationDate >= '11/01/2014' 
	and vw_loans_reporting.CreationDate < '03/31/2015'
	and	vw_loans_reporting.FundedDate is not null 
	and LoanPaymentFrequency.FirstDueDate < getdate()
	and LoanPaymentFrequency.FirstDueDate is not null
	and (vw_loans_reporting.ID % 3 = 0 or month(vw_loans_reporting.CreationDate) = 3 and year(vw_loans_reporting.CreationDate) = 2015
		and day(vw_loans_reporting.CreationDate) % 2 = 0) --the mod allows for more random spread
	--and vw_loans_reporting.DisplayID = '772030-1'


order by vw_loans_reporting.CreationDate asc
