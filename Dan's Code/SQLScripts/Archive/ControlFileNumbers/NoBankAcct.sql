
SET Transaction Isolation Level Read Uncommitted;


SELECT		ID AS ApprovalProviderResponseLogID,
			ApplicantID,
			CreatedDate as InquiryDate,
			Decision,
			DenyDescription,
			TrackingID,
			SmartSettings
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 10
			AND SmartSettings = 'NoBankAcct'

SELECT		ApprovalProviderSettingsID, 
			count(ApprovalProviderSettingsID)
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
group by	ApprovalProviderSettingsID



SELECT		--ApplicantID, CreatedDate
			CAST(ApprovalProviderResponseLog.CreatedDate AS DATE) CreatedDate,
			COUNT (ApprovalProviderResponseLog.ApplicantID) as Total, 
			COUNT (ApprovalProviderResponseLogApprovals.ApplicantID) as ApprovedTotal,
			COUNT (ApprovalProviderResponseLogDenials.ApplicantID) as DeniedTotal, 
			ApprovalProviderResponseLog.SmartSettings
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
LEFT JOIN	(SELECT		DISTINCT
						ApplicantID, 
						CAST(CreatedDate AS DATE) CreatedDate, 
						Decision, 
						SmartSettings
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 10
						AND Decision IN ('Approve','Approved')
			--GROUP BY	CAST(CreatedDate AS DATE), Decision, SmartSettings
			) AS ApprovalProviderResponseLogApprovals ON CAST(ApprovalProviderResponseLog.CreatedDate AS DATE) = 
			CAST(ApprovalProviderResponseLogApprovals.CreatedDate AS DATE) 
LEFT JOIN	(SELECT		DISTINCT
						ApplicantID, 
						CAST(CreatedDate AS DATE) CreatedDate, 
						Decision, 
						SmartSettings
			FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
			WHERE		ApprovalProviderSettingsID = 10
						AND Decision IN ('Deny','Denied')
			--GROUP BY	CAST(CreatedDate AS DATE), Decision, SmartSettings
			) AS ApprovalProviderResponseLogDenials ON CAST(ApprovalProviderResponseLog.CreatedDate AS DATE) = 
			CAST(ApprovalProviderResponseLogDenials.CreatedDate AS DATE) 
WHERE		ApprovalProviderSettingsID = 10
			AND ApprovalProviderResponseLog.CreatedDate >= '09/12/2015'-- this is the first day that the smartSettings was actually set to "NoBankAcct"
			AND ApprovalProviderResponseLog.CreatedDate < '09/22/2016' 
			--AND cast(CreatedDate as date) = cast(getdate() as date)
GROUP BY	CAST(ApprovalProviderResponseLog.CreatedDate AS DATE), ApprovalProviderResponseLog.SmartSettings
ORDER BY	CreatedDate DESC

SELECT		DISTINCT 
			CAST(ApprovalProviderResponseLog.CreatedDate AS DATE) CreatedDate,
			vw_loans_reporting.ApplicantID,
			vw_loans_reporting.DisplayID,
			vw_loans_reporting.ID AS LoanID,
			LoanStatuses.[Status],
			CAST(vw_loans_reporting.ApprovalDate AS DATE) ApprovalDate,
			vw_loans_reporting.ApprovalAmount,
			vw_loans_reporting.Amount AS FinancedAmount,
			CAST(vw_loans_reporting.FundedDate AS DATE) FundedDate
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
INNER JOIN	vw_loans_reporting ON ApprovalProviderResponseLog.ApplicantID = vw_loans_reporting.ApplicantID
INNER JOIN	LoanStatuses ON vw_loans_reporting.StatusID = LoanStatuses.ID
WHERE		ApprovalProviderSettingsID = 10
			AND CreatedDate >= '09/12/2015'-- this is the first day that the smartSettings was actually set to "NoBankAcct"
			AND CreatedDate < '09/22/2016' 
ORDER BY	CAST(ApprovalProviderResponseLog.CreatedDate AS DATE) DESC

SELECT		CAST(CreatedDate AS DATE) CreatedDate,
			COUNT(ApplicantID) Total,
			Decision
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 10
			AND CreatedDate >= '9/12/2015'-- this is the first day that the smartSettings was actually set to "NoBankAcct"
			AND CreatedDate < '10/5/2016' 
GROUP BY	CAST(CreatedDate AS DATE) ,
			Decision
ORDER BY	CreatedDate Desc

SELECT		*
FROM		ApplicantCreditCards
WHERE		ApplicantCreditCards.ApplicantID in (1068137
,1064345
,1063992
,1063763
,1063574
,1061877
,1055850
,1054601
,1050197
,1048026
,1045144
,1045144
,1044591
,1042753
,1042753
,1041291
,738021)
ORDER BY	ApplicantID DESC

SELECT * 
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		ApplicantID = 738022


SELECT		ApprovalProviderResponseLog.CreatedDate,
			Loans.ApplicantID, 
			Loans.DisplayID,
			CreatedDate, 
			ApprovalProviderSettingsID,
			Loans.DealerID

--SELECT		COUNT(ApprovalProviderResponseLog.ApplicantID)
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
INNER JOIN	Loans ON Loans.ApplicantID = ApprovalProviderResponseLog.ApplicantID
WHERE		ResponseXML LIKE '%NoBankAcct%'
			AND ApprovalProviderSettingsID = 10
			AND Loans.DealerID NOT IN (1251
			,3970
			,3971
			,3976
			,4259
			,4355
			,4356
			,4357
			,4358
			,4359
			,4740
			,5017
			,5660)--gardner white dealer id's
--ORDER BY	CreatedDate DESC

select id from dealers where Name like '%Gardner%'