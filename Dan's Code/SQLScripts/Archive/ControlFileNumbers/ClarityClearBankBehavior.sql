SET Transaction Isolation Level Read Uncommitted;

SELECT		Count(Distinct ApplicantID) Total
FROM		CrestWareHouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 1
			AND CreatedDate >= '2015-12-07 18:20:00.000'

SELECT		Count(Distinct ApplicantID) TotalApproved
FROM		CrestWareHouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 1
			AND CreatedDate >= '2015-12-07 18:20:00.000'
			AND Decision = 'Approved'

--this gives the hour by hour
SELECT		Year(CreatedDate) AS Year,
			Month(CreatedDate) AS Month,
			Day(CreatedDate) AS Day,
			--Cast(DATEPART(HOUR, CreatedDate) as int) AS Hour,
			Count(ApplicantID) AS Total,
			Decision
FROM		CrestWareHouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 1
			AND CreatedDate >= '2015-12-04 20:59:00.000' -- Generic V.3 file (stricter cutoff score)
GROUP BY	Year(CreatedDate),
			Month(CreatedDate),
			Day(CreatedDate),
			--DATEPART(HOUR, CreatedDate),
			Decision
ORDER BY	1,2 ASC,3 DESC 


--this gives the previous month as a baseline
SELECT		Year(CreatedDate) AS Year,
			Month(CreatedDate) AS Month,
			Day(CreatedDate) AS Day,
			Count(ApplicantID) AS Total,
			CASE	WHEN Decision IN ('SoftKillDenied','HardKillDenied','Denied') THEN 'Denied'
					ELSE Decision END AS Decision
FROM		CrestWareHouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 1
			AND CreatedDate >= '2015-11-03' 
			AND CreatedDate < '2015-12-04 17:07:08.063'-- the time that Early Warning Shuts off
			AND Decision IS NOT NULL
GROUP BY	Year(CreatedDate),
			Month(CreatedDate),
			Day(CreatedDate),
			CASE
				WHEN Decision IN ('SoftKillDenied','HardKillDenied','Denied') THEN 'Denied'
				ELSE Decision END 
ORDER BY	1,2,3 DESC


--this is a breakdown of the previous month also
SELECT		Year(CreatedDate) AS Year,
			Month(CreatedDate) AS Month,
			Count(ApplicantID) AS Total,
			CASE	WHEN Decision IN ('SoftKillDenied','HardKillDenied','Denied') THEN 'Denied'
					ELSE Decision END AS Decision
FROM		CrestWareHouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 1
			AND CreatedDate >= '2015-11-03' 
			AND CreatedDate < '2015-12-04 17:07:08.063'-- the time that Early Warning Shuts off
			AND Decision IS NOT NULL
GROUP BY	Year(CreatedDate),
			Month(CreatedDate),
			CASE
				WHEN Decision IN ('SoftKillDenied','HardKillDenied','Denied') THEN 'Denied'
				ELSE Decision END 
ORDER BY	1,2,3
