
SELECT		year(vw_loans_reporting.ApprovalDate) as Year,
			month(vw_loans_reporting.ApprovalDate) as Month,
			vw_loans_reporting.ApplicantID, 
			vw_loans_reporting.[Status],
			cast(LoanPaymentFrequency.FirstDueDate as Date) FirstDueDate,
			cast(FirstReturnedPayment.RetunredPaymentDate as Date) FirstRetunredPaymentDate,
			CASE WHEN	FirstReturnedPayment.RetunredPaymentDate IS NOT NULL
						THEN DATEDIFF(day, vw_loans_reporting.FundedDate, FirstReturnedPayment.RetunredPaymentDate)
						END AS LenTimeToDefault,
			CASE WHEN	vw_loans_reporting.ChargedOffDate IS NOT NULL AND 
						(vw_loans_reporting.[Status] = 'Charged Off' OR vw_loans_reporting.[Status] LIKE 'Bank%')
						THEN DATEDIFF(day, vw_loans_reporting.FundedDate, vw_loans_reporting.ChargedOffDate)
						END AS LenTimeToChargeOff,
			case when ReturnedPayments.IsReturned = 1 THEN 1 ELSE 0 END AS FirstPaymentDefault
FROM		(SELECT		vw_loans_reporting.ApplicantID, vw_loans_reporting.ID, vw_loans_reporting.DealerID, 
						vw_loans_reporting.AutoApprovedFlag, vw_loans_reporting.ApprovalDate, LoanStatuses.[Status], vw_loans_reporting.CreationDate,
						vw_loans_reporting.FundedDate,
						vw_loans_reporting.ChargedOffDate
			FROM		vw_loans_reporting
			INNER JOIN	LoanStatuses on vw_loans_reporting.StatusID = LoanStatuses.ID
			WHERE		CreationDate <= '07/14/2015'
						AND DealerID = 2454
						AND LoanStatuses.[Status] != 'Canceled'
						AND vw_loans_reporting.FundedDate IS NOT NULL
			) as vw_loans_reporting
LEFT JOIN	(SELECT		LoanPayments.LoanID, MIN(PaymentDate) AS FirstPaymentDate, 
						MIN(LoanPayments.ID) as PaymentID
			FROM		LoanPayments
			INNER JOIN	LoanPaymentTypes on LoanPayments.TypeID = LoanPaymentTypes.ID
			WHERE		PaymentDate is not null and IsDeleted = 0
						AND LoanPaymentTypes.[Type] NOT IN ('Cash','Initial Payment', 'Paid By Dealer','Refund')
			GROUP BY	LoanID) AmountPaid on vw_loans_reporting.ID = AmountPaid.LoanID		
LEFT JOIN 
			(SELECT		LoanID, max(ScheduleEntryDesc) as LoanPaymentFrequency, MIN(DueDate) as FirstDueDate
			FROM		LoanPaymentSchedule
			WHERE		ScheduleEntryDesc != 'Initial Payment'
			GROUP BY	LoanID) LoanPaymentFrequency on vw_loans_reporting.ID = LoanPaymentFrequency.LoanID	
LEFT JOIN	
			(SELECT PaymentID, EffectiveDate, 1 as IsReturned
			FROM	WellsFargoReturns	
			) ReturnedPayments on ReturnedPayments.PaymentID = AmountPaid.PaymentID
LEFT JOIN	(SELECT LoanID, min(EffectiveDate) as RetunredPaymentDate
			FROM	WellsFargoReturns
			GROUP BY LoanID
			) FirstReturnedPayment on vw_loans_reporting.ID = FirstReturnedPayment.LoanID
ORDER BY	1,2


----break down per month of who's auto approved, manually, denied, etc.
--select year(CreationDate) year, month(creationDate) month, 
--sum(case when AutoApprovedFlag = 0 and ApprovalDate is not null then 1 else 0 end) ManuallyApproved,
--sum(case when  AutoApprovedFlag = 1 then 1 else 0 end) as AutoApproved,
--sum(case when ApprovalDate is null and AutoApprovedFlag = 0 then 1 else 0 end) as NotApproved,
--sum(case when LoanStatuses.[Status] = 'Denied' then 1 else 0 end) as Denied,
--sum(case when DealerID in (3189, 5704) then 1 else 0 end) as total
--from vw_loans_reporting
--inner join loanstatuses on loanstatuses.id = vw_loans_reporting.statusID
--where DealerID in (3189, 5704)
--group by year(CreationDate), month(creationDate)
--order by 1,2

----finds the stores on the dealers table.
--select top 10 *
--from dealers
--where Name like 'Rudy%'

----gets different denial reasons
--select		count(distinct ApprovalProviderResponseLog.ApplicantID) counting, DenyDescription
--From		[CrestWareHouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog
--Inner join	vw_loans_reporting on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
--INNER JOIN	LoanStatuses on LoanStatuses.ID = vw_loans_reporting.StatusID
--Inner Join	Dealers on Dealers.ID = vw_loans_reporting.DealerID
--Inner join	ApprovalProviderSettings on ApprovalProviderResponseLog.ApprovalProviderSettingsID = ApprovalProviderSettings.ID
--Where		Dealers.Name in ('Rudy Furniture- Montgomery, AL','Rudy Furniture- Lawrenceville, GA') 
--			and ApprovalProviderSettings.ProviderName != 'PurePredictive'
--			and vw_loans_reporting.ApprovalDate is null and vw_loans_reporting.CreationDate > '06/30/2015'
--			and vw_loans_reporting.AutoApprovedFlag = 0
--			and ApprovalProviderResponseLog.Decision in ('Denied','Deny')
--			and LoanStatuses.[Status] = 'Denied'
--group by	DenyDescription
