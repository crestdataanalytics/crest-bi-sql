
select * from dealers where name like '%Sleep Authority%'


--Select		ApprovalProviderResponseLog.ApplicantID, ApprovalProviderResponseLog.CreatedDate, ApprovalProviderResponseLog.Decision, 
--			ApprovalProviderResponseLog.DenyDescription, ApprovalProviderSettings.ProviderName
select		count(ApprovalProviderResponseLog.ApplicantID) counting, DenyDescription
From		[CrestWareHouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog
Inner join	vw_loans_reporting on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
Inner Join	Dealers on Dealers.ID = vw_loans_reporting.DealerID
Inner join	ApprovalProviderSettings on ApprovalProviderResponseLog.ApprovalProviderSettingsID = ApprovalProviderSettings.ID
Where		Dealers.ID = 2454 and ApprovalProviderSettings.ProviderName != 'PurePredictive'
			and vw_loans_reporting.ApprovalDate is null and year(vw_loans_reporting.CreationDate) = 2015
			and month(vw_loans_reporting.CreationDate) = 7 and ApprovalProviderResponseLog.Decision in ('Denied','Deny')
group by	DenyDescription
--Order by	ApprovalProviderResponseLog.CreatedDate desc

SELECT		top 10 *
FROM		[CrestWareHouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog 
INNER JOIN	vw_loans_reporting on ApprovalProviderResponseLog.ApplicantID = vw_loans_reporting.ApplicantID
WHERE		DealerID = 2454 and vw_loans_reporting.ApprovalDate is null and year(vw_loans_reporting.CreationDate) = 2015
			and month(vw_loans_reporting.CreationDate) = 7
			and ApprovalProviderResponseLog.Decision in ('Denied','Deny') AND ApprovalProviderResponseLog.DenyDescription = 'Unknown'

SELECT		top 10 *
FROM		[CrestWareHouse].[dbo].[ApprovalProviderResponseLog] as ApprovalProviderResponseLog 
WHERE		ApplicantID = 972917

select count(id)
from vw_loans_reporting
where DealerID = 2454

select year(CreationDate) year, month(creationDate) month, 
sum(case when AutoApprovedFlag = 0 and ApprovalDate is not null then 1 else 0 end) ManuallyApproved,
sum(case when  AutoApprovedFlag = 1 then 1 else 0 end) as AutoApproved,
sum(case when ApprovalDate is null then 1 else 0 end) as NotApproved,
sum(case when DealerID = 2454 then 1 else 0 end) as total
from vw_loans_reporting
where DealerID = 2454
group by year(CreationDate), month(creationDate)
order by 1,2

