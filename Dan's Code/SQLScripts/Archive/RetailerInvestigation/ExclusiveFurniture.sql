
SET Transaction Isolation Level Read Uncommitted;

SELECT	MONTH(CreationDate) Month, 
		YEAR(CreationDate) Year, 
		COUNT(ID) Total, 
		COUNT(ApprovalDate) TotalApproved,
		COUNT(FundedDate) Funded
FROM vw_loans_reporting
WHERE DealerID IN (8969,1831,1830,1829,2763,5026)
--and month(CreationDate) in (8,9) and year(CreationDate) = 2015
and vw_loans_reporting.StatusID != 11
GROUP BY month(CreationDate), year(CreationDate)
ORDER BY 2,1 DESC

SELECT	CAST(creationdate AS DATE) CreationDate, 
		COUNT(ID) Total, 
		COUNT(ApprovalDate) TotalApproved,
		COUNT(FundedDate) Funded
FROM	vw_loans_reporting
WHERE	DealerID IN (8969,1831,1830,1829,2763,5026)
		AND vw_loans_reporting.StatusID != 11
		AND vw_loans_reporting.ApplicantID not in (1039081,1039165,1040003,1040654,1044737)  --these are Demo accounts that Sulby ID'd
GROUP BY CAST(creationdate AS DATE)
ORDER BY 1 DESC

SELECT	month(CreationDate) Month, 
		year(CreationDate) year,
		count(DenyDescription),
		DenyDescription
From CrestWarehouse.dbo.ApprovalProviderResponseLog
inner join vw_loans_reporting on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
where DealerID IN (8969,1831,1830,1829,2763,5026)
and vw_loans_reporting.StatusID != 11
and month(CreationDate) in (9,10) and year(CreationDate) = 2015
and Decision in ('Denied','Deny')
and ApprovalProviderResponseLog.ApplicantID not in (1039081,1039165,1040003,1040654,1044737)
group by month(CreationDate), 
		year(CreationDate),
		DenyDescription
order by 1,2,3

SELECT	vw_loans_reporting.DisplayID,
		CAST(ApprovalProviderResponseLog.CreatedDate AS DATE) CreatedDate,
		ApprovalProviderResponseLog.RequestXML,
		ApprovalProviderResponseLog.ResponseXML,
		ApprovalProviderResponseLog.ApprovalProviderSettingsID,
		DenyDescription
From CrestWarehouse.dbo.ApprovalProviderResponseLog
inner join vw_loans_reporting on vw_loans_reporting.ApplicantID = ApprovalProviderResponseLog.ApplicantID
where DealerID IN (8969,1831,1830,1829,2763,5026)
and vw_loans_reporting.StatusID != 11
and month(CreationDate) in (9) and year(CreationDate) = 2015
and Decision in ('Denied','Deny')
and DenyDescription in ('Invalid format. One or more data fields contain invalid data. The <hint> value inside the error block contains which element(s) have invalid data.')
and ApprovalProviderResponseLog.ApplicantID not in (1039081,1039165,1040003,1040654,1044737)
order by ApprovalProviderResponseLog.ApplicantID

--this shows all records with that error.
SELECT		--count(distinct applicantID)
			ApprovalProviderResponseLog.CreatedDate,
			--vw_loans_reporting.DealerID,
			ApprovalProviderResponseLog.TrackingID
From		CrestWarehouse.dbo.ApprovalProviderResponseLog
--INNER JOIN	vw_loans_reporting ON ApprovalProviderResponseLog.ApplicantID = vw_loans_reporting.ApplicantID
WHERE		DenyDescription = 'Invalid format. One or more data fields contain invalid data. The <hint> value inside the error block contains which element(s) have invalid data.'
ORDER BY	CreatedDate DESC





