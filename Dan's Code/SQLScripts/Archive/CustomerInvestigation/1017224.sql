
select *
from [crestwarehouse].[dbo].[approvalproviderresponselog]
where applicantid = 1017224
order by approvalprovidersettingsid

select *
from ApplicantBankAccounts
inner join BankAccounts on ApplicantBankAccounts.BankAccountID = BankAccounts.iD
where ApplicantBankAccounts.applicantid = 1017224

SELECT		Applicants.id, 
			ApplicantJob.MonthlyIncome, 
			vw_loans_reporting.ApprovalAmount
FROM		Applicants
INNER JOIN	ApplicantJob on ApplicantJob.ApplicantID = Applicants.ID
INNER JOIN	vw_loans_reporting on vw_loans_reporting.ApplicantID = Applicants.ID
INNER JOIN	Dealers on Dealers.ID = vw_loans_reporting.DealerID
WHERE		Dealers.Ranking10Month = 0
			AND CAST(vw_loans_reporting.CreationDate AS DATE) = '8/18/2015'
			AND vw_loans_reporting.ApprovalDate IS NOT NULL
			AND vw_loans_reporting.ApprovalAmount >= 2400
			AND vw_loans_reporting.ApprovalAmount <= 3000
			AND ApplicantJob.MonthlyIncome <= 6700

SELECT		Applicants.id, 
			ApplicantJob.MonthlyIncome, 
			vw_loans_reporting.ApprovalAmount
FROM		Applicants
INNER JOIN	ApplicantJob on ApplicantJob.ApplicantID = Applicants.ID
INNER JOIN	vw_loans_reporting on vw_loans_reporting.ApplicantID = Applicants.ID
INNER JOIN	Dealers on Dealers.ID = vw_loans_reporting.DealerID
WHERE		Dealers.Ranking10Month = 0
			AND CAST(vw_loans_reporting.CreationDate AS DATE) = '8/18/2015'
			AND vw_loans_reporting.ApprovalDate IS NOT NULL
			AND vw_loans_reporting.ApprovalAmount < 2400
			AND ApplicantJob.MonthlyIncome > 6000

