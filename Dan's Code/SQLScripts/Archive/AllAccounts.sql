
        select  vw_loans_reporting.DisplayID, cast(vw_loans_reporting.CreationDate as date) as ApplicantCreationDate,
				cast(vw_loans_reporting.ApprovalDate as date) LeaseApprovalDate, AutoApprovalProcessResponses.ApprovalPath,
				eBureau.eBureauDecision, ClarityBank.ClarityBankDecision, Clarity.ClarityDecision, DataX.DataXDecision, 
				MicroBilt.MicroBiltDecision, FactorTrust.FactorTrustDecision, 
				vw_loans_reporting.autoApprovedFlag, cast(vw_loans_reporting.fundedDate as date) FundedDate, 
				cast(vw_loans_reporting.paidOffDate as date) PaidOffDate, LoanStatuses.Status, Delinquent.IsDelinquent, Loans.FirstPaymentDate,
				fees.TotalFeesOwed, vw_loans_reporting.ApprovalAmount, vw_loans_reporting.FundedAmount, vw_loans_reporting.TotalNote, TotalPaid.TotalPaid
		--select count(eBureau.eBureauDecision), eBureau.eBureauDecision
		from Applicants                
        inner join vw_loans_reporting on Applicants.id = vw_loans_reporting.applicantid	
		inner join LoanStatuses on 	vw_loans_reporting.StatusID = LoanStatuses.ID
		inner join AutoApprovalProcessResponses on AutoApprovalProcessResponses.ApplicantID = vw_loans_reporting.ApplicantID
		inner join Loans on vw_loans_reporting.ID = Loans.ID
		inner join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as ClarityDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 1 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as Clarity
				on Clarity.applicantid = vw_loans_reporting.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as DataXDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 2 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as DataX
				on DataX.applicantid = vw_loans_reporting.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as MicroBiltDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 3 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as MicroBilt
				on MicroBilt.applicantid = vw_loans_reporting.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as FactorTrustDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 4 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as FactorTrust
				on FactorTrust.applicantid = vw_loans_reporting.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as eBureauDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 6 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as eBureau
				on eBureau.applicantid = vw_loans_reporting.applicantid
		left join 
				(select ApplicantID, count(ApplicantID) as countAID, count(decision) as countAD, Decision as ClarityBankDecision
				from [CrestWarehouse].[dbo].[ApprovalProviderResponseLog] 
				where ApprovalProviderSettingsID = 7 and CreatedDate is not null and Decision is not null
				group by applicantid, decision) as ClarityBank
				on ClarityBank.applicantid = vw_loans_reporting.applicantid
        left join
                (
				select  vw_loans_reporting.id, count(vw_loans_reporting.id) as countID,sum(LoanPaymentFeesOwed.Amount) as TotalFeesOwed
                from vw_loans_reporting 
                inner join LoanPaymentFeesOwed on LoanPaymentFeesOwed.loanid = vw_loans_reporting.id
                where LoanPaymentFeesOwed.id is not null and LoanPaymentFeesOwed.feetypeid = 2 and LoanPaymentFeesOwed.waiveddate is null
				group by LoanPaymentFeesOwed.Amount,vw_loans_reporting.id) as Fees
                on fees.id = vw_loans_reporting.id
		--left join
		--		(select LoanID, count(LoanID) as CountLoanID,1 as HasDuePayment
		--		from WFLoanPaymentSchedule
		--		where WFLoanPaymentSchedule.DueDate <= getdate() and WFLoanPaymentSchedule.DeletedDate is not null
		--		group by LoanID) as PaymentDue
		--		on PaymentDue.Loanid = vw_loans_reporting.id
		left join
				(select vw_loans_reporting.DisplayID, sum(LoanPayments.Amount) as TotalPaid
				from LoanPayments
				inner join LoanPaymentTypes on LoanPaymentTypes.ID = LoanPayments.TypeID
				inner join vw_loans_reporting on vw_loans_reporting.ID = LoanPayments.LoanID
				where LoanPaymentTypes.Type not in ('Refund','Migrated', 'Initial Payment') and LoanPayments.HasBeenReturned = 0 
					and LoanPayments.IsDeleted = 0 and vw_loans_reporting.ID = LoanPayments.LoanID
					and vw_loans_reporting.DealerID not in (4232, 3205)
				group by vw_loans_reporting.DisplayID) as TotalPaid
				on TotalPaid.DisplayID = vw_loans_reporting.DisplayID
		left join
				(select vw_loans_reporting.ApplicantID, 1 as IsDelinquent
				from vw_loans_reporting
				where vw_loans_reporting.StatusID in (6,12, 16)) as Delinquent
				on Delinquent.ApplicantID = vw_loans_reporting.ApplicantID
		where --clarity.claritydecision is not null and 
			vw_loans_reporting.DealerID not in (4232, 3205) --these are the retailer id's for 'crest financial flooring' for retailers
			and vw_loans_reporting.FundedAmount is not null
			--and AutoApprovalProcessResponses.ApprovalPath = 'Full'
		order by vw_loans_reporting.FundedAmount asc
		--group by datepart(year, vw_loans_reporting.ApprovalDate), datepart(month,vw_loans_reporting.ApprovalDate),vw_loans_reporting.ApplicantID
		--order by datepart(year, vw_loans_reporting.ApprovalDate), datepart(month,vw_loans_reporting.ApprovalDate)