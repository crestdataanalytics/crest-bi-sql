--select top 10 * from CrestWarehouse.dbo.approvalproviderresponselog where approvalprovidersettingsid = 15 and createddate >= '2016-09-29'
SET Transaction Isolation Level Read Uncommitted;

SELECT		ID AS ApprovalProviderResponseLogID,
			ApprovalProviderSettingsID,
			ApplicantID,
			TrackingID AS InsightGUID
FROM		CrestWarehouse.dbo.ApprovalProviderResponseLog
WHERE		ApprovalProviderSettingsID = 15
			