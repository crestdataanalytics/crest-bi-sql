/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [AutoApprovalProcessResponses].[ApplicantID]
      ,[ClarityTracking]
      ,[ClarityResponse]
      ,[ClarityResponseDescription]
      ,[ClarityRequestDate]
      ,[DataXTransactionID]
      ,[DataXResponse]
      ,[DataXResponseDescription]
      ,[DataXRequestDate]
      ,[MicroBiltRequestUID]
      ,[MicroBiltResponse]
      ,[MicroBiltResponseDescription]
      ,[MicroBiltRequestDate]
      ,[CreditsUsageHistoryID]
      ,[DataXInquiryID]
      ,[FactorTrustClientTransactionID]
      ,[FactorTrustResponse]
      ,[FactorTrustResponseDescription]
      ,[FactorTrustRequestDate]
      ,[ApprovalPath]
      ,[EBureauHardDenied]
      ,[EBureauRequestDate]
      ,[TeletrackCoreClientTransactionID]
      ,[TeletrackCoreResponse]
      ,[TeletrackCoreResponseDescription]
      ,[TeletrackCoreRequestDate]
      ,[NeustarTransactionID]
      ,[NeustarRequestDate]
      ,[NeustarResponse]
  FROM [DealerManage].[dbo].[AutoApprovalProcessResponses]
  inner join vw_loans_reporting on vw_loans_reporting.applicantid = AutoApprovalProcessResponses.applicantid
  where DataXTransactionID in (233664857,
233664527,
233664465,
233664435,
233664255,
233663909,
233663867,
233661287,
233661131,
233661073,
233660839,
233660011,
233659639,
233659251,
233659185,
233658967,
233648851,
233648663,
233648513,
233647957,
233647889,
233647337
)
and (cast(AutoApprovalProcessResponses.ClarityRequestDate as date) = cast(vw_loans_reporting.creationdate as date) or
	cast(AutoApprovalProcessResponses.ClarityRequestDate as date) = cast(vw_loans_reporting.UpdatedDate as date))
--and vw_loans_reporting.approvaldate is not null
--and vw_loans_reporting.autoapprovedflag = 1 