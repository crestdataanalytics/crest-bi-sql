select count(distinct applicantid) as total
from AutoApprovalProcessResponses
where 
	
	ClarityRequestDate >= '2015-08-14 09:52:22.003'
	and ClarityRequestDate <= '2015-08-14 11:56:56.277'
	
	--month(ClarityRequestDate) = 8
	--and day(ClarityRequestDate) = 14
	--and year(ClarityRequestDate) = 2015


select count(distinct vw_loans_reporting.applicantid) as approved
from AutoApprovalProcessResponses
inner join vw_loans_reporting on vw_loans_reporting.applicantid = AutoApprovalProcessResponses.applicantid
where 

	ClarityRequestDate >= '2015-08-14 09:52:22.003'
	and ClarityRequestDate <= '2015-08-14 11:56:56.277'

	--month(ClarityRequestDate) = 8
	--and day(ClarityRequestDate) = 14
	--and year(ClarityRequestDate) = 2015
	and (cast(AutoApprovalProcessResponses.ClarityRequestDate as date) = cast(vw_loans_reporting.creationdate as date) or
	cast(AutoApprovalProcessResponses.ClarityRequestDate as date) = cast(vw_loans_reporting.UpdatedDate as date))
	and vw_loans_reporting.approvaldate is not null
	and vw_loans_reporting.statusid not in (11,7)

select count(distinct vw_loans_reporting.applicantid) as autoapproved
from AutoApprovalProcessResponses
inner join vw_loans_reporting on vw_loans_reporting.applicantid = AutoApprovalProcessResponses.applicantid
where 

	ClarityRequestDate >= '2015-08-14 09:52:22.003'
	and ClarityRequestDate <= '2015-08-14 11:56:56.277'
	
	--month(ClarityRequestDate) = 8
	--and day(ClarityRequestDate) = 14
	--and year(ClarityRequestDate) = 2015
	and (cast(AutoApprovalProcessResponses.ClarityRequestDate as date) = cast(vw_loans_reporting.creationdate as date) or
	cast(AutoApprovalProcessResponses.ClarityRequestDate as date) = cast(vw_loans_reporting.UpdatedDate as date))
	and vw_loans_reporting.AutoApprovedFlag = 1 
	and vw_loans_reporting.statusid not in (11,7)