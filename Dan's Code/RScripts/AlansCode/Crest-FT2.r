#=========================================================================================================#
# CUSTOMER NAME:         Crest - Factor Trust Project
# DATA SET DATE(S):      04/17/15
# AUTHOR:                AKW
#
# DIRECTIONS:            There are 6 STEPS to perform this analysis (see comments/headers in code).
#                        --------------------------------------------------------------------
#                        STEP #1: Initialize/setup code
#                        STEP #2: Gather and merge data
#                             Optionally write out/read in merged file to save time on subsequent runs
#                        STEP #3: Extract and prepare the data relevant for this project
#                        STEP #4: Subset the data into a form deemed most accurate or relevant for analysis
#                             Make sure to use the correctly built filter which will generate df5
#                                  Current (uncommented) code uses the ApprovalPath="FULL" subset
#                        STEP #5: Prepare summary data for viewing
#                        STEP #6: Create summary the tables for viewing
#                        STEP #7: Prepare time series data for viewing
#                        STEP #8: Create time series plots (read comments for description of each - 21)
#
# NOTES/TO DOs:          Run an analysis where the order of the "Waterfall" is evaluated
#                             Try all combinations to find optimal loan value and accuracy
#                        Examine loans that are in-flight by pro-rating and determining if they are tracking
#                             If we do this, we'll need a way of determining the "Term" of the loan
#                        Look at subset (windows) where loan dates aren't too old and/or aren't too new
#                        Re-run using the larger 8k dataset?
#                        Re-run for individual bureaus for side-by-side comparison?
#                        Look at overall accuracy and revenue patterns over time (average term of charged-off loan @ 120 days)
#=========================================================================================================#
#=========================================================================================================#
#    STEP 1: INITIALIZE - LOAD LIBRARIES AND SPECIFY GLOBAL CONSTANTS
#=========================================================================================================#
{ 
        #Assumes these packages are already installed - so just need to load them here
        library(gridExtra)
        library(reshape2)
        library(ggplot2)                   
        library(scales)
        library(gdata)
        library(time)
        
        # GLOBAL CONSTANTS (THAT WE MAY WANT TO TWEAK)
        acquireDate              <- "04/06/2015"     # Date we acquired this dataset
        tooRecentDate            <- "10/06/2014"     # Loans since this date are probably still "in-flight" (~6 months before acquireDate)
        tooOldDate               <- "04/06/2012"     # Loans older than this date may not be representative of current state of affairs (remove them @ 3+ yrs?)
        
        startDate                <- "01/01/2011"     # Range to begin for Time Series analysis
        interval                 <- (365/12)*3600*24 # Time series interval for ignoring leap-year
        intervalL                <- (366/12)*3600*24 # Time series interval for leap-year consideration
}
#=========================================================================================================#
#    STEP 2: DATA GATHERING AND MERGING (assumes "data" subdirectory that includes CSV files)
#=========================================================================================================#
 
{
        setwd("F:/CSVFiles/AlansProgram/FactorTrustEvaluation")
        
        df1 <- read.csv("ModifiedAccounting_2009_2013.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
        colnames(df1)[1] <- "AgreementNumber"
        
        df2 <- read.csv("ModifiedAccounting_2014_1.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
        colnames(df2)[1] <- "AgreementNumber"
        
        
        df3 <- read.csv("ModifiedAccounting_2014_2.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
        colnames(df3)[1] <- "AgreementNumber"
        
        df4 <- read.csv("ModifiedAccounting_2015_1.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
        colnames(df4)[1] <- "AgreementNumber"
        
        df5 <- read.csv("ModifiedAccounting_2015_2.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
        colnames(df5)[1] <- "AgreementNumber"
        
        #Dan's optimization
        dtimebegin <- Sys.time()
        df <- rbind(df1,df2,df3,df4,df5)
        dtimeend <- Sys.time()
        dtimeend - dtimebegin
        
        atimebegin <- Sys.time()
        dfa <- merge(df1, df2, all.x=TRUE, all.y=TRUE)
        dfa <- merge(dfa, df3, all.x=TRUE, all.y=TRUE)
        dfa <- merge(dfa, df4, all.x=TRUE, all.y=TRUE)
        dfa <- merge(dfa, df5, all.x=TRUE, all.y=TRUE)
        atimeend <- Sys.time()
        atimeend - atimebegin
        
        rm(list = "df1","df2","df3","df4","df5","df6","df7")
        
        dfcopy <- df
        #df <- dfcopy
}

#df1 <- read.csv("data/041715/2009.csv", sep=",", header=FALSE, stringsAsFactors=FALSE)
#df2 <- read.csv("data/041715/2010.csv", sep=",", header=FALSE, stringsAsFactors=FALSE)
#df3 <- read.csv("data/041715/2011.csv", sep=",", header=FALSE, stringsAsFactors=FALSE)
#df4 <- read.csv("data/041715/2012.csv", sep=",", header=FALSE, stringsAsFactors=FALSE)
#df5 <- read.csv("data/041715/2013.csv", sep=",", header=FALSE, stringsAsFactors=FALSE)
#df6 <- read.csv("data/041715/2014.csv", sep=",", header=FALSE, stringsAsFactors=FALSE)
#df7 <- read.csv("data/041715/2015.csv", sep=",", header=FALSE, stringsAsFactors=FALSE)
#df <- merge(df, df6[-1,], all.x=TRUE, all.y=TRUE)
#df <- merge(df, df7[-1,], all.x=TRUE, all.y=TRUE)

# I had to read in this set of files w/ "header=FALSE" because of 3 hex #'s at the beginning that are causing errors
#colnames(df) <- c("AgreementNumber","Status","ApprovalPath","RetailerID","Platinum","FundedDate","FaceValue","FundedAmount","TotalNote","PaymentAmount","PaymentStatus","TransactionDate","ReturnedDate","LastTransactionDate","PaidOffDate","ChargedOffDate","TransactionOrder","ClarityResponse","DataXResponse","MicroBiltResponse","FactorTrustResponse","OwnerName")
# write.csv(file="AllDataNEW.csv", x=df)                            # Saves me time to start from here next time
# df <- read.csv("AllDataNEW.csv", sep=",", stringsAsFactors=FALSE) # Time saving step only (not mandatory)

#=========================================================================================================#
#    STEP 3: DATA TRANSFORMATIONS & FEATURE ENGINEERING (FULL DATASET)
#=========================================================================================================#
{
        # Change these variables into "Factors"
        df$ClarityResponse       <- as.factor(df$ClarityResponse)
        df$DataXResponse         <- as.factor(df$DataXResponse)
        df$MicroBiltResponse     <- as.factor(df$MicroBiltResponse)
        df$FactorTrustResponse   <- as.factor(df$FactorTrustResponse)
        df$ApprovalPath          <- as.factor(df$ApprovalPath)
        
        # Take a peak at these new Factor variables to make sure they are all the same/consistent
        mapLevels(df$ClarityResponse)                # Approved = 1, Denied = 2, None = 3
        mapLevels(df$DataXResponse)                  # Approved = 1, Denied = 2, None = 3 - Doesn't DataX sometimes get called and return "None"?
        mapLevels(df$MicroBiltResponse)              # Approved = 1, Denied = 2, None = 3
        mapLevels(df$FactorTrustResponse)            # Approved = 1, Denied = 2, None = 3
        mapLevels(df$ApprovalPath)                   # Efficient = 1, Full = 2, NULL = 3
        
        length(unique(df$AgreementNumber))           # 177,206 Total Unique Loans in combined dataset
        
        # Create a sub-sample of loans that need their payments aggregated (any/all "CLEARED" in PaymentStatus)
        df1 <- df[df$PaymentStatus %in% c("CLEARED"),]
        
        # Now aggregate (sum all values per Agreement#) on the records with valid payments (keep Status field in tact)
        df2 <- aggregate(as.numeric(PaymentAmount) ~ AgreementNumber+Status, data=df1, FUN=sum)
        
        # Now consolidate duplicate info on ALL loans into a single record...will be used for future calculations (includes loans with NO PAYMENTS - 11,024)
        #    NOTE: The 1st na.action=na.pass instructs "aggregate" to not delete rows where NA's exist, the 2nd na.rm=TRUE tells "mean" to ignore NA's
        #    NOTE: I actually tried using the "max" function instead of "mean", but it threw some warnings (still worked -Inf), but I used "mean" instead (no warnings).
        df3 <- aggregate(cbind(as.numeric(FundedAmount), 
                               as.integer(as.POSIXct(FundedDate, format="%Y-%m-%d")), 
                               as.integer(as.POSIXct(PaidOffDate, format="%Y-%m-%d")), 
                               as.integer(as.POSIXct(ChargedOffDate, format="%Y-%m-%d")), 
                               as.numeric(ClarityResponse), 
                               as.numeric(DataXResponse), 
                               as.numeric(MicroBiltResponse), 
                               as.numeric(FactorTrustResponse), 
                               as.numeric(ApprovalPath)) ~ AgreementNumber+Status, data=df, FUN=mean, na.action=na.pass, na.rm=TRUE)
        
        # Now merge all this new data together and re-name columns
        df4 <- merge(df2, df3, all=TRUE)                       # Outer join - keep all data still at this point
        colnames(df4)   <- c("AgreementNumber","Status","TotalPaid","FundedAmount","FundedDate","PaidOffDate","ChargedOffDate","ClarityResponse","DataXResponse","MicroBiltResponse","FactorTrustResponse","ApprovalPath")
        
        # Since we included loans with NO PAYMENTS, we need to change their value from "NA" to "0"
        df4$TotalPaid   <- ifelse(is.na(df4$TotalPaid), 0, df4$TotalPaid)
        
        # Add new columns for greater insight and future calculations
        df4$TotalProfit <- df4$TotalPaid - df4$FundedAmount
        df4$LABEL       <- ifelse(df4$TotalProfit > 0, 1, 0)
}

#=========================================================================================================#
#    STEP 4: SUBSET LOANS
#         Option 1: Only look at those loans that have a "COMPLETED" status (good or bad)
#                        The downside to this is that the more recent apps are skewed towards defaults (charge offs)
#                        May want to exclude more recent apps
#         Option 2: Consider "IN-FLIGHT" loans, because we either want to look at the more recent data
#                   OR we don't have enough data to exclude these from the analysis
#                        If we do this, we will need to extrapolate whether or not they are tracking
#                        May also want to exclude the most recent loans?
#=========================================================================================================#
#---------------------------------------------------------------------------------------------------------#
#    Option 1: Only loans that have completed ("Charged Off" or "Paid Off")
#---------------------------------------------------------------------------------------------------------#

{
        # Subset where we only look at loans that have completed (nothing in-flight)
        df5 <- df4[(!is.na(df4$ChargedOffDate) | !is.na(df4$PaidOffDate)),]
        
        # Subset where we eliminate loans that are too recent
        # df6 <- subset(df5, FundedDate <= as.integer(as.POSIXct(tooRecentDate, format = "%m/%d/%Y"))) # Don't look at recent ones - skewed towards "Collections"
        
        # Subset where we eliminate loans that are too old
        # df7 <- subset(df6, FundedDate > as.integer(as.POSIXct(tooOldDate, format = "%m/%d/%Y"))) # Don't look at older ones - not relevant any more?
        
        # Subset where we only look at observations where ALL 4 bureaus have responded (currently > 8K)
        # df5 <- df5[(df5$ClarityResponse=="1" | df5$ClarityResponse=="2") & (df5$DataXResponse=="1" | df5$DataXResponse=="2") & (df5$MicroBiltResponse=="1" | df5$MicroBiltResponse=="2") & (df5$FactorTrustResponse=="1" | df5$FactorTrustResponse=="2"),]
        
        # Subset where we look at the ApprovalPath="FULL", or theoretically where all 4 bureaus have responded (currently > 4K)
        df5 <- df5[df5$ApprovalPath=="2",]
}

#---------------------------------------------------------------------------------------------------------#
#    Option 2: Loans that have completed (as above) + some of the more mature "Active"/In-flight loans
#---------------------------------------------------------------------------------------------------------#


#=========================================================================================================#
#    STEP 5: PREPARE SUMMARIZED RESULTS
#=========================================================================================================#
{
        # Factor Trust
        ft1    <- length(df5$FactorTrustResponse[df5$FactorTrustResponse=="1"])
        ft2    <- length(df5$FactorTrustResponse[df5$FactorTrustResponse=="2"])
        ft1hit <- length(df5$FactorTrustResponse[df5$FactorTrustResponse=="1" & df5$LABEL=="1"])
        ft2hit <- length(df5$FactorTrustResponse[df5$FactorTrustResponse=="2" & df5$LABEL=="0"])
        ft1rev <- sum(df5$TotalProfit[df5$FactorTrustResponse=="1"])
        ft2rev <- sum(df5$TotalProfit[df5$FactorTrustResponse=="2"])
        
        # Clarity
        cl1    <- length(df5$ClarityResponse[df5$ClarityResponse=="1"])
        cl2    <- length(df5$ClarityResponse[df5$ClarityResponse=="2"])
        cl1hit <- length(df5$ClarityResponse[df5$ClarityResponse=="1" & df5$LABEL=="1"])
        cl2hit <- length(df5$ClarityResponse[df5$ClarityResponse=="2" & df5$LABEL=="0"])
        cl1rev <- sum(df5$TotalProfit[df5$ClarityResponse=="1"])
        cl2rev <- sum(df5$TotalProfit[df5$ClarityResponse=="2"])
        
        # DataX
        dx1    <- length(df5$DataXResponse[df5$DataXResponse=="1"])
        dx2    <- length(df5$DataXResponse[df5$DataXResponse=="2"])
        dx1hit <- length(df5$DataXResponse[df5$DataXResponse=="1" & df5$LABEL=="1"])
        dx2hit <- length(df5$DataXResponse[df5$DataXResponse=="2" & df5$LABEL=="0"])
        dx1rev <- sum(df5$TotalProfit[df5$DataXResponse=="1"])
        dx2rev <- sum(df5$TotalProfit[df5$DataXResponse=="2"])
        
        # MicroBilt
        mb1    <- length(df5$MicroBiltResponse[df5$MicroBiltResponse=="1"])
        mb2    <- length(df5$MicroBiltResponse[df5$MicroBiltResponse=="2"])
        mb1hit <- length(df5$MicroBiltResponse[df5$MicroBiltResponse=="1" & df5$LABEL=="1"])
        mb2hit <- length(df5$MicroBiltResponse[df5$MicroBiltResponse=="2" & df5$LABEL=="0"])
        mb1rev <- sum(df5$TotalProfit[df5$MicroBiltResponse=="1"])
        mb2rev <- sum(df5$TotalProfit[df5$MicroBiltResponse=="2"])
        
        # TOTAL SCORED W/ FACTOR TRUST
        total1    <- nrow(df5[df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1" | df5$FactorTrustResponse=="1",]) # Anyone said "approve"
        total2    <- nrow(df5[(df5$ClarityResponse=="2" | df5$DataXResponse=="2" | df5$MicroBiltResponse=="2" | df5$FactorTrustResponse=="2") & !(df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1" | df5$FactorTrustResponse=="1"),]) # Anyone said "deny" and nobody said "approve"
        total1hit <- nrow(df5[(df5$ClarityResponse=="1" & df5$LABEL=="1") | (df5$DataXResponse=="1" & df5$LABEL=="1") | (df5$MicroBiltResponse=="1" & df5$LABEL=="1") | (df5$FactorTrustResponse=="1" & df5$LABEL=="1"),]) # Of those "any people" that said "approve", did they also predict correctly (was it GOOD)
        total2hit <- nrow(df5[((df5$ClarityResponse=="2" & df5$LABEL=="0") | (df5$DataXResponse=="2" & df5$LABEL=="0") | (df5$MicroBiltResponse=="2" & df5$LABEL=="0") | (df5$FactorTrustResponse=="2" & df5$LABEL=="0")) & !(df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1" | df5$FactorTrustResponse=="1"),]) # Of those "any people" that said "deny" (and nobody said "approve"), did they also predict correctly (was it BAD)
        total1rev <- sum(df5[df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1" | df5$FactorTrustResponse=="1",]$TotalProfit) # Revenue where anyone said "approve"
        total2rev <- sum(df5[(df5$ClarityResponse=="2" | df5$DataXResponse=="2" | df5$MicroBiltResponse=="2" | df5$FactorTrustResponse=="2") & !(df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1" | df5$FactorTrustResponse=="1"),]$TotalProfit) # Revenue where anyone said "deny" and nobody said "approve"
        
        # TOTAL SCORED W/O FACTOR TRUST
        Ntotal1    <- nrow(df5[df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1",]) # Anyone (but FT) said "approve"
        Ntotal2    <- nrow(df5[(df5$ClarityResponse=="2" | df5$DataXResponse=="2" | df5$MicroBiltResponse=="2") & !(df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1"),]) # Anyone (but FT) said "deny" and nobody said "approve"
        Ntotal1hit <- nrow(df5[(df5$ClarityResponse=="1" & df5$LABEL=="1") | (df5$DataXResponse=="1" & df5$LABEL=="1") | (df5$MicroBiltResponse=="1" & df5$LABEL=="1"),]) # Of those "any people" (but FT) that said "approve", did they also predict correctly (was it GOOD)
        Ntotal2hit <- nrow(df5[((df5$ClarityResponse=="2" & df5$LABEL=="0") | (df5$DataXResponse=="2" & df5$LABEL=="0") | (df5$MicroBiltResponse=="2" & df5$LABEL=="0")) & !(df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1"),]) # Of those "any people" (but FT) that said "deny" (and nobody said "approve"), did they also predict correctly (was it BAD)
        Ntotal1rev <- sum(df5[df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1",]$TotalProfit) # Revenue where anyone said "approve"
        Ntotal2rev <- sum(df5[(df5$ClarityResponse=="2" | df5$DataXResponse=="2" | df5$MicroBiltResponse=="2") & !(df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1"),]$TotalProfit) # Revenue where anyone said "deny" and nobody said "approve"
        
        # MISC TOTALS
        untot1         <- nrow(df5) - (total1 + total2) # The total number of records completed - the number that were scored
        totalRevScored <- sum(df5[df5$ClarityResponse=="2" | df5$DataXResponse=="2" | df5$MicroBiltResponse=="2" | df5$FactorTrustResponse=="2" | df5$ClarityResponse=="1" | df5$DataXResponse=="1" | df5$MicroBiltResponse=="1" | df5$FactorTrustResponse=="1",]$TotalProfit)
        totalRevAll    <- sum(df5$TotalProfit)
}
#=========================================================================================================#
#    STEP 6: VISUALIZE SUMMARIZED RESULTS
#=========================================================================================================#
#---------------------------------------------------------------------------------------------------------#
#    DECLARE FUNCTIONS USED IN THIS STEP
#---------------------------------------------------------------------------------------------------------#
{        
        # Declare function that takes a decimal value and converts it to a formatted percentage string
        percForm <- function(x){
                paste0(format(x*100, digits=2, nsmall=2, trim=TRUE, decimal.mark=".", justify="right"),"%") 
        }
        
        # Declare function that takes a dollar value and formats it into a string with commas and a $
        dlrForm <- function(x){
                paste0("$ ", format(round(x, 0), big.mark=","))
        }
}
#---------------------------------------------------------------------------------------------------------#
#    COUNTS TABLE
#---------------------------------------------------------------------------------------------------------#
{
        gLoans <- data.frame(
                row.names = c(
                        'Total Loans Scored', 
                        'Loans Approved', 
                        'Loans Denied', 
                        '', 
                        '% Approved'), 
                "Clarity" = c(
                        cl1 + cl2, 
                        cl1, 
                        cl2, 
                        '', 
                        percForm(cl1 / (cl1 + cl2))), 
                "Data X" = c(
                        dx1 + dx2, 
                        dx1, 
                        dx2, 
                        '', 
                        percForm(dx1 / (dx1 + dx2))), 
                "MicroBilt" = c(
                        mb1 + mb2, 
                        mb1, 
                        mb2, 
                        '', 
                        percForm(mb1 / (mb1 + mb2))), 
                " " = c(
                        '', 
                        '', 
                        '', 
                        '', 
                        ''), 
                "Factor\nTrust" = c(
                        ft1 + ft2, 
                        ft1, 
                        ft2, 
                        '', 
                        percForm(ft1 / (ft1 + ft2))), 
                "TOTAL\n(w/o FT*)" = c(
                        Ntotal1 + Ntotal2, 
                        Ntotal1, 
                        Ntotal2, 
                        '', 
                        percForm(Ntotal1 / (Ntotal1 + Ntotal2))), 
                "TOTAL\n(w/ FT**)" = c(
                        total1 + total2, 
                        total1, 
                        total2, 
                        '', 
                        percForm(total1 / (total1 + total2))), 
                check.names=FALSE
        )
     
     # pdf("Fig1.pdf", width=11, paper="USr")
     grid.newpage()
     table <- gLoans
     grid.table(table)
     grid.text("Figure 1: Number of Loans Scored", y=0.70, gp=gpar(fontsize=20, fontface="bold"))
     grid.text("* Using \"Waterfall\" logic but excluding Factor Trust", y=0.32, gp=gpar(fontsize=12, fontface="italic"))
     grid.text("** Using \"Waterfall\" logic and including Factor Trust", y=0.29, gp=gpar(fontsize=12, fontface="italic"))
     grid.text(paste0("(NOTE: Total UNSCORED = ", untot1, ")"), y=0.24, gp=gpar(fontsize=12, fontface="bold"))
     #grid.draw(gTree(children=gList(table)))
     # dev.off()
}
#---------------------------------------------------------------------------------------------------------#
#    ACCURACY TABLE
#---------------------------------------------------------------------------------------------------------#
{
        gLoans <- data.frame(
                row.names = c(
                        'Total Accuracy', 
                        '', 
                        'Approved - Good (True Positive)', 
                        'Approved - Bad (False Positive)', 
                        'Approved - Accuracy %', 
                        ' ', 
                        'Denied - Good (False Negative', 
                        'Denied - Bad (True Negative)', 
                        'Denied - Accuracy %'), 
                "Clarity" = c(
                        percForm((cl1hit + cl2hit) / (cl1 + cl2)), 
                        '', 
                        cl1hit,
                        cl1 - cl1hit,
                        percForm(cl1hit / cl1), 
                        '', 
                        cl2 - cl2hit,
                        cl2hit,
                        percForm(cl2hit / cl2)), 
                "Data X" = c(
                        percForm((dx1hit + dx2hit) / (dx1 + dx2)), 
                        '', 
                        dx1hit,
                        dx1 - dx1hit,
                        percForm(dx1hit / dx1), 
                        '', 
                        dx2 - dx2hit,
                        dx2hit,
                        percForm(dx2hit / dx2)), 
                "MicroBilt" = c(
                        percForm((mb1hit + mb2hit) / (mb1 + mb2)), 
                        '', 
                        mb1hit,
                        mb1 - mb1hit,
                        percForm(mb1hit / mb1), 
                        '', 
                        mb2 - mb2hit,
                        mb2hit,
                        percForm(mb2hit / mb2)), 
                " " = c(
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        ''), 
                "Factor\nTrust" = c(
                        percForm((ft1hit + ft2hit) / (ft1 + ft2)), 
                        '', 
                        ft1hit,
                        ft1 - ft1hit,
                        percForm(ft1hit / ft1), 
                        '', 
                        ft2 - ft2hit,
                        ft2hit,
                        percForm(ft2hit / ft2)), 
                "TOTAL\n(w/o FT)" = c(
                        percForm((Ntotal1hit + Ntotal2hit) / (Ntotal1 + Ntotal2)), 
                        '', 
                        Ntotal1hit,
                        Ntotal1 - Ntotal1hit,
                        percForm(Ntotal1hit / Ntotal1), 
                        '', 
                        Ntotal2 - Ntotal2hit,
                        Ntotal2hit,
                        percForm(Ntotal2hit / Ntotal2)), 
                "TOTAL\n(w/ FT)" = c(
                        percForm((total1hit + total2hit) / (total1 + total2)), 
                        '', 
                        total1hit,
                        total1 - total1hit,
                        percForm(total1hit / total1), 
                        '', 
                        total2 - total2hit,
                        total2hit,
                        percForm(total2hit / total2)), 
                check.names=FALSE
        )
        
        # pdf("Fig2.pdf", width=11, paper="USr")
        grid.newpage()
        table <- gLoans
        grid.table(table)
        grid.text("Figure 2: Accuracy of Scored Loans", y=0.83, gp=gpar(fontsize=20, fontface="bold"))
        grid.text("(Confusion Matrix)", y=0.785, gp=gpar(fontsize=16))
        #grid.draw(gTree(children=gList(table)))
        # dev.off()
}
#---------------------------------------------------------------------------------------------------------#
#    LOAN VALUE TABLE
#---------------------------------------------------------------------------------------------------------#

{
        gLoans <- data.frame(
                row.names = c(
                        '# of Loans APPROVED', 
                        '* Total $ Approved Loans', 
                        '* Avg $ per Approved Loan', 
                        ' ', 
                        '# of Loans DENIED', 
                        '** Total $ Denied Loans', 
                        '** Avg $ per Denied Loan'), 
                "Clarity" = c(
                        cl1, 
                        dlrForm(cl1rev), 
                        dlrForm(cl1rev/cl1), 
                        '', 
                        cl2, 
                        dlrForm(cl2rev), 
                        dlrForm(cl2rev/cl2)), 
                "Data X" = c(
                        dx1, 
                        dlrForm(dx1rev), 
                        dlrForm(dx1rev/dx1), 
                        '', 
                        dx2, 
                        dlrForm(dx2rev), 
                        dlrForm(dx2rev/dx2)), 
                "MicroBilt" = c(
                        mb1, 
                        dlrForm(mb1rev), 
                        dlrForm(mb1rev/mb1), 
                        '', 
                        mb2, 
                        dlrForm(mb2rev), 
                        dlrForm(mb2rev/mb2)), 
                " " = c(
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        ''), 
                "Factor Trust" = c(
                        ft1, 
                        dlrForm(ft1rev), 
                        dlrForm(ft1rev/ft1), 
                        '', 
                        ft2, 
                        dlrForm(ft2rev), 
                        dlrForm(ft2rev/ft2)), 
                "TOTAL\n(w/o FT)" = c(
                        Ntotal1, 
                        dlrForm(Ntotal1rev), 
                        dlrForm(Ntotal1rev/Ntotal1), 
                        '', 
                        Ntotal2, 
                        dlrForm(Ntotal2rev), 
                        dlrForm(Ntotal2rev/Ntotal2)), 
                "TOTAL\n(w/ FT)" = c(
                        total1, 
                        dlrForm(total1rev), 
                        dlrForm(total1rev/total1), 
                        '', 
                        total2, 
                        dlrForm(total2rev), 
                        dlrForm(total2rev/total2)), 
                check.names=FALSE
        )
        
        # pdf("Fig3.pdf", width=11, paper="USr")
        grid.newpage()
        table <- gLoans
        grid.table(table)
        grid.text("Figure 3: Revenue From Scoring Recommendations", y=0.75, gp=gpar(fontsize=20, fontface="bold"))
        grid.text("* POSITIVE VALUES ARE GOOD: Since they were approved, you would have made this $", y=0.28, gp=gpar(fontsize=12, fontface="italic"))
        grid.text("** NEGATIVE VALUES ARE GOOD: Since they were denied, you wouldn't have lost this $", y=0.25, gp=gpar(fontsize=12, fontface="italic"))
        grid.text(paste0("(NOTE: Total Revenue - All Loans = ", dlrForm(totalRevAll), " @ ", nrow(df5), " loans or ", dlrForm(totalRevAll/nrow(df5)), " per loan)"), y=0.20, gp=gpar(fontsize=12, fontface="bold"))
        grid.text(paste0("(NOTE: Total Revenue - Scored Loans = ", dlrForm(totalRevScored), " @ ", total1 + total2, " loans or ", dlrForm(totalRevScored/(total1 + total2)), " per loan)"), y=0.17, gp=gpar(fontsize=12, fontface="bold"))
        #grid.draw(gTree(children=gList(table)))
        # dev.off()
}

#---------------------------------------------------------------------------------------------------------#
#    LOAN VALUE TABLE - COMMON DATA
#---------------------------------------------------------------------------------------------------------#
{
        # Create a subset that contains responses from ALL 4 credit bureaus (8608 such cases)
        dfA <- df5[(df5$ClarityResponse=="1" | df5$ClarityResponse=="2") & (df5$DataXResponse=="1" | df5$DataXResponse=="2") & (df5$MicroBiltResponse=="1" | df5$MicroBiltResponse=="2") & (df5$FactorTrustResponse=="1" | df5$FactorTrustResponse=="2"),]
        # dfA <- subset(dfA, FundedDate <= as.integer(as.POSIXct(tooRecentDate, format = "%m/%d/%Y"))) # Don't look at recent ones - skewed towards "Collections"
        
        # Factor Trust
        Sft1    <- length(dfA$FactorTrustResponse[dfA$FactorTrustResponse=="1"])
        Sft2    <- length(dfA$FactorTrustResponse[dfA$FactorTrustResponse=="2"])
        Sft1rev <- sum(dfA$TotalProfit[dfA$FactorTrustResponse=="1"])
        Sft2rev <- sum(dfA$TotalProfit[dfA$FactorTrustResponse=="2"])
        
        # Clarity
        Scl1    <- length(dfA$ClarityResponse[dfA$ClarityResponse=="1"])
        Scl2    <- length(dfA$ClarityResponse[dfA$ClarityResponse=="2"])
        Scl1rev <- sum(dfA$TotalProfit[dfA$ClarityResponse=="1"])
        Scl2rev <- sum(dfA$TotalProfit[dfA$ClarityResponse=="2"])
        
        # DataX
        Sdx1    <- length(dfA$DataXResponse[dfA$DataXResponse=="1"])
        Sdx2    <- length(dfA$DataXResponse[dfA$DataXResponse=="2"])
        Sdx1rev <- sum(dfA$TotalProfit[dfA$DataXResponse=="1"])
        Sdx2rev <- sum(dfA$TotalProfit[dfA$DataXResponse=="2"])
        
        # MicroBilt
        Smb1    <- length(dfA$MicroBiltResponse[dfA$MicroBiltResponse=="1"])
        Smb2    <- length(dfA$MicroBiltResponse[dfA$MicroBiltResponse=="2"])
        Smb1rev <- sum(dfA$TotalProfit[dfA$MicroBiltResponse=="1"])
        Smb2rev <- sum(dfA$TotalProfit[dfA$MicroBiltResponse=="2"])
        
        # TOTAL SCORED W/ FACTOR TRUST
        Stotal1    <- nrow(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1",]) # Anyone said "approve"
        Stotal2    <- nrow(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1"),]) # Anyone said "deny" and nobody said "approve"
        Stotal1rev <- sum(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1",]$TotalProfit) # Revenue where anyone said "approve"
        Stotal2rev <- sum(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1"),]$TotalProfit) # Revenue where anyone said "deny" and nobody said "approve"
        
        # TOTAL SCORED W/O FACTOR TRUST
        SNtotal1    <- nrow(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1",]) # Anyone (but FT) said "approve"
        SNtotal2    <- nrow(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1"),]) # Anyone (but FT) said "deny" and nobody said "approve"
        SNtotal1rev <- sum(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1",]$TotalProfit) # Revenue where anyone said "approve"
        SNtotal2rev <- sum(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1"),]$TotalProfit) # Revenue where anyone said "deny" and nobody said "approve"
        
        # MISC TOTALS
        Suntot1         <- nrow(dfA) - (total1 + total2) # The total number of records completed - the number that were scored
        StotalRevScored <- sum(dfA[dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2" | dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1",]$TotalProfit)
        StotalRevAll    <- sum(dfA$TotalProfit)
        
        # Now chart the results
        gLoans <- data.frame(
                row.names = c(
                        '# of Loans APPROVED', 
                        '* Total $ Approved Loans', 
                        '* Avg $ per Approved Loan', 
                        ' ', 
                        '# of Loans DENIED', 
                        '** Total $ Denied Loans', 
                        '** Avg $ per Denied Loan'), 
                "Clarity" = c(
                        Scl1, 
                        dlrForm(Scl1rev), 
                        dlrForm(Scl1rev/Scl1), 
                        '', 
                        Scl2, 
                        dlrForm(Scl2rev), 
                        dlrForm(Scl2rev/Scl2)), 
                "Data X" = c(
                        Sdx1, 
                        dlrForm(Sdx1rev), 
                        dlrForm(Sdx1rev/Sdx1), 
                        '', 
                        Sdx2, 
                        dlrForm(Sdx2rev), 
                        dlrForm(Sdx2rev/Sdx2)), 
                "MicroBilt" = c(
                        Smb1, 
                        dlrForm(Smb1rev), 
                        dlrForm(Smb1rev/Smb1), 
                        '', 
                        Smb2, 
                        dlrForm(Smb2rev), 
                        dlrForm(Smb2rev/Smb2)), 
                " " = c(
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        ''), 
                "Factor Trust" = c(
                        Sft1, 
                        dlrForm(Sft1rev), 
                        dlrForm(Sft1rev/Sft1), 
                        '', 
                        Sft2, 
                        dlrForm(Sft2rev), 
                        dlrForm(Sft2rev/Sft2)), 
                "TOTAL\n(w/o FT)" = c(
                        SNtotal1, 
                        dlrForm(SNtotal1rev), 
                        dlrForm(SNtotal1rev/SNtotal1), 
                        '', 
                        SNtotal2, 
                        dlrForm(SNtotal2rev), 
                        dlrForm(SNtotal2rev/SNtotal2)), 
                "TOTAL\n(w/ FT)" = c(
                        Stotal1, 
                        dlrForm(Stotal1rev), 
                        dlrForm(Stotal1rev/Stotal1), 
                        '', 
                        Stotal2, 
                        dlrForm(Stotal2rev), 
                        dlrForm(Stotal2rev/Stotal2)), 
                check.names=FALSE
        )
        
        # pdf("Fig4.pdf", width=11, bg="transparent", paper="USr")
        grid.newpage()
        table <- gLoans
        grid.table(table)
        grid.text("Figure 4: Revenue From Commonly Scored Loans", y=0.75, gp=gpar(fontsize=20, fontface="bold"))
        grid.text("* POSITIVE VALUES ARE GOOD: Since they were approved, you would have made this $", y=0.28, gp=gpar(fontsize=12, fontface="italic"))
        grid.text("** NEGATIVE VALUES ARE GOOD: Since they were denied, you wouldn't have lost this $", y=0.25, gp=gpar(fontsize=12, fontface="italic"))
        grid.text(paste0("(NOTE: Total Revenue - All Scored Loans = ", dlrForm(StotalRevAll), " @ ", nrow(dfA), " loans or ", dlrForm(StotalRevAll/nrow(dfA)), " per loan)"), y=0.20, gp=gpar(fontsize=12, fontface="bold"))
        grid.text(paste0("(NOTE: ", nrow(dfA[dfA$TotalProfit >= 0,]), " loans made a profit of ", dlrForm(sum(dfA[dfA$TotalProfit >= 0,]$TotalProfit)), ", while ", nrow(dfA[dfA$TotalProfit < 0,]), " loans lost ", dlrForm(sum(dfA[dfA$TotalProfit < 0,]$TotalProfit)), ")"), y=0.17, gp=gpar(fontsize=12, fontface="bold"))
        #grid.draw(gTree(children=gList(table)))
        # dev.off()
}
        
#---------------------------------------------------------------------------------------------------------#
#    LOAN VALUE TABLE - COMMON DATA USING "APPROVALPATH" FLAG
#---------------------------------------------------------------------------------------------------------#
{
        # Create a subset that contains responses from ALL 4 credit bureaus (8608 such cases)
        dfA <- df5[df5$ApprovalPath=="2",]
        # dfA <- subset(dfA, FundedDate <= as.integer(as.POSIXct(tooRecentDate, format = "%m/%d/%Y"))) # Don't look at recent ones - skewed towards "Collections"
        
        # Factor Trust
        Sft1    <- length(dfA$FactorTrustResponse[dfA$FactorTrustResponse=="1"])
        Sft2    <- length(dfA$FactorTrustResponse[dfA$FactorTrustResponse=="2"])
        Sft1rev <- sum(dfA$TotalProfit[dfA$FactorTrustResponse=="1"])
        Sft2rev <- sum(dfA$TotalProfit[dfA$FactorTrustResponse=="2"])
        
        # Clarity
        Scl1    <- length(dfA$ClarityResponse[dfA$ClarityResponse=="1"])
        Scl2    <- length(dfA$ClarityResponse[dfA$ClarityResponse=="2"])
        Scl1rev <- sum(dfA$TotalProfit[dfA$ClarityResponse=="1"])
        Scl2rev <- sum(dfA$TotalProfit[dfA$ClarityResponse=="2"])
        
        # DataX
        Sdx1    <- length(dfA$DataXResponse[dfA$DataXResponse=="1"])
        Sdx2    <- length(dfA$DataXResponse[dfA$DataXResponse=="2"])
        Sdx1rev <- sum(dfA$TotalProfit[dfA$DataXResponse=="1"])
        Sdx2rev <- sum(dfA$TotalProfit[dfA$DataXResponse=="2"])
        
        # MicroBilt
        Smb1    <- length(dfA$MicroBiltResponse[dfA$MicroBiltResponse=="1"])
        Smb2    <- length(dfA$MicroBiltResponse[dfA$MicroBiltResponse=="2"])
        Smb1rev <- sum(dfA$TotalProfit[dfA$MicroBiltResponse=="1"])
        Smb2rev <- sum(dfA$TotalProfit[dfA$MicroBiltResponse=="2"])
        
        # TOTAL SCORED W/ FACTOR TRUST
        Stotal1    <- nrow(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1",]) # Anyone said "approve"
        Stotal2    <- nrow(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1"),]) # Anyone said "deny" and nobody said "approve"
        Stotal1rev <- sum(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1",]$TotalProfit) # Revenue where anyone said "approve"
        Stotal2rev <- sum(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1"),]$TotalProfit) # Revenue where anyone said "deny" and nobody said "approve"
        
        # TOTAL SCORED W/O FACTOR TRUST
        SNtotal1    <- nrow(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1",]) # Anyone (but FT) said "approve"
        SNtotal2    <- nrow(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1"),]) # Anyone (but FT) said "deny" and nobody said "approve"
        SNtotal1rev <- sum(dfA[dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1",]$TotalProfit) # Revenue where anyone said "approve"
        SNtotal2rev <- sum(dfA[(dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1"),]$TotalProfit) # Revenue where anyone said "deny" and nobody said "approve"
        
        # MISC TOTALS
        Suntot1         <- nrow(dfA) - (total1 + total2) # The total number of records completed - the number that were scored
        StotalRevScored <- sum(dfA[dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2" | dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1",]$TotalProfit)
        StotalRevAll    <- sum(dfA$TotalProfit)
        
        # Now chart the results
        gLoans <- data.frame(
                row.names = c(
                        '# of Loans APPROVED', 
                        '* Total $ Approved Loans', 
                        '* Avg $ per Approved Loan', 
                        ' ', 
                        '# of Loans DENIED', 
                        '** Total $ Denied Loans', 
                        '** Avg $ per Denied Loan'), 
                "Clarity" = c(
                        Scl1, 
                        dlrForm(Scl1rev), 
                        dlrForm(Scl1rev/Scl1), 
                        '', 
                        Scl2, 
                        dlrForm(Scl2rev), 
                        dlrForm(Scl2rev/Scl2)), 
                "Data X" = c(
                        Sdx1, 
                        dlrForm(Sdx1rev), 
                        dlrForm(Sdx1rev/Sdx1), 
                        '', 
                        Sdx2, 
                        dlrForm(Sdx2rev), 
                        dlrForm(Sdx2rev/Sdx2)), 
                "MicroBilt" = c(
                        Smb1, 
                        dlrForm(Smb1rev), 
                        dlrForm(Smb1rev/Smb1), 
                        '', 
                        Smb2, 
                        dlrForm(Smb2rev), 
                        dlrForm(Smb2rev/Smb2)), 
                " " = c(
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        '', 
                        ''), 
                "Factor Trust" = c(
                        Sft1, 
                        dlrForm(Sft1rev), 
                        dlrForm(Sft1rev/Sft1), 
                        '', 
                        Sft2, 
                        dlrForm(Sft2rev), 
                        dlrForm(Sft2rev/Sft2)), 
                "TOTAL\n(w/o FT)" = c(
                        SNtotal1, 
                        dlrForm(SNtotal1rev), 
                        dlrForm(SNtotal1rev/SNtotal1), 
                        '', 
                        SNtotal2, 
                        dlrForm(SNtotal2rev), 
                        dlrForm(SNtotal2rev/SNtotal2)), 
                "TOTAL\n(w/ FT)" = c(
                        Stotal1, 
                        dlrForm(Stotal1rev), 
                        dlrForm(Stotal1rev/Stotal1), 
                        '', 
                        Stotal2, 
                        dlrForm(Stotal2rev), 
                        dlrForm(Stotal2rev/Stotal2)), 
                check.names=FALSE
        )
        
        # pdf("Fig5.pdf", width=11, bg="transparent", paper="USr")
        grid.newpage()
        table <- gLoans
        grid.table(table)
        grid.text("Figure 5: Revenue Where \"Approval Path\" = \"FULL\"", y=0.75, gp=gpar(fontsize=20, fontface="bold"))
        grid.text("* POSITIVE VALUES ARE GOOD: Since they were approved, you would have made this $", y=0.28, gp=gpar(fontsize=12, fontface="italic"))
        grid.text("** NEGATIVE VALUES ARE GOOD: Since they were denied, you wouldn't have lost this $", y=0.25, gp=gpar(fontsize=12, fontface="italic"))
        grid.text(paste0("(NOTE: Total Revenue - All Scored Loans = ", dlrForm(StotalRevAll), " @ ", nrow(dfA), " loans or ", dlrForm(StotalRevAll/nrow(dfA)), " per loan)"), y=0.20, gp=gpar(fontsize=12, fontface="bold"))
        grid.text(paste0("(NOTE: ", nrow(dfA[dfA$TotalProfit >= 0,]), " loans made a profit of ", dlrForm(sum(dfA[dfA$TotalProfit >= 0,]$TotalProfit)), ", while ", nrow(dfA[dfA$TotalProfit < 0,]), " loans lost ", dlrForm(sum(dfA[dfA$TotalProfit < 0,]$TotalProfit)), ")"), y=0.17, gp=gpar(fontsize=12, fontface="bold"))
        #grid.draw(gTree(children=gList(table)))
        # dev.off()
}

#=========================================================================================================#
#    STEP 7: PREPARE TIME SERIES RESULTS (ASSUMES "dfA" IS DATA.FRAME WITH DATA TO ANALYZE)
#=========================================================================================================#
{
        # Explore date range
        min(dfA$FundedDate)
        max(dfA$FundedDate)
        
        # Pre-calculate new columns of information for Factor Trust
        dfA$FactorTrustAppC  <- ifelse(dfA$FactorTrustResponse=="1" & dfA$LABEL=="1", 1, 0)
        dfA$FactorTrustDenyC <- ifelse(dfA$FactorTrustResponse=="2" & dfA$LABEL=="0", 1, 0)
        dfA$FactorTrustAppR  <- ifelse(dfA$FactorTrustResponse=="1", dfA$TotalProfit, 0)
        dfA$FactorTrustDenyR <- ifelse(dfA$FactorTrustResponse=="2", dfA$TotalProfit, 0)
        
        # Pre-calculate new columns of information for ALL Bureas
        dfA$AnyApp           <- ifelse(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1", 1, 0)
        dfA$AnyDeny          <- ifelse((dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1"), 1, 0) # Anyone said "deny" and nobody said "approve"
        dfA$AnyAppC          <- ifelse(dfA$AnyApp=="1" & dfA$LABEL=="1", 1, 0)
        dfA$AnyDenyC         <- ifelse(dfA$AnyDeny=="1" & dfA$LABEL=="1", 1, 0)
        dfA$AnyAppR          <- ifelse(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1", dfA$TotalProfit, 0)
        dfA$AnyDenyR         <- ifelse((dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2" | dfA$FactorTrustResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1" | dfA$FactorTrustResponse=="1"), dfA$TotalProfit, 0) # Anyone said "deny" and nobody said "approve"
        
        # Pre-calculate new columns of information for ALL Bureas except (remove) Factor Trust
        dfA$AnyBFTApp        <- ifelse(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1", 1, 0)
        dfA$AnyBFTDeny       <- ifelse((dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1"), 1, 0) # Anyone said "deny" and nobody said "approve"
        dfA$AnyBFTAppC       <- ifelse(dfA$AnyBFTApp=="1" & dfA$LABEL=="1", 1, 0)
        dfA$AnyBFTDenyC      <- ifelse(dfA$AnyBFTDeny=="1" & dfA$LABEL=="1", 1, 0)
        dfA$AnyBFTAppR       <- ifelse(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1", dfA$TotalProfit, 0)
        dfA$AnyBFTDenyR      <- ifelse((dfA$ClarityResponse=="2" | dfA$DataXResponse=="2" | dfA$MicroBiltResponse=="2") & !(dfA$ClarityResponse=="1" | dfA$DataXResponse=="1" | dfA$MicroBiltResponse=="1"), dfA$TotalProfit, 0) # Anyone said "deny" and nobody said "approve"
        
        # Set the default start date and number of days (range) for all calculations
        start     <- as.integer(as.POSIXct(startDate, format = "%m/%d/%Y"))
        dRange    <- max(dfA$FundedDate) - start             # Find the # of days (range) for analysis
        
        # Initialize a dataframe with the first few columns (others can be added on later)
        results <- data.frame(matrix(0, ncol=4, nrow=(round(dRange/interval))))
        names(results) <- c("Date", "Approve", "Deny", "aRate")
}

#---------------------------------------------------------------------------------------------------------#
#    TIME SLICING THE DATASET - LOOP ON INTERVAL AND CREATE DATA AGGREGATED INTO TIME CHUNKS
#---------------------------------------------------------------------------------------------------------#
{
        for(i in seq(1:round(dRange/interval))){
                # Initialize date, FT approve/deny counts and approval rate
                results$Date[i]     <- as.Date((start/3600/24), origin = "1970-01-01")
                results$Approve[i]  <- sum(dfA$FactorTrustResponse[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1")
                results$Deny[i]     <- sum(dfA$FactorTrustResponse[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="2")
                results$aRate[i]    <- results$Approve[i] / (results$Approve[i] + results$Deny[i])
                
                # Build Factor Trust accuracy results     
                results$approveC[i] <- sum(dfA$FactorTrustAppC[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1")
                results$denyC[i]    <- sum(dfA$FactorTrustDenyC[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1")
                results$appAcc[i]   <- results$approveC[i] / results$Approve[i]
                results$denyAcc[i]  <- results$denyC[i] / results$Deny[i]
                results$totalAcc[i] <- (results$approveC[i] + results$denyC[i]) / (results$Approve[i] + results$Deny[i])
                
                # Build Factor Trust revenue results
                results$approveR[i] <- sum(dfA$FactorTrustAppR[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))])
                results$denyR[i]    <- sum(dfA$FactorTrustDenyR[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))])
                results$avgAppR[i]  <- results$approveR[i] / results$Approve[i]
                results$avgDenyR[i] <- results$denyR[i] / results$Deny[i]
                
                # Build accuracy results with ALL Bureaus, INcluding FT
                results$aApprove[i] <- sum(dfA$AnyApp[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$aDeny[i]    <- sum(dfA$AnyDeny[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$aAppC[i]    <- sum(dfA$AnyAppC[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$aDenyC[i]   <- sum(dfA$AnyDenyC[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$aAppAcc[i]  <- results$aAppC[i] / results$aApprove[i]
                results$aDenyAcc[i] <- results$aDenyC[i] / results$aDeny[i]
                results$aTotAcc[i]  <- (results$aAppC[i] + results$aDenyC[i]) / (results$aApprove[i] + results$aDeny[i])
                
                # Build revenue results with ALL Bureaus, INcluding FT
                results$appAnyR[i]  <- sum(dfA$AnyAppR[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))])
                results$denyAnyR[i] <- sum(dfA$AnyDenyR[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))])
                results$avgAAnyR[i] <- results$appAnyR[i] / results$aApprove[i]
                results$avgDAnyR[i] <- results$denyAnyR[i] / results$aDeny[i]
                
                # Build accuracy results with ALL Bureaus, EXcluding FT
                results$bApprove[i] <- sum(dfA$AnyBFTApp[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$bDeny[i]    <- sum(dfA$AnyBFTDeny[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$bAppC[i]    <- sum(dfA$AnyBFTAppC[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$bDenyC[i]   <- sum(dfA$AnyBFTDenyC[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1") # Aggregated column, where 1=TRUE
                results$bAppAcc[i]  <- results$bAppC[i] / results$bApprove[i]
                results$bDenyAcc[i] <- results$bDenyC[i] / results$bDeny[i]
                results$bTotAcc[i]  <- (results$bAppC[i] + results$bDenyC[i]) / (results$bApprove[i] + results$bDeny[i])
                
                # Build revenue results with ALL Bureaus, EXcluding FT
                results$bappAnyR[i]  <- sum(dfA$AnyBFTAppR[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))])
                results$bdenyAnyR[i] <- sum(dfA$AnyBFTDenyR[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))])
                results$bavgAAnyR[i] <- results$bappAnyR[i] / results$bApprove[i]
                results$bavgDAnyR[i] <- results$bdenyAnyR[i] / results$bDeny[i]
                
                # Now find the DIFFERENCE in accuracy results with vs. without FT
                results$diffAppAcc[i]  <- results$aAppAcc[i] - results$bAppAcc[i]
                results$diffDenyAcc[i] <- results$aDenyAcc[i] - results$bDenyAcc[i]
                results$diffTotAcc[i]  <- results$aTotAcc[i] - results$bTotAcc[i]
                
                # Now find the DIFFERENCE in revenue results with vs. without FT
                results$diffAppR[i]  <- results$avgAAnyR[i] - results$bavgAAnyR[i]
                results$diffDenyR[i] <- results$avgAAnyR[i] - results$bavgDAnyR[i]
                
                # Now find the Number of good and bad loans
                results$NumGood[i]   <- sum(dfA$LABEL[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="1")
                results$NumBad[i]    <- sum(dfA$LABEL[(dfA$FundedDate > start) & (dfA$FundedDate <= (start + interval))]=="0")
                
                #      start <- start + interval             # Change to this if you want to ignore leap years
                ifelse(format(as.Date(start/3600/24), "%Y")=="2012" | format(as.Date(start/3600/24), "%Y")=="2016", start <- start + intervalL, start <- start + interval) # Consider leap years
        }
        
        # Change from POSIX to Date format...ggplot requires it in this format
        results$Date <- as.Date(results$Date)
        
        # For now, I'm just going to let ggplot handle the NaN's...but we could remove them here if we'd like.
        # results[,"aRate"][is.nan(results[,"aRate"])] <- 0      # Replace NaN's in this variable with "0"
}

#=========================================================================================================#
#    STEP 8: VISUALIZE TIME SERIES RESULTS
#=========================================================================================================#
{
        # Prep for two of the graphs (that are displayed as "stacked" bars)
        
        resultsA <- results[ , which(names(results) %in% c("Date", "Approve", "Deny"))]
        resultsA <- melt(resultsA, id.vars = "Date")
        resultsB <- results[ , which(names(results) %in% c("Date", "NumGood", "NumBad"))]
        resultsB <- melt(resultsB, id.vars = "Date")
}
#---------------------------------------------------------------------------------------------------------#
#    BUILD PLOTS
#---------------------------------------------------------------------------------------------------------#
{
        # p1 - Factor Trust Only: Monthly approval rate of loans
        p1 <- ggplot(data = results, aes(x = Date)) +
                geom_line(aes(y = aRate, colour = "var")) +
                scale_colour_manual(values = c("blue3")) +
                ggtitle("Figure P1/P2: Factor Trust Approval Rate (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Approval Rate") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
        
        # p2 - Factor Trust Only: Monthly # of responses by Approval or Denial
        p2 <- ggplot(data = resultsA, aes(x = Date, y = value, fill = variable)) +
                geom_bar(stat="identity") +
                theme(
                        legend.position=c(.09,.5), 
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_blank(),
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Monthly Responses") +
                coord_cartesian(ylim = c(0, 1000)) +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(minor_breaks = seq(-1.0,1.0,0.10))
        
        # p3 - Factor Trust Only: Monthly accuracy on Approved Loans
        p3 <- ggplot(data = results, aes(x = Date)) +
                geom_line(aes(y = appAcc, colour = "App")) +
                scale_colour_manual(values = c("red3")) +
                ggtitle("Figure P3: Factor Trust Accuracy (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Approval Accuracy") +
                coord_cartesian(ylim = c(0, 1)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
        
        # p4 - Factor Trust Only: Monthly accuracy on Denied Loans
        p4 <- ggplot(data = results, aes(x = Date)) +
                geom_line(aes(y = denyAcc, colour = "Deny")) +
                scale_colour_manual(values = c("orange2")) +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Deny Accuracy") +
                coord_cartesian(ylim = c(0, 1)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
        
        # p5 - Factor Trust Only: Monthly total accuracy (trend) for all loans
        p5 <- ggplot(data = results, aes(x = Date, y = totalAcc)) +
                geom_line(aes(y = totalAcc, colour = "App")) +
                scale_colour_manual(values = c("red3")) +
                geom_smooth(method="loess") +
                ggtitle("Figure P5: Factor Trust Accuracy (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Total Accuracy") +
                coord_cartesian(ylim = c(0, 1)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
        
        # p6 - Factor Trust Only: Monthly revenue/loss from Approved Loans
        p6 <- ggplot(data = results, aes(x = Date, y = approveR)) +
                geom_bar(stat="identity", position="identity", fill="red") +
                ggtitle("Figure P6/P7: Factor Trust Total $ (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Monthly Approval $") +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p7 - Factor Trust Only: Monthly revenue/loss from Denied Loans
        p7 <- ggplot(data = results, aes(x = Date, y = denyR)) +
                geom_bar(stat="identity", position="identity", fill="blue") +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Monthly Denial $") +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p8 - Actual number of good and bad loans
        p8 <- ggplot(data = resultsB, aes(x = Date, y = value, fill = variable)) +
                geom_bar(stat="identity") +
                ggtitle("Figure P8: Actual Results - Good vs. Bad Loans (Monthly Averages)") + 
                theme(
                        legend.position=c(.09,.5), 
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_blank(),
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Monthly Responses") +
                coord_cartesian(ylim = c(0, 1000)) +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(minor_breaks = seq(-1.0,1.0,0.10))
        
        # p9 - FT revenue per approved loan
        p9 <- ggplot(data = results, aes(x = Date, y = avgAppR)) +
                geom_bar(stat="identity", position="identity", fill="red") +
                ggtitle("Figure P9/P10: Factor Trust $ Per Loan (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. $ per Appr. Loan") +
                coord_cartesian(ylim = c(-500, 1500)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p10 - FT revenue per denied loan
        p10 <- ggplot(data = results, aes(x = Date, y = avgDenyR)) +
                geom_bar(stat="identity", position="identity", fill="blue") +
                scale_colour_manual(values = c("orange2")) +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. $ per Deny Loan") +
                coord_cartesian(ylim = c(-500, 1500)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p11 - All Bureaus - revenue per approved loan
        p11 <- ggplot(data = results, aes(x = Date, y = avgAAnyR)) +
                geom_bar(stat="identity", position="identity", fill="red") +
                ggtitle("Figure P11/P12: ALL Bureaus $ Per Loan (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. $ per Appr. Loan") +
                coord_cartesian(ylim = c(-500, 1500)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p12 - All Bureaus - revenue per denied loan
        p12 <- ggplot(data = results, aes(x = Date, y = avgDAnyR)) +
                geom_bar(stat="identity", position="identity", fill="blue") +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. $ per Deny Loan") +
                coord_cartesian(ylim = c(-500, 1500)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p13 - All Bureaus/No FT - revenue per approved loan
        p13 <- ggplot(data = results, aes(x = Date, y = bavgAAnyR)) +
                geom_bar(stat="identity", position="identity", fill="red") +
                ggtitle("Figure P13/P14: ALL Bureaus/No FT - $ Per Loan (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. $ per Appr. Loan") +
                coord_cartesian(ylim = c(-500, 1500)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p14 - All Bureaus/No FT - revenue per denied loan
        p14 <- ggplot(data = results, aes(x = Date, y = bavgDAnyR)) +
                geom_bar(stat="identity", position="identity", fill="blue") +
                scale_colour_manual(values = c("orange2")) +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. $ per Deny Loan") +
                coord_cartesian(ylim = c(-500, 1500)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p15 - Revenue Difference w/ and w/o FT on a per approved loan basis
        p15 <- ggplot(data = results, aes(x = Date, y = diffAppR)) +
                geom_bar(stat="identity", position="identity", fill="green4") +
                ggtitle("Figure P15/P16: Profit Diff (w/ & w/o FT) Per Loan (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. Lift per Appr. Loan") +
                coord_cartesian(ylim = c(-100, 100)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p16 - Revenue Difference w/ and w/o FT on a per denied loan basis
        p16 <- ggplot(data = results, aes(x = Date, y = diffDenyR)) +
                geom_bar(stat="identity", position="identity", fill="orange2") +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("Avg. Lift per Deny Loan") +
                coord_cartesian(ylim = c(-100, 100)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = dollar)
        
        # p17 - All Bureaus (Including FT): Monthly total accuracy on All Loans
        p17 <- ggplot(data = results, aes(x = Date, y = aTotAcc)) +
                geom_line(aes(y = aTotAcc, colour = "red")) +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Total Accuracy") +
                coord_cartesian(ylim = c(0, 1)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
        
        # p18 - All Bureaus (Excluding FT): Monthly total accuracy on All Loans
        p18 <- ggplot(data = results, aes(x = Date, y = bTotAcc)) +
                geom_line(aes(y = bTotAcc, colour = "red")) +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Total Accuracy") +
                coord_cartesian(ylim = c(0, 1)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
        
        # p19 - All Bureaus difference in accuracy on approved Loans
        p19 <- ggplot(data = results, aes(x = Date, y = diffAppAcc)) +
                geom_bar(stat="identity", position="identity", fill="green4") +
                ggtitle("Figure P19/P20: Accuracy Diff (w/ & w/o FT) Per Loan (Monthly Averages)") + 
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Lift Approval Accuracy") +
                coord_cartesian(ylim = c(-0.05, 0.05)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
        
        # p20 - All Bureaus difference in accuracy on denied Loans
        p20 <- ggplot(data = results, aes(x = Date, y = diffDenyAcc)) +
                geom_bar(stat="identity", position="identity", fill="orange2") +
                theme(
                        legend.position="none",
                        axis.title.x = element_blank(),
                        plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
                        axis.title.x = element_text(size=15),
                        axis.title.y = element_text(size=15),
                        axis.text.x = element_text(size=12),
                        axis.text.y = element_text(size=12)) +
                ylab("% Lift Deny Accuracy") +
                coord_cartesian(ylim = c(-0.05, 0.05)) +
                scale_fill_discrete(name = "") +
                scale_x_date(minor_breaks="1 month", labels=date_format("%Y")) +
                scale_y_continuous(labels = percent)
}
#---------------------------------------------------------------------------------------------------------#
#    NOW PLOT IN VARIOUS COMBINATIONS
#---------------------------------------------------------------------------------------------------------#
{
        # p1 - Factor Trust Only: Monthly approval rate of loans
        # p2 - Factor Trust Only: Monthly # of responses by Approval or Denial
        # p3 - Factor Trust Only: Monthly accuracy on Approved Loans
        # p4 - Factor Trust Only: Monthly accuracy on Denied Loans
        # p5 - Factor Trust Only: Monthly total accuracy (trend) for all loans
        # p6 - Factor Trust Only: Monthly revenue/loss from Approved Loans
        # p7 - Factor Trust Only: Monthly revenue/loss from Denied Loans
        # p8 - Actual number of good and bad loans
        # p9 - FT revenue per approved loan
        # p10 - FT revenue per denied loan
        # p11 - All Bureaus - revenue per approved loan
        # p12 - All Bureaus - revenue per denied loan
        # p13 - All Bureaus/No FT - revenue per approved loan
        # p14 - All Bureaus/No FT - revenue per denied loan
        # p15 - Revenue Difference w/ and w/o FT on a per approved loan basis
        # p16 - Revenue Difference w/ and w/o FT on a per denied loan basis
        # p17 - All Bureaus (Including FT): Monthly total accuracy on All Loans
        # p18 - All Bureaus (Excluding FT): Monthly total accuracy on All Loans
        # p19 - All Bureaus difference in accuracy on approved Loans
        # p20 - All Bureaus difference in accuracy on denied Loans
        
        # pdf("FigP1P2.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p1, p2, ncol = 1)
        # dev.off()
        
        # pdf("FigP5.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p5, ncol = 1)
        # dev.off()
        
        # pdf("FigP6P7.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p6, p7, ncol = 1)
        # dev.off()
        
        # pdf("FigP8.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p8, ncol = 1)
        # dev.off()
        
        # pdf("FigP9P10.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p9, p10, ncol = 1)
        # dev.off()
        
        # pdf("FigP1P2P8.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p1, p2, p8, ncol = 1)
        # dev.off()
        
        grid.arrange(p1, p2, p9, p10, ncol = 1)
        grid.arrange(p1, p2, p9, ncol = 1)
        grid.arrange(p1, p2, p10, ncol = 1)
        
        # pdf("FigP11P12.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p11, p12, ncol = 1)
        # dev.off()
        
        # pdf("FigP13P14.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p13, p14, ncol = 1)
        # dev.off()
        
        # pdf("FigP15P16.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p15, p16, ncol = 1)
        # dev.off()
        
        # pdf("FigP19P20.pdf", width=11, bg="transparent", paper="USr")
        grid.arrange(p19, p20, ncol = 1)
        # dev.off()
        
        resultsC <- results[30:51 , which(names(results) %in% c("Date", "Approve", "Deny", "avgAppR", "avgDenyR", "bavgAAnyR", "bavgDAnyR", "NumGood", "NumBad"))]
        colnames(resultsC) <- c("Date","FT App","FT Deny","FT App $","FT Deny $","ALL App $","ALL Deny $","Actual # Good","Actual # Bad")
        
        # pdf("results.pdf")
        grid.table(resultsC, gpar.coretext = gpar(fontsize=8), gpar.coltext = gpar(fontsize=8), padding.h=unit(2, "mm"), padding.v=unit(2, "mm"), show.colnames = TRUE, show.rownames = FALSE)
        # dev.off()
}

#=========================================================================================================#
#    OPTIONAL RESEARCH: VALIDATING SOME "RED FLAG" VALUES
#=========================================================================================================#
#---------------------------------------------------------------------------------------------------------#
#    I was curious about MicroBilt revenue numbers, so I dug into it a bit to validate/verify. Looks like there is an explanation for the weirdness, but my numbers still look correct.
#---------------------------------------------------------------------------------------------------------#
{
        df11 <- df5[df5$MicroBiltResponse=="1", c("MicroBiltResponse","TotalProfit")]
        sum(df11$TotalProfit[11000:12000], na.rm = T)
        df11$TotalProfit[11000:11500]
        
        # What was the actual profit on the loans where these Bureaus were called?
        sum(df5$TotalProfit[df5$ClarityResponse=="1" | df5$ClarityResponse=="2"])            #  5,997,860
        sum(df5$TotalProfit[df5$DataXResponse=="1" | df5$DataXResponse=="2"])                #  2,580,165
        sum(df5$TotalProfit[df5$MicroBiltResponse=="1" | df5$MicroBiltResponse=="2"])        # -4,346,764
        sum(df5$TotalProfit[df5$FactorTrustResponse=="1" | df5$FactorTrustResponse=="2"])    # -1,117,955
        
        # What was the actual profit on the loans where these Bureaus were NOT called?
        sum(df5$TotalProfit[df5$ClarityResponse=="3"])         #    735,642
        sum(df5$TotalProfit[df5$DataXResponse=="3"])           #  4,153,337
        sum(df5$TotalProfit[df5$MicroBiltResponse=="3"])       # 11,080,266
        sum(df5$TotalProfit[df5$FactorTrustResponse=="3"])     #  7,851,457
        
        # Let's look at all 4 side by side...hmmmm. Looks like quite a few times where cl=2, dx=2 and mb=1 and totalprofit is NEGATIVE. That explains it.
        df12 <- df5[df5$MicroBiltResponse=="1", c("ClarityResponse","DataXResponse","MicroBiltResponse","FactorTrustResponse","TotalProfit")]
        sum(df12$TotalProfit[10000:11000], na.rm = T)
        df12[10000:10500,]
}
#---------------------------------------------------------------------------------------------------------#
#    Trying to figure out why we have 2 loans with the same number
#---------------------------------------------------------------------------------------------------------#
{
        # Sanity check to make sure we have one loan per aggregated AgreementNumber
        length(df2$AgreementNumber)                  # 144,526 Loans with at least ONE valid payment
        length(unique(df2$AgreementNumber))          # 144,524 unique Loans - mismatch, dig into it more
        
        df2 <- df2[with(df2, order(AgreementNumber)), ]
        df2[duplicated(df2$AgreementNumber),]
        
        # For some reason, we have 2 loans that have the same AgreementNumber, but 2 different origination dates and 2 different "STATUS"/FundedAmount/etc.
        df[df$AgreementNumber=="724001-2",]
        df[df$AgreementNumber=="751573-2",]
}



#*************************************************END*****************************************************
