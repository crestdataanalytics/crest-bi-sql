#jeff wants to see the performance on accounts where there was a hard denial and they funded anyway

library(xlsx)

#get the leases that are funded, hardkilldenied, and created after jan 2014 
    leases <- dfV3[!is.na(dfV3$FundedDate),]
    table(leases$Decision)
    leases <- leases[leases$Decision %in% c("HardKillDenied"),]
    leases <- leases[leases$CreationDate >= "2014-01-01",]
    table(leases$Status)
    leases <- leases[leases$Status %in% c("DealerReturn","Denied") == F,]
    leases$FirstDueDate <- as.Date(leases$FirstDueDate)
    
    theDueDate <- as.Date(max(dfV3$CreationDate))
    day(theDueDate) <- day(theDueDate) - 7
    
    leases <- leases[!is.na(leases$FirstDueDate) & leases$FirstDueDate <= theDueDate,]
    
#get total per vintage and then charge off of unique 
    leases$Vintage <- as.Date(leases$FundedDate)
    day(leases$Vintage) <- 1
    
    #unique leases
        uniqueLeases <- leases[order(leases$ApplicantID,leases$CreationDate,decreasing = T),]
        uniqueLeases$row.number <- F
        uniqueLeases <- subset(uniqueLeases, subset = !duplicated(uniqueLeases[c("ApplicantID")]))
    #total
        leasesAgg <- aggregate(ID ~ Vintage, data = uniqueLeases, FUN = c("length"))
        colnames(leasesAgg)[2] <- "TotalFunded"
    
    #FPD
        leasesFPD <- uniqueLeases[!is.na(uniqueLeases$FirstPaymentDefault) & uniqueLeases$FirstPaymentDefault == 1,]
        leasesFPDAgg <- aggregate(ID ~ Vintage, data = leasesFPD, FUN = c("length"))
        colnames(leasesFPDAgg)[2] <- "TotalFPD"
        
        leasesAgg <- merge(leasesAgg,leasesFPDAgg, by = c("Vintage"),all.x = T)
        leasesAgg$FPDPercent <- leasesAgg$TotalFPD / leasesAgg$TotalFunded * 100
        
    #Charge off
        leasesCO <- uniqueLeases[!is.na(uniqueLeases$ChargedOffDate) & uniqueLeases$Status %in% c("Bankrupt13","Bankrupt7","Charged Off"),]
        leasesCOAgg <- aggregate(ID ~ Vintage, data = leasesCO, FUN = c("length"))
        colnames(leasesCOAgg)[2] <- "TotalChargedOff"
        
        leasesAgg <- merge(leasesAgg,leasesCOAgg, by = c("Vintage"),all.x = T)
        leasesAgg$ChargedOffPercent <- leasesAgg$TotalChargedOff / leasesAgg$TotalFunded * 100
        
    #cash paid and cash collected
        leasesMerchantPaidAgg <- aggregate(MerchantPaidAmount ~ Vintage, data = uniqueLeases, FUN = c("sum"))
        leasesCumulativeCollectedAgg <- aggregate(CumulativeAmountCollected ~ Vintage, data = uniqueLeases, FUN = c("sum"))
        
        leasesAgg <- merge(leasesAgg,leasesMerchantPaidAgg, by = c("Vintage"),all.x = T)
        leasesAgg <- merge(leasesAgg,leasesCumulativeCollectedAgg, by = c("Vintage"),all.x = T)
        leasesAgg$MerchantPaidAmount <- as.numeric(leasesAgg$MerchantPaidAmount)
        leasesAgg$CumulativeAmountCollected <- as.numeric(leasesAgg$CumulativeAmountCollected)
        leasesAgg$CashMultiple <- leasesAgg$CumulativeAmountCollected / leasesAgg$MerchantPaidAmount * 100
        
        
#give jeff the raw data
    leases <- leases[,c("ID","ApplicantID","CreationDate","FundedDate","ChargedOffDate","FirstPaymentDefault","Decision","ApprovalProviderSettingsID")]
    
    fileTitle <- paste0("HardKillDenied_Funded_Performance_",month(Sys.Date()),"_",day(Sys.Date()),"_",year(Sys.Date()),".xlsx")
    
    write.xlsx(leasesAgg, file = fileTitle, sheetName = "Vintages", row.names = F, append = T)
    write.xlsx(leases, file = fileTitle, sheetName = "RawData", row.names = F, append = T)
    
        