setwd("F:/CSVFiles")

df <- read.csv("PaymentsWithBankInfo_2014.csv", stringsAsFactors=F, colClasses = c("character"))
df1 <- read.csv("PaymentsWithBankInfo_2015_1.csv", stringsAsFactors=F, colClasses = c("character"))
df2<- read.csv("PaymentsWithBankInfo_2015_2.csv", stringsAsFactors=F, colClasses = c("character"))

dfbind <- rbind(df,df1,df2)
#dfbind$LoanID <- as.numeric(dfbind$LoanID)
dfbind$ProcessedDate <- as.Date(dfbind$ProcessedDate, format = "%m/%d/%Y")
colnames(dfbind)[1] <- "LoanID"

#orders by LoanID then by ProcessedDate
dfbind <- dfbind[order(dfbind$LoanID,dfbind$ProcessedDate, decreasing = F) ,]
dfbind <- unique(dfbind)
#this gets unique payment ID's (just in case there are duplicates)
dfbind <- subset(dfbind, subset = !duplicated(dfbind[c("PaymentID")]))
dfbind <- data.frame(dfbind, row.names = NULL)
dfbind$Month <- format(dfbind$ProcessedDate, "%m")
dfbind$Year <- format(dfbind$ProcessedDate, "%Y")

#this makes a copy just in case we need it later or for comparison purposes.
dfbindcopy <- dfbind
#dfbind <- dfbindcopy

#sets fields for aggregation later
dfbind$FirstBouncedPayment <- 0
dfbind$FirstBouncedPaymentWrongAcct <- 0
dfbind$SecondPaymentWithDiffMOP <- 0

#this gets the first transaction and then just gets the ones that are retunred for FPD
dfFPD <- dfbind[dfbind$ReturnCode != "NULL",]
dfFPD <- dfFPD[dfFPD$FirstDraft == 1,]
dfFPD$FirstBouncedPayment <- 1



length(dfFPD$LoanID[dfFPD$Month == "01" & dfFPD$Year == "2014"])
length(dfFPD$LoanID[dfFPD$Month == "01" & dfFPD$Year == "2014"])
length(dfbind$LoanID[dfbind$Month == "01" & dfbind$Year == "2014" & dfbind$ReturnCode != "NULL"])




#length(dfFPD$LoanID[dfFPD$FirstBouncedPayment == 1 & dfFPD$Month == "02" & dfFPD$Year == "2014"])

dfFPDWrongAccount <- dfFPD[dfFPD$ReturnCode == "R03" | dfFPD$ReturnCode == "R04", ]
dfFPDWrongAccount$FirstBouncedPaymentWrongAcct <- 1

#this removes the FPD transactions from the dfbind to not double count
dfbind <- dfbind[ (dfbind$PaymentID %in% dfFPDWrongAccount$PaymentID) == F,]
dfbind <- dfbind[ (dfbind$PaymentID %in% dfFPD$PaymentID) == F,]
dfFPD <- dfFPD[ (dfFPD$PaymentID %in% dfFPDWrongAccount$PaymentID) == F,]

#put it all back together
dfAll <- rbind(dfbind, dfFPD, dfFPDWrongAccount)
dfAll <- subset(dfAll, subset = !duplicated(dfAll[c("PaymentID")]))

firstAgg <- aggregate(FirstBouncedPayment~Month+Year, FUN = "sum", data = dfAll)
#length(dfAll$LoanID[dfAll$FirstBouncedPayment==1])
#length(dfAll$LoanID[dfAll$SecondPaymentWithDiffMOP==1])
#length(dfAll$LoanID[dfAll$FirstBouncedPaymentWrongAcct==1])
firstWrongAcctAgg <- aggregate(FirstBouncedPaymentWrongAcct~Month+Year, FUN = "sum", data = dfAll)
totalAgg <- aggregate(LoanID~Month+Year, FUN = "length", data = dfAll)

aggAll <- merge(totalAgg, firstAgg)
aggAll <- merge(aggAll, firstWrongAcctAgg)
colnames(aggAll)[3] <- "TotalTransactions"

aggAll <- aggAll[order(aggAll$Year,aggAll$Month, decreasing = F) ,]
aggAll$FPBounceRatio <- aggAll$FirstBouncedPayment / aggAll$TotalTransactions
aggAll$WrongMOPRatio <- aggAll$FirstBouncedPaymentWrongAcct / aggAll$TotalTransactions

x<- data.frame(table(dfAll$ReturnCode[dfAll$Month == "07" & dfAll$Year == "2015" & dfAll$ReturnCode != "NULL"]))

write.csv(aggAll, file = "WrongBankAccounts.csv", quote = T, row.names = F)
write.csv(x, file = "WrongBankAccountsRCodes.csv", quote = T, row.names = F)
