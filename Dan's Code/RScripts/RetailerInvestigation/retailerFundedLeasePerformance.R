getwd()
setwd("F:/CSVFiles")
df <- read.csv("sleepAuthority.csv", stringsAsFactors = F)
colnames(df)[1] <- "Year"
df$LenTimeToDefault <- as.numeric(df$LenTimeToDefault)
df$IsBounced <- 0
df$IsChargedOff <- 0
df$IsBounced[df$Status == "Bounced"] <- 1
df$IsChargedOff[df$Status == "Charged Off" | df$Status == "Bankrupt7" | df$Status == "Bankrupt13"] <- 1

#Bounced or Charged Off
dfBCO <- df[df$Status == "Bounced" | df$Status == "Charged Off" | df$Status == "Bankrupt7" | df$Status == "Bankrupt13",]
rownames(dfBCO) <- NULL

dfBCOFPD <- dfBCO[dfBCO$FirstPaymentDefault == 1,]
rownames(dfBCOFPD) <- NULL

dfBCONotFPD <- dfBCO[dfBCO$FirstPaymentDefault == 0,]
rownames(dfBCONotFPD) <- NULL

#Not bounced or Charged off
dfNotBCO <- df[df$Status == "Funded" | df$Status == "Paid",]
rownames(dfBCOFPD) <- NULL

dfNotBCOFPD <- dfNotBCO[dfNotBCO$FirstPaymentDefault == 1,]
rownames(dfNotBCOFPD) <- NULL

dfNotBCONotFPD <- dfNotBCO[dfNotBCO$FirstPaymentDefault == 0,]
rownames(dfNotBCONotFPD) <- NULL

#bounced or charged off
BCOFPDMean <- aggregate(LenTimeToDefault~Year+Month,dfBCOFPD,mean)
colnames(BCOFPDMean)[3] <- "LenTimeToDefault_BCOFPDMean"

BCONotFPDMean <- aggregate(LenTimeToDefault~Year+Month,dfBCONotFPD,mean)
colnames(BCONotFPDMean)[3] <- "LenTimeToDefault_BCONotFPDMean"

FPDRatioInBCO <- aggregate(FirstPaymentDefault~Year+Month,dfBCO,mean)
colnames(FPDRatioInBCO)[3] <- "FPDRatioInBCO"

#not bounced or charged off
NotBCOFPDMean <- aggregate(LenTimeToDefault~Year+Month,dfNotBCOFPD,mean)
colnames(NotBCOFPDMean)[3] <- "LenTimeToDefault_NotBCOFPDMean"

NotBCONotFPDMean <- aggregate(LenTimeToDefault~Year+Month,dfNotBCONotFPD,mean)
colnames(NotBCONotFPDMean)[3] <- "LenTimeToDefault_NotBCONotFPDMean"

FPDRatioInNotBCO <- aggregate(FirstPaymentDefault~Year+Month,dfNotBCO,mean)
colnames(FPDRatioInNotBCO)[3] <- "FPDRatioInNotBCO"

FPDRatioInTotal <- aggregate(FirstPaymentDefault~Year+Month,df,mean)
colnames(FPDRatioInTotal)[3] <- "FPDRatioInTotal"

bouncedRatioTotal <- aggregate(IsBounced~Year+Month,df,mean)
colnames(bouncedRatioTotal)[3] <- "bouncedRatioTotal"

chargedOffRatioTotal <- aggregate(IsChargedOff~Year+Month,df,mean)
colnames(chargedOffRatioTotal)[3] <- "chargedOffRatioTotal"

TotalFundedApplicants <- aggregate(ApplicantID~Year+Month,df,length)
colnames(TotalFundedApplicants)[3] <- "TotalFundedApplicants"

summaryDf <- merge(BCOFPDMean,BCONotFPDMean, by = c("Year","Month"))
summaryDf <- merge(summaryDf, NotBCOFPDMean, by = c("Year","Month"), all = T)
summaryDf <- merge(summaryDf, NotBCONotFPDMean, by = c("Year","Month"), all = T)
summaryDf <- merge(summaryDf, FPDRatioInBCO, by = c("Year","Month"), all = T)
summaryDf <- merge(summaryDf, FPDRatioInNotBCO, by = c("Year","Month"), all = T)
summaryDf <- merge(summaryDf, FPDRatioInTotal, by = c("Year","Month"), all = T)
summaryDf <- merge(summaryDf, bouncedRatioTotal, by = c("Year","Month"), all = T)
summaryDf <- merge(summaryDf, chargedOffRatioTotal, by = c("Year","Month"), all = T)
summaryDf <- merge(summaryDf, TotalFundedApplicants, by = c("Year","Month"), all = T)
summaryDf$BCORatioTotal <- summaryDf$bouncedRatioTotal + summaryDf$chargedOffRatioTotal
summaryDf <- summaryDf[with(summaryDf, order(Year, Month)),]
rownames(summaryDf) <- NULL

#this should cover for much of the up swing (very quick and dirty)
summaryDf$MeanBCORatioTotal <- mean(summaryDf$BCORatioTotal)


#roughly how many accounts will default and when
summaryDf$PotentialBCO <- round((summaryDf$MeanBCORatioTotal * summaryDf$TotalFundedApplicants) - 
  ((summaryDf$bouncedRatioTotal + summaryDf$chargedOffRatioTotal) * summaryDf$TotalFundedApplicants), digits = 0)
summaryDf$MeanLenTimeToDefault_BCONotFPDMean <- mean(summaryDf$LenTimeToDefault_BCONotFPDMean, na.rm = F)
summaryDf[is.na(summaryDf)] <- 0
write.csv(summaryDf ,file = "sleepAuthoritySummary.csv", row.names = F)


