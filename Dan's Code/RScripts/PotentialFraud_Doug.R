#look at accounts that are paying off quick and getting another lease for Doug
{
        qpo <- df
        qpo$FundedDate <- as.Date(qpo$FundedDate, format = "%Y-%m-%d")
        qpoPaid <- qpo[!is.na(qpo$PaidOffDate),]
        qpoPaid <- qpoPaid[, c("ID","ApplicantID","PaidOffDate", "Status")]
        AppIDsQPO <- qpoPaid$ApplicantID
        
        qpoNextLease <- qpo[qpo$ApplicantID %in% AppIDsQPO,]
        qpoNextLease <- qpoNextLease[qpoNextLease$FundedIndicator == 1,]
        qpoNextLease <- qpoNextLease[, c("ID","ApplicantID","FundedDate", "Status")]
        colnames(qpoNextLease)[1] <- "NextLeaseID"
        colnames(qpoNextLease)[3] <- "NextFundedDate"
        colnames(qpoNextLease)[4] <- "NextStatus"
        
        nextLease <- merge(x = qpoPaid, y = qpoNextLease, by = c("ApplicantID"))
        nextLease <- nextLease[nextLease$NextLeaseID > nextLease$ID,]
        nextLease <- nextLease[order(nextLease$NextLeaseID),]
        nextLease <- data.frame(nextLease, row.number = F)
        nextLease <- subset(nextLease, subset = !duplicated(nextLease[c("ID")]))
        
        table(nextLease$Status)
        
        firstNotPaid <- nextLease[nextLease$Status %in% c("Bounced", "Charged Off"),]
        
        length(unique(firstNotPaid$ApplicantID))
        
        countIDs <- aggregate(ApplicantID~ID, data = nextLease, FUN = length)
        
}