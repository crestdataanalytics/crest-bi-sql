#need to evaluate the leases where they had between 90-180 days on job (creationdate - hire date)

#get leases created april 2016 forward .... and funded with first due date
    lpV2Leases$CreationDate <- as.POSIXct(lpV2Leases$OriginalCreationDate)
    leases <- lpV2Leases
    leases <- leases[year(leases$CreationDate) == 2016,]
    leases <- leases[leases$CreationDate >= "2016-04-01",]
    leases$HireDate <- as.POSIXct(leases$OriginalHireDate)
    theDueDate <- max(leases$CreationDate)
    day(theDueDate) <- day(theDueDate) - 7
    leases <- leases[!is.na(leases$FundedDate),]
    table(leases$Status)
    leases <- leases[leases$Status %in% c("DealerReturn","Denied") == F,]
    leases <- leases[!is.na(leases$FirstDueDate) & leases$FirstDueDate <= theDueDate,]
    leases$HireDate <- as.POSIXct(leases$OriginalHireDate)
    
#look at them altogether and then by vintage
    #altogether
        job <- leases[!is.na(leases$HireDate),]
        job$DaysAtJob <- difftime(job$CreationDate, job$HireDate,units = "days")
        job <- job[job$DaysAtJob >= 90 & job$DaysAtJob <= 180,]
        jobFPD <- length(job$ID[!is.na(job$FirstPaymentDefault) & job$FirstPaymentDefault == 1]) / length(job$ID)
        job$Vintage <- as.Date(job$CreationDate)
        day(job$Vintage) <- 1
        
        #write these to csv to do pivot
            writeJob <- job[,c("ID","Vintage","FirstPaymentDefault","MerchantPaidAmount")]
            
            #get ebureau score
                setwd("F:/CSVFiles/DataProviderAnalysis/DFsFromR")
                aprl <- data.frame(fread("lpV2_APRL.csv",stringsAsFactors = F))
                setwd("F:/CSVFiles/DataProviderAnalysis")
                
                eb <- aprl[!is.na(aprl$ApprovalProviderSettingsID) & aprl$ApprovalProviderSettingsID == 6,]
                rm(aprl)
                gc()
                eb$InquiryDate <- as.POSIXct(eb$OriginalInquiryDate)
                eb$CreationDate <- as.POSIXct(eb$OriginalCreationDate)
                summary(eb$InquiryDate)
                summary(eb$CreationDate)
                
                eb$row.number <- F
                eb <- eb[order(eb$ID,eb$CreationDate,decreasing = T),]
                eb <- subset(eb, subset = !duplicated(eb[c("ID")]))
                eb <- eb[eb$CreationDate >= min(eb$InquiryDate),]
                eb <- eb[,c("ID","RiskScore")]
                gc()
                
                writeJob <- merge(writeJob,eb,by = c("ID"),all.x = T)
                table(is.na(writeJob$RiskScore))
                write.csv(writeJob,file = "TimeAtJob_EbureauScore.csv",quote = T,row.names = F)
        
        test <- job[,c("ID","CreationDate","HireDate","DaysAtJob")]
        head(test)
        
    #by vintage
        total <- aggregate(ID ~ Vintage, data = job, FUN = "length")
        colnames(total)[2] <- "Total"
        
        fpd <- job[!is.na(job$FirstPaymentDefault) & job$FirstPaymentDefault == 1,]
        fpdagg <- aggregate(ID ~ Vintage, data = fpd, FUN = "length")
        colnames(fpdagg)[2] <- "fpd"
        total <- merge(total,fpdagg, by = c("Vintage"),all.x = T)
        total$fpdPercent <- total$fpd / total$Total * 100
        
        #cash
            totalCash <- aggregate(MerchantPaidAmount ~ Vintage, data = job, FUN = "sum")
            colnames(totalCash)[2] <- "TotalCash"
            
            fpd <- job[!is.na(job$FirstPaymentDefault) & job$FirstPaymentDefault == 1,]
            fpdCash <- aggregate(MerchantPaidAmount ~ Vintage, data = fpd, FUN = "sum")
            colnames(fpdCash)[2] <- "fpdCash"
            total <- merge(total,totalCash, by = c("Vintage"),all.x = T)
            total <- merge(total,fpdCash, by = c("Vintage"),all.x = T)
            total$fpdCashPercent <- total$fpdCash / total$TotalCash * 100
        
#dean had questions about the lift it's providing
    lift <- lpV2Leases
    lift <- lift[year(lift$CreationDate) == 2016,]
    lift$CreationDate <- as.POSIXct(lift$OriginalCreationDate)
    lift <- lift[lift$CreationDate >= "2016-04-01",]
    lift$HireDate <- as.POSIXct(lift$HireDate)
    lift$DaysAtJob <- difftime(lift$CreationDate, lift$HireDate,units = "days")
    lift <- lift[!is.na(lift$DaysAtJob),]
    lift$Vintage <- as.Date(lift$CreationDate)
    day(lift$Vintage) <- 1
    
    #need to get approval rate for population >180 days at job ... then what it was altogether ... and what lift was on auto approved leases
        original <- lift
        originalTotal <- aggregate(ID ~ Vintage, data = original, FUN = "length")
        colnames(originalTotal)[2] <- "OriginalTotal"
        
        autoapproved <- original[original$AutoApprovedFlag == 1 & !is.na(original$AutoApprovedFlag) & !is.na(original$ApprovalDate) & original$DaysAtJob >= 180,]
        autoapprovedTotal <- aggregate(ID ~ Vintage, data = autoapproved, FUN = "length")
        colnames(autoapprovedTotal)[2] <- "OriginalAutoApproved"
        originalTotal <- merge(originalTotal,autoapprovedTotal, by = c("Vintage"),all.x = T)
        originalTotal$AutoApprovedPercent <- originalTotal$OriginalAutoApproved / originalTotal$OriginalTotal * 100
        
        #now with the time at job change
            autoapproved <- original[original$AutoApprovedFlag == 1 & !is.na(original$AutoApprovedFlag) & !is.na(original$ApprovalDate) & original$DaysAtJob >= 90,]
            autoapprovedTotal <- aggregate(ID ~ Vintage, data = autoapproved, FUN = "length")
            colnames(autoapprovedTotal)[2] <- "NewAutoApproved"
            originalTotal <- merge(originalTotal,autoapprovedTotal, by = c("Vintage"),all.x = T)
            originalTotal$NewAutoApprovedPercent <- originalTotal$NewAutoApproved / originalTotal$OriginalTotal * 100
            gc()
        
    
rm(eb,finalDocIDs,job,leases,writeJob)
gc()
    
    
    
    
    
    
    
    
    
    
    
    
    