#function to build file for customer Fraud Score

###THIS WILL BUILD WITH DATA FROM APPROVAL PROVIDER RESPONSE LOG

library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)
library(stringr)


buildFileForFraudScore_DataRobot <- function(setOfApprovalProviderSettingsIDs,theStartDate,theEndDate){
    # setOfApprovalProviderSettingsIDs <- c(6,2)
    # theStartDate <- "2016-11-01"
    # theEndDate <- "2016-12-01"
    
    theStartDate <- as.Date(theStartDate)
    theEndDate <- as.Date(theEndDate)
    setOfApprovalProviderSettingsIDsString <- paste(setOfApprovalProviderSettingsIDs,collapse = ",")
    
    #get leases
        if(exists("leases")){
            1==1
        }else{
            setwd("E:/Analytics/RScripts/DataProviderAnalysis")
            source("Leases_OpenMostRecent.R",echo = T)
        }

    #read in xml shred files
        cfsql02_CrestAnalytics <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestAnalytics;trusted_connection=true')
        script <- "SELECT DISTINCT ApprovalProviderResponseLogID,ApprovalProviderSettingsID, ApplicantID, CreatedDate FROM CrestAnalytics.dbo.XMLShred_Data" 
        script <- paste0(script," WHERE ApprovalProviderSettingsID IN (",setOfApprovalProviderSettingsIDsString,")")
        script <- paste0(script, " AND CreatedDate >= '",theStartDate,"' AND CreatedDate < '",theEndDate,"'")
        aprlRecords <- sqlQuery(cfsql02_CrestAnalytics, script, stringsAsFactors = F)
        table(aprlRecords$ApprovalProviderSettingsID)
        aprlRecords$row.number <- F
        aprlRecords <- aprlRecords[order(aprlRecords$ApplicantID,aprlRecords$CreatedDate,decreasing = T),]
        aprlRecords <- subset(aprlRecords,subset = !duplicated(aprlRecords[c("ApplicantID","ApprovalProviderSettingsID")]))
        # length(unique(aprlRecords$ApprovalProviderResponseLogID))
        # check <- subset(aprlRecords,subset = duplicated(aprlRecords[c("ApprovalProviderResponseLogID")]))
        # setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        # listedFiles <- list.files()
    
    #get leases created 12-16-2016 to 1-15-2017 and 45 days out from first due date
        templeases <- leases[leases$CreationDate >= theStartDate & leases$CreationDate < theEndDate,]
        
        # templeases$Days <- as.numeric(difftime(Sys.Date(),templeases$FirstDueDate, units = c("days")))
        # summary(templeases$Days)
        # templeases <- templeases[templeases$Days >= 45,]
        templeases$row.number <- F
        templeases <- templeases[order(templeases$ApplicantID,templeases$ID,decreasing = F),]
        templeases <- subset(templeases,subset = !duplicated(templeases[c("ApplicantID")]))
        
        #set 45/60/90 dnp flags
            templeases45 <- templeases[!is.na(templeases$FundedPastFirstDueDate) & templeases$FundedPastFirstDueDate == 1,]
            templeases45$NoPay_45 <- 0
            templeases45$NoPay_45[templeases45$DaysToPayment >= 45] <- 1
            templeases45 <- templeases45[,c("ID","NoPay_45")]
            templeases <- merge(templeases,templeases45,by = c("ID"),all.x = T)
            rm(templeases45)
            gc()
            # table(templeases$NoPay_45)
        
        #subset the shredded data for qualifying funded leases
        # keepAPRLIDs <- unique(xmlShred[,c("ApplicantID","ApprovalProviderResponseLogID","ApprovalProviderSettingsID")])
        # keepAPRLIDs$row.number <- F
        # keepAPRLIDs <- keepAPRLIDs[order(keepAPRLIDs$ApplicantID,keepAPRLIDs$ApprovalProviderResponseLogID,keepAPRLIDs$ApprovalProviderSettingsID,decreasing = T),]
        # keepAPRLIDs <- subset(keepAPRLIDs,subset = !duplicated(keepAPRLIDs[c("ApplicantID","ApprovalProviderSettingsID")]))
            
        #keep leases and aprls
            # keepAPRLIDs <- unique(aprlRecords$ApprovalProviderResponseLogID)
            keepApplicantIDs <- templeases$ApplicantID
            gc()
            
            aprlRecords <- aprlRecords[aprlRecords$ApplicantID %in% keepApplicantIDs,]
        
        
        
        
        #get shred data
            keepAPRLIDs <- paste(aprlRecords$ApprovalProviderResponseLogID,collapse = ",")
            cfsql02_CrestAnalytics <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestAnalytics;trusted_connection=true')
            script <- paste0("SELECT * FROM CrestAnalytics.dbo.XMLShred_Data WHERE ApprovalProviderResponseLogID IN (",keepAPRLIDs,")")
            # script <- paste0(script," AND XPathID IN (",xpathIDs_String,")")
            xmlShred <- sqlQuery(cfsql02_CrestAnalytics, script, stringsAsFactors = F)
            # xmlShred <- xmlShred[xmlShred$ApprovalProviderResponseLogID %in% keepAPRLIDs,]
            # keepApplicantIDs <- templeases$ApplicantID
            xmlShred <- xmlShred[xmlShred$ApplicantID %in% keepApplicantIDs,]
            gc()
            # table(xmlShred$ApprovalProviderSettingsID)

    

    #normalize shredded data
    #get unique xpaths and ids to append
        uniqueXPathIDs <- unique(xmlShred$XPathID)
        xpathIDs_String <- paste(uniqueXPathIDs,collapse = ",")

    #pull data
        cfsql02_CrestAnalytics <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestAnalytics;trusted_connection=true')
        script <- "SELECT XPath,XPathID FROM CrestAnalytics.dbo.XMLShred_XPaths WHERE XPathID IN ("
        script <- paste0(script,xpathIDs_String,")")
        xpathDB <- sqlQuery(cfsql02_CrestAnalytics, script, stringsAsFactors = F)

    #subset the CL xpaths
        clXPaths <- xpathDB[grepl("GetDataResponse",xpathDB$XPath),]
        xpathDB <- xpathDB[xpathDB$XPathID %in% clXPaths$XPathID == F,]
        clXPaths <- clXPaths[grepl("TransactionResponse",clXPaths$XPath),]
        clXPaths <- clXPaths[grepl("Subscriber",clXPaths$XPath) == F,]
        clXPaths <- clXPaths[grepl("RequestDetails",clXPaths$XPath) == F,]
        xpathDB <- rbind(xpathDB,clXPaths)

    #remove unneccessary xpaths
        xpathDB <- xpathDB[grepl("_Input_",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("_CRAResponse_Consumer_",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("GenerationTime",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("CodeVersion",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("RequestVersion",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("TransactionId",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("TrackHash",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("TrackId",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("transaction_input_",xpathDB$XPath) == F,]
        xpathDB <- xpathDB[grepl("transaction_status_",xpathDB$XPath) == F,]

    #filter out "text for datax
        tempxpathDB <- xpathDB
        tempxpathDB <- tempxpathDB[grepl("Response",tempxpathDB$XPath),]
        tempxpathDB <- tempxpathDB[grepl("_text",tempxpathDB$XPath),]
        xpathDB <- xpathDB[xpathDB$XPathID %in% tempxpathDB$XPathID == F,]




    #loop through xpathIDs to do this
        uniqueXPathIDs <- unique(xpathDB$XPathID)
        finalNormalizedXML <- data.frame(unique(xmlShred$ApplicantID),stringsAsFactors = F)
        colnames(finalNormalizedXML)[1] <- "ApplicantID"
        count <- 0
        # uniqueXPathIDs <- uniqueXPathIDs[uniqueXPathIDs %in% c(5) == F]


        setwd("E:/Analytics/RScripts/DataProviderAnalysis")
        source("ProcessAndNormalizeXMLShred.R",echo = T)
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        finalNormalizedXML <- ProcessAndNormalizeXMLShred(uniqueXPathIDs,finalNormalizedXML,xmlShred)




    ###ADD THE APRLID on each applicant for each bureau at the end so we can tie back
        APRLID <- unique(xmlShred[,c("ApplicantID","ApprovalProviderSettingsID","ApprovalProviderResponseLogID")])

        #dx
            dx <- APRLID[APRLID$ApprovalProviderSettingsID == 2,]
            dx$ApprovalProviderSettingsID <- NULL
            names(dx)[names(dx) == "ApprovalProviderResponseLogID"] <- "DataX_ApprovalProviderResponseLogID"
            finalNormalizedXML <- merge(finalNormalizedXML,dx,by = c("ApplicantID"),all.x = T)

        #eb
            eb <- APRLID[APRLID$ApprovalProviderSettingsID == 6,]
            eb$ApprovalProviderSettingsID <- NULL
            names(eb)[names(eb) == "ApprovalProviderResponseLogID"] <- "eBureau_ApprovalProviderResponseLogID"
            finalNormalizedXML <- merge(finalNormalizedXML,eb,by = c("ApplicantID"),all.x = T)

        rm(dx,eb,cl,APRLID)
        gc()

    ###ADD State, zipcode
    #get data from production
    #GET APRLID FOR THE EBUREAU PULLS
        ebAPRLIDs <- unique(xmlShred$ApprovalProviderResponseLogID[xmlShred$ApprovalProviderSettingsID == 6])
        ebAPRLIDs_String <- paste(ebAPRLIDs,collapse = ",")

        cfsql02_CrestWarehouse <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestWarehouse;trusted_connection=true')
        script <- "SELECT RequestXML, ID AS ApprovalProviderResponseLogID, ApplicantID FROM CrestWarehouse.dbo.ApprovalProviderResponseLog WHERE ID IN ("
        script <- paste0(script,ebAPRLIDs_String,")")
        requestXML <- sqlQuery(cfsql02_CrestWarehouse, script, stringsAsFactors = F)
        # requestXML <- requestXML[requestXML$ApprovalProviderResponseLogID %in% unique(xmlShred$ApprovalProviderResponseLogID),]
        requestXML$Zip <- as.numeric(sub("</zip>.*","",sub("^.*<zip>","",requestXML$RequestXML)))
        requestXML$DOB <- sub("</dob>.*","",sub("^.*<dob>","",requestXML$RequestXML))
        requestXML$DOB[grepl("<",requestXML$DOB)] <- ""
        requestXML$DOB_Year <- as.numeric(substr(requestXML$DOB,1,4))
        requestXML$DOB_Year[is.na(requestXML$DOB_Year)] <- 0
        requestXML$DOB_Month <- as.numeric(substr(requestXML$DOB,5,6))
        requestXML$DOB_Month[is.na(requestXML$DOB_Month)] <- 0
        requestXML$PayFrequency <- sub("</interval>.*","",sub("^.*<interval>","",requestXML$RequestXML))
        requestXML$PayFrequency[grepl("<",requestXML$PayFrequency)] <- ""
        requestXML$MonthlyIncome <- as.numeric(sub("</pay>.*","",sub("^.*<pay>","",requestXML$RequestXML)))
        requestXML$WorkName <- sub("</company>.*","",sub("^.*<company>","",requestXML$RequestXML))
        requestXML$WorkName[grepl("<",requestXML$WorkName)] <- ""
        requestXML$WorkName <- gsub(" ","",requestXML$WorkName)
        requestXML$WorkName <- tolower(requestXML$WorkName)
        requestXML$WorkName[is.na(requestXML$WorkName)] <- "Missing"
        requestXML$RequestXML <- NULL
        requestXML$ApprovalProviderResponseLogID <- NULL
        requestXML$DOB <- NULL
        finalNormalizedXML <- merge(finalNormalizedXML,requestXML,by = c("ApplicantID"),all.x = T)
        finalNormalizedXML <- finalNormalizedXML[!is.na(finalNormalizedXML$DOB_Year),]
        # table(is.na(finalNormalizedXML$Zip))
        rm(requestXML)
        gc()

    #retailer info
    #cash collection curve
        setwd("E:/Analytics/RScripts/DataProviderAnalysis")
        source("GetCashCollectionCurve.R",echo = T)
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        ccc <- getCashCollectionCurve(templeases[,c("ID","RetailerID","CreationDate")])
        ccc <- ccc[,c("ID","CashCollectionCurve")]
        templeases <- merge(templeases,ccc,by = c("ID"),all.x = T)

    #BIScore
        setwd("E:/Analytics/RScripts/DataProviderAnalysis")
        source("GetBIScore.R",echo = T)
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        biscore <- getBIScore(templeases[,c("ID","RetailerID","CreationDate")])
        biscore <- biscore[,c("ID","BIScore")]
        templeases <- merge(templeases,biscore,by = c("ID"),all.x = T)


    #get retailer fraud score
        setwd("E:/Analytics/RScripts/DataProviderAnalysis")
        source("GetFraudScore_Retailer_Results.R",echo = T)
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        retailerFraudScore <- getFraudScore_Retailers_Retro_Results(min(templeases$CreationDate,na.rm = T),max(templeases$CreationDate,na.rm = T))
        retailerFraudScore$CreationDate <- as.POSIXct(retailerFraudScore$CreationDate)
        names(retailerFraudScore)[names(retailerFraudScore) == "FraudScore"] <- "RetailerFraudScore"
        templeases <- merge(templeases,retailerFraudScore,by = c("RetailerID","CreationDate"),all.x = T)

    #day of week and hour of day
        templeases$DayOfWeek <- weekdays(templeases$CreationDate)
        table(is.na(templeases$DayOfWeek))

    #merge
        finalNormalizedXML <- merge(templeases[,c("ID","ApplicantID","NoPay_45","BIScore","CashCollectionCurve","DayOfWeek","RetailerFraudScore","ChargeOffIndicator")],
                                    finalNormalizedXML,by = c("ApplicantID"),all.y = T)
        
    #repeat customers
        rc <- leases[,c("ID","FirstPaidLeaseIndicator","SubsequentLeaseIndicator","ReturningCustomerIndicator")]
        rc <- rc[is.na(rc$FirstPaidLeaseIndicator) & !is.na(rc$SubsequentLeaseIndicator) & rc$SubsequentLeaseIndicator == 1,]
        rc <- rc[,c("ID","ReturningCustomerIndicator")]
        finalNormalizedXML <- merge(finalNormalizedXML,rc, by = c("ID"),all.x = T)
        
return(finalNormalizedXML)
}