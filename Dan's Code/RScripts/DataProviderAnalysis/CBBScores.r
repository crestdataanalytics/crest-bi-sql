#check approval rates if cbb score moved to 735

library(lubridate)
library(data.table)
library(fasttime)
library(xlsx)
setwd("F:/CSVFiles/DataProviderAnalysis")

#get the cbb scores, keep only most recent transaction per applicant and tie to leases
    cbb <- read.csv("CBBScores.csv", stringsAsFactors = F, quote = '"', row.names = NULL)
    colnames(cbb)[1] <- "DocumentID"
    cbb$GenerationDate <- as.Date(cbb$GenerationDate)
    cbb <- cbb[order(cbb$ApplicantID, cbb$GenerationDate, decreasing = T),]
    cbb$row.number <- F
    cbb <- subset(cbb, subset = !duplicated(cbb[c("ApplicantID")]))
    cbb$row.number <- NULL
    
    leases <- dfV3Leases
    leases <- leases[leases$CreationDate >= "2016-01-01",]
    leases <- merge(leases,cbb, by = c("ApplicantID"),all.x = T)
    leases$Vintage <- as.Date(leases$CreationDate)
    day(leases$Vintage) <- 1
            
#get total approvals and how many more there would have been with new cbb score
    #total submits
        totalSubmissions <- aggregate(ID ~ Vintage, data = leases, FUN = "length")
        colnames(totalSubmissions)[2] <- "TotalSubmits"
    
    #auto approved
        totalAutoApproved <- leases[!is.na(leases$AutoApprovedFlag) & leases$AutoApprovedFlag == 1,]
        totalAutoApproved <- aggregate(ID ~ Vintage, data = totalAutoApproved, FUN = "length")
        colnames(totalAutoApproved)[2] <- "AutoApproved"
        totalSubmissions <- merge(totalSubmissions,totalAutoApproved, by = c("Vintage"),all.x = T)
        totalSubmissions$AutoApprovedPercent <- totalSubmissions$AutoApproved / totalSubmissions$TotalSubmits * 100
        
    #not auto approved with cbb score less than 765 and >= 735
        #in order to do this thoroughly need to tie on the clarity decision where denial was "cbb score less than ..."
        #keep most recent transaction
            clarityDecisions <- read.csv("Clarity_GlobalDecisions_Since_1_1_2016.csv", stringsAsFactors = F, quote = '"', row.names = NULL)
            colnames(clarityDecisions)[1] <- "DocumentID"
            clarityDecisions$GenerationDate <- as.Date(clarityDecisions$GenerationDate)
            
            recentTransaction <- unique(clarityDecisions[,c("DocumentID","ApplicantID","GenerationDate")])
            recentTransaction <- recentTransaction[order(recentTransaction$ApplicantID,recentTransaction$GenerationDate,decreasing = T),]
            recentTransaction$row.number <- F
            recentTransaction <- subset(recentTransaction, subset = !duplicated(recentTransaction[c("ApplicantID")]))
            
            clarityDecisions <- clarityDecisions[clarityDecisions$DocumentID %in% recentTransaction$DocumentID,]
        
        #identify and keep only clarity denials
            clarityDenials <- clarityDecisions[clarityDecisions$VariableValue == "Deny",]
            clarityDecisions <- clarityDecisions[clarityDecisions$ApplicantID %in% clarityDenials$ApplicantID,]
            
        #identify and keep cbb score less than ...
            cbbScoreLessThan <- clarityDecisions[clarityDecisions$VariableID == 45110,]
            cbbScoreLessThan <- cbbScoreLessThan[grepl("Clear Bank Behavior Score is less than 765",cbbScoreLessThan$VariableValue),]
            table(cbbScoreLessThan$VariableValue)
            
            clarityLift <- leases[leases$ApplicantID %in% cbbScoreLessThan$ApplicantID,]
            
            #tie on the number of applicants denied by cbb score
            cbbDenials <- aggregate(ID ~ Vintage, data = clarityLift, FUN = "length")
            colnames(cbbDenials)[2] <- "CBBScoreDenials"
            totalSubmissions <- merge(totalSubmissions,cbbDenials, by = c("Vintage"),all.x = T)
            
            clarityLift <- clarityLift[clarityLift$VariableValue >= 735,]
            table(clarityLift$AutoApprovedFlag)
            clarityLift <- aggregate(ID ~ Vintage, data = clarityLift, FUN = "length")
            colnames(clarityLift)[2] <- "Above735"
            totalSubmissions <- merge(totalSubmissions,clarityLift, by = c("Vintage"),all.x = T)
            
            totalSubmissions$EstimatedAutoApprovalPercent <- (totalSubmissions$AutoApproved + totalSubmissions$Above735) / totalSubmissions$TotalSubmits * 100
        
            write.csv(totalSubmissions, file = "CBB_PotentialLift_Score735.csv", quote = T, row.names = F)
        
rm(cbb,cbbDenials,cbbScoreLessThan,clarityDenials,clarityDecisions,clarityLift,leases,recentTransaction,totalAutoApproved,totalSubmissions)        
gc()    
