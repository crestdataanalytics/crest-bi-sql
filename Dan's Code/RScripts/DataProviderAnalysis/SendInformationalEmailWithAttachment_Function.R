
#Need a function to set an email

library(gmailr)

sendInformationalEmailWithAttachment <- function(description,theEmailAddress,theBody,theCCEmailAddresses,theFileDirectory,theFileTitle){
    # #testing
    #     setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    #     theEmailAddress <- "dhinkson@crestfinancial.com"
    #     theCCEmailAddresses <- "dhinkson@crestfinancial.com"
    #     theFileTitle <- "ChargeOffProbabilities_4_10_2017.csv"
    #     theFileDirectory <- "//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files"
    #     startTime <- Sys.time()
    #     endTime <- Sys.time()
    #     totalTime <- round(as.numeric(difftime(endTime,startTime,units = "hours")),digits = 2)
    #     description <- paste0(description," - ",totalTime," hours")
    #     theBody <- "test"

    #structure and send email
        setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")
        use_secret_file("Emailing2.json")
        toemail <- theEmailAddress
        toccemail <- theCCEmailAddresses
        subject <- description
        setwd(theFileDirectory)
        attachment <- theFileTitle
        # theBody <- paste0("<html><body>",theBody,"</body></html>")
        mime() %>%
            to(toemail) %>%
            cc(toccemail) %>%
            subject(subject) %>%
            html_body(theBody) %>%
            attach_part(theBody) %>%
            attach_file(theFileTitle) -> theEmail

        send_message(theEmail)
}


