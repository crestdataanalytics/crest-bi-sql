library(lubridate)
library(data.table)
library(fasttime)
library(dplyr)
library(RODBC)
library(gmailr)
library(flexclust)
library(datarobot)
#look at eb and dx



#best models for Fraud Score Analysis----

    #w/ Clusters
        # 0 - 4
            # https://app.datarobot.com/projects/5915f1bdc808916f0a1f9872/models/59161955f8d3651d361f19a0
        # 4 - 6
            # https://app.datarobot.com/projects/59135d9ac8089126fd846599/models/5914c8f5733d590ebe7c13e1

        # 6 - 8
            # https://app.datarobot.com/projects/59135fa3c80891367d538f3d/models/5914ab8812d9d57105b300d8

        # 8 - 10 
            # https://app.datarobot.com/projects/59135f70c80891367d538f33/models/59148145f8d36503cf1f199d

        # 10 - 12
            # https://app.datarobot.com/projects/59135eb0c8089159a0b7cc8f/models/5913e3b4f8d3654a5e1f199b

        #12+
            # https://app.datarobot.com/projects/59135ed1c8089159a0b7cc92/models/5913c3aabbf0cd77d36a1f5c



#Score records for each model and identify what the cutoffs are based on the training set of data for both models
    ConnectToDataRobot(endpoint = "https://app.datarobot.com/api/v2",token = "WBLJi2I9IXpCbiJD2qc6_BDETB8PHITf")
    
#get leases-----
    if(exists("leases")){
        print("Leases already in RAM")
    }else{
        setwd("E:/Analytics/RScripts/DataProviderAnalysis")
        source("Leases_OpenMostRecent.R",echo = T)
    }
    
    
#read in clustered files ... #get the scaled files #nov thru feb-----
    #Read in clustered file
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/FormattedXML_Cluster")
        clusterDF <- data.frame(fread("ClusterDF_DX_EB_Cluster_542017.csv", stringsAsFactors = F))
        theColNames <- names(clusterDF)
        theColNames <- gsub("\\.","_",theColNames)
        colnames(clusterDF) <- theColNames
        table(clusterDF$CreatedDate_Vintage)
        clusterDF <- clusterDF[clusterDF$CreatedDate_Vintage >= "2016-11-01",]
        gc()
        finalNormalizedXML <- clusterDF
    
    #read in cluster performance file to assign tiers
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/FormattedXML_Cluster")
        clusterPerformance <- data.frame(fread("ClusterPerformance.csv", stringsAsFactors = F))
    
#get leases created 45 days out from first due date----
    templeases <- leases[!is.na(leases$FundedPastFirstDueDate) & leases$FundedPastFirstDueDate == 1 & leases$CreationDate >= "2016-11-01",]
    templeases$Days <- as.numeric(difftime(Sys.Date(),templeases$FirstDueDate, units = c("days")))
    summary(templeases$Days)
    templeases <- templeases[templeases$Days >= 45,]
    templeases$row.number <- F
    templeases <- templeases[order(templeases$ApplicantID,templeases$ID,decreasing = F),]
    templeases <- subset(templeases,subset = !duplicated(templeases[c("ApplicantID")]))
        
    #set 45/60/90 dnp flags
        templeases$NoPay_45 <- 0
        templeases$NoPay_45[templeases$DaysToPayment >= 45] <- 1
        table(templeases$NoPay_45)
        
    #append no pay flag to finalNormalizedXML
        finalNormalizedXML <- finalNormalizedXML[finalNormalizedXML$ID %in% templeases$ID,]
        finalNormalizedXML <- merge(finalNormalizedXML,templeases[,c("ApplicantID","NoPay_45")],by = c("ApplicantID"),all.x = T)
        
    # merge
        # templeases <- templeases[,c("ID","Funded","FundedPastFirstDueDate","FundedAmount","CrystalFPDAMT","NoPay_45")]
        clusterDF <- clusterDF[clusterDF$ID %in% templeases$ID,]
        getclusters <- clusterDF[,c("ID","Cluster")]
        getclusters <- merge(getclusters,templeases,by = c("ID"),all.x = T)
        
    #get approved flag and other data fields
        getclusters <- merge(getclusters,leases[,c("ID","Approved","Vintage","VintageFunded","FundedDate","CreationDate","ApprovalDate","FirstDueDate")],
                             by = c("ID"),all.x = T)
    
    
#create dictionary of what model to use for each tier ... projectID and modelID----
    projectDictionary <- data.frame(Range=character(),
                                    ProjectID=character(), 
                                    ModelID=character(), 
                                    stringsAsFactors=FALSE) 
        #0%
            tempdict <- projectDictionary[1,]
            tempdict$Range <- "0%"
            tempdict$ProjectID <- "5915f1bdc808916f0a1f9872"
            tempdict$ModelID <- "5916108dd96494320379fa73" #"59161955f8d3651d361f19a0"
            projectDictionary <- rbind(projectDictionary,tempdict)
        
        #4% 
            tempdict <- projectDictionary[1,]
            tempdict$Range <- "4%"
            tempdict$ProjectID <- "59135d9ac8089126fd846599"
            tempdict$ModelID <- "5914afbb8bb53e356c3a441d" #"5914c8f5733d590ebe7c13e1"
            projectDictionary <- rbind(projectDictionary,tempdict)
        
        #6% 
            tempdict <- projectDictionary[1,]
            tempdict$Range <- "6%"
            tempdict$ProjectID <- "59135fa3c80891367d538f3d"
            tempdict$ModelID <- "591492d6f31a440ee0607548" #"5914ab8812d9d57105b300d8"
            projectDictionary <- rbind(projectDictionary,tempdict)
        
        #8% 
            tempdict <- projectDictionary[1,]
            tempdict$Range <- "8%"
            tempdict$ProjectID <- "59135f70c80891367d538f33"
            tempdict$ModelID <- "59147bf68bb53e15e13a4418" #"59148145f8d36503cf1f199d"
            projectDictionary <- rbind(projectDictionary,tempdict)
        
        #10% 
            tempdict <- projectDictionary[1,]
            tempdict$Range <- "10%"
            tempdict$ProjectID <- "59135eb0c8089159a0b7cc8f"
            tempdict$ModelID <- "5913dd4ed96494534279fa8f" #"5913e3b4f8d3654a5e1f199b"
            projectDictionary <- rbind(projectDictionary,tempdict)
        
        #12% 
            tempdict <- projectDictionary[1,]
            tempdict$Range <- "12%"
            tempdict$ProjectID <- "59135ed1c8089159a0b7cc92"
            tempdict$ModelID <- "5913a61e733d5920dc7c13e1" #"5913c3aabbf0cd77d36a1f5c"
            projectDictionary <- rbind(projectDictionary,tempdict)   
    
    
    
    
    
#score the records----
    for(thePercentRange in projectDictionary$Range){
        # thePercentRange <- projectDictionary$Range[1]
        tempprojectDictionary <- projectDictionary[projectDictionary$Range == thePercentRange,]
        print(thePercentRange)
        
        #get clusters in that range
            minRange <- as.numeric(gsub("%","",thePercentRange)) / 100
            if(minRange == 0){
                maxRange <- 0.04
                tempClusters <- clusterPerformance$Cluster[clusterPerformance$NoPay_45Perc >= minRange & clusterPerformance$NoPay_45Perc < maxRange]
            }else{
                maxRange <- minRange + .02
                tempClusters <- clusterPerformance$Cluster[clusterPerformance$NoPay_45Perc >= minRange & clusterPerformance$NoPay_45Perc < maxRange]
            }
            if(minRange == .12){
                maxRange <- max(clusterPerformance$NoPay_45Perc,na.rm = T)
                tempClusters <- clusterPerformance$Cluster[clusterPerformance$NoPay_45Perc >= minRange & clusterPerformance$NoPay_45Perc <= maxRange]
            }
            
        #get records to be scored
            tempclusterDF <- finalNormalizedXML[finalNormalizedXML$Cluster %in% tempClusters,]
            theColNames <- names(tempclusterDF)
            theColNames <- gsub("\\.","_",theColNames)
            colnames(tempclusterDF) <- theColNames
            # check$NoPay_45Perc[check$Cluster == 112]
            sum(finalNormalizedXML$NoPay_45[finalNormalizedXML$Cluster %in% tempClusters]) / 
                length(finalNormalizedXML$NoPay_45[finalNormalizedXML$Cluster %in% tempClusters])
            # table(clusterDF$NoPay_45[clusterDF$Cluster == 112])
            
        
        #get needed features
            theprojectID <- tempprojectDictionary$ProjectID
            themodelID <- tempprojectDictionary$ModelID
            allmodels <- data.frame(GetAllModels(project = theprojectID),stringsAsFactors = F)
            themodel <- allmodels[allmodels$modelId == themodelID,]
            thefeaturelistid <- themodel$featurelistId
            featurelistinfo <- GetFeaturelist(project = theprojectID,featurelistId = thefeaturelistid)
            thefeaturenames <- featurelistinfo$features
            
        #score and write file
            if(length(thefeaturenames[thefeaturenames %in% names(tempclusterDF) == F]) == 0){
                #upload dataset and get probabilities
                    theUploadPredictionDataset <- UploadPredictionDataset(project = theprojectID,
                                                                          dataSource = tempclusterDF[,c(thefeaturenames)])
                    theRequestPredictionsForDataset <- RequestPredictionsForDataset(project = theprojectID,modelId = themodelID,
                                                                                    datasetId = theUploadPredictionDataset$id)
                    theGetPredictions <- GetPredictions(project = theprojectID,predictJobId = theRequestPredictionsForDataset,type = "probability",maxWait = 60*60*24)
                    theGetPredictions <- data.frame(theGetPredictions,stringsAsFactors = F)
                    colnames(theGetPredictions)[1] <- "NoPay_45_Probability"
                
                #merge
                    theGetPredictions$id <- seq.int(nrow(theGetPredictions))
                    tempclusterDF$id <- seq.int(nrow(tempclusterDF))
                    tempclusterDF <- merge(tempclusterDF,theGetPredictions,by = c("id"),all.x = T)
                    # table(is.na(check$ChargeOffProbability))
                
                #get file ready for excel
                    writefile <- merge(tempclusterDF[,c("ID","NoPay_45_Probability","NoPay_45")],
                                       leases[,c("ID","Vintage","VintageFunded","FundedPastFirstDueDate","Funded","FundedAmount",
                                                 "CrystalFPDAMT","Approved")],by = c("ID"),all.x = T)
                
                writefile <- merge(writefile,templeases[,c("ID","Days")],by = c("ID"),all.x = T)
                writefile$Eligible_45[writefile$Days >= 45 & !is.na(writefile$Days)] <- 1 
                writefile <- writefile[order(writefile$NoPay_45_Probability,decreasing = F),]
                setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
                fileTitle <- paste0("Fraud_ConsumerFraudScore_45DayNoPay_EB_DX_V2_IdentifyCutoffs_ClusterRange_100Attributes",gsub("%","",thePercentRange),"to",
                                    as.character(maxRange*100),"Percent",".csv")
                write.csv(writefile,file = fileTitle,quote = T,row.names = F)
                
                # 0.041 <= NoPay_45Probability < 0.455  ... cut at top end, low end is < 2% NoPay
                
                #send email
                    library(gmailr)
                    #get email function
                        setwd("E:/Analytics/RScripts/DataProviderAnalysis")
                        source("SendInformationalEmail_Function.R",echo = T)
                        setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")
                    
                    #format email and send
                        emailAddress <- "dhinkson@crestfinancial.com"
                        emailBody <- ""
                        # emailBody <- paste0("<body>",emailBody,"</body>")
                        emailDescription <- paste0(fileTitle," Written")
                        sendInformationalEmail(emailDescription,emailBody,emailAddress)
            }else{
                print("Issue with the columns")
                
                library(gmailr)
                #get email function
                    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
                    source("SendInformationalEmail_Function.R",echo = T)
                    setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")
                
                #format email and send
                    emailAddress <- "dhinkson@crestfinancial.com"
                    emailBody <- ""
                    # emailBody <- paste0("<body>",emailBody,"</body>")
                    emailDescription <- paste0(fileTitle," **** ISSUE WITH COLNAMES")
                    sendInformationalEmail(emailDescription,emailBody,emailAddress)
                break
            }
            
            
    }
    
    
    
    
    
    
    
    
    
    


























