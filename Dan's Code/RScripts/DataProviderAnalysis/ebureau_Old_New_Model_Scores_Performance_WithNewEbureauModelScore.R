#evaluate the new score eBureau sent us

#get docIDs
    setwd("F:/CSVFiles/DataProviderAnalysis/DFsFromR")
    docIDs <- data.frame(fread("lpV2LeasesDocumentIDs.csv", stringsAsFactors = F))
    setwd("F:/CSVFiles/DataProviderAnalysis")

#get the ebureau info
    eb <- data.frame(fread("ebureau_Old_New_Model_Scores.csv", stringsAsFactors = F))
    colnames(eb)[1] <- "DocumentID"
    eb$RecordID <- ave(eb$ApplicantID, FUN = seq_along) #puts an id on the df

#need to get the everything horizontal to look at 
    ebFinal <- unique(eb[,c("DocumentID","ApplicantID","ProviderID","GenerationDate")])
    ebFinal$Vintage <- as.Date(ebFinal$GenerationDate)
    min(ebFinal$Vintage)
    day(ebFinal$Vintage) <- 1
    # summary(ebFinal$Vintage)
    
#bring in bav codes
    bav <- read.csv("BAVCodes.csv", stringsAsFactors = F,quote = '"', row.names = NULL)
    colnames(bav)[1] <- "DocumentID"
    bav$GenerationDate <- fastPOSIXct(bav$GenerationDate)
    bav <- bav[bav$GenerationDate >= "2016-01-01",]
    bavDocIDs <- unique(bav$DocumentID)
    colnames(bav)[6] <- "BAVCode"
    bav$ProviderID <- NULL
    bav$VariableID <- NULL
    bav$ApplicantID <- NULL
    bav$GenerationDate <- NULL
    bav <- bav[bav$BAVCode %in% c("1111","3333","4444","P01","P02","P03","P04","P05","XD00"),]
    bav <- merge(bav,docIDs,by=c("DocumentID"))
    colnames(bav) <- paste0("BAV",colnames(bav))
    colnames(bav)[3] <- "ID"
    colnames(bav)[2] <- "BAVCode"
    gc()

tempEB <- eb

#get the names
    nameEB <- tempEB[tempEB$VariableID %in% c(39800,39751,45955),]
    nameEB <- nameEB[,c("DocumentID","ApplicantID","VariableID","VariableValue","RecordID")]
    nameEB$row.number <- F
    nameEB <- nameEB[order(nameEB$ApplicantID,nameEB$VariableID,decreasing = T),]
    table(nameEB$VariableValue)
    
    oldScoreName <- nameEB[,c("DocumentID","ApplicantID")]
    colnames(oldScoreName)[2] <- "OldScoreName"
    oldScoreName <- oldScoreName[0,]
    
    newScoreName <- nameEB[,c("DocumentID","ApplicantID")]
    colnames(newScoreName)[2] <- "NewScoreName"
    newScoreName <- newScoreName[0,]

    while(length(nameEB$DocumentID) > 1){
        #old score
        tempOldScoreName <- subset(nameEB,subset = !duplicated(nameEB[c("ApplicantID")]))
        table(tempOldScoreName$VariableValue)
        oldScoreNameRecordIDs <- unique(tempOldScoreName$RecordID)
        nameEB <- nameEB[nameEB$RecordID %in% oldScoreNameRecordIDs == F,]
        table(nameEB$VariableValue)
        tempOldScoreName$row.number <- NULL
        tempOldScoreName$RecordID <- NULL
        tempOldScoreName$VariableID <- NULL
        tempOldScoreName$ApplicantID <- NULL
        colnames(tempOldScoreName)[2] <- "OldScoreName"
        oldScoreNameDocIDs <- tempOldScoreName$DocumentID
        gc()
        
        #new score
        tempNewScoreName <- nameEB[nameEB$DocumentID %in% oldScoreNameDocIDs,]
        table(tempNewScoreName$VariableValue)
        tempNewScoreName$row.number <- NULL
        tempNewScoreName$RecordID <- NULL
        tempNewScoreName$VariableID <- NULL
        tempNewScoreName$ApplicantID <- NULL
        colnames(tempNewScoreName)[2] <- "NewScoreName"
        nameEB <- nameEB[nameEB$DocumentID %in% oldScoreNameDocIDs == F,]
        
        #bind
        newScoreName <- rbind(newScoreName,tempNewScoreName)
        oldScoreName <- rbind(oldScoreName,tempOldScoreName)
    }

    table(newScoreName$NewScoreName)
    table(oldScoreName$OldScoreName)

    newScoreNameDocIDs <- unique(newScoreName$DocumentID)
    oldScoreName <- oldScoreName[oldScoreName$DocumentID %in% newScoreNameDocIDs,]
    
    rm(tempNewScoreName,tempOldScoreName,newScoreNameDocIDs,oldScoreNameDocIDs,oldScoreNameRecordIDs,oldScoreNameVarIDs,nameEB)
    gc()

#get the scores
    scoreEB <- tempEB[tempEB$VariableID %in% c(39791,39756,45950),]
    scoreEB <- scoreEB[,c("DocumentID","ApplicantID","VariableID","VariableValue","RecordID")]
    scoreEB$row.number <- F
    scoreEB <- scoreEB[order(scoreEB$ApplicantID,scoreEB$VariableID,decreasing = T),]
    
    oldScore <- scoreEB[,c("DocumentID","ApplicantID")]
    colnames(oldScore)[2] <- "OldScore"
    oldScore <- oldScore[0,]
    
    newScore <- scoreEB[,c("DocumentID","ApplicantID")]
    colnames(newScore)[2] <- "NewScore"
    newScore <- newScore[0,]
    
    while(length(scoreEB$DocumentID) > 1){
        #old score
        tempOldScore <- subset(scoreEB,subset = !duplicated(scoreEB[c("ApplicantID")]))
        oldScoreRecordIDs <- unique(tempOldScore$RecordID)
        scoreEB <- scoreEB[scoreEB$RecordID %in% oldScoreRecordIDs == F,]
        tempOldScore$row.number <- NULL
        tempOldScore$RecordID <- NULL
        tempOldScore$VariableID <- NULL
        tempOldScore$ApplicantID <- NULL
        colnames(tempOldScore)[2] <- "OldScore"
        oldScoreDocIDs <- tempOldScore$DocumentID
        gc()
        
        #new score
        tempNewScore <- scoreEB[scoreEB$DocumentID %in% oldScoreDocIDs,]
        tempNewScore$row.number <- NULL
        tempNewScore$RecordID <- NULL
        tempNewScore$VariableID <- NULL
        tempNewScore$ApplicantID <- NULL
        colnames(tempNewScore)[2] <- "NewScore"
        scoreEB <- scoreEB[scoreEB$DocumentID %in% oldScoreDocIDs == F,]
        
        #bind
        newScore <- rbind(newScore,tempNewScore)
        oldScore <- rbind(oldScore,tempOldScore)
    }
    
    newScoreDocIDs <- unique(newScore$DocumentID)
    oldScore <- oldScore[oldScore$DocumentID %in% newScoreDocIDs,]
    rm(tempNewScore,tempOldScore,newScoreDocIDs,oldScoreDocIDs,oldScoreRecordIDs,oldScoreVarIDs,scoreEB)
    gc()

#get the asjusted scores
    adjustedScoreEB <- tempEB[tempEB$VariableID %in% c(39752,39778,45953),]
    adjustedScoreEB <- adjustedScoreEB[,c("DocumentID","ApplicantID","VariableID","VariableValue","RecordID")]
    adjustedScoreEB$row.number <- F
    adjustedScoreEB <- adjustedScoreEB[order(adjustedScoreEB$ApplicantID,adjustedScoreEB$VariableID,decreasing = T),]
    
    oldAdjustedScore <- adjustedScoreEB[,c("DocumentID","ApplicantID")]
    colnames(oldAdjustedScore)[2] <- "OldAdjustedScore"
    oldAdjustedScore <- oldAdjustedScore[0,]
    
    newAdjustedScore <- adjustedScoreEB[,c("DocumentID","ApplicantID")]
    colnames(newAdjustedScore)[2] <- "NewAdjustedScore"
    newAdjustedScore <- newAdjustedScore[0,]
    
    while(length(adjustedScoreEB$DocumentID) > 1){
        #old score
        tempOldScore <- subset(adjustedScoreEB,subset = !duplicated(adjustedScoreEB[c("ApplicantID")]))
        oldScoreRecordIDs <- unique(tempOldScore$RecordID)
        adjustedScoreEB <- adjustedScoreEB[adjustedScoreEB$RecordID %in% oldScoreRecordIDs == F,]
        tempOldScore$row.number <- NULL
        tempOldScore$RecordID <- NULL
        tempOldScore$VariableID <- NULL
        tempOldScore$ApplicantID <- NULL
        colnames(tempOldScore)[2] <- "OldAdjustedScore"
        oldScoreDocIDs <- tempOldScore$DocumentID
        gc()
        
        #new score
        tempNewScore <- adjustedScoreEB[adjustedScoreEB$DocumentID %in% oldScoreDocIDs,]
        tempNewScore$row.number <- NULL
        tempNewScore$RecordID <- NULL
        tempNewScore$VariableID <- NULL
        tempNewScore$ApplicantID <- NULL
        colnames(tempNewScore)[2] <- "NewAdjustedScore"
        adjustedScoreEB <- adjustedScoreEB[adjustedScoreEB$DocumentID %in% oldScoreDocIDs == F,]
        
        #bind
        newAdjustedScore <- rbind(newAdjustedScore,tempNewScore)
        oldAdjustedScore <- rbind(oldAdjustedScore,tempOldScore)
    }
    
    newAdjustedScoreDocIDs <- unique(newAdjustedScore$DocumentID)
    oldAdjustedScore <- oldAdjustedScore[oldAdjustedScore$DocumentID %in% newAdjustedScoreDocIDs,]
    
    ebFinal <- merge(ebFinal,oldScoreName,by = c("DocumentID"))
    ebFinal <- merge(ebFinal,oldScore,by = c("DocumentID"))
    ebFinal <- merge(ebFinal,oldAdjustedScore,by = c("DocumentID"))
    ebFinal <- merge(ebFinal,newScoreName,by = c("DocumentID"))
    ebFinal <- merge(ebFinal,newScore,by = c("DocumentID"))
    ebFinal <- merge(ebFinal,newAdjustedScore,by = c("DocumentID"))
    
    rm(adjustedScoreEB,eb,ebFinalCopy,newAdjustedScore,newScore,newScoreName,oldAdjustedScore,oldScore,oldScoreName,tempEB,tempNewScore,tempOldScore)
    gc()

#put old and new in 20 point bands with fpd
    ebFinal <- merge(ebFinal,docIDs,by = c("DocumentID","ProviderID"))
    ebFinal <- merge(ebFinal,bav,by=c("ID"),all.x = T)
    ebFinal$NewScore <- as.numeric(ebFinal$NewScore)
    ebFinal$OldScore <- as.numeric(ebFinal$OldScore)
    ebFinal$RiskBand <- as.numeric(ebFinal$OldScore)#floor(ebFinal$OldScore / 20)) * 20

    ebFinal$GenerationDate <- as.POSIXct(ebFinal$GenerationDate)
    ebFinal$row.number <- F
    ebFinal <- ebFinal[order(ebFinal$ID,ebFinal$GenerationDate,decreasing = T),]
    ebFinal <- subset(ebFinal, subset = !duplicated(ebFinal$ID))
    ebFinal$row.number <- NULL
    # table(ebFinal$BAVCode)
    ebFinal$BAVCode[is.na(ebFinal$BAVCode)] <- "0"

    leases <- lpV2Leases[lpV2Leases$ID %in% ebFinal$ID,] 
    dueDate <- max(leases$CreationDate)
    day(dueDate) <- day(dueDate) - 7

    #old
        #total
            totalLeases <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= ebFinal, FUN = "length")
            colnames(totalLeases)[4] <- "TotalOld"
        #approved
            approvedLeasesIDs <- leases$ID[!is.na(leases$ApprovalDate) & !is.na(leases$AutoApprovedFlag) & leases$AutoApprovedFlag == 1]
            approvedLeases <- ebFinal[ebFinal$ID %in% approvedLeasesIDs,]
            approvedLeasesAgg <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= approvedLeases, FUN = "length")
            colnames(approvedLeasesAgg)[4] <- "ApprovedOld"
            totalLeases <- merge(totalLeases,approvedLeasesAgg,by = c("RiskBand","Vintage","BAVCode"),all = T)
        #funded
            fundedLeasesIDs <- leases$ID[!is.na(leases$FundedDate) &
                                             leases$Status %in% c("Approved","DealerReturn","Denied","Ready For Funding") == F &
                                             !is.na(leases$FirstDueDate) & 
                                             leases$FirstDueDate <= dueDate]
            fundedLeases <- ebFinal[ebFinal$ID %in% fundedLeasesIDs,]
            fundedLeasesAgg <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= fundedLeases, FUN = "length")
            colnames(fundedLeasesAgg)[4] <- "FundedOld"
            totalLeases <- merge(totalLeases,fundedLeasesAgg,by = c("RiskBand","Vintage","BAVCode"),all = T)
        #fpd
            fpdLeasesIDs <- leases$ID[!is.na(leases$FundedDate) &
                                          leases$Status %in% c("Approved","DealerReturn","Denied","Ready For Funding") == F &
                                          !is.na(leases$FirstDueDate) & 
                                          leases$FirstDueDate <= dueDate &
                                          !is.na(leases$FirstPaymentDefault) &
                                          leases$FirstPaymentDefault == 1]
            fpdLeases <- ebFinal[ebFinal$ID %in% fpdLeasesIDs,]
            fpdLeasesAgg <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= fpdLeases, FUN = "length")
            colnames(fpdLeasesAgg)[4] <- "FPDOld"
            totalLeases <- merge(totalLeases,fpdLeasesAgg,by = c("RiskBand","Vintage","BAVCode"),all = T)

#new
    #new risk band
        ebFinal$RiskBand <- ebFinal$NewScore
        #total
            totalLeasesNew <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= ebFinal, FUN = "length")
            colnames(totalLeasesNew)[4] <- "TotalNew"
            totalLeases <- merge(totalLeases,totalLeasesNew,by = c("RiskBand","Vintage","BAVCode"),all = T)
        #approved
            approvedLeasesIDs <- leases$ID[!is.na(leases$ApprovalDate) & !is.na(leases$AutoApprovedFlag) & leases$AutoApprovedFlag == 1]
            approvedLeases <- ebFinal[ebFinal$ID %in% approvedLeasesIDs,]
            approvedLeasesAgg <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= approvedLeases, FUN = "length")
            colnames(approvedLeasesAgg)[4] <- "ApprovedNew"
            totalLeases <- merge(totalLeases,approvedLeasesAgg,by = c("RiskBand","Vintage","BAVCode"),all = T)
        #funded
            fundedLeasesIDs <- leases$ID[!is.na(leases$FundedDate) &
                                             leases$Status %in% c("Approved","DealerReturn","Denied","Ready For Funding") == F &
                                             !is.na(leases$FirstDueDate) & 
                                             leases$FirstDueDate <= dueDate]
            fundedLeases <- ebFinal[ebFinal$ID %in% fundedLeasesIDs,]
            fundedLeasesAgg <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= fundedLeases, FUN = "length")
            colnames(fundedLeasesAgg)[4] <- "FundedNew"
            totalLeases <- merge(totalLeases,fundedLeasesAgg,by = c("RiskBand","Vintage","BAVCode"),all = T)
        #fpd
            fpdLeasesIDs <- leases$ID[!is.na(leases$FundedDate) &
                                          leases$Status %in% c("Approved","DealerReturn","Denied","Ready For Funding") == F &
                                          !is.na(leases$FirstDueDate) & 
                                          leases$FirstDueDate <= dueDate &
                                          !is.na(leases$FirstPaymentDefault) &
                                          leases$FirstPaymentDefault == 1]
            fpdLeases <- ebFinal[ebFinal$ID %in% fpdLeasesIDs,]
            fpdLeasesAgg <- aggregate(ID ~ RiskBand+Vintage+BAVCode, data= fpdLeases, FUN = "length")
            colnames(fpdLeasesAgg)[4] <- "FPDNew"
            totalLeases <- merge(totalLeases,fpdLeasesAgg,by = c("RiskBand","Vintage","BAVCode"),all = T) 
        
        totalLeases[is.na(totalLeases)] <- 0
        
        
        
        ebFinal$Approved <- 0
        ebFinal$Approved[ebFinal$ID %in% approvedLeasesIDs] <- 1
        ebFinal$Funded <- 0
        ebFinal$Funded[ebFinal$ID %in% fundedLeasesIDs] <- 1
        ebFinal$FPD <- 0
        ebFinal$FPD[ebFinal$ID %in% fpdLeasesIDs] <- 1
        
        
        write.csv(ebFinal,file = "ebureau_Old_New_Model_Scores_1ptbands_ByVintage_WithBAVCodes_RawData.csv",quote = T,row.names = F)
        
        write.csv(totalLeases,file = "ebureau_Old_New_Model_Scores_1ptbands_ByVintage_WithBAVCodes.csv",quote = T,row.names = F)
        
        
        min(ebFinal$GenerationDate)




summary(april$GenerationDate)
april <- ebFinal[ebFinal$GenerationDate >= "2016-04-01" & ebFinal$GenerationDate < "2016-05-01" & ebFinal$BAVCode %in% c("0") == F,]
table(april$BAVCode)
length(lpV2Leases$ID[lpV2Leases$CreationDate >= "2016-04-01" & lpV2Leases$CreationDate < "2016-05-01"]) / length(april$ID)

length(april$ID) / length(lpV2Leases$ID[lpV2Leases$CreationDate >= "2016-04-01" & lpV2Leases$CreationDate < "2016-05-01"])
(length(april$ID) - length(april$ID[april$BAVCode == "3333"])) / length(lpV2Leases$ID[lpV2Leases$CreationDate >= "2016-04-01" & lpV2Leases$CreationDate < "2016-05-01"])
(length(april$ID) - length(april$ID[april$BAVCode == "XD00" & april$NewScore < 200])) / length(lpV2Leases$ID[lpV2Leases$CreationDate >= "2016-04-01" & lpV2Leases$CreationDate < "2016-05-01"])



(sum(totalLeases$ApprovedNew)+sum(totalLeases$ApprovedNew[totalLeases$BAVCode == "XD00"]))/sum(totalLeases$TotalNew)
(sum(totalLeases$TotalNew[totalLeases$BAVCode %in% c("1111","3333","4444","P01","P02","P03","P04","P05")]))/sum(totalLeases$TotalNew)
(sum(totalLeases$TotalNew[totalLeases$BAVCode %in% c("1111","3333","4444","P01","P02","P03","P04","P05")])+sum(totalLeases$ApprovedNew[totalLeases$BAVCode == "XD00"]) - sum(totalLeases$TotalNew[totalLeases$RiskBand <=114 & totalLeases$BAVCode == "3333"]))/sum(totalLeases$TotalNew)
(sum(totalLeases$TotalNew[totalLeases$BAVCode %in% c("1111","3333","4444","P01","P02","P03","P04","P05")])+sum(totalLeases$ApprovedNew[totalLeases$BAVCode == "XD00"]) - sum(totalLeases$TotalNew[totalLeases$RiskBand <=114 & totalLeases$BAVCode == "3333"]) - sum(totalLeases$TotalNew[totalLeases$RiskBand <=24 & totalLeases$BAVCode == "1111"]))/sum(totalLeases$TotalNew)











#get auto approved flag
#         aaf <- lpV2Leases[,c("ID","AutoApprovedFlag")]
#         aaf <- aaf[!is.na(aaf$AutoApprovedFlag),]
#         ebFinal <- merge(ebFinal,aaf,by = c("ID"),all.x = T)

#get fpd
leases <- lpV2Leases
leases <- leases[!is.na(leases$FundedDate),]  
leases <- leases[leases$ID %in% ebFinal$ID,]
leases <- leases[year(leases$CreationDate) >= 2016,]
gc()
table(leases$Status)
leases <- leases[leases$Status %in% c("DealerReturn","Denied","Ready For Funding") == F,]
dueDate <- max(leases$CreationDate)
day(dueDate) <- day(dueDate) - 7
leases <- leases[!is.na(leases$FirstDueDate) & leases$FirstDueDate <= dueDate,]
table(leases$FirstPaymentDefault)

fpd <- leases[,c("ID","FirstPaymentDefault")]
ebFinal <- merge(ebFinal,fpd,by = c("ID"))#,all.x = T)

#evaluate fpd
total <- ebFinal[!is.na(ebFinal$FirstPaymentDefault),]
totalAgg <- aggregate(ID ~ RiskBand,data = total, FUN = "length")
colnames(totalAgg)[2] <- "Total"

fpd <- total[total$FirstPaymentDefault == 1,]
fpdAgg <- aggregate(ID ~ RiskBand,data = fpd, FUN = "length")
colnames(fpdAgg)[2] <- "FPD"

totalAgg <- merge(totalAgg,fpdAgg,by = c("RiskBand"),all = T)
totalAgg$FPDPercent <- totalAgg$FPD / totalAgg$Total * 100

total$RiskBand <- as.numeric(total$NewScore)#floor(total$NewScore / 20)) * 20
totalNewAgg <- aggregate(ID ~ RiskBand,data = total, FUN = "length")
colnames(totalNewAgg)[2] <- "TotalNew"

fpd$RiskBand <- as.numeric(fpd$NewScore)#floor(fpd$NewScore / 20)) * 20
fpdNewAgg <- aggregate(ID ~ RiskBand,data = fpd, FUN = "length")
colnames(fpdNewAgg)[2] <- "FPDNew"

totalAgg[is.na(totalAgg)] <- 0

# totalAgg <- merge(totalAgg,totalNewAgg,by = c("NewRiskBand"),all.x = T)
totalAgg <- merge(totalAgg,totalNewAgg,by = c("RiskBand"),all = T)
totalAgg <- merge(totalAgg,fpdNewAgg,by = c("RiskBand"),all = T)
totalAgg$FPDNewPercent <- totalAgg$FPDNew / totalAgg$TotalNew * 100
totalAgg$Diff <- totalAgg$FPDPercent - totalAgg$FPDNewPercent
totalAgg[is.na(totalAgg)] <- 0

write.csv(totalAgg,file = "ebureau_Old_New_Model_Scores_1ptbands.csv",quote = T,row.names = F)

summary(totalAgg$Diff)
sum(totalAgg$Diff[totalAgg$Diff >= 0],na.rm = T)
sum(totalAgg$Diff[totalAgg$Diff < 0],na.rm = T)

length(total$ID[total$NewScore < 35]) / length(total$ID)
length(ebFinal$ID[ebFinal$NewScore < 35]) / length(ebFinal$ID)

total <- length(ebFinal$DocumentID)
below35 <- length(ebFinal$DocumentID[ebFinal$OldScore < 35])
newbelow35 <- length(ebFinal$DocumentID[ebFinal$NewScore < 34])






