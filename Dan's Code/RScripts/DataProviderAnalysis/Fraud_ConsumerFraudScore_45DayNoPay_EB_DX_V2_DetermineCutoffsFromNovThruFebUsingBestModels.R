library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)
library(stringr)
library(flexclust)
library(Hmisc)
library(datarobot)


#best models for Fraud Score Analysis

    #W/O Clusters
        # https://app.datarobot.com/projects/5907ade8c808917160b85b28/models/590a0fccaf927c378a934d93
        # try https://app.datarobot.com/projects/591b878dc808916fabaabdf1/models/591c8cb562875624798fd823

#Score records for each model and identify what the cutoffs are based on the training set of data for both models
    ConnectToDataRobot(endpoint = "https://app.datarobot.com/api/v2",token = "WBLJi2I9IXpCbiJD2qc6_BDETB8PHITf")

#get file
    #get leases
        if(exists("leases")){
            print("Leases already in RAM")
        }else{
            setwd("E:/Analytics/RScripts/DataProviderAnalysis")
            source("Leases_OpenMostRecent.R",echo = T)
        }

    #read in scaled files
        #get the scaled files #nov thru feb
            setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/FormatXML_Cluster_Scaled")
            formattedFiles <- list.files()
            formattedFiles <- formattedFiles[grepl("FormatXML",formattedFiles)]
            startdate <- as.Date("2016-11-01")
            enddate <- as.Date("2017-02-01")
            theDates <- seq.Date(startdate,enddate,by = c("months"))
            theDates <- theDates[order(theDates,decreasing = T)]
            theDates <- as.character(theDates)
            for(date in theDates){
                # date <- theDates[1]
                # date <- theDates[2]
                print(date)
                theFileTitle <- paste0("FormatXML_UniqueApplicantsPerMonth_DX_EB_Cluster_Scaled_",month.abb[month(date)],"_",year(date),".csv")
                if(theFileTitle %in% formattedFiles){
                    print("Exists")
                }else{
                    next
                }
                tempfinalNormalizedXML <- data.frame(fread(theFileTitle, stringsAsFactors = F))
                tempfinalNormalizedXML$CreatedDate_Vintage <- date
                if(exists("finalNormalizedXML")){
                    tempfinalNormalizedXML$ApplicantID <- as.numeric(tempfinalNormalizedXML$ApplicantID)
                    tempfinalNormalizedXML_MissingColumns <- names(finalNormalizedXML)[names(finalNormalizedXML) %in% names(tempfinalNormalizedXML) == F]
                    tempfinalNormalizedXML[,tempfinalNormalizedXML_MissingColumns] <- ""
                    finalNormalizedXML_MissingColumns <- names(tempfinalNormalizedXML)[names(tempfinalNormalizedXML) %in% names(finalNormalizedXML) == F]
                    finalNormalizedXML[,finalNormalizedXML_MissingColumns] <- ""

                    #this keeps appIDs unique in finalNormalizedXML
                    tempfinalNormalizedXMLAppIDs <- tempfinalNormalizedXML$ApplicantID
                    finalNormalizedXML <- finalNormalizedXML[finalNormalizedXML$ApplicantID %in% tempfinalNormalizedXMLAppIDs == F,]

                    finalNormalizedXML <- rbind(finalNormalizedXML,tempfinalNormalizedXML)
                }else{
                    finalNormalizedXML <- tempfinalNormalizedXML
                    finalNormalizedXML$ApplicantID <- as.numeric(finalNormalizedXML$ApplicantID)
                }
            }

            #there will be some NA's ... b/c it has already been scaled, just using the mean value  of values present to impute
                for(thecolname in names(finalNormalizedXML)[names(finalNormalizedXML) %in% c("ApplicantID","ID","RetailerID") == F]){
                    # thecolname <- names(finalNormalizedXML)[names(finalNormalizedXML) %in% c("ApplicantID","ID","RetailerID") == F][1]
                    # thecolname <- "XPath179_VarValue_AE"
                    print(thecolname)
                    tempfinalNormalizedXML <- finalNormalizedXML[,c("ID",thecolname)]
                    if(length(tempfinalNormalizedXML$ID[is.na(tempfinalNormalizedXML[,thecolname])]) > 0){
                        if(length(tempfinalNormalizedXML$ID[is.na(tempfinalNormalizedXML[,thecolname])]) == length(tempfinalNormalizedXML$ID)){
                            finalNormalizedXML[,thecolname] <- NULL
                            next
                        }
                        print("NA's!!!!!!")
                        # break
                        themean <- mean(tempfinalNormalizedXML[,thecolname],na.rm = T)
                        finalNormalizedXML[,thecolname][is.na(finalNormalizedXML[,thecolname])] <- themean
                    }else{
                        print("No NAs Present")
                    }
                }


    #get leases created 45 days out from first due date
        templeases <- leases[!is.na(leases$FundedPastFirstDueDate) & leases$FundedPastFirstDueDate == 1 & 
                                 leases$CreationDate >= "2016-11-01",]
        templeases$Days <- as.numeric(difftime(Sys.Date(),templeases$FirstDueDate, units = c("days")))
        summary(templeases$Days)
        templeases <- templeases[templeases$Days >= 45,]
        templeases$row.number <- F
        templeases <- templeases[order(templeases$ApplicantID,templeases$ID,decreasing = F),]
        templeases <- subset(templeases,subset = !duplicated(templeases[c("ApplicantID")]))

    #set 45/60/90 dnp flags
        templeases$NoPay_45 <- 0
        templeases$NoPay_45[templeases$DaysToPayment >= 45] <- 1
        table(templeases$NoPay_45)

    #subset the shredded data for qualifying funded leases
        finalNormalizedXML$id <- NULL
        finalNormalizedXML$row.number <- F
        finalNormalizedXML <- finalNormalizedXML[order(finalNormalizedXML$ApplicantID,finalNormalizedXML$ID,decreasing = T),]
        finalNormalizedXML <- subset(finalNormalizedXML,subset = !duplicated(finalNormalizedXML[c("ApplicantID")]))
        finalNormalizedXML <- finalNormalizedXML[finalNormalizedXML$ID %in% templeases$ID,]

    #merge
        finalNormalizedXML <- merge(templeases[,c("ID","ApplicantID","NoPay_45")],finalNormalizedXML,by = c("ApplicantID","ID"))
        theColNames <- names(finalNormalizedXML)
        theColNames <- gsub("\\.","_",theColNames)
        colnames(finalNormalizedXML) <- theColNames
        
        
#score the records
    #get needed features
        theprojectID <- "591b878dc808916fabaabdf1"
        themodelID <- "591c8cb562875624798fd823"
        allmodels <- data.frame(GetAllModels(project = theprojectID),stringsAsFactors = F)
        themodel <- allmodels[allmodels$modelId == themodelID,]
        thefeaturelistid <- themodel$featurelistId
        featurelistinfo <- GetFeaturelist(project = theprojectID,featurelistId = thefeaturelistid)
        thefeaturenames <- featurelistinfo$features
        
        if(length(thefeaturenames[thefeaturenames %in% names(finalNormalizedXML) == F]) == 0){
            #upload dataset and get probabilities
                theUploadPredictionDataset <- UploadPredictionDataset(project = theprojectID,
                                                                      dataSource = finalNormalizedXML[,c(thefeaturenames)])
                theRequestPredictionsForDataset <- RequestPredictionsForDataset(project = theprojectID,modelId = themodelID,
                                                                                datasetId = theUploadPredictionDataset$id)
                theGetPredictions <- GetPredictions(project = theprojectID,predictJobId = theRequestPredictionsForDataset,type = "probability")
                theGetPredictions <- data.frame(theGetPredictions,stringsAsFactors = F)
                colnames(theGetPredictions)[1] <- "NoPay_45_Probability"
                
            #merge
                theGetPredictions$id <- seq.int(nrow(theGetPredictions))
                finalNormalizedXML$id <- seq.int(nrow(finalNormalizedXML))
                finalNormalizedXML <- merge(finalNormalizedXML,theGetPredictions,by = c("id"),all.x = T)
                # table(is.na(check$ChargeOffProbability))
                
            #get file ready for excel
                writefile <- merge(finalNormalizedXML[,c("ID","NoPay_45_Probability","NoPay_45")],
                                                      leases[,c("ID","Vintage","VintageFunded","FundedPastFirstDueDate","Funded","FundedAmount",
                                                                "CrystalFPDAMT","Approved")],by = c("ID"),all.x = T)
                
                writefile <- merge(writefile,templeases[,c("ID","Days")],by = c("ID"),all.x = T)
                writefile$Eligible_45[writefile$Days >= 45 & !is.na(writefile$Days)] <- 1 
                writefile <- writefile[order(writefile$NoPay_45_Probability,decreasing = F),]
                setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
                write.csv(writefile,file = "Fraud_ConsumerFraudScore_IdentifyCutoffs_v3.csv",quote = T,row.names = F)
                
                # 0.041 <= NoPay_45Probability < 0.455  ... cut at top end, low end is < 2% NoPay
        }
        
        
        
        
        
       
        
        
        
        
        
        
        
        