#function to assign NoPay_countOfDays flag

assignNoPay_countOfDaysFlag <- function(setOfIDs,countOfDays){
    # setOfIDs <- clusterDF$ID
    # countOfDays <- 45
    if(exists("leases")){
        print("leases already in RAM")
    }else{
        setwd("E:/Analytics/RScripts/DataProviderAnalysis")
        source("Leases_OpenMostRecent.R",echo = T)
    }
    
    assignNoPay_countOfDaysFlagleases <- leases[!is.na(leases$FundedPastFirstDueDate) & leases$FundedPastFirstDueDate == 1,]
    assignNoPay_countOfDaysFlagleases <- assignNoPay_countOfDaysFlagleases[assignNoPay_countOfDaysFlagleases$ID %in% setOfIDs,]
    assignNoPay_countOfDaysFlagleases$Days <- as.numeric(difftime(Sys.Date(),assignNoPay_countOfDaysFlagleases$FirstDueDate, units = c("days")))
    summary(assignNoPay_countOfDaysFlagleases$Days)
    assignNoPay_countOfDaysFlagleases <- assignNoPay_countOfDaysFlagleases[assignNoPay_countOfDaysFlagleases$Days >= countOfDays,]
    assignNoPay_countOfDaysFlagleases$row.number <- F
    assignNoPay_countOfDaysFlagleases <- assignNoPay_countOfDaysFlagleases[order(assignNoPay_countOfDaysFlagleases$ApplicantID,assignNoPay_countOfDaysFlagleases$ID,decreasing = F),]
    assignNoPay_countOfDaysFlagleases <- subset(assignNoPay_countOfDaysFlagleases,subset = !duplicated(assignNoPay_countOfDaysFlagleases[c("ApplicantID")]))
    
    #set countOfDays/60/90 dnp flags
        assignNoPay_countOfDaysFlagleases$NoPay_countOfDays <- 0
        assignNoPay_countOfDaysFlagleases$NoPay_countOfDays[assignNoPay_countOfDaysFlagleases$DaysToPayment >= countOfDays] <- 1
        assignNoPay_countOfDaysFlagleases <- assignNoPay_countOfDaysFlagleases[,c("ID","NoPay_countOfDays")]
        return(assignNoPay_countOfDaysFlagleases)
}