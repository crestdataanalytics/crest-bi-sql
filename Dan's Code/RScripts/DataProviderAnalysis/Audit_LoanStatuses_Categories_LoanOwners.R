#check if there are any new/delted of the following
    # loan statuses
    # product categories on non demo retailers tied to Crest's loan owner ids
    # loan owner id's


library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)



# create table with tables and scripts ------------------------------------

    thingsToCheck <- data.frame(Table = character(),
                                Script = character(),
                                stringsAsFactors = F)
    tempthingsToCheck <- thingsToCheck[1,]
    
    # loan statuses
        tempthingsToCheck$Table <- "LoanStatuses"
        tempthingsToCheck$Script <- "SELECT DISTINCT LoanStatuses.[Status] FROM LoanStatuses INNER JOIN Loans ON Loans.StatusID = LoanStatuses.ID WHERE Loans.LoanOwnerID IN (1,2,4,5)"
        thingsToCheck <- rbind(thingsToCheck,tempthingsToCheck)
        
    # product categories on non demo retailers tied to Crest's loan owner ids
        tempthingsToCheck$Table <- "RetailerCategories"
        tempthingsToCheck$Script <- "SELECT DISTINCT Categories.CategoryName FROM DealerCategories INNER JOIN Categories ON Categories.ID = DealerCategories.CategoryID INNER JOIN Dealers ON Dealers.ID = DealerCategories.DealerID INNER JOIN Loans ON Loans.DealerID = Dealers.ID WHERE Dealers.IsDemoAccount = 0 AND Loans.LoanOwnerID IN (1,2,4,5)"
        thingsToCheck <- rbind(thingsToCheck,tempthingsToCheck)
        
    # loan owner id's
        tempthingsToCheck$Table <- "LoanOwners"
        tempthingsToCheck$Script <- "SELECT ID AS LoanOwnerID, [Name] From LoanOwners"
        thingsToCheck <- rbind(thingsToCheck,tempthingsToCheck)


# get data, check and email ----------------------------------------------------------------
    for(i in thingsToCheck$Table){
        # i <- thingsToCheck$Table[1]
        # i <- thingsToCheck$Table[2]
        # i <- thingsToCheck$Table[3]
        cfsql02_DealerManage <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=DealerManage;trusted_connection=true')
        thetable <- sqlQuery(cfsql02_DealerManage, thingsToCheck$Script[thingsToCheck$Table == i], stringsAsFactors = F)
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit")
        odbcCloseAll()
        filenames <- list.files()
        filetitle <- paste0(i,".csv")
        if(filetitle %in% filenames){
            currenttable <- data.frame(fread(filetitle, stringsAsFactors = F))
            newtable <- thetable
            # newtable$NewInfo[grepl("A",newtable$TableName)] <- 1
            newtable$NewInfo <- 1
            newcurrenttable <- merge(currenttable,newtable,by = c(names(thetable)),all = T)
            newcurrenttable <- newcurrenttable[is.na(newcurrenttable$CurrentInfo) | is.na(newcurrenttable$NewInfo),]
            newinfo <- newcurrenttable[is.na(newcurrenttable$CurrentInfo) & !is.na(newcurrenttable$NewInfo),]
            deletedinfo <- newcurrenttable[!is.na(newcurrenttable$CurrentInfo) & is.na(newcurrenttable$NewInfo),]
            if(length(newinfo$NewInfo) > 0){
                newinfo$Action <- "New Item Added"
            }
            if(length(deletedinfo$NewInfo) > 0){
                deletedinfo$Action <- "Item Deleted"
            }
            newdeletedinfo <- rbind(newinfo,deletedinfo)
            rm(newinfo,deletedinfo)
            gc()
            newdeletedinfo$NewInfo <- NULL
            newdeletedinfo$CurrentInfo <- NULL
            
            if(length(newdeletedinfo$Action) > 0){
                setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit")
                write.csv(newdeletedinfo,file = "NewOrDeleted_Values.csv",quote = T,row.names = F)
                
                #email
                    #get email function
                    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
                    source("SendInformationalEmailWithAttachment_Function.R",echo = T)
                    setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")
                    
                    colnames(newdeletedinfo)[1] <- "Values"
                    
                    #format email and send
                    # emailAddress <- "dhinkson@crestfinancial.com"
                    ccemailAddress <- ""
                    emailAddress <- "dhinkson@crestfinancial.com;rthorpe@crestfinancial.com"
                    emailDescription <- paste0("Changes Were Made to the ",i," table values"," - ",Sys.Date())
                    
                    if(length(newdeletedinfo$Values[newdeletedinfo$Action == "New Item Added" & !is.na(newdeletedinfo$Action)]) > 0){
                        emailBody <- paste0("New values on the ",i,": ", paste0(newdeletedinfo$Values[
                            newdeletedinfo$Action == "New Item Added" & !is.na(newdeletedinfo$Action)],collapse = ", "),"\r\n")
                    }else{
                        emailBody <- paste0("New values on the ",i,": ", length(newdeletedinfo$Values[
                            newdeletedinfo$Action == "New Item Added" & !is.na(newdeletedinfo$Action)]),"\r\n")
                    }
                    
                    if(length(newdeletedinfo$Values[newdeletedinfo$Action == "Item Deleted" & !is.na(newdeletedinfo$Action)]) > 0){
                        emailBody <- paste0(emailBody,"Deleted values on the ",i,": ", paste0(newdeletedinfo$Values[
                            newdeletedinfo$Action == "Item Deleted" & !is.na(newdeletedinfo$Action)],collapse = ", "),"\r\n")
                    }else{
                        emailBody <- paste0(emailBody,"Deleted values on the ",i,": ", length(newdeletedinfo$Values[
                            newdeletedinfo$Action == "Item Deleted" & !is.na(newdeletedinfo$Action)]),"\r\n")
                    }
                    
                    
                    emailBody <- paste0(emailBody,"See the attached file below")
                    sendInformationalEmailWithAttachment(description = emailDescription,theEmailAddress = emailAddress,theCCEmailAddresses = ccemailAddress,
                                                         theBody = emailBody,
                                                         theFileDirectory = "//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit",
                                                         theFileTitle = "NewOrDeleted_Values.csv")
            }
            
            #overwrite files and delete NewOrDeleted_TablesFields.csv
                names(newtable)[names(newtable) == "NewInfo"] <- "CurrentInfo"
                setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit")
                write.csv(newtable,file = filetitle,quote = T,row.names = F)
                file.remove("NewOrDeleted_Values.csv")
                rm(newtable,currenttable,newdeletedinfo,newcurrenttable)
                gc()
            
        }else{
            #write the file
                thetable$CurrentInfo <- 1
                # write.csv(thetable[1:5,],file = filetitle,row.names = F,quote = T)
                write.csv(thetable,file = filetitle,row.names = F,quote = T)
        }
    }

    
    
    
    
    
    
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit")
    filenames <- list.files()
    
    
    
    
    
    
    
    
    
    
    
    
    
    