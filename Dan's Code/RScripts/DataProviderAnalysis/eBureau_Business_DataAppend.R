#prep retailer file for ebureau


library(lubridate)
library(data.table)
library(fasttime)
setwd("F:/CSVFiles/DataProviderAnalysis")

#ID demo retailers
        retailers                       <- data.frame(fread("Retailers.csv", stringsAsFactors = F, colClasses = c("character")))
        colnames(retailers)[1]          <- "RetailerID"
        colnames(retailers)[2]          <- "RetailerName"
        colnames(retailers)[3] <- "RetailerCreationDate"
        colnames(retailers)[9]          <- "RetailerRepID"
        retailers$RetailerRepID         <- as.numeric(retailers$RetailerRepID)
        retailers$RetailerID            <- as.numeric(retailers$RetailerID)
        retailers$RetailerCreationDate <- as.Date(retailers$RetailerCreationDate, format = "%Y-%m-%d")
        retailers$IsDemoAccount         <- as.numeric(retailers$IsDemoAccount)
        retailers$IsDeleted             <- as.numeric(retailers$IsDeleted)
        retailers$RetailerGroupsID             <- as.numeric(retailers$RetailerGroupsID)
        retailers$CancelDate            <- as.Date(retailers$CancelDate, format = "%Y-%m-%d")
        retailers$ThresholdBiggerThan1  <- as.numeric(retailers$ThresholdBiggerThan1)
        nonDemoRetailers                <- retailers[retailers$IsDemoAccount == 0,]
        demoRetailers                   <- retailers[retailers$IsDemoAccount == 1,]
        demoRetailerIDs                 <- demoRetailers$RetailerID


#need to get business bank account on here and phone number
        
        
ebr <- retailers[retailers$IsDemoAccount == 0,]

ebrFinal <- ebr[,c("RetailerID","RetailerName","StreetLine1","StreetLine2","City","StateID","PostalCode")]

#get retailer phone and possibly bank info 
        setwd("C:/Users/dhinkson/Downloads")
        retailerPhoneNumbers <- data.frame(fread("DealerPhoneNumbers.csv", stringsAsFactors = F))
        colnames(retailerPhoneNumbers)[1] <- "RetailerPhoneNumberID"
        colnames(retailerPhoneNumbers)[2] <- "RetailerID"
        
        retailerPhoneNumbers <- retailerPhoneNumbers[order(retailerPhoneNumbers$RetailerPhoneNumberID,decreasing = F),]
        retailerPhoneNumbers$row.number <- F
        retailerPhoneNumbers <- subset(retailerPhoneNumbers, subset = !duplicated(retailerPhoneNumbers[c("RetailerID")]))
        retailerPhoneNumbers <- retailerPhoneNumbers[,c("RetailerID","PhoneNumber")]
        
        setwd("F:/CSVFiles/DataProviderAnalysis")
        ebrFinal <- merge(ebrFinal,retailerPhoneNumbers, by = c("RetailerID"), all.x = T)


#get FPD by vintage for every retailer
        leases <- dfV3Leases[!is.na(dfV3Leases$FundedDate),]
        leases <- leases[leases$Status %in% c("Approved","Cancelled","DealerReturn","Denied","Exchange","Migrated","Returned") == F,]
        table(leases$Status)
        
        leases$Vintage <- as.Date(leases$FundedDate)
        day(leases$Vintage) <- 1
        leases$RetailerID <- as.numeric(leases$RetailerID)
        
        leasesTotal <- aggregate(ID ~ Vintage + RetailerID, data = leases, FUN = c("length"))
        colnames(leasesTotal)[3] <- "TotalLeases"
        
        leasesFPD <- leases[!is.na(leases$FirstPaymentDefault) & leases$FirstPaymentDefault == 1,]
        leasesFPDAgg <- aggregate(ID ~ Vintage + RetailerID, data = leasesFPD, FUN = "length")
        colnames(leasesFPDAgg)[3] <- "FPD"
        
        leasesTotal <- merge(leasesTotal,leasesFPDAgg, by = c("Vintage","RetailerID"), all.x = T)
        leasesTotal$FPDPercent <- leasesTotal$FPD / leasesTotal$TotalLeases * 100
        #leasesTotal[is.na(leasesTotal)] <- 0
        table(is.na(leasesTotal$TotalLeases))
        table(is.na(leasesTotal$FPD))
        
        FPDPercent <- leasesTotal[,c("Vintage","RetailerID","FPDPercent")]
        FPDPercent <- FPDPercent[FPDPercent$Vintage >= "2014-01-01",]
        
        length(unique(FPDPercent$RetailerID))
        
#merge the fpd metrics to the retailer file
        ebrFinal <- ebrFinal[ebrFinal$RetailerID %in% FPDPercent$RetailerID,]
        FPDPercent <- merge(FPDPercent,ebrFinal, by = c("RetailerID"),all.x = T)
        FPDPercent <- FPDPercent[,c("Vintage","RetailerID","FPDPercent","RetailerName","StreetLine1","StreetLine2","City","StateID","PostalCode","PhoneNumber")]
        FPDPercent <- FPDPercent[order(FPDPercent$Vintage,FPDPercent$RetailerID, decreasing = F),]
        
        length(unique(FPDPercent$RetailerID))
        
        FPDPercent <- data.frame(lapply(FPDPercent, as.character), stringsAsFactors = F)
        FPDPercent[FPDPercent == "NA"] <- ""
        FPDPercent[FPDPercent == "NULL"] <- ""
        FPDPercent[is.na(FPDPercent)] <- ""
        
        setwd("C:/Users/dhinkson/Downloads")
        write.csv(FPDPercent, file = "ebureau_RetailerAppend_2_26_2016.csv", quote = T, row.names = F)
        
        test <- data.frame(fread("ebureau_RetailerAppend_2_26_2016.csv", stringsAsFactors = F, colClasses = c("character")))
        length(unique(test$RetailerID))
        min(test$Vintage)
        
rm(demoRetailers,ebr,ebrFinal,er,FPDPercent,leases,leasesFPD,leasesFPDAgg,leasesTotal,nonDemoRetailers,retailerPhoneNumbers)
gc()

test <- unique(FPDPercent[,c("RetailerID","PhoneNumber")])
table(is.na(test$PhoneNumber))
test <- test[is.na(test$PhoneNumber),]
tempRetailers <- tempRetailers[tempRetailers$RetailerID %in% test$RetailerID,]

summary(tempRetailers$RetailerCreationDate)
        
        