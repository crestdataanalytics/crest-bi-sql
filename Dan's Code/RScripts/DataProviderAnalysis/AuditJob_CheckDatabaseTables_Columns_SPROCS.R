#build script to check if there are new/dropped tables/fields on the different Databases

library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)


# pull data ------------------------------------------------------------
    
    cfsql02_DealerManage <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=DealerManage;trusted_connection=true')
    cfsql02_CrestWarehouse <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestWarehouse;trusted_connection=true')
    script <- "SELECT  T.NAME AS TableName, C.NAME AS ColumnName, P.NAME AS DataType, P.MAX_LENGTH AS [Size], T.type_desc AS Type FROM SYS.OBJECTS AS T JOIN SYS.COLUMNS AS C ON T.OBJECT_ID=C.OBJECT_ID JOIN SYS.TYPES AS P ON C.SYSTEM_TYPE_ID=P.SYSTEM_TYPE_ID WHERE  T.TYPE_DESC in ('USER_TABLE','VIEW');"
    dealerManageInfo <- sqlQuery(cfsql02_DealerManage, script, stringsAsFactors = F)
    dealerManageInfo$DataBase <- "DealerManage"
    crestWarehouseInfo <- sqlQuery(cfsql02_CrestWarehouse, script, stringsAsFactors = F)
    crestWarehouseInfo$DataBase <- "CrestWarehouse"
    
    #get sprocs
        cfsql02_DealerManage <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=DealerManage;trusted_connection=true')
        cfsql02_CrestWarehouse <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestWarehouse;trusted_connection=true')
        script <- "SELECT Name, Type FROM dbo.sysobjects WHERE (type = 'P')"
        dealerManageSPROCS <- sqlQuery(cfsql02_DealerManage, script, stringsAsFactors = F)
        dealerManageSPROCS$DataBase <- "DealerManage"
        crestWarehouseSPROCS <- sqlQuery(cfsql02_CrestWarehouse, script, stringsAsFactors = F)
        crestWarehouseSPROCS$DataBase <- "CrestWarehouse"

# get into one dataframe -------------------------------------------------- 
        
    tablesAndColumns <- rbind(dealerManageInfo,crestWarehouseInfo)
    tablesAndColumns <- unique(tablesAndColumns)
    sprocs <- rbind(dealerManageSPROCS,crestWarehouseSPROCS)
    sprocs <- unique(sprocs)

# write for first time if the files don't exist, otherwise audit and email---------------------------
    
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit")
    filenames <- list.files()
    
    #tables
        if("TablesAndColumns.csv" %in% filenames){
            #find new and deleted info
                currenttable <- data.frame(fread("TablesAndColumns.csv", stringsAsFactors = F))
                currenttable$CurrentInfo <- 1
                newtable <- tablesAndColumns
                # newtable$NewInfo[grepl("A",newtable$TableName)] <- 1
                newtable$NewInfo <- 1
                newcurrenttable <- merge(currenttable,newtable,by = c(names(tablesAndColumns)),all = T)
                newcurrenttable <- newcurrenttable[is.na(newcurrenttable$CurrentInfo) | is.na(newcurrenttable$NewInfo),]
                newinfo <- newcurrenttable[is.na(newcurrenttable$CurrentInfo) & !is.na(newcurrenttable$NewInfo),]
                deletedinfo <- newcurrenttable[!is.na(newcurrenttable$CurrentInfo) & is.na(newcurrenttable$NewInfo),]
                newinfo$NewInfo <- NULL
                newinfo$CurrentInfo <- NULL
                deletedinfo$NewInfo <- NULL
                deletedinfo$CurrentInfo <- NULL
                if(length(newinfo$TableName) > 0){
                    newinfo$Action <- "New Item Added"
                }
                if(length(deletedinfo$TableName) > 0){
                    deletedinfo$Action <- "Item Deleted"
                }
                newdeletedinfo <- rbind(newinfo,deletedinfo)
                rm(newinfo,deletedinfo)
                gc()
                
            #if either exist, write files and email
                if(length(newdeletedinfo$TableName) > 0){
                    write.csv(newdeletedinfo,file = "NewOrDeleted_TablesFields.csv",quote = T,row.names = F)
                    
                    #email
                        #get email function
                            setwd("E:/Analytics/RScripts/DataProviderAnalysis")
                            source("SendInformationalEmailWithAttachment_Function.R",echo = T)
                            setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")
                    
                    #format email and send
                        # emailAddress <- "dhinkson@crestfinancial.com"
                        ccemailAddress <- ""
                        emailAddress <- "dhinkson@crestfinancial.com;rthorpe@crestfinancial.com"
                        emailDescription <- paste0("Changes Were Made to the Database - ",Sys.Date())
                        emailBody <- paste0("New items on the database: ", length(newdeletedinfo$TableName[
                            newdeletedinfo$Action == "New Item Added" & !is.na(newdeletedinfo$Action)]),"\r\n")
                        emailBody <- paste0(emailBody,"Deleted items on the database: ", length(newdeletedinfo$TableName[
                            newdeletedinfo$Action == "Item Deleted" & !is.na(newdeletedinfo$Action)]),"\r\n")
                        emailBody <- paste0(emailBody,"See the attached file below")
                        sendInformationalEmailWithAttachment(description = emailDescription,theEmailAddress = emailAddress,theCCEmailAddresses = ccemailAddress,
                                                             theBody = emailBody,
                                                             theFileDirectory = "//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit",
                                                             theFileTitle = "NewOrDeleted_TablesFields.csv")
                }
                    
            #overwrite files and delete NewOrDeleted_TablesFields.csv
                setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit")
                write.csv(tablesAndColumns,file = "TablesAndColumns.csv",quote = T,row.names = F)
                file.remove("NewOrDeleted_TablesFields.csv")
            
        }else{
            # write.csv(tablesAndColumns[1:100,],file = "TablesAndColumns.csv",quote = T,row.names = F)
            write.csv(tablesAndColumns,file = "TablesAndColumns.csv",quote = T,row.names = F)
        }
    
    #sprocs
        if("SPROCS.csv" %in% filenames){
            #find new and deleted info
                currenttable <- data.frame(fread("SPROCS.csv", stringsAsFactors = F))
                # currenttable <- currenttable[1:100,]
                currenttable$CurrentInfo <- 1
                newtable <- sprocs
                # newtable$NewInfo[grepl("A",newtable$Name)] <- 1
                newtable$NewInfo <- 1
                newcurrenttable <- merge(currenttable,newtable,by = c(names(sprocs)),all = T)
                newcurrenttable <- newcurrenttable[is.na(newcurrenttable$CurrentInfo) | is.na(newcurrenttable$NewInfo),]
                newinfo <- newcurrenttable[is.na(newcurrenttable$CurrentInfo) & !is.na(newcurrenttable$NewInfo),]
                deletedinfo <- newcurrenttable[!is.na(newcurrenttable$CurrentInfo) & is.na(newcurrenttable$NewInfo),]
                newinfo$NewInfo <- NULL
                newinfo$CurrentInfo <- NULL
                deletedinfo$NewInfo <- NULL
                deletedinfo$CurrentInfo <- NULL
                if(length(newinfo$Name) > 0){
                    newinfo$Action <- "New Item Added"
                }
                if(length(deletedinfo$Name) > 0){
                    deletedinfo$Action <- "Item Deleted"
                }
                newdeletedinfo <- rbind(newinfo,deletedinfo)
                rm(newinfo,deletedinfo)
                gc()
            
            #if either exist, write files and email
                if(length(newdeletedinfo$Name) > 0){
                    write.csv(newdeletedinfo,file = "NewOrDeleted_SPROCS.csv",quote = T,row.names = F)
                    
                    #email
                        #get email function
                            setwd("E:/Analytics/RScripts/DataProviderAnalysis")
                            source("SendInformationalEmailWithAttachment_Function.R",echo = T)
                            setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")
                    
                    #format email and send
                        # emailAddress <- "dhinkson@crestfinancial.com"
                        ccemailAddress <- ""
                        emailAddress <- "dhinkson@crestfinancial.com;rthorpe@crestfinancial.com"
                        emailDescription <- paste0("Changes Were Made to the SPROCs - ",Sys.Date())
                        emailBody <- paste0("New SPROCS: ", length(newdeletedinfo$Name[
                            newdeletedinfo$Action == "New Item Added" & !is.na(newdeletedinfo$Action)]),"\r\n")
                        emailBody <- paste0(emailBody,"Deleted SPROCS: ", length(newdeletedinfo$Name[
                            newdeletedinfo$Action == "Item Deleted" & !is.na(newdeletedinfo$Action)]),"\r\n")
                        emailBody <- paste0(emailBody,"See the attached file below")
                        sendInformationalEmailWithAttachment(description = emailDescription,theEmailAddress = emailAddress,theCCEmailAddresses = ccemailAddress,
                                                             theBody = emailBody,
                                                             theFileDirectory = "//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit",
                                                             theFileTitle = "NewOrDeleted_SPROCS.csv")
                }
            
            #overwrite files and delete NewOrDeleted_TablesFields.csv
                setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/DatabaseAudit")
                write.csv(sprocs,file = "SPROCS.csv",quote = T,row.names = F)
                file.remove("NewOrDeleted_SPROCS.csv")
        }else{
            # write.csv(sprocs[1:100,],file = "SPROCS.csv",quote = T,row.names = F)
            write.csv(sprocs,file = "SPROCS.csv",quote = T,row.names = F)
        }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


