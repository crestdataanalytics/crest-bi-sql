#need to evaluate the difference between Ryan's weekly vintage and mine

#make temp copy of leases
    tempLeases <- leases
    tempLeases <- tempLeases[tempLeases$CreationDate >= "2016-01-01",]
    tempLeases <- tempLeases[,c("ID","LoanID","CreationDate","FundedAmount","Funded","FundedPastFirstDueDate","CrestFPD","CrestFPDAMT","Status")]
    gc()
    
#get ryans data
    fpdStandard <- data.frame(fread("FPD_Standard.csv", stringsAsFactors = F,colClasses = c("character")))
    colnames(fpdStandard)[1] <- "LoanID"
    # fpdStandard <- fpdStandard[,c("LoanID","Status","FundedAmount","ApprovalAmount","AutoApprovedFlag","ExchangedLease","ApprovalUser","SalesRep",
    #                               "RiskScore","FirstScheduledPaymentOutcomeID")]
    fpdStandard$FirstScheduledPaymentOutcomeID <- as.numeric(fpdStandard$FirstScheduledPaymentOutcomeID)
    fpdStandard$LoanID <- as.numeric(fpdStandard$LoanID)
    # table(fpdStandard$Status[fpdStandard$FirstScheduledPaymentOutcomeID %in% c(3,4,5)])
    fpdStandard$FPDStandardEligible <- 0
    fpdStandard$FPDStandardEligible[fpdStandard$FirstScheduledPaymentOutcomeID %in% c(3,4,5) & !is.na(fpdStandard$FirstScheduledPaymentOutcomeID)] <- 1
    fpdStandard$FPDStandard <- 0
    fpdStandard$FPDStandard[fpdStandard$FirstScheduledPaymentOutcomeID == 3 & !is.na(fpdStandard$FirstScheduledPaymentOutcomeID)] <- 1
    colnames(fpdStandard) <- paste0("FPD_Standard_",colnames(fpdStandard))
    colnames(fpdStandard)[1] <- "LoanID"
    names(fpdStandard)[names(fpdStandard) == "FPD_Standard_FPDStandard"] <- "FPDStandard"
    names(fpdStandard)[names(fpdStandard) == "FPD_Standard_FPDStandardEligible"] <- "FPDStandardEligible"
    
#merge the two
    check <- merge(tempLeases,fpdStandard,by = c("LoanID"),all = T)
    
#check differences
    #eligibility
        #dan yes ryan no
            dyrn <- check[!is.na(check$Funded) & check$Funded == 1,]
            dyrn <- dyrn[!is.na(dyrn$FundedPastFirstDueDate) & dyrn$FundedPastFirstDueDate == 1,]
            table(dyrn$FPDStandardEligible) #i call eligible and Ryan doesn't
            table(is.na(dyrn$FPDStandardEligible)) #didn't hit ryan's script
            dyrn <- dyrn[is.na(dyrn$FPDStandardEligible) | (!is.na(dyrn$FPDStandardEligible) & dyrn$FPDStandardEligible == 0),]
            dyrn <- dyrn[,c("ID","LoanID","CreationDate","Status","Funded","FundedPastFirstDueDate","CrestFPD","FPDStandard","FPDStandardEligible",
                            "FPD_Standard_ExchangedLease")]
            dyrnIDs <- unique(dyrn$ID)
            
            #look at accounts that aren't exchange
                dyrnsub <- dyrn[dyrn$Status != "Exchange",]
                
        #dan no ryan yes
            dnry <- check[!is.na(check$FPDStandardEligible) & check$FPDStandardEligible == 1,]
            dnry <- dnry[(!is.na(dnry$Funded) & dnry$Funded == 0) | is.na(dnry$FPDStandardEligible),]
        
        
        
    #FPD
        fpdCheck <- check[check$ID %in% dyrnIDs == F,]
        
        #assumptions for crest standard
            fpdCheck <- fpdCheck[!is.na(fpdCheck$FPD_Standard_FirstScheduledPaymentOutcomeID) & 
                                     fpdCheck$FPD_Standard_FirstScheduledPaymentOutcomeID %in% c(3,5),]
            table(is.na(fpdCheck$FPD_Standard_FirstScheduledPaymentOutcomeID))
            
        #dan yes ryan no
            fpddyrn <- fpdCheck[!is.na(fpdCheck$CrestFPD) & fpdCheck$CrestFPD == 1,]
            fpddyrn <- fpddyrn[fpddyrn$FPD_Standard_FirstScheduledPaymentOutcomeID != 3,]
        
        #dan no ryan yes
            fpddnry <- fpdCheck[!is.na(fpdCheck$FPD_Standard_FirstScheduledPaymentOutcomeID) & fpdCheck$FPD_Standard_FirstScheduledPaymentOutcomeID == 3,]
            fpddnry <- fpddnry[is.na(fpddnry$CrestFPD) | (!is.na(fpddnry$CrestFPD) & fpddnry$CrestFPD == 0),]
            fpddnry <- fpddnry[!is.na(fpddnry$ID),]
            fpddnry <- fpddnry[,c("ID","LoanID","CreationDate","Status","Funded","FundedPastFirstDueDate","CrestFPD","FPDStandard","FPDStandardEligible",
                            "FPD_Standard_ExchangedLease")]
    
    # #dan yes ryan no
    #     dyrn <- check[!is.na(check$CrestFPD) & check$CrestFPD == 1,]
    #     dyrn <- dyrn[]
    #     dyrn <- dyrn[!is.na(dyrn$FundedPastFirstDueDate) & dyrn$FundedPastFirstDueDate == 1,]
    #     table(is.na(check$FPDStandard))
    #     table(check$FPDStandard)
    #     dyrn <- dyrn[is.na(dyrn$FPDStandard) | (!is.na(dyrn$FPDStandard) & dyrn$FPDStandard == 0),]
    #     table(dyrn$FPD_Standard_Status)
    #     table(dyrn$FPD_Standard_ExchangedLease)
    #     table(dyrn$FPDStandardEligible)
        