#we want to see what volume on "no info" bav codes from datax there is that would go to FactorTrustlibrary(lubridate)



library(data.table)
library(fasttime)
setwd("F:/CSVFiles/DataProviderAnalysis")

#this gets the csv files
        bav <- data.frame(fread("BAV_DataX_Dec_2015.csv", stringsAsFactors = F))
        colnames(bav)[1] <- "DocumentID"
        bav$GenerationDate <- as.Date(bav$GenerationDate)
        bav$Vintage <- bav$GenerationDate
        day(bav$Vintage) <- 1
        bav <- bav[bav$GenerationDate >= "2016-01-09",]
        
        noInfo <- bav[bav$VariableValue %in% c("XD00","XD01","XT00"),]
        freq <- data.frame(table(noInfo$VariableValue))
        
        freqAll <- data.frame(table(bav$VariableValue))
        summary(bav$GenerationDate[bav$VariableValue == "NULL"])
        