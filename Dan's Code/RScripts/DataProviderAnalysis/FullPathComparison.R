
#Al wants a daily comparison of the full path
#need to use the dfV3 for this.

setwd("F:/CSVFiles/DataProviderAnalysis")

#get leases that went through full path
fullPath <- dfV3

FullPathClarityDataX <- read.csv("FullPathClarityDataX.csv",stringsAsFactors = F)
colnames(FullPathClarityDataX)[1] <- "TrackingID"

fullPath <- fullPath[!is.na(fullPath$ApprovalProviderSettingsID) & fullPath$ApprovalProviderSettingsID == 1,]
fullPath <- fullPath[!is.na(fullPath$InquiryDate) & fullPath$InquiryDate >= "2015-01-01",]
fullPath <- fullPath[!is.na(fullPath$TrackingID),]
fullPath <- fullPath[fullPath$TrackingID %in% FullPathClarityDataX$TrackingID,]

fullPath <- merge(fullPath, FullPathClarityDataX, by = c("TrackingID"))
fullPath$Vintage <- as.Date(fullPath$InquiryDate)
colnames(fullPath)



#ENUM FOR CLARITY AND DATAX RESPONSE
# Approved        = 1,
# Denied          = 2,
# Exception       = 3, 
# SoftKillDenied  = 4, 
# HardKillDenied  = 5, 
# Pending         = 6


FullPathClarityDataX$TrackingID[FullPathClarityDataX$TrackingID %in% fullPath$TrackingID == F]


table(fullPath$ApprovalProviderSettingsID)
dfV3$TrackingID[dfV3$ApprovalProviderSettingsID == 2 & !is.na(dfV3$ApprovalProviderSettingsID)]
table(dfV3$TrackingID[dfV3$TrackingID %in% fullPath$DataXTrackingID])

#clarity approved datax approved
{
        #SUBMITTED
        cApproveDApprove <- fullPath[fullPath$ClarityResponse == 1 & fullPath$DataXResponse == 1,]
        totalSubmitsLeases <- unique(cApproveDApprove[,c("ID","InquiryDate","Vintage")])
        totalSubmitsAgg <- aggregate(ID~Vintage, data = totalSubmitsLeases, FUN = "length")
        totalSubmitsAgg <- totalSubmitsAgg[order(totalSubmitsAgg$Vintage, decreasing = T),]
        colnames(totalSubmitsAgg)[2] <- "TotalSubmits"
        
        submits <- totalSubmitsAgg
        
        
        #total Applicants
        totalApplicants <- unique(cApproveDApprove[,c("ID","InquiryDate","Vintage"),])
        totalApplicantsAgg <- aggregate(ID~Vintage, data = totalApplicants, FUN = "length")
        colnames(totalApplicantsAgg)[2] <- "TotalApplicants"
        
        #auto approvals
        autoApproved <- cApproveDApprove[cApproveDApprove$IsAutoApproved == 1 & !is.na(cApproveDApprove$IsAutoApproved),]
        autoApproved <- unique(autoApproved[,c("ID","InquiryDate","Vintage")])
        totalAutoApprovedAgg <- aggregate(ID~Vintage, data = autoApproved, FUN = "length")
        totalAutoApprovedAgg <- totalAutoApprovedAgg[order(totalAutoApprovedAgg$Vintage, decreasing = T),]
        colnames(totalAutoApprovedAgg)[2] <- "TotalAutoApproved"
        
        
        #PERFORMANCE
        #funded
        funded <- cApproveDApprove[cApproveDApprove$FundedIndicator == 1 & !is.na(cApproveDApprove$FundedIndicator),]
        funded <- unique(funded[,c("ID","InquiryDate","Vintage")])
        totalFundedAgg <- aggregate(ID~Vintage, data = funded, FUN = "length")
        totalFundedAgg <- totalFundedAgg[order(totalFundedAgg$Vintage, decreasing = T),]
        colnames(totalFundedAgg)[2] <- "TotalFunded"
        
        #fpd
        fpd <- cApproveDApprove[cApproveDApprove$FirstPaymentDefault == 1 & !is.na(cApproveDApprove$FirstPaymentDefault),]
        fpd <- unique(fpd[,c("ID","InquiryDate","Vintage")])
        totalfpdAgg <- aggregate(ID~Vintage, data = fpd, FUN = "length")
        totalfpdAgg <- totalfpdAgg[order(totalfpdAgg$Vintage, decreasing = T),]
        colnames(totalfpdAgg)[2] <- "TotalFPD"
        
        #90DNP
        NinetyDNP <- cApproveDApprove[cApproveDApprove$NinetyDayDelinquency == 1 & !is.na(cApproveDApprove$NinetyDayDelinquency),]
        NinetyDNP <- unique(NinetyDNP[,c("ID","InquiryDate","Vintage")])
        totalNinetyDNPAgg <- aggregate(ID~Vintage, data = NinetyDNP, FUN = "length")
        totalNinetyDNPAgg <- totalNinetyDNPAgg[order(totalNinetyDNPAgg$Vintage, decreasing = T),]
        colnames(totalNinetyDNPAgg)[2] <- "TotalNinetyDNP"
        
        #ChargeOff
        chargeOff <- cApproveDApprove[cApproveDApprove$ChargeOffIndicator == 1 & !is.na(cApproveDApprove$ChargeOffIndicator),]
        chargeOff <- unique(chargeOff[,c("ID","InquiryDate","Vintage")])
        totalChargeOffAgg <- aggregate(ID~Vintage, data = chargeOff, FUN = "length")
        totalChargeOffAgg <- totalChargeOffAgg[order(totalChargeOffAgg$Vintage, decreasing = T),]
        colnames(totalChargeOffAgg)[2] <- "TotalChargeOff"
        
        
        #set up finalcAdA report
        finalcAdA <- merge(x = submits, y = totalApplicantsAgg, by = c("Vintage"), all.x = T) #this replaces the line above
        finalcAdA <- merge(x = finalcAdA, y = totalAutoApprovedAgg, by = c("Vintage"), all.x = T)
        finalcAdA$AutoApprovedPercent <- finalcAdA$TotalAutoApproved / finalcAdA$TotalApplicants
        finalcAdA <- merge(x = finalcAdA, y = totalFundedAgg, by = c("Vintage"), all.x = T)
        finalcAdA$FundedPercent <- finalcAdA$TotalFunded / finalcAdA$TotalAutoApproved
        finalcAdA <- merge(x = finalcAdA, y = totalfpdAgg, by = c("Vintage"), all.x = T)
        finalcAdA$FPDPercent <- finalcAdA$TotalFPD / finalcAdA$TotalFunded
        finalcAdA <- merge(x = finalcAdA, y = totalNinetyDNPAgg, by = c("Vintage"), all.x = T)
        finalcAdA$NinetyDNPPercent <- finalcAdA$TotalNinetyDNP / finalcAdA$TotalFunded
        finalcAdA <- merge(x = finalcAdA, y = totalChargeOffAgg, by = c("Vintage"), all.x = T)
        finalcAdA$ChargeOffPercent <- finalcAdA$TotalChargeOff / finalcAdA$TotalFunded
        
        finalcAdA$ClarityCase <- "Approved"
        finalcAdA$DataXCase <- "Approved"
        
        finalcAdA$Vintage[finalcAdA$TotalFPD != finalcAdA$TotalNinetyDNP]
}

#clarity approved datax denied
{
        #SUBMITTED
        cApproveDDeny <- fullPath[fullPath$ClarityResponse == 1 & fullPath$DataXResponse %in% c(2,4,5),]
        table(cApproveDDeny$DataXResponse)
        totalSubmitsLeases <- unique(cApproveDDeny[,c("ID","InquiryDate","Vintage")])
        totalSubmitsAgg <- aggregate(ID~Vintage, data = totalSubmitsLeases, FUN = "length")
        totalSubmitsAgg <- totalSubmitsAgg[order(totalSubmitsAgg$Vintage, decreasing = T),]
        colnames(totalSubmitsAgg)[2] <- "TotalSubmits"
        
        submits <- totalSubmitsAgg
        
        
        #total Applicants
        totalApplicants <- unique(cApproveDDeny[,c("ID","InquiryDate","Vintage"),])
        totalApplicantsAgg <- aggregate(ID~Vintage, data = totalApplicants, FUN = "length")
        colnames(totalApplicantsAgg)[2] <- "TotalApplicants"
        
        #auto approvals
        autoApproved <- cApproveDDeny[cApproveDDeny$IsAutoApproved == 1 & !is.na(cApproveDDeny$IsAutoApproved),]
        autoApproved <- unique(autoApproved[,c("ID","InquiryDate","Vintage")])
        totalAutoApprovedAgg <- aggregate(ID~Vintage, data = autoApproved, FUN = "length")
        totalAutoApprovedAgg <- totalAutoApprovedAgg[order(totalAutoApprovedAgg$Vintage, decreasing = T),]
        colnames(totalAutoApprovedAgg)[2] <- "TotalAutoApproved"
        
        
        #PERFORMANCE
        #funded
        funded <- cApproveDDeny[cApproveDDeny$FundedIndicator == 1 & !is.na(cApproveDDeny$FundedIndicator),]
        funded <- unique(funded[,c("ID","InquiryDate","Vintage")])
        totalFundedAgg <- aggregate(ID~Vintage, data = funded, FUN = "length")
        totalFundedAgg <- totalFundedAgg[order(totalFundedAgg$Vintage, decreasing = T),]
        colnames(totalFundedAgg)[2] <- "TotalFunded"
        
        #fpd
        fpd <- cApproveDDeny[cApproveDDeny$FirstPaymentDefault == 1 & !is.na(cApproveDDeny$FirstPaymentDefault),]
        fpd <- unique(fpd[,c("ID","InquiryDate","Vintage")])
        totalfpdAgg <- aggregate(ID~Vintage, data = fpd, FUN = "length")
        totalfpdAgg <- totalfpdAgg[order(totalfpdAgg$Vintage, decreasing = T),]
        colnames(totalfpdAgg)[2] <- "TotalFPD"
        
        #90DNP
        NinetyDNP <- cApproveDDeny[cApproveDDeny$NinetyDayDelinquency == 1 & !is.na(cApproveDDeny$NinetyDayDelinquency),]
        NinetyDNP <- unique(NinetyDNP[,c("ID","InquiryDate","Vintage")])
        totalNinetyDNPAgg <- aggregate(ID~Vintage, data = NinetyDNP, FUN = "length")
        totalNinetyDNPAgg <- totalNinetyDNPAgg[order(totalNinetyDNPAgg$Vintage, decreasing = T),]
        colnames(totalNinetyDNPAgg)[2] <- "TotalNinetyDNP"
        
        #ChargeOff
        chargeOff <- cApproveDDeny[cApproveDDeny$ChargeOffIndicator == 1 & !is.na(cApproveDDeny$ChargeOffIndicator),]
        chargeOff <- unique(chargeOff[,c("ID","InquiryDate","Vintage")])
        totalChargeOffAgg <- aggregate(ID~Vintage, data = chargeOff, FUN = "length")
        totalChargeOffAgg <- totalChargeOffAgg[order(totalChargeOffAgg$Vintage, decreasing = T),]
        colnames(totalChargeOffAgg)[2] <- "TotalChargeOff"
        
        
        #set up finalcAdD report
        finalcAdD <- merge(x = submits, y = totalApplicantsAgg, by = c("Vintage"), all.x = T) #this replaces the line above
        finalcAdD <- merge(x = finalcAdD, y = totalAutoApprovedAgg, by = c("Vintage"), all.x = T)
        finalcAdD$AutoApprovedPercent <- finalcAdD$TotalAutoApproved / finalcAdD$TotalApplicants
        finalcAdD <- merge(x = finalcAdD, y = totalFundedAgg, by = c("Vintage"), all.x = T)
        finalcAdD$FundedPercent <- finalcAdD$TotalFunded / finalcAdD$TotalAutoApproved
        finalcAdD <- merge(x = finalcAdD, y = totalfpdAgg, by = c("Vintage"), all.x = T)
        finalcAdD$FPDPercent <- finalcAdD$TotalFPD / finalcAdD$TotalFunded
        finalcAdD <- merge(x = finalcAdD, y = totalNinetyDNPAgg, by = c("Vintage"), all.x = T)
        finalcAdD$NinetyDNPPercent <- finalcAdD$TotalNinetyDNP / finalcAdD$TotalFunded
        finalcAdD <- merge(x = finalcAdD, y = totalChargeOffAgg, by = c("Vintage"), all.x = T)
        finalcAdD$ChargeOffPercent <- finalcAdD$TotalChargeOff / finalcAdD$TotalFunded
        
        finalcAdD$ClarityCase <- "Approved"
        finalcAdD$DataXCase <- "Denied"
        
        finalcAdD$Vintage[finalcAdD$TotalFPD != finalcAdD$TotalNinetyDNP]
}

#clarity denied datax approved
{
        #SUBMITTED
        cDenyDApprove <- fullPath[fullPath$ClarityResponse %in% c(2,4,5) & fullPath$DataXResponse == 1,]
        totalSubmitsLeases <- unique(cDenyDApprove[,c("ID","InquiryDate","Vintage")])
        totalSubmitsAgg <- aggregate(ID~Vintage, data = totalSubmitsLeases, FUN = "length")
        totalSubmitsAgg <- totalSubmitsAgg[order(totalSubmitsAgg$Vintage, decreasing = T),]
        colnames(totalSubmitsAgg)[2] <- "TotalSubmits"
        
        submits <- totalSubmitsAgg
        
        
        #total Applicants
        totalApplicants <- unique(cDenyDApprove[,c("ID","InquiryDate","Vintage"),])
        totalApplicantsAgg <- aggregate(ID~Vintage, data = totalApplicants, FUN = "length")
        colnames(totalApplicantsAgg)[2] <- "TotalApplicants"
        
        #auto approvals
        autoApproved <- cDenyDApprove[cDenyDApprove$IsAutoApproved == 1 & !is.na(cDenyDApprove$IsAutoApproved),]
        autoApproved <- unique(autoApproved[,c("ID","InquiryDate","Vintage")])
        totalAutoApprovedAgg <- aggregate(ID~Vintage, data = autoApproved, FUN = "length")
        totalAutoApprovedAgg <- totalAutoApprovedAgg[order(totalAutoApprovedAgg$Vintage, decreasing = T),]
        colnames(totalAutoApprovedAgg)[2] <- "TotalAutoApproved"
        
        
        #PERFORMANCE
        #funded
        funded <- cDenyDApprove[cDenyDApprove$FundedIndicator == 1 & !is.na(cDenyDApprove$FundedIndicator),]
        funded <- unique(funded[,c("ID","InquiryDate","Vintage")])
        totalFundedAgg <- aggregate(ID~Vintage, data = funded, FUN = "length")
        totalFundedAgg <- totalFundedAgg[order(totalFundedAgg$Vintage, decreasing = T),]
        colnames(totalFundedAgg)[2] <- "TotalFunded"
        
        #fpd
        fpd <- cDenyDApprove[cDenyDApprove$FirstPaymentDefault == 1 & !is.na(cDenyDApprove$FirstPaymentDefault),]
        fpd <- unique(fpd[,c("ID","InquiryDate","Vintage")])
        totalfpdAgg <- aggregate(ID~Vintage, data = fpd, FUN = "length")
        totalfpdAgg <- totalfpdAgg[order(totalfpdAgg$Vintage, decreasing = T),]
        colnames(totalfpdAgg)[2] <- "TotalFPD"
        
        #90DNP
        NinetyDNP <- cDenyDApprove[cDenyDApprove$NinetyDayDelinquency == 1 & !is.na(cDenyDApprove$NinetyDayDelinquency),]
        NinetyDNP <- unique(NinetyDNP[,c("ID","InquiryDate","Vintage")])
        totalNinetyDNPAgg <- aggregate(ID~Vintage, data = NinetyDNP, FUN = "length")
        totalNinetyDNPAgg <- totalNinetyDNPAgg[order(totalNinetyDNPAgg$Vintage, decreasing = T),]
        colnames(totalNinetyDNPAgg)[2] <- "TotalNinetyDNP"
        
        #ChargeOff
        chargeOff <- cDenyDApprove[cDenyDApprove$ChargeOffIndicator == 1 & !is.na(cDenyDApprove$ChargeOffIndicator),]
        chargeOff <- unique(chargeOff[,c("ID","InquiryDate","Vintage")])
        totalChargeOffAgg <- aggregate(ID~Vintage, data = chargeOff, FUN = "length")
        totalChargeOffAgg <- totalChargeOffAgg[order(totalChargeOffAgg$Vintage, decreasing = T),]
        colnames(totalChargeOffAgg)[2] <- "TotalChargeOff"
        
        
        #set up finalcDdA report
        finalcDdA <- merge(x = submits, y = totalApplicantsAgg, by = c("Vintage"), all.x = T) #this replaces the line above
        finalcDdA <- merge(x = finalcDdA, y = totalAutoApprovedAgg, by = c("Vintage"), all.x = T)
        finalcDdA$AutoApprovedPercent <- finalcDdA$TotalAutoApproved / finalcDdA$TotalApplicants
        finalcDdA <- merge(x = finalcDdA, y = totalFundedAgg, by = c("Vintage"), all.x = T)
        finalcDdA$FundedPercent <- finalcDdA$TotalFunded / finalcDdA$TotalAutoApproved
        finalcDdA <- merge(x = finalcDdA, y = totalfpdAgg, by = c("Vintage"), all.x = T)
        finalcDdA$FPDPercent <- finalcDdA$TotalFPD / finalcDdA$TotalFunded
        finalcDdA <- merge(x = finalcDdA, y = totalNinetyDNPAgg, by = c("Vintage"), all.x = T)
        finalcDdA$NinetyDNPPercent <- finalcDdA$TotalNinetyDNP / finalcDdA$TotalFunded
        finalcDdA <- merge(x = finalcDdA, y = totalChargeOffAgg, by = c("Vintage"), all.x = T)
        finalcDdA$ChargeOffPercent <- finalcDdA$TotalChargeOff / finalcDdA$TotalFunded
        
        finalcDdA$ClarityCase <- "Denied"
        finalcDdA$DataXCase <- "Approved"
        
        finalcDdA$Vintage[finalcDdA$TotalFPD != finalcDdA$TotalNinetyDNP]
}

#clarity denied datax denied
{
        #SUBMITTED
        cDenyDDeny <- fullPath[fullPath$ClarityResponse %in% c(2,4,5) & fullPath$DataXResponse %in% c(2,4,5),]
        totalSubmitsLeases <- unique(cDenyDDeny[,c("ID","InquiryDate","Vintage")])
        totalSubmitsAgg <- aggregate(ID~Vintage, data = totalSubmitsLeases, FUN = "length")
        totalSubmitsAgg <- totalSubmitsAgg[order(totalSubmitsAgg$Vintage, decreasing = T),]
        colnames(totalSubmitsAgg)[2] <- "TotalSubmits"
        
        submits <- totalSubmitsAgg
        
        
        #total Applicants
        totalApplicants <- unique(cDenyDDeny[,c("ID","InquiryDate","Vintage"),])
        totalApplicantsAgg <- aggregate(ID~Vintage, data = totalApplicants, FUN = "length")
        colnames(totalApplicantsAgg)[2] <- "TotalApplicants"
        
        #auto approvals
        autoApproved <- cDenyDDeny[cDenyDDeny$IsAutoApproved == 1 & !is.na(cDenyDDeny$IsAutoApproved),]
        autoApproved <- unique(autoApproved[,c("ID","InquiryDate","Vintage")])
        totalAutoApprovedAgg <- aggregate(ID~Vintage, data = autoApproved, FUN = "length")
        totalAutoApprovedAgg <- totalAutoApprovedAgg[order(totalAutoApprovedAgg$Vintage, decreasing = T),]
        colnames(totalAutoApprovedAgg)[2] <- "TotalAutoApproved"
        
        
        #PERFORMANCE
        #funded
        funded <- cDenyDDeny[cDenyDDeny$FundedIndicator == 1 & !is.na(cDenyDDeny$FundedIndicator),]
        funded <- unique(funded[,c("ID","InquiryDate","Vintage")])
        totalFundedAgg <- aggregate(ID~Vintage, data = funded, FUN = "length")
        totalFundedAgg <- totalFundedAgg[order(totalFundedAgg$Vintage, decreasing = T),]
        colnames(totalFundedAgg)[2] <- "TotalFunded"
        
        #fpd
        fpd <- cDenyDDeny[cDenyDDeny$FirstPaymentDefault == 1 & !is.na(cDenyDDeny$FirstPaymentDefault),]
        fpd <- unique(fpd[,c("ID","InquiryDate","Vintage")])
        totalfpdAgg <- aggregate(ID~Vintage, data = fpd, FUN = "length")
        totalfpdAgg <- totalfpdAgg[order(totalfpdAgg$Vintage, decreasing = T),]
        colnames(totalfpdAgg)[2] <- "TotalFPD"
        
        #90DNP
        NinetyDNP <- cDenyDDeny[cDenyDDeny$NinetyDayDelinquency == 1 & !is.na(cDenyDDeny$NinetyDayDelinquency),]
        NinetyDNP <- unique(NinetyDNP[,c("ID","InquiryDate","Vintage")])
        totalNinetyDNPAgg <- aggregate(ID~Vintage, data = NinetyDNP, FUN = "length")
        totalNinetyDNPAgg <- totalNinetyDNPAgg[order(totalNinetyDNPAgg$Vintage, decreasing = T),]
        colnames(totalNinetyDNPAgg)[2] <- "TotalNinetyDNP"
        
        #ChargeOff
        chargeOff <- cDenyDDeny[cDenyDDeny$ChargeOffIndicator == 1 & !is.na(cDenyDDeny$ChargeOffIndicator),]
        chargeOff <- unique(chargeOff[,c("ID","InquiryDate","Vintage")])
        totalChargeOffAgg <- aggregate(ID~Vintage, data = chargeOff, FUN = "length")
        totalChargeOffAgg <- totalChargeOffAgg[order(totalChargeOffAgg$Vintage, decreasing = T),]
        colnames(totalChargeOffAgg)[2] <- "TotalChargeOff"
        
        
        #set up finalcDdD report
        finalcDdD <- merge(x = submits, y = totalApplicantsAgg, by = c("Vintage"), all.x = T) #this replaces the line above
        finalcDdD <- merge(x = finalcDdD, y = totalAutoApprovedAgg, by = c("Vintage"), all.x = T)
        finalcDdD$AutoApprovedPercent <- finalcDdD$TotalAutoApproved / finalcDdD$TotalApplicants
        finalcDdD <- merge(x = finalcDdD, y = totalFundedAgg, by = c("Vintage"), all.x = T)
        finalcDdD$FundedPercent <- finalcDdD$TotalFunded / finalcDdD$TotalAutoApproved
        finalcDdD <- merge(x = finalcDdD, y = totalfpdAgg, by = c("Vintage"), all.x = T)
        finalcDdD$FPDPercent <- finalcDdD$TotalFPD / finalcDdD$TotalFunded
        finalcDdD <- merge(x = finalcDdD, y = totalNinetyDNPAgg, by = c("Vintage"), all.x = T)
        finalcDdD$NinetyDNPPercent <- finalcDdD$TotalNinetyDNP / finalcDdD$TotalFunded
        finalcDdD <- merge(x = finalcDdD, y = totalChargeOffAgg, by = c("Vintage"), all.x = T)
        finalcDdD$ChargeOffPercent <- finalcDdD$TotalChargeOff / finalcDdD$TotalFunded
        
        finalcDdD$ClarityCase <- "Denied"
        finalcDdD$DataXCase <- "Denied"
        
        finalcDdD$Vintage[finalcDdD$TotalFPD != finalcDdD$TotalNinetyDNP]
}

#get final summary
{
        final <- unique(data.frame(fullPath$Vintage))
        colnames(final)[1] <- "Vintage"
        final <- merge(x = final, y = finalcAdA, by = c("Vintage"), all.x = T)
        final <- rbind(final, finalcAdD)
        final <- rbind(final, finalcDdA)
        final <- rbind(final, finalcDdD)
        final[is.na(final)] <- 0
        final <- final[order(final$Vintage,decreasing = T),]
        
        #audit final
        head(final)
        test <- fullPath[fullPath$Vintage == "2015-12-09",]
        test <- test[test$ClarityResponse == 1 & test$DataXResponse == 1,]
        length(unique(test$ID))
}

write.csv(final, file = "FullPathApprovals_Clarity_DataX.csv", quote = T, row.names = F)
max(fullPath$InquiryDate,na.rm = T)
rawAccounts <- fullPath[,c("ID","ApplicantID","CreationDate","InquiryDate","ApprovalDate","FundedDate","FundedIndicator","IsAutoApproved",
                        "IsApproved","FirstPaymentDefault","NinetyDayDelinquency","ChargeOffIndicator","ClarityResponse","DataXResponse")]
# rawAccounts$DataProviderApproved <- 0
# rawAccounts$DataProviderApproved[rawAccounts$Decision %in% c("Approve","Approved")] <- 1
cra <- rawAccounts[,c("ID","ApplicantID","CreationDate","InquiryDate","ApprovalDate","FundedDate","FundedIndicator","IsAutoApproved",
                        "IsApproved","FirstPaymentDefault","NinetyDayDelinquency","ChargeOffIndicator","ClarityResponse")]
cra$Decision <- "Denied"
cra$Decision[cra$ClarityResponse == 1] <- "Approved"
write.csv(cra, file = "FullPathClarityLeases.csv", quote = T, row.names = F)
dra <- rawAccounts[,c("ID","ApplicantID","CreationDate","InquiryDate","ApprovalDate","FundedDate","FundedIndicator","IsAutoApproved",
                        "IsApproved","FirstPaymentDefault","NinetyDayDelinquency","ChargeOffIndicator","DataXResponse")]
dra$Decision <- "Denied"
dra$Decision[dra$DataXResponse == 1] <- "Approved"
write.csv(cra, file = "FullPathDataXLeases.csv", quote = T, row.names = F)





#WE WANT PERCENT FPD, CO AND NINETYDNP FOR 2015 AND ALSO FOR THE JAN-JUN 2015 for cAdD and cDdA


#cAdD
fullPathcAdD <- fullPath[fullPath$ClarityResponse == 1 & fullPath$DataXResponse %in% c(2,4,5),]
#FPD
sum(fullPathcAdD$FirstPaymentDefault,na.rm = T) / sum(fullPathcAdD$FundedIndicator,na.rm = T)
sum(fullPathcAdD$FirstPaymentDefault[fullPathcAdD$InquiryDate < "2015-07-01"],na.rm = T) / 
        sum(fullPathcAdD$FundedIndicator[fullPathcAdD$InquiryDate < "2015-07-01"],na.rm = T)
#90DNP
sum(fullPathcAdD$NinetyDayDelinquency,na.rm = T) / sum(fullPathcAdD$FundedIndicator,na.rm = T)
sum(fullPathcAdD$NinetyDayDelinquency[fullPathcAdD$InquiryDate < "2015-07-01"],na.rm = T) / 
        sum(fullPathcAdD$FundedIndicator[fullPathcAdD$InquiryDate < "2015-07-01"],na.rm = T)
#CO
sum(fullPathcAdD$ChargeOffIndicator,na.rm = T) / sum(fullPathcAdD$FundedIndicator,na.rm = T)
sum(fullPathcAdD$ChargeOffIndicator[fullPathcAdD$InquiryDate < "2015-07-01"],na.rm = T) / 
        sum(fullPathcAdD$FundedIndicator[fullPathcAdD$InquiryDate < "2015-07-01"],na.rm = T)



#cDdA
fullPathcDdA <- fullPath[fullPath$ClarityResponse %in% c(2,4,5) & fullPath$DataXResponse == 1,]
#FPD
sum(fullPathcDdA$FirstPaymentDefault,na.rm = T) / sum(fullPathcDdA$FundedIndicator,na.rm = T)
sum(fullPathcDdA$FirstPaymentDefault[fullPathcDdA$InquiryDate < "2015-07-01"],na.rm = T) / 
        sum(fullPathcDdA$FundedIndicator[fullPathcDdA$InquiryDate < "2015-07-01"],na.rm = T)
#90DNP
sum(fullPathcDdA$NinetyDayDelinquency,na.rm = T) / sum(fullPathcDdA$FundedIndicator,na.rm = T)
sum(fullPathcDdA$NinetyDayDelinquency[fullPathcDdA$InquiryDate < "2015-07-01"],na.rm = T) / 
        sum(fullPathcDdA$FundedIndicator[fullPathcDdA$InquiryDate < "2015-07-01"],na.rm = T)
#CO
sum(fullPathcDdA$ChargeOffIndicator,na.rm = T) / sum(fullPathcDdA$FundedIndicator,na.rm = T)
sum(fullPathcDdA$ChargeOffIndicator[fullPathcDdA$InquiryDate < "2015-07-01"],na.rm = T) / 
        sum(fullPathcDdA$FundedIndicator[fullPathcDdA$InquiryDate < "2015-07-01"],na.rm = T)




rm(allIDs,cdDF,chargeOff,clarityfp,claritySubmits,cra,dataxfp,DataXSubmits,dra,final,finalcAdA,finalcAdD,finalcDdA,finalcDdD,fpd,fullPath,NinetyDNP,
        rawAccounts,submits,test,totalApplicants,totalApplicantsAgg,totalApproved,totalApprovedAgg,totalAutoApprovedAgg,totalChargeOffAgg,
        totalClarityApproved,totalApplicants,totalApplicantsAgg,totalApproved,totalApprovedAgg,totalAutoApprovedAgg,totalChargeOffAgg,totalClarityApprovedAgg,
        totalClaritySubmits,totalClaritySubmitsAgg,totalDataXApproved,totalDataXApprovedAgg,totalDataXSubmits,totalDataXSubmitsAgg,totalDupeClarityRequests,
        totalDupeDataXRequests,totalfpdAgg,totalFundedAgg,totalNinetyDNPAgg,totalSubmitsAgg,totalSubmitsLeases,uniqueAPDFIDs,cApproveDApprove,cApproveDDeny,
        cDenyDApprove,cDenyDDeny,fullPathcDdA,fullPathcAdD,fullPath,FullPathClarityDataX)





