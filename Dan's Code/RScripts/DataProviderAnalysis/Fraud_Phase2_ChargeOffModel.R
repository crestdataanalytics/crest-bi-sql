#run test with DX and EB fraud model

library(datarobot)
library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)
library(datarobot)


ConnectToDataRobot(endpoint = "https://app.datarobot.com/api/v2",token = "WBLJi2I9IXpCbiJD2qc6_BDETB8PHITf")

#get leases and formatted xmlshred data
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("Leases_OpenMostRecent.R",echo = T)
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("Fraud_buildFileForFraudScore_DataRobot.R",echo = T)
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("Fraud_runPredictionsUsingCurrentModel_DataRobot.R",echo = T)
    
# ##USE THIS IF THE FILE HASN"T ALREADY BEEN BUILT
# #format xml data
#     theStartDate <- "2016-11-01"
#     theEndDate <- "2016-12-01"
#     finalNormalizedXML <- buildFileForFraudScore_DataRobot(setOfApprovalProviderSettingsIDs = c(6,2),theStartDate = theStartDate,theEndDate = theEndDate)
#         
# #run prediction through DataRobot model ... remove charge off and nopay fields.
#     theprojectID <- "58f0ec94c808914a6a35c8df"
#     themodelID <- "58f1a9108af1cf526dcaa229"
#     thefeatureListName <- "Top43of51"
#     theuploadFile <- finalNormalizedXML[,names(finalNormalizedXML)[names(finalNormalizedXML) %in% c("NoPay_45","ChargeOffIndicator") == F]]
#     themaxWait <- 60*60*24
#     thepredictionColumnName <- "NoPay45_Prediction"
#     finalNormalizedXMLPredictions <- runPredictionsUsingCurrentModel_DataRobot(projectID = theprojectID,modelID = themodelID,featureListName = thefeatureListName,
#                                                       uploadFile = theuploadFile,maxWait = themaxWait,predictionColumnName = thepredictionColumnName)
#     finalNormalizedXMLPredictions <- finalNormalizedXMLPredictions[,c("ID","NoPay45_Prediction")]
#     left_join(finalNormalizedXML,finalNormalizedXMLPredictions,by = c("ID")) -> finalNormalizedXML
#     
# #write the file
#     setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
#     fileTitle <- paste0("Fraud_DataRobotPredictions_Project_",theprojectID,"Model_",themodelID,"_",month(Sys.Date()),day(Sys.Date()),year(Sys.Date()),".csv")
#     write.csv(finalNormalizedXML,file = fileTitle,quote = T,row.names = F)
    
#read raw data
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    finalNormalizedXML <- data.frame(fread("Fraud_DataRobotPredictions_Project_58f0ec94c808914a6a35c8dfModel_58f1a9108af1cf526dcaa229_4192017.csv", 
                     stringsAsFactors = F, colClasses = "character"))
    finalNormalizedXML <- finalNormalizedXML[,names(finalNormalizedXML)[names(finalNormalizedXML) %in% c("NoPay_45","ChargeOffIndicator") == F]]
    finalNormalizedXML$ID <- as.numeric(finalNormalizedXML$ID)
    finalNormalizedXML$NoPay45_Prediction <- as.numeric(finalNormalizedXML$NoPay45_Prediction)
    
    #update the no pay and charge off flags
        templeases45 <- leases[!is.na(leases$FundedPastFirstDueDate) & leases$FundedPastFirstDueDate == 1,]
        templeases45$NoPay_45 <- 0
        templeases45$NoPay_45[templeases45$DaysToPayment >= 45] <- 1
        templeases45 <- templeases45[,c("ID","NoPay_45","ChargeOffIndicator")]
        finalNormalizedXML <- merge(finalNormalizedXML,templeases45,by = c("ID"),all.x = T)
        rm(templeases45)
        gc()
    
#get the cumNoPay (<= 3.5%) and decumNoPay (cutoff >= 30% ) on probabilities
    #put on ChargeOffIndicator,fundedPastFirstDueDate if missing for future use
        left_join(finalNormalizedXML,leases[,c("ID","FundedPastFirstDueDate")],by = c("ID")) -> leasePerformance
        if("ChargeOffIndicator" %in% names(leasePerformance) == F){
            left_join(leasePerformance,leases[,c("ID","ChargeOffIndicator")],by = c("ID")) -> leasePerformance
        }
        if("FundedPastFirstDueDate" %in% names(leasePerformance) == F){
            left_join(leasePerformance,leases[,c("ID","FundedPastFirstDueDate")],by = c("ID")) -> leasePerformance
        }
        
    #calculate cumNoPay and get cutoff
        fpfddfinalNormalizedXML <- leasePerformance[!is.na(leasePerformance$FundedPastFirstDueDate) & leasePerformance$FundedPastFirstDueDate == 1,]
        fpfddfinalNormalizedXML <- fpfddfinalNormalizedXML[order(fpfddfinalNormalizedXML$NoPay45_Prediction,decreasing = F),]
        fpfddfinalNormalizedXML$cumSum_Funded <- cumsum(fpfddfinalNormalizedXML$FundedPastFirstDueDate)
        fpfddfinalNormalizedXML$cumsum_NoPay <- cumsum(fpfddfinalNormalizedXML$NoPay_45)
        fpfddfinalNormalizedXML$cumNoPayPerc <- fpfddfinalNormalizedXML$cumsum_NoPay / fpfddfinalNormalizedXML$cumSum_Funded
        cumNoPay_Cutoff <- max(fpfddfinalNormalizedXML$NoPay45_Prediction[fpfddfinalNormalizedXML$cumNoPayPerc <= 0.035],na.rm = T)
        # setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        # write.csv(fpfddfinalNormalizedXML[,c("ID","NoPay45_Prediction","FundedPastFirstDueDate","NoPay_45","cumSum_Funded","cumsum_NoPay")],file = "check.csv",quote = T,row.names = F)
    
    #calculate decumNoPay and get cutoff
        totalFunded <- sum(fpfddfinalNormalizedXML$FundedPastFirstDueDate)
        totalNoPay <- sum(fpfddfinalNormalizedXML$NoPay_45)
        fpfddfinalNormalizedXML$decum_Funded <- totalFunded - fpfddfinalNormalizedXML$cumSum_Funded
        fpfddfinalNormalizedXML$decum_NoPay <- totalNoPay - fpfddfinalNormalizedXML$cumsum_NoPay
        fpfddfinalNormalizedXML$decumNoPayPerc <- fpfddfinalNormalizedXML$decum_NoPay / fpfddfinalNormalizedXML$decum_Funded
        decumNoPay_Cutoff <- min(fpfddfinalNormalizedXML$NoPay45_Prediction[fpfddfinalNormalizedXML$decumNoPayPerc >= 0.333],na.rm = T)
        # setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        # write.csv(fpfddfinalNormalizedXML[,c("ID","NoPay45_Prediction","FundedPastFirstDueDate","NoPay_45","decum_Funded","decum_NoPay")],file = "check.csv",quote = T,row.names = F)
        
#get records for CO model
    coModelFile <- fpfddfinalNormalizedXML[fpfddfinalNormalizedXML$NoPay45_Prediction > cumNoPay_Cutoff & 
                                               fpfddfinalNormalizedXML$NoPay45_Prediction < decumNoPay_Cutoff,]
    coModelFile$NoPay45_Prediction <- NULL
    coModelFile$NoPay_45 <- NULL
    # table(coModelFile$ChargeOffIndicator[is.na(coModelFile$NoPay_45) | coModelFile$NoPay_45 == 0])
    coModelFile <- coModelFile[,names(coModelFile)[names(coModelFile) %in% c("FundedPastFirstDueDate","cumSum_Funded","cumsum_NoPay","cumNoPayPerc",
                                                                             "decum_Funded","decum_NoPay","decumNoPayPerc") == F]]
    
    #write the raw data for new model
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
        fileTitle <- paste0("Fraud_Phase2_ChargeOffModel_RawData_",month(Sys.Date()),day(Sys.Date()),year(Sys.Date()),".csv")
        # write.csv(coModelFile,file = "check.csv",quote = T,row.names = F)
    
    
#upload to DataRobot
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("DataRobot_UploadFileToDataRobotAndNotify.R",echo = T)
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    theProjectName <- "Fraud_Phase2_ChargeOffModel_DX_EB"
    theNewTarget <- "ChargeOffIndicator"
    UploadFileToDataRobotAndNotify(datafile = coModelFile,theProjectName = theProjectName, theNewTarget = theNewTarget)
    
# #cycle through the featureImpact and running new models with reduced feature list
#     setwd("E:/Analytics/RScripts/DataProviderAnalysis")
#     source("DataRobot_AutomateDataRobotFeatureImpactAndAutoPilot.R",echo = T)
#     
#     #get project id
#         theGetProjectList <- data.frame(GetProjectList())
#         theNewProjectID <- theGetProjectList$projectId[theGetProjectList$projectName == theProjectName]
        
    # #get all models
    #     # newAutopilotInfo <- returnDataFrame
    #     newAutopilotInfo <- automateDataRobotFeatureImpactAndAutoPilot(theProjectID = theNewProjectID,theModelID = "",featureListUsed = "")
    #     while(newAutopilotInfo$NumberOfFeaturesUsed > 20){
    #         featureListUsed <- newAutopilotInfo$featureListName
    #         newAutopilotInfo <- automateDataRobotFeatureImpactAndAutoPilot(theProjectID = theNewProjectID,theModelID = "",featureListUsed = featureListUsed)
    #     }
        
    
    
    
    
    
    
    
    
    