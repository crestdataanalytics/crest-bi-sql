#this will take the files produced by "FormatXML_UniqueApplicantsPerMonth_DX_EB_Cluster_Job.R" and scale



library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)
library(stringr)
library(flexclust)
library(Hmisc)

beginTime <- Sys.time()

#get un-scaled cluster files
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/FormattedXML_Cluster")
    listedFiles <- list.files()
    listedFiles <- listedFiles[grepl("FormatXML_",listedFiles)]
    
    #just scale this month and last month
        theCurrentDate <- Sys.Date()
        day(theCurrentDate) <- 1
        thePastMonth <- theCurrentDate
        month(thePastMonth) <- month(thePastMonth) - 1
        day(thePastMonth) <- 1
        listedFiles <- listedFiles[(grepl(month.abb[month(theCurrentDate)],listedFiles) & grepl(year(theCurrentDate),listedFiles)) |
                                       (grepl(month.abb[month(thePastMonth)],listedFiles) & grepl(year(thePastMonth),listedFiles))]
    
#scale each file
    ###EVENTUALLY WILL NEED TO ACCOUNT FOR ALREADY SCALED FILES
    for(eachFile in listedFiles){
        # eachFile <- listedFiles[1]
        # eachFile <- "FormatXML_UniqueApplicantsPerMonth_DX_EB_Cluster_Oct_2016.csv"
        setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/FormattedXML_Cluster")
        tempClusterDF <- data.frame(fread(eachFile, stringsAsFactors = F))
        tempClusterDF_Scaled <- data.frame(scale(tempClusterDF[names(tempClusterDF) %in% c("ApplicantID","ID","RetailerID") == F]),stringsAsFactors = F)
        
        #get applicantID and ID
            tempClusterDF_Scaled$id <- seq.int(nrow(tempClusterDF_Scaled))
            tempClusterDF$id <- seq.int(nrow(tempClusterDF))
            tempClusterDF_Scaled <- merge(tempClusterDF[,c("ID","ApplicantID","RetailerID","id")],tempClusterDF_Scaled,by = c("id"))
            
        #write file
            setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/FormatXML_Cluster_Scaled")
            filetitle <- gsub("Cluster_","Cluster_Scaled_",eachFile)
            write.csv(tempClusterDF_Scaled,file = filetitle,quote = T,row.names = F)
    }
    
    
    # summary(tempClusterDF_Scaled$RetailerFraudScore)
    # summary(tempClusterDF$RetailerFraudScore)
    # tempClusterDF$ID[tempClusterDF$BIScore == 100]
    
#get email function
    endTime <- Sys.time()
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("SendInformationalEmail_Function.R",echo = T)
    setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")
    
    #format email and send
        emailAddress <- "dhinkson@crestfinancial.com"
        emailBody <- paste0("Start time: ", beginTime,"\n","End time: ", endTime)
        emailDescription <- paste0("FormatXML_UniqueApplicantsPerMonth_DX_EB_Cluster_Scale_Job.R Complete - ",round(as.numeric(difftime(endTime,beginTime,units = "hours")),digits = 2)," hours")
        sendInformationalEmail(emailDescription,emailBody,emailAddress)
    
    
    