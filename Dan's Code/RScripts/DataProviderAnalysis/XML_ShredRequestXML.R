#build shred for xml Requests made to eBureau and DataX


library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)



#####SOME FIELDS NOT GOING THROUGH!!!!! NEEDS FIX





# get data from APRL ------------------------------------------------------

setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Raw/Read_Files/ResponseRequestXML")

startdate <- Sys.Date()
day(startdate) <- 1
enddate <- startdate
month(startdate) <- month(startdate) - 1
dates <- seq.Date(startdate,enddate,by = "months")
# dates <- seq.Date(as.Date("2016-06-01"),as.Date("2017-06-01"),by = "months")
dates <- dates[order(dates,decreasing = T)]
dates <- as.character(dates)
for(i in dates){
    # i <- dates[1]
    print(i)
    fileTitle <- paste0("RequestResponseXML_",year(i),"_",month(i),"_",day(i),".csv")
    aprl <- data.frame(fread(fileTitle, stringsAsFactors = F))
    # if(exists("aprl")){
    #     aprl <- rbind(aprl,tempfile)
    # }else{
    #     aprl <- tempfile
    # }
    aprl <- aprl[!is.na(aprl$RequestXML) & aprl$RequestXML != "",]
    
    # #get all aprlIDs currently in database and filter -----------------------
    
    cfsql02_CrestAnalytics <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestAnalytics;trusted_connection=true')
    script <- "SELECT DISTINCT ApprovalProviderResponseLogID FROM [CrestAnalytics].[dbo].[RequestXML_Shred]"
    aprlids <- sqlQuery(cfsql02_CrestAnalytics, script, stringsAsFactors = F)
    if(length(aprlids$ApprovalProviderResponseLogID) > 0){
        aprl <- aprl[aprl$ApprovalProviderResponseLogID %in% aprlids$ApprovalProviderResponseLogID == F,]
    }
    
    
    # get the request elements ------------------------------------------------
    if(length(aprl$ApplicantID) > 0){
        
        requestXML <- aprl[,names(aprl)[names(aprl) != "ResponseXML"]]
        
        #datax
        dataXElements <- c("NAMEFIRST",
                           "NAMEMIDDLE",
                           "NAMELAST",
                           "STREET1",
                           "STREET2",
                           "CITY",
                           "STATE",
                           "ZIP",
                           "PHONEHOME",
                           "PHONECELL",
                           "PHONEWORK",
                           "PHONEEXT",
                           "EMAIL",
                           "DOB",
                           "SSN",
                           "DLNUMBER",
                           "DLSTATE",
                           "WORKNAME",
                           "WORKSTREET1",
                           "WORKSTREET2",
                           "WORKCITY",
                           "WORKSTATE",
                           "WORKZIP",
                           "PAYPERIOD",
                           "MONTHLYINCOME",
                           "BANKNAME",
                           "BANKABA",
                           "BANKACCTNUMBER")
        dataxRequest <- requestXML[requestXML$ApprovalProviderSettingsID == 2,]
        for(theElement in dataXElements){
            # theElement <- dataXElements[1]
            dataxRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",dataxRequest$RequestXML))
            dataxRequest$Var[grepl("<",dataxRequest$Var)] <- ""
            names(dataxRequest)[names(dataxRequest) == "Var"] <- theElement
        }
        
        #eb
        ebElementGroups <- c("home","work","finance","bank")
        ebHomeElements <- c("first","last","address","city","state","zip","phone","social","email","dob")
        ebWorkElements <- c("company","phone")
        ebFinanceElements <- c("pay","interval")
        ebBankElements <- c("account","route")
        
        ebureauRequest <- requestXML[requestXML$ApprovalProviderSettingsID == 6,]
        
        for(group in ebElementGroups){
            # group <- ebElementGroups[1]
            ebureauRequest$group <- sub(paste0("</",group,">.*"),"",sub(paste0("^.*<",group,">"),"",ebureauRequest$RequestXML))
            if(group == "home"){
                for(theElement in ebHomeElements){
                    # theElement <- ebHomeElements[1]
                    ebureauRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",ebureauRequest$RequestXML))
                    ebureauRequest$Var[grepl("<",ebureauRequest$Var)] <- ""
                    names(ebureauRequest)[names(ebureauRequest) == "Var"] <- theElement
                }
            }
            
            if(group == "work"){
                for(theElement in ebWorkElements){
                    # theElement <- ebHomeElements[1]
                    ebureauRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",ebureauRequest$RequestXML))
                    ebureauRequest$Var[grepl("<",ebureauRequest$Var)] <- ""
                    names(ebureauRequest)[names(ebureauRequest) == "Var"] <- theElement
                }
            }
            
            if(group == "bank"){
                for(theElement in ebBankElements){
                    # theElement <- ebBankElements[1]
                    ebureauRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",ebureauRequest$RequestXML))
                    ebureauRequest$Var[grepl("<",ebureauRequest$Var)] <- ""
                    names(ebureauRequest)[names(ebureauRequest) == "Var"] <- theElement
                }
            }
            
            if(group == "finance"){
                for(theElement in ebFinanceElements){
                    # theElement <- ebFinanceElements[1]
                    ebureauRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",ebureauRequest$RequestXML))
                    ebureauRequest$Var[grepl("<",ebureauRequest$Var)] <- ""
                    names(ebureauRequest)[names(ebureauRequest) == "Var"] <- theElement
                }
            }
            ebureauRequest$group <- NULL
            names(ebureauRequest)[names(ebureauRequest) == "first"] <- "NAMEFIRST"
            names(ebureauRequest)[names(ebureauRequest) == "last"] <- "NAMELAST"
            names(ebureauRequest)[names(ebureauRequest) == "address"] <- "STREET1"
            names(ebureauRequest)[names(ebureauRequest) == "city"] <- "CITY"
            names(ebureauRequest)[names(ebureauRequest) == "state"] <- "STATE"
            names(ebureauRequest)[names(ebureauRequest) == "zip"] <- "ZIP"
            names(ebureauRequest)[names(ebureauRequest) == "social"] <- "SSN"
            names(ebureauRequest)[names(ebureauRequest) == "email"] <- "EMAIL"
            names(ebureauRequest)[names(ebureauRequest) == "dob"] <- "DOB"
            if("PHONEHOME" %in% names(ebureauRequest)){
                names(ebureauRequest)[names(ebureauRequest) == "phone"] <- "PHONEWORK"
            }else{
                names(ebureauRequest)[names(ebureauRequest) == "phone"] <- "PHONEHOME"
            }
            names(ebureauRequest)[names(ebureauRequest) == "company"] <- "WORKNAME"
            names(ebureauRequest)[names(ebureauRequest) == "pay"] <- "MONTHLYINCOME"
            names(ebureauRequest)[names(ebureauRequest) == "interval"] <- "PAYPERIOD"
            names(ebureauRequest)[names(ebureauRequest) == "account"] <- "BANKACCTNUMBER"
            names(ebureauRequest)[names(ebureauRequest) == "route"] <- "BANKABA"
        }
        
        #GDS
        gdsElementGroups <- c("Applicant_Information","ApplicantAddress","BankAccount","Employer","CrystalBall")
        gdsApplicantAddressElements <- c("StreetAddress1","StreetAddress2","City","State","ZipCode","MonthsAtAddress")
        gdsBankAccountElements <- c("BankName","AccountNumber","RoutingNumber","BankAccountType")
        gdsEmployerElements <- c("EmployerName","EmployerPhoneNumber","EmployerAddress","MonthsAtCurrentEmployer","MonthlyIncome","PayFrequency",
                                 "DateOfNextPayday")
        gdsCrystalBallElements <- c("CrystalBallQualifiedAmount","CrystalBallRepeatCustomer","CrystalBallMaxLimit","CrystalBallRegionName",
                                    "CrystalBallProductCategory","CrystalBallDealerRank","CrystalBallDealerLoanLimit","CrystalBallFundingMatrixCategoryId",
                                    "CrystalBallDealerID","CrystalBallCashCollectionCurve","NewRetailerBIOverride","RetailerResetBIScore")
        gdsApplicant_InformationElements <- c("LoanId","ApplicationId","FirstName","LastName","CellPhone","HomePhone","Email","SSN","DateOfBirth",
                                              "DriversLicenseNumber","OwnsHome")
        
        gdsRequest <- requestXML[requestXML$ApprovalProviderSettingsID == 17,]
        
        for(group in gdsElementGroups){
            # group <- gdsElementGroups[1]
            gdsRequest$group <- sub(paste0("</",group,">.*"),"",sub(paste0("^.*<",group,">"),"",gdsRequest$RequestXML))
            if(group == "ApplicantAddress"){
                for(theElement in gdsApplicantAddressElements){
                    # theElement <- gdsApplicantAddressElements[1]
                    gdsRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",gdsRequest$RequestXML))
                    gdsRequest$Var[grepl("<",gdsRequest$Var)] <- ""
                    names(gdsRequest)[names(gdsRequest) == "Var"] <- theElement
                }
            }
            
            if(group == "BankAccount"){
                for(theElement in gdsBankAccountElements){
                    # theElement <- gdsApplicantAddressElements[1]
                    gdsRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",gdsRequest$RequestXML))
                    gdsRequest$Var[grepl("<",gdsRequest$Var)] <- ""
                    names(gdsRequest)[names(gdsRequest) == "Var"] <- theElement
                }
            }
            
            if(group == "CrystalBall"){
                for(theElement in gdsCrystalBallElements){
                    # theElement <- gdsCrystalBallElements[1]
                    gdsRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",gdsRequest$RequestXML))
                    gdsRequest$Var[grepl("<",gdsRequest$Var)] <- ""
                    names(gdsRequest)[names(gdsRequest) == "Var"] <- theElement
                }
            }
            
            if(group == "Employer"){
                for(theElement in gdsEmployerElements){
                    # theElement <- gdsEmployerElements[1]
                    gdsRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",gdsRequest$RequestXML))
                    gdsRequest$Var[grepl("<",gdsRequest$Var)] <- ""
                    names(gdsRequest)[names(gdsRequest) == "Var"] <- theElement
                }
            }
            
            if(group == "Applicant_Information"){
                for(theElement in gdsApplicant_InformationElements){
                    # theElement <- gdsApplicant_InformationElements[1]
                    gdsRequest$Var <- sub(paste0("</",theElement,">.*"),"",sub(paste0("^.*<",theElement,">"),"",gdsRequest$RequestXML))
                    gdsRequest$Var[grepl("<",gdsRequest$Var)] <- ""
                    names(gdsRequest)[names(gdsRequest) == "Var"] <- theElement
                }
            }
            
            gdsRequest$group <- NULL
            names(gdsRequest)[names(gdsRequest) == "FirstName"] <- "NAMEFIRST"
            names(gdsRequest)[names(gdsRequest) == "LastName"] <- "NAMELAST"
            names(gdsRequest)[names(gdsRequest) == "StreetAddress1"] <- "STREET1"
            names(gdsRequest)[names(gdsRequest) == "StreetAddress2"] <- "STREET2"
            names(gdsRequest)[names(gdsRequest) == "City"] <- "CITY"
            names(gdsRequest)[names(gdsRequest) == "State"] <- "STATE"
            names(gdsRequest)[names(gdsRequest) == "ZipCode"] <- "ZIP"
            names(gdsRequest)[names(gdsRequest) == "phone"] <- "PHONEHOME"
            names(gdsRequest)[names(gdsRequest) == "CellPhone"] <- "PHONECELL"
            names(gdsRequest)[names(gdsRequest) == "social"] <- "SSN"
            names(gdsRequest)[names(gdsRequest) == "Email"] <- "EMAIL"
            names(gdsRequest)[names(gdsRequest) == "DateOfBirth"] <- "DOB"
            names(gdsRequest)[names(gdsRequest) == "EmployerName"] <- "WORKNAME"
            names(gdsRequest)[names(gdsRequest) == "EmployerPhoneNumber"] <- "PHONEWORK"
            names(gdsRequest)[names(gdsRequest) == "MonthlyIncome"] <- "MONTHLYINCOME"
            names(gdsRequest)[names(gdsRequest) == "PayFrequency"] <- "PAYPERIOD"
            names(gdsRequest)[names(gdsRequest) == "AccountNumber"] <- "BANKACCTNUMBER"
            names(gdsRequest)[names(gdsRequest) == "RoutingNumber"] <- "BANKABA"
            names(gdsRequest)[names(gdsRequest) == "MonthsAtAddress"] <- "MONTHSATADDRESS"
            names(gdsRequest)[names(gdsRequest) == "BankName"] <- "BANKNAME"
            names(gdsRequest)[names(gdsRequest) == "BankAccountType"] <- "BANKACCOUNTTYPE"
            names(gdsRequest)[names(gdsRequest) == "EmployerAddress"] <- "EMPLOYERADDRESS"
            names(gdsRequest)[names(gdsRequest) == "MonthsAtCurrentEmployer"] <- "MONTHSATCURRENTEMPLOYER"
            names(gdsRequest)[names(gdsRequest) == "DateOfNextPayday"] <- "DATEOFNEXTPAYDAY"
            names(gdsRequest)[names(gdsRequest) == "LoanId"] <- "LoanID_Request"
            names(gdsRequest)[names(gdsRequest) == "ApplicationId"] <- "ApplicantID_Request"
            names(gdsRequest)[names(gdsRequest) == "DriversLicenseNumber"] <- "DLNUMBER"
            names(gdsRequest)[names(gdsRequest) == "HomePhone"] <- "PHONEHOME"
        }
        
        #ensure all have the same columns
        allcolumns <- unique(c(names(dataxRequest),names(ebureauRequest),names(gdsRequest)))
        dxmissingcols <- allcolumns[allcolumns %in% names(dataxRequest) == F]
        if(length(dxmissingcols) > 0 & length(dataxRequest$ApplicantID) > 0){
            dataxRequest[,c(dxmissingcols)] <- ""
        }
        
        ebmissingcols <- allcolumns[allcolumns %in% names(ebureauRequest) == F]
        if(length(ebmissingcols) > 0 & length(ebureauRequest$ApplicantID) > 0){
            ebureauRequest[,c(ebmissingcols)] <- ""
        }
        
        gdsmissingcols <- allcolumns[allcolumns %in% names(gdsRequest) == F]
        if(length(gdsmissingcols) > 0 & length(gdsRequest$ApplicantID) > 0){
            gdsRequest[,c(gdsmissingcols)] <- ""
        }
        
        #final request shred ... fomate fields for write to database
        finalRequestXML <- rbind(dataxRequest,ebureauRequest,gdsRequest)
        if(length(finalRequestXML$ApplicantID) > 0){
            finalRequestXML$RequestXML <- NULL
            finalRequestXML$DOB <- as.POSIXct(finalRequestXML$DOB,format = "%m/%d/%Y")
            finalRequestXML$DATEOFNEXTPAYDAY <- as.POSIXct(finalRequestXML$DATEOFNEXTPAYDAY,format = "%m/%d/%Y")
            finalRequestXML$CreatedDate <- as.POSIXct(finalRequestXML$CreatedDate,format = "%Y-%m-%d %H:%M:%S")
            finalRequestXML$MONTHLYINCOME <- as.numeric(finalRequestXML$MONTHLYINCOME)
            finalRequestXML$LoanID_Request <- as.integer(finalRequestXML$LoanID_Request)
            finalRequestXML$ApplicantID_Request <- as.integer(finalRequestXML$ApplicantID_Request)
            finalRequestXML$MONTHSATADDRESS <- as.numeric(finalRequestXML$MONTHSATADDRESS)
            finalRequestXML$MONTHSATCURRENTEMPLOYER <- as.numeric(finalRequestXML$MONTHSATCURRENTEMPLOYER)
            finalRequestXML$CrystalBallQualifiedAmount <- as.numeric(finalRequestXML$CrystalBallQualifiedAmount)
            finalRequestXML$CrystalBallMaxLimit <- as.numeric(finalRequestXML$CrystalBallMaxLimit)
            finalRequestXML$CrystalBallDealerRank <- as.numeric(finalRequestXML$CrystalBallDealerRank)
            finalRequestXML$CrystalBallDealerLoanLimit <- as.numeric(finalRequestXML$CrystalBallDealerLoanLimit)
            finalRequestXML$CrystalBallDealerLoanLimit <- as.numeric(finalRequestXML$CrystalBallDealerLoanLimit)
            finalRequestXML$CrystalBallFundingMatrixCategoryId <- as.numeric(finalRequestXML$CrystalBallFundingMatrixCategoryId)
            finalRequestXML$CrystalBallDealerID <- as.numeric(finalRequestXML$CrystalBallDealerID)
            finalRequestXML$CrystalBallCashCollectionCurve <- as.numeric(finalRequestXML$CrystalBallCashCollectionCurve)
            finalRequestXML$NewRetailerBIOverride <- as.numeric(finalRequestXML$NewRetailerBIOverride)
            finalRequestXML$RetailerResetBIScore <- as.numeric(finalRequestXML$RetailerResetBIScore)
        }
        
    }
    
    # #write to table in CrestAnalytics ---------------------------------------
    if(length(finalRequestXML$ApplicantID) > 0){
        
        cfsql02_CrestAnalytics <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestAnalytics;trusted_connection=true')
        columnTypes <- list(ApplicantID="int",
                            CreatedDate="datetime",
                            ApprovalProviderSettingsID="int",
                            ApprovalProviderResponseLogID="int",
                            NAMEFIRST="varchar(50)",
                            NAMEMIDDLE="varchar(50)",
                            NAMELAST="varchar(50)",
                            STREET1="varchar(100)",
                            STREET2="varchar(100)",
                            CITY="varchar(200)",
                            STATE="varchar(20)",
                            ZIP="varchar(10)",
                            PHONEHOME="varchar(100)",
                            PHONECELL="varchar(100)",
                            PHONEWORK="varchar(100)",
                            PHONEEXT="varchar(15)",
                            EMAIL="varchar(100)",
                            DOB="date",
                            SSN="varchar(11)",
                            DLNUMBER="varchar(500)",
                            DLSTATE="varchar(20)",
                            WORKNAME="varchar(100)",
                            WORKSTREET1="varchar(100)",
                            WORKSTREET2="varchar(100)",
                            WORKCITY="varchar(200)",
                            WORKSTATE="varchar(20)",
                            WORKZIP="varchar(10)",
                            PAYPERIOD="varchar(15)",
                            MONTHLYINCOME="decimal(10,2)",
                            BANKNAME="varchar(500)",
                            BANKABA="varchar(9)",
                            BANKACCTNUMBER="varchar(20)",
                            LoanID_Request="int",
                            ApplicantID_Request="int",
                            OwnsHome="varchar(5)",
                            MONTHSATADDRESS="decimal(10,2)",
                            BANKACCOUNTTYPE="varchar(20)",
                            EMPLOYERADDRESS="varchar(50)",
                            MONTHSATCURRENTEMPLOYER="decimal(10,2)",
                            DATEOFNEXTPAYDAY="date",
                            CrystalBallQualifiedAmount="decimal(10,2)",
                            CrystalBallRepeatCustomer="varchar(3)",
                            CrystalBallMaxLimit="decimal(10,2)",
                            CrystalBallRegionName="varchar(20)",
                            CrystalBallProductCategory="varchar(20)",
                            CrystalBallDealerRank="decimal(10,2)",
                            CrystalBallDealerLoanLimit="decimal(10,2)",
                            CrystalBallFundingMatrixCategoryId="int",
                            CrystalBallDealerID="int",
                            CrystalBallCashCollectionCurve="decimal(10,2)",
                            NewRetailerBIOverride="decimal(10,2)",
                            RetailerResetBIScore="decimal(10,2)")
        
        
        
        sqlSave(cfsql02_CrestAnalytics, finalRequestXML, rownames = FALSE, varTypes = columnTypes)
        script <- "INSERT INTO [CrestAnalytics].[dbo].[RequestXML_Shred] SELECT * FROM [CrestAnalytics].[CRESTFINANCIAL\\dhinkson].[finalRequestXML]"
        sqlQuery(cfsql02_CrestAnalytics, script)
        script <- "drop table [CrestAnalytics].[CRESTFINANCIAL\\dhinkson].[finalRequestXML]"
        sqlQuery(cfsql02_CrestAnalytics, script)
        
    }
}
#
#get email function
setwd("E:/Analytics/RScripts/DataProviderAnalysis")
source("SendInformationalEmail_Function.R",echo = T)
setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")

#format email and send
emailAddress <- "dhinkson@crestfinancial.com"
emailBody <- ""
emailDescription <- "RequestXML Shred Complete!"
sendInformationalEmail(emailDescription,emailBody,emailAddress)