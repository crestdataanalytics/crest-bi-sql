library(lubridate)
library(data.table)
library(fasttime)
library(plyr)
library(dplyr)
library(RODBC)
library(gmailr)
library(stringr)
#CoreLogic was run 12/16/2016 through 1/15/2017

#get leases
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("Leases_OpenMostRecent.R",echo = T)


#read in xml shred files
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    xmlDec <- data.frame(fread("XMLShred_Data_Dec_.csv", stringsAsFactors = F))
    xmlJan <- data.frame(fread("XMLShred_Data_Jan_.csv", stringsAsFactors = F))
    xmlShred <- rbind(xmlDec,xmlJan)
    rm(xmlOct,xmlNov,xmlDec,xmlJan)
    gc()
    # table(xmlShred$ApprovalProviderSettingsID)
    xmlShred$CreatedDate <- fastPOSIXct(xmlShred$CreatedDate)
    xmlShred <- xmlShred[xmlShred$CreatedDate >= "2016-12-16" & xmlShred$CreatedDate < "2017-01-16",]
    gc()
    table(xmlShred$ApprovalProviderSettingsID)

#get leases created 12-16-2016 to 1-15-2017 and 45 days out from first due date
    templeases <- leases[!is.na(leases$FundedPastFirstDueDate) & leases$FundedPastFirstDueDate == 1 & leases$CreationDate >= "2016-12-16" & 
                             leases$CreationDate <= "2017-01-15",]
    templeases$Days <- as.numeric(difftime(Sys.Date(),templeases$FirstDueDate, units = c("days")))
    summary(templeases$Days)
    templeases <- templeases[templeases$Days >= 45,]
    templeases$row.number <- F
    templeases <- templeases[order(templeases$ApplicantID,templeases$ID,decreasing = F),]
    templeases <- subset(templeases,subset = !duplicated(templeases[c("ApplicantID")]))

#set 45/60/90 dnp flags
    templeases$NoPay_45 <- 0
    templeases$NoPay_45[templeases$DaysToPayment >= 45] <- 1
    table(templeases$NoPay_45)

#subset the shredded data for qualifying funded leases
    keepAPRLIDs <- unique(xmlShred[,c("ApplicantID","ApprovalProviderResponseLogID","ApprovalProviderSettingsID")])
    keepAPRLIDs$row.number <- F
    keepAPRLIDs <- keepAPRLIDs[order(keepAPRLIDs$ApplicantID,keepAPRLIDs$ApprovalProviderResponseLogID,keepAPRLIDs$ApprovalProviderSettingsID,decreasing = T),]
    keepAPRLIDs <- subset(keepAPRLIDs,subset = !duplicated(keepAPRLIDs[c("ApplicantID","ApprovalProviderSettingsID")]))
    keepAPRLIDs <- keepAPRLIDs$ApprovalProviderResponseLogID
    gc()
    xmlShred <- xmlShred[xmlShred$ApprovalProviderResponseLogID %in% keepAPRLIDs,]
    keepApplicantIDs <- templeases$ApplicantID
    xmlShred <- xmlShred[xmlShred$ApplicantID %in% keepApplicantIDs,]
    gc()
    table(xmlShred$ApprovalProviderSettingsID)

#normalize shredded data
    #get unique xpaths and ids to append
    uniqueXPathIDs <- unique(xmlShred$XPathID)

#get xpaths
    # get all XPathIDs into 1 string
    for(i in uniqueXPathIDs){
        if(exists("xpathIDs_String")){
            xpathIDs_String <- paste0(xpathIDs_String,",",i)
        }else{
            xpathIDs_String <- i
        }
    }

#pull data
    cfsql02_CrestAnalytics <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestAnalytics;trusted_connection=true')
    script <- "SELECT XPath,XPathID FROM CrestAnalytics.dbo.XMLShred_XPaths WHERE XPathID IN ("
    script <- paste0(script,xpathIDs_String,")")
    xpathDB <- sqlQuery(cfsql02_CrestAnalytics, script, stringsAsFactors = F)

#subset the CL xpaths
    clXPaths <- xpathDB[grepl("GetDataResponse",xpathDB$XPath),]
    xpathDB <- xpathDB[xpathDB$XPathID %in% clXPaths$XPathID == F,]
    clXPaths <- clXPaths[grepl("TransactionResponse",clXPaths$XPath),]
    clXPaths <- clXPaths[grepl("Subscriber",clXPaths$XPath) == F,]
    clXPaths <- clXPaths[grepl("RequestDetails",clXPaths$XPath) == F,]  
    xpathDB <- rbind(xpathDB,clXPaths)

#remove unneccessary xpaths
    xpathDB <- xpathDB[grepl("_Input_",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("_CRAResponse_Consumer_",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("GenerationTime",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("CodeVersion",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("RequestVersion",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("TransactionId",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("TrackHash",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("TrackId",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("transaction_input_",xpathDB$XPath) == F,]
    xpathDB <- xpathDB[grepl("transaction_status_",xpathDB$XPath) == F,]

#filter out "text for datax
    tempxpathDB <- xpathDB
    tempxpathDB <- tempxpathDB[grepl("Response",tempxpathDB$XPath),]
    tempxpathDB <- tempxpathDB[grepl("_text",tempxpathDB$XPath),]
    xpathDB <- xpathDB[xpathDB$XPathID %in% tempxpathDB$XPathID == F,]




#loop through xpathIDs to do this
    uniqueXPathIDs <- unique(xpathDB$XPathID)
    finalNormalizedXML <- data.frame(unique(xmlShred$ApplicantID),stringsAsFactors = F)
    colnames(finalNormalizedXML)[1] <- "ApplicantID"
    count <- 0
    # uniqueXPathIDs <- uniqueXPathIDs[uniqueXPathIDs %in% c(5) == F]
    
    
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("ProcessAndNormalizeXMLShred.R",echo = T)
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    finalNormalizedXML <- ProcessAndNormalizeXMLShred(uniqueXPathIDs,finalNormalizedXML)
    
    
    
    # #audit
    #     names(finalNormalizedXML)[grepl("Concatenated",names(finalNormalizedXML))]
    #     xpathDB$XPath[xpathDB$XPathID == 554]
    #     head(finalNormalizedXML$ApplicantID[finalNormalizedXML$XPath554_Concatenated != "NA"])
    #     head(finalNormalizedXML$XPath554_Concatenated[finalNormalizedXML$XPath554_Concatenated != "NA"])
    #     names(finalNormalizedXML)[grepl("XPath233",names(finalNormalizedXML))]
    #     finalNormalizedXML$ApplicantID[!is.na(finalNormalizedXML$XPath77_VarValue_29)]

###ADD THE APRLID on each applicant for each bureau at the end so we can tie back
    APRLID <- unique(xmlShred[,c("ApplicantID","ApprovalProviderSettingsID","ApprovalProviderResponseLogID")])

#dx
    dx <- APRLID[APRLID$ApprovalProviderSettingsID == 2,]
    dx$ApprovalProviderSettingsID <- NULL
    names(dx)[names(dx) == "ApprovalProviderResponseLogID"] <- "DataX_ApprovalProviderResponseLogID"
    finalNormalizedXML <- merge(finalNormalizedXML,dx,by = c("ApplicantID"),all.x = T)

#eb
    eb <- APRLID[APRLID$ApprovalProviderSettingsID == 6,]
    eb$ApprovalProviderSettingsID <- NULL
    names(eb)[names(eb) == "ApprovalProviderResponseLogID"] <- "eBureau_ApprovalProviderResponseLogID"
    finalNormalizedXML <- merge(finalNormalizedXML,eb,by = c("ApplicantID"),all.x = T)

#cl
    cl <- APRLID[APRLID$ApprovalProviderSettingsID == 8,]
    cl$ApprovalProviderSettingsID <- NULL
    names(cl)[names(cl) == "ApprovalProviderResponseLogID"] <- "CoreLogic_ApprovalProviderResponseLogID"
    finalNormalizedXML <- merge(finalNormalizedXML,cl,by = c("ApplicantID"),all.x = T)

rm(dx,eb,cl,APRLID)
gc()

###ADD State, zipcode
#get data from production
#GET APRLID FOR THE EBUREAU PULLS
    ebAPRLIDs <- unique(xmlShred$ApprovalProviderResponseLogID[xmlShred$ApprovalProviderSettingsID == 2])
    
    for(i in ebAPRLIDs){
        if(exists("ebAPRLIDs_String")){
            ebAPRLIDs_String <- paste0(ebAPRLIDs_String,",",i)
        }else{
            ebAPRLIDs_String <- i
        }
    }
    
    cfsql02_CrestWarehouse <- odbcDriverConnect('driver={SQL Server};server=cfsql02;database=CrestWarehouse;trusted_connection=true')
    script <- "SELECT RequestXML, ID AS ApprovalProviderResponseLogID, ApplicantID FROM CrestWarehouse.dbo.ApprovalProviderResponseLog WHERE ID IN ("
    script <- paste0(script,ebAPRLIDs_String,")")
    requestXML <- sqlQuery(cfsql02_CrestWarehouse, script, stringsAsFactors = F)
    # requestXML <- requestXML[requestXML$ApprovalProviderResponseLogID %in% unique(xmlShred$ApprovalProviderResponseLogID),]
    requestXML$Zip <- as.numeric(sub("</zip>.*","",sub("^.*<zip>","",requestXML$RequestXML)))
    requestXML$DOB <- sub("</dob>.*","",sub("^.*<dob>","",requestXML$RequestXML))
    requestXML$DOB[grepl("<",requestXML$DOB)] <- ""
    requestXML$DOB_Year <- as.numeric(substr(requestXML$DOB,1,4))
    requestXML$DOB_Year[is.na(requestXML$DOB_Year)] <- 0
    requestXML$DOB_Month <- as.numeric(substr(requestXML$DOB,5,6))
    requestXML$DOB_Month[is.na(requestXML$DOB_Month)] <- 0
    requestXML$PayFrequency <- sub("</interval>.*","",sub("^.*<interval>","",requestXML$RequestXML))
    requestXML$PayFrequency[grepl("<",requestXML$PayFrequency)] <- ""
    requestXML$MonthlyIncome <- as.numeric(sub("</pay>.*","",sub("^.*<pay>","",requestXML$RequestXML)))
    requestXML$WorkName <- sub("</company>.*","",sub("^.*<company>","",requestXML$RequestXML))
    requestXML$WorkName[grepl("<",requestXML$WorkName)] <- ""
    requestXML$WorkName <- gsub(" ","",requestXML$WorkName)
    requestXML$WorkName <- tolower(requestXML$WorkName)
    requestXML$WorkName[is.na(requestXML$WorkName)] <- "Missing"
    requestXML$RequestXML <- NULL
    requestXML$ApprovalProviderResponseLogID <- NULL
    requestXML$DOB <- NULL
    finalNormalizedXML <- merge(finalNormalizedXML,requestXML,by = c("ApplicantID"),all.x = T)
    finalNormalizedXML <- finalNormalizedXML[!is.na(finalNormalizedXML$DOB_Year),]
    # table(is.na(finalNormalizedXML$Zip))
    rm(requestXML)
    gc()

#FT
    ft <- data.frame(fread("FactorTrust_FT_to_Crest_Data.csv", stringsAsFactors = F,colClasses = c("character")))
    head(ft$applicantid)
    names(ft)[names(ft) == "applicantid"] <- "ApplicantID"
    ft$ApplicantID <- as.numeric(ft$ApplicantID)
    ft <- ft[ft$ApplicantID %in% keepApplicantIDs,]
    names(ft)[names(ft) %in% names(finalNormalizedXML)]
    finalNormalizedXML <- merge(finalNormalizedXML,ft,by = c("ApplicantID"),all.x = T)

#retailer info
#cash collection curve
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("GetCashCollectionCurve.R",echo = T)
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    ccc <- getCashCollectionCurve(templeases[,c("ID","RetailerID","CreationDate")])
    ccc <- ccc[,c("ID","CashCollectionCurve")]
    templeases <- merge(templeases,ccc,by = c("ID"),all.x = T)

#BIScore
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("GetBIScore.R",echo = T)
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    biscore <- getBIScore(templeases[,c("ID","RetailerID","CreationDate")])
    biscore <- biscore[,c("ID","BIScore")]
    templeases <- merge(templeases,biscore,by = c("ID"),all.x = T)
    
#get retailer fraud score
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("GetFraudScore_Retailer_Results.R",echo = T)
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    retailerFraudScore <- getFraudScore_Retailers_Retro_Results(min(templeases$CreationDate,na.rm = T),max(templeases$CreationDate,na.rm = T))
    retailerFraudScore$CreationDate <- as.POSIXct(retailerFraudScore$CreationDate)
    names(retailerFraudScore)[names(retailerFraudScore) == "FraudScore"] <- "RetailerFraudScore"
    templeases <- merge(templeases,retailerFraudScore,by = c("RetailerID","CreationDate"),all.x = T)

#day of week and hour of day
    templeases$DayOfWeek <- weekdays(templeases$CreationDate)
    table(is.na(templeases$DayOfWeek))

#merge
    finalNormalizedXML <- merge(templeases[,c("ID","ApplicantID","NoPay_45","BIScore","CashCollectionCurve","DayOfWeek","RetailerFraudScore")],
                                finalNormalizedXML,by = c("ApplicantID"),all.y = T)
    
#repeat customers
    rc <- leases[,c("ID","FirstPaidLeaseIndicator","SubsequentLeaseIndicator","ReturningCustomerIndicator")]
    rc <- rc[is.na(rc$FirstPaidLeaseIndicator) & !is.na(rc$SubsequentLeaseIndicator) & rc$SubsequentLeaseIndicator == 1,]
    rc <- rc[,c("ID","ReturningCustomerIndicator")]
    finalNormalizedXML <- merge(finalNormalizedXML,rc, by = c("ID"),all.x = T)

#take care of "T" in CL time stamp
    clxpathid <- xpathDB$XPathID[xpathDB$XPath == "GetDataResponse_TransactionResponse_ConsumerCreditReport_PreviousInquiries_Inquiry_InquiryDateTime"]
    clxpathidcolnames <- names(finalNormalizedXML)[grepl(paste0("XPath",clxpathid),names(finalNormalizedXML))]
    for(i in clxpathidcolnames){
        # i <- clxpathidcolnames[1]
        names(finalNormalizedXML)[names(finalNormalizedXML) == i] <- "Var"
        # finalNormalizedXML$Var <- gsub("T"," ",finalNormalizedXML$Var)
        finalNormalizedXML$Var <- as.POSIXct(substr(finalNormalizedXML$Var,1,10),format = "%Y-%m-%d")
        names(finalNormalizedXML)[names(finalNormalizedXML) == "Var"] <- i
    }
    
    finalNormalizedXML <- finalNormalizedXML[,names(finalNormalizedXML)[grepl("XPath1642",names(finalNormalizedXML)) == F]]
    finalNormalizedXML <- finalNormalizedXML[,names(finalNormalizedXML)[grepl("XPath1623",names(finalNormalizedXML)) == F]]
    
#remove concatenated fields for setCountNotVarColumns
    



writefile <- data.frame(lapply(finalNormalizedXML, as.character), stringsAsFactors = F)
writefile[writefile == "NA"] <- ""
writefile[is.na(writefile)] <- ""
writefile[writefile == "NULL"] <- ""


head(finalNormalizedXML$XPath1642_3[!is.na(finalNormalizedXML$XPath1642_3)])
###WRITE DICTIONARY FOR XPATHS AND COLNAMES
#write file
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    write.csv(writefile,paste0("DataRobot_45DayNoPay_EB_DX_FT_CL_",month(Sys.Date()),day(Sys.Date()),year(Sys.Date()),".csv")
              ,quote = T,row.names = F)
    write.csv(xpathDB,paste0("DataRobot_45DayNoPay_EB_DX_FT_CL_XPathDataDictionary_",month(Sys.Date()),day(Sys.Date()),year(Sys.Date()),".csv"),
              quote = T,row.names = F)


# # read in the file
#     setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
#     finalNormalizedXML <- data.frame(fread("DataRobot_45DayNoPay_EB_DX_FT_CL_4142017.csv", stringsAsFactors = F))
#     xpathDB <- data.frame(fread("DataRobot_45DayNoPay_EB_DX_FT_CL_XPathDataDictionary_472017.csv", stringsAsFactors = F))
# 
#     xpathID <- 382
#     concatenatedName <- paste0("XPath",xpathID,"_Concatenated")
#     head(finalNormalizedXML[,names(finalNormalizedXML)[grepl(concatenatedName,names(finalNormalizedXML))]])
#     head(finalNormalizedXML[,names(finalNormalizedXML)[grepl(concatenatedName,names(finalNormalizedXML)) & grepl(",",names(finalNormalizedXML))]])
#     concatenatedName <- paste0("XPath",xpathID,"_ConcatenatedID")
#     length(unique(finalNormalizedXML[,names(finalNormalizedXML)[grepl(concatenatedName,names(finalNormalizedXML))]]))
#     xpathDB$XPath[xpathDB$XPathID == xpathID]
    
    # #read file for retro retailer fraud score
    # setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    # fraudScore <- data.frame(fread("FraudScore_Retailers_Retro.csv", stringsAsFactors = F))
    # fraudScore <- fraudScore[fraudScore$FraudScore > 0 & !is.na(fraudScore$FraudScore),]
    # length(unique(fraudScore$RetailerID))
    # check <- finalNormalizedXML[!is.na(finalNormalizedXML$RetailerFraudScore) & finalNormalizedXML$RetailerFraudScore > 0,]

head(finalNormalizedXML$XPath289_Concatenated)

library(gmailr)
#get email function
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("SendInformationalEmail_Function.R",echo = T)
    setwd("E:/CSVFiles/DataProviderAnalysis/Read_Files/CoreReadFiles")

#format email and send
    emailAddress <- "dhinkson@crestfinancial.com"
    emailBody <- ""
    emailDescription <- paste0("DataRobot_45DayNoPay_EB_DX_FT_CL_XPathDataDictionary_",month(Sys.Date()),day(Sys.Date()),year(Sys.Date())," FINISHED")
    sendInformationalEmail(emailDescription,emailBody,emailAddress)
    
    
#upload file to dataRobot
    setwd("E:/Analytics/RScripts/DataProviderAnalysis")
    source("DataRobot_UploadFileToDataRobotAndNotify.R",echo = T)
    setwd("//cfsisense/Analytics/OutputFiles/Daniel/MasterFile/Write_Files")
    
    UploadFileToDataRobotAndNotify(datafile = writefile,theProjectName = paste0("DataRobot_45DayNoPay_EB_DX_FT_CL_",month(Sys.Date()),day(Sys.Date()),
                                                                                year(Sys.Date()),".csv"))

















