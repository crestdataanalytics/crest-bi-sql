#we want to see what FPD is on CBB scores between 735 and 765 ... pre and post clarity gate ... and total

lpV2Leases$CreationDate <- as.POSIXct(lpV2Leases$OriginalCreationDate)

theDueDate <- as.POSIXct(max(lpV2Leases$CreationDate))
day(theDueDate) <- day(theDueDate) - 7

#get CBB scores and BAV codes and leases that were funded/created after 12/5/2015 who have a first due date and enough time for a payment to have cleared
    cbb <- data.frame(fread("CBBScores.csv", stringsAsFactors = F))
    colnames(cbb)[1] <- "DocumentID"
    cbb$GenerationDate <- as.Date(cbb$GenerationDate)
    
    #get ids with docids
        setwd("F:/CSVFiles/DataProviderAnalysis/DFsFromR")
        docIDs <- data.frame(fread("lpV2LeasesDocumentIDs.csv",stringsAsFactors = F))
        setwd("F:/CSVFiles/DataProviderAnalysis")
        docIDs <- docIDs[,c("ID","DocumentID")]
        gc()
        cbb <- merge(cbb,docIDs,by = c("DocumentID"),all.x = T)
        table(is.na(cbb$ID))
        cbb <- cbb[!is.na(cbb$ID),]
        cbb$row.number <- F
        cbb <- cbb[order(cbb$ID,cbb$GenerationDate,decreasing = T),]
        cbb <- subset(cbb, subset = !duplicated(cbb[c("ID")]))
        rm(docIDs)
        gc()
    
    cbb <- cbb[,c("ID","VariableValue")]
    colnames(cbb)[2] <- "cbbScore"
    
    leases <- lpV2Leases
    leases <- leases[leases$CreationDate >= "2016-04-01",]
    leases <- leases[!is.na(leases$FundedDate),]
    table(leases$Status)
    leases <- leases[leases$Status %in% c("DealerReturn","Denied") == F,]
    leases$FirstDueDate <- as.Date(leases$FirstDueDate)
    leases <- leases[!is.na(leases$FirstDueDate) & leases$FirstDueDate <= theDueDate,]
    leases$Vintage <- as.Date(leases$CreationDate)
    day(leases$Vintage) <- 1
    leases <- merge(leases,cbb,by = c("ID"))
    leases <- leases[leases$cbbScore <= 765 & leases$cbbScore >= 735,]
    gc()
    
    
#get performance
    #total
        total <- aggregate(ID ~ Vintage, data = leases, FUN = "length")
        colnames(total)[2] <- "TotalFunded"
        
    #total cash
        totalCash <- aggregate(MerchantPaidAmount ~ Vintage, data = leases, FUN = "sum")
        colnames(totalCash)[2] <- "TotalFundedCash"
        total <- merge(total,totalCash,by = c("Vintage"),all.x = T)
        
    #total fpd
        fpd <- leases[!is.na(leases$FirstPaymentDefault) & leases$FirstPaymentDefault == 1,]
        totalFPD <- aggregate(ID ~ Vintage, data = fpd, FUN = "length")
        colnames(totalFPD)[2] <- "TotalFPD"
        total <- merge(total,totalFPD,by = c("Vintage"),all.x = T)
        
    #total cash
        totalFPDCash <- aggregate(MerchantPaidAmount ~ Vintage, data = fpd, FUN = "sum")
        colnames(totalFPDCash)[2] <- "TotalFPDCash"
        total <- merge(total,totalFPDCash,by = c("Vintage"),all.x = T)
        
    #percents
        total$FPDPercent <- total$TotalFPD / total$TotalFunded * 100
        total$FPDCashPercent <- total$TotalFPDCash / total$TotalFundedCash * 100
        