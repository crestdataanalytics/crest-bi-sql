

setwd("F:/CSVFiles/DataXDataAppend")

closedAccounts <- read.csv("closedAccounts.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
colnames(closedAccounts)[1] <- "ID"
closedAccounts$OriginationDate <- as.Date(closedAccounts$OriginationDate, format = "%Y-%m-%d")
closedAccounts$PaidOffDate <- as.Date(closedAccounts$PaidOffDate, format = "%Y-%m-%d")
closedAccounts$ChargedOffDate <- as.Date(closedAccounts$ChargedOffDate, format = "%Y-%m-%d")
closedAccounts$InvoiceAmount <- as.numeric(closedAccounts$InvoiceAmount)
closedAccounts$IsPaid <- as.numeric(closedAccounts$IsPaid)
closedAccounts$IsChargedOff <- as.numeric(closedAccounts$IsChargedOff)
closedAccounts$DealerID <- as.numeric(closedAccounts$DealerID)


#Format the payments df
{
        #get payments
        payments <- read.csv("payments.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
        colnames(payments)[1] <- "RowNumber"
        payments$Amount <- as.numeric(payments$Amount)
        payments$PaymentDate <- as.Date(payments$PaymentDate, format = "%Y-%m-%d")
        payments$ReturnDate <- as.Date(payments$ReturnDate, format = "%Y-%m-%d")
        payments$IsReturned <- as.numeric(payments$IsReturned)
        payments$FirstPaymentFatalReturnIndicator <- as.numeric(payments$FirstPaymentFatalReturnIndicator)
        payments$ID <- as.numeric(payments$ID)
        
        #get count of first payments made
        firstPayment <- subset(payments, subset = payments$RowNumber == "1")
        firstPayment$Month <- format(firstPayment$PaymentDate, "%m")
        firstPayment$Year <- format(firstPayment$PaymentDate, "%Y")
        firstPaymentAgg <- aggregate(ID~Year+Month, data = firstPayment, FUN = "length")
        firstPaymentAgg <- firstPaymentAgg[order(firstPaymentAgg$Year, firstPaymentAgg$Month),]
        firstPaymentAgg <- data.frame(firstPaymentAgg, row.number = F)
        colnames(firstPaymentAgg)[3] <- "TotalFirstPayment"
        
        
        #get the loan payment schedule
        loanPaymentSch <- read.csv("loanPaymentSchedule.csv", stringsAsFactors=F, colClasses = c("character"), quote = "", row.names = NULL)
        colnames(loanPaymentSch)[1] <- "RowNumber"
        loanPaymentSch$ID <- as.numeric(loanPaymentSch$ID)
        loanPaymentSch$RowNumber <- as.numeric(loanPaymentSch$RowNumber)
        loanPaymentSch$ScheduleEntryDesc <- as.factor(loanPaymentSch$ScheduleEntryDesc)
        loanPaymentSch$DueAmount <- as.numeric(loanPaymentSch$DueAmount)
        loanPaymentSch$DueDate <- as.Date(loanPaymentSch$DueDate, format = "%Y-%m-%d")
        loanPaymentSch$AmountPaid <- as.numeric(loanPaymentSch$AmountPaid)
        loanPaymentSch$PaidStatus <- as.factor(loanPaymentSch$PaidStatus)        
        
        #min(payments$PaymentDate)
        #firstPayment$ID[firstPayment$Month == "10" & firstPayment$Year == "2009" & firstPayment$IsReturned == 1]
        
        #get first draft from loan payment schedule
        loanPaymentSchFirst <- subset(loanPaymentSch, subset = loanPaymentSch$RowNumber == 1)
        
        lpsFPmerge <- merge(x = loanPaymentSchFirst, y = firstPayment, by = c("ID"), all.x = T)
        lpsFPmerge <- lpsFPmerge[lpsFPmerge$DueDate <= Sys.Date(),]
        lpsFPmerge$IsFPD <- 0
        lpsFPmerge$IsFPD[is.na(lpsFPmerge$PaymentDate) & lpsFPmerge$DueDate <= Sys.Date()] <- 1       
        sum(lpsFPmerge$IsFPD, na.rm = T)
        lpsFPmerge$IsFPD[(lpsFPmerge$PaymentDate - lpsFPmerge$DueDate) >= 5] <- 1
        
        min(lpsFPmerge$DueDate[lpsFPmerge$IsFPD == 1])
        max(lpsFPmerge$DueDate[lpsFPmerge$IsFPD == 1])
        x <- lpsFPmerge[lpsFPmerge$IsFPD ==1 ,]
        x <- x[order(x$DueDate, decreasing = T),]
        
        #get second payments made
        secondPayment <- subset(payments, subset = payments$RowNumber == "2")        
        
        firstPaymentReturned <- firstPayment[firstPayment$IsReturned == 1,]
        firstPaymentReturnedAgg <- aggregate(ID~Year+Month, data = firstPaymentReturned, FUN = "length")
        colnames(firstPaymentReturnedAgg)[3] <- "TotalFirstPaymentReturned"
        firstPaymentReturnedAgg <- firstPaymentReturnedAgg[order(firstPaymentReturnedAgg$Year, firstPaymentReturnedAgg$Month),]
        firstPaymentReturnedAgg <- data.frame(firstPaymentReturnedAgg, row.number = F)
        
        FPD <- merge(x = firstPaymentReturned, y = secondPayment, by = c("ID"), all.x = T)
        FPD$IsFPD <- 1
        FPD$IsFPD[FPD$PaymentDate.y - FPD$PaymentDate.x <= 2] <- 0
        FPD <- FPD[, -which(names(FPD) %in%
                                    c("RowNumber.y",
                                      "Amount.y",
                                      "PaymentDate.y",
                                      "PaymentID.y",
                                      "ReturnDate.y",
                                      "ReturnCode.y",
                                      "PaymentID.y",
                                      "IsReturned.y",
                                      "FirstPaymentFatalReturnIndicator.y"))]
        FPD <- FPD[FPD$IsFPD == 1,]
        FPDAgg <- aggregate(ID~Year+Month, data = FPD, FUN = "length")
        colnames(FPDAgg)[3] <- "FPD"
        
        firstPaymentMerge <- merge(x = firstPaymentAgg, y = firstPaymentReturnedAgg, by = c("Year","Month"), all.x = T)
        firstPaymentMerge <- merge(x = firstPaymentMerge, y = FPDAgg, by = c("Year","Month"), all.x = T)
        firstPaymentMerge[4] <- NULL
        firstPaymentMerge[5] <- NULL
        firstPaymentMerge$TotalFirstPaymentReturned[is.na(firstPaymentMerge$TotalFirstPaymentReturned)] <- 0
        firstPaymentMerge <- data.frame(firstPaymentMerge,row.number = F)
        
        #length(FPD$ID) / length(firstPayment$ID)
        
        #write.csv(firstPaymentMerge, file = "FirstPaymentsByMonth.csv", quote = T, row.names = F)
        
        
}

#Get the total payments made

{
        paymentsNotReturned <- payments[payments$IsReturned != "1",]
        totalPaymentsMade <- aggregate(Amount~ID, data = paymentsNotReturned, FUN = "sum")
        colnames(totalPaymentsMade)[2] <- "CumulativeAmountCollected"
}

df <- closedAccounts
df$PaidOffDate[df$IsPaid == 0] <- NA
df$ChargedOffDate[df$IsPaid == 1] <- NA

#this gets the CumulativeAmountCollected amount in df
{
        df$CumulativeAmountCollected <- NULL
        df <- merge(x = df, y = totalPaymentsMade, by = "ID", all.x = T)
        df$CumulativeAmountCollected[is.na(df$CumulativeAmountCollected)] <- 0
}


df$LeaseMultiplier <- 0
df$LeaseMultiplier <- df$CumulativeAmountCollected / df$InvoiceAmount

df <- subset(df, subset = df$OriginationDate <= as.Date("07/31/2014", format = "%m/%d/%Y"))

dealersPaidIn <- aggregate(CumulativeAmountCollected~DealerID, FUN = "sum", data = df)
colnames(dealersPaidIn)[2] <- "PaidIn"
dealersPaidOut <- aggregate(InvoiceAmount~DealerID, FUN = "sum", data = df)
colnames(dealersPaidOut)[2] <- "PaidOut"

dealers <- merge(x = dealersPaidIn, y = dealersPaidOut, by = "DealerID")

dealers$LeaseMultiplier <- 0
dealers$LeaseMultiplier <- dealers$PaidIn / dealers$PaidOut

 test <- dealers[dealers$DealerID == 3261 |
                                dealers$DealerID == 6108 |
                                dealers$DealerID == 2056 |
                                dealers$DealerID == 8198 |
                                dealers$DealerID == 1771 |
                                dealers$DealerID == 3711 |
                                dealers$DealerID == 3970 |
                                dealers$DealerID == 4780 |
                                dealers$DealerID == 6089 |
                                dealers$DealerID == 3626,]


#format and get APR >= 0
{
        df$PercentPaid <- (df$CumulativeAmountCollected - df$InvoiceAmount) / df$InvoiceAmount
        df$ChargedOffDate[!is.na(df$ChargedOffDate) & df$IsPaid == 1] <- NA
        df$PaidOffDate[!is.na(df$PaidOffDate) & df$IsChargedOff == 1] <- NA
        df$PaidOffDate[!is.na(df$PaidOffDate) & df$IsBankrupt == 1] <- NA
        df$PaidOffDate[!is.na(df$PaidOffDate) & df$IsReturned == 1] <- NA
        df <- subset(df, subset = df$PercentPaid >= 0)
        df$MonthsOpen <- 0
        #df$PercentPaid <- NULL
}

#sanity checks
{
        length(df$ID[df$IsPaid == 1 & is.na(df$PaidOffDate)])
        length(df$ID[df$IsPaid == 0 & !is.na(df$PaidOffDate)])
        length(df$ID[df$IsChargedOff == 1 & !is.na(df$PaidOffDate)])
        length(df$ID[df$IsReturned == 1 & !is.na(df$PaidOffDate)])
        length(df$ID[df$IsBankrupt == 1 & !is.na(df$PaidOffDate)])
        length(df$ID[(df$IsChargedOff == 1 | df$IsBankrupt == 1 | df$IsReturned == 1) & is.na(df$ChargedOffDate)])
        length(df$ID[(df$IsChargedOff == 0 & df$IsBankrupt == 0 & df$IsReturned == 0) & !is.na(df$ChargedOffDate)])
        length(unique(df$ID))
}


write.csv(df, file = "ClosedAccountsAppend.csv", quote = T, row.names = F)


APRTotal <- sum(df$PercentPaid, na.rm = T) / length(df$ID)

#All paid accounts
dfPaid <- subset (df, subset = df$IsPaid == 1)
dfPaid$MonthsOpen <- (dfPaid$PaidOffDate - dfPaid$OriginationDate) * 12 / 365
APRPaid <- sum(dfPaid$PercentPaid, na.rm = T) / length(dfPaid$ID)

# length(dfPaid$ID[dfPaid$IsPaid == 0])
# length(dfPaid$ID[dfPaid$IsChargedOff == 1])
# length(dfPaid$ID[dfPaid$IsBankrupt == 1])
# length(dfPaid$ID[dfPaid$IsReturned == 1])

#Paid full term
dfPaidFullTerm <- subset(dfPaid, subset = dfPaid$MonthsOpen >= 12)
APRPaidFullTerm <- sum(dfPaidFullTerm$PercentPaid, na.rm = T) / length(dfPaidFullTerm$ID)
APRPaidFullTerm <- sum(dfPaidFullTerm$PercentPaid, na.rm = T) / length(dfPaidFullTerm$ID)

# length(dfPaidFullTerm$ID[dfPaidFullTerm$IsPaid == 0])
# length(dfPaidFullTerm$ID[dfPaidFullTerm$IsChargedOff == 1])
# length(dfPaidFullTerm$ID[dfPaidFullTerm$IsBankrupt == 1])
# length(dfPaidFullTerm$ID[dfPaidFullTerm$IsReturned == 1])

#Paid 4-12 months
dfPaid4to12 <- subset(dfPaid, subset = dfPaid$MonthsOpen < 12 & dfPaid$MonthsOpen > 3)
APRPaid4to12 <- sum(dfPaid4to12$PercentPaid, na.rm = T) / length(dfPaid4to12$ID)
APRPaid4to12 <- sum(dfPaid4to12$PercentPaid, na.rm = T) / length(dfPaid4to12$ID)

# length(dfPaid4to12$ID[dfPaid4to12$IsPaid == 0])
# length(dfPaid4to12$ID[dfPaid4to12$IsChargedOff == 1])
# length(dfPaid4to12$ID[dfPaid4to12$IsBankrupt == 1])
# length(dfPaid4to12$ID[dfPaid4to12$IsReturned == 1])

#Paid 90 Days
dfPaidEarly <- subset(dfPaid, subset = dfPaid$MonthsOpen <= 3)
APRPaidEarly <- sum(dfPaidEarly$PercentPaid, na.rm = T) / length(dfPaidEarly$ID)
APRPaidEarly <- sum(dfPaidEarly$PercentPaid, na.rm = T) / length(dfPaidEarly$ID)

# length(dfPaidEarly$ID[dfPaidEarly$IsPaid == 0])
# length(dfPaidEarly$ID[dfPaidEarly$IsChargedOff == 1])
# length(dfPaidEarly$ID[dfPaidEarly$IsBankrupt == 1])
# length(dfPaidEarly$ID[dfPaidEarly$IsReturned == 1])

#Returned
dfReturned <- subset(df, subset = df$IsReturned == 1)
APRReturned <- sum(dfReturned$PercentPaid, na.rm = T) / length(dfReturned$ID)
APRReturned <- sum(dfReturned$PercentPaid, na.rm = T) / length(dfReturned$ID)

# length(dfReturned$ID[dfReturned$IsPaid == 1])
# length(dfReturned$ID[dfReturned$IsChargedOff == 1])
# length(dfReturned$ID[dfReturned$IsBankrupt == 1])
# length(dfReturned$ID[dfReturned$IsReturned == 0])

#ChargedOff
dfChargedOff <- subset(df, subset = df$IsChargedOff == 1 | df$IsBankrupt == 1)
APRChargedOff <- sum(dfChargedOff$PercentPaid, na.rm = T) / length(dfChargedOff$ID)
APRChargedOff <- sum(dfChargedOff$PercentPaid, na.rm = T) / length(dfChargedOff$ID)

# length(dfChargedOff$ID[dfChargedOff$IsPaid == 1])
# length(dfChargedOff$ID[dfChargedOff$IsChargedOff == 0])
# length(dfChargedOff$ID[dfChargedOff$IsBankrupt == 0])
# length(dfChargedOff$ID[dfChargedOff$IsReturned == 1])


