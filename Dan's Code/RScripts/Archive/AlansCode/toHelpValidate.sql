SET Transaction Isolation Level Read Uncommitted;
Declare @PayMonth int,
		@PayYear int,
		@FundMonth int,
		@FundYear int
SET @PayMonth = 4;
SET	@PayYear = 2009;
SET @FundMonth =2;
SET	@FundYear = 2009;

select sum(LoanPayments.amount)
from LoanPayments
inner join vw_loans_reporting on LoanPayments.LoanID = vw_loans_reporting.ID
inner join loanstatuses on loanstatuses.id = vw_loans_reporting.statusid
where	LoanPayments.returned = 0
		and LoanPayments.isdeleted = 0
		and month(LoanPayments.paymentdate) = @PayMonth
		and year(LoanPayments.paymentdate) = @PayYear
		and month(vw_loans_reporting.fundeddate) = @FundMonth
		and year(vw_loans_reporting.fundeddate) = @FundYear
		and loanstatuses.[status] not in ('Canceled','Cancelled')
		--and LoanPayments.effectivedate is not null
		--and vw_loans_reporting.DisplayID = '542718-1'

select	vw_loans_reporting.DisplayID as AgreementNumber,
		LoanPayments.LoanID,
		LoanPayments.amount as PaymentAmount,
		--LoanPayments.Returned,
		--LoanPayments.ReturnDate,
		--LoanPayments.IsDeleted,
		--LoanPayments.HasBeenReturned,
		--LoanPayments.ChargeBackDate,
		--LoanPayments.TransactionID,
		--LoanPayments.DeletedDate,
		cast(LoanPayments.PaymentDate as Date) as TransactionDate
from LoanPayments
inner join vw_loans_reporting on LoanPayments.LoanID = vw_loans_reporting.ID
inner join loanstatuses on loanstatuses.id = vw_loans_reporting.statusid
where	LoanPayments.returned = 0
		and LoanPayments.isdeleted = 0
		and month(LoanPayments.paymentdate) = @PayMonth
		and year(LoanPayments.paymentdate) = @PayYear
		and month(vw_loans_reporting.fundeddate) = @FundMonth
		and year(vw_loans_reporting.fundeddate) = @FundYear
		and loanstatuses.[status] not in ('Canceled','Cancelled')
		--and LoanPayments.commissionpercentage != 0-- is not null
		--and vw_loans_reporting.DisplayID = '542033-1'
