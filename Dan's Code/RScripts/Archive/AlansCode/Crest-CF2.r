#=========================================================================================================#
# CUSTOMER NAME:         Crest - Cash Flow Project (v1 and v2)
# DATA SET DATE(S):      04/17/15
# AUTHOR:                AKW
#
{
        # DIRECTIONS:            These 4 COMMON STEPS are required prior to performing ANY analyses (steps below)
        #                        --------------------------------------------------------------------------------
        #                        STEP #A: Initialize/setup code
        #                        STEP #B: Gather and merge data
        #                        STEP #C: Data transformation and feature engineering
        #                        STEP #D: Load data into dfA, look at date range
        #                        STEP #E: Declare common functions
        #
        #                        Task #1 STEPS
        #                        -------------
        #                        STEP #1A: Create matrices for payment allocation and % allocation
        #                        STEP #1B: Fill in tables based on time interval # of months
        #                        STEP #1C: Fill in tables going back to the beginning of time (All)
        #                        STEP #1D: Fill in tables for graphing
        #                        STEP #1E: Visualize time series results (build plots)
        #
        #                        Task #2 STEPS
        #                        -------------
        #                        STEP #2A: Transform data into days, weeks and months (aggregate observations - keep everything)
        #                        STEP #2B: Subset data by grabbing "windows" for further analysis
        #                        STEP #2C: Perform exploratory time series analysis, using transformations and models
        #                        STEP #2D: Cross validate incrementing a month at a time - forecast vs. actual for 6 month chunks
        #                             Run one of 2 options (depending on which dataset to create models from)
        #                             OPTION 1: Test ALL data ... Put dfMTS into tsX (and dfMTS.L into tsXL)
        #                             OPTION 2: Test data since 2012 ... Put dfMTS.12 into tsX (and dfMTS.12L into tsXL)
        #                        STEP #2E: Compare Mean Absolute Error (MAE) from full dataset and 2012 onward dataset
        #                        STEP #2F: Final model recommendations
        #
        #                        Task #3 STEPS
        #                        -------------
        #                        STEP #3A: Subset historical data
        #                        STEP #3B: Create matrices/dataframes and prepare data
        #                             Must set the "horiz" variable to match the corresponding plot in #3C
        #                                  Set to >= "20" for Fig 3.0 and Fig 3.2 (values < 20 will throw an Error)
        #                                  Set to "13" for Fig 3.1 (values > "13" will work, but un-readable)
        #                                  Fig 3.3 will work with any "horiz" value > 13
        #                        STEP #3C: Various plots
        #                        STEP #3D: Prep forecasting model data for lease payback
        #                        STEP #3E: Plot forecasting model based on monthly returns (include max and min)
        #                        STEP #3F: Plot forecasting model based on cumulative returns (include max and min)
        #                        STEP #3G: Now apply percentages to dollars for the ROI estimates
        #                        STEP #3H: Forecast lease payback based on initial (specified) investment
        #
        #                        Task #4 STEPS (This is a RE-DO of task #2 above...also using pieces of task #3)
        #                        -------------------------------------------------------------------------------
        #                        STEP #4A: Transform data into days, weeks and months (aggregate observations - keep everything)
        #                                  (Only difference is we're aggregating FundedAmount vs. PaymentAmount)
        #                        STEP #4B: Now build the tables where we'll store the final %'s (percSeasonal)
        #                        STEP #4C: Then, we'll take the % tables to determine forecast based on monthly FundedAmounts
        #                        STEP #4D: Subset data by grabbing "windows" for further analysis
        #                        STEP #4E: Apply various transformations
        #                        STEP #4F: Perform exploratory time series analysis, using transformations and models
        #                        STEP #4G: Cross validate FundedAmount (Probabilistic) model, incrementing a month at a time
        #                        STEP #4H: Cross validate COMBINED model, incrementing a month at a time - forecast vs. actual
        #                             NOTE: PRIOR TO RUNNING THIS STEP, YOU MUST RUN THE FOLLOWING SECTIONS: 
        #                                   2A, 2B, 2C, 3A, 3B, 3D and 3G
}
#=========================================================================================================#
#=========================================================================================================#
#    STEP A: INITIALIZE - LOAD LIBRARIES AND SPECIFY GLOBAL CONSTANTS
#=========================================================================================================#
# Assumes these packages are already installed - so just need to load them here

{
        library(lubridate)
        library(gridExtra)
        library(reshape2)
        library(ggplot2)                   
        library(scales)
        library(gdata)
        library(grid)
        library(zoo)
        library(gtools)
        library(forecast)
        library(tseries)
        options(scipen=999)
        # GLOBAL CONSTANTS (THAT WE MAY WANT TO TWEAK)
        # firstDate <- as.Date("03/01/2009", format="%m/%d/%Y")
        useDate   <- as.Date("03/31/2015", format="%m/%d/%Y")
        fcDate    <- as.Date("09/30/2015", format="%m/%d/%Y") # Forecast date = useDate + 6 Months
}

#=========================================================================================================#
#    STEP B: DATA GATHERING AND MERGING (assumes "data" subdirectory that includes CSV files)
#=========================================================================================================#
#this would need to change depending on where the data is

{
        setwd("F://CSVFiles/AlansProgram")
        df1 <- read.csv("modifiedAccountingReport2009_2012.csv", sep=",", stringsAsFactors=FALSE)
        df2 <- read.csv("modifiedAccountingReport2013.csv", sep=",", stringsAsFactors=FALSE)
        df3 <- read.csv("modifiedAccountingReport2014_1.csv", sep=",", stringsAsFactors=FALSE)
        df4 <- read.csv("modifiedAccountingReport2014_2.csv", sep=",", stringsAsFactors=FALSE)
        df5 <- read.csv("modifiedAccountingReport2015_1.csv", sep=",", stringsAsFactors=FALSE)
        df6 <- read.csv("modifiedAccountingReport2015_2.csv", sep=",", stringsAsFactors=FALSE)
        
        #df <- merge(df1[-1,], df2[-1,], all.x=TRUE, all.y=TRUE)
        #df <- merge(df, df3[-1,], all.x=TRUE, all.y=TRUE)
        #df <- merge(df, df4[-1,], all.x=TRUE, all.y=TRUE)
        #df <- merge(df, df5[-1,], all.x=TRUE, all.y=TRUE)
        #df <- merge(df, df6[-1,], all.x=TRUE, all.y=TRUE)
        #df <- merge(df, df7[-1,], all.x=TRUE, all.y=TRUE)
        df <- rbind(df1, df2, df3, df4, df5, df6)

        rm(list = "df1","df2","df3","df4","df5","df6","df7")        # Don't need these any longer...free up memory
}

#=========================================================================================================#
#    STEP C: DATA TRANSFORMATIONS & FEATURE ENGINEERING (FULL DATASET)
#=========================================================================================================#

{
        # Change these variables into "Factors"
        df$Status          <- as.factor(df$Status)
        df$PaymentStatus   <- as.factor(df$PaymentStatus)
        df$ApprovalPath    <- as.factor(df$ApprovalPath)
        
        # Change these variables into "Dates"
        # The format on the date may need to be changed to read properly, otherwise NAs will ensue
        df$FundedDate      <- as.Date(df$FundedDate, format="%m/%d/%Y")
        df$TransactionDate <- as.Date(df$TransactionDate, format="%m/%d/%Y")
        
        # Change these variables into "Numerics"
        df$FundedAmount    <- as.numeric(df$FundedAmount)
        df$PaymentAmount   <- as.numeric(as.character(df$PaymentAmount))
        
        # New features
        df$xYear           <- year(df$TransactionDate)
        df$xMonth          <- month(df$TransactionDate)
        df$xPay            <- ifelse(df$PaymentStatus=="CLEARED", df$PaymentAmount, 0)
        df$fYear           <- year(df$FundedDate)
        df$fMonth          <- month(df$FundedDate)
}

#---------------------------------------------------------------------------------------------------------#
#    STEP D: LOAD DATA INTO dfA DATA.FRAME, LOOK AT DATE RANGE
#---------------------------------------------------------------------------------------------------------#

{
        dfA <- df
        
        # Explore date range
        min(dfA$FundedDate)
        max(dfA$FundedDate)
}

#=========================================================================================================#
#    STEP E: COMMON FUNCTION DECLARATIONS
#=========================================================================================================#

{
        # Declare function that takes a dollar value and formats it into a string with commas and a $
        dollar <- function(x){
                paste0("$ ", format(round(x, 0), big.mark=","))
        }
        
        # Declare function that takes a decimal value and converts it to a formatted percentage string
        percent <- function(x){
                paste0(format(x*100, digits=2, nsmall=2, trim=TRUE, decimal.mark=".", justify="right"),"%") 
        }
        
        # Plotting functions to place a table below the graph
        Layout <- grid.layout(nrow = 2, ncol = 1, heights = unit(c(1, 0.25), c("null", "null")))
        grid.show.layout(Layout)
        
        vplayout <- function(...) {
                grid.newpage()
                pushViewport(viewport(layout = Layout))
        }
        
        subplot <- function(x, y) viewport(layout.pos.row = x, layout.pos.col = y)
        
        mmplot <- function(a, b) {
                vplayout()
                print(a, vp = subplot(1, 1))
                print(b, vp = subplot(2, 1))
        }
        
        # Functions to get the minimum and maximum of a column
        colMax <- function(data) sapply(data, max, na.rm=TRUE)
        colMin <- function(data) sapply(data, min, na.rm=TRUE)
}

#=========================================================================================================#
#    TASK #1: PAYMENT ALLOCATION
#=========================================================================================================#
#---------------------------------------------------------------------------------------------------------#
#    STEP 1A: CREATE MATRICES FOR PAYMENT ALLOCATION AND PERCENT ALLOCATION (INIT NAMES)
#---------------------------------------------------------------------------------------------------------#
{
        interval <- 13
        
        # Initialize date sequence variables (to get lengths and names)
        months <- seq(from=min(dfA$TransactionDate, na.rm=TRUE), to=max(dfA$TransactionDate, na.rm=TRUE), by='months' )
        xDate  <- seq(from=floor_date(min(dfA$TransactionDate, na.rm=TRUE), "month"), to=max(dfA$TransactionDate, na.rm=TRUE), by='months' )
        fDate  <- seq(from=floor_date(min(dfA$FundedDate, na.rm=TRUE), "month"), to=max(dfA$FundedDate, na.rm=TRUE), by='months' )
        
        # Initialize two dataframes for Payment Allocation and Percent Allocation
        payAlloc     <- data.frame(matrix(0, ncol=length(fDate), nrow=length(fDate)+1)) # Add 1 extra row for payment "Totals"
        percAlloc    <- data.frame(matrix(0, ncol=length(fDate), nrow=length(fDate)))   
        payAllocInt  <- data.frame(matrix(0, ncol=length(fDate), nrow=interval+3)) # Add 3 extra rows, one for "current" month, one for payment "Sub-Totals" and one for "ALL Totals"
        percAllocInt <- data.frame(matrix(0, ncol=length(fDate), nrow=interval+2)) # Add 2 extra rows, one for "current" month and one for payment "Sub-Totals"
        
        # Initialize column and row.names for dataframes (note row.name option depending on ORDER)
        names(payAlloc) <- names(percAlloc) <- names(payAllocInt) <- names(percAllocInt) <- format(fDate, "%b %Y")
        row.names(payAlloc)        <- c(format(rev(fDate), "%b %Y"),"Total") # For showing newest date first - match w/ loop order below
        row.names(percAlloc)       <- format(rev(fDate), "%b %Y")            # For showing newest date first - match w/ loop order below
        
        row.names(percAllocInt)[1] <- row.names(payAllocInt)[1]  <- "Current"  # Months are named relative to "Current" month
        row.names(payAllocInt)[2]  <- row.names(percAllocInt)[2] <- paste0("-1 Month")
        for(i in 2:interval){row.names(payAllocInt)[i+1] <- row.names(percAllocInt)[i+1] <- paste0("-", i, " Months")}
        row.names(payAllocInt)[interval+2]  <- row.names(percAllocInt)[interval+2] <- "Sub-total"
        row.names(payAllocInt)[interval+3]  <- "Total ALL"
        
        # Initialize Year/Month variables for comparing TransactionDates (iYear/iMonth)
        iYear  <- year(min(dfA$TransactionDate, na.rm=TRUE))
        iMonth <- month(min(dfA$TransactionDate, na.rm=TRUE))
}

#---------------------------------------------------------------------------------------------------------#
#    STEP 1B: FILL IN TABLES BASED ON TIME INTERVAL NUMBER OF MONTHS (13?)
#---------------------------------------------------------------------------------------------------------#


#my code
#need to format and make it look nice like Alan's
{
        payAllocCopy     <- data.frame(matrix(0, ncol=length(fDate), nrow=length(fDate)+1)) # Add 1 extra row for payment "Totals"
        percAllocCopy    <- data.frame(matrix(0, ncol=length(fDate), nrow=length(fDate)))   
        payAllocIntCopy  <- data.frame(matrix(0, ncol=length(fDate), nrow=interval+3)) # Add 3 extra rows, one for "current" month, one for payment "Sub-Totals" and one for "ALL Totals"
        percAllocIntCopy <- data.frame(matrix(0, ncol=length(fDate), nrow=interval+2)) # Add 2 extra rows, one for "current" month and one for payment "Sub-Totals"
        
        # Initialize column and row.names for dataframes (note row.name option depending on ORDER)
        names(payAllocCopy) <- names(percAllocCopy) <- names(payAllocIntCopy) <- names(percAllocIntCopy) <- format(fDate, "%b %Y")
        row.names(payAllocCopy)        <- c(format(rev(fDate), "%b %Y"),"Total") # For showing newest date first - match w/ loop order below
        row.names(percAllocCopy)       <- format(rev(fDate), "%b %Y")            # For showing newest date first - match w/ loop order below
        
        row.names(percAllocIntCopy)[1] <- row.names(payAllocIntCopy)[1]  <- "Current"  # Months are named relative to "Current" month
        row.names(payAllocIntCopy)[2]  <- row.names(percAllocIntCopy)[2] <- paste0("-1 Month")
        for(i in 2:interval){row.names(payAllocIntCopy)[i+1] <- row.names(percAllocIntCopy)[i+1] <- paste0("-", i, " Months")}
        row.names(payAllocIntCopy)[interval+2]  <- row.names(percAllocIntCopy)[interval+2] <- "Sub-total"
        row.names(payAllocIntCopy)[interval+3]  <- "Total ALL"
        
        x <- Sys.time()
        dfCopy <- df
        
        
        dfCopy$XFDifference <- 0
        dfCopy$XFDifference <- (((year(dfCopy$TransactionDate) - year(dfCopy$FundedDate) - 1) * 12) + 
                                        (12 - month(dfCopy$FundedDate)) + 
                                        month(dfCopy$TransactionDate))
        
        dfCopy <- dfCopy[dfCopy$ReturnedDate == "NULL",]
        dfCopyAggregate <- aggregate(PaymentAmount~fYear + fMonth + XFDifference, sum, data = dfCopy)
        dfCopyAggregate <- dfCopyAggregate[order(dfCopyAggregate$fYear, dfCopyAggregate$fMonth, decreasing = T) ,]
        
        dfXCopyAggregate <- aggregate(PaymentAmount~xYear + xMonth + XFDifference, sum, data = dfCopy)
        dfXCopyAggregate <- dfXCopyAggregate[order(dfXCopyAggregate$xYear, dfXCopyAggregate$xMonth, decreasing = T) ,]
        
        startDate <- min(dfCopy$FundedDate)
        endDate <- max(dfCopy$FundedDate)
        
        elapsedTimeMonths <- (year(endDate) - year(startDate) - 1) * 12 + (12 - month(startDate)) + month(endDate)
        
        #this fills in each cell in the interval
        #NEEDS TO HAPPEN FIRST
        for (i in 1:(elapsedTimeMonths + 1)){
                initialDate <- startDate
                month(initialDate) <- month(initialDate) + i - 1
                monthInitialDate <- month(initialDate)
                yearInitialDate <- year(initialDate)
                subsetDF <- subset(dfCopyAggregate, dfCopyAggregate$fMonth == monthInitialDate & 
                                           dfCopyAggregate$fYear == yearInitialDate &
                                           dfCopyAggregate$XFDifference >= 0 &
                                           dfCopyAggregate$XFDifference <= interval)
                for (j in 1:(length(subsetDF$XFDifference))){
                        if (subsetDF$XFDifference[1] == 0){
                                payAllocIntCopy[j,i + j - 1] <- subsetDF$PaymentAmount[j]
                        }
                        else{
                                payAllocIntCopy[j + 1,i + j] <- subsetDF$PaymentAmount[j]
                        }
                }
        }
        
        #this fills in the overall total
        for (i in 1:(elapsedTimeMonths + 1)){
                initialDate <- startDate
                month(initialDate) <- month(initialDate) + i - 1
                monthInitialDate <- month(initialDate)
                yearInitialDate <- year(initialDate)
                payAllocIntCopy[16,i] <- sum(dfXCopyAggregate$PaymentAmount[dfXCopyAggregate$xYear == yearInitialDate &
                                                                                    dfXCopyAggregate$xMonth == monthInitialDate])
        }
        
        #this fills the sub total
        for (i in 1:(elapsedTimeMonths + 1)){
                initialDate <- startDate
                month(initialDate) <- month(initialDate) + i - 1
                monthInitialDate <- month(initialDate)
                yearInitialDate <- year(initialDate)
                
                payAllocIntCopy[15,i] <- sum(payAllocIntCopy[i], na.rm = T) - payAllocIntCopy[16,i]
        }
        
        percAllocIntCopy <- payAllocIntCopy
        time <- elapsedTimeMonths + 1
        for (i in 1:time){
                percAllocIntCopy[i] <- round(percAllocIntCopy[i] / percAllocIntCopy[15,i] * 100, 2)
        }
        
        for (i in 1:time){
                percAllocIntCopy[15,i] <- round(payAllocIntCopy[15,i],2)
                percAllocIntCopy[16,i] <- round(payAllocIntCopy[16,i],2)
        }
        
} 

#Alan's Code
{
        a <- Sys.time()
        for(i in seq_along(months))
        {
                # Initialize Year/Month variables for comparing FundedDates (jYear/jMonth) = TransactionDate - Interval
                jYear <- iYear - (interval %/% 12)
                if(iMonth >= (interval %% 12)){jMonth <- iMonth - (interval %% 12)}else{jMonth <- 12 + iMonth - (interval %% 12); jYear <- jYear - 1}
                
                payAllocInt[interval+3, i] <- y <- sum(dfA$xPay[dfA$xYear == iYear & dfA$xMonth == iMonth], na.rm=TRUE) # Add a ALL TOTAL value in the last row
                
                for(j in (interval+1):1)        # For showing newest date first - match w/ row.names above (+1 shows current month)
                {
                        payAllocInt[j, i]  <- x <- sum(dfA$xPay[(dfA$xYear == iYear & dfA$xMonth == iMonth) & (dfA$fYear == jYear & dfA$fMonth == jMonth)], na.rm=TRUE)
                        percAllocInt[j, i] <- ifelse((is.nan(as.numeric(x) / as.numeric(y))) | (as.numeric(x) == 0), 0, as.numeric(x) / as.numeric(y))
                        
                        if(jMonth == 12){jYear <- jYear + 1; jMonth <- 1}else{jMonth <- jMonth + 1} # Roll thru all month/year combos (FundedDate)
                }
                
                payAllocInt[interval+2, i] <- sum(payAllocInt[1:(interval+1), i])   # Add a SUB-TOTAL value in the 2nd to last row
                percAllocInt[interval+2, i] <- sum(percAllocInt[1:(interval+1), i]) # Add a SUB-TOTAL value in the last row (Total assumed = 100%)
                
                payAllocInt[, i]  <- sapply(payAllocInt[, i], dollar)               # Format the dollar values in this column
                percAllocInt[, i] <- sapply(percAllocInt[, i], percent)             # Format the percent values in this column
                
                payAllocInt[, i]  <- ifelse(payAllocInt[, i] == "$ 0", "", payAllocInt[, i])     # Remove ZERO values to make it more readable
                percAllocInt[, i] <- ifelse(percAllocInt[, i] == "0.00%", "", percAllocInt[, i]) # Remove ZERO values to make it more readable
                
                if(iMonth == 12){iYear <- iYear + 1; iMonth <- 1}else{iMonth <- iMonth + 1} # Roll thru all month/year combos (TransactionDate)
        }
        b <- Sys.time()
        alanCode <- b - a
        #grid.table(payAllocInt, gpar.coretext = gpar(fontsize=8), gpar.coltext = gpar(fontsize=8), gpar.rowtext = gpar(fontsize=8), padding.h=unit(2, "mm"), padding.v=unit(2, "mm"), show.colnames = TRUE, show.rownames = TRUE)
        #grid.table(percAllocInt, gpar.coretext = gpar(fontsize=8), gpar.coltext = gpar(fontsize=8), gpar.rowtext = gpar(fontsize=8), padding.h=unit(2, "mm"), padding.v=unit(2, "mm"), show.colnames = TRUE, show.rownames = TRUE)
        
} 

write.csv(file="PayAlloc.csv", x=payAllocInt)
write.csv(file="PercAlloc.csv", x=percAllocInt)

#---------------------------------------------------------------------------------------------------------#
#    STEP 1C: FILL IN TABLES GOING BACK TO THE BEGINNING OF TIME (ALL)
#---------------------------------------------------------------------------------------------------------#
# Initialize Year/Month variables for comparing TransactionDates (iYear/iMonth)

#my code

{
        x <- Sys.time()
        dfCopy <- df
        
        
        dfCopy$XFDifference <- 0
        dfCopy$XFDifference <- (((year(dfCopy$TransactionDate) - year(dfCopy$FundedDate) - 1) * 12) + 
                                        (12 - month(dfCopy$FundedDate)) + 
                                        month(dfCopy$TransactionDate))
        
        dfCopy <- dfCopy[dfCopy$ReturnedDate == "NULL",]
        dfCopyAggregate <- aggregate(PaymentAmount~fYear + fMonth + XFDifference, sum, data = dfCopy)
        dfCopyAggregate <- dfCopyAggregate[order(dfCopyAggregate$fYear, dfCopyAggregate$fMonth, decreasing = T) ,]
        
        dfXCopyAggregate <- aggregate(PaymentAmount~xYear + xMonth + XFDifference, sum, data = dfCopy)
        dfXCopyAggregate <- dfXCopyAggregate[order(dfXCopyAggregate$xYear, dfXCopyAggregate$xMonth, decreasing = T) ,]
        
        startDate <- min(dfCopy$FundedDate)
        endDate <- max(dfCopy$FundedDate)
        
        elapsedTimeMonths <- (year(endDate) - year(startDate) - 1) * 12 + (12 - month(startDate)) + month(endDate)
        
        #this fills in each cell in the interval
        #NEEDS TO HAPPEN FIRST
        rows <- length(payAllocCopy$"Feb 2009") - 1
        for (i in 1:(elapsedTimeMonths + 1)){
                initialDate <- startDate
                month(initialDate) <- month(initialDate) + i - 1
                monthInitialDate <- month(initialDate)
                yearInitialDate <- year(initialDate)
                subsetDF <- subset(dfCopyAggregate, dfCopyAggregate$fMonth == monthInitialDate & 
                                           dfCopyAggregate$fYear == yearInitialDate &
                                           dfCopyAggregate$XFDifference >= 0
                                           #dfCopyAggregate$XFDifference <= interval
                                           )
                for (j in 1:(length(subsetDF$XFDifference))){
                        newrow <- rows - j
                        payAllocCopy[newrow,i] <- subsetDF$PaymentAmount[j]
                        
                        
                        #if (subsetDF$XFDifference[1] == 0){
                        #        payAllocCopy[80 - j,i + j - 1] <- subsetDF$PaymentAmount[j]
                        #}
                        #else{
                        #        payAllocCopy[j + 1,i + j] <- subsetDF$PaymentAmount[j]
                        #}
                }
        }
        
        #this fills in the overall total
        for (i in 1:(elapsedTimeMonths + 1)){
                initialDate <- startDate
                month(initialDate) <- month(initialDate) + i - 1
                monthInitialDate <- month(initialDate)
                yearInitialDate <- year(initialDate)
                payAllocIntCopy[16,i] <- sum(dfXCopyAggregate$PaymentAmount[dfXCopyAggregate$xYear == yearInitialDate &
                                                                                    dfXCopyAggregate$xMonth == monthInitialDate])
        }
        
        #this fills the sub total
        for (i in 1:(elapsedTimeMonths + 1)){
                initialDate <- startDate
                month(initialDate) <- month(initialDate) + i - 1
                monthInitialDate <- month(initialDate)
                yearInitialDate <- year(initialDate)
                
                payAllocIntCopy[15,i] <- sum(payAllocIntCopy[i], na.rm = T) - payAllocIntCopy[16,i]
        }
        
        percAllocIntCopy <- payAllocIntCopy
        time <- elapsedTimeMonths + 1
        for (i in 1:time){
                percAllocIntCopy[i] <- round(percAllocIntCopy[i] / percAllocIntCopy[15,i] * 100, 2)
        }
        
        for (i in 1:time){
                percAllocIntCopy[15,i] <- round(payAllocIntCopy[15,i],2)
                percAllocIntCopy[16,i] <- round(payAllocIntCopy[16,i],2)
        }
}

#alan's code
{
        iYear  <- year(min(dfA$TransactionDate, na.rm=TRUE))
        iMonth <- month(min(dfA$TransactionDate, na.rm=TRUE))
        
        for(i in seq_along(months))
        {
                # Start with the minimum "Funded Date" year and month...e.g., March 2009, then increment up from there.
                jYear  <- year(min(dfA$FundedDate, na.rm=TRUE))
                jMonth <- month(min(dfA$FundedDate, na.rm=TRUE))
                
                payAlloc[length(fDate)+1, i] <- dollar(y <- sum(dfA$xPay[dfA$xYear == iYear & dfA$xMonth == iMonth], na.rm=TRUE)) # Add a TOTAL value in the last row
                
                for(j in length(fDate):1)          # For showing newest date first - match w/ row.names above
                {
                        payAlloc[j, i]  <- dollar(x <- sum(dfA$xPay[(dfA$xYear == iYear & dfA$xMonth == iMonth) & (dfA$fYear == jYear & dfA$fMonth == jMonth)], na.rm=TRUE))
                        percAlloc[j, i] <- ifelse((is.nan(as.numeric(x) / as.numeric(y))) | (as.numeric(x) == 0), "", percent(as.numeric(x) / as.numeric(y)))
                        
                        if(jMonth == 12){jYear <- jYear + 1; jMonth <- 1}else{jMonth <- jMonth + 1} # Roll thru all month/year combos (FundedDate)
                }
                
                if(iMonth == 12){iYear <- iYear + 1; iMonth <- 1}else{iMonth <- iMonth + 1} # Roll thru all month/year combos (TransactionDate)
        }
        
        #grid.table(payAlloc, gpar.coretext = gpar(fontsize=4), gpar.coltext = gpar(fontsize=4), gpar.rowtext = gpar(fontsize=4), padding.h=unit(2, "mm"), padding.v=unit(2, "mm"), show.colnames = TRUE, show.rownames = TRUE)
        #grid.table(percAlloc, gpar.coretext = gpar(fontsize=4), gpar.coltext = gpar(fontsize=4), gpar.rowtext = gpar(fontsize=4), padding.h=unit(2, "mm"), padding.v=unit(2, "mm"), show.colnames = TRUE, show.rownames = TRUE)
}

write.csv(file="PayAllocAll.csv", x=payAlloc)
write.csv(file="PercAllocAll.csv", x=percAlloc)

#---------------------------------------------------------------------------------------------------------#
#    STEP 1D: RE-FILL IN TABLES FOR GRAPHING
#---------------------------------------------------------------------------------------------------------#
# Initialize Year/Month variables for comparing TransactionDates (iYear/iMonth)
iYear  <- year(min(dfA$TransactionDate, na.rm=TRUE))
iMonth <- month(min(dfA$TransactionDate, na.rm=TRUE))

for(i in seq_along(months))
{
     # Initialize Year/Month variables for comparing FundedDates (jYear/jMonth) = TransactionDate - Interval
     jYear <- iYear - (interval %/% 12)
     if(iMonth >= (interval %% 12)){jMonth <- iMonth - (interval %% 12)}else{jMonth <- 12 + iMonth - (interval %% 12); jYear <- jYear - 1}

     payAllocInt[interval+3, i] <- y <- sum(dfA$xPay[dfA$xYear == iYear & dfA$xMonth == iMonth], na.rm=TRUE) # Add a ALL TOTAL value in the last row

     for(j in (interval+1):1)        # For showing newest date first - match w/ row.names above (+1 shows current month)
     {
          payAllocInt[j, i]  <- x <- sum(dfA$xPay[(dfA$xYear == iYear & dfA$xMonth == iMonth) & (dfA$fYear == jYear & dfA$fMonth == jMonth)], na.rm=TRUE)
          percAllocInt[j, i] <- ifelse((is.nan(as.numeric(x) / as.numeric(y))) | (as.numeric(x) == 0), 0, as.numeric(x) / as.numeric(y))
          
          if(jMonth == 12){jYear <- jYear + 1; jMonth <- 1}else{jMonth <- jMonth + 1} # Roll thru all month/year combos (FundedDate)
     }

     if(iMonth == 12){iYear <- iYear + 1; iMonth <- 1}else{iMonth <- iMonth + 1} # Roll thru all month/year combos (TransactionDate)
}

noMonths        <- 12                                                 # Number of months to "average" together (how far back to go)

payAllocInt[payAllocInt == 0] <- NA                                   # Need to replace ZERO's w/ NA's, so they don't change the "mean"
results         <- data.frame(matrix(0, ncol=2, nrow=(interval+1)))   # Create new dataframe
names(results)  <- c("Mo", "Amount")                                  # Initialize column names
results$Mo      <- seq(0, -(interval), -1)                            # Initialize month values/names
for(i in 1:(interval+1)){results[i,"Amount"] <- mean(as.numeric(payAllocInt[i,(ncol(payAllocInt)-noMonths+1):ncol(payAllocInt)]), na.rm=TRUE)}

percAllocInt[percAllocInt == 0] <- NA                                 # Need to replace ZERO's w/ NA's, so they don't change the "mean"
results1        <- data.frame(matrix(0, ncol=2, nrow=(interval+1)))   # Create new dataframe
names(results1) <- c("Mo", "Percent")                                 # Initialize column names
results1$Mo     <- seq(0, -(interval), -1)                            # Initialize month values/names
for(i in 1:(interval+1)){results1[i,"Percent"] <- mean(as.numeric(percAllocInt[i,(ncol(percAllocInt)-noMonths+1):ncol(percAllocInt)]), na.rm=TRUE)}

#---------------------------------------------------------------------------------------------------------#
#    STEP 1E: VISUALIZE HISTORICAL PAYMENT ALLOCATION BASED ON DOLLARS AND PERCENTAGES
#---------------------------------------------------------------------------------------------------------#
pP <- ggplot(data = results1, aes(x = Mo, y = Percent, fill = Amount)) +
     ggtitle("Figure 1.0: Payment Allocation (Last Year Avg.)") + 
     geom_bar(colour="black", fill="red4", width=.8, stat="identity") + 
     theme(
          plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
          axis.title.x = element_text(size=15),
          axis.title.y = element_text(size=15),
          axis.text.x = element_text(size=12),
          axis.text.y = element_text(size=12)) +
     xlab("Month of Funded Loan (Current Month = 0)") +
     ylab("Percent of Investment Funded (% on a given Month)")

pdf("pap.pdf")
pP
dev.off()

pD <- ggplot(data = results, aes(x = Mo, y = Amount, fill = Amount)) +
     ggtitle("Figure 1.1: Payment Allocation (Last Year Avg.)") + 
     geom_bar(colour="black", fill="blue4", width=.8, stat="identity") + 
     theme(
          plot.title = element_text(size=rel(1.6), lineheight=0.8, vjust=2), 
          axis.title.x = element_text(size=15),
          axis.title.y = element_text(size=15),
          axis.text.x = element_text(size=12),
          axis.text.y = element_text(size=12)) +
     xlab("Month of Funded Loan (Current Month = 0)") +
     ylab("Amount of Investment Funded ($ on a given Month)")

pdf("pad.pdf")
pD
dev.off()

#=========================================================================================================#
#    TASK #2: PAYMENT PREDICTION
#=========================================================================================================#
#---------------------------------------------------------------------------------------------------------#
#    STEP 2A: TRANSFORM DATA INTO DAYS, WEEKS AND MONTHS (AGGREGATE OBSERVATIONS - KEEP ALL STILL)
#---------------------------------------------------------------------------------------------------------#
df1 <- dfA[dfA$PaymentStatus %in% c("CLEARED"),]
df1 <- df1[, c("TransactionDate", "xPay")]

dfM <- df1
dfM$TransactionDate <- floor_date(dfM$TransactionDate, "month")  # Group into months
dfM <- aggregate(xPay ~ TransactionDate, dfM, FUN = sum)         # Now sum payments by month

#---------------------------------------------------------------------------------------------------------#
#    STEP 2B: SUBSET DATA BY GRABBING "WINDOWS" FOR FURTHER ANALYSIS
#---------------------------------------------------------------------------------------------------------#
# Since we don't have a complete month for April of 2015, let stop in March of 2015
dfMTS    <- ts(dfM$xPay, start=c(2009, 3), end=c(2015, 3), freq=12)  # Turn into "Time Series" object
dfMTS.12 <- window(dfMTS, start=c(2012, 1), end=c(2015, 3), freq=12) # Values from beginning of 2012

#---------------------------------------------------------------------------------------------------------#
#    STEP 2C: TRANSFORMATIONS
#---------------------------------------------------------------------------------------------------------#
# Look at some transformations (Log and Differencing and Log + Differencing)
dfMTS.d    <- diff(dfMTS, lag=1)             # First differenced
dfMTS.12d  <- diff(dfMTS.12, lag=1)          # First differenced

dfMTS.L    <- log(dfMTS)                     # Log transform applied
dfMTS.12L  <- log(dfMTS.12)                  # Log transform applied

dfMTS.Ld   <- diff(log(dfMTS), lag=1)        # Log transform + 1st differenced
dfMTS.12Ld <- diff(log(dfMTS.12), lag=1)     # Log transform + 1st differenced

dfMTS.dd   <- diff(dfMTS.d, lag=1)           # Second differenced

# Plot transformations
plot(dfMTS.d)                                # Still see heteroskedasticity
plot(dfMTS.dd)                               # No change
plot(dfMTS.L)                                # Still trend exists
plot(dfMTS.Ld)                               # Probably looks the best

#---------------------------------------------------------------------------------------------------------#
#    STEP 2CM: PERFORM SOME EXPLORATORY ANALYSIS ON MONTHLY DATA USING DIFFERENT TRANSFORMATIONS AND MODELS
#         First, look at your date by plotting it
#         Second, decompose to look at the trend, seasonality
#         Third, remove trend and seasonality and look at residuals for stochastic structure
#         Fourth, compute autocorrelation and autocovariance (check for stationarity)
#---------------------------------------------------------------------------------------------------------#

# Look at monthly plots
pdf("fData.pdf")
plot(dfMTS)              # Clearly heteroskedastic - probably apply log transform/BoxCox (+ maybe differencing)
dev.off()

pdf("sData.pdf")
plot(dfMTS.12)           # Clearly heteroskedastic - probably apply log transform/BoxCox (+ maybe differencing)
dev.off()

# Look at weekly plots
pdf("fData.pdf")
plot(dfWTS)              # Still heteroskedastic - but now see a strong seasonal component as well as exponential trend
dev.off()

pdf("sData.pdf")
plot(dfWTS.12)           # Still heteroskedastic - but now see a strong seasonal component as well as exponential trend
dev.off()

# Decompose into Trend, Seasonal and Random components - plot
decomp <- decompose(dfMTS)
plot(decomp)

adf.test(dfMTS, alternative="stationary")    # Non-stationary
adf.test(dfMTS.d, alternative="stationary")  # Non-stationary still
adf.test(dfMTS.dd, alternative="stationary") # Stationary

acf(dfMTS)
acf(dfMTS.d)
acf(dfMTS.dd)

pacf(dfMTS)
pacf(dfMTS.d)
pacf(dfMTS.dd)

# First determine stationarity
# If an AR model is not stationary, this implies that previous values of the error term will have a non-declining effect on the current value of the dependent variable.
# This implies that the coefficients on the MA process would not converge to zero as the lag length increases.
# For an AR model to be stationary, the coefficients on the corresponding MA process decline with lag length, converging on 0.

# Test #1: Augmented Dickey-Fuller (ADF) test, small p-values (< 0.05) reject null hypothesis
adf.test(dfMTS, alternative="stationary", k=0)    # p-value = 0.99 cannot reject null hypothesis = explosive or NOT stationary
adf.test(dfMTS, alternative="explosive", k=0)     # p-value = 0.01 reject null hypothesis (accept alternative hypothesis = explosive or NOT stationary)
adf.test(dfMTS.d, alternative="stationary", k=0)  # p-value = 0.01 reject null hypothesis (accept alternative hypothesis = stationary)
adf.test(dfMTS.dd, alternative="stationary", k=0) # p-value = 0.01 reject null hypothesis (accept alternative hypothesis = stationary)

# Test #2: Kwiatkowski-Phillips-Schmidt-Shin (KPSS) test for the null hypothesis that x is level or trend stationary
kpss.test(dfMTS, null="Trend")                    # p-value = 0.01 reject null hypothesis (stationary). Accept alternative = non-stationary
kpss.test(dfMTS.d, null="Trend")                  # p-value = 0.011 reject null hypothesis (stationary). Accept alternative = non-stationary
kpss.test(dfMTS.dd, null="Trend")                 # p-value = 0.10 cannot reject null hypothesis = stationary

# Run somes auto ARIMA models for the different data sets
# ARIMA are linearly dependent models (linearly dependent on previous data points)
# Small k in MA is more responsive to recent changes in data and will have less of a smoothing effect
# Large k in MA is less response, and produce more of a pronounced lag in the smoothing sequence
dfMTSFit <- auto.arima(dfMTS)                # ARIMA(0,2,3)(0,1,0)[12], LL -852, AIC 1712
plot(forecast.Arima(dfMTSFit, h=6))

dfMTS.12Fit <- auto.arima(dfMTS.12)          # ARIMA(0,1,0)(0,1,0)[12], LL -389, AIC 780.27
plot(forecast.Arima(dfMTS.12Fit, h=6))

dfMTS.13Fit <- auto.arima(dfMTS.13)          # ARIMA(0,1,0) w/ drift, LL -372, AIC 749.08
plot(forecast.Arima(dfMTS.13Fit, h=6))

# Log transform first, then apply Arima (Full dataset)
aaFit <- auto.arima(dfMTS.L)                 # ARIMA(0,2,0)(1,0,0)[12], LL 28.37, AIC -52.74
plot(forecast.Arima(aaFit, h=6))             # Look at forecast of LOG data
aafc       <- forecast(aaFit, h=6)
aafc$mean  <- exp(aafc$mean)
aafc$upper <- exp(aafc$upper)
aafc$lower <- exp(aafc$lower)
aafc$x     <- exp(aafc$x)
plot(aafc)                                   # Plot inverse log (back to original) data (not so great using full dataset)

# Log transform first, then apply Arima (Only data after 2012)
aa12Fit <- auto.arima(dfMTS.12L)             # ARIMA(0,1,0)(1,0,0)[12], LL 36.45, AIC -66.9
plot(forecast.Arima(aa12Fit, h=6))           
aa12fc       <- forecast(aa12Fit, h=6)
aa12fc$mean  <- exp(aa12fc$mean)
aa12fc$upper <- exp(aa12fc$upper)
aa12fc$lower <- exp(aa12fc$lower)
aa12fc$x     <- exp(aa12fc$x)
plot(aa12fc)                                 # Plot inverse log (back to original) data (looks much better just using data > 2012)

# Now look at some algorithms that already work well with exponential trends (HW, ETS)
# Exponential smoothing algorithms (such as HW), weigh the more recent data points more strongly than others (such as in MA - mean of last k points -, where past observations are weighted equally)
# Exponential smoothing is commonly applied to financial market or economic data
hwFit <- HoltWinters(dfMTS)                  # HW: alpha=0.4080884, beta=0.6777756, gamma=1
hwFitL <- HoltWinters(dfMTS.L)               # HW: alpha=0.7616733, beta=0.001827628, gamma=1
summary(hwFit)
accuracy(hwFit)
plot(forecast(hwFit, 6))

hwFit <- HoltWinters(dfMTS.12)               # HW: alpha=0.6085406, beta=0.3613982, gamma=0.4264201

# Exponential Smoothing (ETS) model w/ & w/o BoxCox transformation for stabilizing the variation in the variance
etsFit  <- ets(dfMTS)                        # ETS: (M,Md,N) alpha=0.7507, beta=0.2868, phi=0.8, drift=TRUE
etsFitL <- ets(dfMTS.L)                      # ETS: (M,Md,N) alpha=0.9997, beta=0.8806, phi=0.8, drift=TRUE
summary(etsFit)
accuracy(etsFit)
plot(forecast(etsFit, 6))

etsFit <- ets(dfMTS.12)                      # ETS: (M,M,N) alpha=0.9999, beta=0.1e-04, drift=FALSE

lam <- BoxCox.lambda(dfMTS)
bcFit <- ets(dfMTS, additive=TRUE, lambda=lam)
fcbc <- forecast(bcFit, h=6)
plot(fcbc)

# Linear Regression models (w/ and w/o log transformation)
lmFit   <- tslm(dfMTS ~ trend + season, lambda=0)
lmfc    <- forecast(lmFit, h=6)
plot(lmfc)                                   # Plot inverse log (back to original) data (not so great using full dataset)
summary(lmFit)
acf(resid(lmFit))
pacf(resid(lmFit))

lmLFit      <- tslm(dfMTS.L ~ trend + season, lambda=0)
lmLfc       <- forecast(lmLFit, h=6)
lmLfc$mean  <- exp(lmLfc$mean)
lmLfc$upper <- exp(lmLfc$upper)
lmLfc$lower <- exp(lmLfc$lower)
lmLfc$x     <- exp(lmLfc$x)
plot(lmLfc)                                   # Plot inverse log (back to original) data (not so great using full dataset)

#---------------------------------------------------------------------------------------------------------#
#    STEP 2DM: CROSS VALIDATION INCREMENTING A MONTH AT A TIME - FORECAST VS. ACTUAL FOR 6 MONTH CHUNKS
#         Find the best model for each algorithm family and compare to see who has the lowest MAE
#         Try the following options:
#
#         From the previous exploratory analysis, we know that this dataset is:
#              * Non-stationary (with an exponential trend component)
#                   - Will require transformations before we can use (Log, BoxCox and/or Differencing)
#              * Multiplicative
#              * Heteroskedastic
#
#         OPTION 1: Test ALL data ... Put dfMTS into tsX (and dfMTS.L into tsXL)
#         OPTION 2: Test data since 2012 ... Put dfMTS.12 into tsX (and dfMTS.12L into tsXL)
#---------------------------------------------------------------------------------------------------------#
#         OPTION 1:  (ALL data)
#---------------------------------------------------------------------------------------------------------#
tsX  <- dfMTS   
tsXL <- dfMTS.L   

k <- 24                       # The minimum number of months to begin training with
p <- 6                        # The time period to compare actual vs. forecast (in months)
n <- length(tsX)              # The total # of months in the dataset

mae1a  <- mae2a  <- mae3a  <- mae4a  <- matrix(NA, n-k, p)
cia1a  <- cia2a  <- cia3a  <- cia4a  <- matrix(NA, n-k, p)
cib1a  <- cib2a  <- cib3a  <- cib4a  <- matrix(NA, n-k, p)
intb1a <- intb2a <- intb3a <- intb4a <- matrix(NA, n-k, p)

st  <- tsp(tsX)[1] + (k-2) / 12
stL <- tsp(tsXL)[1] + (k-2) / 12

for(i in 1:(n-k))
{
     xshort   <- window(tsX, end=st + i/12)
     xnext    <- window(tsX, start=st + (i+1)/12, end=st + (i+p)/12)
     xshortL  <- window(tsXL, end=stL + i/12)
     xnextL   <- window(tsXL, start=stL + (i+1)/12, end=stL + (i+p)/12)

     fit1a    <- tslm(xshortL ~ trend + season, lambda=0)
     fc1a     <- forecast(fit1a, h=p)
     fit2a    <- auto.arima(xshortL)    # Re-fit each time through for optimal accuracy
     fc2a     <- forecast(fit2a, h=p)
     fit3a    <- ets(xshort)            # Re-fit each time through for optimal accuracy
     fc3a     <- forecast(fit3a, h=p)
     fit4a    <- HoltWinters(xshort)    # Re-fit each time through for optimal accuracy
     fc4a     <- forecast(fit4a, h=p)

     # Inverse-Log (exponentiate) the 2 forecasts that used the Log transformation
     fc1a$mean  <- exp(fc1a$mean)
     fc1a$lower <- exp(fc1a$lower)
     fc1a$upper <- exp(fc1a$upper)
     fc1a$x     <- exp(fc1a$x)
     fc2a$mean  <- exp(fc2a$mean)
     fc2a$lower <- exp(fc2a$lower)
     fc2a$upper <- exp(fc2a$upper)
     fc2a$x     <- exp(fc2a$x)

     mae1a[i, 1:length(xnext)] <- abs(fc1a$mean - xnext)
     mae2a[i, 1:length(xnext)] <- abs(fc2a$mean - xnext)
     mae3a[i, 1:length(xnext)] <- abs(fc3a$mean - xnext)
     mae4a[i, 1:length(xnext)] <- abs(fc4a$mean - xnext)

     cia1a[i, 1:length(xnext)] <- (xnext > fc1a$lower[1:length(xnext),2]) & (xnext < fc1a$upper[1:length(xnext),2])
     cia2a[i, 1:length(xnext)] <- (xnext > fc2a$lower[1:length(xnext),2]) & (xnext < fc2a$upper[1:length(xnext),2])
     cia3a[i, 1:length(xnext)] <- (xnext > fc3a$lower[1:length(xnext),2]) & (xnext < fc3a$upper[1:length(xnext),2])
     cia4a[i, 1:length(xnext)] <- (xnext > fc4a$lower[1:length(xnext),2]) & (xnext < fc4a$upper[1:length(xnext),2])

     cib1a[i, 1:length(xnext)] <- (xnext > fc1a$lower[1:length(xnext),1]) & (xnext < fc1a$upper[1:length(xnext),1])
     cib2a[i, 1:length(xnext)] <- (xnext > fc2a$lower[1:length(xnext),1]) & (xnext < fc2a$upper[1:length(xnext),1])
     cib3a[i, 1:length(xnext)] <- (xnext > fc3a$lower[1:length(xnext),1]) & (xnext < fc3a$upper[1:length(xnext),1])
     cib4a[i, 1:length(xnext)] <- (xnext > fc4a$lower[1:length(xnext),1]) & (xnext < fc4a$upper[1:length(xnext),1])

     intb1a[i, 1:length(xnext)] <- (fc1a$upper[1:length(xnext),1] - fc1a$lower[1:length(xnext),1])
     intb2a[i, 1:length(xnext)] <- (fc2a$upper[1:length(xnext),1] - fc2a$lower[1:length(xnext),1])
     intb3a[i, 1:length(xnext)] <- (fc3a$upper[1:length(xnext),1] - fc3a$lower[1:length(xnext),1])
     intb4a[i, 1:length(xnext)] <- (fc4a$upper[1:length(xnext),1] - fc4a$lower[1:length(xnext),1])

     plot(forecast(fc3a, 6))                      # Plots the HISTORICAL + FORECAST for next 6 months (with confidence intervals)
     lines(xnext, col="red")                      # Plots the ACTUAL values for next 6 months
}

pdf("mae1.pdf")
plot(1:6, colMeans(mae1a, na.rm=TRUE), type="l", col="orange2", main="Forecast Accuracy:\n6-Month, Mean Absolute Error", xlab="Horizons (Months Ahead)", ylab="MAE", ylim=c(0, 2000000), xaxs="i", yaxs="i")
lines(1:6, colMeans(mae2a, na.rm=TRUE), type="l", col="green")
lines(1:6, colMeans(mae3a, na.rm=TRUE), type="l", col="blue")
lines(1:6, colMeans(mae4a, na.rm=TRUE), type="l", col="red")
legend("topleft", legend=c("LM", "ARIMA", "ETS", "HW"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

# plot(1:6, colMeans(cia1, na.rm=TRUE), type="l", col="orange2", xlab="Horizon", ylab="% Within Confidence Interval", ylim=c(0, 1))
# lines(1:6, colMeans(cia2, na.rm=TRUE), type="l", col="green")
# lines(1:6, colMeans(cia3, na.rm=TRUE), type="l", col="blue")
# lines(1:6, colMeans(cia4, na.rm=TRUE), type="l", col="red")
# legend("bottomright", legend=c("LM (95)", "ARIMA (95)", "ETS (97.5)", "HW (95)"), col=c("orange2","green","blue","red"), lty=1)

pdf("inCI.pdf")
plot(1:6, colMeans(cib1a, na.rm=TRUE), type="l", col="orange2", main="Percentage of Forecast\nWithin Confidence Interval", xlab="Horizons (Months Ahead)", ylab="% Accuracy", ylim=c(0, 1))
lines(1:6, colMeans(cib2a, na.rm=TRUE), type="l", col="green")
lines(1:6, colMeans(cib3a, na.rm=TRUE), type="l", col="blue")
lines(1:6, colMeans(cib4a, na.rm=TRUE), type="l", col="red")
legend("bottomright", legend=c("LM (80)", "ARIMA (80)", "ETS (90)", "HW (80)"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

pdf("ciRange.pdf")
plot(1:6, colMeans(intb1a, na.rm=TRUE), type="l", col="orange2", main="Confidence Interval: Range", xlab="Horizons (Months Ahead)", ylab="$ Value Between Upper and Lower Limits", ylim=c(0,10000000))
lines(1:6, colMeans(intb2a, na.rm=TRUE), type="l", col="green")
lines(1:6, colMeans(intb3a, na.rm=TRUE), type="l", col="blue")
lines(1:6, colMeans(intb4a, na.rm=TRUE), type="l", col="red")
legend("topright", legend=c("LM (80)", "ARIMA (80)", "ETS (90)", "HW (80)"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

plot(forecast(fc1a, 6), ylim=c(0,50000000))
plot(forecast(fc2a, 6), ylim=c(0,50000000))
plot(forecast(fc3a, 6), ylim=c(0,50000000))
plot(forecast(fc4a, 6), ylim=c(0,50000000))

pdf("fit.pdf")
plot(dfMTS, main="Model Fit vs. Historical Data (In Sample)", xlab="Year", ylab="Monthly Revenue")
legend("topleft", legend=c("LM", "ARIMA", "ETS", "HW"), col=c("orange2","green","blue","red"), lty=1)
lines(exp(fitted(fit1a)), col="orange2")
lines(exp(fitted(fit2a)), col="green")
lines(fit3a$fitted, col="blue")
lines(fit4a$fitted[,"xhat"], col="red")
dev.off()

#---------------------------------------------------------------------------------------------------------#
#         OPTION 2: (Only data since 2012)
#---------------------------------------------------------------------------------------------------------#
tsX  <- dfMTS.12   
tsXL <- dfMTS.12L   

k <- 24                       # The minimum number of months to begin training with
p <- 6                        # The time period to compare actual vs. forecast (in months)
n <- length(tsX)              # The total # of months in the dataset

mae1b  <- mae2b  <- mae3b  <- mae4b  <- matrix(NA, n-k, p)
cib1b  <- cib2b  <- cib3b  <- cib4b  <- matrix(NA, n-k, p)
intb1b <- intb2b <- intb3b <- intb4b <- matrix(NA, n-k, p)

st  <- tsp(tsX)[1] + (k-2) / 12
stL <- tsp(tsXL)[1] + (k-2) / 12

for(i in 1:(n-k))
{
     xshort  <- window(tsX, end=st + i/12)
     xnext   <- window(tsX, start=st + (i+1)/12, end=st + (i+p)/12)
     xshortL <- window(tsXL, end=stL + i/12)
     xnextL  <- window(tsXL, start=stL + (i+1)/12, end=stL + (i+p)/12)
     fit1b   <- tslm(xshortL ~ trend + season, lambda=0)
     fc1b    <- forecast(fit1b, h=p)
     fit2b   <- Arima(xshortL, order=c(0,1,0), seasonal=list(order=c(1,0,0), period=12), include.drift=TRUE) # For Option 2
     fc2b    <- forecast(fit2b, h=p)
     fit3b   <- ets(xshort, model="MMN", damped=FALSE, alpha=0.9999, beta=0.0004) # For Option 2
     fc3b    <- forecast(fit3b, h=p)
     fit4b   <- HoltWinters(xshort, alpha=0.6085406, beta=0.3613982, gamma=0.4264201) # For Option 2
     fc4b    <- forecast(fit4b, h=p)

     # Inverse-Log (exponentiate) the 2 forecasts that used the Log transformation
     fc1b$mean  <- exp(fc1b$mean)
     fc1b$lower <- exp(fc1b$lower)
     fc1b$upper <- exp(fc1b$upper)
     fc1b$x     <- exp(fc1b$x)
     fc2b$mean  <- exp(fc2b$mean)
     fc2b$lower <- exp(fc2b$lower)
     fc2b$upper <- exp(fc2b$upper)
     fc2b$x     <- exp(fc2b$x)

     mae1b[i, 1:length(xnext)]  <- abs(fc1b$mean - xnext)
     mae2b[i, 1:length(xnext)]  <- abs(fc2b$mean - xnext)
     mae3b[i, 1:length(xnext)]  <- abs(fc3b$mean - xnext)
     mae4b[i, 1:length(xnext)]  <- abs(fc4b$mean - xnext)

     cib1b[i, 1:length(xnext)]  <- (xnext > fc1b$lower[1:length(xnext),1]) & (xnext < fc1b$upper[1:length(xnext),1])
     cib2b[i, 1:length(xnext)]  <- (xnext > fc2b$lower[1:length(xnext),1]) & (xnext < fc2b$upper[1:length(xnext),1])
     cib3b[i, 1:length(xnext)]  <- (xnext > fc3b$lower[1:length(xnext),1]) & (xnext < fc3b$upper[1:length(xnext),1])
     cib4b[i, 1:length(xnext)]  <- (xnext > fc4b$lower[1:length(xnext),1]) & (xnext < fc4b$upper[1:length(xnext),1])

     intb1b[i, 1:length(xnext)] <- (fc1b$upper[1:length(xnext),1] - fc1b$lower[1:length(xnext),1])
     intb2b[i, 1:length(xnext)] <- (fc2b$upper[1:length(xnext),1] - fc2b$lower[1:length(xnext),1])
     intb3b[i, 1:length(xnext)] <- (fc3b$upper[1:length(xnext),1] - fc3b$lower[1:length(xnext),1])
     intb4b[i, 1:length(xnext)] <- (fc4b$upper[1:length(xnext),1] - fc4b$lower[1:length(xnext),1])

     plot(forecast(fc4b, 6))                       # Plots the HISTORICAL + FORECAST for next 6 months (with confidence intervals)
     lines(xnext, col="red")                      # Plots the ACTUAL values for next 6 months
}

pdf("mae1.pdf")
plot (1:6, colMeans(mae1b, na.rm=TRUE), type="l", col="orange2", main="Forecast Accuracy:\n6-Month, Mean Absolute Error", xlab="Horizons (Months Ahead)", ylab="MAE", ylim=c(0, 5000000)) # 2,372,428
lines(1:6, colMeans(mae2b, na.rm=TRUE), type="l", col="green") # 1,821,362
lines(1:6, colMeans(mae3b, na.rm=TRUE), type="l", col="blue")  # 1,479,971
lines(1:6, colMeans(mae4b, na.rm=TRUE), type="l", col="red")   # 1,472,877
legend("topleft", legend=c("LM", "ARIMA", "ETS", "HW"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

pdf("inCI1.pdf")
plot (1:6, colMeans(cib1b, na.rm=TRUE), type="l", col="orange2", main="Percentage of Forecast\nWithin Confidence Interval", xlab="Horizons (Months Ahead)", ylab="% Accuracy", ylim=c(0, 1))
lines(1:6, colMeans(cib2b, na.rm=TRUE), type="l", col="green")
lines(1:6, colMeans(cib3b, na.rm=TRUE), type="l", col="blue")
lines(1:6, colMeans(cib4b, na.rm=TRUE), type="l", col="red")
legend("topright", legend=c("LM (80)", "ARIMA (80)", "ETS (90)", "HW (80)"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

pdf("ciRange1.pdf")
plot (1:6, colMeans(intb1b, na.rm=TRUE), type="l", col="orange2", main="Confidence Interval: Range", xlab="Horizons (Months Ahead)", ylab="$ Value Between Upper and Lower Limits", ylim=c(0,10000000))
lines(1:6, colMeans(intb2b, na.rm=TRUE), type="l", col="green")
lines(1:6, colMeans(intb3b, na.rm=TRUE), type="l", col="blue")
lines(1:6, colMeans(intb4b, na.rm=TRUE), type="l", col="red")
legend("topright", legend=c("LM (80)", "ARIMA (80)", "ETS (90)", "HW (80)"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

plot(forecast(fc1b, 6), ylim=c(0,50000000))
plot(forecast(fc2b, 6), ylim=c(0,50000000))
plot(forecast(fc3b, 6), ylim=c(0,50000000))
plot(forecast(fc4b, 6), ylim=c(0,50000000))

accuracy(fit1b) # Log transformed data
accuracy(fit2b) # Log transformed data
accuracy(fit3b) # Raw data
accuracy(fit4b) # Raw data

pdf("fit1.pdf")
plot(dfMTS, main="Model Fit vs. Historical Data (In Sample)", xlab="Year", ylab="Monthly Revenue")
legend("topleft", legend=c("LM", "ARIMA", "ETS", "HW"), col=c("orange2","green","blue","red"), lty=1)
lines(exp(fitted(fit1b)), col="orange2")
lines(exp(fitted(fit2b)), col="green")
lines(fit3b$fitted, col="blue")
lines(fit4b$fitted[,"xhat"], col="red")
dev.off()

#---------------------------------------------------------------------------------------------------------#
#    STEP 2E: COMPARE MAE FROM FULL DATASET TO 2012 ONWARD DATASET
#---------------------------------------------------------------------------------------------------------#

mae1 <- mae1a[35:49,]         # Remove the first 35 instances, so we can compare apples to apples
mae2 <- mae2a[35:49,]         # Remove the first 35 instances, so we can compare apples to apples
mae3 <- mae3a[35:49,]         # Remove the first 35 instances, so we can compare apples to apples
mae4 <- mae4a[35:49,]         # Remove the first 35 instances, so we can compare apples to apples

mean(mae1[,1], na.rm=TRUE)    # Average error 1st forecast (ALL)  = 7,159,827 (Crappy)
mean(mae1b[,1], na.rm=TRUE)   # Average error 1st forecast (2012) = 1,659,262 (Improved quite a bit - understandable, more linear)

mean(mae2[,1], na.rm=TRUE)    # Average error 1st forecast (ALL)  =   967,236 
mean(mae2b[,1], na.rm=TRUE)   # Average error 1st forecast (2012) =   734,236 (Also improved a bit - also more linear)

mean(mae3[,1], na.rm=TRUE)    # Average error 1st forecast (ALL)  =   806,456
mean(mae3b[,1], na.rm=TRUE)   # Average error 1st forecast (2012) =   759,081 (Slight improvement each month)

mean(mae3[,2], na.rm=TRUE)    # Average error 2nd forecast (ALL)  = 1,089,609
mean(mae3b[,2], na.rm=TRUE)   # Average error 2nd forecast (2012) = 1,006,076 

mean(mae3[,3], na.rm=TRUE)    # Average error 3rd forecast (ALL)  = 1,589,977
mean(mae3b[,3], na.rm=TRUE)   # Average error 3rd forecast (2012) = 1,350,274

mean(mae4[,1], na.rm=TRUE)    # Average error 1st forecast (ALL)  =   671,749 (Only model that got worse each month)
mean(mae4b[,1], na.rm=TRUE)   # Average error 1st forecast (2012) =   729,201

mean(mae4[,2], na.rm=TRUE)    # Average error 2nd forecast (ALL)  =   849,009
mean(mae4b[,2], na.rm=TRUE)   # Average error 2nd forecast (2012) =   967,442

mean(mae4[,3], na.rm=TRUE)    # Average error 3rd forecast (ALL)  = 1,146,577
mean(mae4b[,3], na.rm=TRUE)   # Average error 3rd forecast (2012) = 1,366,751

mean(colMeans(mae3, na.rm=TRUE))   # OVERALL Average error = 1,612,073
mean(colMeans(mae3b, na.rm=TRUE))  # OVERALL Average error = 1,479,972

mean(colMeans(mae4, na.rm=TRUE))   # OVERALL Average error = 1,336,978
mean(colMeans(mae4b, na.rm=TRUE))  # OVERALL Average error = 1,472,877

# Compare most recent three 1st forecasts for each dataset
mae3b[13,1]; mae4b[13,1]           # ETS beats 3 months ago
mae3b[14,1]; mae4b[14,1]           # ETS beats 2 months ago
mae3b[15,1]; mae4b[15,1]           # HW beats last month

mae3[13,1]; mae4a[13,1]            # ETS beats 3 months ago
mae3[14,1]; mae4a[14,1]            # ETS beats 2 months ago
mae3[15,1]; mae4a[15,1]            # HW beats last month

#---------------------------------------------------------------------------------------------------------#
#    STEP 2F: FINAL MODEL RECOMMENDATIONS
#
#         When selecting the final the following criteria were considered:
#         * Lowest Log Likelihood - LL
#         * Lowest Akaike's Information Criterion - AIC
#         * Lowest Mean Absolute Error - MAE
#         * Highest accuracy inside confidence interval vs. range of confidence interval
#         * Parsimony
#         * Best fit (linear vs. log; additive vs. multiplicative, heteroskedastic)
#         * Find the optimal dataset/subset (full vs. subset)
#---------------------------------------------------------------------------------------------------------#
# We decided to use the full dataset ("a" vs. "b"), since it worked better for the exponential models
# We also decided to use one of the two exponential models (ETS or HW)
fit3a     # ETS! Best overall, particularly for forecasting. Use smaller, 2012 dataset/model
fit4a     # HW!  Best accuracy. Use full dataset/model

etsFC <- forecast(fc3a, 6)         # Write ETS forecast out into table
sink('etsFC.txt')
etsFC                              # Save forecast (can change to different format later if needed)
sink() 

hwFC  <- forecast(fc4a, 6)         # Write HW forecast out into table
sink('hwFC.txt')
hwFC                               # Save forecast (can change to different format later if needed)
sink() 

pdf("fit3a.pdf")
plot(fc3a)                         # ETS graph
dev.off()

pdf("fit4a.pdf")
plot(fc4a)                         # HW graph
dev.off()

#=========================================================================================================#
#    TASK #3: LEASE PAYBACK ESTIMATION
#=========================================================================================================#
#         Historical data analysis (Subset by month of origination (FundedDate))
#         * For each month of funded loans, determine:
#           - How much of that amount was paid (Cleared) in the current and each future month (also determine %'s)
#           - How about how much of the amount was returned (Returned) in the current and future months
#           - Repeat for each historical month (probably 6 years * 12 months worth of data points)
#           - Summarize the data from all 72 months into some sort of trend
#           - Apply that trend to unseen, new Monthly funding amounts
#=========================================================================================================#
#---------------------------------------------------------------------------------------------------------#
#    STEP 3A: SUBSET HISTORICAL DATA (FOR THINGS WE NEED IN THIS ANALYSIS)
#         * We are eliminating the current (incomplete) month - includes partial payments gathered for month
#---------------------------------------------------------------------------------------------------------#
df1 <- df[df$PaymentStatus %in% c("CLEARED", "ORIGINATION"),]  # Only look at payments that went through
df1 <- df1[, c("TransactionDate", "FundedDate", "FundedAmount", "PaymentStatus", "xYear", "xMonth", "xPay", "fYear", "fMonth")]
df1 <- subset(df1, as.integer(as.POSIXct(TransactionDate, format = "%Y-%m-%d")) <= as.integer(as.POSIXct(useDate, format = "%Y-%m-%d"))) # Don't include the current/partial month

#---------------------------------------------------------------------------------------------------------#
#    STEP 3B: CREATE MATRICES/DATAFRAMES AND PREPARE DATA
#         * Set the "horiz" variable to the number of months to evaluate (must match PLOTS below!!!)
#---------------------------------------------------------------------------------------------------------#
months <- seq(from=min(df1$TransactionDate, na.rm=TRUE), to=useDate, by='months' )
horiz  <- 31 # Number of months into the future (+1) to continue looking for payments (31 for 30 month window, 19 for 18 month window, 13 for 12 month window)

payPc <- payDc <- payD <- payP <- data.frame(matrix(NA, length(months), horiz + 3)) # Add columns for Year, Month & TOTAL (in case we want to use them)

colnames(payD)[1]       <- colnames(payP)[1]       <- colnames(payDc)[1]       <- colnames(payPc)[1]       <- "Current"
colnames(payD)[horiz+1] <- colnames(payP)[horiz+1] <- colnames(payDc)[horiz+1] <- colnames(payPc)[horiz+1] <- "Year"
colnames(payD)[horiz+2] <- colnames(payP)[horiz+2] <- colnames(payDc)[horiz+2] <- colnames(payPc)[horiz+2] <- "Month"
colnames(payD)[horiz+3] <- colnames(payP)[horiz+3] <- colnames(payDc)[horiz+3] <- colnames(payPc)[horiz+3] <- "TOTAL"

for(i in 2:horiz){colnames(payD)[i] <- colnames(payP)[i] <- colnames(payDc)[i] <- colnames(payPc)[i] <- paste0("m", i-1)}

# Initialize Year/Month variables to 1st month to begin capturing monthly funding amounts
iYear  <- year(min(df1$TransactionDate, na.rm=TRUE))
iMonth <- month(min(df1$TransactionDate, na.rm=TRUE))

for(i in seq_along(months))
{
     # Determine the TOTAL funded amount for the current month (Subset for iYear=fYear & iMonth=fMonth)
     df2 <- df1[(df1$fYear == iYear) & (df1$fMonth == iMonth),]
     payD[i, horiz+1] <- payP[i, horiz+1] <- payDc[i, horiz+1] <- payPc[i, horiz+1] <- iYear
     payD[i, horiz+2] <- payP[i, horiz+2] <- payDc[i, horiz+2] <- payPc[i, horiz+2] <- iMonth
     payD[i, horiz+3] <- sum(df2$FundedAmount[df2$PaymentStatus=="ORIGINATION"]) # Total funded this month

     # Now loop through the next "horiz" months to see how much came back in each
     jYear  <- iYear
     jMonth <- iMonth
     
     for(j in 1:horiz) 
     {
          payD[i, j]  <- sum(df2$xPay[(df2$xYear == jYear) & (df2$xMonth == jMonth)])     
          payP[i, j]  <- payD[i, j] / payD[i, horiz+3] # Get the % (of the TOTAL) paid back this month
          payDc[i, j] <- sum(payD[i, 1:j])             # Cumulative percentages
          payPc[i, j] <- sum(payP[i, 1:j])             # Cumulative percentages

          if(jMonth == 12){jYear <- jYear + 1; jMonth <- 1}else{jMonth <- jMonth + 1} # Roll thru all month/year combos
     }     

     payP[i, horiz+3] <- sum(payP[i, 1:horiz], na.rm=TRUE)

     if(iMonth == 12){iYear <- iYear+1; iMonth <- 1}else{iMonth <- iMonth+1} # Roll thru all month/year combos (TransactionDate)
}

# Now let's combine/aggregate the payD and payP DF's by years
payD.agg   <- aggregate(payD, by=list(payD$Year), FUN=sum)
payP.agg   <- aggregate(payP, by=list(payP$Year), FUN=mean)
payDc.agg  <- aggregate(payDc, by=list(payD$Year), FUN=sum)
payPc.agg  <- aggregate(payPc, by=list(payP$Year), FUN=mean) # Also save a copy for forecasting

# ...and now by months
payD.aggm  <- aggregate(payD, by=list(payD$Month), FUN=sum)
payP.aggm  <- aggregate(payP, by=list(payP$Month), FUN=mean)
payDc.aggm <- aggregate(payDc, by=list(payD$Month), FUN=sum)
payPc.aggm <- aggregate(payPc, by=list(payP$Month), FUN=mean)

# Remove columns that do not contain actual payment values (will mess up some future calculations)
payPc.agg$Month <- payPc.aggm$Month <- payD.aggm$Month <- NULL
payPc.agg$Year  <- payPc.aggm$Year  <- payD.aggm$Year  <- NULL
payPc.agg$TOTAL <- payPc.aggm$TOTAL <- payD.aggm$TOTAL <- NULL

# Put the data in a form that "ggplot" likes
mdf   <- melt(payPc.agg, id.vars="Group.1", value.name="Return", variable.name="Month")
mdfm  <- melt(payPc.aggm, id.vars="Group.1", value.name="Return", variable.name="Horizon")
mdfDm <- melt(payD.aggm, id.vars="Group.1", value.name="Return", variable.name="Horizon")

colnames(mdfm)[1] <- colnames(mdfDm)[1] <- "Month"
colnames(mdf)[1]  <- "Year"

#---------------------------------------------------------------------------------------------------------#
#    STEP 3C: VARIOUS PLOTS
#         These plots require different subsets of information to execute.
#         PLEASE MAKE SURE THE "horiz" is set, re-run (all of step 3B) and prepped 
#              at the beginning of 3B above for the corresponding plot below!!!
#---------------------------------------------------------------------------------------------------------#
# Plot by year (large window - set "horiz" = 31 months and re-run everything up to here)
# REMEMBER TO SET "horiz" = 31 for this PLOT!!!!!! (m20 doesn't exist in 12 or 18 month window - will post an Error)
p30 <- ggplot(data=mdf, aes(x=Month, y=Return, group = Year, colour = Year)) +
     ggtitle("Figure 3.0: Cumulative, 30-month ROI (for each month of year)") + 
     scale_color_gradient2(low="orange", mid="red", high="blue3", midpoint=2012) +
     theme(
          panel.background = element_blank(),
          axis.text.x      = element_text(angle=45, vjust=1),
          axis.line        = element_line(size=1, colour="grey50"),
          axis.line.y      = element_blank(),
          panel.grid.major = element_blank()) +
     geom_line(size=1.5) +
     ylab("Return Percentage (Multiplier)") +
     xlab("Horizon (in monthly increments)") +
     geom_hline(yintercept = 1, color="red", linetype="dashed") +
     geom_vline(xintercept = as.numeric(c(mdf$Month[22], mdf$Month[50], mdf$Month[60], mdf$Month[85], mdf$Month[170])), color="grey50", linetype="dashed") +
     annotate("text", label = "Break-Even", x=3.9, y=1.03, size=4, colour="red") +
     annotate("text", label = "90-Day Period", x=3.3, y=0.82, size=4, colour="grey50", angle=90) +
     annotate("text", label = "12-Month Period", x=13.6, y=0.82, size=4, colour="grey50", angle=90) +
     annotate("text", label = "24-Month Period", x=25.6, y=0.82, size=4, colour="grey50", angle=90) +
     geom_text(data = mdf[mdf$Month == "m20",], aes(label = Year, hjust = 1.0, vjust = -0.5))

pdf("p30.pdf")
p30
dev.off()

# Plot by year (small window - set "horiz" = 13 months and re-run everything up to here)
# REMEMBER TO SET "horiz" = 13 for this PLOT!!!!!! (the plot axis is illegible for "horiz" > 13)
p31 <- ggplot(data=mdf, aes(x=Month, y=Return, group = Year, colour = Year)) +
     ggtitle("Figure 3.1: Cumulative, 12-month ROI (for each month of year)") + 
     scale_color_gradient2(low="orange", mid="red", high="blue3", midpoint=2012) +
     theme(
          panel.background = element_blank(),
          axis.title.x     = element_blank(),
          axis.line        = element_line(size=1, colour="grey50"),
          axis.line.y      = element_blank(),
          panel.grid.major = element_blank()) +
     geom_line(size=1.2) +
     ylab("Return Percentage (Multiplier)") +
     geom_hline(yintercept = 1, color="red", linetype="dashed") +
     geom_vline(xintercept = as.numeric(c(mdf$Month[22], mdf$Month[50], mdf$Month[60])), color="grey50", linetype="dashed") +
     annotate("text", label = "Break-Even", x=6.0, y=1.03, size=4, colour="red") +
     annotate("text", label = "90-Day Period", x=3.6, y=0.82, size=4, colour="grey50", angle=90) +
     geom_text(data = mdf[mdf$Month == "m11",], aes(label = Year, hjust = 1.0, vjust = 1.5))

# Create Plot Table (right below the graph, by combining with "mmplot")
# REMEMBER TO SET "horiz" = 13 for this PLOT!!!!!! (this table is illegible for "horiz" > 13)
t31 <- ggplot(data=mdf, aes(x=Month, y=Year, label=format(round(Return, 2), nsmall=1), colour=Year)) +
     scale_color_gradient2(low = "orange", mid="red", high = "blue3", midpoint=2012) +
     theme(
          legend.position  = "none",
          axis.title       = element_blank(),
          axis.text.x      = element_blank(),
          axis.ticks       = element_blank(),
          plot.margin      = unit(c(0, 6.0, 0, 0.6), "lines"), # The white space around the table - so it lines up with plot above
          panel.background = element_blank(),
          panel.border     = element_blank()) +
     scale_y_continuous(breaks = c(2009, 2010, 2011, 2012, 2013, 2014, 2015), labels = c("2009", "2010", "2011", "2012", "2013", "2014", "2015")) +
     geom_text(size = 3.5)

# Now place the table below the graph
pdf("p31.pdf")
mmplot(p31, t31)
dev.off()

# Plot by month (large window - set "horiz" = 31 months and re-run everything up to here)
# REMEMBER TO SET "horiz" = 31 for this PLOT!!!!!! (m20 doesn't exist in 12 or 18 month window - will post an Error)
p32 <- ggplot(data=mdfm, aes(x=Horizon, y=Return, group=Month, colour=Month)) +
     ggtitle("Figure 3.2: Cumulative, 30-month ROI (based on originating month)") + 
     scale_color_gradient2(low = "orange", mid="red", high = "blue3", midpoint=6) +
     theme(
          panel.background = element_blank(),
          axis.text.x      = element_text(angle=45, vjust=1),
          axis.line        = element_line(size = 1, colour = "grey50"),
          axis.line.y      = element_blank(),
          panel.grid.major = element_blank()) +
     geom_line(size=1.2) +
     ylab("Return Percentage (Multiplier)") +
     xlab("Horizon (in monthly increments)") +
     geom_hline(yintercept = 1, color="red", linetype="dashed") +
     geom_vline(xintercept = as.numeric(c(mdf$Month[22], mdf$Month[50], mdf$Month[75], mdf$Month[85], mdf$Month[170])), color="grey50", linetype="dashed") +
     annotate("text", label = "Break-Even", x = 3.9, y = 1.03, size = 4, colour = "red") +
     annotate("text", label = "90-Day Period", x=3.3, y=0.82, size=4, colour="grey50", angle=90) +
     annotate("text", label = "12-Month Period", x=13.6, y=0.82, size=4, colour="grey50", angle=90) +
     annotate("text", label = "24-Month Period", x=25.6, y=0.82, size=4, colour="grey50", angle=90) +
     geom_text(data = mdfm[mdfm$Horizon == "m20",], aes(label=Month, hjust = 1.0, vjust = 1.2))

pdf("p32.pdf")
p32
dev.off()

# Plot by month (will work for "horiz" set to any value > 13)
p33 <- ggplot(data=mdfDm, aes(x=Horizon, y=Return, group=Month, colour=Month)) +
     ggtitle("Figure 3.3: Monthly ROI (based on originating month)") + 
     scale_color_gradient2(low = "orange", mid="red", high = "blue3", midpoint=6) +
     theme(
          panel.background = element_blank(),
          axis.text.x      = element_text(angle=45, vjust=1),
          axis.line        = element_line(size = 1, colour = "grey50"),
          axis.line.y      = element_blank(),
          panel.grid.major = element_blank()) +
     geom_line(size=1.2) +
     ylab("$ Return On Investment") +
     xlab("Horizon (in monthly increments)") +
     geom_vline(xintercept = as.numeric(c(mdfDm$Horizon[40], mdfDm$Horizon[150])), color="grey50", linetype="dashed") +
     geom_text(data = mdfDm[mdfDm$Horizon == "m3",], aes(label=Month, hjust = 1.0, vjust = 1.5))

pdf("p33.pdf")
p33
dev.off()

#---------------------------------------------------------------------------------------------------------#
#    STEP 3D: PREP FORECASTING MODEL DATA FOR LEASE PAYBACK
#---------------------------------------------------------------------------------------------------------#
# Need to remove any data within the past 18 months (misleading results - haven't yet collected completely on these)
fcA   <- payP.agg                       # Start with these percentages (for saving month by month totals)
fcB   <- payPc.agg                      # Start with these percentages (for saving cumulative totals)

fcB$Group.1 <- fcA$Group.1 <- fcA$Year <- fcA$Month <- fcA$TOTAL <- NULL # Remove these unneeded columns

newA <- newB <- 18                      # Number of months of data to remove (must be >= 12 months)
newA <- newB <- newA-month(useDate)     # Adjust number of months remaining after removal of current year (since we'll remove current year next)

fcA <- head(fcA, -1)                    # Remove all months from current year (<= 12 months) (keep all but last row)
fcB <- head(fcB, -1)                    # Remove all months from current year (<= 12 months) (keep all but last row)

for(i in 1:nrow(fcA))
{
     if(newA > 12)
     {
          fcA <- head(fcA, -1)
          newA <- newA-12
     }else{
          if((ncol(fcA)-newA) > 12)
          {
               fcA[nrow(fcA)-1, (12+13-newA+1):ncol(fcA)] <- NA  # overwrite the balance of the previous year too
               fcA[nrow(fcA), (13-newA+1):ncol(fcA)] <- NA       # overwrite the balance of this year
               break
          }else{
               fcA[nrow(fcA), (13-newA+1):ncol(fcA)] <- NA
               break
          }
     }
} # Overwrite invalid data (months) with "NA", will throw out later

for(i in 1:nrow(fcB))
{
     if(newB > 12)
     {
          fcB <- head(fcB, -1)
          newB <- newB-12
     }else{
          if((ncol(fcB)-newB) > 12)
          {
               fcB[nrow(fcB)-1, (12+13-newB+1):ncol(fcB)] <- NA  # overwrite the balance of the previous year too
               fcB[nrow(fcB), (13-newB+1):ncol(fcB)] <- NA       # overwrite the balance of this year
               break
          }else{
               fcB[nrow(fcB), (13-newB+1):ncol(fcB)] <- NA
               break
          }
     }
} # Overwrite invalid data (months) with "NA", will throw out later

fc1 <- fc2 <- data.frame(matrix(NA, 3, ncol(fcA))) # Create new data.frames for the results
colnames(fc1) <- colnames(fc2) <- colnames(fcA)    # Copy column names

fc1[1,] <- colMin(fcA)                  # Find minimums and write out to the new data.frame
fc1[2,] <- colMeans(fcA, na.rm=TRUE)    # Find means and write out to the new data.frame
fc1[3,] <- colMax(fcA)                  # Find maximums and write out to the new data.frame
fMXt    <- fc1                          # Save for multiplying final investment against

fc2[1,] <- colMin(fcB)                  # Find minimums and write out to the new data.frame
fc2[2,] <- colMeans(fcB, na.rm=TRUE)    # Find means and write out to the new data.frame
fc2[3,] <- colMax(fcB)                  # Find maximums and write out to the new data.frame
fCXt    <- fc2                          # Save for multiplying final investment against

#---------------------------------------------------------------------------------------------------------#
#    STEP 3E: PLOT FORECASTING MODEL, MONTHLY RETURNS INCLUDING MAX AND MIN
#---------------------------------------------------------------------------------------------------------#
fc1[,14] <- c("Minimum", "Mean", "Maximum") # Create new column with corresponding values
colnames(fc1)[14] <- "Forecast"             # Label the new column

fc1X <- melt(fc1, id.vars="Forecast", value.name="Return", variable.name="Horizon") # Prepare data for graphing

# Plot by month
pMX <- ggplot(data=fc1X, aes(x=Horizon, y=Return, group=Forecast, colour=Forecast)) +
     ggtitle("Figure 3.5: Lease Payback Model (per Month)") + 
     scale_color_manual(values=c("grey50", "red4", "grey70")) +
     theme(
          panel.background = element_blank(),
          axis.text.x      = element_text(angle=45, vjust=1),
          axis.line        = element_line(size = 1, colour = "grey50"),
          axis.line.y      = element_blank(),
          panel.grid.major = element_blank()) +
     geom_line(size=1.2) +
     ylab("Return Percentage (Multiplier)") +
     xlab("Horizon (in monthly increments)") +
     annotate("text", label = "90-Day Period", x=3.3, y=0.15, size=4, colour="grey50", angle=90) +
     annotate("text", label = "12-Month Period", x=13.6, y=0.15, size=4, colour="grey50", angle=90) +
     geom_vline(xintercept = as.numeric(c(fc1X$Horizon[10], fc1X$Horizon[38])), color="grey50", linetype="dashed")

pdf("pMX.pdf")
pMX
dev.off()

#---------------------------------------------------------------------------------------------------------#
#    STEP 3F: PLOT FORECASTING MODEL, CUMULATIVE RETURNS INCLUDING MAX AND MIN
#---------------------------------------------------------------------------------------------------------#
fc2[,14] <- c("Minimum", "Mean", "Maximum") # Create new column with corresponding values
colnames(fc2)[14] <- "Forecast"             # Label the new column

fc2X <- melt(fc2, id.vars="Forecast", value.name="Return", variable.name="Horizon") # Prepare data for graphing

# Plot by month
pCX <- ggplot(data=fc2X, aes(x=Horizon, y=Return, group=Forecast, colour=Forecast)) +
     ggtitle("Figure 3.4: Lease Payback Model (Cumulative)") + 
     scale_color_manual(values=c("grey50", "red4", "grey70")) +
     theme(
          panel.background = element_blank(),
          axis.text.x      = element_text(angle=45, vjust=1),
          axis.line        = element_line(size = 1, colour = "grey50"),
          axis.line.y      = element_blank(),
          panel.grid.major = element_blank()) +
     geom_hline(yintercept = 1, color="red", linetype="dashed") +
     geom_line(size=1.2) +
     ylab("Return Percentage (Multiplier)") +
     xlab("Horizon (in monthly increments)") +
     annotate("text", label = "Break-Even", x = 3.9, y = 1.03, size = 4, colour = "red") +
     annotate("text", label = "90-Day Period", x=3.3, y=0.82, size=4, colour="grey50", angle=90) +
     annotate("text", label = "12-Month Period", x=13.6, y=0.82, size=4, colour="grey50", angle=90) +
     geom_vline(xintercept = as.numeric(c(fc2X$Horizon[10], fc2X$Horizon[38])), color="grey50", linetype="dashed")

pdf("pCX.pdf")
pCX
dev.off()

#---------------------------------------------------------------------------------------------------------#
#    STEP 3G: NOW APPLY PERCENTAGES TO DOLLARS FOR THE ROI ESTIMATES
#         Need to run sections 3A, 3B and 3D before I run this code to create "fMXt"
#---------------------------------------------------------------------------------------------------------#
# Initialize date sequence variables (to get lengths and names)
yDate  <- seq(from=floor_date(min(dfA$TransactionDate, na.rm=TRUE), "month"), to=fcDate, by='months' )
zDate  <- seq(from=floor_date(min(dfA$FundedDate, na.rm=TRUE), "month"), to=fcDate, by='months' )

yDate <- yDate[-1] # THIS IS REALLY IMPORTANT!!!! We need to remove the first value so it MATCHES the removal in df2M!!!!!!
zDate <- zDate[-1] # THIS IS REALLY IMPORTANT!!!! We need to remove the first value so it MATCHES the removal in df2M!!!!!!

# Initialize dataframes for Funded Amount payment receiving forecasting
fundAmount <- data.frame(matrix(0, ncol=length(yDate), nrow=length(zDate)+1))

# Initialize column and row.names for dataframes (note row.name option depending on ORDER)
names(fundAmount)     <- format(yDate, "%b %Y")
row.names(fundAmount) <- c(format(zDate, "%b %Y"),"Total") # Show newest date first

for(i in 1:nrow(df2M)){
#      fundAmount[i, i:(i+ncol(fMXt)-1)] <- fMXt[2,] * df2M$FundedAmount[i]
     fundAmount[i, i:if((i+ncol(fMXt)-1) >= ncol(fundAmount)){ncol(fundAmount)}else{i+ncol(fMXt)-1}] <- fMXt[2,] * df2M$FundedAmount[i]
     fundAmount[nrow(fundAmount), i] <- sum(fundAmount[1:(nrow(fundAmount)-1), i])
}

#---------------------------------------------------------------------------------------------------------#
#    STEP 3H: FORECAST LEASE PAYBACK BASED ON AN INITIAL (SPECIFIED) INVESTMENT
#---------------------------------------------------------------------------------------------------------#
investment <- 20000000             # Initial investment to be made this month
fCX <- fCXt * investment           # Cumulative roll-out (each month added to previous month)
fMX <- fMXt * investment           # Monthly roll-out (each month separate)

row.names(fCX)[1] <- row.names(fMX)[1] <- "Minimum"
row.names(fCX)[2] <- row.names(fMX)[2] <- "Mean"
row.names(fCX)[3] <- row.names(fMX)[3] <- "Maximum"

write.csv(file="ROI Forecast-CUM.csv", x=fCX)
write.csv(file="ROI Forecast-MO.csv", x=fMX)




























#=========================================================================================================#
#    TASK #4: RE-DO PAYMENT PREDICTION
#=========================================================================================================#
#---------------------------------------------------------------------------------------------------------#
#    STEP 4A: TRANSFORM DATA INTO MONTHS (AGGREGATE OBSERVATIONS - KEEP ALL STILL)
#         Similar to STEP 2A above, only instead of aggregating Transaction Amounts, we are going
#         to aggregate Funded Amounts. This will give us a history of amounts funded for each month.
#---------------------------------------------------------------------------------------------------------#
df2  <- dfA[dfA$PaymentStatus %in% c("ORIGINATION"),]
df2M <- df2[, c("FundedDate", "FundedAmount")]
df2M$FundedDate <- floor_date(df2M$FundedDate, "month")       # Group into months
df2M <- aggregate(FundedAmount ~ FundedDate, df2M, FUN = sum) # Now sum payments by month
df2M <- df2M[-1,]                                             # Remove the first row so it matches the Transaction Amount DF

#---------------------------------------------------------------------------------------------------------#
#    STEP 4B: FIRST, CREATE A LIST OF SEASONAL TABLES (ONE FOR EACH MONTH - TRANSACTION AMOUNT BREAKDOWN)
#         FILL IN TABLES BASED ON HORIZON NUMBER OF MONTHS (18?)
#         * Do not look at recent months (receipt of payments are incomplete) for 13 months previous
#         * Fill in percSeasonal list of 12 tables (one for each month). Each table is "intYear" + 2 rows long
#         * Begin filling in Transaction Payment Amounts for a given Monthly Funding in each subsequent month
#         * Loop through and do this for each month and each year for an "mGood" horizon of months
#         * Then in the fall through loop below, get a total of all payments per table (12), then calculate %'s
#---------------------------------------------------------------------------------------------------------#
# We may want to change this to 12 months...let's try it first and see what it looks like
# newDate <- useDate-365-(365/2)     # Don't look at values newer than 18 months as we haven't had time to collect on these
newDate <- useDate-365-31          # Don't look at values newer than 13 months as we haven't had time to collect on these
mGood   <- 18                      # Look out into the horizon for this many months for Transaction Payments

# Initialize Year/Month variables for comparing TransactionDates (iYear/iMonth)
iYear  <- year(min(dfA$TransactionDate, na.rm=TRUE))
iMonth <- 1 # always start from the month of January

# Create 2 lists:   1) Matrices to hold Payment Amounts for loans originating on a specific month (Jan = list[[1]], Feb= list[[2]], etc.)
#                   2) Vectors to hold total and summaries, including %'s
d1 <- d2 <- d3 <- d4 <- d5 <- d6 <- d7 <- d8 <- d9 <- d10 <- d11 <- d12 <- data.frame(matrix(0, ncol=mGood, nrow= 2+(intYears <- year(max(dfA$FundedDate))-year(min(dfA$FundedDate)))))
percSeasonal <- list(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12)
d1 <- d2 <- d3 <- d4 <- d5 <- d6 <- d7 <- d8 <- d9 <- d10 <- d11 <- d12 <- vector(mode="numeric", length=2+(intYears <- year(max(dfA$FundedDate))-year(min(dfA$FundedDate))))
percSeasonalT <- list(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12)

for(i in 1:12)
{
     row.names(percSeasonal[[i]])[1] <- "Monthly %"
     row.names(percSeasonal[[i]])[2] <- "Monthly Sum"
     for(j in 1:intYears){row.names(percSeasonal[[i]])[j+2] <- paste0("Year: ", iYear+j-1)}
}

# Fill in the "percSeasonal" tables based on the "horizon" and "intYears" variables
for(i in 1:intYears)
{
     for(k in 1:12)
     {
          jYear  <- iYear
          jMonth <- iMonth

          # Capture a "horizon" worth of data (oldest date first) for this particular month
          for(j in 1:mGood)
          {
               # Only fill table for valid values (not more recent than newDate)
               if((newDate - as.Date(paste0(iYear,"-",iMonth,"-1"))) > 0)
               { 
                    percSeasonal[[k]][(i+2), j] <- sum(dfA$xPay[(dfA$xYear == jYear & dfA$xMonth == jMonth) & (dfA$fYear == iYear & dfA$fMonth == iMonth)], na.rm=TRUE)
               }

               # Roll thru all month/year combos (FundedDate)
               if(jMonth == 12){jYear <- jYear + 1; jMonth <- 1}else{jMonth <- jMonth + 1} 

               # Are we on the last year? If so, let's add up the columns, store sums in row #2
               if(i==intYears){percSeasonal[[k]][2, j] <- sum(percSeasonal[[k]][3:(2+intYears), j])}
          }

          if((newDate - as.Date(paste0(iYear,"-",iMonth,"-1"))) > 0)
          { 
               dfX <- dfA[(dfA$fYear == iYear) & (dfA$fMonth == iMonth),]
               percSeasonalT[[k]][(i+2)] <- sum(dfX$FundedAmount[dfX$PaymentStatus=="ORIGINATION"]) # Total funded this month
          }
          
          if(iMonth == 12){iYear <- iYear + 1; iMonth <- 1}else{iMonth <- iMonth + 1} # Roll thru all month/year combos (TransactionDate)
     }
}

# Run this section after preparing the matrices in the loop before. This loop will get monthly totals (amount funded), then % of return (monthly)
for(i in 1:12)
{
     percSeasonalT[[i]][2] <- sum(percSeasonalT[[i]][3:(2+intYears)]) # Sum the total amount funded for this origination month (over all years)
     
     for(j in 1:mGood)
     {
          percSeasonal[[i]][1, j] <- percSeasonal[[i]][2, j]/percSeasonalT[[i]][2] # Get the % return expected for each month after funding
     }

     percSeasonalT[[i]][1] <- sum(percSeasonal[[i]][1, (1:mGood)])    # Store the total return % in this vector, row #1
}

#---------------------------------------------------------------------------------------------------------#
#    STEP 4C: BUILD THE fundXAmount TABLE - DOLLAR PROJECTIONS BASED ON percSeasonal TABLE
#---------------------------------------------------------------------------------------------------------#
# Initialize date sequence variables (to get lengths and names)
yDate  <- seq(from=floor_date(min(dfA$TransactionDate, na.rm=TRUE), "month"), to=fcDate, by='months' )
zDate  <- seq(from=floor_date(min(dfA$FundedDate, na.rm=TRUE), "month"), to=fcDate, by='months' )

# DOUBLE CHECK AND MAKE SURE THAT df2M, fundXAmount AND THE 3 VARIABLES BELOW ALL START AT THE SAME MONTH!!!!
yDate  <- yDate[-1] # THIS IS REALLY IMPORTANT!!!! We need to remove the first value so it MATCHES the removal in df2M!!!!!!
zDate  <- zDate[-1] # THIS IS REALLY IMPORTANT!!!! We need to remove the first value so it MATCHES the removal in df2M!!!!!!
nMonth <- month(min(dfA$FundedDate))+1 # AS A RESULT, I ALSO NEED TO DO THIS (INCREMENT BY ONE)

# Initialize dataframes for Funded Amount payment receiving forecasting
fundXAmount <- data.frame(matrix(0, ncol=length(yDate), nrow=length(zDate)+1))

# Initialize column and row.names for dataframes (note row.name option depending on ORDER)
names(fundXAmount)     <- format(yDate, "%b %Y")
row.names(fundXAmount) <- c(format(zDate, "%b %Y"),"Total") # Show newest date first

for(i in 1:nrow(df2M)){
     fundXAmount[i, i:if((i+ncol(percSeasonal[[nMonth]])-1) >= ncol(fundXAmount)){ncol(fundXAmount)}else{i+ncol(percSeasonal[[nMonth]])-1}] <- percSeasonal[[nMonth]][1,] * df2M$FundedAmount[i]
     fundXAmount[nrow(fundXAmount), i] <- sum(fundXAmount[1:(nrow(fundXAmount)-1), i])
     nMonth <- (nMonth%%12)+1
}

#---------------------------------------------------------------------------------------------------------#
#    STEP 4D: SUBSET DATA BY GRABBING "WINDOWS" FOR FURTHER ANALYSIS
#         Turn into a "Time Series" object, then grab window of time
#---------------------------------------------------------------------------------------------------------#
# Since we don't have a complete month for April of 2015, lets stop in March of 2015
df2MTS    <- ts(df2M$FundedAmount, start=c(2009, 3), end=c(2015, 3), freq=12)  # Turn into "Time Series" object
df2MTS.12 <- window(df2MTS, start=c(2012, 1), end=c(2015, 3), freq=12) # Values from beginning of 2012

#---------------------------------------------------------------------------------------------------------#
#    STEP 4E: APPLY VARIOUS TRANSFORMATIONS
#---------------------------------------------------------------------------------------------------------#
# Look at some transformations (Log and Differencing and Log + Differencing)
df2MTS.d    <- diff(df2MTS, lag=1)             # First differenced
df2MTS.12d  <- diff(df2MTS.12, lag=1)          # First differenced

df2MTS.L    <- log(df2MTS)                     # Log transform applied
df2MTS.12L  <- log(df2MTS.12)                  # Log transform applied

df2MTS.Ld   <- diff(log(df2MTS), lag=1)        # Log transform + 1st differenced
df2MTS.12Ld <- diff(log(df2MTS.12), lag=1)     # Log transform + 1st differenced

df2MTS.dd   <- diff(df2MTS.d, lag=1)           # Second differenced

# Plot transformations
plot(df2MTS.d)                                # Still see heteroskedasticity
plot(df2MTS.dd)                               # No change
plot(df2MTS.L)                                # Still trend exists
plot(df2MTS.Ld)                               # Probably looks the best

#---------------------------------------------------------------------------------------------------------#
#    STEP 4F: PERFORM SOME EXPLORATORY ANALYSIS ON MONTHLY DATA USING DIFFERENT TRANSFORMATIONS AND MODELS
#         First, look at your date by plotting it
#         Second, decompose to look at the trend, seasonality
#         Third, remove trend and seasonality and look at residuals for stochastic structure
#         Fourth, compute autocorrelation and autocovariance (check for stationarity)
#---------------------------------------------------------------------------------------------------------#
# Look at monthly plots for both large and smaller datasets
pdf("fData.pdf")
plot(df2MTS)              # Clearly heteroskedastic - probably apply log transform/BoxCox (+ maybe differencing)
dev.off()

pdf("sData.pdf")
plot(df2MTS.12)           # Clearly heteroskedastic - probably apply log transform/BoxCox (+ maybe differencing)
dev.off()

# Since the plot looks roughly the same as it did before, we'll skip the exploratory analysis and assume:
# 1) The data is exponentially increasing (although this data levels out the last few months)
# 2) The data is non-stationary as it is.
# 3) We need to add either a exponential algorithm, or pre-process using differencing and/or log transforms

# Decompose into Trend, Seasonal and Random components - plot
# The trend and seasonal components can be modeled and predicted in a fairly straightforward manner (linear/reg, non-linear/log/exp/polynomial)
# The random component is neither systematic nor predictable and thus is where the work of smoothing and other methods are employed to explain behavior
decomp <- decompose(df2MTS)
plot(decomp)

# Holt Winters parameter explanation for our models:
# alpha = level smoother(closer to 1 means more recent data used to smooth levels, not weighing older values very much)
# beta  = trend smoother(closer to 0 means older data weighted more for trend, not weighing newer values very much)
# gamma = seasonal smoother(closer to 1 means more recent data used for seasonal smoothing, not weighing older values very much)

hwFit <- HoltWinters(df2MTS)                  # HW: alpha=0.7952748, beta=0.06045618, gamma=1
plot(forecast(hwFit, 6))

hwFit12 <- HoltWinters(df2MTS.12)             # HW: alpha=0.8195081, beta=0.06330932, gamma=0
plot(forecast(hwFit12, 6))

# Run somes auto ARIMA models for the different data sets
# ARIMA are linearly dependent models (linearly dependent on previous data points)
# Small k in MA is more responsive to recent changes in data and will have less of a smoothing effect
# Large k in MA is less response, and produce more of a pronounced lag in the smoothing sequence
df2MTSFit <- auto.arima(df2MTS)                # ARIMA(0,2,3)(0,1,0)[12], LL -852, AIC 1712
plot(forecast.Arima(df2MTSFit, h=6))

df2MTS.12Fit <- auto.arima(df2MTS.12)          # ARIMA(0,1,0)(0,1,0)[12], LL -389, AIC 780.27
plot(forecast.Arima(df2MTS.12Fit, h=6))

# Log transform first, then apply Arima (Full dataset)
aaFit <- auto.arima(df2MTS.L)                 # ARIMA(0,2,0)(1,0,0)[12], LL 28.37, AIC -52.74
plot(forecast.Arima(aaFit, h=6))             # Look at forecast of LOG data
aafc       <- forecast(aaFit, h=6)
aafc$mean  <- exp(aafc$mean)
aafc$upper <- exp(aafc$upper)
aafc$lower <- exp(aafc$lower)
aafc$x     <- exp(aafc$x)
plot(aafc)                                   # Plot inverse log (back to original) data (not so great using full dataset)

# Log transform first, then apply Arima (Only data after 2012)
aa12Fit <- auto.arima(df2MTS.12L)             # ARIMA(0,1,0)(1,0,0)[12], LL 36.45, AIC -66.9
plot(forecast.Arima(aa12Fit, h=6))           
aa12fc       <- forecast(aa12Fit, h=6)
aa12fc$mean  <- exp(aa12fc$mean)
aa12fc$upper <- exp(aa12fc$upper)
aa12fc$lower <- exp(aa12fc$lower)
aa12fc$x     <- exp(aa12fc$x)
plot(aa12fc)                                 # Plot inverse log (back to original) data (looks much better just using data > 2012)

# Now look at some algorithms that already work well with exponential trends (HW, ETS)
# Exponential smoothing algorithms (such as HW), weigh the more recent data points more strongly than others (such as in MA - mean of last k points -, where past observations are weighted equally)
# Exponential smoothing is commonly applied to financial market or economic data
hwFit <- HoltWinters(df2MTS)                  # HW: alpha=0.4080884, beta=0.6777756, gamma=1
hwFitL <- HoltWinters(df2MTS.L)               # HW: alpha=0.7616733, beta=0.001827628, gamma=1
summary(hwFit)
plot(forecast(hwFit, 6))

hwFit <- HoltWinters(df2MTS.12)               # HW: alpha=0.6085406, beta=0.3613982, gamma=0.4264201

# Exponential Smoothing (ETS) model w/ & w/o BoxCox transformation for stabilizing the variation in the variance
# etsFit  <- ets(df2MTS)                        # ETS: (M,Md,N) alpha=0.7507, beta=0.2868, phi=0.8, drift=TRUE
etsFit  <- ets(df2MTS)                        # ETS: (M,M,N) alpha=0.7596, beta=0.0001, drift=FALSE
etsFitL <- ets(df2MTS.L)                      # ETS: (M,Md,N) alpha=0.9997, beta=0.8806, phi=0.8, drift=TRUE
summary(etsFit)
plot(forecast(etsFit, 6))

etsFit <- ets(df2MTS.12)                      # ETS: (M,M,N) alpha=0.9999, beta=0.1e-04, drift=FALSE

lam <- BoxCox.lambda(df2MTS)
bcFit <- ets(df2MTS, additive=TRUE, lambda=lam)
fcbc <- forecast(bcFit, h=6)
plot(fcbc)

# Okay, after exploring things a bit, create the best options here
# I've also created my OWN model to compare against the others
# Let's horse race all of them against each other.
# this is now a model to forecast the AMOUNT FUNDED (vs. the AMOUNT RECEIVED) for each future month based on the past
# On TOP of this model, we'll overlay the payout for the FUNDED AMOUNTS, which will give us a much
# more accurate forecast of what we'll eventually RECEIVE.

#---------------------------------------------------------------------------------------------------------#
#    STEP 4G: CROSS VALIDATION INCREMENTING A MONTH AT A TIME - FORECAST VS. ACTUAL FOR 6 MONTH CHUNKS
#         Find the best model for the "Funded Amount" dataset and compare to see who has the lowest MAE
#---------------------------------------------------------------------------------------------------------#
tsX  <- df2MTS   
tsXL <- df2MTS.L   

k <- 24                       # The minimum number of months to begin training with
p <- 6                        # The time period to compare actual vs. forecast (in months)
n <- length(tsX)              # The total # of months in the dataset

mae1a  <- mae2a  <- mae3a  <- mae4a  <- mae5a  <- matrix(NA, n-k, p)
cia1a  <- cia2a  <- cia3a  <- cia4a  <- matrix(NA, n-k, p)
cib1a  <- cib2a  <- cib3a  <- cib4a  <- matrix(NA, n-k, p)
intb1a <- intb2a <- intb3a <- intb4a <- matrix(NA, n-k, p)

fc5a <- matrix(NA, p, 3)

st  <- tsp(tsX)[1] + (k-2) / 12
stL <- tsp(tsXL)[1] + (k-2) / 12

for(i in 1:(n-k))
{
     xshort   <- window(tsX, end=st + i/12)
     xnext    <- window(tsX, start=st + (i+1)/12, end=st + (i+p)/12)
     xshortL  <- window(tsXL, end=stL + i/12)
     xnextL   <- window(tsXL, start=stL + (i+1)/12, end=stL + (i+p)/12)

     fit1a    <- HoltWinters(xshort, alpha=0.7, beta=0.2, gamma=1) # BEST
     fc1a     <- forecast(fit1a, h=p)
     fit2a    <- Arima(xshortL, order=c(1,1,0), include.drift=TRUE) # BEST
     fc2a     <- forecast(fit2a, h=p)   
     fit3a    <- ets(xshort, model="ANN", opt.crit="mae") # BEST
     fc3a     <- forecast(fit3a, h=p)
     fit4a    <- HoltWinters(xshort, alpha=0.85, beta=0.15, gamma=1) # BEST
     fc4a     <- forecast(fit4a, h=p)

     fc5a[1,1] <- ((((2*xshort[k+i-1])+(1*xshort[k+i-2]))/3)+(fc5a[1,2]<-((xshort[k+i-1]-xshort[k+i-7])/6))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[2,1] <- ((((2*xshort[k+i-1])+(1*xshort[k+i-2]))/3)+(fc5a[2,2]<-((xshort[k+i-1]-xshort[k+i-6])/5))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[3,1] <- ((((2*xshort[k+i-1])+(1*xshort[k+i-2]))/3)+(fc5a[3,2]<-((xshort[k+i-1]-xshort[k+i-5])/4))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[4,1] <- ((((2*xshort[k+i-1])+(1*xshort[k+i-2]))/3)+(fc5a[4,2]<-((xshort[k+i-1]-xshort[k+i-4])/3))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[5,1] <- ((((2*xshort[k+i-1])+(1*xshort[k+i-2]))/3)+(fc5a[5,2]<-((xshort[k+i-1]-xshort[k+i-3])/2))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[6,1] <- ((((2*xshort[k+i-1])+(1*xshort[k+i-2]))/3)+(fc5a[6,2]<-((xshort[k+i-1]-xshort[k+i-2])/1))) # weighted, moving average (2 lags) + linear regression fit over 6 months

     # Inverse-Log (exponentiate) the 2 forecasts that used the Log transformation
     fc2a$mean  <- exp(fc2a$mean)
     fc2a$lower <- exp(fc2a$lower)
     fc2a$upper <- exp(fc2a$upper)
     fc2a$x     <- exp(fc2a$x)

     mae1a[i, 1:length(xnext)] <- abs(fc1a$mean - xnext)
     mae2a[i, 1:length(xnext)] <- abs(fc2a$mean - xnext)
     mae3a[i, 1:length(xnext)] <- abs(fc3a$mean - xnext)
     mae4a[i, 1:length(xnext)] <- abs(fc4a$mean - xnext)
     mae5a[i, 1:length(xnext)] <- abs(fc5a[1:length(xnext), 1] - xnext)

#      cib1a[i, 1:length(xnext)] <- (xnext > fc1a$lower[1:length(xnext),1]) & (xnext < fc1a$upper[1:length(xnext),1])
     cib2a[i, 1:length(xnext)] <- (xnext > fc2a$lower[1:length(xnext),1]) & (xnext < fc2a$upper[1:length(xnext),1])
     cib3a[i, 1:length(xnext)] <- (xnext > fc3a$lower[1:length(xnext),1]) & (xnext < fc3a$upper[1:length(xnext),1])
     cib4a[i, 1:length(xnext)] <- (xnext > fc4a$lower[1:length(xnext),1]) & (xnext < fc4a$upper[1:length(xnext),1])

#      intb1a[i, 1:length(xnext)] <- (fc1a$upper[1:length(xnext),1] - fc1a$lower[1:length(xnext),1])
     intb2a[i, 1:length(xnext)] <- (fc2a$upper[1:length(xnext),1] - fc2a$lower[1:length(xnext),1])
     intb3a[i, 1:length(xnext)] <- (fc3a$upper[1:length(xnext),1] - fc3a$lower[1:length(xnext),1])
     intb4a[i, 1:length(xnext)] <- (fc4a$upper[1:length(xnext),1] - fc4a$lower[1:length(xnext),1])

     plot(forecast(fc1a, 6))                      # Plots the HISTORICAL + FORECAST for next 6 months (with confidence intervals)
     lines(xnext, col="red")                      # Plots the ACTUAL values for next 6 months
}

pdf("maeFA.pdf")
plot(1:6, colMeans(mae1a, na.rm=TRUE), type="l", col="black", main="Forecast Accuracy:\n6-Month, Mean Absolute Error", xlab="Horizons (Months Ahead)", ylab="MAE", ylim=c(0, 2000000), xaxs="i", yaxs="i") # 3,766,570
lines(1:6, colMeans(mae2a, na.rm=TRUE), type="l", col="blue") # 1,096,437
lines(1:6, colMeans(mae3a, na.rm=TRUE), type="l", col="green")  #   609,871
lines(1:6, colMeans(mae4a, na.rm=TRUE), type="l", col="red")   #   523,310
lines(1:6, colMeans(mae5a, na.rm=TRUE), type="l", col="orange2") #   523,310
legend("topleft", legend=c("LM", "ARIMA", "ETS", "HW", "AW"), col=c("black","blue","green","red","orange2"), lty=1)
dev.off()

pdf("inCI.pdf")
plot(1:6, colMeans(cib1a, na.rm=TRUE), type="l", col="orange2", main="Percentage of Forecast\nWithin Confidence Interval", xlab="Horizons (Months Ahead)", ylab="% Accuracy", ylim=c(0, 1))
lines(1:6, colMeans(cib2a, na.rm=TRUE), type="l", col="green")
lines(1:6, colMeans(cib3a, na.rm=TRUE), type="l", col="blue")
lines(1:6, colMeans(cib4a, na.rm=TRUE), type="l", col="red")
legend("bottomright", legend=c("LM (80)", "ARIMA (80)", "ETS (90)", "HW (80)"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

pdf("ciRange.pdf")
plot(1:6, colMeans(intb1a, na.rm=TRUE), type="l", col="orange2", main="Confidence Interval: Range", xlab="Horizons (Months Ahead)", ylab="$ Value Between Upper and Lower Limits", ylim=c(0,10000000))
lines(1:6, colMeans(intb2a, na.rm=TRUE), type="l", col="green")
lines(1:6, colMeans(intb3a, na.rm=TRUE), type="l", col="blue")
lines(1:6, colMeans(intb4a, na.rm=TRUE), type="l", col="red")
legend("topright", legend=c("LM (80)", "ARIMA (80)", "ETS (90)", "HW (80)"), col=c("orange2","green","blue","red"), lty=1)
dev.off()

plot(forecast(fc1a, 6), ylim=c(0,50000000))
plot(forecast(fc2a, 6), ylim=c(0,50000000))
plot(forecast(fc3a, 6), ylim=c(0,50000000))
plot(forecast(fc4a, 6), ylim=c(0,50000000))

pdf("fit.pdf")
plot(df2MTS, main="Model Fit vs. Historical Data (In Sample)", xlab="Year", ylab="Monthly Revenue")
legend("topleft", legend=c("LM", "ARIMA", "ETS", "HW"), col=c("orange2","green","blue","red"), lty=1)
lines(fit1a$fitted[,"xhat"], col="orange2")
lines(exp(fitted(fit2a)), col="green")
lines(fit3a$fitted, col="blue")
lines(fit4a$fitted[,"xhat"], col="red")
dev.off()


# didn't change anything below here for this analysis....may want to revisit this dataset, or just ignore for now...

#---------------------------------------------------------------------------------------------------------#
#    STEP 4H: CROSS VALIDATION INCREMENTING A MONTH AT A TIME - FORECAST VS. ACTUAL
#         Find the best model for each algorithm family and compare to see who has the lowest MAE
#         Try two models:
#              1) Average Month by Year (AMY)
#              2) Average Month by Month (AMM)
#---------------------------------------------------------------------------------------------------------#
tsX  <- dfMTS                 # Monthly aggregation of Transaction Amounts
tsXL <- dfMTS.L               # Monthly aggregation of Transaction Amounts + Log transformed
ts2X <- df2MTS                # Monthly aggregation of Funded Amounts

k <- 24                       # The minimum number of months to begin training with
p <- 6                        # The time period to compare actual vs. forecast (in months)
n <- length(tsX)              # The total # of months in the dataset

mae1a  <- mae2a  <- mae3a  <- mae4a  <- mae5a  <- mae6a  <- mae7a  <- mae8a  <- matrix(NA, n-k, p)

fc5a <- matrix(NA, p+2, 15)
fc6a <- matrix(NA, p+2, 15)
fc7a <- matrix(NA, p+2, 15)
fc8a <- matrix(NA, p+2, 15)

st  <- tsp(tsX)[1] + (k-2) / 12
stL <- tsp(tsXL)[1] + (k-2) / 12
st2 <- tsp(ts2X)[1] + (k-2) / 12

for(i in 1:(n-k))
{
     xshort   <- window(tsX, end=st + i/12)
     xnext    <- window(tsX, start=st + (i+1)/12, end=st + (i+p)/12)
     xshortL  <- window(tsXL, end=stL + i/12)
     xnextL   <- window(tsXL, start=stL + (i+1)/12, end=stL + (i+p)/12)
     xshort2  <- window(ts2X, end=st2 + i/12)
     xnext2   <- window(ts2X, start=st2 + (i+1)/12, end=st + (i+p)/12)

     fit2a    <- auto.arima(xshortL) # For Option 1
     fc2a     <- forecast(fit2a, h=p)
     fit3a    <- ets(xshort, opt.crit="mae") # For Option 1
     fc3a     <- forecast(fit3a, h=p)
     fit4a    <- HoltWinters(xshort) # For Option 1
     fc4a     <- forecast(fit4a, h=p)

     # So first we need to forecast the Funded Amount (forecasts above are for the Transaction Amount)
     # Look at historical data, fill in trend and weighted average
     fc5a[1,1] <- ((((2*xshort2[k+i-1])+(1*xshort2[k+i-2]))/3)+(fc5a[1,2]<-((xshort2[k+i-1]-xshort2[k+i-7])/6))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[2,1] <- ((((2*xshort2[k+i-1])+(1*xshort2[k+i-2]))/3)+(fc5a[2,2]<-((xshort2[k+i-1]-xshort2[k+i-6])/5))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[3,1] <- ((((2*xshort2[k+i-1])+(1*xshort2[k+i-2]))/3)+(fc5a[3,2]<-((xshort2[k+i-1]-xshort2[k+i-5])/4))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[4,1] <- ((((2*xshort2[k+i-1])+(1*xshort2[k+i-2]))/3)+(fc5a[4,2]<-((xshort2[k+i-1]-xshort2[k+i-4])/3))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[5,1] <- ((((2*xshort2[k+i-1])+(1*xshort2[k+i-2]))/3)+(fc5a[5,2]<-((xshort2[k+i-1]-xshort2[k+i-3])/2))) # weighted, moving average (2 lags) + linear regression fit over 6 months
     fc5a[6,1] <- ((((2*xshort2[k+i-1])+(1*xshort2[k+i-2]))/3)+(fc5a[6,2]<-((xshort2[k+i-1]-xshort2[k+i-2])/1))) # weighted, moving average (2 lags) + linear regression fit over 6 months

     fc7a <- fc6a <- fc5a

     fit6a    <- ets(xshort2, model="MNN", opt.crit="mae")
     fc6x     <- forecast(fit6a, h=p)
     fc6a[1:p,1] <- fc6x$mean

     for(j in 1:p){
          fc5a[7,(3+j)] <- sum(fundXAmount[1:(i+k-1),(i+k+j-1)]) # All historical transactions up to (but not including) current month
     }

     for(j in 1:p){
          fc6a[7,(3+j)] <- sum(fundXAmount[1:(i+k-1),(i+k+j-1)]) # All historical transactions up to (but not including) current month
     }

     for(j in 1:p){
          fc7a[7,(3+j)] <- sum(fundXAmount[1:(i+k-1),(i+k+j-1)]) # All historical transactions up to (but not including) current month
     }

# Here is an example of what this fc5a/fc6a table should look like if it is built correctly:
#          [,1]     [,2] [,3]       [,4]       [,5]       [,6]       [,7]       [,8]       [,9]     [,10]    [,11]    [,12]    [,13]   [,14]
# [1,] 165598.4 17736.23   NA   7833.608  25687.430  29080.895  23845.128  10096.603  10809.130        NA       NA       NA       NA      NA
# [2,] 189238.0 23639.61   NA         NA   8460.272  30330.624  30444.435  26958.856  13585.203  11561.39       NA       NA       NA      NA
# [3,] 196448.7 24946.91   NA         NA         NA   7381.932  29409.137  30428.138  30659.622  12851.74 12241.64       NA       NA      NA
# [4,] 186960.9 14151.78   NA         NA         NA         NA   6168.031  28492.533  31619.906  27494.24 12495.98 12146.66       NA      NA
# [5,] 165491.0  3477.00   NA         NA         NA         NA         NA   5243.192  24669.956  25981.25 23940.21 12246.61 10535.51      NA
# [6,] 194123.3 42784.09   NA         NA         NA         NA         NA         NA   7220.735  29082.12 31388.37 28166.20 12614.63 12811.8
# [7,]       NA       NA   NA 112429.267  80002.939  66380.580  45310.854  38624.126  36560.022        NA       NA       NA       NA      NA
# [8,]       NA       NA   NA 120262.875 114150.641 133174.031 135177.585 139843.448 155124.574        NA       NA       NA       NA      NA

     nMonth <- month(as.Date(xnext))[1]

     for(y in 1:p)
     {
          # Now get the historical % lease payback from model for next "p" months (6) after the Funding Date, then multiply by Funded Amount to get forecast
          # Let's save the 6 new forecasts in columns 6, 7, 8, 9, 10 and 11 of "fc5a", then SUM them into column 12
          for(j in 1:p){
#                fc5a[y,(2+j+y)]  <- fc5a[y,1]*percSeasonal[[nMonth]][1,j] # Funded Amount * percent transaction to receive = NEW Transaction Amount (forecast)
               fc5a[y,(2+j+y)]  <- fc5a[y,1]*fMXt[2,j] # This historical projection actually look better
          } # The "mean" is in the 2nd row of this matrix
     
          # Now sum the historical numbers plus the NEW Transaction Amounts forecasted from the NEW Funded Amounts
          fc5a[8,(3+y)] <- sum(fc5a[1:7,(3+y)], na.rm=TRUE)
          nMonth <- (nMonth%%12)+1
     }

     nMonth <- month(as.Date(xnext))[1]

     for(y in 1:p)
     {
          # Now get the historical % lease payback from model for next "p" months (6) after the Funding Date, then multiply by Funded Amount to get forecast
          # Let's save the 6 new forecasts in columns 6, 7, 8, 9, 10 and 11 of "fc5a", then SUM them into column 12
          for(j in 1:p){
               fc6a[y,(2+j+y)]  <- fc6a[y,1]*percSeasonal[[nMonth]][1,j] # Funded Amount * percent transaction to receive = NEW Transaction Amount (forecast)
#                fc6a[y,(2+j+y)]  <- fc6a[y,1]*fMXt[2,j] # Funded Amount * percent transaction to receive = NEW Transaction Amount (forecast)
          } # The "mean" is in the 2nd row of this matrix
     
          # Now sum the historical numbers plus the NEW Transaction Amounts forecasted from the NEW Funded Amounts
          fc6a[8,(3+y)] <- sum(fc6a[1:7,(3+y)], na.rm=TRUE)
          nMonth <- (nMonth%%12)+1
     }

     nMonth <- month(as.Date(xnext))[1]

     for(y in 1:p)
     {
          # Now get the historical % lease payback from model for next "p" months (6) after the Funding Date, then multiply by Funded Amount to get forecast
          # Let's save the 6 new forecasts in columns 6, 7, 8, 9, 10 and 11 of "fc5a", then SUM them into column 12
          for(j in 1:p){
               fc7a[y,(2+j+y)]  <- fc7a[y,1]*percSeasonal[[nMonth]][1,j] # Funded Amount * percent transaction to receive = NEW Transaction Amount (forecast)
#                fc7a[y,(2+j+y)]  <- fc7a[y,1]*fMXt[2,j] # Funded Amount * percent transaction to receive = NEW Transaction Amount (forecast)
          } # The "mean" is in the 2nd row of this matrix
     
          # Now sum the historical numbers plus the NEW Transaction Amounts forecasted from the NEW Funded Amounts
          fc7a[8,(3+y)] <- sum(fc7a[1:7,(3+y)], na.rm=TRUE)
          nMonth <- (nMonth%%12)+1
     }

     fc1a       <- fundXAmount[80, (i+24):(i+29)] # This requires that I run some other code before it works here!!!!
     fc8a       <- fundAmount[80, (i+24):(i+29)]  # This requires that I run some other code before it works here!!!!

     # Inverse-Log (exponentiate) the 2 forecasts that used the Log transformation
     fc2a$mean  <- exp(fc2a$mean)
     fc2a$lower <- exp(fc2a$lower)
     fc2a$upper <- exp(fc2a$upper)
     fc2a$x     <- exp(fc2a$x)

     # Now figure out the Mean Absolute Errors
     mae1a[i, 1:length(xnext)] <- abs(as.numeric(fc1a[1, 1:length(xnext)]) - xnext)
     mae2a[i, 1:length(xnext)] <- abs(fc2a$mean - xnext)
     mae3a[i, 1:length(xnext)] <- abs(fc3a$mean - xnext)
     mae4a[i, 1:length(xnext)] <- abs(fc4a$mean - xnext)
     mae5a[i, 1:length(xnext)] <- abs(as.numeric(fc5a[8, 4:(3+length(xnext))]) - xnext)
     mae6a[i, 1:length(xnext)] <- abs(as.numeric(fc6a[8, 4:(3+length(xnext))]) - xnext)
     mae7a[i, 1:length(xnext)] <- abs(as.numeric(fc7a[8, 4:(3+length(xnext))]) - xnext)
     mae8a[i, 1:length(xnext)] <- abs(as.numeric(fc8a[1, 1:length(xnext)]) - xnext)

     plot(forecast(fc3a, 6))                      # Plots the HISTORICAL + FORECAST for next 6 months (with confidence intervals)
     lines(xnext, col="red")                      # Plots the ACTUAL values for next 6 months
}

pdf("maeFinal.pdf")
plot(1:6, colMeans(mae1a, na.rm=TRUE), lty="dashed", type="l", col="black", main="Forecast Accuracy:\n6-Month, Mean Absolute Error", xlab="Horizons (Months Ahead)", ylab="MAE", ylim=c(0, 1000000), xaxs="i", yaxs="i")
lines(1:6, colMeans(mae2a, na.rm=TRUE), type="l", col="dodgerblue1")  #ARIMA
lines(1:6, colMeans(mae3a, na.rm=TRUE), type="l", col="royalblue1")   #ETS
lines(1:6, colMeans(mae4a, na.rm=TRUE), type="l", col="blue")         #HW
lines(1:6, colMeans(mae5a, na.rm=TRUE), type="l", col="orange2")      #AWAW
lines(1:6, colMeans(mae6a, na.rm=TRUE), type="l", col="green")        #ETSAW
lines(1:6, colMeans(mae7a, na.rm=TRUE), type="l", col="red")          #HWAW
lines(1:6, colMeans(mae8a, na.rm=TRUE), lty="dashed", type="l", col="grey")
text(x=4.5, y=270000, "Average Monthly by Year (AMY)", col="grey")
text(x=4.5, y=140000, "Average Monthly by Month (AMM)")
legend("topleft", legend=c("ARIMA", "ETS", "HW", "AW/AMM", "ETS/AMM", "HW/AMM"), col=c("dodgerblue1","royalblue1","blue","orange2","green","red"), lty=1)
dev.off()

#---------------------------------------------------------------------------------------------------------#
#    MISCELLANEOUS RESEARCH AND DISCOVERY
#---------------------------------------------------------------------------------------------------------#
# Validate the spike we see in April
# Unfortunately, because revenue keep increasing each year, the spike in April doesn't really show up in any of this data...
sum(df1[df1$xYear<2012 & df1$xMonth=="1",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="2",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="3",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="4",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="5",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="6",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="7",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="8",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="9",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="10",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="11",]$xPay)
sum(df1[df1$xYear<2012 & df1$xMonth=="12",]$xPay)

length(df1[df1$xYear<2012 & df1$xMonth=="1",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="2",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="3",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="4",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="5",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="6",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="7",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="8",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="9",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="10",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="11",]$xPay)
length(df1[df1$xYear<2012 & df1$xMonth=="12",]$xPay)

sum(df1[df1$xYear==2012 & df1$xMonth=="1",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="2",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="3",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="4",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="5",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="6",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="7",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="8",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="9",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="10",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="11",]$xPay)
sum(df1[df1$xYear==2012 & df1$xMonth=="12",]$xPay)

#*************************************************END*****************************************************
