x2009 <- data.frame(read.csv("coreLogicDataAppend.csv"))
colnames(x2009)[1] <- "DisplayID"

length(x2009$DisplayID[
  (x2009$FPD == 1 & x2009$AccountChargedOff == 0) | 
    (x2009$FPD == 0 & x2009$AccountChargedOff == 1) |
    (x2009$FPD == 1 & x2009$AccountChargedOff == 1)])
