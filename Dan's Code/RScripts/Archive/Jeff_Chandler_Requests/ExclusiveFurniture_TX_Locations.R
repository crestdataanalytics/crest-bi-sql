#This depends on DataProviderAnalysis_V2.R having been run


#Jeff wanted us to look at all of the TX locations for Exclusive Furniture separately and then together

#All TX Exclusive Furniture Locations
{
        edf <- df[df$RetailerID %in% c(8969,1831,1830,1829,2763,5026),]
        edf$CreationDate <- as.Date(edf$CreationDate, format = "%Y-%m-%d")
        edf$Vintage <- paste0(month(edf$CreationDate),"/",year(edf$CreationDate))
        
        edfTotalVolume <- aggregate(ID~Vintage, data = edf, FUN = length)
        colnames(edfTotalVolume)[2] <- "TotalVolume"
        
        edfIsApproved <- aggregate(IsApproved~Vintage, data = edf, FUN = sum)
        colnames(edfIsApproved)[2] <- "IsApproved"
        
        edfFundedIndicator <- aggregate(FundedIndicator~Vintage, data = edf, FUN = sum)
        colnames(edfFundedIndicator)[2] <- "FundedIndicator"
        
        edfSummary <- merge(x = edfTotalVolume, y = edfIsApproved, by = c("Vintage"), all.x = T)
        edfSummary$PercentApproved <- edfSummary$IsApproved / edfSummary$TotalVolume
        edfSummary <- merge(x = edfSummary, y = edfFundedIndicator, by = c("Vintage"), all.x = T)
        edfSummary$PercentFunded <- edfSummary$FundedIndicator / edfSummary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(edfSummary, file = "edfSummary.csv", quote = T, row.names = F)
}

#Exclusive Furniture (Grand Parkway)
{
        eGrandParkway <- df[df$RetailerID == 8969,]
        
        eGrandParkway$CreationDate <- as.Date(eGrandParkway$CreationDate, format = "%Y-%m-%d")
        eGrandParkway$Vintage <- paste0(month(eGrandParkway$CreationDate),"/",year(eGrandParkway$CreationDate))
        
        eGrandParkwayTotalVolume <- aggregate(ID~Vintage, data = eGrandParkway, FUN = length)
        colnames(eGrandParkwayTotalVolume)[2] <- "TotalVolume"
        
        eGrandParkwayIsApproved <- aggregate(IsApproved~Vintage, data = eGrandParkway, FUN = sum)
        colnames(eGrandParkwayIsApproved)[2] <- "IsApproved"
        
        eGrandParkwayFundedIndicator <- aggregate(FundedIndicator~Vintage, data = eGrandParkway, FUN = sum)
        colnames(eGrandParkwayFundedIndicator)[2] <- "FundedIndicator"
        
        eGrandParkwaySummary <- merge(x = eGrandParkwayTotalVolume, y = eGrandParkwayIsApproved, by = c("Vintage"), all.x = T)
        eGrandParkwaySummary$PercentApproved <- eGrandParkwaySummary$IsApproved / eGrandParkwaySummary$TotalVolume
        eGrandParkwaySummary <- merge(x = eGrandParkwaySummary, y = eGrandParkwayFundedIndicator, by = c("Vintage"), all.x = T)
        eGrandParkwaySummary$PercentFunded <- eGrandParkwaySummary$FundedIndicator / eGrandParkwaySummary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(eGrandParkwaySummary, file = "eGrandParkwaySummary.csv", quote = T, row.names = F)
        
        min(eGrandParkway$CreationDate)
}

#Exclusive Furniture (Gulf Fwy - 45)
{
        eGlufFwy45 <- df[df$RetailerID == 1831,]
        
        eGlufFwy45$CreationDate <- as.Date(eGlufFwy45$CreationDate, format = "%Y-%m-%d")
        eGlufFwy45$Vintage <- paste0(month(eGlufFwy45$CreationDate),"/",year(eGlufFwy45$CreationDate))
        
        eGlufFwy45TotalVolume <- aggregate(ID~Vintage, data = eGlufFwy45, FUN = length)
        colnames(eGlufFwy45TotalVolume)[2] <- "TotalVolume"
        
        eGlufFwy45IsApproved <- aggregate(IsApproved~Vintage, data = eGlufFwy45, FUN = sum)
        colnames(eGlufFwy45IsApproved)[2] <- "IsApproved"
        
        eGlufFwy45FundedIndicator <- aggregate(FundedIndicator~Vintage, data = eGlufFwy45, FUN = sum)
        colnames(eGlufFwy45FundedIndicator)[2] <- "FundedIndicator"
        
        eGlufFwy45Summary <- merge(x = eGlufFwy45TotalVolume, y = eGlufFwy45IsApproved, by = c("Vintage"), all.x = T)
        eGlufFwy45Summary$PercentApproved <- eGlufFwy45Summary$IsApproved / eGlufFwy45Summary$TotalVolume
        eGlufFwy45Summary <- merge(x = eGlufFwy45Summary, y = eGlufFwy45FundedIndicator, by = c("Vintage"), all.x = T)
        eGlufFwy45Summary$PercentFunded <- eGlufFwy45Summary$FundedIndicator / eGlufFwy45Summary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(eGlufFwy45Summary, file = "eGlufFwy45Summary.csv", quote = T, row.names = F)
}

#Exclusive Furniture (NW Fwy - 290)
{
        eNWFwy290 <- df[df$RetailerID == 1830,]
        
        eNWFwy290$CreationDate <- as.Date(eNWFwy290$CreationDate, format = "%Y-%m-%d")
        eNWFwy290$Vintage <- paste0(month(eNWFwy290$CreationDate),"/",year(eNWFwy290$CreationDate))
        
        eNWFwy290TotalVolume <- aggregate(ID~Vintage, data = eNWFwy290, FUN = length)
        colnames(eNWFwy290TotalVolume)[2] <- "TotalVolume"
        
        eNWFwy290IsApproved <- aggregate(IsApproved~Vintage, data = eNWFwy290, FUN = sum)
        colnames(eNWFwy290IsApproved)[2] <- "IsApproved"
        
        eNWFwy290FundedIndicator <- aggregate(FundedIndicator~Vintage, data = eNWFwy290, FUN = sum)
        colnames(eNWFwy290FundedIndicator)[2] <- "FundedIndicator"
        
        eNWFwy290Summary <- merge(x = eNWFwy290TotalVolume, y = eNWFwy290IsApproved, by = c("Vintage"), all.x = T)
        eNWFwy290Summary$PercentApproved <- eNWFwy290Summary$IsApproved / eNWFwy290Summary$TotalVolume
        eNWFwy290Summary <- merge(x = eNWFwy290Summary, y = eNWFwy290FundedIndicator, by = c("Vintage"), all.x = T)
        eNWFwy290Summary$PercentFunded <- eNWFwy290Summary$FundedIndicator / eNWFwy290Summary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(eNWFwy290Summary, file = "eNWFwy290Summary.csv", quote = T, row.names = F)
}


#Exclusive Furniture (SW Fwy - 59)
{
        eSWFwy59 <- df[df$RetailerID == 1829,]
        
        eSWFwy59$CreationDate <- as.Date(eSWFwy59$CreationDate, format = "%Y-%m-%d")
        eSWFwy59$Vintage <- paste0(month(eSWFwy59$CreationDate),"/",year(eSWFwy59$CreationDate))
        
        eSWFwy59TotalVolume <- aggregate(ID~Vintage, data = eSWFwy59, FUN = length)
        colnames(eSWFwy59TotalVolume)[2] <- "TotalVolume"
        
        eSWFwy59IsApproved <- aggregate(IsApproved~Vintage, data = eSWFwy59, FUN = sum)
        colnames(eSWFwy59IsApproved)[2] <- "IsApproved"
        
        eSWFwy59FundedIndicator <- aggregate(FundedIndicator~Vintage, data = eSWFwy59, FUN = sum)
        colnames(eSWFwy59FundedIndicator)[2] <- "FundedIndicator"
        
        eSWFwy59Summary <- merge(x = eSWFwy59TotalVolume, y = eSWFwy59IsApproved, by = c("Vintage"), all.x = T)
        eSWFwy59Summary$PercentApproved <- eSWFwy59Summary$IsApproved / eSWFwy59Summary$TotalVolume
        eSWFwy59Summary <- merge(x = eSWFwy59Summary, y = eSWFwy59FundedIndicator, by = c("Vintage"), all.x = T)
        eSWFwy59Summary$PercentFunded <- eSWFwy59Summary$FundedIndicator / eSWFwy59Summary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(eSWFwy59Summary, file = "eSWFwy59Summary.csv", quote = T, row.names = F)
}

#Exclusive Furniture (West Houston)
{
        eWestHouston <- df[df$RetailerID == 2763,]
        
        eWestHouston$CreationDate <- as.Date(eWestHouston$CreationDate, format = "%Y-%m-%d")
        eWestHouston$Vintage <- paste0(month(eWestHouston$CreationDate),"/",year(eWestHouston$CreationDate))
        
        eWestHoustonTotalVolume <- aggregate(ID~Vintage, data = eWestHouston, FUN = length)
        colnames(eWestHoustonTotalVolume)[2] <- "TotalVolume"
        
        eWestHoustonIsApproved <- aggregate(IsApproved~Vintage, data = eWestHouston, FUN = sum)
        colnames(eWestHoustonIsApproved)[2] <- "IsApproved"
        
        eWestHoustonFundedIndicator <- aggregate(FundedIndicator~Vintage, data = eWestHouston, FUN = sum)
        colnames(eWestHoustonFundedIndicator)[2] <- "FundedIndicator"
        
        eWestHoustonSummary <- merge(x = eWestHoustonTotalVolume, y = eWestHoustonIsApproved, by = c("Vintage"), all.x = T)
        eWestHoustonSummary$PercentApproved <- eWestHoustonSummary$IsApproved / eWestHoustonSummary$TotalVolume
        eWestHoustonSummary <- merge(x = eWestHoustonSummary, y = eWestHoustonFundedIndicator, by = c("Vintage"), all.x = T)
        eWestHoustonSummary$PercentFunded <- eWestHoustonSummary$FundedIndicator / eWestHoustonSummary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(eWestHoustonSummary, file = "eWestHoustonSummary.csv", quote = T, row.names = F)
}

#Exclusive Furniture Humble Corporation
{
        eCorp <- df[df$RetailerID == 5026,]
        
        eCorp$CreationDate <- as.Date(eCorp$CreationDate, format = "%Y-%m-%d")
        eCorp$Vintage <- paste0(month(eCorp$CreationDate),"/",year(eCorp$CreationDate))
        
        eCorpTotalVolume <- aggregate(ID~Vintage, data = eCorp, FUN = length)
        colnames(eCorpTotalVolume)[2] <- "TotalVolume"
        
        eCorpIsApproved <- aggregate(IsApproved~Vintage, data = eCorp, FUN = sum)
        colnames(eCorpIsApproved)[2] <- "IsApproved"
        
        eCorpFundedIndicator <- aggregate(FundedIndicator~Vintage, data = eCorp, FUN = sum)
        colnames(eCorpFundedIndicator)[2] <- "FundedIndicator"
        
        eCorpSummary <- merge(x = eCorpTotalVolume, y = eCorpIsApproved, by = c("Vintage"), all.x = T)
        eCorpSummary$PercentApproved <- eCorpSummary$IsApproved / eCorpSummary$TotalVolume
        eCorpSummary <- merge(x = eCorpSummary, y = eCorpFundedIndicator, by = c("Vintage"), all.x = T)
        eCorpSummary$PercentFunded <- eCorpSummary$FundedIndicator / eCorpSummary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(eCorpSummary, file = "eCorpSummary.csv", quote = T, row.names = F)
}

#All TX Exclusive Furniture Locations
{
        edf <- df[df$RetailerID %in% c(8969,1831,1830,1829,2763,5026),]
        edf$CreationDate <- as.Date(edf$CreationDate, format = "%Y-%m-%d")
        edf$Vintage <- paste0(month(edf$CreationDate),"/",year(edf$CreationDate))
        
        edfTotalVolume <- aggregate(ID~Vintage, data = edf, FUN = length)
        colnames(edfTotalVolume)[2] <- "TotalVolume"
        
        edfIsApproved <- aggregate(IsApproved~Vintage, data = edf, FUN = sum)
        colnames(edfIsApproved)[2] <- "IsApproved"
        
        edfFundedIndicator <- aggregate(FundedIndicator~Vintage, data = edf, FUN = sum)
        colnames(edfFundedIndicator)[2] <- "FundedIndicator"
        
        edfSummary <- merge(x = edfTotalVolume, y = edfIsApproved, by = c("Vintage"), all.x = T)
        edfSummary$PercentApproved <- edfSummary$IsApproved / edfSummary$TotalVolume
        edfSummary <- merge(x = edfSummary, y = edfFundedIndicator, by = c("Vintage"), all.x = T)
        edfSummary$PercentFunded <- edfSummary$FundedIndicator / edfSummary$TotalVolume
        
        setwd("F:/CSVFiles/FilesForChandler_Jeff/ExclusiveFurnitureTX")
        write.csv(edfSummary, file = "edfSummary.csv", quote = T, row.names = F)
}


