library(lubridate)

x2009 <- data.frame(read.csv("2009.csv"))
x2009 <- data.frame(read.csv("2009.csv",header=T, fill=T, col.names=c(colnames(x2009)),row.names=NULL,stringsAsFactors=F))
x2010 <- data.frame(read.csv("2010.csv",header=T, fill=T, col.names=c(colnames(x2009)),row.names=NULL,stringsAsFactors=F))
x2011 <- data.frame(read.csv("2011.csv",header=T, fill=T, col.names=c(colnames(x2009)),row.names=NULL,stringsAsFactors=F))
x2012 <- data.frame(read.csv("2012.csv",header=T, fill=T, col.names=c(colnames(x2009)),row.names=NULL,stringsAsFactors=F))
x2013 <- data.frame(read.csv("2013.csv",header=T, fill=T, col.names=c(colnames(x2009)),row.names=NULL,stringsAsFactors=F))
x2014 <- data.frame(read.csv("2014.csv",header=T, fill=T, col.names=c(colnames(x2009)),row.names=NULL,stringsAsFactors=F))
x2015 <- data.frame(read.csv("2015.csv",header=T, fill=T, col.names=c(colnames(x2009)),row.names=NULL,stringsAsFactors=F))


#this binds all the previous tables
allDFBinded <- 
  rbind(x2009, x2010, x2011, x2012, x2013, x2014, x2015)
rm(list = "x2009","x2010","x2011","x2012","x2013","x2014","x2015")
colnames(allDFBinded)[1] <- "AgreementNumber"
allDFBinded$PaymentAmount <- as.numeric(as.character(allDFBinded$PaymentAmount))


#this is losing data....need to fiddle more
allDFBinded$fundedDatetest<-gsub(" [0-9]{2}:[0-9]{2} [A-Z]{2}","",allDFBinded$FundedDate)
length(allDFBinded$FundedDate[is.na(allDFBinded$FundedDate)])
length(allDFBinded$FundedDate[allDFBinded$FundedDate == "NULL"])
allDFBinded$fundedDateTest <- as.POSIXct(allDFBinded$FundedDate, format="%m/%d/%y")
length(allDFBinded$fundedDateTest[is.na(allDFBinded$fundedDateTest)])



length(allDFBinded$FundedDate[allDFBinded$FundedDate == "NULL"])
length(allDFBinded$FundedDate[is.na(allDFBinded$FundedDate)])

#this sets the dates to readable dates in R with the correct year and sets the day to 1.
allDFBinded$FundedDate<-gsub(" [0-9]{2}:[0-9]{2} [A-Z]{2}","",allDFBinded$FundedDate)
allDFBinded$FundedDate <- as.Date(allDFBinded$FundedDate, format="%m/%d/%y")
allDFBinded$FundedDateMonth <- month(allDFBinded$FundedDate)
allDFBinded$FundedDateYear <- year(allDFBinded$FundedDate)
allDFBinded$TransactionDate<-gsub(" [0-9]{2}:[0-9]{2} [A-Z]{2}","",allDFBinded$TransactionDate)
allDFBinded$TransactionDate <- as.Date(allDFBinded$TransactionDate,format="%m/%d/%y")
allDFBinded$TransactionDateMonth <- month(allDFBinded$TransactionDate)
allDFBinded$TransactionDateYear <- year(allDFBinded$TransactionDate)
allDFBinded$ReturnedDate<-gsub(" [0-9]{2}:[0-9]{2} [A-Z]{2}","",allDFBinded$ReturnedDate)
allDFBinded$ReturnedDate <- as.Date(allDFBinded$ReturnedDate, format="%m/%d/%y")
allDFBinded$ReturnedDateMonth <- month(allDFBinded$ReturnedDate)
allDFBinded$ReturnedDateYear <- year(allDFBinded$ReturnedDate)
allDFBinded$LastTransactionDate<-gsub(" [0-9]{2}:[0-9]{2} [A-Z]{2}","",allDFBinded$LastTransactionDate)
allDFBinded$LastTransactionDate <- as.Date(allDFBinded$LastTransactionDate, format="%m/%d/%y")
allDFBinded$LastTransactionDateMonth <- month(allDFBinded$LastTransactionDate)
allDFBinded$LastTransactionDateYear <- year(allDFBinded$LastTransactionDate)
allDFBinded$PaidOffDate<-gsub(" [0-9]{2}:[0-9]{2} [A-Z]{2}","",allDFBinded$PaidOffDate)
allDFBinded$PaidOffDate <- as.Date(allDFBinded$PaidOffDate, format="%m/%d/%y")
allDFBinded$PaidOffDateMonth <- month(allDFBinded$PaidOffDate)
allDFBinded$PaidOffDateYear <- year(allDFBinded$PaidOffDate)
allDFBinded$ChargedOffDate<-gsub(" [0-9]{2}:[0-9]{2} [A-Z]{2}","",allDFBinded$ChargedOffDate)
allDFBinded$ChargedOffDate <- as.Date(allDFBinded$ChargedOffDate, format="%m/%d/%y")
allDFBinded$ChargedOffDateMonth <- month(allDFBinded$ChargedOffDate)
allDFBinded$ChargedOffDateYear <- year(allDFBinded$ChargedOffDate)

  
  
#this sets all the NULL values here to 0 and then converts the whole column to numeric
#allDFBinded$PaymentAmount[allDFBinded$PaymentAmount == "NULL"] <- NA
#allDFBinded$PaymentAmount <- as.numeric(as.character(allDFBinded$PaymentAmount))

x <- sum(allDFBinded$PaymentAmount[(!is.na(allDFBinded$FundedDate)) & allDFBinded$PaymentStatus == "CLEARED" &
  allDFBinded$TransactionDateMonth == 3 & allDFBinded$TransactionDateYear == 2015],na.rm=TRUE) #some of these are coming out NA? wtf
x 

length(allDFBinded$PaymentAmount[(!is.na(allDFBinded$FundedDate)) & allDFBinded$PaymentStatus == "CLEARED" &
                            allDFBinded$TransactionDateMonth == 3 & allDFBinded$TransactionDateYear == 2015]))
length(allDFBinded$FundedAmount[(is.na(allDFBinded$FundedAmount))])

y <- sum(allDFBinded$PaymentAmount[(!is.na(allDFBinded$FundedDate)) & allDFBinded$PaymentStatus == "RETURNED" &
 allDFBinded$TransactionDateMonth == 3 & allDFBinded$TransactionDateYear == 2015])
y



sum(allDFBinded$FundedAmount[month(allDFBinded$FundedDate) == 3 & year(allDFBinded$FundedDate) == 2015])
