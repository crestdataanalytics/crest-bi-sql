library(plyr)
library(ggplot2)
library(lubridate)
ymd("2015-04-10")
dataDumpAll = read.csv("eBureauAll.csv")
dataDumpAll <- data.frame(dataDumpAll)
summary(dataDumpAll)

dataDumpAll$RiskScore <- as.numeric(as.character(dataDumpAll$RiskScore))
colnames(dataDumpAll)[1] <- "DisplayID"

#this format's the date as just month and year so i can better group by it
dataDumpAll$CreatedDate <- as.Date(dataDumpAll$CreatedDate)
dataDumpAll$FundedDate <- ymd(dataDumpAll$FundedDate)
dataDumpAll$FundedDate <- as.Date(dataDumpAll$FundedDate)
dataDumpAll$CreationDate <- as.Date(dataDumpAll$CreationDate)
dataDumpAll$ApprovalDate <- ymd(dataDumpAll$ApprovalDate)
dataDumpAll$ApprovalDate <- as.Date(dataDumpAll$ApprovalDate)
dataDumpAll$ChargedOffDate <- ymd(dataDumpAll$ChargedOffDate)
dataDumpAll$FirstPaymentDate <- ymd(dataDumpAll$FirstPaymentDate)

#this sets the day on these date fields as the first of the month
day(dataDumpAll$CreatedDate) <- 1
day(dataDumpAll$FundedDate) <- 1
day(dataDumpAll$CreationDate) <- 1
day(dataDumpAll$ApprovalDate) <- 1
day(dataDumpAll$ChargedOffDate) <- 1
day(dataDumpAll$FirstPaymentDate) <- 1

#this creates a column that will equate an existing but with a bit value.  sets all bits to 0 and then true cases to 1
dataDumpAll$LoanIsApproved <- 0
dataDumpAll$LoanIsApproved[!is.na(dataDumpAll$ApprovalDate)] <- 1
dataDumpAll$TotalNSFFeesAmount <- as.numeric(as.character(dataDumpAll$TotalNSFFeesAmount))
dataDumpAll$HasNSF <- 0
dataDumpAll$HasNSF[(dataDumpAll$TotalNSFFeesAmount!=0) | !is.na(dataDumpAll$TotalNSFFeesAmount)
                   & !is.na(dataDumpAll$FirstPaymentDate)] <- 1
dataDumpAll$StatusIsBounced<-0
dataDumpAll$StatusIsBounced[dataDumpAll$Status == "Bounced"]<-1
dataDumpAll$StatusIsFunded<-0
dataDumpAll$StatusIsFunded[dataDumpAll$Status == "Funded"]<-1
dataDumpAll$IsChargedOff<-0
dataDumpAll$IsChargedOff[(!is.na(dataDumpAll$ChargedOffDate)) & ]<-1 #when the chargedOffDate column gets into the data, that needs to be accounted for
dataDumpAll$LoanIsFunded <- 0
dataDumpAll$LoanIsFunded[(!is.na(dataDumpAll$FundedDate))] <- 1
dataDumpAll$LoanIsFundedWithFirstPayment <- 0
dataDumpAll$LoanIsFundedWithFirstPayment[(!is.na(dataDumpAll$FundedDate)) & (!is.na(dataDumpAll$FirstPaymentDate))] <- 1

#this gets all the different months
differentMonths <- unique(dataDumpAll$CreatedDate)

#this gets all the eBureau decisions
totalApplications <- length(dataDumpAll$DisplayID[dataDumpAll$Decision == "Approved" | dataDumpAll$Decision == "Denied"])

#delinquent leases
riskScoreForDelinquentLeases <-
  dataDumpAll$RiskScore[(!is.na(dataDumpAll$FundedDate)) &
                                ((!is.na(dataDumpAll$ChargedOffDate) | 
                                (dataDumpAll$FirstPaymentLate == 1) |
                                (dataDumpAll$IsDelinquent == 1)))]
summary(riskScoreForDelinquentLeases)

#good leases
riskScoreForGoodLeases <-
  dataDumpAll$RiskScore[(!is.na(dataDumpAll$FundedDate) & 
                             (dataDumpAll$FirstPaymentOnTime == 1) &
                             (dataDumpAll$IsDelinquent == 0))]
summary(riskScoreForGoodLeases)

#looked at some of these account numbers...the created dates aren't just in Feb...this might make sense in the case where
#they have already paid off a lease with us before.  Some of the "-1" loans...ebureau was run twice and on the second instance
# it passed.  868499-1 is the one that doesn't follow that pattern and the only one that doesn't make sense to me
leasesRiskScoreLessThan35 <- dataDumpAll$DisplayID[dataDumpAll$RiskScore < 35 & !is.na(dataDumpAll$FundedDate)]
length(leasesRiskScoreLessThan35) 

#this will look at each risk score and and see where we have the most success
riskScoreAccuracy <- data.frame(1:1000)
riskScoreAccuracy$RiskScore <- 0
riskScoreAccuracy$Accuracy <- 0
riskScoreAccuracy$TotalGood <- 0
riskScoreAccuracy$Total <- 0

for (i in 1:1000)
{
  riskScoreAccuracy$RiskScore[i] <- i
  
  riskScoreAccuracy$Accuracy[i] <- length(dataDumpAll$DisplayID[dataDumpAll$LoanIsFunded == 1 
                                   & dataDumpAll$HasFirstPaymentDue == 1
                                   & dataDumpAll$FirstPaymentOnTime == 1
                                   & dataDumpAll$RiskScore >= i
                                   & dataDumpAll$StatusIsFunded == 1]) / #good leases divided by total
                                   length(dataDumpAll$DisplayID[dataDumpAll$LoanIsFunded == 1 
                                   & dataDumpAll$HasFirstPaymentDue == 1 
                                   & dataDumpAll$RiskScore >= i])
  
  riskScoreAccuracy$TotalGood[i] <- length(dataDumpAll$DisplayID[dataDumpAll$LoanIsFunded == 1 
                                 & dataDumpAll$HasFirstPaymentDue == 1
                                 & dataDumpAll$FirstPaymentOnTime == 1
                                 & dataDumpAll$RiskScore >= i
                                 & dataDumpAll$StatusIsFunded == 1])
  
  riskScoreAccuracy$Total[i] <-  length(dataDumpAll$DisplayID[dataDumpAll$LoanIsFunded == 1 
                              & dataDumpAll$HasFirstPaymentDue == 1 
                              & dataDumpAll$RiskScore >= i])
  
  
}

summary(riskScoreAccuracy)

riskScoreAccuracy$RiskScore[riskScoreAccuracy$Total > 1]
cor.test(riskScoreAccuracy$Accuracy,riskScoreAccuracy$RiskScore)
plot(riskScoreAccuracy$Accuracy,riskScoreAccuracy$RiskScore)







#this aggregates on the different items we want it to. Each row is a different scenario and the ID is the count of them
temp <- aggregate(DisplayID ~ LoanIsFunded+CreatedDate+Decision+AutoApprovedFlag
                  +HasNSF+StatusIsBounced+StatusIsFunded+IsChargedOff+LoanIsFundedWithFirstPayment+HasFirstPaymentDue
                  +FirstPaymentOnTime+FirstPaymentLate,dataDumpAll, length)

differentMonths <- unique(temp$CreatedDate)

#this is the total that eBureau said yes or no to
totalDecisions <- c()
for (i in 1:length(differentMonths))
{
  timeFrame <- differentMonths[i]
  totalDecisions <- c(totalDecisions, 
                             sum(temp$DisplayID[(temp$Decision == "Approved" | temp$Decision == "Denied")
                              & temp$CreatedDate == timeFrame]))
}
length(dataDumpAll$DisplayID[(dataDumpAll$Decision == "Approved" | dataDumpAll$Decision == "Denied")]) == sum(totalDecisions)

#this is the total decisions that were approved 
totalApprovedDecision <- c()
for (i in 1:length(differentMonths))
{
  timeFrame <- differentMonths[i]
  totalApprovedDecision <- c(totalApprovedDecision, 
                             sum(temp$DisplayID[temp$Decision == "Approved" & temp$CreatedDate == timeFrame]))
}
length(dataDumpAll$DisplayID[(dataDumpAll$Decision == "Approved")]) == sum(totalApprovedDecision)

#gets the leases that have a first payment date
totalLoansFundedWithFirstPaymentDue <- c()
for (i in 1:length(differentMonths))
{
  timeFrame <- differentMonths[i]
  totalLoansFundedWithFirstPaymentDue <- c(totalLoansFundedWithFirstPaymentDue, 
                        sum(temp$DisplayID[(temp$LoanIsFunded == 1 & 
                        temp$HasFirstPaymentDue == 1 & temp$CreatedDate == timeFrame)]))
}
length(dataDumpAll$DisplayID[dataDumpAll$LoanIsFunded == 1 & dataDumpAll$HasFirstPaymentDue == 1]) == 
    sum(totalLoansFundedWithFirstPaymentDue)

totalLoansFundedWithFirstPaymentOnTime <- c()
for (i in 1:length(differentMonths))
{
  timeFrame <- differentMonths[i]
  totalLoansFundedWithFirstPaymentOnTime <- c(totalLoansFundedWithFirstPaymentOnTime, 
                                              sum(temp$DisplayID[(temp$LoanIsFunded == 1 & 
                                              temp$HasFirstPaymentDue == 1 & temp$FirstPaymentOnTime == 1 &
                                              temp$CreatedDate == timeFrame)]))
}
length(dataDumpAll$DisplayID[dataDumpAll$LoanIsFunded == 1 & dataDumpAll$HasFirstPaymentDue == 1 &
      dataDumpAll$FirstPaymentOnTime == 1]) == sum(totalLoansFundedWithFirstPaymentOnTime)


totalChargedOff <- c()
for (i in 1:length(differentMonths))
{
  timeFrame <- differentMonths[i]
  totalChargedOff <- c(totalChargedOff, 
                   sum(temp$DisplayID[(temp$IsChargedOff == 1 & temp$CreatedDate == timeFrame)]))
}
length(dataDumpAll$DisplayID[dataDumpAll$IsChargedOff == 1]) == sum(totalChargedOff)

totalBounced <- c()
for (i in 1:length(differentMonths))
{
  timeFrame <- differentMonths[i]
  totalBounced <- c(totalBounced, 
                       sum(temp$DisplayID[(temp$StatusIsBounced == 1 & temp$CreatedDate == timeFrame)]))
}
length(dataDumpAll$DisplayID[dataDumpAll$StatusIsBounced == 1]) == sum(totalBounced)

totalHasNoNSF <- c()
for (i in 1:length(differentMonths))
{
  timeFrame <- differentMonths[i]
  totalHasNoNSF <- c(totalHasNoNSF, 
                    sum(temp$DisplayID[(temp$HasNSF == 0 & temp$CreatedDate == timeFrame
                                           & temp$HasFirstPaymentDue == 1)]))
}
length(dataDumpAll$DisplayID[dataDumpAll$HasNSF == 0 & dataDumpAll$HasFirstPaymentDue == 1]) == sum(totalHasNoNSF)

AutoApprovedOverApprovedDecision <- totalApprovedDecision / totalDecisions
FundedLoanOverAutoApproved <- totalLoansFunded / totalAutoApproved
FundedLoanOverApprovedDecision <- totalLoansFunded / totalApprovedDecision
LoanFundedNoNSFOverFunded <- totalHasNoNSF / totalLoansFunded
BouncedOverOpenLoans <- totalBounced / (totalFundedStatus + totalBounced)

#this puts all of that into a dataFrame
summaryDataFrame <- data.frame(differentMonths, AutoApprovedOverApprovedDecision, BouncedOverOpenLoans, LoanFundedNoNSFOverFunded,
                               FundedLoanOverApprovedDecision, FundedLoanOverAutoApproved,totalApprovedDecision, 
                               totalAutoApproved, totalBounced, totalChargedOff, totalDecisions, totalFundedStatus,
                               totalHasNoNSF, totalLoansFunded)
summary(summaryDataFrame)

#let's look at the risk scores
riskScoresOnApprovedAccounts <- unique(dataDumpAll$RiskScore[dataDumpAll$LoanIsApproved ==1])
summary(riskScoresOnApprovedAccounts)
riskScoreOnDeniedAccounts <- unique(dataDumpAll$RiskScore[dataDumpAll$LoanIsApproved ==0])
summary(riskScoreOnDeniedAccounts)



