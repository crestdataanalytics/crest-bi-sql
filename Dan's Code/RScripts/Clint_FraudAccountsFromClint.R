#Potential Fraud Accounts sent by clint

1093279-1
1042452-1
1096703-1
1071810-3
1095125-1
1095737-1
1093590-2

#get the data
setwd("F:/CSVFiles")
CCodesClarity   <- read.csv("CCodesClarity.csv", stringsAsFactors = F, colClasses = c("character"),quote = '"')
colnames(CCodesClarity)[1] <- "DocumentID"
colnames(CCodesClarity)[2] <- "ApplicantID"
colnames(CCodesClarity)[3] <- "InquiryDate"
CCodesClarity$ApplicantID <- as.numeric(CCodesClarity$ApplicantID)
CCodesClarity$DocumentID <- as.numeric(CCodesClarity$DocumentID)
CCodesClarity$ApplicantID <- as.numeric(CCodesClarity$ApplicantID)
CCodesClarity$InquiryDate <- as.Date(CCodesClarity$InquiryDate, format = "%Y-%m-%d")

CCodesClarity_TrackingIDs   <- read.csv("CCodesClarity_TrackingIDs.csv", stringsAsFactors = F, colClasses = c("character"),quote = '"')
colnames(CCodesClarity_TrackingIDs)[1] <- "DocumentID"
CCodesClarity_TrackingIDs <- CCodesClarity_TrackingIDs[,c("DocumentID","VariableValue")]
colnames(CCodesClarity_TrackingIDs)[2] <- "TrackingID"

CCodesClarity <- merge(CCodesClarity, CCodesClarity_TrackingIDs, by = c("DocumentID"), all.x = T)
#CCodesClarity <- CCodesClarity[!is.na(CCodesClarity$TrackingID),]


#ties on ID from dfV3 based on the TrackingID
applicantIDs <- dfV3[dfV3$InquiryDate >= "2015-01-01" & !is.na(dfV3$InquiryDate) & dfV3$ApprovalProviderSettingsID == 1 & 
                        !is.na(dfV3$ApprovalProviderSettingsID),]
applicantIDs <- applicantIDs[,c("ID","TrackingID")]
applicantIDs <- unique(applicantIDs[!is.na(applicantIDs$TrackingID),])


ApplicantsCCC <- merge(applicantIDs, CCodesClarity, by = c("TrackingID"), all.y = T)
ApplicantsCCC <- unique(ApplicantsCCC)


ApplicantsCCC <- merge(dfV3Leases, ApplicantsCCC, by = c("ID"), all.y = T)



length(unique(ApplicantsCCC$ID[ApplicantsCCC$ChargeOffIndicator == 1])) / 
        length(unique(ApplicantsCCC$ID[ApplicantsCCC$PaidOffIndicator == 1 | ApplicantsCCC$ChargeOffIndicator == 1]))


C204 <- ApplicantsCCC[ grepl("(C204)", ApplicantsCCC$VariableValue),]
length(unique(C204$ID[C204$ChargeOffIndicator == 1])) / length(unique(C204$ID[C204$PaidOffIndicator == 1 | C204$ChargeOffIndicator == 1]))
length(unique(C204$ID[C204$FirstPaymentDefault == 1])) / length(unique(C204$ID[C204$FundedIndicator == 1]))
length(unique(C204$ID[C204$NinetyDayDelinquency == 1])) / length(unique(C204$ID[C204$FundedIndicator == 1]))
summary(C204$FundedDate)

C214 <- ApplicantsCCC[ grepl("(C214)", ApplicantsCCC$VariableValue),]
length(unique(C214$ID[C214$ChargeOffIndicator == 1])) / length(unique(C214$ID[C214$PaidOffIndicator == 1 | C214$ChargeOffIndicator == 1]))
length(unique(C214$ID[C214$FirstPaymentDefault == 1])) / length(unique(C214$ID[C214$FundedIndicator == 1]))
length(unique(C214$ID[C214$NinetyDayDelinquency == 1])) / length(unique(C214$ID[C214$FundedIndicator == 1]))
summary(C214$FundedDate)

C209 <- ApplicantsCCC[ grepl("(C209)", ApplicantsCCC$VariableValue),]
length(unique(C209$ID[C209$ChargeOffIndicator == 1])) / length(unique(C209$ID[C209$PaidOffIndicator == 1 | C209$ChargeOffIndicator == 1]))
length(unique(C209$ID[C209$FirstPaymentDefault == 1])) / length(unique(C209$ID[C209$FundedIndicator == 1]))
length(unique(C209$ID[C209$NinetyDayDelinquency == 1])) / length(unique(C209$ID[C209$FundedIndicator == 1]))
summary(C209$FundedDate)

C217 <- ApplicantsCCC[ grepl("(C217)", ApplicantsCCC$VariableValue),]
length(unique(C217$ID[C217$ChargeOffIndicator == 1])) / length(unique(C217$ID[C217$PaidOffIndicator == 1 | C217$ChargeOffIndicator == 1]))
length(unique(C217$ID[C217$FirstPaymentDefault == 1])) / length(unique(C217$ID[C217$FundedIndicator == 1]))
length(unique(C217$ID[C217$NinetyDayDelinquency == 1])) / length(unique(C217$ID[C217$FundedIndicator == 1]))
summary(C217$FundedDate)


C205 <- ApplicantsCCC[ grepl("(C205)", ApplicantsCCC$VariableValue),]
length(unique(C205$ID[C205$ChargeOffIndicator == 1])) / length(unique(C205$ID[C205$PaidOffIndicator == 1 | C205$ChargeOffIndicator == 1]))
length(unique(C205$ID[C205$FirstPaymentDefault == 1])) / length(unique(C205$ID[C205$FundedIndicator == 1]))
length(unique(C205$ID[C205$NinetyDayDelinquency == 1])) / length(unique(C205$ID[C205$FundedIndicator == 1]))
summary(C205$FundedDate)


C213 <- ApplicantsCCC[ grepl("(C213)", ApplicantsCCC$VariableValue),]
length(unique(C213$ID[C213$ChargeOffIndicator == 1])) / length(unique(C213$ID[C213$PaidOffIndicator == 1 | C213$ChargeOffIndicator == 1]))
length(unique(C213$ID[C213$FirstPaymentDefault == 1])) / length(unique(C213$ID[C213$FundedIndicator == 1]))
length(unique(C213$ID[C213$NinetyDayDelinquency == 1])) / length(unique(C213$ID[C213$FundedIndicator == 1]))
summary(C213$FundedDate)


C223 <- ApplicantsCCC[ grepl("(C223)", ApplicantsCCC$VariableValue),]
length(unique(C223$ID[C223$ChargeOffIndicator == 1])) / length(unique(C223$ID[C223$PaidOffIndicator == 1 | C223$ChargeOffIndicator == 1]))
length(unique(C223$ID[C223$FirstPaymentDefault == 1])) / length(unique(C223$ID[C223$FundedIndicator == 1]))
length(unique(C223$ID[C223$NinetyDayDelinquency == 1])) / length(unique(C223$ID[C223$FundedIndicator == 1]))
summary(C223$FundedDate)


C220 <- ApplicantsCCC[ grepl("(C220)", ApplicantsCCC$VariableValue),]
length(unique(C220$ID[C220$ChargeOffIndicator == 1])) / length(unique(C220$ID[C220$PaidOffIndicator == 1 | C220$ChargeOffIndicator == 1]))
length(unique(C220$ID[C220$FirstPaymentDefault == 1])) / length(unique(C220$ID[C220$FundedIndicator == 1]))
length(unique(C220$ID[C220$NinetyDayDelinquency == 1])) / length(unique(C220$ID[C220$FundedIndicator == 1]))
summary(C220$FundedDate)


C218 <- ApplicantsCCC[ grepl("(C218)", ApplicantsCCC$VariableValue),]
length(unique(C218$ID[C218$ChargeOffIndicator == 1])) / length(unique(C218$ID[C218$PaidOffIndicator == 1 | C218$ChargeOffIndicator == 1]))
length(unique(C218$ID[C218$FirstPaymentDefault == 1])) / length(unique(C218$ID[C218$FundedIndicator == 1]))
length(unique(C218$ID[C218$NinetyDayDelinquency == 1])) / length(unique(C218$ID[C218$FundedIndicator == 1]))
summary(C218$FundedDate)


