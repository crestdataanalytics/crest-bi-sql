WITH X AS
(
SELECT  
	Decision,
	CreatedDate,
	ApplicantID,
	CAST(REPLACE(ResponseXML, 'utf-16', 'utf-8') AS XML).value('(CrystalBallApprovalResult/ApprovalProviderResultType)[1]', 'VARCHAR(100)') AS ResultType,
	CAST(REPLACE(ResponseXML, 'utf-16', 'utf-8') AS XML).value('(CrystalBallApprovalResult/Factor)[1]', 'VARCHAR(100)') AS Factor,
	CAST(REPLACE(ResponseXML, 'utf-16', 'utf-8') AS XML).value('(CrystalBallApprovalResult/ApprovalScore)[1]', 'VARCHAR(100)') AS ApprovalScore,
	CAST(REPLACE(ResponseXML, 'utf-16', 'utf-8') AS XML).value('(CrystalBallApprovalResult/ApprovedAmount)[1]', 'VARCHAR(100)') AS ApprovedAmount
	FROM CrestWarehouse.dbo.ApprovalProviderResponseLog 
	WHERE CreatedDate >= '2016-09-23'
	AND ApprovalProviderSettingsId = 16
)
SELECT
	distinct L.ID,
	L.ApplicantID,
	ISNULL(L.ApprovalAmount, 0),
	C.CategoryName,
	AJ.MonthlyIncome,
	AD.PostalCode,
	AD.City,
	AD.StateID,
	case when L.AutoApprovedFlag = 1 and L.StatusID not in (7,11) then 'Auto-New'
		when L.AutoApprovedFlag = 2 and L.StatusID not in (7,11) then 'Auto-Repeat' 
		when L.AutoApprovedFlag = 0 and L.StatusID not in (7,11) then 'Manual' 
		else 'Denied' end as ApprovalPath,
	cast(L.CreationDate as date) as SubmitDate,
	cast(L.CreationDate as time) as SubmitTime,
	L.AutoApprovedFlag as AutoApproved,
	x.*
FROM
	Loans as L left outer join LoanStatuses as LS on L.StatusID = LS.ID
	left outer join Dealers as D on D.ID = L.DealerID
	left outer join DealerCategories as DC on DC.DealerID = D.ID
	left outer join Categories as C on C.ID = DC.CategoryID
	left outer join ApplicantJob as AJ on AJ.ApplicantID = L.ApplicantID
	left outer join Applicants as A on A.ID = L.ApplicantID
	left outer join Addresses as AD on AD.ID = A.AddressID
	LEFT JOIN x ON x.ApplicantID = L.ApplicantID
WHERE
	cast(L.CreationDate as date) = cast(getdate() as date)
	--and L.StatusID = 7
	and D.IsDemoAccount = 0
	--and LS.[Status] = 'Approved'
	and L.StatusID NOT IN (11,21)
	and ApprovalScore <= 16
	and factor not in ('1.00','0.00')
ORDER BY
	CategoryName