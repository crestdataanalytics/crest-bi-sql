SELECT
	convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovalScore)')) as CBScore,
	convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApplicantID)')) as ApplicantID,
	convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovedAmount)')) as ApprovedAmount,
	convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/Factor)')) as Factor,
	convert(varchar(255),convert(xml,convert(nvarchar(max),ResponseXML)).query('data(/CrystalBallApprovalResult/ApprovalProviderResultType)')) as Response,
	cast(CreatedDate as date) as SubmitDate
FROM approvalproviderresponselog 
where cast(createddate as date) > '9-22-16' and approvalprovidersettingsid = 16
order by
ApprovedAmount