If OBJECT_ID('tempdb..##TempRetailer') is not null
drop table ##TempRetailer

select
	R.[Retailer Name],
	R.[Retailer ID],
	R.[State],
	sum(case when datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end) as '3 mo. Funded',	
	isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 then [Funded Amount] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end),0),0) as '3 mo. FPD $',
	isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 then 1 else null end),0)*1.0/	
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
	isnull(nullif(count(case when [LeaseQueueKey] = 6 and datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/	
		nullif(count(case when [Close Date] is null and datediff(mm,[Funded Date],getdate())  <= 3 then 1 else null end),0),0) as '3 mo. Bounced %',
	isnull(nullif(count(case when [LeaseQueueKey] = 12 and datediff(mm,[Funded Date],getdate()) between 5 and 8 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(mm,[Funded Date],getdate()) between 5 and 8 then 1 else null end),0),0) as '3 mo. Charge-Off %',
	isnull(nullif(count(case when [LeaseQueueKey] = 5 and datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0),0) as '3 mo. Paid %',
	isnull(nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. Closing Multiple',
	isnull(nullif(sum(case when [Close Date] is not null then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when [Close Date] is not null then [Funded Amount] else 0 end),0),0) as 'Overall Closing Multiple',
	isnull(nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Funded Amount] else 0 end),0),0) as 'One Year Multiple',
	count(case when datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end) as '3 mo Volume',	
	count(case when datediff(mm,[Funded Date],getdate()) <= 6 then 1 else null end) as '6 mo Volume',	
	count(case when datediff(mm,[Funded Date],getdate()) <= 12 then 1 else null end) as '12 mo Volume',	
	isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
	isnull(nullif(count(case when datediff(mm,[Submitted Date],getdate()) <= 3 and [LeaseQueueKey] not in (7,11) then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Submitted Date],getdate()) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate',
	(isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end),0)*1.0/
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0))-
	 (isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0)) as 'Quarterly FPD vs Volume %',
	sum(case when datediff(mm,[Funded Date],getdate()) >= 12 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],getdate()) = 11 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],getdate()) = 10 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],getdate()) = 9 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],getdate()) = 8 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],getdate()) = 7 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],getdate()) = 6 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],getdate()) = 5 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],getdate()) = 4 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],getdate()) = 3 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],getdate()) = 2 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],getdate()) = 1 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],getdate()) = 0 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.06
		else 0
		end)/count([Lease ID]) as 'Actual vs. Target'
	into ##TempRetailer
from
	CFREPORTS.[Crest Business Intelligence].dbo.Retailers as R join CFREPORTS.[Crest Business Intelligence].dbo.Leases as L on R.[Retailer ID] = L.[Retailer ID]
--where
	--R.[Current Status] in ('Active') 
group by
	R.[Retailer Name], R.[Retailer ID], R.[State]
order by
	[One Year Multiple] asc,
	r.[Retailer Name]

If OBJECT_ID('tempdb..##RetailerMax') is not null
drop table ##RetailerMax

select
TR.[Retailer ID] as RetailerID,
(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*4
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*3
				)
				+
				(
				case
				when [3 mo. Bounced %] >= .45 Then 0
				when [3 mo. Bounced %] >= .30 Then 1
				when [3 mo. Bounced %] >= .20 Then 2
				when [3 mo. Bounced %] >= .10 Then 3
				when [3 mo. Bounced %] >= .05 Then 4
				when [3 mo. Bounced %] >= 0 Then 5
				else 5
				End*2
				)
				+
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/100.00 'Max Approval Score'
			into ##RetailerMax
			from ##TempRetailer as TR

SELECT
	R.[Retailer ID],
	R.[Retailer Name],
	R.[Category],
	R.[Region],
	cast(R.[Creation Date] as date) as CreateDate,
	cast(RM.[Max Approval Score]*10 as decimal(18,2)) as BIrank,
	cast((CB.AvgScore/10) as decimal(18,2)) as AvgRiskScore,
	Count(L.[Lease ID]) as Leases,
	Sum([Funded Amount]) as Funded,
	cast(Sum([Funded Amount])/nullif(Sum([Approval Amount]),0) as decimal(18,2)) as Utilization
FROM
	Retailers as R join ##RetailerMax as RM on R.[Retailer ID] = RM.[RetailerID]
		left outer join Leases as L on L.[Retailer ID] = R.[Retailer ID]
		left outer join
			(select
				L.[Retailer ID],
				avg(cast(padv.VariableValue as int)) as AvgScore
			from [ProviderAnalysis].dbo.DocumentInfos as pad 
					join [ProviderAnalysis].dbo.DocumentVariables as padv on
					pad.DocumentID = padv.DocumentID
					join [ProviderAnalysis].dbo.Variables as pav on
					padv.VariableID = pav.VariableID
					join [ProviderAnalysis].dbo.Providers as pap on
					pad.providerID = pap.providerId
					left outer join [Crest Business Intelligence].[dbo].[Leases] as L on L.[Customer ID] = pad.SubjectID
			where 
				ProviderName = 'Crystalball'
				and pav.variableID = 45996
			group by
				L.[Retailer ID]) as CB on CB.[Retailer ID] = R.[Retailer ID]
WHERE
	CB.AvgScore is not null
	and [Funded Amount] > 0
	and R.[Current Status] = 'Active'
	--and datediff(dd,R.[Creation Date],getdate()) < 45
	--and R.[Retailer ID] = 2512
	and R.[Retailer ID] in (2512,10990,5354,9473,6620,5388,9863,10316,10764,10863,2412,5211,6622,9677,10191,10747)
GROUP BY
	R.[Retailer ID],
	R.[Retailer Name],
	R.[Category],
	R.[Region],
	R.[Creation Date],
	RM.[Max Approval Score]*10,
	CB.AvgScore
--HAVING
--	Count(L.[Lease ID]) > 4 and
--	Sum([Funded Amount]) > 2000 and
--	Sum([Funded Amount])/Sum([Approval Amount]) >= .9
ORDER BY 
	CB.AvgScore desc