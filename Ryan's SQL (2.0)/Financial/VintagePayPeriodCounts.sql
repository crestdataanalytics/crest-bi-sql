SELECT
	CONVERT(CHAR(7),L.[FundedDate],120) as 'Vintage',
	PP.[Period],
	count(L.ID) as Leases
FROM
	Loans as L join ApplicantJob as AJ on L.ApplicantID = AJ.ApplicantID
	join PayPeriodTypes as PP on PP.ID = AJ.PayPeriodTypeID
WHERE
	FundedDate > '6-30-15'
	and FundedAmount > 0
	and (datediff(dd,fundeddate,paidoffdate) > 30 or datediff(dd,fundeddate,chargedoffdate) > 30)
	--and PP.[Period] not in ('None')
GROUP BY
	CONVERT(CHAR(7),L.[FundedDate],120),
	PP.[Period]
ORDER BY
	CONVERT(CHAR(7),L.[FundedDate],120) desc,
	PP.[Period] desc

-----------------------------------

SELECT
	CONVERT(CHAR(7),L.[FundedDate],120) as 'Vintage',
	case when PP.ScheduleEntryDesc in ('Biweekly','Bi-weekly') then 'Bi-weekly'
	else PP.ScheduleEntryDesc end as PayPeriod,
	count(L.ID) as Leases
FROM
	Loans as L 
	join 
		(SELECT
			LoanID,
			min(cast(DueDate as date)) as DueDate,
			SUBSTRING (ScheduleEntryDesc ,  CHARINDEX('(', ScheduleEntryDesc)+1 ,   CHARINDEX(')', ScheduleEntryDesc)- CHARINDEX('(', ScheduleEntryDesc)-1 ) as ScheduleEntryDesc
		FROM
			vw_loanPaymentSchedule
		WHERE
			DueDate > getdate()
			and ScheduleEntryDesc not in ('','Initial Payment')
			and ScheduleEntryDesc is not null
		GROUP BY
			LoanID,
			ScheduleEntryDesc) as PP on PP.LoanID = L.ID
WHERE
	FundedDate > '5-30-15'
	and FundedAmount > 0
	and (datediff(dd,fundeddate,paidoffdate) > 30 or datediff(dd,fundeddate,chargedoffdate) > 30)
	--and PP.[Period] not in ('None')
GROUP BY
	CONVERT(CHAR(7),L.[FundedDate],120),
	case when PP.ScheduleEntryDesc in ('Biweekly','Bi-weekly') then 'Bi-weekly'
	else PP.ScheduleEntryDesc end
ORDER BY
	CONVERT(CHAR(7),L.[FundedDate],120) desc,
	PayPeriod desc



