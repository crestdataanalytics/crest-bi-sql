SELECT --top 25 percent
	D.ID as DealerID,
	D.Name as DealerName,
	C.CategoryName,
	--CASE WHEN A.[StateID] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
	--	WHEN A.[StateID] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
	--	WHEN A.[StateID] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
	--	WHEN A.[StateID] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
	--	ELSE 'NCT' END AS 'Region',
	A.StreetLine1,
	A.City,
	A.StateID,
	A.PostalCode,
	left(D.PhoneNumber,3) + '-' +substring(D.PhoneNumber,4,3) + '-' + substring(D.PhoneNumber,7,4) as StorePhoneNumber,
	D.ContactName,
	D.EmailAddress,
	cast(D.CreationDate as date) as DealerCreateDate,
	concat(U.FirstName,' ',U.LastName) as RepName,
	D.[Owner],
	left(D.OwnerHomePhone,3) + '-' +substring(D.OwnerHomePhone,4,3) + '-' + substring(D.OwnerHomePhone,7,4) as OwnerPhoneNumber,
	D.RiskScore as 'BI Rank',
	D.Ranking10Month as OldDealerRank,
	Case when D.Ranking10Month between -8 and -19 then 2
		when D.Ranking10Month < -19 then 3
		else 1 end as VerifastApprovals,
	D.ApplicationFee,
	--FP.Name as PlanName,
	--FP.TotalNotePercent as PlanPricingMultiple,
	--FP.CrestPercentage,
	--FP.NinetyDayPayoffPercent,
	MC.[M1 Count],
	MC.[M2 Count],
	MC.[M3 Count],
	MC.[M4 Count],
	MC.[M5 Count],
	MC.[M6 Count],
	MC.TotalFunded,
	MC.TotalLeases,
	MC.SixMonthFunded,
	MC.SixMonthLeases,
	MM.[SAC %],
	MM.[EBO %],
	MM.[PIF %],
	MM.[Charge-Off %],
	MM.[Open %],
	MM.[Bounced %],
	MM.[Other %]
from
	Dealers as D join Addresses as A on D.AddressID = A.ID
		join Users as U on U.ID = D.DealerRepID
		join DealerCategories as DC on DC.DealerID = D.ID
		join Categories as C on C.ID = DC.CategoryID
		--join DealerFinancePlans as DFP on DFP.DealerID = D.ID
		--join FinancePlans as FP on FP.ID = DFP.FinancePlanID
		left outer join 
			(Select
				DealerID,
				--FP.ID as FPID,
				Count(case when datediff(mm,[FundedDate],getdate()) = 0 and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as 'M1 Count',
				Count(case when datediff(mm,[FundedDate],getdate()) = 1 and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as 'M2 Count',
				Count(case when datediff(mm,[FundedDate],getdate()) = 2 and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as 'M3 Count',
				Count(case when datediff(mm,[FundedDate],getdate()) = 3 and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as 'M4 Count',
				Count(case when datediff(mm,[FundedDate],getdate()) = 4 and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as 'M5 Count',
				Count(case when datediff(mm,[FundedDate],getdate()) = 5 and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as 'M6 Count',
				SUM(case when datediff(mm,[FundedDate],getdate()) <= 6 then FundedAmount else 0 end) as SixMonthFunded,
				Count(case when datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end) as SixMonthLeases,
				SUM(FundedAmount) as TotalFunded,
				Count(FundedAmount) as TotalLeases
			From Loans as L --join FinancePlans as FP on FP.ID = L.PlanID
			Where
				FundedAmount > 0
			Group by DealerID) as MC on MC.DealerID = D.ID --and MC.FPID = FP.ID
		left outer join
			(Select
				DealerID,
				--FP.ID as FPID,
				count(case when L.StatusID = 5 and datediff(dd,[FundedDate],[PaidOffDate]) < 90 then 1 else null end)*1.0/nullif(count(L.[ID]),0) as 'SAC %',
				count(case when L.StatusID  = 5 and datediff(dd,[FundedDate],[PaidOffDate]) between 90 and 364 then 1 else null end)*1.0/nullif(count(L.[ID]),0) as 'EBO %',
				count(case when L.StatusID  = 5 and datediff(dd,[FundedDate],[PaidOffDate]) >= 365 then 1 else null end)*1.0/nullif(count(L.[ID]),0) as 'PIF %',
				count(case when L.StatusID  in (9,12,13,14,20,21,22) then 1 else null end)*1.0/nullif(count(L.[ID]),0) as 'Charge-Off %',
				count(case when L.StatusID in (4) and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end)*1.0/nullif(count(L.[ID]),0) as 'Open %',
				count(case when L.StatusID in (6) and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end)*1.0/nullif(count(L.[ID]),0) as 'Bounced %',
				count(case when L.StatusID not in (4,6) and datediff(mm,[FundedDate],getdate()) <= 6 then 1 else null end)*1.0/nullif(count(L.[ID]),0) as 'Other %'
			From Loans as L --join FinancePlans as FP on FP.ID = L.PlanID
			where FundedAmount>0
			Group by DealerID) as MM on MM.DealerID = D.ID --and MM.FPID = FP.ID
Where
	D.StatusID in (1,2) --and 
	--(select count(ID)
	--from Loans
	--where datediff(mm,[FundedDate],getdate()) <= 6
	--	and DealerID = D.ID) > 0
	and A.StateID = 'LA'
Order by
	MC.SixMonthLeases desc,
	D.ID desc,
	D.RiskScore desc





