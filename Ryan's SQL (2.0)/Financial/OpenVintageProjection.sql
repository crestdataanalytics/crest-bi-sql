SELECT
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
		(sum(case when datediff(mm,[Funded Date],getdate()) = 4 and leasequeuekey in (4,6) then ([Funded Amount]*1.64)
			when datediff(mm,[Funded Date],getdate()) = 5 and leasequeuekey in (4,6) then ([Funded Amount]*1.706)
			when datediff(mm,[Funded Date],getdate()) = 6 and leasequeuekey in (4,6) then ([Funded Amount]*1.759)
			when datediff(mm,[Funded Date],getdate()) = 7 and leasequeuekey in (4,6) then ([Funded Amount]*1.808)
			when datediff(mm,[Funded Date],getdate()) = 8 and leasequeuekey in (4,6) then ([Funded Amount]*1.8497)
			when datediff(mm,[Funded Date],getdate()) = 9 and leasequeuekey in (4,6) then ([Funded Amount]*1.88)
			when datediff(mm,[Funded Date],getdate()) = 10 and leasequeuekey in (4,6) then ([Funded Amount]*1.908)
			when datediff(mm,[Funded Date],getdate()) = 11 and leasequeuekey in (4,6) then ([Funded Amount]*1.931) 
			when datediff(mm,[Funded Date],getdate()) >= 12 and leasequeuekey in (4,6) then ([Funded Amount]*1.9506) end)
			+
		sum(case when datediff(mm,[Funded Date],getdate()) = 4 and leasequeuekey not in (4,6) then [Total Paid]
			when datediff(mm,[Funded Date],getdate()) = 5 and leasequeuekey not in (4,6) then [Total Paid]
			when datediff(mm,[Funded Date],getdate()) = 6 and leasequeuekey not in (4,6) then [Total Paid]
			when datediff(mm,[Funded Date],getdate()) = 7 and leasequeuekey not in (4,6) then [Total Paid]
			when datediff(mm,[Funded Date],getdate()) = 8 and leasequeuekey not in (4,6) then [Total Paid]
			when datediff(mm,[Funded Date],getdate()) = 9 and leasequeuekey not in (4,6) then [Total Paid]
			when datediff(mm,[Funded Date],getdate()) = 10 and leasequeuekey not in (4,6) then [Total Paid]
			when datediff(mm,[Funded Date],getdate()) = 11 and leasequeuekey not in (4,6) then [Total Paid] 
			when datediff(mm,[Funded Date],getdate()) >= 12 and leasequeuekey not in (4,6) then [Total Paid] end))/
		sum([Funded Amount]) as 'Projected Multiple',
		sum([Total Paid])*1.0/sum([Funded Amount]) as 'Current Multiple',
		sum(case when datediff(mm,[Funded Date],getdate()) >= 12 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],getdate()) = 11 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],getdate()) = 10 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],getdate()) = 9 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],getdate()) = 8 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],getdate()) = 7 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],getdate()) = 6 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],getdate()) = 5 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],getdate()) = 4 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],getdate()) = 3 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],getdate()) = 2 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],getdate()) = 1 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],getdate()) = 0 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.06
		else 0
		end)/count([Lease ID]) as 'Actual vs. Target'
FROM
	Leases
WHERE
	[Funded Date] between '5-1-15' and '3-30-16'
GROUP BY
	CONVERT(CHAR(7),[Funded Date],120)
ORDER BY
	CONVERT(CHAR(7),[Funded Date],120) asc