select
	R.Category,
	sum(L.[Funded Amount]) as funded,
	(sum(FP.[Retailer Discount %]*L.[Funded Amount]))/sum(L.[Funded Amount]) as WAdiscount,
	avg(FP.[Retailer Discount %]) as discount,
	(sum(FP.[90 Day Payoff %]*L.[Funded Amount]))/sum(L.[Funded Amount]) as WAsacDiscount,
	avg(FP.[90 Day Payoff %]) as sacDiscount,
	(sum(FP.[Total Note %]*L.[Funded Amount]))/sum(L.[Funded Amount]) as WAnote,
	avg(FP.[Total Note %]) as note,
	avg(L.[Application Fee]) as fee
from
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
	join [Finance Plans] as FP on FP.FinancePlanKey = L.FinancePlanKey
where
	[Funded Date] > '7-31-15'
group by
	R.Category
order by
	R.Category

-----------------------

select
	R.Category,
	avg(case when LeaseQueueKey in (9,12,13,14,20,21,22) then datediff(mm,L.[Funded Date],L.[Close Date]) end) as COmonthsOpen,
	avg(case when LeaseQueueKey in (5) and datediff(dd,[Funded Date],[Close Date]) between 90 and 364 then datediff(mm,L.[Funded Date],L.[Close Date]) end) as EBOmonthsOpen,
	avg(case when Leasequeuekey in (9,12,13,14,20,21,22) then [Total Paid]/nullif([Funded Amount],0) end) as COmultiple,
	avg(case when LeaseQueueKey in (5) and datediff(dd,[Funded Date],[Close Date]) between 90 and 364 then [Total Paid]/nullif([Funded Amount],0) end) as EBOmultiple,
	avg(case when LeaseQueueKey in (5) and datediff(dd,[Funded Date],[Close Date]) > 364 then [Total Paid]/nullif([Funded Amount],0) end) as PIFmultiple,
	avg(case when LeaseQueueKey in (5) and datediff(dd,[Funded Date],[Close Date]) < 90 then [Total Paid]/nullif([Funded Amount],0) end) as SACmultiple
from
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
	join [Finance Plans] as FP on FP.FinancePlanKey = L.FinancePlanKey
where
	[Funded Date] > '7-31-14' and [Funded Date] < '7-31-15'
group by
	R.Category
order by
	R.Category

---------------------------

select
	R.Category,
	sum(L.[Funded Amount]) as Funded,
	sum(L.[Total Paid]) as Paid,
	sum(pp.PredictedPaid) as ExpectedCash,
	sum(case when leasequeuekey = 6 then pp.PredictedPaid else 0 end)/nullif(sum(case when leasequeuekey = 6 then [Funded Amount] else 0 end),0) as ExpectedCashRecoveryMultiple
from
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
		left outer join
			(SELECT
				L.[Lease ID],
				case 
					--// Dynamic
					when LeaseQueueKey in (6) and ((L.[Payments Returned] >= 3.5 and [Total Paid] < 240 and L.[Payments Cleared] >=18) or (L.[Payments Returned] >=3.5 and L.[Payments Cleared] >= 18) or (L.[Payments Returned] > 8.5)) then 
						(0.55*[Funded Amount])
					when LeaseQueueKey in (6) and [Total Paid] < 10 then
						(0.015*[Funded Amount])
					when LeaseQueueKey in (6) then
					((case when datediff(mm,[Funded Date],getdate()) = 0 then 0.34
								when datediff(mm,[Funded Date],getdate()) = 1 then 0.45
								when datediff(mm,[Funded Date],getdate()) = 2 then 0.574276
								when datediff(mm,[Funded Date],getdate()) = 3 then 0.787779
								when datediff(mm,[Funded Date],getdate()) = 4 then 0.90620
								when datediff(mm,[Funded Date],getdate()) = 5 then 1.062603
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.210379
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.350049
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.481444
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.605123
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.724040
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.829727
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.907534 end)
					+((-0.015851)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.027979
						when C.[Monthly Income] between 3001 and 4500 then (-0.038214/2)*2
						when C.[Monthly Income] > 4500 then (-0.047496/3)*3 else 0 end)
					+(case when C.Age between 18 and 25 then -0.007051
						when C.Age between 26 and 35 then (-0.002978/2)*2
						when C.Age > 35 then (0.004253/3)*3 else 0 end)
					+((-0.208059)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.208059-0.255615*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12-datediff(mm,[Funded Date],getdate()))*0.124))
					*[Funded Amount]
					end as PredictedPaid
				--count(case when LeaseQueueKey in (4,6) then 1 else null end) as 'Current Leases'
			FROM
				Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
					join Customers as C on C.[Customer ID] = L.[Customer ID]
			WHERE
				--[Funded Amount] > 0 and [Funded Date] > '12-31-15'
				LeaseQueueKey<>21) as PP on PP.[Lease ID] = L.[Lease ID]
where
	[Funded Date] > '12-31-15'
group by
	R.Category
order by
	R.Category