SELECT
	CONVERT(CHAR(7),[Funded Date],120) as Vintage,
	count(case when [Total Paid] = 0 then 1 else null end) as CNP,
	count(case when [Total Paid] = 0 then 1 else null end)*1.0/count([Funded Amount]) as CNP,
	sum(case when [Total Paid] = 0 then [Funded Amount] else 0 end) as NP,
	sum(case when [Total Paid] = 0 then [Funded Amount] else 0 end)/sum([Funded Amount]) as NP$
FROM
	Leases
WHERE
	[Funded Date] between '1-1-16' and '6-30-16'
	--and [Total Paid] = 0
	and [Funded Amount] >0
	and LeaseQueueKey not in (21)
GROUP BY
	CONVERT(CHAR(7),[Funded Date],120)

