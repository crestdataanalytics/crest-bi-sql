/*
SELECT
	CONVERT(CHAR(7),LPS.DueDate,120) as Month,
	SUM(case when LP.[PaymentDate] < LPS.DueDate and (LPS.DueDate between LH.StartDate and LH.EndDate) then LP.[Amount] else 0.00 end) as AmountPaid,
	SUM(case when LPSS.DueDate < LPS.DueDate and (LPS.DueDate between LH.StartDate and LH.EndDate) then LPSS.[DueAmount] else 0.00 end) as AmountDue
	--SUM(datediff(mm,L.FundedDate,dateadd(mm,0,LPS.DueDate))*(L.TotalNote/12)) as AmountDue2
FROM
	Loans as L join vw_LoanPaymentSchedule as LPS on L.ID = LPS.LoanID
	join vw_LoanPaymentSchedule as LPSS on LPSS.LoanID = L.ID
	join LoanHistory as LH on LH.LoanID = L.ID and LH.StatusID = 6
	left outer join vw_loanPayments as LP on LP.LoanID = L.ID
WHERE
	L.FundedDate < getdate()
	and LPS.DueDate <= dateadd(mm,-1,getdate())
	and L.FundedDate > '12-31-14'
GROUP BY
	CONVERT(CHAR(7),LPS.DueDate,120)
ORDER BY
	CONVERT(CHAR(7),LPS.DueDate,120) desc


SELECT
	CONVERT(CHAR(7),LPS.DueDate,120) as Month,
	sum(L.PastDueAmount)
FROM
	Loans as L join vw_LoanPaymentSchedule as LPS on L.ID = LPS.LoanID
	join LoanHistory as LH on LH.LoanID = L.ID and ((CONVERT(CHAR(7),LPS.DueDate,120)=CONVERT(CHAR(7),LH.EndDate,120) and LH.StatusID = 6) and (CONVERT(CHAR(7),LPS.DueDate,120)=CONVERT(CHAR(7),LH.StartDate,120) and LH.StatusID = 12))
WHERE
	L.FundedDate < getdate()
	and LPS.DueDate <= dateadd(mm,-1,getdate())
	and L.FundedDate > '12-31-14'
GROUP BY
	CONVERT(CHAR(7),LPS.DueDate,120)
ORDER BY
	CONVERT(CHAR(7),LPS.DueDate,120) desc


WITH DateTable
AS
(
  SELECT CAST('1-1-2015' AS Date) AS [DATE]
  UNION ALL
  SELECT DATEADD(mm, 1, [DATE])
  FROM DateTable
  WHERE DATEADD(mm, 1, [DATE]) < cast(getdate() as Date)
)

SELECT
	CONVERT(CHAR(7),LH.CO,120) as Month,
	sum(case when LH.CO = LH.DL then L.PastDueAmount end)*1.0/(sum(case when LP.PaymentDate < LH.CO and LP.ReturnDate is null then LP.Amount end)-
		sum(case when LPS.DueDate < LH.CO then LPS.DueAmount end)) as COrate
FROM
	Loans as L left outer join
		(select
			LoanID,
			min(case when StatusID = 12 then cast(startDate as date) end) as CO,
			max(case when StatusID = 6 then cast(EndDate as date) end) as DL
		from LoanHistory
		where STatusID in (6,12)
		group by LoanID) as LH on LH.LoanID = L.ID
	join vw_loanpayments as LP on LP.LoanID = L.ID
	join vw_loanPaymentSchedule as LPS on LPS.LoanID = L.ID
WHERE
	L.FundedDate > '1-1-16'
GROUP BY
	CONVERT(CHAR(7),LH.CO,120)
ORDER BY
	CONVERT(CHAR(7),LH.CO,120) desc

SELECT
	CONVERT(CHAR(7),LPS.DueDate,120), 
	sum(DueAMount) as amountDue, 
	Sum(Lp.Amount) as amountPaid
FROM
	vw_scheduledloanpayments as LPS right outer join vw_LoanPayments as LP on LPS.LoanID = LP.ID
WHERE 
	Year(DueDate) = 2016
	and LP.PaymentDate < getdate()
	and LP.ReturnDate is null
	and LP.appliedtodate < getdate()
Group by
	CONVERT(CHAR(7),DUeDate,120)
order by
	CONVERT(CHAR(7),DUeDate,120) desc
*/

select
	  CONVERT(CHAR(7),LPS.DueDate,120) as PayMonth,
	  sum(LPDue.DueAmount) as AmountDue,
	  sum(lpss.amount) as AmountCollected
from
       Loans as L
	   join 
	   vw_LoanPaymentSchedule as LPS on L.ID = LPS.LoanID
	   join 
			(select
				lps.loanID,
				lps.duedate,
				sum(lpsdue.dueamount) as dueamount
		     from
				vw_LoanPaymentSchedule as LPS join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID and LPS.DueDate > LPSDue.DueDate
					Join LoanHistory as LH on LH.LoanID = LPS.LoanID and (LPS.DueDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
			where lps.loanid = 1161081
			group by
				lps.loanID,
				lps.duedate) as lpdue on lpdue.loanid = lps.loanid and lpdue.duedate = lps.duedate
       join 
			(Select
				lp.LoanID,
				lp.PaymentDate,
				sum(b.Amount) as amount
			From
				vw_loanPayments as LP join vw_loanPayments as b on b.LoanID = LP.LoanID and LP.paymentDate >= b.paymentdate and LP.ReturnDate is null
					Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
			where lp.loanid = 1161081
			group by
				lp.LoanID,
				lp.PaymentDate) as LPSS on LPSS.LoanID = LPS.LoanID and datediff(m,lpss.paymentdate,lps.duedate) = 0
WHERE
	L.FundedDate > '8-1-15'
	and lps.loanid=1161081
group by
	  CONVERT(CHAR(7),LPS.DueDate,120)
Order by
	  CONVERT(CHAR(7),LPS.DueDate,120) desc


