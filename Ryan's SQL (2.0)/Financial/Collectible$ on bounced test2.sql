/*
select
	  CONVERT(CHAR(7),LPS.DueDate,120) as PayMonth,
	  sum(LPDue.DueAmount) as AmountDue,
	  sum(lpss.TotalPaid) as AmountCollected,
	  sum(lpss.CollectedThisMonth) as CollectThisMonth
from
       Loans as L
	   join 
	   vw_LoanPaymentSchedule as LPS on L.ID = LPS.LoanID
	   left outer join 
			(select
				lps.loanID,
				lps.duedate,
				sum(lpsdue.dueamount) as dueamount
		     from
				vw_LoanPaymentSchedule as LPS join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID and LPS.DueDate > LPSDue.DueDate
					Join LoanHistory as LH on LH.LoanID = LPS.LoanID and (LPS.DueDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
			where lps.loanid = 1266767
			group by
				lps.loanID,
				lps.duedate) as lpdue on lpdue.loanid = lps.loanid and lpdue.duedate = lps.duedate
       left outer join 
			(Select
				lp.LoanID,
				CONVERT(CHAR(7),LP.paymentdate,120) as PD,
				sum(case when lp.DeletedDate is null and LP.paymentDate >= b.paymentdate then b.amount else 0 end)-sum(case when lp.DeletedDate is null and lp.returnDate is not null and LP.paymentDate >= b.paymentdate then b.amount else 0 end) as TotalPaid,
				TMLP.TotalPaid as CollectedThisMonth
			From
				loanPayments as LP join loanPayments as b on b.LoanID = LP.LoanID
					Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
					left outer join
						(SELECT
							LoanID,
							CONVERT(CHAR(7),paymentdate,120) as TPD,
							sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						FROM
							LoanPayments
						where loanid = 1266767
						GROUP BY
							LoanID,
							CONVERT(CHAR(7),paymentdate,120)) as TMLP on TMLP.LoanID = LP.LoanID and TMLP.TPD = CONVERT(CHAR(7),LP.paymentdate,120)
			where lp.loanid = 1266767
			group by
				lp.LoanID,
				 CONVERT(CHAR(7),LP.paymentdate,120),
				 TMLP.TotalPaid) as LPSS on LPSS.LoanID = LPS.LoanID
WHERE
	L.FundedDate > '1-1-15'
	and lps.loanid = 1266767
group by
	  CONVERT(CHAR(7),LPS.DueDate,120)
Order by
	  CONVERT(CHAR(7),LPS.DueDate,120) desc
*/

			Select
				lp.LoanID,
				CONVERT(CHAR(7),LP.paymentdate,120) as PD,
				TMLP.TotalPaid as CollectedThisMonth,
				DA.totaldue,
				TPAT.TotalPaid
			From
				loanPayments as LP join loanPayments as b on b.LoanID = LP.LoanID
					Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
					left outer join
						(SELECT
							LoanID,
							CONVERT(CHAR(7),paymentdate,120) as TPD,
							sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						FROM
							LoanPayments
						where loanid = 1266767
						GROUP BY
							LoanID,
							CONVERT(CHAR(7),paymentdate,120)) as TMLP on TMLP.LoanID = LP.LoanID and TMLP.TPD = CONVERT(CHAR(7),LP.paymentdate,120)		
					left outer join
						(SELECT
								lps.LoanID,
								CONVERT(CHAR(7),LPS.Duedate,120) as duedate,
								sum(lps.dueamount) over (order by lps.duedate rows unbounded preceding) as totaldue
							from
							(
							select
								lps.loanID,
								CONVERT(CHAR(7),lps.duedate,120) as duedate,
								sum(lps.Dueamount) as dueamount
							 from
								vw_LoanPaymentSchedule as LPS --join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID
									--Join LoanHistory as LH on LH.LoanID = LPS.LoanID and (LPS.DueDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
							where lps.loanid = 1266767
							group by
								lps.loanID,
								CONVERT(CHAR(7),duedate,120)
							)as LPS) as DA on DA.LoanID = LP.LoanID and DA.duedate = CONVERT(CHAR(7),LP.paymentdate,120)
					Left outer join
						(SELECT
							LoanID,
							CONVERT(CHAR(7),paymentdate,120) as TPD,
							sum(totalpaid) over (order by paymentdate rows unbounded preceding) as totalPaid
						FROM
						(SELECT
							LoanID,
							CONVERT(CHAR(7),paymentdate,120) as paymentdate,
							sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						FROM
							LoanPayments
						where loanid = 1266767
						GROUP BY
							LoanID,
							CONVERT(CHAR(7),paymentdate,120)) as TPAD) as TPAT on TPAT.LoanID = LP.LoanID and TPAT.TPD = CONVERT(CHAR(7),LP.paymentdate,120)
			where lp.loanid = 1266767
			group by
				lp.LoanID,
				 CONVERT(CHAR(7),LP.paymentdate,120),
				 TMLP.TotalPaid,
				 DA.totaldue,
				 TPAT.TotalPaid


			select
				lps.loanID,
				LPS.Duedate,
				sum(lpsdue.Dueamount)
		     from
				vw_LoanPaymentSchedule as LPS join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID and lps.duedate >= lpsdue.duedate
					--Join LoanHistory as LH on LH.LoanID = LPS.LoanID and (LPS.DueDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
			where lps.loanid = 1266767
			group by
				lps.loanID,
				LPS.Duedate

			
			SELECT
				--lps.LoanID,
				CONVERT(CHAR(7),LPS.Duedate,120),
				sum(lps.dueamount) over (order by lps.duedate rows unbounded preceding) as cumulative_sales
			from
			(
			select
				lps.loanID,
				CONVERT(CHAR(7),lps.duedate,120) as duedate,
				sum(lps.Dueamount) as dueamount
		     from
				vw_LoanPaymentSchedule as LPS --join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID
					--Join LoanHistory as LH on LH.LoanID = LPS.LoanID and (LPS.DueDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
			where lps.loanid = 1266767
			group by
				lps.loanID,
				CONVERT(CHAR(7),duedate,120)
			) as LPS


						SELECT
							LoanID,
							CONVERT(CHAR(7),paymentdate,120) as TPD,
							sum(totalpaid) over (order by paymentdate rows unbounded preceding) as totalPaid
						FROM
						(SELECT
							LoanID,
							CONVERT(CHAR(7),paymentdate,120) as paymentdate,
							sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						FROM
							LoanPayments
						where loanid = 1266767
						GROUP BY
							LoanID,
							CONVERT(CHAR(7),paymentdate,120)) as TPAD