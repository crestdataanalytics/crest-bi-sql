WITH DateTable
AS
(
  SELECT CAST('1-1-2014' AS Date) AS [DATE]
  UNION ALL
  SELECT DATEADD(m,
   1, [DATE])
  FROM DateTable
  WHERE DATEADD(m, 1, [DATE]) < cast(getdate() as Date)
)

Select
    CONVERT(CHAR(7),dt.[date],120) as PD,
	L.ID,
	--sum(case when paymentdate < dt.[date] then TPAT.totalPaid end) as TotalPaid,
	coalesce(sum(TMLP.TotalPaid),0) as PaidToDate,
	coalesce(TMLP.TotalPaid,0) as Paid,
	case when L.FundedDate <= dt.[Date] then (datediff(mm,L.FundedDate,dt.[date])*1.0/12)*L.TotalNote else 0 end as Due
FROM 
	[DateTable] as dt                   
	cross join 
	Loans as L
	Join LoanHistory as LH on LH.LoanID = L.ID and (dt.[date] between LH.StartDate and LH.EndDate and LH.StatusID in (6))
	left outer join
        (SELECT
            LP.LoanID,
            CONVERT(CHAR(7),paymentdate,120) as TPD,
            sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
        FROM
            LoanPayments as LP
			--Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
        where --LP.loanid = 1266767
			LP.LoanStatusID = 6
        GROUP BY
            LP.LoanID,
            CONVERT(CHAR(7),paymentdate,120)) as TMLP on TMLP.TPD = CONVERT(CHAR(7),dt.[date],120) and TMLP.LoanID = L.ID 
	 left outer join
        (SELECT
            LP.LoanID,
            paymentdate,
            sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
        FROM
            LoanPayments as LP
			--Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
        where --LP.loanid = 1266767
			LP.LoanStatusID = 6
        GROUP BY
            LP.LoanID,
            paymentdate) as TPAT on TPAT.LoanID = L.ID  
--WHERE 
	--L.ID > 1266767
GROUP BY
    CONVERT(CHAR(7),dt.[date],120),
	L.ID,
	TMLP.TotalPaid,
	L.Fundeddate,
	dt.[date],
	L.totalnote


