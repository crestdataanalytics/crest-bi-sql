declare @date date
set @date = '9-1-16'

select
	CONVERT(CHAR(7),[funded Date],120) as Vintage,
	count([Lease ID]) as 'Leases'
from
	Leases as L
where
	[Funded Amount] > 0 and [Funded Date] <= @date and ((leasequeuekey in (4,6) and [close date] is null) or (leasequeuekey in (5,13,14,20,21,22,12) and [close date] > @date))
group by CONVERT(CHAR(7),[funded Date],120)
order by CONVERT(CHAR(7),[funded Date],120) desc

select
	count([Lease ID]) as 'Leases'
from
	Leases as L
where
	[Funded Amount] > 0 and [Funded Date] <= @date and ((leasequeuekey in (4,6) and [close date] is null) or (leasequeuekey in (5,13,14,20,21,22,12) and [close date] > @date))