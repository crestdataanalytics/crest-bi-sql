WITH DateTable
AS
(
  SELECT CAST('4-1-15' AS Date) AS [DATE]
  UNION ALL
  SELECT DATEADD(m,
   1, [DATE])
  FROM DateTable
  WHERE DATEADD(m, 1, [DATE]) < cast(getdate() as Date)
)

Select
    CONVERT(CHAR(7),dt.[date],120) as PD,
	--DA.LoanID,
    DA.totaldue,
	TPAT.TotalPaid,
	sum(TPAT.TotalPaid),
	sum(TMLP.TotalPaid),
	TMLP.TotalPaid as PaidThisMonth
FROM 
	[DateTable] as dt                   
	left outer join
            (SELECT
                    --lps.LoanID,
                    CONVERT(CHAR(7),LPS.Duedate,120) as duedate,
                    sum(lps.dueamount) over (order by lps.duedate rows unbounded preceding) as totaldue
                from
                (
                select
                    --lps.loanID,
                    CONVERT(CHAR(7),lps.duedate,120) as duedate,
                    sum(lps.Dueamount) as dueamount
                from
                    vw_LoanPaymentSchedule as LPS --join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID
                        Join LoanHistory as LH on LH.LoanID = LPS.LoanID and (LPS.DueDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
                --where lps.loanid = 1266767
                group by
                    --lps.loanID,
                    CONVERT(CHAR(7),duedate,120)
                )as LPS) as DA on DA.duedate = CONVERT(CHAR(7),dt.[date],120)
	left outer join
        (SELECT
            --LP.LoanID,
            CONVERT(CHAR(7),paymentdate,120) as TPD,
            sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
        FROM
            LoanPayments as LP
			Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
       -- where LP.loanid = 1266767
        GROUP BY
            --LP.LoanID,
            CONVERT(CHAR(7),paymentdate,120)) as TMLP on TMLP.TPD = CONVERT(CHAR(7),dt.[date],120) --and TMLP.LoanID = DA.LoanID 
	 left outer join
        (SELECT
            --LoanID,
            CONVERT(CHAR(7),paymentdate,120) as TPD,
            sum(totalpaid) over (order by paymentdate rows unbounded preceding) as totalPaid
        FROM
			(SELECT
				--LP.LoanID,
				CONVERT(CHAR(7),paymentdate,120) as paymentdate,
				sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
			FROM
				LoanPayments as LP
				Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
			--where LP.loanid = 1266767
			GROUP BY
				--Lp.LoanID,
				CONVERT(CHAR(7),paymentdate,120)) as TPAD) as TPAT on TPAT.TPD = CONVERT(CHAR(7),dt.[date],120) --and TPAT.LoanID = DA.LoanID  
GROUP BY
	CONVERT(CHAR(7),dt.[date],120),
	--DA.LoanID,
    DA.totaldue,
	TPAT.TotalPaid,
	TMLP.TotalPaid
               
/*where TMLP.ID in 
    (
        select min(lp.id)
        From
            loanPayments as LP 
        Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
        group by lp.loanid, CONVERT(CHAR(7),LP.paymentdate,120) 
    )   */ 

