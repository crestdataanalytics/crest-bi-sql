/****** SELECT Monthly FPD Metrics  ******/
declare @date1 as date
declare @date2 as date

set @date1 = '12-31-15'
set @date2 = '8-29-16'
--set @date2 = dateadd(d,1,getdate())

SELECT
	datepart(week,L.FundedDate) as Weeknumber,
	concat(datepart(month,L.FundedDate),'/',datepart(year,L.FundedDate)) as Vintage,
	PPT.[Period],
	count(FSP.FirstScheduledPaymentOutcomeID) as TotalLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
	count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
FROM
	Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	join LoanStatuses as LS on LS.ID = L.StatusID
	join ApplicantJob as AJ on AJ.ApplicantID = L.ApplicantID
	join PayPeriodTypes as PPT on PPT.ID = AJ.PayPeriodTypeID
WHERE
	L.FundedAmount > 0
	and FSP.IsCurrentOutcome = 1
	and L.FundedDate > @date1
	and L.FundedDate < @date2
GROUP BY
	datepart(week,L.FundedDate),
	concat(datepart(month,L.FundedDate),'/',datepart(year,L.FundedDate)),
	PPT.[Period]
ORDER BY
	datepart(week,L.FundedDate) desc,
	concat(datepart(month,L.FundedDate),'/',datepart(year,L.FundedDate)) desc