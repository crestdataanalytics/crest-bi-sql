SELECT
			CONVERT(CHAR(7),L.[FundedDate],120) as Vintage,
			count(case when datediff(mm,[FundedDate],LP.PaymentDate) <= 3  and LP.TotalPaid = 0 then 1 else null end) as 'CountNoPay',
			count(case when datediff(mm,[FundedDate],LP.PaymentDate) <= 3  and LP.TotalPaid = 0 then 1 else null end)*1.0/count(*) as 'CountNoPayRate',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) <= 3 and LP.TotalPaid = 0 then FundedAmount end) as 'SumNoPay',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) <= 3 and LP.TotalPaid = 0 then FundedAmount end)*1.0/sum(FundedAmount) as 'SumNoPayRate'	
		FROM
			Loans as L join Dealers as D on L.DealerID = D.ID
			join Users as U on U.ID = D.DealerRepID
			JOIN
				(SELECT
					LoanID,
					PaymentDate,
					sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						--sum(case when DeletedDate is null and returnDate is null and amount < 0 then amount else 0 end) as TotalPaid
				FROM
					LoanPayments
				WHERE
					PaymentDate < getdate()
					--and LoanID = 519707
				GROUP BY
					LoanID,
					PaymentDate
				) as LP on LP.LoanID = L.ID
		WHERE --L.DealerID = 3626 and
			[FundedAmount] > 0 
			and [FundedDate] > '12-31-15'
			--and CONVERT(CHAR(7),L.[FundedDate],120) = '2016-06'
			--and U.ID = 44697
		GROUP BY
			CONVERT(CHAR(7),L.[FundedDate],120)
			--L.ID,
			--L.FundedAmount
			--concat(U.FirstName,' ',U.LastName)
			--L.DealerID
		--Having
			--sum(case when datediff(dd,[FundedDate],LP.PaymentDate) <= 45 then LP.TotalPaid end) = 0
			--and sum(case when datediff(dd,[FundedDate],LP.PaymentDate) between 45 and 60 then LP.TotalPaid end) > 0
			--sum(LP.TotalPaid) = 0
