SELECT
	CONVERT(CHAR(7),LP.PaymentDate,120) as 'Pay Month',
	case when datediff(mm,[FundedDate],[PaymentDate]) < 0 then 'Fee Month'
		when datediff(mm,[FundedDate],[PaymentDate]) = 0 then 'Month 0'
		when datediff(mm,[FundedDate],[PaymentDate]) = 1 then 'Month 1'
		when datediff(mm,[FundedDate],[PaymentDate]) = 2 then 'Month 2'
		when datediff(mm,[FundedDate],[PaymentDate]) = 3 then 'Month 3'
		when datediff(mm,[FundedDate],[PaymentDate]) = 4 then 'Month 4'
		when datediff(mm,[FundedDate],[PaymentDate]) = 5 then 'Month 5'
		when datediff(mm,[FundedDate],[PaymentDate]) = 6 then 'Month 6'
		when datediff(mm,[FundedDate],[PaymentDate]) = 7 then 'Month 7'
		when datediff(mm,[FundedDate],[PaymentDate]) = 8 then 'Month 8'
		when datediff(mm,[FundedDate],[PaymentDate]) = 9 then 'Month 9'
		when datediff(mm,[FundedDate],[PaymentDate]) = 10 then 'Month 10'
		when datediff(mm,[FundedDate],[PaymentDate]) = 11 then 'Month 11'
		when datediff(mm,[FundedDate],[PaymentDate]) = 12 then 'Month 12'
		when datediff(mm,[FundedDate],[PaymentDate]) > 12 then 'Month 13+'
		end as 'Pay Month',
	L.ApplicantID,
	L.[FundedAmount],
	sum(case when [ReturnDate] is null then LP.[Amount] else 0 end) as 'Amount Paid',
	TotalPaid=
            ( SELECT SUM(LPP.[Amount])
              FROM LoanPayments as LPP
               WHERE (CONVERT(CHAR(7),LPP.PaymentDate,120) <= CONVERT(CHAR(7),LP.PaymentDate,120)) and LPP.[ReturnDate] is null and L.ID = LPP.LoanID
                )
from vw_loans_reporting as L left join LoanPayments as LP on L.ID = LP.LoanID
where
	L.[FundedDate] between '1-1-15' and '2-1-15' and LP.[ReturnDate] is null
Group by
	CONVERT(CHAR(7),LP.PaymentDate,120),
	l.ApplicantID,
	L.[FundedAmount],
	l.[FundedDate],
	[PaymentDate],
	l.id
order by
	CONVERT(CHAR(7),LP.PaymentDate,120) asc