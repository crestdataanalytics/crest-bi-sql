declare @date1 as date
declare @date2 as date
declare @date3 as date
declare @date4 as date
declare @date5 as date

set @date1 = '7-31-16'
set @date2 = dateadd(d,7,@date1)
set @date3 = dateadd(d,7,@date2)
set @date4 = dateadd(d,7,@date3)
set @date5 = '9-1-16'

select
	count([Lease ID]) as leases,
	sum(case when [Funded Date] between @date1 and @date2 and [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)/
		sum(case when [Funded Date] between @date1 and @date2 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end) as FPD$w1,
	sum(case when [Funded Date] between @date2 and @date3 and [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)/
		sum(case when [Funded Date] between @date2 and @date3 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end) as FPD$w2,
	sum(case when [Funded Date] between @date3 and @date4 and [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)/
		sum(case when [Funded Date] between @date3 and @date4 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end) as FPD$w3,
	sum(case when [Funded Date] between @date4 and @date5 and [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)/
		sum(case when [Funded Date] between @date4 and @date5 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end) as FPD$w4,
	sum(case when [Funded Date] between @date1 and @date5 and [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)/
		sum(case when [Funded Date] between @date1 and @date5 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end) as FPD$all
from
	Leases
Where
	[Funded Date] >= @date1


select
	count([Lease ID]) as leases,
	sum(case when [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)/
		sum(case when [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end) as FPD$all
from
	Leases
Where
	[Funded Date] > @date1
	and [Funded Date] < @date5