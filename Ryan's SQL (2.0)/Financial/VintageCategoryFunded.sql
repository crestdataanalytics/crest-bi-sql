select
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	C.CategoryName,
	count([FundedAmount]),
	sum([FundedAmount]) as 'Funded',
	sum([FundedAmount])*1.0/count([FundedAmount]) as 'Avg. Funded'
from vw_loans_reporting as l right join DealerCategories as DC on l.DealerID = DC.DealerID
	join Categories as C on C.ID = DC.CategoryID
where
	[FundedAmount] > 0 and statusID in (4,5,6,9,12,13,14,20,21,22) and [FundedDate] >= '1-1-16' and C.CategoryName <> 'Demo'
group by
	CONVERT(CHAR(7),[FundedDate],120),
	C.CategoryName
order by
	CONVERT(CHAR(7),[FundedDate],120) asc,
	C.CategoryName
