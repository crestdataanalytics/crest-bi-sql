select
	CONVERT(CHAR(7),L.[fundedDate],120) as Vintage,
	C.[CategoryName] as 'Category',
	CASE WHEN A.[StateID] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN A.[StateID] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN A.[StateID] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN A.[StateID] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT'
	END AS 'Region',
	count(case when L.[FundedAmount] > 0 then L.ID else null end) as LeaseCount,
	sum(L.[FundedAmount]) as 'Funded $',
	sum(T.Paid) as 'Paid $',
	sum(L.[FundedAmount])/count(L.[ID]) as 'Avg. Funded',
	count(case when L.StatusID = 5 and datediff(dd,L.[FundedDate],L.[PaidOffDate]) < 90 then 1 else null end)*1.0/count(L.[ID]) as 'SAC %',
	count(case when L.StatusID = 5 and datediff(dd,L.[FundedDate],L.[PaidOffDate]) between 90 and 364 then 1 else null end)*1.0/count(L.[ID]) as 'EBO %',
	count(case when L.StatusID = 5 and datediff(dd,L.[FundedDate],L.[PaidOffDate]) >= 365 then 1 else null end)*1.0/count(L.[ID]) as 'PIF %',
	count(case when L.StatusID in (9,12,13,14,20,21,22) then 1 else null end)*1.0/count(L.[ID]) as 'Charge-Off %',
	count(case when L.StatusID in (4,6) then 1 else null end)*1.0/count(L.[ID]) as 'Open %',
	count(case when L.StatusID not in (4,5,6,9,12,13,14,20,21,22) then 1 else null end)*1.0/count(L.[ID]) as 'Other %'
from
	Loans as L join Dealers as D on L.DealerID = D.ID
	left outer join [DealerCategories] as DC on DC.DealerID = D.ID
	left outer join [Categories] as C on C.ID = DC.CategoryID
    left outer join ( SELECT LPP.LoanID, SUM(LPP.[Amount]) as Paid
              FROM LoanPayments as LPP
               WHERE (LPP.[ReturnDate] is null)
			   Group by LPP.LoanID
                ) as T on T.LoanID = L.ID
	left outer join Addresses as A on A.ID = D.AddressID
where
	L.[fundedDate] >= '1-1-14' 
	and L.[fundedDate] < '5-31-16'
group by
	CONVERT(CHAR(7),L.[fundedDate],120),
	C.[CategoryName],
	CASE WHEN A.[StateID] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN A.[StateID] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN A.[StateID] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN A.[StateID] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT' END
order by
	CONVERT(CHAR(7),L.[fundedDate],120) asc

