/*
SET DATEFIRST 1

    SELECT 
		CAST(MIN(FundedDate) AS VARCHAR(20))+' TO '+CAST (MAX(FundedDate) AS VARCHAR(20)) AS DATE,
		D.ID,
		D.Name,
        SUM(FundedAmount) AS Funded,
        COUNT(L.ID) as Leases
    FROM   
		Loans as L join Dealers as D on L.DealerID = D.ID
	WHERE
		FundedDate > '12-31-15'
    GROUP BY 
		DATEPART(WEEK,FundedDate),
		D.ID,
		D.Name
    HAVING COUNT(DISTINCT FundedDate)=7
	ORDER BY
		D.ID desc

SET DATEFIRST 7
*/

Select 
DealerID,
cast(FundedDate as date) as FundedDate,
sum(FundedAmount)
FROM
Loans
WHERE
FundedDate > '12-31-15'
GROUP BY
DealerID,
FundedDate

SELECT
D.ID,
Name,
RiskScore,
C.CategoryName
FROM
Dealers as D join DealerCategories as DC on D.ID = DC.DealerID
join Categories as C on C.ID = DC.CategoryID