/*
drop table ##EligibleCustomers
select 
		max(pad.SubjectID) as 'SubjectID'
		into ##EligibleCustomers
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
			join Leases as L on L.[Customer ID] = pad.SubjectID
			join Retailers as R on R.[Retailer ID] = L.[Retailer ID]
		Where 
		    pap.ProviderName = 'Ebureau'
			and R.[Retailer Group Name] like '%Bennettsville%'
		group by
			pad.SubjectID
*/

declare @date1 as date
declare @date2 as date
declare @name as varchar(25)
set @date1 = '12-31-14'
set @date2 = getdate()
set @name = '%Bennettsville%'

--WITH DateTable
--AS
--(
--  SELECT CAST('6-1-2016' AS Date) AS [DATE]
--  UNION ALL
--  SELECT DATEADD(dd, 7, [DATE])
--  FROM DateTable
--  WHERE DATEADD(dd, 7, [DATE]) < cast(getdate() as Date)
--)

select
	--dt.[Date],
	[Retailer Name],
	R.[Retailer ID],
	R.[State],
	cast(R.[Creation Date] as date),
	R.[Current Status],
	SL.[Approval Rate],
	SL.[Applications],
	SL.[Eligible Cust],
	SL.[Auto Approved],
	SL.[Total Approved],
	SL.[Avg. Approval],
	SL.[Conversion Rate],
	FL.[Funded Amount],
	FL.[# Funded],
	count(case when [LeaseQueueKey] in (5) then 1 else null end) as '# Paid',
	count(case when [LeaseQueueKey] in (12,9,13,14,20,21) then 1 else null end) as '# Charged-Off',
	count(case when [LeaseQueueKey] in (4,6) then 1 else null end) as '# Active',
	sum(case when [LeaseQueueKey] in (4) then [Total Note]-[Total Paid] else 0 end) as '$ Funded RCV',
	count(case when [LeaseQueueKey] in (6) then 1 else null end) as '# Bounced RCV',
	sum(case when [LeaseQueueKey] in (6) then [Total Note]-[Total Paid] else 0 end) as '$ Bounced RCV',
	sum([Total Paid]) as 'Total Paid',
	isnull(nullif(sum([Total Paid]),0)/nullif(sum(L.[Funded Amount]),0),0) as 'Multiple',
	count(case when [Met First Scheduled Payment] in ('No') then 1 else null end) as 'FPD #',
	count(case when [Met First Scheduled Payment] in ('Yes','Partially') then 1 else null end) as 'Paid #',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when L.[Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD % of Funded',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when L.[Funded Amount] > 0 and [Met First Scheduled Payment] in ('No','Yes','Partially') then 1 else null end),0)),0) as 'FPD % of Leases Come Due'
From 
	Retailers as R 
	left outer join Leases as L on L.[Retailer ID] = R.[Retailer ID]
	left outer join
		(Select
			R.[Retailer ID] as ID,
			count(case when [LeaseQueueKey] not in (7,11) then 1 else null end)*1.0/nullif(count(case when EC.[SubjectID] = [Customer ID] and [LeaseQueueKey] not in (11) then 1 else null end),0) as 'Approval Rate',
			count([Customer ID]) as 'Applications',
			--count(case when [LeaseQueueKey] in (11) then 1 else null end) as 'Cancelled',
			--count(case when [LeaseQueueKey] in (7) then 1 else null end) as 'Denied',
			count(case when EC.[SubjectID] = [Customer ID] and [LeaseQueueKey] not in (11) then 1 else null end) as 'Eligible Cust',
			count(case when [Auto Approved] = 1  and leasequeuekey not in (7,11) then 1 else null end) as 'Auto Approved',
			count(case when [LeaseQueueKey] not in (7,11) then 1 else null end) as 'Total Approved',
			sum(case when [LeaseQueueKey] not in (7,11) then [Approval Amount] else 0 end)/nullif(count(case when [LeaseQueueKey] not in (7,11) then 1 else null end),0) as 'Avg. Approval',
			count(case when [Funded Amount] > 0 then 1 else null end)*1.0/nullif(count(case when [LeaseQueueKey] not in (7,11) then 1 else null end),0) as 'Conversion Rate'
		From
			Retailers as R
			left outer join Leases as L on L.[Retailer ID] = R.[Retailer ID]
			left outer join ##EligibleCustomers as EC on EC.SubjectID = L.[Customer ID]
		Where
			[Retailer Group Name] like @name
			and (L.[Submitted Date] >= @date1 and L.[Submitted Date] < @date2)
		Group by
			R.[Retailer ID]) as SL on SL.ID = R.[Retailer ID] 
	left outer join
		(Select
			R.[Retailer ID] as ID,
			sum([Funded Amount]) as 'Funded Amount',
			count(case when [Funded Amount] > 0 then 1 else null end) as '# Funded'
		From
			Retailers as R
			left outer join Leases as L on L.[Retailer ID] = R.[Retailer ID]
			left outer join ##EligibleCustomers as EC on EC.SubjectID = L.[Customer ID]
		Where
			[Retailer Group Name] like @name
			and (L.[Funded Date] >= @date1 and L.[Funded Date] < @date2)
		Group by 
			R.[Retailer ID]) as FL on FL.ID = R.[Retailer ID]
where
	[Retailer Group Name] like @name
	and R.[Current Status] = 'Active'
group by
	R.[Retailer Name],
	--dt.[date],
	R.[Retailer ID],
	R.[State],
	cast(R.[Creation Date] as date),
	R.[Current Status],
	SL.[Approval Rate],
	SL.[Applications],
	SL.[Eligible Cust],
	SL.[Auto Approved],
	SL.[Total Approved],
	SL.[Avg. Approval],
	SL.[Conversion Rate],
	FL.[Funded Amount],
	FL.[# Funded]

/*
UNION ALL

Select
	null,
	null,
	null,
	null,
	null,
	SL.[Approval Rate],
	SL.[Applications],
	SL.[Eligible Cust],
	SL.[Auto Approved],
	SL.[Total Approved],
	SL.[Avg. Approval],
	SL.[Conversion Rate],
	FL.[Funded Amount],
	FL.[# Funded],
	count(case when [LeaseQueueKey] in (5) then 1 else null end) as '# Paid',
	count(case when [LeaseQueueKey] in (12,9,13,14,20,21) then 1 else null end) as '# Charged-Off',
	count(case when [LeaseQueueKey] in (4,6) then 1 else null end) as '# Active',
	sum(case when [LeaseQueueKey] in (4) then [Total Note]-[Total Paid] else 0 end) as '$ Funded RCV',
	count(case when [LeaseQueueKey] in (6) then 1 else null end) as '# Bounced RCV',
	sum(case when [LeaseQueueKey] in (6) then [Total Note]-[Total Paid] else 0 end) as '$ Bounced RCV',
	sum([Total Paid]) as 'Total Paid',
	isnull(nullif(sum([Total Paid]),0)/nullif(sum(L.[Funded Amount]),0),0) as 'Multiple',
	count(case when [Met First Scheduled Payment] in ('No') then 1 else null end) as 'FPD #',
	count(case when [Met First Scheduled Payment] in ('Yes','Partially') then 1 else null end) as 'Paid #',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when L.[Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD % of Funded',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when L.[Funded Amount] > 0 and [Met First Scheduled Payment] in ('No','Yes','Partially') then 1 else null end),0)),0) as 'FPD % of Leases Come Due'
From Retailers as R left outer join Leases as L on L.[Retailer ID] = R.[Retailer ID]
	left outer join
		(Select
			R.[Retailer ID] as ID,
			count(case when [LeaseQueueKey] not in (7,11) then 1 else null end)*1.0/nullif(count(case when EC.[SubjectID] = [Customer ID] and [LeaseQueueKey] not in (11) then 1 else null end),0) as 'Approval Rate',
			count([Customer ID]) as 'Applications',
			--count(case when [LeaseQueueKey] in (11) then 1 else null end) as 'Cancelled',
			--count(case when [LeaseQueueKey] in (7) then 1 else null end) as 'Denied',
			count(case when EC.[SubjectID] = [Customer ID] and [LeaseQueueKey] not in (11) then 1 else null end) as 'Eligible Cust',
			count(case when [Auto Approved] = 1  and leasequeuekey not in (7,11) then 1 else null end) as 'Auto Approved',
			count(case when [LeaseQueueKey] not in (7,11) then 1 else null end) as 'Total Approved',
			sum(case when [LeaseQueueKey] not in (7,11) then [Approval Amount] else 0 end)/nullif(count(case when [LeaseQueueKey] not in (7,11) then 1 else null end),0) as 'Avg. Approval',
			count(case when [Funded Amount] > 0 then 1 else null end)*1.0/nullif(count(case when [LeaseQueueKey] not in (7,11) then 1 else null end),0) as 'Conversion Rate'
		From
			Retailers as R
			left outer join Leases as L on L.[Retailer ID] = R.[Retailer ID]
			left outer join ##EligibleCustomers as EC on EC.SubjectID = L.[Customer ID]
		Where
			[Retailer Group Name] like @name
			and (L.[Submitted Date] >= @date1 and L.[Submitted Date] < @date2)
		Group by
			R.[Retailer ID]) as SL on SL.ID = R.[Retailer ID]
	left outer join
		(Select
			R.[Retailer ID] as ID,
			sum([Funded Amount]) as 'Funded Amount',
			count(case when [Funded Amount] > 0 then 1 else null end) as '# Funded'
		From
			Retailers as R
			left outer join Leases as L on L.[Retailer ID] = R.[Retailer ID]
			left outer join ##EligibleCustomers as EC on EC.SubjectID = L.[Customer ID]
		Where
			[Retailer Group Name] like @name
			and (L.[Funded Date] >= @date1 and L.[Funded Date] < @date2)
		Group by 
			R.[Retailer ID]) as FL on FL.ID = R.[Retailer ID]
where
	[Retailer Group Name] like @name
	and R.[Current Status] = 'Active'
group by
	SL.[Approval Rate],
	SL.[Applications],
	SL.[Eligible Cust],
	SL.[Auto Approved],
	SL.[Total Approved],
	SL.[Avg. Approval],
	SL.[Conversion Rate],
	FL.[Funded Amount],
	FL.[# Funded]
*/