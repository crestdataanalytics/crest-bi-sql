select
	A.ID as 'ApplicantID',
	Convert(date, L.FundedDate) as 'Funded Date',
	PP.[Period] 'Pay Frequency',
	LPS.ScheduleEntryDesc,
	LPS.DueDate,
	LPS.DueAmount
from 
	Applicants as A left join ApplicantJob as AJ on A.ID = AJ.ID
	left join PayPeriodTypes as PP on AJ.PayPeriodTypeID = PP.ID
	left join Loans as L on L.ApplicantID = A.ID
	left join LoanPaymentSchedule as LPS on LPS.LoanID = L.ID 
where
	L.FundedDate > '5-25-16' and convert(date,LPS.DueDate) < getdate() and convert(date,LPS.DueDate) > convert(date,L.FundedDate) and LPS.ScheduleEntryDesc = 'Loan Payment (Monthly)'
order by
	L.FundedDate asc

---------------------------------------------------
select
	A.ID as 'ApplicantID',
	Convert(date, L.FundedDate) as 'Funded Date',
	L.ApplicantID,
	PP.[Period] 'Pay Frequency',
	LPS.ScheduleEntryDesc,
	convert(date,LPS.DueDate) as 'DueDate',
	LPS.LoanID as 'LoanID',
	D.ID,
	D.Name
from 
	Applicants as A left join ApplicantJob as AJ on A.ID = AJ.ApplicantID
	left join PayPeriodTypes as PP on AJ.PayPeriodTypeID = PP.ID
	left join Loans as L on L.ApplicantID = A.ID
	left join Dealers as D on D.ID = L.DealerID
	left join
		(select
			LoanID,
			min(ID) as ID,
			min(DueDate) as DueDate,
			min(ScheduleEntryDesc) as ScheduleEntryDesc
			from LoanPaymentSchedule
			where ScheduleEntryDesc not in ('Initial Payment')
			group by LoanID
			) as LPS on LPS.LoanID = L.ID 
where
	L.FundedDate > '5-25-16' and convert(date,LPS.DueDate) > getdate() and convert(date,LPS.DueDate) > convert(date,L.FundedDate) and LPS.ScheduleEntryDesc = 'Loan Payment (Monthly)' and PP.[Period] not in ('Monthly')

---------------------------------------------------
select
	distinct A.ID as 'ApplicantID',
	Convert(date, L.FundedDate) as 'Funded Date',
	L.ApplicantID,
	LPS.ScheduleEntryDesc
from 
	Applicants as A left join Loans as L on L.ApplicantID = A.ID
	left join LoanPaymentSchedule as LPS on LPS.LoanID = L.ID 
where
	L.FundedDate > '5-25-16' and L.FundedDate < '7-13-16' and LPS.ScheduleEntryDesc = 'Loan Payment (Monthly)' and LPS.DueDate < getdate() and LPS.DueDate > L.FundedDate

-------------------------------------------------

select
	A.ID as 'ApplicantID',
	Convert(date, L.FundedDate) as 'Funded Date',
	L.StatusID,
	L.ApplicantID,
	PP.[Period]
from 
	Applicants as A left join Loans as L on L.ApplicantID = A.ID
	left join ApplicantJob as AJ on A.ID = AJ.ID 
	left join PayPeriodTypes as PP on AJ.PayPeriodTypeID = PP.ID
where
	StatusID not in (5,7,11,12,2,3,13,14,20,21,22,9)
	and PP.[Period]

--------------------------------------------
select
	A.ID as 'ApplicantID',
	Convert(date, L.FundedDate) as 'Funded Date',
	L.ApplicantID,
	L.StatusID,
	PP.[Period] 'Pay Frequency',
	LPS.ScheduleEntryDesc,
	convert(date,LPS.DueDate) as 'DueDate',
	LPS.LoanID as 'LoanID',
	D.ID,
	D.Name
from 
	Applicants as A left join ApplicantJob as AJ on A.ID = AJ.ApplicantID
	left join PayPeriodTypes as PP on AJ.PayPeriodTypeID = PP.ID
	left join Loans as L on L.ApplicantID = A.ID
	left join Dealers as D on D.ID = L.DealerID
	left join
		(select
			LoanID,
			min(ID) as ID,
			min(DueDate) as DueDate,
			min(ScheduleEntryDesc) as ScheduleEntryDesc
			from LoanPaymentSchedule
			where ScheduleEntryDesc not in ('Initial Payment')
			group by LoanID
			) as LPS on LPS.LoanID = L.ID 
where
	L.StatusID not in (5,7,11,12,2,3,13,14,20,21,22,9) and LPS.ScheduleEntryDesc = 'Loan Payment (Monthly)'
	and PP.[Period] in ('Bi-Weekly','Semi-Monthly','Weekly')