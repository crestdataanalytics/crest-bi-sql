select
	CONVERT(CHAR(7),LPS.Duedate,120),
	count(distinct LPS.LoanID),
	Sum(case when LPSDue.DueDate < LPS.DueDate then LPS.DueAmount end) as TotalDue
	--Sum(TPAT.TotalPaid) as PaidToDate,
	--sum(TMLP.TotalPaid) as PaidThisMonth
	from
		vw_LoanPaymentSchedule as LPS join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID
			Join 
			(SELECT 
				Distinct LH.LoanID,
				CONVERT(CHAR(7),LH.StartDate,120) as SD,
				--CONVERT(CHAR(7),LH.EndDate,120) as ED,
				min(LH.ENDDate) as EDT,
				LH.StatusID
			FROM
				LoanHistory as LH 
			WHERE
				LH.StatusID in (6) --and LH.LoanID = 571550
			GROUP BY
				LH.LoanID,
				CONVERT(CHAR(7),LH.StartDate,120),
				LH.StatusID) as LHH on LHH.LoanID = LPS.LoanID and (CONVERT(CHAR(7),LPS.Duedate,120) = LHH.SD) --OR CONVERT(CHAR(7),LPS.Duedate,120) = LHH.EDT)
		/*
		left outer join
        (SELECT
            LoanID,
            CONVERT(CHAR(7),paymentdate,120) as TPD,
            sum(totalpaid) over (order by paymentdate rows unbounded preceding) as totalPaid
        FROM
			(SELECT
				LP.LoanID,
				CONVERT(CHAR(7),paymentdate,120) as paymentdate,
				sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
			FROM
				LoanPayments as LP
				Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
			--where LP.loanid = 1266767
			GROUP BY
				Lp.LoanID,
				CONVERT(CHAR(7),paymentdate,120)) as TPAD) as TPAT on TPAT.TPD = LHH.SD --and TPAT.LoanID = DA.LoanID 
		left outer join
        (SELECT
            LP.LoanID,
            CONVERT(CHAR(7),paymentdate,120) as TPD,
            sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
        FROM
            LoanPayments as LP
			Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
       -- where LP.loanid = 1266767
        GROUP BY
            LP.LoanID,
            CONVERT(CHAR(7),paymentdate,120)) as TMLP on TMLP.TPD = LHH.SD --and TMLP.LoanID = DA.LoanID 
		*/
	--where lps.loanid = 1266767
	WHERE
		LPS.DueDate > '4-30-15'
	and LPS.DueDate < '9-1-16'
	--and LPS.LoanID = 1266767
	group by
	CONVERT(CHAR(7),LPS.Duedate,120)