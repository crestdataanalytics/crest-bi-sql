/*
SELECT
			--L.DealerID,
			concat(U.FirstName,' ',U.LastName) as RepName,
			coalesce(sum(case when datediff(mm,[FundedDate],getdate()) >= 12 then LP.TotalPaid-(1.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 11 then LP.TotalPaid-(1.21*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 10 then LP.TotalPaid-(1.15*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 9 then LP.TotalPaid-(1.09*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 8 then LP.TotalPaid-(1.02*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 7 then LP.TotalPaid-(0.95*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 6 then LP.TotalPaid-(0.88*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 5 then LP.TotalPaid-(0.79*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 4 then LP.TotalPaid-(0.71*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 3 then LP.TotalPaid-(0.62*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 2 then LP.TotalPaid-(0.44*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 1 then LP.TotalPaid-(0.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 0 then LP.TotalPaid-(0.06*[FundedAmount])
			else 0 end)/nullif(sum([FundedAmount]),0),0) as 'CCC$'
		FROM
			Loans as L join Dealers as D on L.DealerID = D.ID
			join Users as U on U.ID = D.DealerRepID
			JOIN
				(SELECT
					LoanID,
					sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						--sum(case when DeletedDate is null and returnDate is null and amount < 0 then amount else 0 end) as TotalPaid
				FROM
					LoanPayments
				WHERE
					PaymentDate < getdate()
					--and LoanID = 519707
				GROUP BY
					LoanID
				) as LP on LP.LoanID = L.ID
		WHERE --L.DealerID = 3626 and
			[FundedAmount] > 0 
			and [FundedDate] > dateadd(mm,-3,getdate())
			--and U.ID = 44697
		GROUP BY
			concat(U.FirstName,' ',U.LastName)
			--L.DealerID
		ORDER BY
			CCC$ asc
*/

SELECT
			--L.DealerID,
			CONVERT(CHAR(7),L.[FundedDate],120) as Vintage,
			--L.FundedAmount,
			--L.ID,
			--concat(U.FirstName,' ',U.LastName) as RepName,
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) >= 12 then LP.TotalPaid end) as '12',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 11 then LP.TotalPaid end) as '11',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 10 then LP.TotalPaid end) as '10',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 9 then LP.TotalPaid end) as '9',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 8 then LP.TotalPaid end) as '8',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 7 then LP.TotalPaid end) as '7',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 6 then LP.TotalPaid end) as '6',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 5 then LP.TotalPaid end) as '5',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 4 then LP.TotalPaid end) as '4',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 3 then LP.TotalPaid end) as '3',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 2 then LP.TotalPaid end) as '2',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 1 then LP.TotalPaid end) as '1',
			sum(case when datediff(mm,[FundedDate],LP.PaymentDate) = 0 then LP.TotalPaid end) as '0'
		FROM
			Loans as L join Dealers as D on L.DealerID = D.ID
			join Users as U on U.ID = D.DealerRepID
			JOIN
				(SELECT
					LoanID,
					PaymentDate,
					sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						--sum(case when DeletedDate is null and returnDate is null and amount < 0 then amount else 0 end) as TotalPaid
				FROM
					LoanPayments
				WHERE
					PaymentDate < getdate()
					--and LoanID = 519707
				GROUP BY
					LoanID,
					PaymentDate
				) as LP on LP.LoanID = L.ID
		WHERE --L.DealerID = 3626 and
			[FundedAmount] > 0 
			and CONVERT(CHAR(7),L.[FundedDate],120) = '2016-06'
			--and U.ID = 44697
		GROUP BY
			CONVERT(CHAR(7),L.[FundedDate],120),
			L.ID,
			L.FundedAmount
			--concat(U.FirstName,' ',U.LastName)
			--L.DealerID
		Having
			sum(case when datediff(dd,[FundedDate],LP.PaymentDate) <= 45 then LP.TotalPaid end) = 0
			and sum(case when datediff(dd,[FundedDate],LP.PaymentDate) between 45 and 60 then LP.TotalPaid end) > 0
			--sum(LP.TotalPaid) = 0
