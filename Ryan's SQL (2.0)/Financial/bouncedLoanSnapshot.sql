GO

/****** Object:  UserDefinedFunction [dbo].[udf_RevisedLoanSnapshot]    Script Date: 9/9/2016 12:24:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



DECLARE @Date DATE
SET @Date = '7-1-16';

WITH LoanIncome AS
	(
		SELECT 
			LoanID,
			MAX(PaymentDate) AS MostRecentPaymentDate,
			SUM(CASE WHEN ReturnDate IS NULL THEN RentAmount ELSE 0.00 END) AS TotalRent,
			SUM(RentAmount + FeeAmount) AS TotalPaid,
			SUM(CASE WHEN TypeID <> 9 THEN (RentAmount + FeeAmount) ELSE 0 END) AS TotalPaidToBalance
		FROM DealerManage.dbo.vw_LoanPayments
		WHERE
			ISNULL(DeletedDate, '12/31/2999') > @Date			
			AND
			ISNULL(ReturnDate, '12/31/2999') > @Date
			AND 
			PaymentDate <= @Date
		GROUP BY LoanID
	),
	LoanFees AS
	(
		SELECT 
			LoanID,
			SUM(FeeAmount) AS TotalFeesApplied,
			SUM(CASE WHEN WaivedDate IS NOT NULL AND WaivedDate > @Date THEN FeeAmount
					 WHEN WaivedDate IS NOT NULL AND WaivedDate <= @Date THEN 0
					 WHEN WaivedDate IS NULL THEN FeeAmount
					 END) AS TotalFeesActive			
		FROM DealerManage.dbo.LoanFees
		GROUP BY LoanID
	),
	ScheduledAmount AS
	(
		SELECT l.ApplicantID, lps.LoanID,
        SUM(CASE WHEN DueDate <= @Date THEN lps.DueAmount END) AS TotalDue
		FROM DealerManage.dbo.LoanPaymentSchedule AS lps
		INNER JOIN DealerManage.dbo.Loans AS l
		ON lps.LoanID = l.ID
		GROUP BY l.ApplicantID, lps.LoanID
	)
	SELECT 
		Loans.ID AS LoanID,
		Loans.FundedDate,
		DisplayID,
		IsDemoAccount,
		Loans.ApplicantID,
		CASE WHEN LoanHistory.StatusID IS NOT NULL THEN LoanHistory.StatusID
			 ELSE
				CASE WHEN Loans.StatusID = 5 AND Loans.PaidOffDate <= @Date THEN 5
					 WHEN Loans.StatusID IN (9,12,13,14,22) AND Loans.ChargedOffDate <= @Date THEN Loans.StatusID
					 WHEN Loans.StatusID = 7 AND Loans.DeniedDate <= @Date THEN 7
				ELSE NULL END
			 END AS StatusID,
		Loans.Amount AS FinancedAmount,
		CASE WHEN FundedDate IS NOT NULL AND FundedDate <= @Date 
			 THEN ISNULL(FundedAmount, Loans.Amount-ISNULL(DiscountCollected,Loans.Amount*0.06)-ISNULL(Loans.ApplicationFee, 40))
			 ELSE NULL END AS FundedAmount,
		TotalNote,
		TotalPaid,
		ScheduledAmount.TotalDue - LoanIncome.TotalRent AS PastDueAmount,
		--CASE WHEN loans.StatusID = 5
		--	 THEN 0.00
		--	 ELSE ScheduledAmount.TotalDue - LoanIncome.TotalRent
		--END AS PastDueAmount,
		TotalFeesActive AS TotalFees,
		CASE
			WHEN Loans.StatusID = 5 AND Loans.PaidOffDate <= @Date THEN 0
			ELSE
				CASE
					WHEN (DATEADD(dd, -60, BuyoutDate) > @Date AND Loans.BuyoutType = 90) THEN ISNULL(ThirtyDayPayoffAmount, Loans.[Amount])
					WHEN (DATEADD(dd, -30, BuyoutDate) > @Date AND Loans.BuyoutType = 90) THEN ISNULL(SixtyDayPayOffAmount, Loans.[Amount])
					WHEN (DATEADD(dd, 1, BuyoutDate) > @Date AND Loans.BuyoutType = 90) THEN ISNULL(NinetyDayPayoffAmount, Loans.[Amount])
					WHEN (DATEADD(dd, -90, BuyoutDate) > @Date AND Loans.BuyoutType = 120) THEN ISNULL(ThirtyDayPayoffAmount, Loans.[Amount])
					WHEN (DATEADD(dd, -60, BuyoutDate) > @Date AND Loans.BuyoutType = 120) THEN ISNULL(SixtyDayPayOffAmount, Loans.[Amount])
					WHEN (DATEADD(dd, -30, BuyoutDate) > @Date AND Loans.BuyoutType = 120) THEN ISNULL(NinetyDayPayoffAmount, Loans.[Amount])
					WHEN (DATEADD(dd, 1, BuyoutDate) > @Date AND Loans.BuyoutType = 120) THEN ISNULL(OneHundredTwentyDayPayoffAmount, Loans.[Amount])
					ELSE TotalNote
				END 
				+ ISNULL(TotalFeesActive, 0)
				- ISNULL(LoanIncome.TotalPaidToBalance,0)
		END AS Balance,
		DaysLate,
		CASE WHEN Loans.StatusID IN (5) AND Loans.PaidOffDate <= @Date THEN PaidOffDate ELSE NULL END AS PaidOffDate,
		CASE WHEN Loans.StatusID IN (9,12,13,14,20,22) AND Loans.ChargedOffDate <= @Date THEN ChargedOffDate ELSE NULL END AS ChargedOffDate,
		MostRecentPaymentDate
	FROM DealerManage.dbo.Loans
		JOIN DealerManage.dbo.Dealers
			ON Loans.DealerID = Dealers.ID
		LEFT JOIN LoanIncome
			ON Loans.ID = LoanIncome.LoanID
		LEFT JOIN LoanFees
			ON Loans.ID = LoanFees.LoanID
		LEFT JOIN ScheduledAmount
			ON ScheduledAmount.LoanID = Loans.ID
		LEFT JOIN DealerManage.dbo.LoanHistory
			ON Loans.ID = LoanHistory.LoanID
			AND LoanHistory.StartDate <= @Date AND ISNULL(LoanHistory.EndDate, GETDATE()) > @Date
	WHERE Loans.CreationDate <= @Date
		and CASE WHEN LoanHistory.StatusID IS NOT NULL THEN LoanHistory.StatusID
			 ELSE
				CASE WHEN Loans.StatusID = 5 AND Loans.PaidOffDate <= @Date THEN 5
					 WHEN Loans.StatusID IN (9,12,13,14,22) AND Loans.ChargedOffDate <= @Date THEN Loans.StatusID
					 WHEN Loans.StatusID = 7 AND Loans.DeniedDate <= @Date THEN 7
				ELSE NULL END
			 END in (6)
;

GO


