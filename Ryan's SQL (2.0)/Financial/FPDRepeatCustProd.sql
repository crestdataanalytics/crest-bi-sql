/****** SELECT Monthly Repeat Cust FPD Metrics  ******/
declare @date1 as date
declare @date2 as date

set @date1 = '7-31-16'
set @date2 = '9-1-16'
--set @date2 = dateadd(d,1,getdate())

SELECT
	count(FSP.FirstScheduledPaymentOutcomeID) as TotalLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
	count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
FROM
	(SELECT
		A.ID,
		count(case when LO.StatusID in (4,6) then 1 else null end) as LeasesFunded,
		count(case when LO.StatusID in (5) then 1 else null end) as LeasesPaid,
		count(case when LO.StatusID in (9) then 1 else null end) as LeasesRT,
		count(case when LO.StatusID in (12,13,14,20) then 1 else null end) as LeasesClosed
	FROM
		Loans as LO join Applicants as A on LO.ApplicantID = A.ID
	GROUP BY
		A.ID
	HAVING
		Count(case when LO.StatusID in (5,12,13,14) then 1 else null end) >= 1
		and Count(case when LO.StatusID in (4,6) then 1 else null end) = 1) as LO join Loans as L on L.ApplicantID = LO.ID
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	join LoanStatuses as LS on LS.ID = L.StatusID
WHERE
	L.FundedAmount > 0
	and FSP.IsCurrentOutcome = 1
	and L.FundedDate > @date1
	and L.FundedDate < @date2


/****** SELECT Monthly Repeat Cust Leases  ******/
SELECT
	cast(L.FundedDate as date) as FundedDate,
	LO.ID,
	LO.LeasesFunded,
	LO.LeasesPaid,
	LO.LeasesClosed,
	--count(FSP.FirstScheduledPaymentOutcomeID) as TotalLeases,
	--count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	--count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	--count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
	count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
FROM
	(SELECT
		A.ID,
		count(case when LO.StatusID in (4,6) then 1 else null end) as LeasesFunded,
		count(case when LO.StatusID in (5) then 1 else null end) as LeasesPaid,
		count(case when LO.StatusID in (9) then 1 else null end) as LeasesRT,
		count(case when LO.StatusID in (22) then 1 else null end) as LeasesST,
		count(case when LO.StatusID in (12,13,14,20) then 1 else null end) as LeasesClosed
	FROM
		Loans as LO join Applicants as A on LO.ApplicantID = A.ID
	GROUP BY
		A.ID
	HAVING
		Count(case when LO.StatusID in (5,9,12,13,14,20) then 1 else null end) >= 1
		and Count(case when LO.StatusID in (4,6) then 1 else null end) = 1) as LO join Loans as L on L.ApplicantID = LO.ID
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	join LoanStatuses as LS on LS.ID = L.StatusID
WHERE
	L.FundedAmount > 0
	and FSP.IsCurrentOutcome = 1
	and L.FundedDate > @date1
	and L.FundedDate < @date2
GROUP BY
	cast(L.FundedDate as date),
	LO.ID,
	LO.LeasesFunded,
	LO.LeasesPaid,
	LO.LeasesClosed


declare @date22 as date

set @date22 = dateadd(d,-7,getdate())

SELECT
	count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as AllLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end) as 'FPD$',
	count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end) as 'FPD+pending$'
FROM
	(SELECT
		A.ID,
		count(case when LO.StatusID in (4,6) then 1 else null end) as LeasesFunded,
		count(case when LO.StatusID in (5) then 1 else null end) as LeasesPaid,
		count(case when LO.StatusID in (9) then 1 else null end) as LeasesRT,
		count(case when LO.StatusID in (22) then 1 else null end) as LeasesST,
		count(case when LO.StatusID in (12,13,14,20) then 1 else null end) as LeasesClosed
	FROM
		Loans as LO join Applicants as A on LO.ApplicantID = A.ID
	GROUP BY
		A.ID
	HAVING
		Count(case when LO.StatusID in (5,9,12,13,14,20) then 1 else null end) >= 1
		and Count(case when LO.StatusID in (4,6) then 1 else null end) = 1) as LO join Loans as L on L.ApplicantID = LO.ID
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID 
WHERE
	L.FundedAmount > 0
	and FSP.IsCurrentOutcome = 1
	and L.FundedDate > @date1
	and L.FundedDate < @date2
	and FSP.FirstScheduledPaymentDueDate <= @date22