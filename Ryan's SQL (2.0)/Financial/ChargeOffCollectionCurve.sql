SELECT
	CONVERT(CHAR(7),FundedDate,120) as Vintage,
	coalesce(sum(case when StatusID = 12 then FundedAmount end)/Sum(FundedAmount),0) as COCurve
From
	Loans
WHERE
	datediff(mm,FundedDate,getdate()) <=12
GROUP BY
	CONVERT(CHAR(7),FundedDate,120)
ORDER BY
	CONVERT(CHAR(7),FundedDate,120) desc
