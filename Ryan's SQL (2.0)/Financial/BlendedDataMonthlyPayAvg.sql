SELECT
	(sum(case when M3 = M12 then M12 else 0 end)*1.0/sum(case when M3 = M12 then [FundedAmount] else 0 end)) 'M3 AVG',
	(sum(case when M3 <> M12 then M12 else 0 end)*1.0/sum(case when M3 <> M12 then [FundedAmount] else 0 end)) 'M4 AVG',
	(sum(case when M4 <> M12 then M12 else 0 end)*1.0/sum(case when M4 <> M12 then [FundedAmount] else 0 end)) 'M5 AVG',
	(sum(case when M5 <> M12 then M12 else 0 end)*1.0/sum(case when M5 <> M12 then [FundedAmount] else 0 end)) 'M6 AVG',
	(sum(case when M6 <> M12 then M12 else 0 end)*1.0/sum(case when M6 <> M12 then [FundedAmount] else 0 end)) 'M7 AVG',
	(sum(case when M7 <> M12 then M12 else 0 end)*1.0/sum(case when M7 <> M12 then [FundedAmount] else 0 end)) 'M8 AVG',
	(sum(case when M8 <> M12 then M12 else 0 end)*1.0/sum(case when M8 <> M12 then [FundedAmount] else 0 end)) 'M9 AVG',
	(sum(case when M9 <> M12 then M12 else 0 end)*1.0/sum(case when M9 <> M12 then [FundedAmount] else 0 end)) 'M10 AVG',
	(sum(case when M10 <> M12 then M12 else 0 end)*1.0/sum(case when M10 <> M12 then [FundedAmount] else 0 end)) 'M11 AVG',
	(sum(case when M11 <> M12 then M12 else 0 end)*1.0/sum(case when M11 <> M12 then [FundedAmount] else 0 end)) 'M12 AVG'
FROM
	FourMonthLoans7

