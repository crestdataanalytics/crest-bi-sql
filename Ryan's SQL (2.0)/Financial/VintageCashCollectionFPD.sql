		SELECT
			--L.DealerID,
			CONVERT(CHAR(7),FundedDate,120) as Vintage,
			coalesce(sum(case when datediff(mm,[FundedDate],getdate()) >= 12 then LP.TotalPaid-(1.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 11 then LP.TotalPaid-(1.21*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 10 then LP.TotalPaid-(1.15*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 9 then LP.TotalPaid-(1.09*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 8 then LP.TotalPaid-(1.02*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 7 then LP.TotalPaid-(0.95*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 6 then LP.TotalPaid-(0.88*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 5 then LP.TotalPaid-(0.79*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 4 then LP.TotalPaid-(0.71*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 3 then LP.TotalPaid-(0.62*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 2 then LP.TotalPaid-(0.44*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 1 then LP.TotalPaid-(0.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 0 then LP.TotalPaid-(0.06*[FundedAmount])
			else 0 end)/nullif(sum([FundedAmount]),0),0) as 'CCC$',
			1.25+coalesce(sum(case when datediff(mm,[FundedDate],getdate()) >= 12 then LP.TotalPaid-(1.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 11 then LP.TotalPaid-(1.21*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 10 then LP.TotalPaid-(1.15*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 9 then LP.TotalPaid-(1.09*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 8 then LP.TotalPaid-(1.02*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 7 then LP.TotalPaid-(0.95*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 6 then LP.TotalPaid-(0.88*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 5 then LP.TotalPaid-(0.79*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 4 then LP.TotalPaid-(0.71*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 3 then LP.TotalPaid-(0.62*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 2 then LP.TotalPaid-(0.44*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 1 then LP.TotalPaid-(0.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 0 then LP.TotalPaid-(0.06*[FundedAmount])
			else 0 end)/nullif(sum([FundedAmount]),0),0) as ExpectedMultple
		FROM
			Loans as L 
			left outer JOIN
				(SELECT
					LoanID,
					sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						--sum(case when DeletedDate is null and returnDate is null and amount < 0 then amount else 0 end) as TotalPaid
				FROM
					LoanPayments
				WHERE
					PaymentDate < getdate()
					--and LoanID = 519707
				GROUP BY
					LoanID
				) as LP on LP.LoanID = L.ID
		WHERE --L.DealerID = 3626 and
			[FundedAmount] > 0 
			and [FundedDate] > '12-31-14'
			and [FundedDate] < '9-1-16'
		GROUP BY
		CONVERT(CHAR(7),FundedDate,120)
			--,L.DealerID
		ORDER BY
			CONVERT(CHAR(7),FundedDate,120) desc

SELECT
				CONVERT(CHAR(7),L.FundedDate,120) as Vintage,
				count(FSP.FirstScheduledPaymentOutcomeID) as TotalLeases,
				count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
				count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
				count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
				count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
				count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
				count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
				count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
				count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
				sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
				count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
				sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
			FROM
				Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
				join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
				join LoanStatuses as LS on LS.ID = L.StatusID
			WHERE
				L.FundedAmount > 0
				and FSP.IsCurrentOutcome = 1
			and L.[FundedAmount] > 0 
			and [FundedDate] > '12-31-14'
			and [FundedDate] < '9-1-16'
			GROUP BY 
				CONVERT(CHAR(7),L.FundedDate,120)
			ORDER BY
				CONVERT(CHAR(7),L.FundedDate,120) desc