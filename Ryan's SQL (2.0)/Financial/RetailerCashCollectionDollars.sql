Select
	[Retailer ID],
	coalesce(sum(case when datediff(mm,[Funded Date],getdate()) >= 12 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],getdate()) = 11 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],getdate()) = 10 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],getdate()) = 9 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],getdate()) = 8 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],getdate()) = 7 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],getdate()) = 6 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],getdate()) = 5 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],getdate()) = 4 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],getdate()) = 3 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],getdate()) = 2 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],getdate()) = 1 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],getdate()) = 0 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.06
		else 0
		end)/nullif(count([Funded Amount]),0),0) as 'CC Avg',
	coalesce(sum(case when datediff(mm,[Funded Date],getdate()) >= 12 then [Total Paid]-(1.25*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 11 then [Total Paid]-(1.21*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 10 then [Total Paid]-(1.15*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 9 then [Total Paid]-(1.09*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 8 then [Total Paid]-(1.02*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 7 then [Total Paid]-(0.95*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 6 then [Total Paid]-(0.88*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 5 then [Total Paid]-(0.79*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 4 then [Total Paid]-(0.71*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 3 then [Total Paid]-(0.62*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 2 then [Total Paid]-(0.44*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 1 then [Total Paid]-(0.25*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 0 then [Total Paid]-(0.06*[Funded Amount])
		else 0
		end)/nullif(sum([Funded Amount]),0),0) as 'CC Sum'
From
	Leases
Where
	[Funded Amount] > 0 
	and [Funded Date] > '7-31-15'
	and [Retailer id] = 5269

Group by
	[Retailer ID]