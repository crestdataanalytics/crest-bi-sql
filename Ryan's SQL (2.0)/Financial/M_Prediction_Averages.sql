Select
Vintage,
Category,
sum(TotalPaid)/Sum(Funded) as CurrentMultiple,
(sum(case when PredictedPaid is not null then PredictedPaid else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMultiple,
(sum(case when PredictedPaid is not null then PredictedPaid*1.10 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMidPoint,
(sum(case when PredictedPaid is not null then PredictedPaid*1.20 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedUpperLimit,
sum(PredictedPaid)/(sum(TotalPaid)+sum(PredictedPaid)) as PercentPredictedAmount,
convert(numeric(14,2),count(PredictedPaid))/count(*) as PercentPredictedLeases
--Sum(Funded) as TotalFunded,
--Count(*) as TotalLeases
from
	(
	select 
		[Lease ID],
		TotalPaid,
		Funded,
		Vintage,
		Category,
		case when TotalPaid>PredictedPaid then TotalPaid else PredictedPaid end as PredictedPaid
		from
		(
			SELECT
				L.[Lease ID],
				LeaseQueueKey,
				[Approval Amount],
				R.Category,
				CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
				case 
					--// Dynamic
					when LeaseQueueKey in (4,6) and ((L.[Payments Returned] >= 3.5 and [Total Paid] < 240 and L.[Payments Cleared] >=18) or (L.[Payments Returned] >=3.5 and L.[Payments Cleared] >= 18) or (L.[Payments Returned] > 8.5)) then 
						(case when R.Category = 'Appliance/Electronics/Video' then (0.47*[Funded Amount])
							when R.Category = 'Furniture' then (0.57*[Funded Amount])
							when R.Category = 'Jewelry' then (0.53*[Funded Amount])
							when R.Category = 'Mattress' then (0.62*[Funded Amount])
							else (0.50*[Funded Amount]) end)
					when LeaseQueueKey in (4,6) and [Total Paid] < 10 then
						(0.015*[Funded Amount])
					when LeaseQueueKey in (4,6) then
					(case when R.Category = 'Appliance/Electronics/Video' then (1.66*[Funded Amount])
							when R.Category = 'Furniture' then (1.7*[Funded Amount])
							when R.Category = 'Jewelry' then (1.65*[Funded Amount])
							when R.Category = 'Mattress' then (1.74*[Funded Amount])
							else (1.68*[Funded Amount]) end)
					end as PredictedPaid,
					[Total Paid] as TotalPaid,
					[Funded Amount] as Funded,
					[Total Paid]/[Funded Amount] as CurrentMultiple
				--count(case when LeaseQueueKey in (4,6) then 1 else null end) as 'Current Leases'
			FROM
				Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
					join Customers as C on C.[Customer ID] = L.[Customer ID]
			WHERE
				[Funded Amount] > 0 and [Funded Date] > '1-1-15'
				and datediff(mm,[Funded Date],getdate()) between 4 and 16
				and LeaseQueueKey<>21
		) as ActiveLeases
	) as PredictedLeases

group by Vintage,Category
order by Vintage