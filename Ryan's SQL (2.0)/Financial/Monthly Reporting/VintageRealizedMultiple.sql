Select
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	Case when R.Category not in ('Furniture','Mattress','Jewelry','Tires/Rims','Car Audio') then 'Other' else R.Category end as Category,
	sum([Total Paid])/Sum([Funded Amount]) as Multiple
From
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
Where
	[Funded Date]>'4-30-14'
	and [Funded Date] < '9-1-15'
GROUP BY
	CONVERT(CHAR(7),[Funded Date],120),
	Case when R.Category not in ('Furniture','Mattress','Jewelry','Tires/Rims','Car Audio') then 'Other' else R.Category end
ORDER BY
	CONVERT(CHAR(7),[Funded Date],120) asc

Select
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	sum([Total Paid])/Sum([Funded Amount]) as Multiple
From
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
Where
	[Funded Date]>'4-30-14'
	and [Funded Date] < '9-1-15'
GROUP BY
	CONVERT(CHAR(7),[Funded Date],120)
ORDER BY
	CONVERT(CHAR(7),[Funded Date],120) asc