WITH dt AS (
SELECT DISTINCT DATEADD(mm,DATEDIFF(mm,0,[Funded Date]),0) AS [Funded Date]
  FROM leases
)
SELECT CONVERT(CHAR(7),dt.[funded Date],120) as Vintage
     , COUNT(DISTINCT r.[retailer Id]) AS RetailerCount,
	 , sum(L.[Funded Amount) as Funded
	 , CASE WHEN r.[State] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN r.[State] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN r.[State] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN r.[State] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT' END AS 'Region'
  FROM leases as s join retailers as r on s.[retailer ID] = r.[Retailer ID]
 INNER JOIN dt
    ON s.[Funded Date] >= DATEADD(MM, -2, dt.[Funded Date])
   AND s.[Funded Date] < DATEADD(MM, 1, dt.[Funded Date])
 GROUP BY CONVERT(CHAR(7),dt.[funded Date],120)
	 , CASE WHEN r.[State] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN r.[State] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN r.[State] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN r.[State] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT' END 
 ORDER BY 1, 2

SELECT
	Retailers.Region,
	sum([Funded Amount])
FROM
	Leases join Retailers on Leases.[Retailer ID] = Retailers.[Retailer ID]
WHERE
	CONVERT(CHAR(7),[funded Date],120) = '2016-08'
GROUP BY
	Retailers.Region