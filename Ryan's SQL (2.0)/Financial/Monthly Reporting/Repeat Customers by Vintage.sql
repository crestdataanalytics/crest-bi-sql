select 
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	count(distinct T.[Customer ID])
from (
		select [Customer ID],
			   [Funded Date],
			   [close date],
			   [LeaseQueuekey],	
			   count(*) over(partition by [Customer ID]) as dc
			   from [crest business intelligence].dbo.Leases
			   where [Funded Date] is not null and leasequeuekey not in (7,11)
			   ) as T
where dc > 1
group by
	CONVERT(CHAR(7),[Funded Date],120)
order by
	CONVERT(CHAR(7),[Funded Date],120)