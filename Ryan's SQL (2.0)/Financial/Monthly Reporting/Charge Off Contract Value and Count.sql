select 
sum([Total Note])-sum([Total Paid]) as 'Remaining Value',
count([Lease ID]) as 'Leases Charged Off'
from [Crest Business Intelligence].dbo.Leases
where leasequeuekey in(12)
and [Funded Date] < '9/1/2016'
and [close date] between '8/1/2016' and '8/31/2016'
