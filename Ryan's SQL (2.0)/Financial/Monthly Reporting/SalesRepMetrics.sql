/*SELECT
	U.ID,
	C.CategoryName,
	L.FundedAmount
FROM
	Users as U join Dealers as D on U.ID = D.DealerRepID
	join DealerCategories as DC on DC.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
	join Loans as L on L.DealerID = D.ID
WHERE
	datediff(mm,L.FundedDate,getdate())<=12
	and L.StatusID not in (21)
*/

SELECT
	CONVERT(CHAR(7),L.FundedDate,120) as Vintage,
	case when concat(U.FirstName,' ',U.LastName) in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') then 'ASM'
		when  U.EmailAddress like '%@crestfinancial.com' and concat(U.FirstName,' ',U.LastName) not in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') 
		then 'Inside Sales'
		else 'Outside Sales' end as SalesType,
	sum(L.FundedAmount) as Funded
FROM
	Dealers as D left outer join Users as U on D.DealerRepID = U.ID
	--join DealerCategories as DC on DC.DealerID = D.ID
	--join Categories as C on C.ID = DC.CategoryID
	left outer join Loans as L on L.DealerID = D.ID
	--left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	--left outer join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	--left outer join LoanStatuses as LS on LS.ID = L.StatusID
WHERE
	--L.StatusID not in (21) and
	L.FundedDate > '5-30-15'
	and D.IsDemoAccount not in (1)
	--and concat(U.FirstName,' ',U.LastName) not in ('Travis Huntsman','Kyle Fransen')
	--and U.EmailAddress like '%@crestfinancial.com'
GROUP BY
	CONVERT(CHAR(7),L.FundedDate,120),
	case when concat(U.FirstName,' ',U.LastName) in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') then 'ASM'
		when  U.EmailAddress like '%@crestfinancial.com' and concat(U.FirstName,' ',U.LastName) not in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') 
		then 'Inside Sales'
		else 'Outside Sales' end


select
	sum(FundedAmount)
From
	Loans
WHEre
	FundedDate > '7-31-16' and fundeddate<'9-1-16'