select
	count(distinct R.[Retailer ID])
from
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
where
	[Funded Date] between '5-1-2016' and '5-31-2016'