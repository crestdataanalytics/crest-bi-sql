select
	--CONVERT(CHAR(7),[funded Date],120) as Vintage,
	region,
	count([Lease ID]) as 'Leases'
from
	Leases as l join retailers as r on l.[retailer id] = r.[retailer id]
where
	[Funded Amount] > 0 and [Funded Date] between '8-1-16' and '8-31-16'
group by
	region
