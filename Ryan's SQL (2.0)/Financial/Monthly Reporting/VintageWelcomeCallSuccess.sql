SELECT
	CONVERT(CHAR(7),L.[FundedDate],120) as Vintage,
	count(case when WCS.ID in (3) then 1 else null end) as CountWelcomeCallSuccess
FROM
	Loans as L join LoanWelcomeCallStatus as LWCS on LWCS.LoanID = L.ID
	join WelcomeCallStatuses as WCS on WCS.ID = LWCS.WelcomeCallStatusID
WHERE
	L.[FundedDate] > '6-30-16'
GROUP BY	
	CONVERT(CHAR(7),L.[FundedDate],120)
