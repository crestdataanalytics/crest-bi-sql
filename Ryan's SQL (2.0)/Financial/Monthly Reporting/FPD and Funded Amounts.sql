select 
sum([Funded Amount]) as 'Funded Amount',
sum(case when [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end) as 'FPD Funded Amount'
from [Crest Business Intelligence].dbo.Leases
where 
	[Funded Date] between '12/1/2015' and '12/31/2015'
