SELECT
	CONVERT(CHAR(7),L.[FundedDate],120) as Vintage,
	Case when C.CategoryName not in ('Furniture','Mattress','Jewelry','Tires/Rims','Car Audio') then 'Other' else C.CategoryName end as Category,
	Avg(D.ApplicationFee) as InitialPayment,
	avg(FP.CrestPercentage) as Discount,
	avg(FP.TotalNotePercent) as Multiple
FROM
	Dealers as D join Loans as L on D.ID = L.DealerID
	join FinancePlans as FP on FP.ID = L.PlanID
	join DealerCategories as DC on DC.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
WHERE
	L.[FundedDate] > '7-31-16'
GROUP BY
	CONVERT(CHAR(7),L.[FundedDate],120),
	Case when C.CategoryName not in ('Furniture','Mattress','Jewelry','Tires/Rims','Car Audio') then 'Other' else C.CategoryName end
ORDER BY
	CONVERT(CHAR(7),L.[FundedDate],120) desc