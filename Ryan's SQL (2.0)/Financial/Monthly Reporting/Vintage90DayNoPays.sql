SELECT
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	L.ID,
	L.[FundedAmount],
	L.[StatusID],
	T.M1,
	T.M2,
	T.M3,
	Case when T.[CC1] >=0 then 'Yes' else 'No' end as 'CC1 on Track',
	Case when T.[CC2] >=0 then 'Yes' else 'No' end as 'CC2 on Track',
	Case when T.[CC3] >=0 then 'Yes' else 'No' end as 'CC3 on Track'
from vw_loans_reporting as L
	left outer join 
		(SELECT
			LPP.LoanID, 
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M1,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M2,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M3,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) - 0.25 as CC1,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.44 as CC2,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.62 as CC3
            FROM LoanPayments as LPP left join vw_loans_reporting as L on LPP.LoanID = L.ID
			GROUP BY 
			LPP.LoanID,
			L.[FundedAmount]
                ) as T on L.ID = T.LoanID
WHERE L.[FundedDate] between '5-1-16' and '5-30-16' --and [Total Paid] = 0
	and T.M3 = 0
Group by
	CONVERT(CHAR(7),[FundedDate],120),
	L.ID,
	L.[StatusID],
	L.[FundedAmount],
	l.[FundedDate],
	T.M1,
	T.M2,
	T.M3,
	T.CC1,
	T.CC2,
	T.CC3
order by
	CONVERT(CHAR(7),[FundedDate],120) asc