SELECT
	count([Agreement Number]) as 'Total',
	sum(case when leasequeuekey = 6 then 1 else 0 end) as 'Bounced'
FROM [Crest Business Intelligence].dbo.Leases as L
	join [Crest Business Intelligence].dbo.Retailers as R
	on L.[Retailer ID] = R.[Retailer ID]	
WHERE LeaseQueueKey in(4,5,6,16)
	and [Funded Date] < DATEADD(month, DATEDIFF(month, 0, getdate()), 0)
	and ([Close Date] > DATEADD(month, DATEDIFF(month, 0, getdate()), 0) or [Close Date] is null)