SELECT
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	Case when C.CategoryName not in ('Furniture','Mattress','Jewelry','Tires/Rims','Car Audio') then 'Other' else C.CategoryName end as Category,
	sum(L.[FundedAmount]) as Funded,
	sum(L.[ApprovalAmount]) as ApprovalAmount
from vw_loans_reporting as L join DealerCategories as DC on L.DealerID = DC.DealerID
	join Categories as C on C.ID = DC.CategoryID
	left outer join 
		(SELECT
			LPP.LoanID, 
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M1,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M2,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M3,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) - 0.25 as CC1,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.44 as CC2,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.62 as CC3
            FROM LoanPayments as LPP left join vw_loans_reporting as L on LPP.LoanID = L.ID
			GROUP BY 
			LPP.LoanID,
			L.[FundedAmount]
                ) as T on L.ID = T.LoanID
WHERE L.[FundedDate] between '12-31-13' and '5-30-16'
	and T.M3 = 0
Group by
	CONVERT(CHAR(7),[FundedDate],120),
	Case when C.CategoryName not in ('Furniture','Mattress','Jewelry','Tires/Rims','Car Audio') then 'Other' else C.CategoryName end
order by
	CONVERT(CHAR(7),[FundedDate],120) asc


------------------------------------------------------------------------------------------------------------------------------


SELECT
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	sum(case when T.M3 = 0 then L.[FundedAmount] else 0 end)*1.0/sum(L.[FundedAmount]) as Funded
	--sum(L.[ApprovalAmount]) as ApprovalAmount
from vw_loans_reporting as L join DealerCategories as DC on L.DealerID = DC.DealerID
	join Categories as C on C.ID = DC.CategoryID
	left outer join 
		(SELECT
			LPP.LoanID, 
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M1,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M2,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M3,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) - 0.25 as CC1,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.44 as CC2,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.62 as CC3
            FROM LoanPayments as LPP left join vw_loans_reporting as L on LPP.LoanID = L.ID
			GROUP BY 
			LPP.LoanID,
			L.[FundedAmount]
                ) as T on L.ID = T.LoanID
WHERE L.[FundedDate] between '12-31-14' and '5-30-16'
Group by
	CONVERT(CHAR(7),[FundedDate],120)
order by
	CONVERT(CHAR(7),[FundedDate],120) asc