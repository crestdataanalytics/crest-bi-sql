           Select  
			   --lp.LoanID,
               CONVERT(CHAR(7),LP.paymentdate,120) as PD,
               TMLP.TotalPaid as CollectedThisMonth,
               DA.totaldue
			   --TPAT.TotalPaid
           From
               loanPayments as LP 
                  left outer join
                       (SELECT
                           --LoanID,
                           CONVERT(CHAR(7),paymentdate,120) as TPD,
                           sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
                       FROM
                           LoanPayments
                       --WHERE loanid = 1266767
                       GROUP BY
                           --LoanID,
                           CONVERT(CHAR(7),paymentdate,120)) as TMLP on TMLP.TPD = CONVERT(CHAR(7),LP.paymentdate,120) --TMLP.LoanID = LP.LoanID        
                   left outer join
                       (SELECT
                               lps.LoanID,
                               CONVERT(CHAR(7),LPS.Duedate,120) as duedate,
                               sum(lps.dueamount) over (order by lps.duedate rows unbounded preceding) as totaldue
                           from
                           (
                           select
                               lps.loanID,
                               CONVERT(CHAR(7),lps.duedate,120) as duedate,
                               sum(lps.Dueamount) as dueamount
                            from
                               vw_LoanPaymentSchedule as LPS --join vw_loanPaymentSchedule as LPSDue on LPSDue.LoanID = LPS.LoanID
                                   --Join LoanHistory as LH on LH.LoanID = LPS.LoanID and (LPS.DueDate between LH.StartDate and LH.EndDate and LH.StatusID in (6,12))
                           --where lps.loanid = 1266767
                           group by
                               lps.loanID,
                               CONVERT(CHAR(7),duedate,120)
                           )as LPS) as DA on DA.duedate = CONVERT(CHAR(7),LP.paymentdate,120)    
                    --Left outer join
                    --   (SELECT
                    --       LoanID,
                    --       CONVERT(CHAR(7),paymentdate,120) as TPD,
                    --       sum(totalpaid) over (order by paymentdate rows unbounded preceding) as totalPaid
                    --   FROM
                    --   (SELECT
                    --       LoanID,
                    --       CONVERT(CHAR(7),paymentdate,120) as paymentdate,
                    --       sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
                    --   FROM
                    --       LoanPayments
                    --   --where loanid = 1266767
                    --   GROUP BY
                    --       LoanID,
                    --       CONVERT(CHAR(7),paymentdate,120)) as TPAD) as TPAT on TPAT.TPD = CONVERT(CHAR(7),LP.paymentdate,120)
           where --lp.loanid = 1266767 and
               lp.PaymentDate > '12-31-15'
               and DA.LoanID is not null
                and lp.id in 
                (
                    select min(lp.id)
                    From
                        loanPayments as LP 
                    Join LoanHistory as LH on LH.LoanID = LP.LoanID and (LP.PaymentDate between LH.StartDate and LH.EndDate and LH.StatusID in (6))
                    group by lp.loanid, CONVERT(CHAR(7),LP.paymentdate,120) 
                )
           --group by
               --lp.LoanID,
                --CONVERT(CHAR(7),LP.paymentdate,120)
                --TMLP.TotalPaid,
                --DA.totaldue,
                --TPAT.TotalPaid
           Order by
               CONVERT(CHAR(7),LP.paymentdate,120) desc