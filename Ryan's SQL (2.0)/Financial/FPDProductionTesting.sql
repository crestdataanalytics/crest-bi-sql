SELECT
	count(L.ID),
	count(FPD.LoanID),
	count(LPS.LoanID),
	count(FPD.LoanID)*1.0/count(LPS.LoanID)
FROM
	Loans as L left outer join vw_FirstPaymentDefaultedLoans as FPD on FPD.LoanID = L.ID and FPD.ReceivedPartialPayment = 0
	left outer join 
		(SELECT
			LoanID,
			min(DueDate) as DueDate
		FROM
			vw_loanPaymentSchedule
		GROUP BY
			LoanID) as LPS on LPS.LoanID = L.ID and LPS.DueDate < getdate()
WHERE
	L.FundedDate >= '8-1-16'

select * from vw_Firstpaymentdefaultedloans
order by missedduedate desc

select
	L.FundedDate,
	LPS.ScheduleEntryDesc,
	L.ID as LID,
	LPS.LoanID as LPSID,
	LP.LoanID as LPID,
	LP.Returndate as LPRD,
	LP.ChargebackDate as LPCO,
	LP.AppliedTodate as LPAD,
	min(LPS.DueDate) as DueDate
from Loans as L left outer join vw_scheduledLoanPayments as LPS on L.ID = LPS.LoanID
	left outer join vw_loanPayments as LP on LP.ScheduledLoanPaymentID = LPS.ID
where
	L.FundedDate >= '8-1-16'
	and LPS.ScheduleEntryDesc not in ('InitialPayment')
	and LPS.DueDate < getdate()
GROUP BY
	L.FundedDate,
	LPS.ScheduleEntryDesc,
	L.ID,
	LPS.LoanID,
	LP.LoanID,
	LP.Returndate,
	LP.ChargebackDate,
	LP.AppliedTodate
ORDER BY
	min(LPS.DueDate) desc
	

