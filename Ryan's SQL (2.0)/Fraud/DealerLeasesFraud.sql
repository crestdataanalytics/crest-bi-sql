SELECT
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	Loans.ID,
	cast(Loans.FundedDate as date) as FundingDate,
	sum([FundedAmount]) as Funded,
	cast([FundedAmount]/[ApprovalAmount] as decimal(18,2)) as Utilization
FROM
	Loans join Dealers as D on Loans.DealerID = D.ID
WHERE
	--[DealerID] = 11368
	D.Name like '%american trade%'
	and FundedAmount > 0
	and FundedDate > '8-1-16'
GROUP BY
	CONVERT(CHAR(7),[FundedDate],120),
	Loans.Id,
	Loans.FundedDate,
	[FundedAmount]/[ApprovalAmount]
ORDER BY
	CONVERT(CHAR(7),[FundedDate],120)