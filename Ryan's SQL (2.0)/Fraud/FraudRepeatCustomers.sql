SELECT
	distinct L.ApplicantID,
	cast(LL.maxFunded as date) as LastFundedDate,
	count(L.ID) as Leases
FROM
	Loans as L
	join
	(SELECT
		ApplicantID,
		max(FundedDate) as maxFunded
	FROM
		Loans
	GROUP BY
		ApplicantID) as LL on LL.ApplicantID = L.ApplicantID
WHERE
	datediff(dd,[FundedDate],[PaidOffDate]) < 10
	and datediff(dd,[FundedDate],getdate()) < 120
GROUP BY
	L.ApplicantID,
	LL.maxFunded
HAVING
	count(L.ID) > 1
ORDER BY
	Leases desc