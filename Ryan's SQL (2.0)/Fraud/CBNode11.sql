SELECT
	l.[submitted date] as appDate,
	l.[lease id] as leaseID,
	l.[customer id] as custID,
	l.[leasequeuekey],
	cast(c.Amount as decimal(18,2)) as CBApproval,
	c.Node as CBNode,
	c.score as CBScore
from 
	Leases as l right outer join
	(select
		DI.SubjectID as 'CustomerID',
		max(case when dv.[variableID] = 45993 then dv.[variablevalue] end) as 'Result',
		max(case when dv.[variableID] = 45994 then dv.[variablevalue] end) as 'Node',
		max(case when dv.[variableID] = 45995 then dv.[variablevalue] end) as 'Factor',
		max(case when dv.[variableID] = 45996 then dv.[variablevalue] end) as 'Score',
		max(case when dv.[variableID] = 45997 then dv.[variablevalue] end) as 'Amount'
		from ProviderAnalysis.dbo.DocumentVariables as DV
			join ProviderAnalysis.dbo.DocumentInfos as DI on DV.documentid = DI.DocumentID
			join ProviderAnalysis.dbo.Providers as P on P.ProviderID = DI.ProviderID
			join ProviderAnalysis.dbo.Variables as V on V.variableid = DV.variableID
		where p.providerid = 14
		group by di.subjectid) as c on c.customerID = L.[Customer ID]
	join retailers as r on r.[retailer id] = l.[retailer id]
	join Customers as cu on cu.[Customer ID] = l.[Customer ID]
where [submitted date] >= '6-29-16'
	and C.Node = 11