select
	l.applicantID,
	cast(l.fundeddate as date) as fundedDate,
	l.fundedamount,
	l.statusid,
	l.pastdueamount,
	l.balance,
	uat.ipaddress
	--ba.routingnumber
	--ba.accountnumber
from
	loans as l join userapplicant as ua on ua.applicantid = l.applicantid
	join users as u on ua.userid = u.id
	join useraudittrail as uat on uat.userid = u.id
	--join applicantbankaccounts as aba on aba.applicantid = l.applicantid
	--join bankaccounts as ba on ba.id = aba.bankaccountid
where
	l.fundedamount > 0 and
	(uat.ipaddress like '75.215%' or uat.ipaddress like '75.217%')
	--and ba.routingnumber = '121000358' and ba.accountnumber like '3250%'

