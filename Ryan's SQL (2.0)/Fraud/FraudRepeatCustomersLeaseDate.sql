SELECT
	L.ApplicantID,
	AJ.MonthlyIncome,
	L.ID,
	L.StatusID,
	L.FundedAmount,
	cast(L.Amount/L.ApprovalAmount as decimal(18,2)) as Utilization,
	cast(L.FundedDate as date) as FundingDate,
	cast(L.PaidOffDate as date) as PaidDate,
	LL.LC
FROM
	Loans as L join ApplicantJob as AJ on AJ.ApplicantID = L.ApplicantID
		join Dealers as D on D.ID = L.DealerID
	left outer join
		(select distinct L.ApplicantID, count(L.ID) as LC
	from Loans as L
	where L.StatusID not in (7,11) and datediff(dd,[FundedDate],[PaidOffDate]) < 30
	group by L.ApplicantID) as LL on LL.ApplicantID = L.ApplicantID
WHERE
	datediff(dd,[FundedDate],getdate()) < 120
	and LL.LC > 2
	and D.Name not like '%demo%'
GROUP BY
	L.ApplicantID,
	AJ.MonthlyIncome,
	L.ID,
	L.StatusID,
	L.FundedAmount,
	L.Amount/L.ApprovalAmount,
	L.FundedDate,
	L.PaidOffDate,
	LL.LC
ORDER BY
	L.ApplicantID desc