SELECT
	distinct d.ID,
	d.Name,
	DG.DealerGroupName,
	d.riskScore,
	FM.Category,
	case when FM.Category = 'Superstar' and d.riskScore <= 4.1 then '3'
		when FM.Category = 'Superstar' and d.riskScore <= 6.1 then '2'
		when FM.Category = 'Superstar' and d.riskScore <= 8.1 then '1'
		when FM.Category = 'Allstar' and d.riskscore <= 2.1 then '3'
		when FM.Category = 'Allstar' and d.riskscore <= 4.1 then '2'
		when FM.Category = 'Allstar' and d.riskscore <= 6.1 then '1'
		else '0'
		end as RiskStatus
FROM
	dealers as d left outer join dealerGroups as DG on d.dealerGroupID = DG.ID
	left outer join FundingMatrixCategory as FM on d.fundingMatrixCategoryID = FM.ID
WHERE
	fundingMatrixCategoryID is not null
	and d.statusid = 1
	and case when FM.Category = 'Superstar' and d.riskScore <= 4.1 then '3'
		when FM.Category = 'Superstar' and d.riskScore <= 6.1 then '2'
		when FM.Category = 'Superstar' and d.riskScore <= 8.1 then '1'
		when FM.Category = 'Allstar' and d.riskscore <= 2.1 then '3'
		when FM.Category = 'Allstar' and d.riskscore <= 4.1 then '2'
		when FM.Category = 'Allstar' and d.riskscore <= 6.1 then '1'
		else '0'
		end is not null
ORDER BY
	RiskStatus asc,
	d.riskScore asc