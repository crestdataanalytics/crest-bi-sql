SELECT
	ID,
	Name,
	RiskScore,
	ApplicationFee
FROM
	Dealers
WHERE
	StatusID = 1
	and ApplicationFee is null