SELECT
	D.ID,
	D.Name,
	A.City,
	A.StateID,
	D.RiskScore as BIrank,
	count(case when L.FundedDate between dateadd(mm,-2,getdate()) and dateadd(mm,-1,getdate()) then 1 else null end) as PriorMonthVolume,
	count(case when datediff(mm,L.FundedDate,getdate())=0 then 1 else null end) as CurrentMonthVolume,
	sum(case when L.FundedDate between dateadd(mm,-2,getdate()) and dateadd(mm,-1,getdate()) then L.FundedAmount else 0 end) as 'PriorMonth$',
	sum(case when  datediff(mm,L.FundedDate,getdate())=0 then L.Amount else 0 end) as 'CurrentMonth$',
	sum(case when L.FundedDate between dateadd(mm,-3,getdate()) and dateadd(mm,0,getdate()) then L.FundedAmount else 0 end) as '3Month$',
	FPD.[%Read],
	FPD.[FPD$],
	FPD.[FPD+Pending$],
	CC.[CCC$],
	sum(case when datediff(mm,L.FundedDate,getdate()) < 3 and L.ApprovalAmount is not null then L.FundedAmount else 0 end)*1.0/
		nullif(sum(case when datediff(mm,L.FundedDate,getdate()) < 3 and L.ApprovalAmount is not null then L.ApprovalAmount else 0 end),0) as Utilization,
	Count(case when L.StatusID in (4,5,6,9,12,13,14,20,21,22) then 1 else null end)*1.0/nullif(Count(case when L.StatusID in (3,4,5,6,9,11,12,13,14,20,21,22) then 1 else null end),0) as Conversion
FROM
	Dealers as D left outer join Loans as L on D.ID = L.DealerID
	left outer join Addresses as A on A.ID = D.AddressID
	left outer join
		(
		/****** SELECT Monthly FPD Metrics  ******/
		--declare @date1 as date
		--declare @date2 as date

		--set @date1 = dateadd(m,-3,getdate())
		--set @date2 = dateadd(d,-7,getdate())
		--set @date2 = dateadd(d,1,getdate())

		SELECT
			L.DealerID,
			count(FSP.FirstScheduledPaymentOutcomeID) as TotalLeases,
			count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
			count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
			count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
			count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
			count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
			count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
			count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
			count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
			sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
			count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
			sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
		FROM
			Loans as L left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
			join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
			join LoanStatuses as LS on LS.ID = L.StatusID
		WHERE
			L.FundedAmount > 0
			and FSP.IsCurrentOutcome = 1
			and L.FundedDate > dateadd(mm,-3,getdate())
		GROUP BY
			L.DealerID
		) as FPD on FPD.DealerID = D.ID
		LEFT OUTER JOIN
		(
		SELECT
			L.DealerID,
			coalesce(sum(case when datediff(mm,[FundedDate],getdate()) >= 12 then LP.TotalPaid-(1.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 11 then LP.TotalPaid-(1.21*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 10 then LP.TotalPaid-(1.15*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 9 then LP.TotalPaid-(1.09*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 8 then LP.TotalPaid-(1.02*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 7 then LP.TotalPaid-(0.95*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 6 then LP.TotalPaid-(0.88*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 5 then LP.TotalPaid-(0.79*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 4 then LP.TotalPaid-(0.71*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 3 then LP.TotalPaid-(0.62*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 2 then LP.TotalPaid-(0.44*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 1 then LP.TotalPaid-(0.25*[FundedAmount])
			when datediff(mm,[FundedDate],getdate()) = 0 then LP.TotalPaid-(0.06*[FundedAmount])
			else 0 end)/nullif(sum([FundedAmount]),0),0) as 'CCC$'
		FROM
			Loans as L 
			JOIN
				(SELECT
					LoanID,
					sum(case when DeletedDate is null then amount else 0 end)-sum(case when DeletedDate is null and returnDate is not null then amount else 0 end) as TotalPaid
						--sum(case when DeletedDate is null and returnDate is null and amount < 0 then amount else 0 end) as TotalPaid
				FROM
					LoanPayments
				WHERE
					PaymentDate < getdate()
					--and LoanID = 519707
				GROUP BY
					LoanID
				) as LP on LP.LoanID = L.ID
		WHERE --L.DealerID = 3626 and
			[FundedAmount] > 0 
			and [FundedDate] > dateadd(mm,-3,getdate())
		GROUP BY
			L.DealerID
		) as CC on CC.DealerID = D.ID
WHERE
	D.StatusID = 1
	and name not like '%test%'
	and name not like '%demo%'
GROUP BY
	D.ID,
	D.Name,
	A.City,
	A.StateID,
	D.RiskScore,
	FPD.[%Read],
	FPD.[FPD$],
	FPD.[FPD+Pending$],
	CC.[CCC$]
--HAVING
--	sum(case when L.FundedDate between datediff(m,-3,getdate()) and datediff(m,0,getdate()) then L.FundedAmount else 0 end) > 0
ORDER BY
	sum(case when L.FundedDate between dateadd(m,-3,getdate()) and dateadd(m,0,getdate()) then L.FundedAmount else 0 end) desc