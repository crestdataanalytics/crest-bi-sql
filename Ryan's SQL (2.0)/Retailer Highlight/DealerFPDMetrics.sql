/****** SELECT Monthly FPD Metrics  ******/
declare @date1 as date
declare @date2 as date
declare @name as varchar(25)

set @date1 = '12-31-15'
set @date2 = getdate()
set @name = '%Bennettsville%'
--set @date2 = dateadd(d,1,getdate())

SELECT
	--CONVERT(CHAR(7),L.[FundedDate],120) as Vintage,
	D.Name,
	D.ID,
	D.RiskScore as BIrank,
	C.CategoryName,
	sum(L.FundedAmount) as Funded$,
	sum(L.FundedAmount)*1.0/Sum(L.ApprovalAmount) as Utilization,
	count(FSP.FirstScheduledPaymentOutcomeID) as TotalLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5) then 1 else null end),0) as 'FPD%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end),0) as 'FPD$',
	count(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then 1 else null end)*1.0/nullif(count(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end),0) as 'FPD+pending%',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end)*1.0/nullif(sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end),0) as 'FPD+pending$'
FROM
	Loans as L
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	join LoanStatuses as LS on LS.ID = L.StatusID
	join Dealers as D on D.ID = L.DealerID
	join DealerCategories as DC on DC.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
	join DealerGroups as DG on DG.ID = D.DealerGroupID
WHERE
	L.FundedAmount > 0
	and L.StatusID not in (21)
	and FSP.IsCurrentOutcome = 1
	and L.FundedDate > @date1
	and L.FundedDate < @date2
	--and D.RiskScore <=3
	and D.StatusID = 1
	and DG.[DealerGroupName] like @name
	--and datediff(mm,D.CreationDate,getdate()) > 6)
GROUP BY
	--CONVERT(CHAR(7),L.[FundedDate],120),
	D.Name,
	D.ID,
	D.riskscore,
	C.CategoryName