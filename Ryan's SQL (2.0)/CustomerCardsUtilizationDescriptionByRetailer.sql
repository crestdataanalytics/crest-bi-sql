SELECT
	L.ApplicantID,
	cast(L.FundedDate as date) as FundingDate,
	L.Amount,
	L.ApprovalAmount,
	cast((L.Amount*1.0)/L.ApprovalAmount as decimal(18,2)) as Utilization,
	LI.[Description],
	count(distinct LP.PaymentMethodLastFour) as CreditCards,
	count(distinct case when LP.ID is not null then LP.ReturnDate else null end) as PaymentsReturned,
	count(distinct N.ID) as Notes
FROM Loans as L left outer join LoanPayments as LP on L.ID = LP.LoanID
	left outer join LoanItems as LI on LI.LoanID = L.ID
	left outer join Notes as N on N.LoanID = L.ID
WHERE
	L.FundedAmount > 0
	and L.DealerID = 9281

	--and LP.TypeID in (7,9,8,2)
GROUP BY
	L.ApplicantID,
	L.DealerID,
	cast(L.FundedDate as date),
	L.Amount,
	L.ApprovalAmount,
	(L.Amount*1.0)/L.ApprovalAmount,
	LI.[Description]
ORDER BY
	cast(L.FundedDate as date) asc



	
