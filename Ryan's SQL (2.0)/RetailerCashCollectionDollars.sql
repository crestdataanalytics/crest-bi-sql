Select
	[Retailer ID],
	sum([Funded Amount]) as Funded,
	sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Total Paid] else 0 end)/nullif(
		sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Funded Amount] else 0 end),0) as Multiple,
	coalesce(sum(case when datediff(mm,[Funded Date],getdate()) = 12 then [Total Paid]-(1.25*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 11 then [Total Paid]-(1.21*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 10 then [Total Paid]-(1.15*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 9 then [Total Paid]-(1.09*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 8 then [Total Paid]-(1.02*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 7 then [Total Paid]-(0.95*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 6 then [Total Paid]-(0.88*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 5 then [Total Paid]-(0.79*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 4 then [Total Paid]-(0.71*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 3 then [Total Paid]-(0.62*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 2 then [Total Paid]-(0.44*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 1 then [Total Paid]-(0.25*[Funded Amount])
		when datediff(mm,[Funded Date],getdate()) = 0 then [Total Paid]-(0.06*[Funded Amount])
		else 0
		end)/nullif(sum(case when datediff(mm,[Funded Date],getdate()) <= 12 then [Funded Amount] else 0 end),0),0) as CashCollectionCurve
From
	Leases
Where
	[Funded Amount] > 0 
	and [Funded Date] > '7-31-15'
Group by
	[Retailer ID]