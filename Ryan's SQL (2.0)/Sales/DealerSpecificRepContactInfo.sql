SELECT
	D.ID as DealerID,
	D.Name as DealerName,
	A.City,
	A.StateID,
	concat(U.FirstName,' ',U.LastName) as RepName,
	U.EmailAddress as RepEmail,
	U.PhoneNumber as RepPhone#
FROM
	Dealers as D join Users as U on D.DealerRepID = U.ID
	join Addresses as A on A.ID = D.AddressID
WHERE
	D.Name like 'Orleans Furniture & More' or D.Name LIKE 
'louisiana furniture outlet' or D.Name LIKE 
'WCC Furniture' or D.Name LIKE 
'Basic Your Best Buy' or D.Name LIKE 
'br furniture outlet (Louisiana)' or D.Name LIKE 
'Mattress & Furniture Outlet' or D.Name LIKE 
'LOPEZ FURNITURE' or D.Name LIKE 
'Empire Furniture (LA)' or D.Name LIKE 
'Grizzaffi Furniture' or D.Name LIKE 
'buyandsave furniture' or D.Name LIKE 
'Hamilton Enterprises' or D.Name LIKE 
'Necessity Furniture Warehouse' or D.Name LIKE 
'Furniture Liquidators' or D.Name LIKE 
'Diversified Customs' or D.Name LIKE 
'Dynamic Customs' or D.Name LIKE 
'MR GOLDMAN SUPERSTORE' or D.Name LIKE 
'Bewley%s Furniture Center' or D.Name LIKE 
'INVENTORY LIQUIDATORS' or D.Name LIKE 
'New Look Furniture' or D.Name LIKE 
'Bills House of Hubcaps' or D.Name LIKE 
'Hometown Furniture' or D.Name LIKE 
'Goldman & Sons Jewelers' or D.Name LIKE 
'Gilleon%s Home Furnishing Inc.' or D.Name LIKE 
'Mobile Tint and Audio (Kenner)' or D.Name LIKE 
'D and S Custom Automotive' or D.Name LIKE 
'Gold Center & Karats Jewelers' or D.Name LIKE 
'Western Auto' or D.Name LIKE 
'D & D%s Custom Rims & Used Tires LLC' or D.Name LIKE 
'Sweet Dreams Mattress And Furniture' or D.Name LIKE 
'The Furniture Store LLC' or D.Name LIKE 
'Mobile Tint and Audio (Metairie)' or D.Name LIKE 
'miriam%s furniture' or D.Name LIKE 
'MR GOLDMAN AND SONS' or D.Name LIKE 
'Rush Center' or D.Name LIKE 
'Clearance Center' or D.Name LIKE 
'Extreme Car Audio LLC' or D.Name LIKE 
'Cellfix (Gretna)' or D.Name LIKE 
'Haydel%s Furniture & Appliances' or D.Name LIKE 
'Fix-A-Fone (New Orleans)' or D.Name LIKE 
'Sherman%s Furniture' or D.Name LIKE 
'FISCHER%S JEWELRY' or D.Name LIKE 
'CII%S BLESSINGS STORE' or D.Name LIKE 
'IN & Out Furniture' or D.Name LIKE 
'Bauer Hardware & Furniture Co Inc.' or D.Name LIKE 
'Sol%s Jewelers' or D.Name LIKE 
'Sol%s Jewelry (Kenner)' or D.Name LIKE 
'Mobile Tint and Audio (Carollton)' or D.Name LIKE 
'Furniture Expo' or D.Name LIKE 
'EBONY GOLD AND DIAMONDS' or D.Name LIKE 
'Mattress Joe + Furniture' or D.Name LIKE 
'Southern Tint' or D.Name LIKE 
'K & R FURNITURE (Leesville)' or D.Name LIKE 
'Watson%s Furniture' or D.Name LIKE 
'Buy & Save' or D.Name LIKE 
'Mattress Joe 2' or D.Name LIKE 
'RAINBOW CHEVROLET' or D.Name LIKE 
'Mobile Tint and Audio (Chalmette)' or D.Name LIKE 
'Fix A Fone (Broad)' or D.Name LIKE 
'Trademark Home' or D.Name LIKE 
'Olinde Hardware & Supply' or D.Name LIKE 
'Fix-A-Fone (Gretna)' or D.Name LIKE 
'BJ%s Warehouse' or D.Name LIKE 
'Shannon%s Home Furnishings' or D.Name LIKE 
'Rim Pros of Baton Rouge' or D.Name LIKE 
'Sol%s Jewelry' or D.Name LIKE 
'Brown%s Jewelers' or D.Name LIKE 
'Young%s Home Appliance' or D.Name LIKE 
'St.Germain%s Furniture' or D.Name LIKE 
'Let%s Do Furniture (Marrero)' or D.Name LIKE 
'Perfect Dreamer SleepShop & More LLC' or D.Name LIKE 
'Courtesy Discount Furniture Appliances and Mobile Homes' or D.Name LIKE 
'J&M Furniture' or D.Name LIKE 
'VIP AUTOMOTIVES' or D.Name LIKE 
'Bayou Bedding of br' or D.Name LIKE 
'PRECISION CAR AUDIO AND ACCESSORIES' or D.Name LIKE 
'Rim Pros of Metairie' or D.Name LIKE 
'Superior Mattress LLC'
and A.StateID = 'LA'