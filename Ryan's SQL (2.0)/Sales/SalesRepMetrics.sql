/*SELECT
	U.ID,
	C.CategoryName,
	L.FundedAmount
FROM
	Users as U join Dealers as D on U.ID = D.DealerRepID
	join DealerCategories as DC on DC.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
	join Loans as L on L.DealerID = D.ID
WHERE
	datediff(mm,L.FundedDate,getdate())<=12
	and L.StatusID not in (21)
*/


SELECT
	concat(U.FirstName,' ',U.LastName) as RepName,
	U.ID,
	U.EmailAddress,
	--D.Name,
	case when concat(U.FirstName,' ',U.LastName) in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') then 'ASM'
		when  U.EmailAddress like '%@crestfinancial.com' and concat(U.FirstName,' ',U.LastName) not in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') 
		then 'Inside Sales'
		else 'Outside Sales' end as SalesType,
	C.CategoryName,
	count(case when datediff(mm,L.FundedDate,getdate())<=12 then L.FundedAmount else null end) as YTDLeases,
	sum(case when datediff(mm,L.FundedDate,getdate())<=12 then L.FundedAmount else 0 end) as YTDFunded,
	count(case when datediff(mm,L.FundedDate,getdate()) between 9 and 12 then L.FundedAmount else null end) as '4QLeases',
	sum(case when datediff(mm,L.FundedDate,getdate()) between 9 and 12 then L.FundedAmount else 0 end) as '4QFunded',
	count(case when datediff(mm,L.FundedDate,getdate()) between 6 and 9 then L.FundedAmount else null end) as '3QLeases',
	sum(case when datediff(mm,L.FundedDate,getdate()) between 6 and 9 then L.FundedAmount else 0 end) as '3QFunded',
	count(case when datediff(mm,L.FundedDate,getdate()) between 3 and 6 then L.FundedAmount else null end) as '2QLeases',
	sum(case when datediff(mm,L.FundedDate,getdate()) between 3 and 6 then L.FundedAmount else 0 end) as '2QFunded',
	count(case when datediff(mm,L.FundedDate,getdate()) between 0 and 3 then L.FundedAmount else null end) as '1QLeases',
	sum(case when datediff(mm,L.FundedDate,getdate()) between 0 and 3 then L.FundedAmount else 0 end) as '1QFunded',
	count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end) as MeasurableLeases,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (2) then 1 else null end) as LeasesNotYetDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end) as LeasesComeDue,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3) then 1 else null end) as LeasesFPD,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 1 else null end) as LeasesPaid,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (7,8) then 1 else null end) as LeasesProcessing,
	count(case when FSP.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then 1 else null end)*1.0/nullif(count(case when FSP.FirstScheduledPaymentOutcomeID in (1,2,3,4,5,7,8) then 1 else null end),0) as '%Read',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) then L.FundedAmount else 0 end) as YTDFundedFPD,
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) then L.FundedAmount else 0 end) as YTDFundedFPDMeasure,
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) then L.FundedAmount else 0 end) as YTDFundedPending,
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3) and datediff(mm,L.FundedDate,getdate()) <= 3 then L.FundedAmount else 0 end) as '3MonthFundedFPD',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5) and datediff(mm,L.FundedDate,getdate()) <= 3 then L.FundedAmount else 0 end) as '3MonthFunded',
	sum(case when FSPO.FirstScheduledPaymentOutcomeID in (3,4,5,7,8) and datediff(mm,L.FundedDate,getdate()) <= 3 then L.FundedAmount else 0 end) as '3MonthFundedPending'
FROM
	Dealers as D left outer join Users as U on D.DealerRepID = U.ID
	join DealerCategories as DC on DC.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
	left outer join Loans as L on L.DealerID = D.ID
	left outer join FirstScheduledPaymentDetails as FSP on L.ID = FSP.LoanID
	left outer join FirstScheduledPaymentOutcome as FSPO on FSPO.FirstScheduledPaymentOutcomeID = FSP.FirstScheduledPaymentOutcomeID
	left outer join LoanStatuses as LS on LS.ID = L.StatusID
WHERE
	L.StatusID not in (21) and
	datediff(mm,L.FundedDate,getdate()) <=12
	and D.IsDemoAccount not in (1)
	--and concat(U.FirstName,' ',U.LastName) not in ('Travis Huntsman','Kyle Fransen')
	--and U.EmailAddress like '%@crestfinancial.com'
GROUP BY
	concat(U.FirstName,' ',U.LastName),
	U.ID,
	--D.Name,
	U.EmailAddress,
	case when concat(U.FirstName,' ',U.LastName) in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') then 'ASM'
		when  U.EmailAddress like '%@crestfinancial.com' and concat(U.FirstName,' ',U.LastName) not in
		('Nick Nader',
		'Gio Benedetti',
		'Randy Lee',
		'Todd Crowder',
		'Travis Huntsman',
		'Kyle Fransen') 
		then 'Inside Sales'
		else 'Outside Sales' end,
		C.CategoryName
ORDER BY
	RepName,
	CategoryName,
	SalesType desc

