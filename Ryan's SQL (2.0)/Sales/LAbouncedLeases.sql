select
	count(a.id) as leases,
	ad.City,
	--ad.stateID,
	--l.id as leaseID,
	case when l.statusID = 6 then 'Bounced'
		when l.statusID = 4 then 'Current' end as statusType,
	--cast(fundedDate as date) as fundedDate,
	--datediff(mm,fundeddate,getdate()) as monthsSinceFunding,
	--amount,
	sum(lps.dueAmount) as AmountDue,
	cast(lps.dueDate as date) as dueDate,
	sum(approvalAmount) as approvedAmount,
	sum(invoiceAmount) as invoicedAmount,
	sum(fundedAmount) as fundedAmount,
	sum(totalNote) as totalNote,
	sum(monthlyPayment) as monthlyPayment,
	sum(balance) as outstandingBalance,
	sum(pastDueAmount) as amountPastDue,
	sum(numDaysLate) as daysLate
from 
	loans as l join applicants as a on l.applicantid = a.id
	join addresses as ad on ad.id = a.addressid
	left outer join 
		(Select
			DueAmount,
			DueDate,
			loanID
		from vw_loanpaymentschedule 
		where DueDate > getdate() and DueDate < dateadd(dd,14,getdate())) as lps on lps.loanid = l.id
where
	ad.stateID = 'LA'
	and ad.City in ('Acadia',
		'Assumption',
		'Ascension',
		'Avoyelles',
		'East Baton Rouge',
		'East Feliciana',
		'Evangeline',
		'Iberia',
		'Iberville',
		'Jefferson Davis',
		'Lafayette',
		'Livingston',
		'Pointe Coupee',
		'St. Charles',
		'St. Helena',
		'St. Landry',
		'St. Martin',
		'St. Tammany',
		'Tangipahoa',
		'Vermilion',
		'Washington',
		'West Feliciana')
	and l.statusID in (4,6)
group by
	l.statusID,
	lps.duedate,
	ad.city
order by
	l.statusID,
	lps.duedate