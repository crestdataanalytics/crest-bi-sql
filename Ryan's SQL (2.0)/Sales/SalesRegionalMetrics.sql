SELECT
	CASE WHEN a.[StateID] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN a.[StateID] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN a.[StateID] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'EAST'
		WHEN a.[StateID] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'NE+FL'
		ELSE 'NCT' END AS Region,
	--C.CategoryName,
	SUM(case when L.FundedDate between dateadd(wk,datediff(wk,6,getdate()),0) and DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 6) then L.FundedAmount else 0 end) as WeeklyFunded,
	SUM(case when datediff(mm,L.FundedDate,getdate()) < 1 then L.FundedAmount else 0 end) as MonthlyFunded,
	SUM(case when datediff(mm,L.FundedDate,getdate()) < 3 then L.FundedAmount else 0 end) as QuarterlyFunded,
	SUM(case when datediff(mm,L.FundedDate,getdate()) <= 12 then L.FundedAmount else 0 end) as YearlyFunded,
	COUNT(case when L.FundedDate between dateadd(wk,datediff(wk,6,getdate()),0) and DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 6) then 1 else null end) as WeeklyCount,
	COUNT(case when datediff(mm,L.FundedDate,getdate()) < 1 then 1 else null end) as MonthlyCount,
	COUNT(case when datediff(mm,L.FundedDate,getdate()) < 3 then 1 else null end) as QuarterlyCount,
	COUNT(case when datediff(mm,L.FundedDate,getdate()) <= 12 then 1 else null end) as YearlyCount,
	avg(case when L.FundedDate between dateadd(wk,datediff(wk,6,getdate()),0) and DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 6) then L.FundedAmount end) as WeeklyAvgFunded,
	avg(case when datediff(mm,L.FundedDate,getdate()) < 1 then L.FundedAmount end) as MonthlyAvgFunded,
	avg(case when datediff(mm,L.FundedDate,getdate()) < 3 then L.FundedAmount end) as QuarterlyAvgFunded,
	avg(case when datediff(mm,L.FundedDate,getdate()) <= 12 then L.FundedAmount end) as YearlyAvgFunded,
	COUNT(case when D.CreationDate between dateadd(wk,datediff(wk,6,getdate()),0) and DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 6) and D.StatusID = 1 then 1 else null end) as WeeklyNewRetailerCount,
	COUNT(case when datediff(mm,D.CreationDate,getdate()) < 1  and D.StatusID = 1 then 1 else null end) as MonthlyNewRetailerCount,
	SUM(case when datediff(mm,D.CreationDate,getdate()) < 3 and D.StatusID = 1 then L.FundedAmount else 0 end) as ThreeMonthNewRetailerFunded
FROM
	Dealers as D join Addresses as A on D.AddressID = A.ID
	left outer join Loans as L on L.DealerID = D.ID
	join DealerCategories as DC on DC.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
GROUP BY
	CASE WHEN a.[StateID] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN a.[StateID] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN a.[StateID] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'EAST'
		WHEN a.[StateID] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'NE+FL'
		ELSE 'NCT' END
	--C.CategoryName
ORDER BY
	CASE WHEN a.[StateID] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN a.[StateID] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN a.[StateID] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'EAST'
		WHEN a.[StateID] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'NE+FL'
		ELSE 'NCT' END desc
	--C.CategoryName desc