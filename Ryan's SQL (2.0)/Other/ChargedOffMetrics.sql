SELECT
	count([Lease ID]) as 'Accounts',
	sum([Total Paid]) as 'Total Paid',
	sum([Funded Amount]) as 'Funded',
	sum([Total Note]) as 'Total Note',
	sum([Total Paid]) - sum([Total Note]) as 'Loss',
	count(case when [Auto Approved] = 1 then 1 end) as 'Auto Approved',
	count(case when [Met First Scheduled Payment] = 'No' then 1 end) as 'FPD',
	count(case when [Met First Scheduled Payment] in ('Yes','Partially') then 1 end) as 'no FPD'
FROM
	[Crest Business Intelligence].dbo.Leases
WHERE
	LeaseQueueKey in (9,12,13,14,20,22)
	and
	[Close Date] between '4/1/2016' and '4/30/2016'