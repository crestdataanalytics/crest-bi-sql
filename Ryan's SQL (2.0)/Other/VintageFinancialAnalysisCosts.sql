select
	sum([Funded Amount]) as 'Funded',
	sum([Total Paid]) as 'Paid',
	count([Funded Amount])*40 as 'Setup Fees',
	count([Funded Amount])*125 as 'Setup Cost',
	count([Funded Amount])*100 as 'Service Cost'
from Leases
where
	[Funded Date] between '1-1-15' and '5-31-15'

