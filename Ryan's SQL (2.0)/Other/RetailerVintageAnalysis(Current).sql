select
	R.[Retailer Name],
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	sum([Funded Amount]) as 'Funded',
	sum([Total Paid]) as 'Paid',
	nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) as 'Multiple',
	case when datediff(mm,[Funded Date],getdate()) >= 12 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],getdate()) = 11 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],getdate()) = 10 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],getdate()) = 9 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],getdate()) = 8 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],getdate()) = 7 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],getdate()) = 6 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],getdate()) = 5 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],getdate()) = 4 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],getdate()) = 3 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],getdate()) = 2 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],getdate()) = 1 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],getdate()) = 0 then nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) - 0.06
		end as 'Actual vs. Target',

	sum(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then [Funded Amount] else 0 end) as '90 Day Buyout $',
	nullif(isnull(sum(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then [Funded Amount] else 0 end),0)/isnull(sum([Funded Amount]),0),0) as '90 Day Buyout $ of Funded',
	sum(case when [Met First Scheduled Payment] = 'No' then [Funded Amount] else 0 end) as 'FPD $',
	nullif(isnull(sum(case when [Met First Scheduled Payment] = 'No' then [Funded Amount] else 0 end),0)/isnull(sum([Funded Amount]),0),0) as 'FPD $ of Funded',
	sum(case when LeaseQueueKey = 12 then [Funded Amount] else 0 end) as 'Charge-Off $',
	nullif(isnull(sum(case when LeaseQueueKey = 12 then [Funded Amount] else 0 end),0)/isnull(sum([Funded Amount]),0),0) as 'Charge-Off $ of Funded'
from
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
where
	R.[Retailer Name] like '%Discount Audio%' and [Funded Date] between '1-1-14' and '5-31-16'
group by
	R.[Retailer Name],CONVERT(CHAR(7),[Funded Date],120),datediff(mm,[Funded Date],getdate())
order by
	CONVERT(CHAR(7),[Funded Date],120) asc