select top 100
	dv.[VariableValue],
	dv.[variableID],
	DI.subjectID,
	V.variablename
from DocumentVariables as DV
	join DocumentInfos as DI on DV.documentid = DI.DocumentID
	join Providers as P on P.ProviderID = DI.ProviderID
	join Variables as V on V.variableid = DV.variableID
where p.providerid = 12