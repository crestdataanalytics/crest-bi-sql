SELECT
	concat([Customer ID],'-',[Sequence Number]) as 'Cust',
	[Funded Date],
	[Funded Amount],
	case when [Met First Scheduled Payment] in ('Yes','Partially') then 'No' else 'Yes' end as 'FPD',
	isnull(nullif([Total Paid],0)/nullif([Funded Amount],0),0) as 'Multiple',
	LeaseQueueKey
From Leases
Where [Funded Date] between '3-1-16' and '5-1-16'