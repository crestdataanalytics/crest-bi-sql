select
	R.[Retailer ID],
	[Retailer Name],
	[State],
	R.[Application Fee],
	case when [Sequence Number] > 1 and [LeaseQueueKey] not in (8) then 'Y' else 'N' end as 'Rpt Cust',
	[Funded Amount],
	[Total Paid],
	[Total Note],
	isnull(nullif(L.[Total Paid],0)/nullif(L.[Funded Amount],0),0) as 'Multiple',
	isnull(Datediff(mm,L.[Funded Date], L.[Close Date]),Datediff(mm,L.[Funded Date], getdate())) as 'Lease Length',
	case when L.[Auto Approved] = 1 then 'Yes' else 'No' end as 'Auto Approved',
	case when L.[Met First Scheduled Payment] in ('Yes','Partially') then 'No' else 'Yes' end as 'FPD'
from Retailers as R join
	Leases as L on R.[Retailer ID] = L.[Retailer ID]
where
	L.LeaseQueueKey in (4,5,6,12,16)
	and L.[Funded Date] > '2014-01-01'
	and L.[Met First Scheduled Payment] in ('Yes','No','Partially')
order by
	[Retailer ID]