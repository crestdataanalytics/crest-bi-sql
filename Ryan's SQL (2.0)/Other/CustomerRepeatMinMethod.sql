/*

	select 
		pad.[Customer ID],
		L.[Retailer ID],
		CONVERT(CHAR(7),L.[Submitted Date],120) as 'Funded Date'
		into ##CustomerRepeat
	from 
		[Crest Business Intelligence].dbo.Customers as pad
		join [Crest Business Intelligence].dbo.Leases as L on pad.[Customer ID] = L.[Customer ID]
	Where 
		[LeaseQueueKey] in (4,5,6,12,16)

*/

drop table ##CustomerRepeat2

select * from ##CustomerRepeat2
order by [Customer ID]

/*

	select 
		pad.[Customer ID],
		min(L.[Lease ID]) as 'First Lease'
		into ##CustomerRepeat2
	from 
		[Crest Business Intelligence].dbo.Customers as pad
		join [Crest Business Intelligence].dbo.Leases as L on pad.[Customer ID] = L.[Customer ID]
	Where 
		[LeaseQueueKey] in (4,5,6,12,16)
	Group by pad.[Customer ID]

*/


SELECT
	CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
	C.[Customer ID],
	L.[Lease ID],
    CONCAT(C.[Customer ID],'-',L.[Sequence Number]) as 'CustomerLease',
	L.[Retailer ID],
	case when (Select count(CR2.[First Lease]) from ##CustomerRepeat2 as CR2 where C.[Customer ID] = CR2.[Customer ID] 
		and L.[Lease ID] <> CR2.[First Lease]) > 0 then 'Yes' else 'No' end as 'Repeat Cust',
    L.[Funded Date],
	[Funded Amount],
	[Total Paid]
FROM
    [Crest Business Intelligence].dbo.Customers as C
		join [Crest Business Intelligence].dbo.Leases as L on C.[Customer ID] = L.[Customer ID]
WHERE
	L.[Funded Date] > '1-1-2011'
ORDER BY
    [Customer ID] asc,
    [LeaseQueueKey] asc
