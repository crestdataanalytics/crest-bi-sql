SELECT
	distinct L.[Lease ID],
	L.[LeaseQueuekey],
	CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
	L.[Funded Date],
	case when L.[Close Date] IS NULL then 'No' else 'Yes' end as 'Account Closed',
	L.[Approval Amount],
	L.[Funded Amount],
	isnull(nullif(L.[Funded Amount],0)/nullif(L.[Approval Amount],0),0) as 'Utilization',
	L.[Total Note],
	L.[Total Paid],
	isnull(nullif(L.[Total Paid],0)/nullif(L.[Funded Amount],0),0) as 'Multiple',
	isnull(Datediff(mm,L.[Funded Date], L.[Close Date]),Datediff(mm,L.[Funded Date], getdate())) as 'Lease Length',
	case when L.[Auto Approved] = 1 then 'Yes' else 'No' end as 'Auto Approved',
	case when L.[Met First Scheduled Payment] in ('Yes','Partially') then 'No' else 'Yes' end as 'FPD',
	[Age],
	datediff(mm,C.[Hire Date],getdate()) as 'Employement Length',
	C.[Monthly Income] as 'Stated Income',
	case when padv.VariableID = 6315 then padv.VariableValue end as 'Actual Income',
	case when C.[Has Credit Card] = 1 then 'Yes' else 'No' end as 'Credit Card',
	case when datediff(dd,[Funded Date],[Close Date]) < 90 then 'Yes' else 'No' end as '90 Buyout',
	[Funded Date],
	[Close Date],
	case when padv.VariableID = 8189 and padv.VariableValue = [Home Phone] then 'Yes' else 'No' end as 'Phone Verified',
	C.[State]
FROM
	[Crest Business Intelligence].dbo.Customers as C join
	[Crest Business Intelligence].dbo.Leases as L on
	C.[Customer ID] = L.[Customer ID] 
	join [ProviderAnalysis].dbo.DocumentInfos as pad on 
	C.[Customer ID] = pad.SubjectID
	join [ProviderAnalysis].dbo.DocumentVariables as padv on
	pad.DocumentID = padv.DocumentID
	join [ProviderAnalysis].dbo.Variables as pav on
	padv.VariableID = pav.VariableID
	join [ProviderAnalysis].dbo.Providers as pap on
	pad.providerID = pap.providerId
WHERE
	L.LeaseQueueKey in (4,5,6,12,16)
	and L.[Funded Date] > '2015-01-01'
	and L.[Met First Scheduled Payment] in ('Yes','No','Partially')
	and pap.ProviderName = 'DataX'
	and padv.VariableID in (6315,8189)
	and datediff(dd,[Funded Date],[Close Date]) < 90
ORDER BY
	[Vintage] asc,
	[Lease Length] asc,
	[LeaseQueueKey] asc