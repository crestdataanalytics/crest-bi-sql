/*	

IF OBJECT_ID('tempdb..##VariableData') IS NOT NULL
drop table ##VariableData

	select 
		pad.SubjectID
		,max(case when padv.VariableID = 6315 then padv.VariableValue else null end) as 'Actual Income'
		,max(case when padv.VariableID = 8189 then padv.VariableValue else null end) as [Phone Number]
		,max(case when padv.VariableID = 7219 then padv.VariableValue else null end) as [HPN]
		,max(case when padv.VariableID = 7217 then padv.VariableValue else null end) as [WPN]
		into ##VariableData
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'DataX'
			and padv.VariableID in (6315,8189,7217,7219)
			and padv.VariableValue is not null
		group by
			pad.SubjectID
*/



SELECT
    L.[Lease ID],
    L.[LeaseQueuekey],
    CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
    L.[Funded Date],
    case when L.[Close Date] IS NULL then 'No' else 'Yes' end as 'Account Closed',
    L.[Approval Amount],
    L.[Funded Amount],
    isnull(nullif(L.[Funded Amount],0)/nullif(L.[Approval Amount],0),0) as 'Utilization',
    L.[Total Note],
    L.[Total Paid],
    isnull(nullif(L.[Total Paid],0)/nullif(L.[Funded Amount],0),0) as 'Multiple',
    isnull(Datediff(mm,L.[Funded Date], L.[Close Date]),Datediff(mm,L.[Funded Date], getdate())) as 'Lease Length',
    case when L.[Auto Approved] = 1 then 'Yes' else 'No' end as 'Auto Approved',
    case when L.[Met First Scheduled Payment] in ('Yes','Partially') then 'No' else 'Yes' end as 'FPD',
    C.[Age],
    datediff(mm,C.[Hire Date],getdate()) as 'Employement Length',
    C.[Monthly Income] as 'Stated Income',
    VariableData.[Actual Income],
    case when C.[Has Credit Card] = 1 then 'Yes' else 'No' end as 'Credit Card',
    case when datediff(dd,L.[Funded Date],L.[Close Date]) < 90 then 'Yes' else 'No' end as '90 Buyout',
    L.[Funded Date],
    L.[Close Date],
    case when C.[Home Phone] = VariableData.[Phone Number] then 'Yes' else 'No' end as [Phone Verified],
	case when C.[Home Phone] = VariableData.[HPN] then 'Yes' else 'No' end as [HPN Phone Verified],
	case when C.[Home Phone] = VariableData.[WPN] then 'Yes' else 'No' end as [WPN Phone Verified],
    C.[State]
FROM
    [Crest Business Intelligence].dbo.Customers as C join
    [Crest Business Intelligence].dbo.Leases as L on
    C.[Customer ID] = L.[Customer ID] 
	left outer join ##VariableData as VariableData on C.[Customer ID]=VariableData.SubjectID
WHERE
    L.LeaseQueueKey in (12)
    and L.[Funded Date] > '2014-01-01'
    and L.[Met First Scheduled Payment] in ('Yes','No','Partially')
	and L.[Funded Date] < '2015-05-01'
	and datediff(dd,L.[Funded Date],L.[Close Date]) < 90
ORDER BY
    [Vintage] asc,
    [Lease Length] asc,
    [LeaseQueueKey] asc