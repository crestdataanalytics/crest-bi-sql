/*

update t
Set t.[Actual Income] = padv.VariableValue
from 
		##ProviderData as t join [ProviderAnalysis].dbo.DocumentInfos as pad on pad.SubjectID = t.SubjectID
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
	Where 
		    pap.ProviderName = 'DataX'
			and pav.VariableID in (6315)
*/

alter table ##ProviderData ADD [Actual Income] money
