SELECT
	'Week ' + cast(datepart(wk, [Funded Date]) as varchar(2)) Week,
	year([Funded Date]),
	sum([Funded Amount])
FROM
	Leases
WHERE
	[Funded Date] > '1-1-2013'
GROUP BY
	datepart(wk, [Funded Date]), year([Funded Date])
ORDER BY
	datepart(wk, [Funded Date]), year([Funded Date])

SELECT
    DATEADD(dd, 7-(DATEPART(dw, min([Funded Date]))), min([Funded Date]))
      as EndOfWeek
,   sum([Funded Amount])
FROM Leases

GROUP BY datepart(week,[Funded Date])
ORDER BY datepart(week,[Funded Date])

SELECT 
	'Month ' + cast(datepart(month, [Funded Date]) as varchar(2)) Month,
    DATEADD(dd, 1-(DATEPART(dw, min([Funded Date]))), min([Funded Date]))
      as EndOfWeek,
	year([Funded Date]),
    sum([Funded Amount])
FROM Leases
WHERE
	[Funded Date] > '1-1-2013'
GROUP BY datepart(Month,[Funded Date]),year([Funded Date])
ORDER BY year([Funded Date]),datepart(Month,[Funded Date])