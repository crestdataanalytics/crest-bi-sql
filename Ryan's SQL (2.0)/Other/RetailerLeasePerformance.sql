select 
	Retailers.[Retailer ID],
	[Retailer Group Name],
	[Retailer Name],
	Retailers.[State],
	Leases.[Application Fee],
	[Lease ID],
	[Submitted Date],
	[Funded Date],
	case when [LeaseQueueKey] in (4) then 'Current'
	when [LeaseQueueKey] in (6,16) then 'Past Due'
	when [LeaseQueueKey] in (5) then 'Paid'
	when [LeaseQueueKey] in (9,12,13,14,20) then 'Charge Off'
	when [LeaseQueueKey] in (1,2,3) then 'Pending'
	when [LeaseQueueKey] in (7) then 'Denied' else 'N/A' end as 'LeaseStatus',
	[Agreement Number]
	[Sequence Number],
	[Approval Amount],
	[Funded Amount],
	[Total Note],
	[Total Paid],
	[Met First Scheduled Payment],
	[Age],
	[Monthly Income]
from Retailers join
leases on Leases.[Retailer ID] = Retailers.[Retailer ID] join
customers on leases.[Customer ID] = Customers.[Customer ID]
where retailers.[Retailer Group Name] like 'aamco'
and leases.[leasequeuekey] in (1,2,3,4,5,6,7,9,12,13,14,16,20)