select  top 10
	[Monthly Income],
	[Monthly Income]-VariableValue as 'Diff'
from [Crest Business Intelligence].dbo.customers as cbic
	join [ProviderAnalysis].dbo.DocumentInfos as pad on 
	cbic.[Customer ID] = pad.SubjectID
	join [ProviderAnalysis].dbo.DocumentVariables as padv on
	pad.DocumentID = padv.DocumentID
	join [ProviderAnalysis].dbo.Variables as pav on
	padv.VariableID = pav.VariableID
	join [ProviderAnalysis].dbo.Providers as pap on
	pad.providerID = pap.providerId
	join [Crest Business Intelligence].dbo.Leases as cbil on
	cbic.[Customer ID] = cbil.[Customer ID]
	where pap.ProviderName = 'DataX'
	and padv.VariableID in (6315)
	and cbil.leasequeuekey in (4,5,6,12)


