/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
	D.Name,
	C.CategoryName
FROM [CrestFinancial_20160424].[dbo].[DealerCategories] as DC
	join [CrestFinancial_20160424].[dbo].[Dealers] as D on DC.[DealerID] = D.ID
	join [CrestFinancial_20160424].[dbo].[Categories] as C on DC.CategoryID = C.ID