Declare @specified_date Date
set @specified_date = '1-31-2016'
select
	--CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	count([Lease ID]) as 'Active Lease'
from
	leases
where
	([Close Date] is null or [Close Date] > @specified_date) and ([Funded Date] < @specified_date)
--group by
	--CONVERT(CHAR(7),[Funded Date],120)
--order by
	--CONVERT(CHAR(7),[Funded Date],120) asc