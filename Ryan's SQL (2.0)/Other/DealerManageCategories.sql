SELECT 
	  DC.[DealerID],
	  D.[Name],
      C.[CategoryName]
  FROM [CrestFinancial_20160424].[dbo].[Categories] as C
	join [CrestFinancial_20160424].[dbo].[DealerCategories] as DC on C.ID = DC.CategoryID
	join [CrestFinancial_20160424].[dbo].[Dealers] as D on DC.DealerID = D.ID