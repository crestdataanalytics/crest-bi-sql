select count(distinct [Customer ID]) as 'Repeat Customers'
from (
		select [Customer ID],
			   [Funded Date],
			   [close date],
			   [LeaseQueuekey],	
			   count(*) over(partition by [Customer ID]) as dc
			   from [crest business intelligence].dbo.Leases
			   where [Funded Date] is not null and LeaseQueueKey in (4,6,5,12)
			   ) as T
where dc > 1
and [Funded Date] < '5/1/2016'
and ([close date] > '5/1/2016' or [close date] is null)
and leasequeuekey in(4,5,6,12)