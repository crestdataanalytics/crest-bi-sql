/*
 
       select
             pad.[Customer ID],
             L.[Retailer ID],
             [Funded Date]
             into ##CustomerRepeat
       from
             [Crest Business Intelligence].dbo.Customers as pad
             join [Crest Business Intelligence].dbo.Leases as L on pad.[Customer ID] = L.[Customer ID]
       Where
             [LeaseQueueKey] in (4,5,6,12,16)
 
*/

SELECT
       CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
       [Customer ID],
    CONCAT([Customer ID],'-',[Sequence Number]) as 'CustomerLease',
       [Retailer ID],
       Case when [LeaseCount]>1 then 'Yes' else 'No' end as RepeatCustomer,
       [Funded Date],
       [Funded Amount],
       [Total Paid]
FROM
       (
       Select
             L.[Funded Date],
             l.[Sequence Number],
             l.[Retailer ID],
             L.[Funded Amount],
             L.[Total Paid],
             L.LeaseQueueKey,
             C.[Customer ID],
             count(CR.[Customer ID]) as [LeaseCount]
             from
             [Crest Business Intelligence].dbo.Customers as C
             join [Crest Business Intelligence].dbo.Leases as L on C.[Customer ID] = L.[Customer ID]
             left outer join ##CustomerRepeat as CR on C.[Customer ID] = CR.[Customer ID] and L.[Funded Date] >= CR.[Funded Date]
             where L.[Funded Date] > '1-1-2009'
             group by
             L.[Funded Date],
             l.[Sequence Number],
             l.[Retailer ID],
             L.[Funded Amount],
             L.[Total Paid],
             L.LeaseQueueKey,
             C.[Customer ID]
       ) as SummaryData
ORDER BY
    [Customer ID] asc,
    [LeaseQueueKey] asc