select
	[Retailer Name],
	R.[State],
	count(case when [LeaseQueueKey] not in (7,11) then 1 else null end)*1.0/count([Customer ID]) as 'Approval Rate',
	count([Customer ID]) as 'Applications',
	count(case when [Auto Approved] = 1 then 1 else null end) as 'Auto Approved',
	count(case when [LeaseQueueKey] not in (7,11) then 1 else null end) as 'Total Approved',
	count(case when [Funded Amount] > 0 then 1 else null end) as '# Funded',
	count(case when [LeaseQueueKey] in (5)then 1 else null end) as '# Paid',
	count(case when [LeaseQueueKey] in (12)then 1 else null end) as '# Charged-Off',
	count(case when [LeaseQueueKey] in (4,6) then 1 else null end) as 'Active Leases',
	sum(case when [LeaseQueueKey] in (4) then [Total Note]-[Total Paid] else 0 end) as '$ Funded RCV',
	count(case when [LeaseQueueKey] in (6) then 1 else null end) as '# Bounced RCV',
	sum(case when [LeaseQueueKey] in (6) then [Total Note]-[Total Paid] else 0 end) as '$ Bounced RCV',
	sum([Total Paid]) as 'Total Paid',
	sum([Funded Amount]) as 'Funded Amount',
	isnull(nullif(sum([Total Paid]),0)/nullif(sum([Funded Amount]),0),0) as 'Multiple',
	count(case when [Met First Scheduled Payment] in ('No') then 1 else null end) as 'FPD #',
	count(case when [Met First Scheduled Payment] in ('Yes','Partially') then 1 else null end) as 'Paid #',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when [Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD % of Funded',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when [Funded Amount] > 0 and [Met First Scheduled Payment] in ('Yes','Partially','No') then 1 else null end),0)),0) as 'FPD % of Leases Come Due'
From Retailers as R
join Leases as L on L.[Retailer ID] = R.[Retailer ID]
where
	[Retailer Group Name] = 'aamco'
Group by	
	 R.[Retailer ID], [Retailer Name], R.[state]

UNION ALL

Select
	null,
	null,
	count(case when [LeaseQueueKey] not in (7,11) then 1 else null end)*1.0/count([Customer ID]) as 'Approval Rate',
	count([Customer ID]) as 'Applications',
	count(case when [Auto Approved] = 1 then 1 else null end) as 'Auto Approved',
	count(case when [LeaseQueueKey] not in (7,11) then 1 else null end) as 'Total Approved',
	count(case when [Funded Amount] > 0 then 1 else null end) as '# Funded',
	count(case when [LeaseQueueKey] in (5)then 1 else null end) as '# Paid',
	count(case when [LeaseQueueKey] in (12)then 1 else null end) as '# Charged-Off',
	count(case when [LeaseQueueKey] in (4,6) then 1 else null end) as 'Active Leases',
	sum(case when [LeaseQueueKey] in (4) then [Total Note]-[Total Paid] else 0 end) as '$ Funded RCV',
	count(case when [LeaseQueueKey] in (6) then 1 else null end) as '# Bounced RCV',
	sum(case when [LeaseQueueKey] in (6) then [Total Note]-[Total Paid] else 0 end) as '$ Bounced RCV',
	sum([Total Paid]) as 'Total Paid',
	sum([Funded Amount]) as 'Funded Amount',
	isnull(nullif(sum([Total Paid]),0)/nullif(sum([Funded Amount]),0),0) as 'Multiple',
	count(case when [Met First Scheduled Payment] in ('No') then 1 else null end) as 'FPD #',
	count(case when [Met First Scheduled Payment] in ('Yes','Partially') then 1 else null end) as 'Paid #',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when [Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD % of Funded',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when [Funded Amount] > 0 and [Met First Scheduled Payment] in ('Yes','Partially','No') then 1 else null end),0)),0) as 'FPD % of Leases Come Due'
From Retailers as R
join Leases as L on L.[Retailer ID] = R.[Retailer ID]
where
	[Retailer Group Name] = 'aamco'