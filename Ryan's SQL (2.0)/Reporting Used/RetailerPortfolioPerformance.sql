declare @todays_date date, @2015db date, @2015de date
set @todays_date = cast(getdate() as date)
set @2015db = '2015-01-01'
set @2015de = '2015-12-31'

select
	[Retailer Group Name],
	[Retailer Name],
	R.[Retailer ID],
	[State],
	count(case when ([Submitted Date] between @2015db and @2015de) then 1 else null end) as 'Applications',
	count(case when ([Submitted Date] between @2015db and @2015de) and [LeaseQueueKey] not in (7,8,11) then 1 else null end) as 'Approved',
	(count(case when ([Submitted Date] between @2015db and @2015de) and [LeaseQueueKey] not in (7,8,11) then 1 else null end) *1.00/
		count(case when ([Submitted Date] between @2015db and @2015de) then 1 else null end)) as 'Approval Rate',
	(count(case when ([Submitted Date] between @2015db and @2015de) and [Auto Approved] = 1 then 1 else null end) *1.00/
		count(case when ([Submitted Date] between @2015db and @2015de) and[LeaseQueueKey] not in (7) then 1 else null end)) as 'Auto Approved %',
	count(case when ([Submitted Date] between @2015db and @2015de) and [Sequence Number] > 1 and [LeaseQueueKey] not in (7,11) then 1 else null end) as 'Rpt Customers',
	count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end) as '# Funded',
	sum(case when ([Funded Date] between @2015db and @2015de) then [Funded Amount] else 0 end) as 'Funded Amount',
	isnull(nullif(sum(case when ([Funded Date] between @2015db and @2015de) then [Funded Amount] else 0 end),0)/
		nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end),0),0) as 'Ave. Funded',
	sum(case when ([Funded Date] between @2015db and @2015de) then [Total Paid] else 0 end) as 'Paid Amount',
	isnull(sum(case when ([Close Date] between @2015db and '2016-05-01') then [Total Paid] else 0 end)/
		sum(case when ([Close Date] between @2015db and '2016-05-01') then [Funded Amount] else 0 end),0) as 'Multiple',
	sum(case when ([Funded Date] between @2015db and @2015de) and [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)*1.00/
		sum(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then [Funded Amount] else 0 end) as 'FPD $',
	isnull((nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD %',
	(count(case when ([Funded Date] between @2015db and @2015de) and datediff(dd,[Funded Date],[Close Date]) < 90 and
		 [LeaseQueueKey] = 5 then 1 else null end)*1.0/count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end)) as '90 Day Buyout %',
	sum(case when ([Funded Date] between @2015db and @2015de) and datediff(dd,[Funded Date],[Close Date]) < 90  and
		 [LeaseQueueKey] = 5 then [Total Paid] else null end)*1.0/
			sum(case when ([Funded Date] between @2015db and @2015de) and
			[LeaseQueueKey] = 5 then [Total Paid] else null end) as '90 Day Buyout $',
	isnull(nullif(count(case when ([Funded Date] between @2015db and @2015de) and [LeaseQueueKey] in (12) then 1 else null end),0)*1.0/
		nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end),0),0) as 'Charge Off %',
	sum(case when ([Funded Date] between @2015db and @2015de) and [LeaseQueueKey] in (12) then ([Total Paid]-[Total Note]) else null end)*1.0/
		sum(case when ([Funded Date] between @2015db and @2015de) then ([Total Paid]-[Total Note]) else null end) as 'Charge Off $',
	isnull(nullif(sum(case when ([Funded Date] between @2015db and @2015de) and ([Close Date] between @2015db and '2016-05-01') then datediff(mm,[Funded Date],[Close Date]) else 0 end),0)/
		nullif(count(case when ([Funded Date] between @2015db and @2015de) and ([Close Date] between @2015db and '2016-05-01') then 1 else null end),0),0) as 'Ave. Lease Length'
From Retailers as R
join Leases as L on L.[Retailer ID] = R.[Retailer ID]
where
	([Close Date] < '2016-05-01' or [Close Date] is null)
	and [Retailer Group Name] = 'Gardner-White'
Group by	
	 [Retailer Group Name],R.[Retailer ID],[Retailer Name], [state]

UNION ALL

Select
	null,
	null,
	null,
	null,
	count(case when ([Submitted Date] between @2015db and @2015de) then 1 else null end) as 'Applications',
	count(case when ([Submitted Date] between @2015db and @2015de) and [LeaseQueueKey] not in (7,8,11) then 1 else null end) as 'Approved',
	(count(case when ([Submitted Date] between @2015db and @2015de) and [LeaseQueueKey] not in (7,8,11) then 1 else null end) *1.00/
		count(case when ([Submitted Date] between @2015db and @2015de) then 1 else null end)) as 'Approval Rate',
	(count(case when ([Submitted Date] between @2015db and @2015de) and [Auto Approved] = 1 then 1 else null end) *1.00/
		count(case when ([Submitted Date] between @2015db and @2015de) and[LeaseQueueKey] not in (7) then 1 else null end)) as 'Auto Approved %',
	count(case when ([Submitted Date] between @2015db and @2015de) and [Sequence Number] > 1 and [LeaseQueueKey] not in (7,11) then 1 else null end) as 'Rpt Customers',
	count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end) as '# Funded',
	sum(case when ([Funded Date] between @2015db and @2015de) then [Funded Amount] else 0 end) as 'Funded Amount',
	isnull(nullif(sum(case when ([Funded Date] between @2015db and @2015de) then [Funded Amount] else 0 end),0)/
		nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end),0),0) as 'Ave. Funded',
	sum(case when ([Funded Date] between @2015db and @2015de) then [Total Paid] else 0 end) as 'Paid Amount',
	isnull(sum(case when ([Close Date] between @2015db and '2016-05-01') then [Total Paid] else 0 end)/
		sum(case when ([Close Date] between @2015db and '2016-05-01') then [Funded Amount] else 0 end),0) as 'Multiple',
	sum(case when ([Funded Date] between @2015db and @2015de) and [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)*1.00/
		sum(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then [Funded Amount] else 0 end) as 'FPD $',
	isnull((nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD %',
	(count(case when ([Funded Date] between @2015db and @2015de) and datediff(dd,[Funded Date],[Close Date]) < 90 and
		 [LeaseQueueKey] = 5 then 1 else null end)*1.0/count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end)) as '90 Day Buyout %',
	sum(case when ([Funded Date] between @2015db and @2015de) and datediff(dd,[Funded Date],[Close Date]) < 90  and
		 [LeaseQueueKey] = 5 then [Total Paid] else null end)*1.0/
			sum(case when ([Funded Date] between @2015db and @2015de) and
			[LeaseQueueKey] = 5 then [Total Paid] else null end) as '90 Day Buyout $',
	isnull(nullif(count(case when ([Funded Date] between @2015db and @2015de) and [LeaseQueueKey] in (12) then 1 else null end),0)*1.0/
		nullif(count(case when ([Funded Date] between @2015db and @2015de) and [Funded Amount] > 0 then 1 else null end),0),0) as 'Charge Off %',
	sum(case when ([Funded Date] between @2015db and @2015de) and [LeaseQueueKey] in (12) then ([Total Paid]-[Total Note]) else null end)*1.0/
		sum(case when ([Funded Date] between @2015db and @2015de) then ([Total Paid]-[Total Note]) else null end) as 'Charge Off $',
	isnull(nullif(sum(case when ([Funded Date] between @2015db and @2015de) and ([Close Date] between @2015db and '2016-05-01') then datediff(mm,[Funded Date],[Close Date]) else 0 end),0)/
		nullif(count(case when ([Funded Date] between @2015db and @2015de) and ([Close Date] between @2015db and '2016-05-01') then 1 else null end),0),0) as 'Ave. Lease Length'
From Retailers as R
join Leases as L on L.[Retailer ID] = R.[Retailer ID]
where
	([Close Date] < '2016-05-01' or [Close Date] is null)
	and [Retailer Group Name] = 'Gardner-White'