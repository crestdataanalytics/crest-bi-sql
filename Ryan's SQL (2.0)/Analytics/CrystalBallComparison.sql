Select
	CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
	R.Category,
	L.[Lease ID],
	L.[Customer ID],
	L.[Funded Date],
	L.[Retailer ID],
	L.[Auto Approved],
	L.LeaseQueueKey,
	L.[Met First Scheduled Payment],
	L.[Approval Amount],
	L.[Funded Amount],
	L.[Funded Amount]/L.[Approval Amount] as Utilization,
	case when [close date] is null then datediff(dd,[Funded Date],getdate()) else datediff(dd,[Funded Date],[Close Date]) end as LeaseLength,
	L.[Total Paid],
	CB.CBAmount,
	CB.CBResult,
	CB.CBNode
FROM
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
	left outer join
			(select
				pad.SubjectID,
				max(case when pav.VariableID = 45997 then padv.VariableValue end) as CBAmount,
				max(case when pav.VariableID = 45993 then padv.VariableValue end) as CBResult,
				max(case when pav.VariableID = 45994 then padv.VariableValue end) as CBNode
				--max(case when pav.VariableID = 6250 then padv.VariableValue end) as BAVCode
			from [ProviderAnalysis].dbo.DocumentInfos as pad 
					join [ProviderAnalysis].dbo.DocumentVariables as padv on
					pad.DocumentID = padv.DocumentID
					join [ProviderAnalysis].dbo.Variables as pav on
					padv.VariableID = pav.VariableID
					join [ProviderAnalysis].dbo.Providers as pap on
					pad.providerID = pap.providerId
			where 
				ProviderName = 'Crystalball'
				and pav.VariableID in (45997,45993,45994)
			group by
				pad.subjectID) as CB on CB.SubjectID = L.[Customer ID]

where
	LeaseQueueKey not in (7,11) and
	--[Funded Date] between '12-31-14' and '7-1-16'
	--and datediff(dd,[Funded Date],getdate()) > 30
	--and L.[Met First Scheduled Payment] in ('Yes','No','Partially')
	--L.[Retailer ID] in (2512)
	--CB.CBNode in (24,25,26,27,28,29)
	CB.CBNode in (11)
	and L.[Funded Date] > '7-15-16'
	--L.[Retailer ID] = 11138