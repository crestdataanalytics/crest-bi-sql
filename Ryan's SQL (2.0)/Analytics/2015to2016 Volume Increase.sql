SELECT
	(case when L.CreationDate between '7-31-16' and '8-20-16' then '2016'
			when L.CreationDate between '6-30-16' and '7-31-16' then '20162'
		when L.CreationDate between '8-2-15' and '8-22-15' then '2015' end) as DateRange,
	--C.CategoryName,
	count(L.ID) as SubmittedLeases,
	count(case when L.StatusID not in (7,11) then 1 else null end) as ApprovedLeases,
	count(case when L.StatusID not in (7,11) then 1 else null end)*1.0/count(L.ID) as ApprovedRate,
	sum(L.ApprovalAmount) as ApprovedAmount,
	sum(L.FundedAmount) as FundedAmount,
	avg(case when L.StatusID not in (7,11) then L.ApprovalAmount end) as AvgApproved,
	avg(case when L.StatusID not in (7,11) and L.FundedAmount > 0 then L.FundedAmount end) as avgFunded
From
	Loans as L join Dealers as D on L.DealerID = D.ID
	join DealerCategories as DC on DC.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
Where
	(L.CreationDate between '7-31-16' and '8-20-16')
	OR (L.CreationDate between '8-2-15' and '8-22-15')
	OR (L.CreationDate between '6-30-16' and '7-31-16')
Group by
	(case when L.CreationDate between '7-31-16' and '8-20-16' then '2016'
	when L.CreationDate between '6-30-16' and '7-31-16' then '20162'
		when L.CreationDate between '8-2-15' and '8-22-15' then '2015' end)
	--C.CategoryName
Order by
		(case when L.CreationDate between '7-31-16' and '8-20-16' then '2016'
	when L.CreationDate between '6-30-16' and '7-31-16' then '20162'
		when L.CreationDate between '8-2-15' and '8-22-15' then '2015' end) desc


select
	d.id, d.Name,
	dg.dealergroupname,
	fmc.category,
	sum(l.fundedamount) as funded,
	riskscore,
	loanlimit,
	case when riskscore < 7 and fmc.category in ('Superstar','Allstar') then 'Too Rich' end as Risk
from
	dealers as d join loans as l on d.id = l.dealerid
	left outer join dealergroups as dg on dg.id = d.dealergroupid
	join fundingmatrixcategory as fmc on fmc.id = d.fundingmatrixcategoryid
where
	fundingmatrixcategoryID is not null
	and datediff(mm,fundeddate,getdate()) <1
group by
	d.id,
	d.name,
	dg.dealergroupname,
	fmc.category,
	riskscore,
	loanlimit
order by
	riskscore asc
	