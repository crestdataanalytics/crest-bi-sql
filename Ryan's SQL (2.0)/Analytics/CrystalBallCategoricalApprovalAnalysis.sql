drop table ##CrystalBallTemp
select
	DI.SubjectID as 'CustomerID',
	max(case when dv.[variableID] = 45993 then dv.[variablevalue] end) as 'Result',
	max(case when dv.[variableID] = 45994 then dv.[variablevalue] end) as 'Node',
	max(case when dv.[variableID] = 45995 then dv.[variablevalue] end) as 'Factor',
	max(case when dv.[variableID] = 45996 then dv.[variablevalue] end) as 'Score',
	max(case when dv.[variableID] = 45997 then dv.[variablevalue] end) as 'Amount'
	into ##CrystalBallTemp
from CFREPORTS.ProviderAnalysis.dbo.DocumentVariables as DV
	join CFREPORTS.ProviderAnalysis.dbo.DocumentInfos as DI on DV.documentid = DI.DocumentID
	join CFREPORTS.ProviderAnalysis.dbo.Providers as P on P.ProviderID = DI.ProviderID
	join CFREPORTS.ProviderAnalysis.dbo.Variables as V on V.variableid = DV.variableID
where p.providerid = 14
group by di.subjectid

declare @sDate as date
set @sDate = '7-2-16'

select
	case when c.result = 1 then convert(decimal(18,2),c.amount)*.7 end,
	c.node,
	case when leasequeuekey not in (7,11) and [auto approved] = 1 then l.[approval amount]*.7 end
from ##CrystalBallTemp as c join CFREPORTS.[Crest Business Intelligence].dbo.Leases as l on c.customerID = L.[Customer ID]
	join CFREPORTS.[Crest Business Intelligence].dbo.retailers as r on r.[retailer id] = l.[retailer id]
	join Blending.dbo.dealerCategories as DC on DC.[DealerID] = R.[Retailer ID]
where [submitted date] >= @sdate and [submitted date] < Convert(date, getdate()) and c.node in (1,2,3,18,19,20,21,22,23,24,27,30,32)
