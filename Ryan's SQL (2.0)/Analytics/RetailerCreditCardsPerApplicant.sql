SELECT
	D.ID,
	D.Name,
	D.RiskScore,
	cast(D.CreationDate as date) as DateOpened,
	count(distinct L.ApplicantID) as Applicants,
	count(distinct ACC.ApplicantID) as ApplicantsWithCard,
	count(distinct ACC.CreditCardID)*1.0/nullif(count(distinct ACC.ApplicantID),0) as AvgCC,
	count(distinct ABA.ApplicantID) as ApplicantsWithBank,
	count(distinct ABA.BankAccountID)*1.0/nullif(count(distinct ABA.ApplicantID),0) as AvgBA,
	count(distinct case when LP.TypeID = 2 then LP.PaymentMethodLastFour else null end)/nullif(count(distinct ACC.ApplicantID),0) as AvgCCP
FROM
	Dealers as D join Loans as L on D.ID = L.DealerID
		left outer join ApplicantCreditCards as ACC on ACC.ApplicantID = L.ApplicantID
		left outer join ApplicantBankAccounts as ABA on ABA.ApplicantID = L.ApplicantID
		left outer join LoanPayments as LP on LP.LoanID = L.ID
WHERE
	L.FundedAmount > 0
	and D.StatusID = 1
GROUP BY
	D.ID,
	D.Name,
	D.RiskScore,
	cast(D.CreationDate as date)
ORDER BY
	AvgCCP desc
