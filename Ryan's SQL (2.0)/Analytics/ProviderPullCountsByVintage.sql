SELECT
	count(L.[Customer ID]),
	count(PD.SubjectID) as DataX,
	count(PDC.SubjectID) as Clarity
FROM
	Leases as L left outer join
	(
	select 
		pad.SubjectID
		,max(case when padv.VariableID = 4620 then padv.VariableValue else null end) as 'DX'
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'DataX'
		group by
			pad.SubjectID
	) as PD on PD.SubjectID = L.[Customer ID]
	left outer join
		(
	select 
		pad.SubjectID
		,max(case when padv.VariableID = 246 then padv.VariableValue else null end) as 'C'
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'Clarity'
		group by
			pad.SubjectID
	) as PDC on PDC.SubjectID = L.[Customer ID]
WHERE
	[Funded Date] >= '8-1-16'
	and [Funded Date] < '9-1-16'