select
	D.ID as DealerID,
	D.Name as DealerName,
	D.CreationDate as DealerCreateDate,
	concat(U.FirstName,' ',U.LastName) as RepName,
	U.EmailAddress as RepEmail,
	U.RepBusinessName,
	A.*
from
	Dealers as D join Addresses as A on D.AddressID = A.ID
		left outer join Users as U on U.ID = D.DealerRepID
where
	D.StatusID in (5,6)
order by
	D.CreationDate asc