/*
select * from ##rankhistory
where RetailerName like '%unclaimed diamonds (online)%'
order by TStamp asc
*/

Declare @intMonth int
set @intMonth = 1
declare @count int
set @count = 0
declare @d1 date


IF OBJECT_ID('tempdb..##RankHistory') IS NOT NULL drop table ##RankHistory
create table ##RankHistory(TStamp date,Category varchar(max), RetailerName varchar(max),RetailerID int,
	CurrentStatus varchar(25),CreateDate Date,[Ranking10Month] int,MaxApprovalScore decimal(18,2),LeaseCount int, Funded decimal(18,2),[3moFPD] decimal(18,2),[3moChargeOff] decimal(18,2),
	[3moPaid] decimal(18,2),Multiple decimal(18,2),AvsT decimal(18,2),[3moBounced] decimal(18,2),QuarterlyFPDvsVolume decimal(18,2), ConversionRate decimal(18,2))

WHILE @intMonth<36
BEGIN

set @d1 = dateadd(month,@count,getdate())
IF OBJECT_ID('tempdb..##TempRetailer') IS NOT NULL
	drop table ##TempRetailer  
    select
		R.[Retailer Name],
	R.[Retailer ID],
	R.[State],
	sum(case when datediff(dd,[Funded Date],@d1) < 90 then [Funded Amount] else 0 end) as '3 mo. Funded',	
	isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) >= 1 and datediff(mm,[Funded Date],@d1) < 4 then [Funded Amount] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],@d1) < 4 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end),0),0) as '3 mo. FPD $',
	isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) >= 1 and datediff(mm,[Funded Date],@d1) < 4 then 1 else null end),0)*1.0/	
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
	isnull(nullif(count(case when [LeaseQueueKey] = 6 and datediff(mm,[Funded Date],@d1) <= 3 then 1 else null end),0)*1.0/	
		nullif(count(case when [Close Date] is null and datediff(mm,[Funded Date],@d1)  <= 3 then 1 else null end),0),0) as '3 mo. Bounced %',
	isnull(nullif(count(case when [LeaseQueueKey] = 12 and datediff(mm,[Funded Date],@d1) between 5 and 8 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(mm,[Funded Date],@d1) between 5 and 8 then 1 else null end),0),0) as '3 mo. Charge-Off %',
	isnull(nullif(count(case when [LeaseQueueKey] = 5 and datediff(dd,[Close Date],@d1) <= 90 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(dd,[Close Date],@d1) <= 90 then 1 else null end),0),0) as '3 mo. Paid %',
	isnull(nullif(sum(case when datediff(dd,[Close Date],@d1) < 90 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(dd,[Close Date],@d1) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. Closing Multiple',
	isnull(nullif(sum(case when [Close Date] is not null then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when [Close Date] is not null then [Funded Amount] else 0 end),0),0) as 'Overall Closing Multiple',
	isnull(nullif(sum(case when datediff(mm,[Funded Date],@d1) between 12 and 24 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],@d1) between 12 and 24 then [Funded Amount] else 0 end),0),0) as 'One Year Multiple',
	count(case when datediff(mm,[Funded Date],@d1) <=3 then 1 else null end) as '3 mo Volume',	
	count(case when datediff(mm,[Funded Date],@d1) <= 6 then 1 else null end) as '6 mo Volume',	
	count(case when datediff(mm,[Funded Date],@d1) <= 12 then 1 else null end) as '12 mo Volume',	
	isnull(nullif(count(case when datediff(mm,[Funded Date],@d1) <= 3 then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Funded Date],@d1) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
	isnull(nullif(count(case when datediff(mm,[Submitted Date],@d1) <= 3 and [LeaseQueueKey] not in (7,11) then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Submitted Date],@d1) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate',
	(isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) <=3 then 1 else null end),0)*1.0/
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],@d1) between 4 and 7 then 1 else null end),0),0))-
	 (isnull(nullif(count(case when datediff(mm,[Funded Date],@d1) <= 3 then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Funded Date],@d1) between 4 and 7  then 1 else null end),0),0)) as 'Quarterly FPD vs Volume %',
	sum(case when datediff(mm,[Funded Date],@d1) >= 12 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],@d1) = 11 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],@d1) = 10 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],@d1) = 9 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],@d1) = 8 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],@d1) = 7 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],@d1) = 6 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],@d1) = 5 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],@d1) = 4 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],@d1) = 3 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],@d1) = 2 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],@d1) = 1 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],@d1) = 0 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.06
		else 0
		end)/nullif(count([Lease ID]),0) as 'Actual vs. Target'
	into ##TempRetailer
from
	CFREPORTS.[Crest Business Intelligence].dbo.Retailers as R left outer join CFREPORTS.[Crest Business Intelligence].dbo.Leases as L on R.[Retailer ID] = L.[Retailer ID]
--where
	--R.[Current Status] in ('Active') 
group by
	R.[Retailer Name], R.[Retailer ID], R.[State]
order by
	[One Year Multiple] asc,
	r.[Retailer Name]


	IF OBJECT_ID('tempdb..##RetailerMax') IS NOT NULL
	drop table ##RetailerMax 
select
##TempRetailer.[Retailer ID] as RetailerID,
[3 mo. FPD $],
[3 mo. Charge-Off %],
[3 mo. Paid %],
coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) as 'Multiple',
[Actual vs. Target],
[3 mo. Bounced %],
[Quarterly FPD vs Volume %],
[3 mo. Funded],
[3 mo Volume],
(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*5
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*4
				)
				+ 
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/100.00 'Max Approval Score'
			into ##RetailerMax
			from ##TempRetailer

declare @sDate as date, @sDate2 as date
set @sDate = Convert(date, dateadd(day, -365, @d1))
set @sDate2 = Convert(date, @d1)

IF OBJECT_ID('tempdb..##RankingTemp') IS NOT NULL
drop table ##RankingTemp

select
	@d1 as T,
	R.Category as C,
	R.[Retailer Name] as RN,
	R.[Retailer ID] as RID,
	R.[Current Status] as CS,
	convert(date,R.[Creation Date]) as CD,
	R.[Ranking (10 month)] as RTEN,
	RM.[Max Approval Score]*10 as S,
	RM.[3 mo. FPD $] as TFPD,
	RM.[3 mo. Charge-Off %] as TCO,
	RM.[3 mo. Paid %] as TP,
	RM.Multiple as M,
	RM.[Actual vs. Target] as AVT,
	RM.[3 mo. Bounced %] as TB,
	RM.[Quarterly FPD vs Volume %] as QFPD,
	count(case when [LeaseQueueKey] in (4,9,12,13,14,20,21,22) then 1 else null end)*1.0/count(case when [LeaseQueueKey] not in (7,11) then 1 else null end) as CR,
	sum(case when datediff(mm,dateadd(month,-1,[Funded Date]),[Funded Date]) = 1 then [Funded Amount] else 0 end) as TMF,
	count(case when datediff(mm,dateadd(month,-1,[Funded Date]),[Funded Date]) = 1 then 1 else null end) as TMC
	into ##RankingTemp
from
	Leases as L left join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
	left outer join ##RetailerMax as RM on R.[Retailer ID] = RM.RetailerID
where
	([Funded Date] between @sDate and @sDate2) and [Funded Amount]>0
	--and RM.[Max Approval Score] < 0.5
GROUP BY
	R.Category,
	R.[Retailer Name],
	R.[Retailer ID],
	R.[Current Status],
	convert(date,R.[Creation Date]),
	R.[Ranking (10 month)],
	RM.[Max Approval Score]*10,
	RM.[3 mo. FPD $],
	RM.[3 mo. Charge-Off %],
	RM.[3 mo. Paid %],
	RM.Multiple,
	RM.[Actual vs. Target],
	RM.[3 mo. Bounced %],
	RM.[Quarterly FPD vs Volume %],
	RM.[3 mo. Funded],
	RM.[3 mo Volume]
ORDER BY
	RM.[Max Approval Score]*10 desc

	Insert into ##RankHistory(TStamp,Category,RetailerName,RetailerID,CurrentStatus,CreateDate,[Ranking10Month],MaxApprovalScore,[3moFPD],LeaseCount,Funded,[3moChargeOff],[3moPaid],Multiple,AvsT,[3moBounced],QuarterlyFPDvsVolume, ConversionRate)
	select T,C,RN,RID,CS,CD,RTEN,S,TFPD,TMC,TMF,TCO,TP,M,AVT,TB,QFPD,CR from ##RankingTemp

	print @intMonth
    set @intMonth = @intMonth + 1
	set @count = @count - 1
END



