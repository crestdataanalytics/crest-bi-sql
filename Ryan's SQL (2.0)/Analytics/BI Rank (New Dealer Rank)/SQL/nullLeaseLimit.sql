select
	ID,
	Name,
	CreationDate,
	LoanLimit
from
	Dealers
where
	LoanLimit is null
	and CancelDate is null
	and StatusID = 1