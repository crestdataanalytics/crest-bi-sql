SELECT
	L.[DealerID],
	avg(case when WC.[ID] not in (3,4) then 0.0 else 1.0 end) as Type
FROM
	Loans as L join LoanWelcomeCallStatus as W on L.ID = W.LoanID
	join WelcomeCallStatuses as WC on WC.ID = W.WelcomeCallStatusID
Group by
	L.[DealerID]