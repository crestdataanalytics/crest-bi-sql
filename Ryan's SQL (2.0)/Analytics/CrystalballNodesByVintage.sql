Select
	CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
	CB.CBNode,
	Count(CB.CBNode) as CountofNode
FROM
	Leases as L left outer join
	(
	select top 1000 *
			--pad.SubjectID
			--,max(case when padv.VariableID = 45994 then padv.VariableValue else null end) as 'CBNODE'
	from [ProviderAnalysis].dbo.DocumentInfos as pad 
		join [ProviderAnalysis].dbo.DocumentVariables as padv on
		pad.DocumentID = padv.DocumentID
		join [ProviderAnalysis].dbo.Variables as pav on
		padv.VariableID = pav.VariableID
		join [ProviderAnalysis].dbo.Providers as pap on
		pad.providerID = pap.providerId
	where 
		--pav.VariableName like '%cra%' and
		ProviderName = 'Crystalball'
		--pav.VariableID = 45997
	GROUP BY
		pad.subjectID
	) as CB on CB.SubjectID = L.[Customer ID]
WHERE
	L.[Funded Date] > '7-1-16'
GROUP BY
	CONVERT(CHAR(7),L.[Funded Date],120),
	CB.CBNode


