SELECT
	R.[Category],
	L.[Retailer ID],
	L.[Lease ID],
	L.[Customer ID],
	M.Zipcode,
	isnull(nullif(M.[Median Income],0)/12,0) as 'MedianMonthlyIncome',
	C.[Monthly Income] as CustomerIncome,
	isnull(nullif(C.[Monthly Income],0)/nullif(M.[Median Income]/12,0),0) as 'PercentOfMedian',
	L.LeaseQueueKey,
	(L.[Total Paid]/nullif(L.[Funded Amount],0)) as Multiple,
	(L.[Funded Amount]/L.[Approval Amount]) as Utilization,
	case when [Auto Approved] = 1 then 'Yes' else 'No' end as AutoApproved,
	case when [Met First Scheduled Payment] = 'No' then 'Yes' else 'No' end as FPD
FROM
	medianIncome2014 as M join [CFREPORTS].[Crest Business Intelligence].[dbo].[Customers] as C on M.Zipcode = C.Zip
		join [CFREPORTS].[Crest Business Intelligence].[dbo].[Leases] as L on L.[Customer ID] = C.[Customer ID]
		join [CFREPORTS].[Crest Business Intelligence].[dbo].[Retailers] as R on R.[Retailer ID] = L.[Retailer ID]
WHERE
	--L.[Funded Date] > '7-1-14' and L.[Funded Date] < '7-1-15'
	--and C.[Monthly Income]/(M.[Median Income]/12) >= 2
	R.[Retailer ID] = 10696
	and M.[Median Income] > 0
	and C.[Monthly Income] > 0
	and L.[Funded Amount] > 0