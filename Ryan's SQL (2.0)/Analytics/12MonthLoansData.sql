SELECT
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	L.ID,
	L.[DisplayID],
	L.ApplicantID,
	L.[FundedAmount],
	L.[StatusID],
	T.M1,
	T.M2,
	T.M3,
	T.M4,
	T.M5,
	T.M6,
	T.M7,
	T.M8,
	T.M9,
	T.M10,
	T.M11,
	T.M12,
	Case when T.[CC1] >=0 then 'Yes' else 'No' end as 'CC1 on Track',
	Case when T.[CC2] >=0 then 'Yes' else 'No' end as 'CC2 on Track',
	Case when T.[CC3] >=0 then 'Yes' else 'No' end as 'CC3 on Track',
	Case when T.[CC4] >=0 then 'Yes' else 'No' end as 'CC4 on Track',
	Case when T.[CC5] >=0 then 'Yes' else 'No' end as 'CC5 on Track',
	Case when T.[CC6] >=0 then 'Yes' else 'No' end as 'CC6 on Track',
	Case when T.[CC7] >=0 then 'Yes' else 'No' end as 'CC7 on Track',
	Case when T.[CC8] >=0 then 'Yes' else 'No' end as 'CC8 on Track',
	Case when T.[CC9] >=0 then 'Yes' else 'No' end as 'CC9 on Track',
	Case when T.[CC10] >=0 then 'Yes' else 'No' end as 'CC10 on Track',
	Case when T.[CC11] >=0 then 'Yes' else 'No' end as 'CC11 on Track',
	Case when T.[CC12] >=0 then 'Yes' else 'No' end as 'CC12 on Track',
	T.[FM Payments Cleared],
	T.[FM Payments Returned]
from vw_loans_reporting as L
	left outer join 
		(SELECT
			LPP.LoanID, 
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M1,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M2,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M3,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M4,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 5 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M5,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 6 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M6,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 7 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M7,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 8 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M8,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 9 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M9,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 10 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M10,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 11 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M11,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 12 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M12,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) as FMM,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) - 0.25 as CC1,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.44 as CC2,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.62 as CC3,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.71 as CC4,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 5 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.79 as CC5,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 6 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.88 as CC6,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 7 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.95 as CC7,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 8 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 1.02 as CC8,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 9 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 1.09 as CC9,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 10 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 1.15 as CC10,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 11 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 1.21 as CC11,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 12 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 1.25 as CC12,
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 12 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'FM Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 12 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'FM Payments Cleared'
            FROM LoanPayments as LPP left join vw_loans_reporting as L on LPP.LoanID = L.ID
			GROUP BY 
			LPP.LoanID,
			L.[FundedAmount]
                ) as T on L.ID = T.LoanID
WHERE L.[FundedDate] between '12-1-13' and '7-1-15' --and [Total Paid] = 0
Group by
	CONVERT(CHAR(7),[FundedDate],120),
	L.ID,
	l.ApplicantID,
	l.[DisplayID],
	L.[StatusID],
	L.[FundedAmount],
	l.[FundedDate],
	l.id,
	T.M1,
	T.M2,
	T.M3,
	T.M4,
	T.M5,
	T.M6,
	T.M7,
	T.M8,
	T.M9,
	T.M10,
	T.M11,
	T.M12,
	T.CC1,
	T.CC2,
	T.CC3,
	T.CC4,
	T.CC5,
	T.CC6,
	T.CC7,
	T.CC8,
	T.CC9,
	T.CC10,
	T.CC11,
	T.CC12,
	T.[FM Payments Cleared],
	T.[FM Payments Returned]
order by
	CONVERT(CHAR(7),[FundedDate],120) asc