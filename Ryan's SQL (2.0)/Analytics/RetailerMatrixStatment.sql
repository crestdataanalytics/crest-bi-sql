select
	R.[Retailer Name],
	R.[Retailer ID],
	R.[State],
	sum(case when datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end) as '3 mo. Funded',
	isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end),0)*1.0/
		nullif(sum(case when [Met First Scheduled Payment] in ('No','Yes','Partially') and datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. FPD $',
	isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end),0)*1.0/
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
	isnull(nullif(count(case when [LeaseQueueKey] = 6 then 1 else null end),0)*1.0/
		nullif(count(case when [Close Date] is null then 1 else null end),0),0) as '3 mo. Bounced %',
	isnull(nullif(count(case when [LeaseQueueKey] = 12 and datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0)*1.0/
		nullif(count(case when datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0),0) as '3 mo. Charge-Off %',
	isnull(nullif(count(case when [LeaseQueueKey] = 5 and datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0)*1.0/
		nullif(count(case when datediff(dd,[Close Date],getdate()) < 90 then 1 else null end),0),0) as '3 mo. Paid %',
	isnull(nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Total Paid] else 0 end),0)*1.0/
		nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. Closing Multiple',
	isnull(nullif(sum(case when [Close Date] is not null then [Total Paid] else 0 end),0)*1.0/
		nullif(sum(case when [Close Date] is not null then [Funded Amount] else 0 end),0),0) as 'Overall Closing Multiple',
	isnull(nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Total Paid] else 0 end),0)*1.0/
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Funded Amount] else 0 end),0),0) as 'One Year Multiple',
	count(case when datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end) as '3 mo Volume',
	count(case when datediff(mm,[Funded Date],getdate()) <= 6 then 1 else null end) as '6 mo Volume',
	count(case when datediff(mm,[Funded Date],getdate()) <= 12 then 1 else null end) as '12 mo Volume',
	isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
	isnull(nullif(count(case when datediff(mm,[Submitted Date],getdate()) <= 3 and [LeaseQueueKey] not in (7,11) then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Submitted Date],getdate()) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate',
	(isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end),0)*1.0/
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0))-
	 (isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0)) as 'Quarterly FPD vs Volume %'

from
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
where
	R.[Current Status] in ('Active') --and R.[Retailer Name] like '%gardner%'
group by
	R.[Retailer Name], R.[Retailer ID], R.[State]
order by
	[3 mo. Funded] desc,
	[One Year Multiple] asc,
	r.[Retailer Name]
	