SELECT
	distinct DG.DealerGroupName,
	FundingMatrixCategoryID
FROM
	Dealers as D join DealerGroups as DG on D.DealerGroupID = DG.ID
WHERE
	FundingMatrixCategoryID is not null


SELECT
	C.CategoryName,
	count(case when L.ApprovalAmount = D.LoanLimit then 1 else null end)*1.0/count(L.ApprovalAmount)
FROM
	Dealers as D join DealerCategories as DC on D.ID = DC.DealerID
	join Loans as L on L.DealerID = D.ID
	join Categories as C on C.ID = DC.CategoryID
WHERE
	L.CreationDate > '8-1-16'
	and D.IsDemoAccount = 0
GROUP BY
	C.CategoryName
ORDER BY
	count(case when L.ApprovalAmount = D.LoanLimit then 1 else null end)*1.0/count(L.ApprovalAmount) desc
	
