SELECT
	C.[Customer ID],
	C.[Age],
	C.[Monthly Income] as MonthlyIncome,
	coalesce(CR.RepeatCust,'No'),
	L.[Funded Amount],
	L.[Funded Date],
	L.[Total Paid]/nullif(L.[Funded Amount],0) as Multiple,
	LQ.[Queue Name] as LeaseQueue,
	R.Region,
	R.Category,
	PD.GlobalResult,
	PD.BAVCode,
	PD.BAVPass,
	PD.IDVCode,
	PD.IDVResult,
	PD.DR,
	PD.CRABucket,
	PD.IDVBucket,
	PD.NASSummary,
	PD.NAPSummary,
	PD.FollowUpOne,
	PD.FollowUpTwo
FROM
	Customers as C join Leases as L on C.[Customer ID] = L.[Customer ID]
	join Retailers as R on R.[Retailer ID] = L.[Retailer ID]
	join [Lease Queues] as LQ on LQ.LeaseQueueKey = L.LeaseQueueKey
	left outer join
	(
	select
		pad.SubjectID
		,max(case when padv.VariableID = 7266 then padv.VariableValue else null end) as 'GlobalResult'
		,max(case when padv.VariableID = 6250 then padv.VariableValue else null end) as 'BAVCode'
		,max(case when padv.VariableID = 6342 then padv.VariableValue else null end) as 'BAVPass'
		,max(case when padv.VariableID = 7765 then padv.VariableValue else null end) as 'IDVCode'
		,max(case when padv.VariableID = 7216 then padv.VariableValue else null end) as 'IDVResult'
		,max(case when padv.VariableID = 8164 then padv.VariableValue else null end) as 'DR'
		,max(case when padv.VariableID = 7214 then padv.VariableValue else null end) as 'CRABucket'
		,max(case when padv.VariableID = 7910 then padv.VariableValue else null end) as 'IDVBucket'
		,max(case when padv.VariableID = 7483 then padv.VariableValue else null end) as 'NASSummary'
		,max(case when padv.VariableID = 7442 then padv.VariableValue else null end) as 'NAPSummary'
		,max(case when padv.VariableID = 7715 then padv.VariableValue else null end) as 'FollowUpOne'
		,max(case when padv.VariableID = 7768 then padv.VariableValue else null end) as 'FollowUpTwo'
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			 join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			 join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			 join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
	where 
		    pap.ProviderName = 'DataX'
	group by
			pad.SubjectID
	) as PD on PD.SubjectID = C.[Customer ID]
	left outer join
	(
	SELECT
		[Customer ID],
		--count([Customer ID]),
		case when count(*) > 1 then 'Yes' else 'No' end as RepeatCust
	FROM
		(	select 
			pad.[Customer ID],
			L.[Retailer ID],
			CONVERT(CHAR(7),L.[Submitted Date],120) as 'Funded Date'
			--into ##CustomerRepeat
		from 
			[Crest Business Intelligence].dbo.Customers as pad
			join [Crest Business Intelligence].dbo.Leases as L on pad.[Customer ID] = L.[Customer ID]
		Where 
			[LeaseQueueKey] in (4,5,6,12,16)) as RC
	GROUP BY
		[Customer ID]
	) as CR on C.[Customer ID] = CR.[Customer ID]
WHERE
	datediff(mm,L.[Funded Date],getdate()) < 6
	and L.LeaseQueueKey not in (21)
	--and PD.GlobalResult is not null
