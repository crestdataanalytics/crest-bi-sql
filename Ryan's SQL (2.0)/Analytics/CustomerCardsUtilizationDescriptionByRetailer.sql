SELECT
	L.ApplicantID,
	AD.StateID,
	cast(L.FundedDate as date) as FundingDate,
	L.StatusID,
	L.Amount,
	L.ApprovalAmount,
	cast((L.Amount*1.0)/L.ApprovalAmount as decimal(18,2)) as Utilization,
	LI.[Description],
	count(distinct LP.PaymentMethodLastFour) as CreditCards,
	count(distinct case when LP.ID is not null then LP.ReturnDate else null end) as PaymentsReturned,
	count(distinct N.ID) as Notes,
	--UAT.IpAddress,
	--uat.browser,
	LST.Ipaddress,
	LSTT.[Type]
FROM Loans as L left outer join LoanPayments as LP on L.ID = LP.LoanID
	left outer join LoanItems as LI on LI.LoanID = L.ID
	left outer join Notes as N on N.LoanID = L.ID
	left outer join UserApplicant as UA on UA.ApplicantID = L.ApplicantID
	--left outer join UserAuditTrail UAT on UAT.UserID = UA.UserID
	left outer join LoanSubmissionTransactions as LST on LST.LoanID = L.ID
	left outer join LoanSubmissionTypes as LSTT on LSTT.LoanSubmissionTypeID = LST.LoanSubmissionType
	left outer join Applicants as A on A.ID = L.ApplicantID
	left outer join Addresses as AD on AD.ID = A.AddressID
WHERE
	L.FundedAmount > 0
	--and D.Name like '%aamco%'
	and L.DealerID = 5269
	--and LP.TypeID in (7,9,8,2)
GROUP BY
	L.ApplicantID,
	AD.StateID,
	L.DealerID,
	L.StatusID,
	cast(L.FundedDate as date),
	L.Amount,
	L.ApprovalAmount,
	(L.Amount*1.0)/L.ApprovalAmount,
	LI.[Description],
	--uat.ipaddress,
	--uat.browser,
	LST.Ipaddress,
	LSTT.[Type]
ORDER BY
	cast(L.FundedDate as date) asc

/*
	SELECT c.name AS ColName, t.name AS TableName
FROM sys.columns c
    JOIN sys.tables t ON c.object_id = t.object_id
WHERE c.name LIKE '%ipaddress%'
*/


	
