SELECT
	L.ID,
	L.ApplicantID,
	AJ.MonthlyIncome,
	AD.PostalCode,
	AD.City,
	AD.StateID,
	L.DealerID,
	D.Name,
	C.CategoryName,
	D.RiskScore as BIRank,
	D.InitialPaymentRequired,
	LS.[Status],
	L.ApprovalAmount,
	L.QualifiedAmount,
	L.Amount as Funded,
	case when L.AutoApprovedFlag = 1 and L.StatusID not in (7,11) then 'Auto' 
		when L.AutoApprovedFlag = 0 and L.StatusID not in (7,11) then 'Manual' 
		else 'Denied' end as ApprovalPath,
	cast(L.CreationDate as date) as SubmitDate,
	cast(L.CreationDate as time) as SubmitTime,
	L.AutoApprovedFlag as AutoApproved
FROM
	Loans as L join LoanStatuses as LS on L.StatusID = LS.ID
	left outer join Dealers as D on D.ID = L.DealerID
	left outer join DealerCategories as DC on DC.DealerID = D.ID
	left outer join Categories as C on C.ID = DC.CategoryID
	left outer join ApplicantJob as AJ on AJ.ApplicantID = L.ApplicantID
	left outer join Applicants as A on A.ID = L.ApplicantID
	left outer join Addresses as AD on AD.ID = A.AddressID
WHERE
	cast(L.CreationDate as date) > '12-31-15'
	--and L.StatusID = 7
