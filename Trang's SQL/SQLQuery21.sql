/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ID]
      ,[IsApplicantNameMatch]
      ,[StartBalance]
      ,[EndBalance]
      ,[HistoryDays]
      ,[NegativeBalanceDays]
      ,[MinimumDeposit]
      ,[CashAdvance]
      ,[GarnishPercent]
      ,[NSF]
      ,[Returned]
      ,[Fees]
      ,[MonthlyIncome]
      ,[DaysBalUnder]
  FROM [DealerManage].[dbo].[LoanRecommendationLimits]


  