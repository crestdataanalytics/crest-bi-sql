-- loan payment

-- 1. count only
SELECT *
FROM [DealerManage].dbo.[FirstScheduledPaymentDetails]
WHERE  datepart(year,firstscheduledpaymentduedate) = 2016

-- 2. count only FPD
SELECT COUNT(*)
FROM [DealerManage].dbo.[FirstScheduledPaymentDetails]
where iscurrentoutcome = 1
and FirstScheduledPaymentOutcomeID in (3,4,5, 7, 8)
and datepart(year,firstscheduledpaymentduedate) = 2016

-- 3. column details for FPD leases
SELECT *
FROM [DealerManage].dbo.[FirstScheduledPaymentDetails]
where iscurrentoutcome = 1
and FirstScheduledPaymentOutcomeID in (3,4,5, 7, 8)
and datepart(year,firstscheduledpaymentduedate) = 2016
order by [FirstScheduledPaymentOutcomeID] desc

-- New
SELECT *
FROM [DealerManage].dbo.[FirstScheduledPaymentOutcome]

-- FPD Funded Dollar
SELECT
	Datepart(month,firstscheduledpaymentduedate) as vintage,
	sum(case when firstscheduledpaymentoutcomeid in (3,4) then L.FundedAmount else 0 end)*1.0/sum(L.FundedAmount) as FPD
FROM [DealerManage].dbo.[FirstScheduledPaymentDetails] as FSPD
join Loans as L on L.ID = FSPD.LoanID
where iscurrentoutcome = 1
and FirstScheduledPaymentOutcomeID in (3,4,5,7,8)
and datepart(year,firstscheduledpaymentduedate) = 2016
group by
Datepart(month,firstscheduledpaymentduedate) 



