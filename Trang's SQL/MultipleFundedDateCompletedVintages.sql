SELECT
	L.[Lease ID] as LoanID,
	
	L.[Total Paid]/nullif(L.[Funded Amount],0) as Multiple,
	L.[Funded Date] as FundedDate,
	L.[LeaseQueuekey] as LeaseStatus
FROM
	Leases as L
WHERE
	L.LeaseQueueKey in (5,12,13,14)
	and L.[Funded Date] > '12-31-14'