SELECT
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	L.ID,
	L.[DisplayID],
	L.ApplicantID,
	L.[FundedAmount],
	L.[StatusID],
	T.M1,
	T.M2,
	T.M3,
	T.M4,
	Case when T.[CC1] >=0 then 'Yes' else 'No' end as 'CC1 on Track',
	Case when T.[CC2] >=0 then 'Yes' else 'No' end as 'CC2 on Track',
	Case when T.[CC3] >=0 then 'Yes' else 'No' end as 'CC3 on Track',
	Case when T.[CC4] >=0 then 'Yes' else 'No' end as 'CC4 on Track',
	T.[M1 Payments Cleared],
	T.[M1 Payments Returned],
	T.[M2 Payments Cleared],
	T.[M2 Payments Returned],
	T.[M3 Payments Cleared],
	T.[M3 Payments Returned],
	T.[M4 Payments Cleared],
	T.[M4 Payments Returned]
from vw_loans_reporting as L
	left outer join 
		(SELECT
			LPP.LoanID, 
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M1,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M2,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M3,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M4,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) as FMM,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) - 0.25 as CC1,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.44 as CC2,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.62 as CC3,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.71 as CC4,	
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M1 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M1 Payments Cleared',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M2 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M2 Payments Cleared',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M3 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M3 Payments Cleared',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M4 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M4 Payments Cleared'
            FROM LoanPayments as LPP left join vw_loans_reporting as L on LPP.LoanID = L.ID
			GROUP BY 
			LPP.LoanID,
			L.[FundedAmount]
                ) as T on L.ID = T.LoanID
WHERE L.[FundedDate] between '12-1-13' and '9-1-16' --and [Total Paid] = 0
Group by
	CONVERT(CHAR(7),[FundedDate],120),
	L.ID,
	l.ApplicantID,
	l.[DisplayID],
	L.[StatusID],
	L.[FundedAmount],
	l.[FundedDate],
	l.id,
	T.M1,
	T.M2,
	T.M3,
	T.M4,
	T.CC1,
	T.CC2,
	T.CC3,
	T.CC4,
	T.[M1 Payments Cleared],
	T.[M1 Payments Returned],
	T.[M2 Payments Cleared],
	T.[M2 Payments Returned],
	T.[M3 Payments Cleared],
	T.[M3 Payments Returned],
	T.[M4 Payments Cleared],
	T.[M4 Payments Returned]
order by
	CONVERT(CHAR(7),[FundedDate],120) asc