SELECT *, 
               CASE 
                 WHEN fpd = 'Yes' THEN [funded amount] 
                 ELSE 0 
               END AS FPDAmount, 
               CASE 
                 WHEN fpd = 'Yes' 
                      AND Cast(newapproval AS INT) > 500 THEN 
                   CASE 
                     WHEN [funded amount] > newapproval THEN newapproval 
                     ELSE [funded amount] 
                   END 
                 ELSE 0 
               END AS FPD_CB_Amount, 
               CASE 
                 WHEN Cast(newapproval AS INT) > 500 THEN 1 
                 ELSE 0 
               END AS CBApproved, 
               CASE 
                 WHEN Cast(originalapproval AS INT) > 500 THEN 1 
                 ELSE 0 
               END AS OriginalApproved 
        FROM   (SELECT l.[lease id], 
                       l.[funded amount], 
                       l.[funded date], 
                       l.[submitted date] 
                       AS 
                       AppDate, 
                       l.[leasequeuekey], 
                       cu.[monthly income] 
                       AS 
                               CustomerIncome, 
                       CASE 
                         WHEN leasequeuekey NOT IN ( 7, 11 ) THEN 
                         l.[approval amount] 
                         ELSE 0 
                       END 
                       AS 
                               OriginalApproval, 
                       CASE 
                         WHEN Cast(c.amount AS DECIMAL(18, 2)) >= 500 
                              AND leasequeuekey NOT IN ( 11 ) THEN Cast( 
                         c.amount AS DECIMAL(18, 
                                     2)) 
                         ELSE 0 
                       END 
                       AS 
                       CBApproval, 
                       Cast(c.score AS INT) 
                       AS 
                       CBScore, 
                       CASE 
                         WHEN l.[met first scheduled payment] IN ( 'No' ) 
                              AND [funded amount] > 0 THEN 'Yes' 
                         WHEN l.[met first scheduled payment] IN ( 'Yes', 
                              'Partially' ) 
                              AND [funded amount] > 0 THEN 'No' 
                         ELSE 'N/A' 
                       END 
                       AS FPD, 
                       CASE 
                         WHEN l.[funded amount] > 0 
                              AND l.[approval amount] > 0 THEN 
                         l.[funded amount] / l.[approval amount] 
                       END 
                       AS 
                       Utilization, 
                       Cast(c.[amount] AS DECIMAL(18, 2)) - l.[approval amount] 
                       AS 
                               'ApprovalDiff', 
                       r.[retailer name] 
                       AS 
                       RetailerName 
                               , 
                       r.[retailer id] 
                               AS RetailerID, 
                       r.[category] 
                       AS 
                       Category, 
                       c.node, 
                       Cast(c.score AS DECIMAL(18, 2)) / 100 
                       AS 
                       scores, 
                       CASE 
                         WHEN C.node = 1 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 1 ) 
                         WHEN C.node = 2 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 1 ) 
                         WHEN C.node = 3 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 1 ) 
                         WHEN C.node = 4 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 0.1 ) 
                         WHEN C.node = 5 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 0.35 ) 
                         WHEN C.node = 6 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 1 ) 
                         WHEN C.node = 7 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 
                                              0.150982546645711 ) 
                         WHEN C.node = 8 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 
                                              0.471963365096107 ) 
                         WHEN C.node = 9 THEN ( ( Cast(C.amount AS DECIMAL(18, 2 
                                                                   )) ) / 
                                                NULLIF(( 
                                                                       Cast( 
                                                C.score AS DECIMAL(18, 2)) 
                                                                       / 100 ), 
                                                0) ) * 
                                              ( 1 ) 
                         WHEN C.node = 10 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.287043797236731 ) 
                         WHEN C.node = 11 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.1 ) 
                         WHEN C.node = 12 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.130035208829801 ) 
                         WHEN C.node = 13 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.1 ) 
                         WHEN C.node = 14 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.2 ) 
                         WHEN C.node = 15 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.594762646498347 ) 
                         WHEN C.node = 16 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 1 
                                               ) 
                         WHEN C.node = 17 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.308212808911958 ) 
                         WHEN C.node = 18 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.955781788019614 ) 
                         WHEN C.node = 19 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 1 
                                               ) 
                         WHEN C.node = 20 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.999999340563454 ) 
                         WHEN C.node = 21 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 1 
                                               ) 
                         WHEN C.node = 22 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 1 
                                               ) 
                         WHEN C.node = 23 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 1 
                                               ) 
                         WHEN C.node = 24 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.977772782923511 ) 
                         WHEN C.node = 25 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.1 ) 
                         WHEN C.node = 26 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.926665619258837 ) 
                         WHEN C.node = 27 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.985545593095021 ) 
                         WHEN C.node = 28 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.161530017310857 ) 
                         WHEN C.node = 29 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.113451769958892 ) 
                         WHEN C.node = 30 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.883012817317812 ) 
                         WHEN C.node = 31 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.135492077639314 ) 
                         WHEN C.node = 32 THEN ( ( Cast(C.amount AS DECIMAL(18, 
                                                                    2)) ) / 
                                                 NULLIF( 
                                                 ( 
                                                                         Cast( 
                                                 C.score AS DECIMAL(18, 2) 
                                                                         ) / 100 
                                                 ), 0) 
                                               ) * ( 
                                               0.119556856417176 ) 
                       END 
                       AS 
                       NewApproval 
                FROM   leases AS l 
                       LEFT OUTER JOIN (SELECT DI.subjectid AS 'CustomerID', 
                                                Max(CASE 
                                                      WHEN dv.[variableid] = 
                                                           45993 THEN 
                                                      dv.[variablevalue] 
                                                    END)     AS 'Result', 
                                                Max(CASE 
                                                      WHEN dv.[variableid] = 
                                                           45994 THEN 
                                                      dv.[variablevalue] 
                                                    END)     AS 'Node', 
                                                Max(CASE 
                                                      WHEN dv.[variableid] = 
                                                           45995 THEN 
                                                      dv.[variablevalue] 
                                                    END)     AS 'Factor', 
                                                Max(CASE 
                                                      WHEN dv.[variableid] = 
                                                           45996 THEN 
                                                      dv.[variablevalue] 
                                                    END)     AS 'Score', 
                                                Max(CASE 
                                                      WHEN dv.[variableid] = 
                                                           45997 THEN 
                                                      dv.[variablevalue] 
                                                    END)     AS 'Amount' 
                       INNER JOIN provideranalysis.dbo.variables AS 
                            V 
                                         FROM 
                       provideranalysis.dbo.documentvariables 
                       AS DV 
                       INNER JOIN provideranalysis.dbo.documentinfos 
                            AS DI 
                         ON DV.documentid = DI.documentid 



					   --WHERE  [submitted date] >= '9-21-16' 
                       INNER JOIN provideranalysis.dbo.providers AS 
                         ON P.providerid = DI.providerid 
                         ON V.variableid = DV.variableid 
                                         WHERE  p.providerid = 14 
                            P 
                                         GROUP  BY di.subjectid) AS c 
                                     ON c.customerid = L.[customer id] 
                       INNER JOIN retailers AS r 
                         ON r.[retailer id] = l.[retailer id] 
                       INNER JOIN customers AS cu 
                         ON cu.[customer id] = l.[customer id] 
                WHERE  [submitted date] > '12-31-2015' 
                       -- AND leasequeuekey NOT IN ( 11 )
					   ) AS Data