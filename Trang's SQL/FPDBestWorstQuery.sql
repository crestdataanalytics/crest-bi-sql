SELECT
	  L.ID as LoanID
	  ,L.StatusID
	  ,C.CategoryName as Category
	  ,L.CoApplicantID
	  ,cast(L.[FundedDate] as date) as FundedDate
	  ,cast([FirstScheduledPaymentDueDate] as date) as FirstScheduledPaymentDueDate
	  ,DATEDIFF(day, L.[FundedDate],[FirstScheduledPaymentDueDate]) as DiffDate
	  ,PPT.[Period] as PayPeriod,[EvaluationDate]
	   ,L.FundedAmount
       ,FSP.[FirstScheduledAmount],
	  [NonInitialAmountPaidToDate]
      ,[FirstScheduledPaymentOutcomeID]
      ,[IsCurrentOutcome]
	  , RiskScore,
	case FSP.FirstScheduledPaymentOutcomeID
		when 3 then 'Yes'
		when 4  then 'No'
		when 5  then 'No'
		when 1 then 'N/A'
		when 2 then 'N/A'
		when 6 then 'N/A'
		when 7 then 'N/A'
		when 8 then 'N/A'
		end as FPDBest,
	case FSP.FirstScheduledPaymentOutcomeID
		when 3 then 'Yes'
		when 4  then 'Yes'
		when 5  then 'No'
		when 1 then 'N/A'
		when 2 then 'N/A'
		when 6 then 'N/A'
		when 7 then 'N/A'
		when 8 then 'N/A'
		end as FPDWorst
FROM
	Loans as L left outer join ApplicantJob as AJ on L.ApplicantID = AJ.ApplicantID
	left outer join FirstScheduledPaymentDetails as FSP on FSP.LoanID = L.ID and 
	 FSP.IsCurrentOutcome = 1
	left outer join Dealers as D on D.ID = L.DealerID
	left outer join PayPeriodTypes as PPT on PPT.ID = AJ.PayPeriodTypeID
	left outer join DealerCategories as DC on D.ID = DC.DealerID
	left outer join Categories as C on C.ID = DC.CategoryID
WHERE
	L.StatusID not in (21)
    and L.StatusID in (4, 5, 6, 12,13,14) --4: Funded/Active, 6: Bounced, 
	and L.FundedDate >  '12-31-15'
	and D.IsDemoAccount = 0
	--and L.ID = 1237078

--	select * from FIrstScheduledPaymentdetails where loanid = 1237078

/* Ryan's Original
SELECT
	L.ID as LoanID,
	PPT.[Period] as PayPeriod,
	case when FSP.FirstScheduledPaymentOutcomeID = 3 then 'Yes'
		when FSP.FirstScheduledPaymentOutcomeID in (4,5) then 'No'
		when FSP.FirstScheduledPaymentOutcomeID in (1,2,6,7,8) then 'N/A'
	end as FPDBest,
	case 
		when FSP.FirstScheduledPaymentOutcomeID in (3,4) then 'Yes'
		when FSP.FirstScheduledPaymentOutcomeID in (5) then 'No'
		when FSP.FirstScheduledPaymentOutcomeID in (1,2,6,7,8) then 'N/A'
		end as FPDWorst
FROM
	Loans as L join ApplicantJob as AJ on L.ApplicantID = AJ.ApplicantID
	join FirstScheduledPaymentDetails as FSP on FSP.LoanID = L.ID and FSP.IsCurrentOutcome = 1
	join Dealers as D on D.ID = L.DealerID
	join PayPeriodTypes as PPT on PPT.ID = AJ.PayPeriodTypeID
WHERE
	L.StatusID not in (21)
	and L.FundedDate > '12-31-14'
	and D.IsDemoAccount = 0


*/