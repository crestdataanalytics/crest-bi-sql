
Select
Vintage,
sum(TotalPaid)/Sum(Funded) as CurrentMultiple,
(sum(case when PredictedPaid is not null then PredictedPaid else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMultiple,
(sum(case when PredictedPaid is not null then (PredictedPaid)*1.10 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMidPoint,
(sum(case when PredictedPaid is not null then (PredictedPaid)*1.20 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedUpperLimit,
sum(PredictedPaid)/(sum(TotalPaid)+sum(PredictedPaid)) as PercentPredictedAmount,
convert(numeric(14,2),count(PredictedPaid))/count(*) as PercentPredictedLeases,
Sum(Funded) as TotalFunded
--Count(*) as TotalLeases
from
	(
	select 
		[Lease ID],
		TotalPaid,
		Funded,
		Vintage,
		case when TotalPaid>PredictedPaid then TotalPaid else PredictedPaid end as PredictedPaid
		from
		(
			SELECT
				L.[Lease ID],
				LeaseQueueKey,
				[Approval Amount],
				CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
				case 
					--// Dynamic
					when LeaseQueueKey in (4,6) and ((L.[Payments Returned] >= 3.5 and [Total Paid] < 240 and L.[Payments Cleared] >=18) or (L.[Payments Returned] >=3.5 and L.[Payments Cleared] >= 18) or (L.[Payments Returned] > 8.5)) then 
						(0.55*[Funded Amount])
					when LeaseQueueKey in (4,6) and [Total Paid] < 10 then
						(0.015*[Funded Amount])
					when LeaseQueueKey in (4,6) then
					((case when datediff(mm,[Funded Date],getdate()) = 4 then 0.90620
								when datediff(mm,[Funded Date],getdate()) = 5 then 1.062603
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.210379
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.350049
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.481444
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.605123
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.724040
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.829727
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.907534 end)
					+((-0.015851)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.027979
						when C.[Monthly Income] between 3001 and 4500 then (-0.038214/2)*2
						when C.[Monthly Income] > 4500 then (-0.047496/3)*3 else 0 end)
					+(case when C.Age between 18 and 28 then -0.007051
						when C.Age between 29 and 36 then (-0.002978/2)*2
						when C.Age > 36 then (0.004253/3)*3 else 0 end)
					+((-0.208059)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.208059-0.255615*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12 - datediff(mm,[Funded Date],getdate()))*0.124))
					*[Funded Amount]
					end as PredictedPaid,
					[Total Paid] as TotalPaid,
					[Funded Amount] as Funded,
					[Total Paid]/[Funded Amount] as CurrentMultiple
				--count(case when LeaseQueueKey in (4,6) then 1 else null end) as 'Current Leases'
			FROM
				Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
					join Customers as C on C.[Customer ID] = L.[Customer ID]
			WHERE
				[Funded Amount] > 0 and [Funded Date] > '1-1-15'
				and R.[Retailer ID]=9038 
				and datediff(mm,[Funded Date],getdate()) between 4 and 16
				and LeaseQueueKey<>21
		) as ActiveLeases
	) as PredictedLeases

group by Vintage
order by Vintage

