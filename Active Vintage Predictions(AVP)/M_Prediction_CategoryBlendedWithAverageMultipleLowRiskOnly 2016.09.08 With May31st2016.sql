SELECT [Lease ID],
       TotalPaid,
       Funded,
       Vintage,
       Category,
       LowRisk,
       HistoricalPaid,
       [Total Note],
       CASE
           WHEN PredictedPaid IS NOT NULL
           THEN PredictedPaid
           ELSE 0
       END + CASE
                 WHEN PredictedPaid IS NULL
                 THEN TotalPaid
                 ELSE 0
             END AS PredictedPaidLow,
       CASE
           WHEN PredictedPaid IS NOT NULL
           THEN CASE
                    WHEN LowRisk = 0
                    THEN PredictedPaid * 1.10
                    ELSE PredictedPaid
                END
           ELSE 0
       END + CASE
                 WHEN PredictedPaid IS NULL
                 THEN TotalPaid
                 ELSE 0
             END AS PredictedPaidHigh
FROM
(
    SELECT [Lease ID],
           TotalPaid,
           Funded,
           Vintage,
           Category,
           LowRisk,
           HistoricalPaid,
           [Total Note],
           CASE
               WHEN TotalPaid > PredictedPaid
               THEN TotalPaid
               WHEN LowRisk = 1
                    AND PredictedPaid IS NOT NULL
               THEN(HistoricalPaid + PredictedPaid) / 2
               ELSE PredictedPaid
           END AS PredictedPaid
    FROM
    (
        SELECT L.[Lease ID],
               LeaseQueueKey,
               [Approval Amount],
               R.Category,
               L.[Total Note],
               CONVERT( CHAR(7), [Funded Date], 120) AS 'Vintage',
               CASE 
                                                        --// Dynamic
                   WHEN LeaseQueueKey IN(4, 6)
               AND ((L.[Payments Returned] > = 3.5
                     AND [Total Paid] < 240
                     AND L.[Payments Cleared] >= 18)
                    OR (L.[Payments Returned] >= 3.5
                        AND L.[Payments Cleared] >= 18)
                    OR (L.[Payments Returned] > 8.5))
                   THEN 0
                   WHEN LeaseQueueKey IN(4, 6)
        AND [Total Paid] < 10
                   THEN 0
                   WHEN [Total Paid] > [Funded Amount] * 1.5
                   THEN --//Need to validate correct multiple cutoff!!!!!
        1
                   ELSE 0
               END AS LowRisk,
               CASE 
                      --// Dynamic
                   WHEN LeaseQueueKey IN(4, 6)
    AND ((L.[Payments Returned] >= 3.5
          AND [Total Paid] < 240
          AND L.[Payments Cleared] >= 18)
         OR (L.[Payments Returned] >= 3.5
             AND L.[Payments Cleared] >= 18)
         OR (L.[Payments Returned] > 8.5))
                   THEN(CASE
                            WHEN R.Category = 'Appliance/Electronics/Video'
                            THEN(0.47 * [Funded Amount])
                            WHEN R.Category = 'Furniture'
                            THEN(0.57 * [Funded Amount])
                            WHEN R.Category = 'Jewelry'
                            THEN(0.53 * [Funded Amount])
                            WHEN R.Category = 'Mattress'
                            THEN(0.62 * [Funded Amount])
                            ELSE(0.50 * [Funded Amount])
                        END)
                   WHEN LeaseQueueKey IN(4, 6)
    AND [Total Paid] < 10
                   THEN(0.015 * [Funded Amount])
                   WHEN LeaseQueueKey IN(4, 6)
                   THEN(CASE
                            WHEN R.Category = 'Jewelry'
                            THEN((CASE
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 4
                                      THEN 0.90199
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 5
                                      THEN 1.05685
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 6
                                      THEN 1.19894
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 7
                                      THEN 1.33537
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 8
                                      THEN 1.459269
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 9
                                      THEN 1.57665
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 10
                                      THEN 1.68803
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 11
                                      THEN 1.78803
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 12
                                      THEN 1.861851
                                  END)+((-0.079889)*(CASE
                                                         WHEN L.[Met First Scheduled Payment] IN('No')
                                                         THEN 1
                                                         ELSE 0
                                                     END))+(CASE
                                                                WHEN C.[Monthly Income] BETWEEN 2200 AND 3000
                                                                THEN-0.027558
                                                                WHEN C.[Monthly Income] BETWEEN 3001 AND 4500
                                                                THEN(-0.033916 / 2) * 2
                                                                WHEN C.[Monthly Income] > 4500
                                                                THEN(-0.0586755 / 3) * 3
                                                                ELSE 0
                                                            END)+(CASE
                                                                      WHEN C.Age BETWEEN 18 AND 25
                                                                      THEN-0.02054755
                                                                      WHEN C.Age BETWEEN 26 AND 35
                                                                      THEN(-0.01229667 / 2) * 2
                                                                      WHEN C.Age > 35
                                                                      THEN(0.0295560 / 3) * 3
                                                                      ELSE 0
                                                                  END)+((-0.20992036) * (L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((-0.20992036-0.20968659*(CASE
                                                                                                                                                                                            WHEN L.[Met First Scheduled Payment] IN('No')
                                                                                                                                                                                            THEN 0
                                                                                                                                                                                            ELSE 1
                                                                                                                                                                                        END))*(L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((12 - DATEDIFF(mm, [Funded Date], '2016-05-31')) * 0.124))
                            WHEN R.Category = 'Furniture'
                            THEN((CASE
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 4
                                      THEN 0.90881
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 5
                                      THEN 1.066181
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 6
                                      THEN 1.21337
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 7
                                      THEN 1.352234
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 8
                                      THEN 1.484367
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 9
                                      THEN 1.608624
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 10
                                      THEN 1.725789
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 11
                                      THEN 1.83367
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 12
                                      THEN 1.910906
                                  END)+((-0.014806725)*(CASE
                                                            WHEN L.[Met First Scheduled Payment] IN('No')
                                                            THEN 1
                                                            ELSE 0
                                                        END))+(CASE
                                                                   WHEN C.[Monthly Income] BETWEEN 2200 AND 3000
                                                                   THEN-0.0248622
                                                                   WHEN C.[Monthly Income] BETWEEN 3001 AND 4500
                                                                   THEN(-0.0360322 / 2) * 2
                                                                   WHEN C.[Monthly Income] > 4500
                                                                   THEN(-0.043569 / 3) * 3
                                                                   ELSE 0
                                                               END)+(CASE
                                                                         WHEN C.Age BETWEEN 18 AND 25
                                                                         THEN-0.012184
                                                                         WHEN C.Age BETWEEN 26 AND 35
                                                                         THEN(-0.008103458 / 2) * 2
                                                                         WHEN C.Age > 35
                                                                         THEN(0.002190049 / 3) * 3
                                                                         ELSE 0
                                                                     END)+((-0.210116531) * (L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((-0.256419958-0.210116531*(CASE
                                                                                                                                                                                                  WHEN L.[Met First Scheduled Payment] IN('No')
                                                                                                                                                                                                  THEN 0
                                                                                                                                                                                                  ELSE 1
                                                                                                                                                                                              END))*(L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((12 - DATEDIFF(mm, [Funded Date], '2016-05-31')) * 0.124))
                            WHEN R.Category = 'Mattress'
                            THEN((CASE
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 4
                                      THEN 0.913306
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 5
                                      THEN 1.076783867
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 6
                                      THEN 1.227971275
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 7
                                      THEN 1.370752939
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 8
                                      THEN 1.508273189
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 9
                                      THEN 1.636032762
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 10
                                      THEN 1.757407946
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 11
                                      THEN 1.868519876
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 12
                                      THEN 1.948830995
                                  END)+((-0.037398454)*(CASE
                                                            WHEN L.[Met First Scheduled Payment] IN('No')
                                                            THEN 1
                                                            ELSE 0
                                                        END))+(CASE
                                                                   WHEN C.[Monthly Income] BETWEEN 2200 AND 3000
                                                                   THEN-0.037870482
                                                                   WHEN C.[Monthly Income] BETWEEN 3001 AND 4500
                                                                   THEN(-0.043857076 / 2) * 2
                                                                   WHEN C.[Monthly Income] > 4500
                                                                   THEN(-0.043526863 / 3) * 3
                                                                   ELSE 0
                                                               END)+(CASE
                                                                         WHEN C.Age BETWEEN 18 AND 25
                                                                         THEN-0.002732159
                                                                         WHEN C.Age BETWEEN 26 AND 35
                                                                         THEN(-0.010705347 / 2) * 2
                                                                         WHEN C.Age > 35
                                                                         THEN(-0.012600856 / 3) * 3
                                                                         ELSE 0
                                                                     END)+((-0.223802260) * (L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((-0.167130270-0.223802260*(CASE
                                                                                                                                                                                                  WHEN L.[Met First Scheduled Payment] IN('No')
                                                                                                                                                                                                  THEN 0
                                                                                                                                                                                                  ELSE 1
                                                                                                                                                                                              END))*(L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((12 - DATEDIFF(mm, [Funded Date], '2016-05-31')) * 0.124))
                            WHEN R.Category = 'Appliance'
                            THEN((CASE
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 4
                                      THEN 0.90815904
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 5
                                      THEN 1.06611611
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 6
                                      THEN 1.21036298
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 7
                                      THEN 1.34692827
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 8
                                      THEN 1.47130036
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 9
                                      THEN 1.58768677
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 10
                                      THEN 1.69505350
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 11
                                      THEN 1.79654187
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 12
                                      THEN 1.86750788
                                  END)+((-0.05918468)*(CASE
                                                           WHEN L.[Met First Scheduled Payment] IN('No')
                                                           THEN 1
                                                           ELSE 0
                                                       END))+(CASE
                                                                  WHEN C.[Monthly Income] BETWEEN 2200 AND 3000
                                                                  THEN-0.034757
                                                                  WHEN C.[Monthly Income] BETWEEN 3001 AND 4500
                                                                  THEN(-0.054217664 / 2) * 2
                                                                  WHEN C.[Monthly Income] > 4500
                                                                  THEN(-0.08063783 / 3) * 3
                                                                  ELSE 0
                                                              END)+(CASE
                                                                        WHEN C.Age BETWEEN 18 AND 25
                                                                        THEN 0.01000387
                                                                        WHEN C.Age BETWEEN 26 AND 35
                                                                        THEN(0.04229324 / 2) * 2
                                                                        WHEN C.Age > 35
                                                                        THEN(0.04026993 / 3) * 3
                                                                        ELSE 0
                                                                    END)+((-0.21785) * (L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((-0.21785-0.19322*(CASE
                                                                                                                                                                                     WHEN L.[Met First Scheduled Payment] IN('No')
                                                                                                                                                                                     THEN 0
                                                                                                                                                                                     ELSE 1
                                                                                                                                                                                 END))*(L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((12 - DATEDIFF(mm, [Funded Date], '2016-05-31')) * 0.124))
                            WHEN R.Category = 'Tires/Rims'
                            THEN((CASE
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 4
                                      THEN 0.877894451
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 5
                                      THEN 1.040961493
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 6
                                      THEN 1.187163821
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 7
                                      THEN 1.327898466
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 8
                                      THEN 1.458994411
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 9
                                      THEN 1.586826607
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 10
                                      THEN 1.706979156
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 11
                                      THEN 1.81515258
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 12
                                      THEN 1.890724640
                                  END)+((-0.119863921)*(CASE
                                                            WHEN L.[Met First Scheduled Payment] IN('No')
                                                            THEN 1
                                                            ELSE 0
                                                        END))+(CASE
                                                                   WHEN C.[Monthly Income] BETWEEN 2200 AND 3000
                                                                   THEN-0.046374665
                                                                   WHEN C.[Monthly Income] BETWEEN 3001 AND 4500
                                                                   THEN(-0.044043003 / 2) * 2
                                                                   WHEN C.[Monthly Income] > 4500
                                                                   THEN(-0.069573312 / 3) * 3
                                                                   ELSE 0
                                                               END)+(CASE
                                                                         WHEN C.Age BETWEEN 18 AND 25
                                                                         THEN-0.003495596
                                                                         WHEN C.Age BETWEEN 26 AND 35
                                                                         THEN(-0.005691023 / 2) * 2
                                                                         WHEN C.Age > 36
                                                                         THEN(0.003347076 / 3) * 3
                                                                         ELSE 0
                                                                     END)+((-0.168855950) * (L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((-0.168855950-0.134412364*(CASE
                                                                                                                                                                                                  WHEN L.[Met First Scheduled Payment] IN('No')
                                                                                                                                                                                                  THEN 0
                                                                                                                                                                                                  ELSE 1
                                                                                                                                                                                              END))*(L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((12 - DATEDIFF(mm, [Funded Date], '2016-05-31')) * 0.124))
                            ELSE((CASE
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 4
                                      THEN 0.84983349
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 5
                                      THEN 0.98474874
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 6
                                      THEN 1.11118942
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 7
                                      THEN 1.23224404
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 8
                                      THEN 1.34233820
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 9
                                      THEN 1.44557737
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 10
                                      THEN 1.54335515
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 11
                                      THEN 1.63177323
                                      WHEN DATEDIFF(mm, [Funded Date], '2016-05-31') = 12
                                      THEN 1.69898603
                                  END)+((0.25046601)*(CASE
                                                          WHEN L.[Met First Scheduled Payment] IN('No')
                                                          THEN 1
                                                          ELSE 0
                                                      END))+(CASE
                                                                 WHEN C.[Monthly Income] BETWEEN 2200 AND 3000
                                                                 THEN-0.01681697
                                                                 WHEN C.[Monthly Income] BETWEEN 3001 AND 4500
                                                                 THEN(-0.06260648 / 2) * 2
                                                                 WHEN C.[Monthly Income] > 4500
                                                                 THEN(-0.03372926 / 3) * 3
                                                                 ELSE 0
                                                             END)+(CASE
                                                                       WHEN C.Age BETWEEN 18 AND 25
                                                                       THEN 0.05177234
                                                                       WHEN C.Age BETWEEN 25 AND 35
                                                                       THEN(0.02470186 / 2) * 2
                                                                       WHEN C.Age > 36
                                                                       THEN(0.02322435 / 3) * 3
                                                                       ELSE 0
                                                                   END)+((-0.15800360) * (L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((-0.15800360-0.71405764*(CASE
                                                                                                                                                                                             WHEN L.[Met First Scheduled Payment] IN('No')
                                                                                                                                                                                             THEN 0
                                                                                                                                                                                             ELSE 1
                                                                                                                                                                                         END))*(L.[Funded Amount] / COALESCE(NULLIF(L.[Approval Amount], 0), 2500)))+((12 - DATEDIFF(mm, [Funded Date], '2016-05-31')) * 0.124))
                        END)*[Funded Amount]
               END AS PredictedPaid,
               [Total Paid] AS TotalPaid,
               [Funded Amount] AS Funded,
               [Total Paid] / [Funded Amount] AS CurrentMultiple,
               CategoryHistory.HistoricalMultiple * [Funded Amount] AS HistoricalPaid
--count(case when LeaseQueueKey in (4,6) then 1 else null end) as 'Current Leases'
        FROM Leases AS L
             JOIN Retailers AS R ON L.[Retailer ID] = R.[Retailer ID]
             JOIN Customers AS C ON C.[Customer ID] = L.[Customer ID] --///////////////////////////////////
             --// Add Average Data from 12-24 months ago
             --// BEGIN
             --//////////////////////////////////
             LEFT OUTER JOIN
        (
            SELECT Category,
                   SUM(TotalPaid) / SUM(Funded) AS HistoricalMultiple
            FROM
            (
                SELECT [Lease ID],
                       TotalPaid,
                       Funded,
                       Vintage,
                       Category
                FROM --//////////////////////////////////
                --// Begin ActiveLeases
                --//////////////////////////////////
                (
                    SELECT L.[Lease ID],
                           LeaseQueueKey,
                           [Approval Amount],
                           R.Category,
                           CONVERT( CHAR(7), [Funded Date], 120) AS 'Vintage',
                           [Total Paid] AS TotalPaid,
                           [Funded Amount] AS Funded,
                           [Total Paid] / [Funded Amount] AS CurrentMultiple
                    FROM Leases AS L
                         JOIN Retailers AS R ON L.[Retailer ID] = R.[Retailer ID]
                         JOIN Customers AS C ON C.[Customer ID] = L.[Customer ID]
                    WHERE [Funded Amount] > 0
                          AND [Total Paid] > [Funded Amount]
                          AND DATEDIFF(mm, [Funded Date], '2016-05-31') BETWEEN 12 AND 24
                          AND LeaseQueueKey = 5 --// 5 = Paid
                ) AS ActiveLeases
                --//////////////////////////////////
                --// End ActiveLeases
                --//////////////////////////////////

            ) AS PredictedLeases
            GROUP BY Category
        ) AS CategoryHistory ON R.Category = CategoryHistory.Category
        --///////////////////////////////////
        --// Add Average Data from 12-24 months ago
        --// END
        --//////////////////////////////////

			
        WHERE [Funded Amount] > 0
              AND [Funded Date] > '1-1-15'
              AND DATEDIFF(mm, [Funded Date], '2016-05-31') BETWEEN 4 AND 16
              AND LeaseQueueKey <> 21
    ) AS ActiveLeases
) AS PredictedLeases
--,Category
ORDER BY Vintage;