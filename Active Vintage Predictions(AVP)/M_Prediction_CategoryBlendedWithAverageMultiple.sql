Select
Vintage,
--Category,
sum(TotalPaid)/Sum(Funded) as CurrentMultiple,
(sum(case when PredictedPaid is not null then PredictedPaid else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMultiple,
(sum(case when PredictedPaid is not null then PredictedPaid*1.10 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMidPoint,
(sum(case when PredictedPaid is not null then PredictedPaid*1.20 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedUpperLimit,
sum(PredictedPaid)/(sum(TotalPaid)+sum(PredictedPaid)) as PercentPredictedAmount,
convert(numeric(14,2),count(PredictedPaid))/count(*) as PercentPredictedLeases
--Sum(Funded) as TotalFunded,
--Count(*) as TotalLeases
from
	(
	select 
		[Lease ID],
		TotalPaid,
		Funded,
		Vintage,
		Category,
		case when TotalPaid>PredictedPaid then TotalPaid else PredictedPaid end as PredictedPaid
		from
		(
			SELECT
				L.[Lease ID],
				LeaseQueueKey,
				[Approval Amount],
				R.Category,
				CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
				case 
					--// Dynamic
					when LeaseQueueKey in (4,6) and ((L.[Payments Returned] >= 3.5 and [Total Paid] < 240 and L.[Payments Cleared] >=18) or (L.[Payments Returned] >=3.5 and L.[Payments Cleared] >= 18) or (L.[Payments Returned] > 8.5)) then 
						(case when R.Category = 'Appliance/Electronics/Video' then (0.47*[Funded Amount])
							when R.Category = 'Furniture' then (0.57*[Funded Amount])
							when R.Category = 'Jewelry' then (0.53*[Funded Amount])
							when R.Category = 'Mattress' then (0.62*[Funded Amount])
							else (0.50*[Funded Amount]) end)
					when LeaseQueueKey in (4,6) and [Total Paid] < 10 then
						(0.015*[Funded Amount])
					when LeaseQueueKey in (4,6) then
					(case when R.Category = 'Jewelry' then
						((case when datediff(mm,[Funded Date],getdate()) = 4 then 0.90199
								when datediff(mm,[Funded Date],getdate()) = 5 then 1.05685
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.19894
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.33537
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.459269
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.57665
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.68803
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.78803
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.861851 end)
					+((-0.079889)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.027558
						when C.[Monthly Income] between 3001 and 4500 then (-0.033916/2)*2
						when C.[Monthly Income] > 4500 then (-0.0586755/3)*3 else 0 end)
					+(case when C.Age between 18 and 25 then -0.02054755
						when C.Age between 26 and 35 then (-0.01229667/2)*2
						when C.Age > 35 then (0.0295560/3)*3 else 0 end)
					+((-0.20992036)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.20992036-0.20968659*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12 - datediff(mm,[Funded Date],getdate()))*0.124))
						when R.Category = 'Furniture' then
						((case when datediff(mm,[Funded Date],getdate()) = 4 then 0.90881
								when datediff(mm,[Funded Date],getdate()) = 5 then 1.066181
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.21337
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.352234
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.484367
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.608624
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.725789
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.83367
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.910906 end)
					+((-0.014806725)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.0248622
						when C.[Monthly Income] between 3001 and 4500 then (-0.0360322/2)*2
						when C.[Monthly Income] > 4500 then (-0.043569/3)*3 else 0 end)
					+(case when C.Age between 18 and 25 then -0.012184
						when C.Age between 26 and 35 then (-0.008103458/2)*2
						when C.Age > 35 then (0.002190049/3)*3 else 0 end)
					+((-0.210116531)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.256419958-0.210116531*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12 - datediff(mm,[Funded Date],getdate()))*0.124))
					when R.Category = 'Mattress' then
						((case when datediff(mm,[Funded Date],getdate()) = 4 then 0.913306
								when datediff(mm,[Funded Date],getdate()) = 5 then 1.076783867
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.227971275
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.370752939
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.508273189
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.636032762
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.757407946
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.868519876
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.948830995 end)
					+((-0.037398454)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.037870482
						when C.[Monthly Income] between 3001 and 4500 then (-0.043857076/2)*2
						when C.[Monthly Income] > 4500 then (-0.043526863/3)*3 else 0 end)
					+(case when C.Age between 18 and 25 then -0.002732159
						when C.Age between 26 and 35 then (-0.010705347/2)*2
						when C.Age > 35 then (-0.012600856/3)*3 else 0 end)
					+((-0.223802260)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.167130270-0.223802260*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12 - datediff(mm,[Funded Date],getdate()))*0.124))
					when R.Category = 'Appliance' then
						((case when datediff(mm,[Funded Date],getdate()) = 4 then 0.90815904
								when datediff(mm,[Funded Date],getdate()) = 5 then 1.06611611
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.21036298
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.34692827
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.47130036
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.58768677
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.69505350
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.79654187
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.86750788 end)
					+((-0.05918468)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.034757
						when C.[Monthly Income] between 3001 and 4500 then (-0.054217664/2)*2
						when C.[Monthly Income] > 4500 then (-0.08063783/3)*3 else 0 end)
					+(case when C.Age between 18 and 25 then 0.01000387
						when C.Age between 26 and 35 then (0.04229324/2)*2
						when C.Age > 35 then (0.04026993/3)*3 else 0 end)
					+((-0.21785)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.21785-0.19322*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12 - datediff(mm,[Funded Date],getdate()))*0.124))
				when R.Category = 'Tires/Rims' then
						((case when datediff(mm,[Funded Date],getdate()) = 4 then 0.877894451
								when datediff(mm,[Funded Date],getdate()) = 5 then 1.040961493
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.187163821
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.327898466
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.458994411
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.586826607
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.706979156
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.81515258
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.890724640 end)
					+((-0.119863921)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.046374665
						when C.[Monthly Income] between 3001 and 4500 then (-0.044043003/2)*2
						when C.[Monthly Income] > 4500 then (-0.069573312/3)*3 else 0 end)
					+(case when C.Age between 18 and 25 then -0.003495596
						when C.Age between 26 and 35 then (-0.005691023/2)*2
						when C.Age > 36 then (0.003347076/3)*3 else 0 end)
					+((-0.168855950)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.168855950 -0.134412364*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12 - datediff(mm,[Funded Date],getdate()))*0.124))
					else
						((case when datediff(mm,[Funded Date],getdate()) = 4 then 0.84983349
								when datediff(mm,[Funded Date],getdate()) = 5 then 0.98474874
								when datediff(mm,[Funded Date],getdate()) = 6 then 1.11118942
								when datediff(mm,[Funded Date],getdate()) = 7 then 1.23224404
								when datediff(mm,[Funded Date],getdate()) = 8 then 1.34233820
								when datediff(mm,[Funded Date],getdate()) = 9 then 1.44557737
								when datediff(mm,[Funded Date],getdate()) = 10 then 1.54335515
								when datediff(mm,[Funded Date],getdate()) = 11 then 1.63177323
								when datediff(mm,[Funded Date],getdate()) = 12 then 1.69898603 end)
					+((0.25046601)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
					+(case when C.[Monthly Income] between 2200 and 3000 then -0.01681697
						when C.[Monthly Income] between 3001 and 4500 then (-0.06260648/2)*2
						when C.[Monthly Income] > 4500 then (-0.03372926/3)*3 else 0 end)
					+(case when C.Age between 18 and 25 then 0.05177234
						when C.Age between 25 and 35 then (0.02470186/2)*2
						when C.Age > 36 then (0.02322435/3)*3 else 0 end)
					+((-0.15800360)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((-0.15800360-0.71405764*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
					+((12 - datediff(mm,[Funded Date],getdate()))*0.124))
					end)*[Funded Amount]
					end as PredictedPaid,
					[Total Paid] as TotalPaid,
					[Funded Amount] as Funded,
					[Total Paid]/[Funded Amount] as CurrentMultiple
				--count(case when LeaseQueueKey in (4,6) then 1 else null end) as 'Current Leases'
			FROM
				Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
					join Customers as C on C.[Customer ID] = L.[Customer ID]

			
			WHERE
				[Funded Amount] > 0 and [Funded Date] > '1-1-15'
				and datediff(mm,[Funded Date],getdate()) between 4 and 16
				and LeaseQueueKey<>21
		) as ActiveLeases
	) as PredictedLeases

group by Vintage
--,Category
order by Vintage