Select
Vintage,
--Category,
sum(TotalPaid)/Sum(Funded) as CurrentMultiple,
(sum(case when PredictedPaid is not null then PredictedPaid else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMultiple,
(sum(case when PredictedPaid is not null then PredictedPaid*1.05 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedMidPoint,
(sum(case when PredictedPaid is not null then PredictedPaid*1.10 else 0 end)+sum(case when PredictedPaid is null then TotalPaid else 0 end)) / sum(Funded) as PredictedUpperLimit,
sum(PredictedPaid)/(sum(TotalPaid)+sum(PredictedPaid)) as PercentPredictedAmount,
convert(numeric(14,2),count(PredictedPaid))/count(*) as PercentPredictedLeases
--Sum(Funded) as TotalFunded,
--Count(*) as TotalLeases
from
	(
	select 
		[Lease ID],
		TotalPaid,
		Funded,
		Vintage,
		Category,
		case when TotalPaid>PredictedPaid then TotalPaid else PredictedPaid end as PredictedPaid
		from
		(
			SELECT
				L.[Lease ID],
				LeaseQueueKey,
				[Approval Amount],
				R.Category,
				CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
				case 
					--// Dynamic
					when LeaseQueueKey in (4,6) then CategoryHistory.CurrentMultiple * [Funded Amount]	end as PredictedPaid,
					[Total Paid] as TotalPaid,
					[Funded Amount] as Funded,
					[Total Paid]/[Funded Amount] as CurrentMultiple
			FROM
				Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
					join Customers as C on C.[Customer ID] = L.[Customer ID]
				
				--///////////////////////////////////
				--// Add Average Data from 12-24 months ago
				--// BEGIN
				--//////////////////////////////////
				left outer join 
				(
				Select
				Category,
				sum(TotalPaid)/Sum(Funded) as CurrentMultiple
				from
					(
					select 
						[Lease ID],
						TotalPaid,
						Funded,
						Vintage,
						Category
						from
						(
							SELECT
								L.[Lease ID],
								LeaseQueueKey,
								[Approval Amount],
								R.Category,
								CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
									[Total Paid] as TotalPaid,
									[Funded Amount] as Funded,
									[Total Paid]/[Funded Amount] as CurrentMultiple
							FROM
								Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
									join Customers as C on C.[Customer ID] = L.[Customer ID]
							WHERE
								[Funded Amount] > 0 
								--and [Total Paid]>0
								and datediff(mm,[Funded Date],getdate()) between 12 and 24
								and LeaseQueueKey<>21
						) as ActiveLeases
					) as PredictedLeases
				group by Category
				) as CategoryHistory on R.Category=CategoryHistory.Category
				--///////////////////////////////////
				--// Add Average Data from 12-24 months ago
				--// END
				--//////////////////////////////////

			WHERE
				[Funded Amount] > 0 and [Funded Date] > '1-1-15'
				and datediff(mm,[Funded Date],getdate()) between 4 and 16
				and LeaseQueueKey<>21
		) as ActiveLeases
	) as PredictedLeases
group by Vintage
--, Category
order by Vintage
