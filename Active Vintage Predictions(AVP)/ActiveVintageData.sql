
Select 
Vintage,
sum(PredictedPaid)/sum(Funded) as PredictedMultiple,
sum(TotalPaid)/Sum(Funded) as CurrentMultiple
from
(
	SELECT
		L.[Lease ID],
		LeaseQueueKey,
		[Approval Amount],
		CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
		case 
			--// Static 
			when LeaseQueueKey in (5,9,12,13,14,16,20,21,22) then [Total Paid]
			--// Dynamic
			when LeaseQueueKey in (4,6) then 
			((case when(case when datediff(mm,[Funded Date],getdate()) = 4 then 0.906862
					when datediff(mm,[Funded Date],getdate()) = 5 then 1.064090
					when datediff(mm,[Funded Date],getdate()) = 6 then 1.210379
					when datediff(mm,[Funded Date],getdate()) = 7 then 1.348679
					when datediff(mm,[Funded Date],getdate()) = 8 then 1.479892
					when datediff(mm,[Funded Date],getdate()) = 9 then 1.603305
					when datediff(mm,[Funded Date],getdate()) = 10 then 1.719931
					when datediff(mm,[Funded Date],getdate()) = 11 then 1.827005
					when datediff(mm,[Funded Date],getdate()) = 12 then 1.903981 end) < ([Total Paid]/[Funded Amount]) then
						([Total Paid]/[Funded Amount]) else
					(case when datediff(mm,[Funded Date],getdate()) = 4 then 0.906862
						when datediff(mm,[Funded Date],getdate()) = 5 then 1.064090
						when datediff(mm,[Funded Date],getdate()) = 6 then 1.210379
						when datediff(mm,[Funded Date],getdate()) = 7 then 1.348679
						when datediff(mm,[Funded Date],getdate()) = 8 then 1.479892
						when datediff(mm,[Funded Date],getdate()) = 9 then 1.603305
						when datediff(mm,[Funded Date],getdate()) = 10 then 1.719931
						when datediff(mm,[Funded Date],getdate()) = 11 then 1.827005
						when datediff(mm,[Funded Date],getdate()) = 12 then 1.903981 end)
				end)
			+((-0.031509)*(case when L.[Met First Scheduled Payment] in ('No') then 1 else 0 end))
			+(case when C.[Monthly Income] between 2200 and 3000 then -0.028563
				when C.[Monthly Income] between 3001 and 4500 then (-0.038493/2)*2
				when C.[Monthly Income] > 4500 then (-0.049052/3)*3 else 0 end)
			+(case when C.Age between 18 and 28 then -0.00631
				when C.Age between 29 and 36 then (-0.003671/2)*2
				when C.Age > 36 then (0.003900/3)*3 else 0 end)
			+((-0.212022)*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
			+((-0.212022-0.229624*(case when L.[Met First Scheduled Payment] in ('No') then 0 else 1 end))*(L.[Funded Amount]/coalesce(nullif(L.[Approval Amount],0),2500)))
			+((12 - datediff(mm,[Funded Date],getdate()))*0.1246399))
			*[Funded Amount] 
			Else
			--// Invalid Lease Queue Key
			[Total Paid]
			end as PredictedPaid,
			[Total Paid] as TotalPaid,
			[Funded Amount] as Funded,
			[Total Paid]/[Funded Amount] as CurrentMultiple
		--count(case when LeaseQueueKey in (4,6) then 1 else null end) as 'Current Leases'
	FROM
		Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
			join Customers as C on C.[Customer ID] = L.[Customer ID]
	WHERE
		[Funded Amount] > 0 and [Funded Date] > '1-1-15'
		and datediff(mm,[Funded Date],getdate()) between 4 and 12
) as ActiveLeases
--where LeaseQueueKey=21
--order by PredictedPaid
--where PredictedCollections is null
group by Vintage
order by Vintage


