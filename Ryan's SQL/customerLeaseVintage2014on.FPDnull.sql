SELECT
	[Lease ID],
	[LeaseQueuekey],
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	[Funded Date],
	[Approval Date],
	[Agreement Date],
	[Submitted Date],
	[Payments Charged],
	case when [Close Date] IS NULL then 'No' else 'Yes' end as 'Account Closed',
	[Funded Amount],
	[Total Note],
	[Total Paid],
	isnull(nullif([Total Paid],0)/nullif([Funded Amount],0),0) as 'Multiple',
	isnull(Datediff(mm,[Funded Date], [Close Date]),Datediff(mm,[Funded Date], getdate())) as 'Lease Length',
	[Auto Approved],
	[Met First Scheduled Payment],
	[Age],
	datediff(mm,[Hire Date],getdate()) as 'Employement Length',
	[Monthly Income],
	case when [Has Credit Card] = 1 then 'Yes' else 'No' end as 'Credit Card',
	[State]
FROM
	[Crest Business Intelligence].dbo.Customers as C join
	[Crest Business Intelligence].dbo.Leases as L on
	C.[Customer ID] = L.[Customer ID] 
WHERE
	L.LeaseQueueKey in (4,5,6,12,16)
	and [Funded Date] > '2014-01-01'
	and [Met First Scheduled Payment] not in ('Yes','No','Partially')
	and [Funded Amount] > 0
ORDER BY
	[Vintage] asc,
	[Lease Length] asc,
	[LeaseQueueKey] asc