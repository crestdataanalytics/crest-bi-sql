SELECT
	CONVERT(CHAR(7),[FundedDate],120) as 'Vintage',
	L.ID,
	L.[Amount] as Approved_Amount,
	/*L.[FundedAmount],
	L.[Balance],
	L.[EarlyPayout] ,
	L.[SettlementAmount], 
	L.[BuyoutDate],*/
	T.M1,
	T.M2,
	T.M3,
	T.M4,
	T.M5,
	T.M6,
	T.M7,
	T.M8,
	Case when T.[CC1] >=0 then 'Yes' else 'No' end as 'CC1 on Track',
	Case when T.[CC2] >=0 then 'Yes' else 'No' end as 'CC2 on Track',
	Case when T.[CC3] >=0 then 'Yes' else 'No' end as 'CC3 on Track',
	Case when T.[CC4] >=0 then 'Yes' else 'No' end as 'CC4 on Track',
	Case when T.[CC5] >=0 then 'Yes' else 'No' end as 'CC5 on Track',
	Case when T.[CC6] >=0 then 'Yes' else 'No' end as 'CC6 on Track',
	Case when T.[CC7] >=0 then 'Yes' else 'No' end as 'CC7 on Track',
	Case when T.[CC8] >=0 then 'Yes' else 'No' end as 'CC8 on Track',
	T.CC1,
	T.CC2,
	T.CC3,
	T.CC4,
	T.CC5,
	T.CC6,
	T.CC7,
	T.CC8,
	T.[M1 Payments Cleared],
	T.[M1 Payments Returned],
	T.[M2 Payments Cleared],
	T.[M2 Payments Returned],
	T.[M3 Payments Cleared],
	T.[M3 Payments Returned],
	T.[M4 Payments Cleared],
	T.[M4 Payments Returned]
from Loans as L
      left outer join 
		(SELECT
			LPP.LoanID, 
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M1,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M2,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M3,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M4,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 5 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M5,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 6 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M6,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 7 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M7,
			SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 8 and LPP.[ReturnDate] is null then LPP.[Amount] else 0 end) as M8,


			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) as FMM,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) - 0.25 as CC1,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.44 as CC2,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.62 as CC3,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 0.71 as CC4,	
			
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 5 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0)) - 0.80 as CC5,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 6 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- .90 as CC6,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 7 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 1.0 as CC7,
			(SUM(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 8 and LPP.[ReturnDate] is null then coalesce(LPP.[Amount],0) else 0 end)/
				nullif(L.[FundedAmount],0))- 1.20 as CC8,
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M1 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 1 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M1 Payments Cleared',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M2 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 2 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M2 Payments Cleared',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M3 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 3 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M3 Payments Cleared',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is not null then LPP.[PaymentDate] else null end) as 'M4 Payments Returned',
			count(case when datediff(mm,L.[FundedDate],LPP.[PaymentDate]) <= 4 and LPP.[ReturnDate] is null then LPP.[PaymentDate] else null end) as 'M4 Payments Cleared'
            FROM LoanPayments as LPP left join Loans as L on LPP.LoanID = L.ID
			GROUP BY 
			LPP.LoanID,
			L.[FundedAmount],
			L.[Balance],
			L.[EarlyPayout] ,
			L.[SettlementAmount], 
			L.[BuyoutDate]

                ) as T on L.ID = T.LoanID
WHERE L.[FundedDate] between '12-1-14' and '12-31-15' --and [Total Paid] = 0
	and L.StatusID not in (21)
    and L.StatusID in (5,12,13,14)
Group by
	CONVERT(CHAR(7),[FundedDate],120),
	L.ID,
	L.[Amount] ,
	l.[FundedDate],
	l.id,
	T.M1,
	T.M2,
	T.M3,
	T.M4,
	T.M5,
	T.M6,
	T.M7,
	T.M8,
	T.CC1,
	T.CC2,
	T.CC3,
	T.CC4,
	T.CC5,
	T.CC6,
	T.CC7,
	T.CC8,
	T.[M1 Payments Cleared],
	T.[M1 Payments Returned],
	T.[M2 Payments Cleared],
	T.[M2 Payments Returned],
	T.[M3 Payments Cleared],
	T.[M3 Payments Returned],
	T.[M4 Payments Cleared],
	T.[M4 Payments Returned]
order by
	CONVERT(CHAR(7),[FundedDate],120) asc