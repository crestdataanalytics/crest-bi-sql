SELECT
	CONVERT(CHAR(7),FundedDate,120) as 'Vintage',
	ID,
	case when StatusID = 5 then 'Paid'
		when StatusID = 9 then 'Returned'
		when StatusID = 12 then 'ChargeOff'
		when StatusID = 13 then 'Bankrupt13'
		when StatusID = 14 then 'Bankrupt7'
		when StatusID = 20 then 'DealerReturn'
		when StatusID = 21 then 'Return'
		when StatusID = 22 then 'Settled' end as CName,
	case when PaidOffDate is not null then datediff(dd,FundedDate,PaidOffDate)
		when ChargedOffDate is not null then datediff(dd,FundedDate,ChargedOffDate)
		else 0 end as 'Days Active'
FROM
	Loans
WHERE
	FundedDate between '1-1-14' and '7-30-15'
	and FundedAmount > 0
ORDER BY
	CONVERT(CHAR(7),FundedDate,120)