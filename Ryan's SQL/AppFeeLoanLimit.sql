/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
	distinct D.ID as storeID,
	D.Name,
	C.CategoryName,
	LoanLimit as LeaseLimit,
	ApplicationFee,
	D.CrestPercentage,
	InitialPaymentRequired,
	FP.Name as PlanName,
	FP.CreatedDate as PlanCreateDate,
	D.NinetyDayPayoffPercent,
	datediff(mm,CreationDate,getdate()) as 'Months Open'
From Dealers as D join DealerCategories as DC on D.ID = DC.DealerID
	join Categories as C on DC.CategoryID = C.ID
	join DealerFinancePlans as DF on DF.DealerID = D.ID
	join FinancePlans as FP on FP.ID = DF.FinancePlanID
Where
	statusid = 1
order by
	ApplicationFee asc
