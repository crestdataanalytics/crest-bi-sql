select
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	count([Funded Amount]) as 'Leases',
	count(case when LeaseQueueKey = 12 then 1 else null end)*1.0/count([Funded Amount]) as '% Charge-Off',
	count(case when LeaseQueueKey = 5 then 1 else null end)*1.0/count([Funded Amount]) as '% Paid',
	count(case when datediff(dd,[Funded Date],[Close Date])<90 and LeaseQueueKey = 5 then 1 else null end)*1.0/count([Funded Amount]) as '% SAC',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) between 91 and 364 then 1 else null end)*1.0/count([Funded Amount]) as '% EBO',
	sum([Total Paid])/sum([Funded Amount]) as 'Multiple'
from Leases
where [Funded Date] between '5-1-14' and '5-31-16' and [Funded Amount] > 0 and [Approval Amount] > 4000
group by CONVERT(CHAR(7),[Funded Date],120)
order by CONVERT(CHAR(7),[Funded Date],120) asc