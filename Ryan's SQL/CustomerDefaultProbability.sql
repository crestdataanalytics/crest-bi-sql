SELECT
	R.[Retailer ID],
	(exp(
			(	(Case when PD.IDVResult = 'D1' then -0.58751116
				when PD.IDVResult = 'D10' then -1.51183217
				when PD.IDVResult = 'D11' then -12.42763105
				when PD.IDVResult = 'D13' then -0.57193736 
				when PD.IDVResult = 'D14' then -1.32448866
				when PD.IDVResult = 'D15' then -1.50499940
				when PD.IDVResult = 'D16' then -2.23585564
				when PD.IDVResult = 'D17' then -2.35251570
				when PD.IDVResult = 'D18' then -1.33222062
				when PD.IDVResult = 'D19' then -3.07827867
				when PD.IDVResult = 'D4' then -11.89686189
				when PD.IDVResult = 'D6' then -1.79865452
				when PD.IDVResult = 'D8' then -2.44117966 end)
				+
				(Case when PD.IDVCode = '10' then 0.50928614
				when PD.IDVResult = '20' then 1.07487556
				when PD.IDVResult = '30' then 2.26382190
				when PD.IDVResult = '40' then 1.83543867 
				when PD.IDVResult = '50' then 1.51877395 end)
			)
		)
	/(1+exp(
			(	(Case when PD.IDVResult = 'D1' then -0.58751116
				when PD.IDVResult = 'D10' then -1.51183217
				when PD.IDVResult = 'D11' then -12.42763105
				when PD.IDVResult = 'D13' then -0.57193736 
				when PD.IDVResult = 'D14' then -1.32448866
				when PD.IDVResult = 'D15' then -1.50499940
				when PD.IDVResult = 'D16' then -2.23585564
				when PD.IDVResult = 'D17' then -2.35251570
				when PD.IDVResult = 'D18' then -1.33222062
				when PD.IDVResult = 'D19' then -3.07827867
				when PD.IDVResult = 'D4' then -11.89686189
				when PD.IDVResult = 'D6' then -1.79865452
				when PD.IDVResult = 'D8' then -2.44117966 end)
				+
				(Case when PD.IDVCode = '10' then 0.50928614
				when PD.IDVResult = '20' then 1.07487556
				when PD.IDVResult = '30' then 2.26382190
				when PD.IDVResult = '40' then 1.83543867 
				when PD.IDVResult = '50' then 1.51877395 end)
			)
		)))*10 as DefaultProbability
FROM
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
	left outer join
		(Select
			pad.SubjectID
			,max(case when padv.VariableID = 7216 then padv.VariableValue else null end) as 'IDVResult'
			,max(case when padv.VariableID = 7765 then padv.VariableValue else null end) as 'IDVCode'
			,max(case when padv.VariableID = 6342 then padv.VariableValue else null end) as 'BAVPass2'
			,max(case when padv.VariableID = 8164 then padv.VariableValue else null end) as 'DR'
			,max(case when padv.VariableID = 7910 then padv.VariableValue else null end) as 'IDABucket'
			,max(case when padv.VariableID = 246 then padv.VariableValue else null end) as 'ClarityAction'
			,max(case when padv.VariableID = 13084 then padv.VariableValue else null end) as 'PropertyOwner'
		from [ProviderAnalysis].dbo.DocumentInfos as pad 
			join [ProviderAnalysis].dbo.DocumentVariables as padv on
			pad.DocumentID = padv.DocumentID
			join [ProviderAnalysis].dbo.Variables as pav on
			padv.VariableID = pav.VariableID
			join [ProviderAnalysis].dbo.Providers as pap on
			pad.providerID = pap.providerId
			left outer join [Crest Business Intelligence].[dbo].[Leases] as L on L.[Customer ID] = pad.SubjectID
		GROUP BY
			pad.SubjectID) as PD on PD.SubjectID = L.[Customer ID]
WHERE
	R.[Retailer ID] = 11138