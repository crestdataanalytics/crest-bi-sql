 SELECT
	LF.LoanID,
	sum([FeeAmount]) as TotalFees
 FROM LoanFees as LF left join vw_loans_reporting as L on LF.LoanID = L.ID
 WHERE
	L.[FundedDate] between '12-1-13' and '7-1-15'
GROUP BY
	LF.LoanID
