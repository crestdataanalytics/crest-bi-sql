select
	R.[Retailer Name],
	C.[Monthly Income],
	sum(L.[Funded Amount]) as 'Funded',
	sum(L.[Total Paid]) as 'Paid',
	Count(C.[Monthly Income]) as IncomeOccurance
From
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
		join Customers as C on L.[Customer ID] = C.[Customer ID]
Where
	R.[Current Status] = 'Active' and L.[Submitted Date] > '1-1-16'
group by
	C.[Monthly Income],
	R.[Retailer Name]
having
	Count(C.[Monthly Income]) > 10
order by
	R.[Retailer Name],
	IncomeOccurance

select
	R.[Retailer Name],
	sum(L.[Funded Amount]) as 'Funded',
	Count(C.[Monthly Income]) as IncomeOccurance
From
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
		join Customers as C on L.[Customer ID] = C.[Customer ID]
Where
	R.[Current Status] = 'Active' and L.[Submitted Date] > '1-1-16'
group by
	R.[Retailer Name]
order by
	R.[Retailer Name],
	IncomeOccurance

select
	R.[Retailer Name],
	L.[Customer ID],
	L.[Funded Date],
	L.[Funded Amount]
From
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
		join Customers as C on L.[Customer ID] = C.[Customer ID]
Where
	R.[Current Status] = 'Active' and L.[Submitted Date] > '1-1-16' and L.[Funded Amount] > 0 and R.[Retailer Name] = 'ACE Furniture'


select
	R.[Retailer Name],
	sum(L.[Total Paid])/sum(L.[Funded Amount]) as 'Multiple'
From
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
		join Customers as C on L.[Customer ID] = C.[Customer ID]
Where
	R.[Current Status] = 'Active' and L.[Funded Amount] > 0
group by
	R.[Retailer Name]
order by
	R.[Retailer Name]