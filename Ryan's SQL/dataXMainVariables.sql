	select 
		pad.SubjectID
		,max(case when padv.VariableID = 7216 then padv.VariableValue else null end) as 'IDV Result'
		,max(case when padv.VariableID = 7765 then padv.VariableValue else null end) as 'IDV Code'
		,max(case when padv.VariableID = 6383 then padv.VariableValue else null end) as 'BAV Result'
		,max(case when padv.VariableID = 6250 then padv.VariableValue else null end) as 'BAV Code'
		,max(case when padv.VariableID = 6355 then padv.VariableValue else null end) as 'BAV Pass'
		,max(case when padv.VariableID = 6342 then padv.VariableValue else null end) as 'BAV Pass 2'
		,max(case when padv.VariableID = 7483 then padv.VariableValue else null end) as 'NAS Summary'
		,max(case when padv.VariableID = 7442 then padv.VariableValue else null end) as 'NAP Summary'
		,max(case when padv.VariableID = 7715 then padv.VariableValue else null end) as 'Follow up 1'
		,max(case when padv.VariableID = 7768 then padv.VariableValue else null end) as 'Follow up 2'
		,max(case when padv.VariableID = 7505 then padv.VariableValue else null end) as 'HRI 1'
		,max(case when padv.VariableID = 7716 then padv.VariableValue else null end) as 'HRI 2'
		,max(case when padv.VariableID = 7792 then padv.VariableValue else null end) as 'HRI 3'
		,max(case when padv.VariableID = 7823 then padv.VariableValue else null end) as 'HRI 4'
		,max(case when padv.VariableID = 7962 then padv.VariableValue else null end) as 'HRI 5'
		,max(case when padv.VariableID = 8114 then padv.VariableValue else null end) as 'HRI 6'
		,max(case when padv.VariableID = 8202 then padv.VariableValue else null end) as 'HRI 7'
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'DataX'
			and pav.VariableID in (7216,7765,6383,6250,6355,6342,7483,7442,7715,7768,7505,7716,7792,7823,7962,7823,7962,8114,8202)
		group by
			pad.SubjectID