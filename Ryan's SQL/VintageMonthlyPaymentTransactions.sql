/*
--Amount Paid By Month
SELECT
	CONVERT(CHAR(7),LP.PaymentDate,120) as 'Pay Month',
	sum(case when [ReturnDate] is null then LP.[Amount] else 0 end) as 'Amount Paid'
from vw_loans_reporting as L left join LoanPayments as LP on L.ID = LP.LoanID
where
	L.[FundedDate] between '1-1-15' and '2-1-15' 
	and LoanID = 852543
Group by
	CONVERT(CHAR(7),LP.PaymentDate,120)
order by
	CONVERT(CHAR(7),LP.PaymentDate,120) asc
*/

/*
--Amount Paid in Total By Month
SELECT
	CONVERT(CHAR(7),LP.PaymentDate,120) as 'Pay Month',
	L.ID,
	L.[FundedAmount],
	sum(case when [ReturnDate] is null then LP.[Amount] else 0 end) as 'Amount Paid',
	TotalPaid=
            ( SELECT SUM(LPP.[Amount])
              FROM LoanPayments as LPP
               WHERE (CONVERT(CHAR(7),LPP.PaymentDate,120) <= CONVERT(CHAR(7),LP.PaymentDate,120)) and LPP.[ReturnDate] is null and L.ID = LPP.LoanID
                )
from vw_loans_reporting as L left join LoanPayments as LP on L.ID = LP.LoanID
where
	L.[FundedDate] between '1-1-15' and '2-1-15' 
Group by
	CONVERT(CHAR(7),LP.PaymentDate,120),
	L.ID,
	L.[FundedAmount]
order by
	CONVERT(CHAR(7),LP.PaymentDate,120) asc
*/
