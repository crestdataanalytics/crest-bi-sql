/*	
	select 
		pad.SubjectID
		,max(case when padv.VariableID = 7266 then padv.VariableValue else null end) as 'Global Result'
		,max(case when padv.VariableID = 7216 then padv.VariableValue else null end) as 'IDV Result'
		,max(case when padv.VariableID = 7765 then padv.VariableValue else null end) as 'IDV Code'
		,max(case when padv.VariableID = 6383 then padv.VariableValue else null end) as 'BAV Result'
		,max(case when padv.VariableID = 6250 then padv.VariableValue else null end) as 'BAV Code'
		,max(case when padv.VariableID = 6355 then padv.VariableValue else null end) as 'BAV Pass'
		,max(case when padv.VariableID = 6342 then padv.VariableValue else null end) as 'BAV Pass 2'
		,max(case when padv.VariableID = 7483 then padv.VariableValue else null end) as 'NAS Summary'
		,max(case when padv.VariableID = 7442 then padv.VariableValue else null end) as 'NAP Summary'
		,max(case when padv.VariableID = 7715 then padv.VariableValue else null end) as 'Follow up 1'
		,max(case when padv.VariableID = 7768 then padv.VariableValue else null end) as 'Follow up 2'
		,max(case when padv.VariableID = 7505 then padv.VariableValue else null end) as 'HRI 1'
		,max(case when padv.VariableID = 7716 then padv.VariableValue else null end) as 'HRI 2'
		,max(case when padv.VariableID = 7792 then padv.VariableValue else null end) as 'HRI 3'
		,max(case when padv.VariableID = 7823 then padv.VariableValue else null end) as 'HRI 4'
		,max(case when padv.VariableID = 7962 then padv.VariableValue else null end) as 'HRI 5'
		,max(case when padv.VariableID = 8114 then padv.VariableValue else null end) as 'HRI 6'
		,max(case when padv.VariableID = 8202 then padv.VariableValue else null end) as 'HRI 7'
		,max(case when padv.VariableID = 6315 then padv.VariableValue else null end) as 'Actual Income'
		into ##ProviderData
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'DataX'
			and pav.VariableID in (7216,7765,6383,6250,6355,6342,7483,7442,7715,7768,7505,7716,7792,7823,7962,7823,7962,8114,8202,6315)
		group by
			pad.SubjectID
*/

/*

	select 
		pad.[Customer ID],
		L.[Retailer ID],
		CONVERT(CHAR(7),L.[Submitted Date],120) as 'Funded Date'
		into ##CustomerRepeat
	from 
		[Crest Business Intelligence].dbo.Customers as pad
		join [Crest Business Intelligence].dbo.Leases as L on pad.[Customer ID] = L.[Customer ID]
	Where 
		[LeaseQueueKey] in (4,5,6,12,16)

*/


SELECT
    C.[Customer ID],
	CONCAT(C.[Customer ID],'-',L.[Sequence Number]) as 'CustomerLease',
	L.[Retailer ID],
	R.[State] as 'Retailer State',
	case when (Select count(CR.[Customer ID]) from ##CustomerRepeat as CR where C.[Customer ID] = CR.[Customer ID] ) > 1 then 'Yes' else 'No' end as 'Repeat Cust',
    L.[LeaseQueuekey],
    L.[Funded Date],
    L.[Approval Amount],
    L.[Funded Amount],
    isnull(nullif(L.[Funded Amount],0)/nullif(L.[Approval Amount],0),0) as 'Utilization',
    isnull(nullif(L.[Total Paid],0)/nullif(L.[Funded Amount],0),0) as 'Multiple',
    isnull(Datediff(mm,L.[Funded Date], L.[Close Date]),Datediff(mm,L.[Funded Date], getdate())) as 'Lease Length',
    case when L.[Auto Approved] = 1 then 'Yes' else 'No' end as 'Auto Approved',
    case when L.[Met First Scheduled Payment] in ('Yes','Partially') then 'No' else 'Yes' end as 'FPD',
	C.[Monthly Income] as 'Stated Income',
	ProviderData.[Actual Income],
    C.[Age],
	C.[Zip],
	C.[State] as 'Customer State',
    case when datediff(dd,L.[Funded Date],L.[Close Date]) < 90 then 'Yes' else 'No' end as '90 Buyout',
	ProviderData.[Global Result],
	ProviderData.[IDV Result],
	ProviderData.[IDV Code],
	ProviderData.[BAV Result],
	ProviderData.[BAV Code],
	ProviderData.[BAV Pass],
	ProviderData.[BAV Pass 2],
	ProviderData.[NAS Summary],
	ProviderData.[NAP Summary],
	ProviderData.[Follow up 1],
	ProviderData.[Follow up 2],
	ProviderData.[HRI 1],
	ProviderData.[HRI 2],
	ProviderData.[HRI 3],
	ProviderData.[HRI 4],
	ProviderData.[HRI 5],
	ProviderData.[HRI 6],
	ProviderData.[HRI 7]
FROM
    [Crest Business Intelligence].dbo.Customers as C left join
		##ProviderData as ProviderData on C.[Customer ID]=ProviderData.SubjectID
			join [Crest Business Intelligence].dbo.Leases as L on C.[Customer ID] = L.[Customer ID] 
WHERE
	ProviderData.[IDV Result] is not null 
	and [Funded Date] > '1-1-2014'
ORDER BY
    [Lease Length] asc,
    [LeaseQueueKey] asc
