select
	[Retailer Name],
	count([Lease ID]) as 'Leases',
	sum([Funded Amount]) as 'Funded',
	sum([Total Paid])*1.0/sum([Funded Amount]) as 'Multiple',
	sum(case when [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)*1.0/sum([Funded Amount]) as 'FPD $',
	sum(case when [LeaseQueueKey] = 12 then [Funded Amount] else 0 end)*1.0/sum([Funded Amount]) as 'Charge Off $',
	sum(case when [LeaseQueueKey] = 5 then [Funded Amount] else 0 end)*1.0/sum([Funded Amount]) as 'Paid $',
	sum([Total Paid])-sum([Funded Amount]) as 'Profit/Loss',
	sum([Financed Amount]) as 'Financed',
	sum([Funded Amount]) as 'Funded A',
	sum([Total Paid]) as 'Paid',
	sum(L.[Application Fee]) as 'FEE'
from
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
where
	datediff(mm,[Funded Date],getdate()) >= 12 and
	[Current Status] = 'Active' 
group by
	[Retailer Name]
having
	count([Lease ID]) > 1 and
	sum([Total Paid])*1.0/sum([Funded Amount]) <= 1
order by
	[Multiple] asc

