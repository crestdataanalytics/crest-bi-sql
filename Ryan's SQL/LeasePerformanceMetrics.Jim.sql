select
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	sum(case when [LeaseQueueKey] = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then [Total Paid] else 0 end)*1.0/
		sum(case when [LeaseQueueKey] = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then [Funded Amount] else 0 end) as 'SAC',
	sum(case when [LeaseQueueKey] = 5 and datediff(dd,[Funded Date],[Close Date]) >= 90 and datediff(dd,[Funded Date],[Close Date]) < 335 then [Total Paid] else 0 end)*1.0/
		sum(case when [LeaseQueueKey] = 5 and datediff(dd,[Funded Date],[Close Date]) >= 90 and datediff(dd,[Funded Date],[Close Date]) < 335 then [Funded Amount] else 0 end) as 'EBO',
	sum(case when [LeaseQueueKey] = 5 and datediff(mm,[Funded Date],[Close Date]) = 12 then [Total Paid] else 0 end)*1.0/
		sum(case when [LeaseQueueKey] = 5 and datediff(mm,[Funded Date],[Close Date]) = 12 then [Funded Amount] else 0 end) as 'PIF',
		sum(case when [LeaseQueueKey] = 5 and datediff(mm,[Funded Date],[Close Date]) > 12 then [Total Paid] else 0 end)*1.0/
		sum(case when [LeaseQueueKey] = 5 and datediff(mm,[Funded Date],[Close Date]) > 12 then [Funded Amount] else 0 end) as 'PIF + 12',
	sum(case when [LeaseQueueKey] = 12 then [Total Paid] else 0 end)*1.0/
		sum(case when [LeaseQueueKey] = 12 then [Funded Amount] else 0 end) as 'Charge Off',
	sum(case when [LeaseQueueKey] not in (5,12) then [Total Paid] else 0 end)*1.0/
		sum(case when [LeaseQueueKey] not in (5,12) then [Funded Amount] else 0 end) as 'All Other',
	sum([Funded Amount]) as 'Total Funded',
	sum([Total Paid]) as 'Total Paid'
from Leases
where [Funded Date] between '5-1-2014' and '4-30-2015' and [Funded Amount] > 0
group by CONVERT(CHAR(7),[Funded Date],120)
order by CONVERT(CHAR(7),[Funded Date],120)