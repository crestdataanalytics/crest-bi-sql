SELECT
	CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
	CASE WHEN R.[State] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN R.[State] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN R.[State] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN R.[State] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT'
	END AS 'Region',
	sum([Financed Amount]) as 'Financed',
	sum([Funded Amount]) as 'Funded',
	count([Funded Amount]) as 'Leases',
	count(distinct case when L.[Funded Date] between DATEADD(month, -3, [Funded Date]) and [Funded Date] then R.[Retailer ID] else null end) as 'Active Doors',
	count(distinct case when (L.[Funded Date] between DATEADD(month, -3, [Funded Date]) and [Funded Date]) and r.[Retailer Group Name] is null then R.[Retailer ID] else null end)
		 + count(distinct case when (L.[Funded Date] between DATEADD(month, -3, [Funded Date]) and [Funded Date]) and r.[Retailer Group Name] is not null then r.[Retailer Group Name] else null end) as 'Retailers'
FROM 
	LEASES AS L JOIN RETAILERS AS R ON L.[Retailer ID] = R.[Retailer ID]
WHERE
	[Funded Date] BETWEEN '1-1-14' and '5-31-16' and [Funded Amount] > 0
GROUP BY
	CONVERT(CHAR(7),L.[Funded Date],120),
	CASE WHEN R.[State] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN R.[State] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN R.[State] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN R.[State] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT' END
ORDER BY
	CONVERT(CHAR(7),L.[Funded Date],120),
	CASE WHEN R.[State] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN R.[State] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN R.[State] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN R.[State] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT' END