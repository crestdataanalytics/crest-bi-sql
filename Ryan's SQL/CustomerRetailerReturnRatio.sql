select
	[Retailer Name],
	[Age],
	C.[State],
	[Zip],
	[Monthly Income],
	[Hire Date],
	L.[LeaseQueueKey],
	[Payments Charged],
	[Payments Returned],
	[Funded Amount],
	[Funded Date],
	isnull(nullif(sum([Payments Returned]),0)*1.0/nullif(sum([Payments Charged]),0),0) as 'Return Ratio',
	isnull(nullif(sum([Total Paid]),0)*1.0/nullif(sum([Funded Amount]),0),0) as 'Multiple',
	sum([Funded Amount])/nullif(sum([Approval Amount]),0) as 'Utilization'
from
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
		join Customers as C on C.[Customer ID] = L.[Customer ID]
where
	[Funded Date] between '1-1-2014' and '5-31-2016'
	and R.[Current Status] in ('Active')
group by
	[Retailer Name],
	[Age],
	C.[State],
	[Zip],
	[Monthly Income],
	[Hire Date],
	L.[LeaseQueueKey],
	[Payments Charged],
	[Payments Returned],
	[Funded Amount],
	[Funded Date]
order by
	[Return Ratio] desc