select
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	R.[Retailer ID],
	R.[Retailer Name],
	R.[Current Status],
	sum([Funded Amount]) as Funded,
	sum([Total Paid]) as Paid,
	sum(case when [Met First Scheduled Payment] in ('No') then [Funded Amount] else 0 end)*1.0/sum([Funded Amount]) as 'FPD $',
	coalesce(sum([Total Paid]),0)/nullif(sum([Funded Amount]),0) as 'Multiple',
	coalesce(count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then 1 else null end),0)*1.0/nullif(count([Lease ID]),0) as 'SAC %',
	coalesce(count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) between 90 and 364 then 1 else null end),0)*1.0/nullif(count([Lease ID]),0) as 'EBO %',
	coalesce(count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) >= 365 then 1 else null end),0)*1.0/nullif(count([Lease ID]),0) as 'PIF %',
	coalesce(count(case when LeaseQueueKey in (9,12,13,14,20,21,22) then 1 else null end),0)*1.0/nullif(count([Lease ID]),1) as 'Charge-Off %',
	coalesce(count(case when LeaseQueueKey in (4) then 1 else null end),0)*1.0/nullif(count([Lease ID]),0) as 'Open %',
	coalesce(count(case when LeaseQueueKey in (6) then 1 else null end),0)*1.0/nullif(count([Lease ID]),0) as 'Bounced %',
	coalesce(count(case when LeaseQueueKey not in (4,5,6,9,12,13,14,20,21,22) then 1 else null end),0)*1.0/nullif(count([Lease ID]),0) as 'Other %'
	
from
	Leases as L left join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
where
	[Funded Date] > '12-31-14'
group by
	[Funded Date],
	R.[Retailer ID],
	R.[Retailer Name],
	R.[Current Status]