select
	R.[Retailer Name],
	R.[Current Status],
	isnull(nullif(sum([Payments Returned]),0)*1.0/nullif(sum([Payments Charged]),0),0) as 'Return Ratio',
	isnull(nullif(sum([Total Paid]),0)*1.0/nullif(sum([Funded Amount]),0),0) as 'Multiple',
	sum([Funded Amount]) as 'Total Funded'
from
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
where
	[Funded Date] between '5-31-2014' and '5-31-2015'
	and R.[Current Status] in ('Active')
group by
	R.[Retailer Name],
	R.[Current Status]
having
	count([Funded Amount]) > 10
order by
	[Return Ratio] desc
