select 
sum([Total Note])-sum([Total Paid]) as 'Remaining Value',
count([Lease ID]) as 'Active Leases'
from [Crest Business Intelligence].dbo.Leases
where leasequeuekey in(4,6,16)
and [Funded Date] < '7/1/2016'
and ([close date] > '7/1/2016' or [close date] is null)
