select
	R.[Retailer Name],
	R.[Retailer ID],
	isnull(count([Lease ID]),0) as 'Lease Count',
	isnull(count(case when [Met First Scheduled Payment] = 'Yes' then 1 else null end),0) as 'FPD Count',
	isnull(nullif(count(case when [Met First Scheduled Payment] = 'Yes' then 1 else null end),0)*1.0/
		nullif(count([Lease ID]),0),0) as 'FPD %',
	isnull(nullif(sum(case when [Met First Scheduled Payment] = 'Yes' then [Funded Amount] else 0 end),0)*1.0/
		nullif(sum([Funded Amount]),0),0) as 'FPD $',
	sum([Funded Amount]) as 'Total Funded',
	isnull(sum(case when [Met First Scheduled Payment] = 'Yes' then [Funded Amount] else 0 end),0) as 'FPD Funded'

from Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
where
	datediff(mm,[Funded Date],getdate()) < 3 and R.[Retailer ID] = 8573
group by
	R.[Retailer Name], R.[Retailer ID]