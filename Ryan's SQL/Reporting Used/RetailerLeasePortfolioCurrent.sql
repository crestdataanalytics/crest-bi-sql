select
	[Retailer Name],
	R.[Retailer ID],
	R.[State],
	count(case when [LeaseQueueKey] in (4,6) then 1 else null end) as 'Active Leases',
	count(case when [LeaseQueueKey] in (4) then 1 else null end) as '# Funded',
	sum(case when [LeaseQueueKey] in (4) then [Total Note]-[Total Paid] else 0 end) as '$ Funded RCV',
	count(case when [LeaseQueueKey] in (6) then [Total Note] else 0 end) as '# Bounced RCV',
	sum(case when [LeaseQueueKey] in (6) then [Total Note]-[Total Paid] else 0 end) as '$ Bounced RCV',
	sum([Total Paid]) as 'Total Paid',
	sum([Funded Amount]) as 'Funded Amount',
	isnull(nullif(sum([Total Paid]),0)/nullif(sum([Funded Amount]),0),0) as 'Multiple',
	count(case when [Met First Scheduled Payment] in ('No') then 1 else null end) as 'FPD #',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when [Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD %',
	sum(case when datediff(mm,[Funded Date],getdate()) = 0 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.06
		when datediff(mm,[Funded Date],getdate()) = 1 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.25
		when datediff(mm,[Funded Date],getdate()) = 2 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.44
		when datediff(mm,[Funded Date],getdate()) = 3 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.62
		when datediff(mm,[Funded Date],getdate()) = 4 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.71
		when datediff(mm,[Funded Date],getdate()) = 5 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.79
		when datediff(mm,[Funded Date],getdate()) = 6 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.88
		when datediff(mm,[Funded Date],getdate()) = 7 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.95
		when datediff(mm,[Funded Date],getdate()) = 8 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.02
		when datediff(mm,[Funded Date],getdate()) = 9 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.09
		when datediff(mm,[Funded Date],getdate()) = 10 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.15
		when datediff(mm,[Funded Date],getdate()) = 11 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.21
		when datediff(mm,[Funded Date],getdate()) = 12 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.25
		else nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.25 end)/count([Lease ID]) as 'Target vs. Expected Multiple'
From Retailers as R
join Leases as L on L.[Retailer ID] = R.[Retailer ID]
where
	[Retailer Group Name] = 'Gardner-White' and
	[Close Date] is null
	and [Funded Amount] > 0
Group by	
	 R.[Retailer ID], [Retailer Name], R.[state]

UNION ALL

Select
	null,
	null,
	null,
	count(case when [LeaseQueueKey] in (4,6) then 1 else null end) as 'Active Leases',
	count(case when [LeaseQueueKey] in (4) then 1 else null end) as '# Funded',
	sum(case when [LeaseQueueKey] in (4) then [Total Note]-[Total Paid] else 0 end) as '$ Funded RCV',
	count(case when [LeaseQueueKey] in (6) then [Total Note] else 0 end) as '# Bounced RCV',
	sum(case when [LeaseQueueKey] in (6) then [Total Note]-[Total Paid] else 0 end) as '$ Bounced RCV',
	sum([Total Paid]) as 'Total Paid',
	sum([Funded Amount]) as 'Funded Amount',
	isnull(nullif(sum([Total Paid]),0)/nullif(sum([Funded Amount]),0),0) as 'Multiple',
	count(case when [Met First Scheduled Payment] in ('No') then 1 else null end) as 'FPD #',
	isnull((nullif(count(case when [Met First Scheduled Payment] in ('No') then 1 else null end),0)*1.0
		/nullif(count(case when [Funded Amount] > 0 then 1 else null end),0)),0) as 'FPD %',
	sum(case when datediff(mm,[Funded Date],getdate()) = 0 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.06
		when datediff(mm,[Funded Date],getdate()) = 1 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.25
		when datediff(mm,[Funded Date],getdate()) = 2 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.44
		when datediff(mm,[Funded Date],getdate()) = 3 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.62
		when datediff(mm,[Funded Date],getdate()) = 4 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.71
		when datediff(mm,[Funded Date],getdate()) = 5 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.79
		when datediff(mm,[Funded Date],getdate()) = 6 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.88
		when datediff(mm,[Funded Date],getdate()) = 7 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-.95
		when datediff(mm,[Funded Date],getdate()) = 8 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.02
		when datediff(mm,[Funded Date],getdate()) = 9 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.09
		when datediff(mm,[Funded Date],getdate()) = 10 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.15
		when datediff(mm,[Funded Date],getdate()) = 11 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.21
		when datediff(mm,[Funded Date],getdate()) = 12 then nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.25
		else nullif((isnull([Total Paid],0)/[Funded Amount]),0)-1.25 end)/count([Lease ID]) as 'Target vs. Expected Multiple'
From Retailers as R
join Leases as L on L.[Retailer ID] = R.[Retailer ID]
where
	[Retailer Group Name] = 'Gardner-White' and
	[Close Date] is null
	and [Funded Amount] > 0