drop table ##EligibleCustomers
	select 
		pad.SubjectID
		,max(case when padv.VariableID = 39767 then padv.VariableValue else null end) as 'EBgrab'
		into ##EligibleCustomers
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'Ebureau'
		group by
			pad.SubjectID

drop table ##RepeatCust
	select 
		pad.[Customer ID],
		L.[Retailer ID],
		CONVERT(CHAR(7),L.[Submitted Date],120) as 'Funded Date'
		into ##RepeatCust
	from 
		[Crest Business Intelligence].dbo.Customers as pad
		join [Crest Business Intelligence].dbo.Leases as L on pad.[Customer ID] = L.[Customer ID]
	Where 
		[LeaseQueueKey] in (4,5,6,12,16)