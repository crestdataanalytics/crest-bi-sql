declare @TFPD decimal(18,2)
declare @TCO decimal(18,2)
declare @TP decimal(18,2)
declare @OYM decimal(18,2)
declare @OCM decimal(18,2)
declare @AVT decimal(18,2)
declare @QFPD decimal(18,2)
declare @Cat varchar(25)

set @TFPD = 0.25
set @TCO = 0.0
set @TP = 0.30
set @OYM = 1.25
set @OCM = 0
set @AVT = -0.1
set @QFPD = 0.0
set @cat = 'Jewelry'

drop table ##temp

create table ##temp([3 mo. FPD $] decimal(18,2),[3 mo. Charge-Off %] decimal(18,2), [3 mo. Paid %] decimal(18,2),
	[One Year Multiple] decimal(18,2),[Overall Closing Multiple] decimal(18,2),[Actual vs. Target] decimal(18,2),
	[Quarterly FPD vs Volume %] decimal(18,2),Category varchar(25))

Insert into ##temp([3 mo. FPD $],[3 mo. Charge-Off %], [3 mo. Paid %] ,
	[One Year Multiple],[Overall Closing Multiple],[Actual vs. Target],
	[Quarterly FPD vs Volume %],Category)
	select @TFPD,@TCO,@TP,@OYM,@OCM,@AVT,@QFPD,@Cat


drop table ##ProductLimits

create table ##ProductLimits(CategoryName varchar(255), LowerLimit integer, Average integer, MaxLimit int)

insert into ##ProductLimits values('Bikes',200,1100,2500)
insert into ##ProductLimits values('Medical Supplies',200,1100,2500)
insert into ##ProductLimits values('Golf',200,1100,2500)
insert into ##ProductLimits values('Exercise Equipment',200,1100,2500)
insert into ##ProductLimits values('Appliance/Electronics/Video',200,3500,5000)
insert into ##ProductLimits values('Appliance',200,3500,5000)
insert into ##ProductLimits values('Jewelry',200,1800,1800)
insert into ##ProductLimits values('Furniture',200,5000,5000)
insert into ##ProductLimits values('Mattress',200,5000,5000)
insert into ##ProductLimits values('Car Audio',200,2500,5000)
insert into ##ProductLimits values('Tires/Rims or Tires / Rims',200,2500,5000)
insert into ##ProductLimits values('All Other',200,1250,2500)

select
(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*5
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*4
				)
				+ 
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/100.00*MaxLimit 'Max Approval Amount',
			(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*5
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*4
				)
				+ 
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/100.00 'Max Approval Score'
from ##temp join ##ProductLimits on ##temp.Category = ##ProductLimits.CategoryName