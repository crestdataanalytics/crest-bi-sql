
drop table ##CrystalBallTemp
select
	DI.SubjectID as 'CustomerID',
	max(case when dv.[variableID] = 45993 then dv.[variablevalue] end) as 'Result',
	max(case when dv.[variableID] = 45994 then dv.[variablevalue] end) as 'Node',
	max(case when dv.[variableID] = 45995 then dv.[variablevalue] end) as 'Factor',
	max(case when dv.[variableID] = 45996 then dv.[variablevalue] end) as 'Score',
	max(case when dv.[variableID] = 45997 then dv.[variablevalue] end) as 'Amount'
	into ##CrystalBallTemp
from ProviderAnalysis.dbo.DocumentVariables as DV
	join ProviderAnalysis.dbo.DocumentInfos as DI on DV.documentid = DI.DocumentID
	join ProviderAnalysis.dbo.Providers as P on P.ProviderID = DI.ProviderID
	join ProviderAnalysis.dbo.Variables as V on V.variableid = DV.variableID
where p.providerid = 14
group by di.subjectid

declare @sDate as date
set @sDate = '6-29-16'

SELECT
	c.*,
	l.[submitted date],
	l.[leasequeuekey],
	cu.[Monthly Income],
	l.[approval amount],
	case when l.[Funded Amount] >0 and l.[approval amount] >0 then l.[Funded amount]/l.[approval amount] end as Utilization,
	c.[amount]-l.[approval amount] as 'Approval Diff',
	r.[Retailer Name],
	r.[Retailer ID]
from ##CrystalBallTemp as c  right join Leases as l on c.customerID = L.[Customer ID]
	join retailers as r on r.[retailer id] = l.[retailer id]
	join Customers as cu on cu.[Customer ID] = l.[Customer ID]
where [submitted date] >= @sdate
order by
	l.leasequeuekey asc

select
	[submitted date],
	count(case when leasequeuekey = 4 then 1 else null end) as '# Funded',
	count(case when leasequeuekey not in (7,11) and [auto approved] = 1 then 1 else null end) as '# Approved',
	count(case when c.result in (1,2) then c.result else null end) as 'Applicants',
	count(case when c.result = 1 and leasequeuekey <> 11 then 1 else null end)*1.0/
		count(case when c.result in (1,2) and leasequeuekey <> 11 then 1 else null end) as 'Model Approved Rate',
	count(case when c.result = 2 then 1 else null end)*1.0/count(c.result) as 'Denied Rate',
	count(case when c.result = 3 then 1 else null end)*1.0/count(c.result) as 'Flagged Rate',
	count(case when leasequeuekey not in (7,11,8) and [Auto Approved] = 1 then 1 else null end)*1.0/
		count(case when leasequeuekey not in (11) then 1 else null end) as 'Old Approved Rate',
	sum(case when c.result in (1,2) then convert(decimal(18,2),c.amount) else 0 end) as 'Model Approved Amount',
	sum(case when c.result in (1,2) then convert(decimal(18,2),c.amount) else 0 end)*1.0/count(case when c.result in (1,2) and leasequeuekey <> 11 then 1 else null end) as 'Model Avg. Approved Amount',
	sum(case when leasequeuekey not in (7,11,8) and [Auto Approved] = 1 then l.[Approval Amount] else 0 end) as 'Old Approved Amount',
	sum(case when leasequeuekey not in (7,11,8) and [Auto Approved] = 1 then l.[Approval Amount] else 0 end)*1.0/count(case when leasequeuekey not in (7,11,8) and [Auto Approved] = 1 then 1 else null end) as 'Old Avg. Approved Amount',
	sum(case when l.[Funded amount]>0 then l.[Funded amount] end)/sum(case when l.[Funded amount]>0 then l.[approval amount] end) as 'Avg. Utilization'
from ##CrystalBallTemp as c right join Leases as l on c.customerID = L.[Customer ID]
	join retailers as r on r.[retailer id] = l.[retailer id]
where [submitted date] >= @sdate and [submitted date] < Convert(date, getdate())
group by [submitted date]
order by [submitted date]

-- Funded leases where model approval was less than the funded
SELECT
	c.*,
	l.[submitted date],
	l.[leasequeuekey],
	l.[approval amount],
	l.[Funded amount],
	l.[Funded amount]/l.[approval amount] as Utilization,
	c.[amount]-l.[approval amount] as 'Approval Diff',
	r.[Retailer Name]
from ##CrystalBallTemp as c  join Leases as l on c.customerID = L.[Customer ID]
	join retailers as r on r.[retailer id] = l.[retailer id]
where [submitted date] >= @sdate
	and leasequeuekey = 4
	and (c.[amount]-l.[funded amount]) < 0
order by
	l.leasequeuekey asc

-- Funded leases where model approval was more than the funded
SELECT
	c.*,
	l.[submitted date],
	l.[leasequeuekey],
	l.[approval amount],
	l.[Funded amount],
	l.[Funded amount]/l.[approval amount] as Utilization,
	c.[amount]-l.[approval amount] as 'Approval Diff',
	r.[Retailer Name]
from ##CrystalBallTemp as c  join Leases as l on c.customerID = L.[Customer ID]
	join retailers as r on r.[retailer id] = l.[retailer id]
where [submitted date] >= @sdate
	and leasequeuekey = 4
	and (c.[amount]-l.[funded amount]) > 0
	and (l.[Funded amount]/l.[approval amount]) > 0.7
order by
	l.leasequeuekey asc


