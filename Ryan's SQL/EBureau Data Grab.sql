select top 100
	[SubjectID],
	[VariableValue],
	[VariableName],
	pav.[VariableID],
	[VariableName]
from [ProviderAnalysis].dbo.DocumentInfos as pad 
	join [ProviderAnalysis].dbo.DocumentVariables as padv on
	pad.DocumentID = padv.DocumentID
	join [ProviderAnalysis].dbo.Variables as pav on
	padv.VariableID = pav.VariableID
	join [ProviderAnalysis].dbo.Providers as pap on
	pad.providerID = pap.providerId
where
	pav.VariableName like '%bav%' and
	ProviderName = 'datax' --and SubjectID in (987588) -- and pav.[VariableID] in (39778,45953)
	--and pav.VariableID =  
order by SubjectID

/*

select *
from [ProviderAnalysis].dbo.DocumentInfos as pad 
	join [ProviderAnalysis].dbo.DocumentVariables as padv on
	pad.DocumentID = padv.DocumentID
	join [ProviderAnalysis].dbo.Variables as pav on
	padv.VariableID = pav.VariableID
	join [ProviderAnalysis].dbo.Providers as pap on
	pad.providerID = pap.providerId
where 
	pav.VariableName like '%code%' and
	ProviderName = 'EBureau' and SubjectID in (1323238) --and pav.[VariableID] in (39778,45953)
	--pav.VariableID = 7266
order by SubjectID

*/

/*

drop table ##EBData
	select 
		pad.SubjectID
		,max(case when padv.VariableID = 39778 then padv.VariableValue else null end) as 'CrestScore'
		,max(case when padv.VariableID = 45953 then padv.VariableValue else null end) as 'EBScore'
		,max(case when padv.VariableID = 39783 then padv.VariableValue else null end) as 'LastNameMatch'
		,max(case when padv.VariableID = 39790 then padv.VariableValue else null end) as 'SocialMatch'
		,max(case when padv.VariableID = 39809 then padv.VariableValue else null end) as 'EmailMatch'
		,max(case when padv.VariableID = 45959 then padv.VariableValue else null end) as 'CompanyMatch'
		,max(case when padv.VariableID = 39782 then padv.VariableValue else null end) as 'FirstNameMatch'
		,max(case when padv.VariableID = 39808 then padv.VariableValue else null end) as 'AddressMatch'
		,max(case when padv.VariableID = 45951 then padv.VariableValue else null end) as 'WorkPhoneMatch'
		,max(case when padv.VariableID = 39748 then padv.VariableValue else null end) as 'dobMatch'
		,max(case when padv.VariableID = 39759 then padv.VariableValue else null end) as 'HomePhone2Match'
		,max(case when padv.VariableID = 39797 then padv.VariableValue else null end) as 'Message1'
		,max(case when padv.VariableID = 39796 then padv.VariableValue else null end) as 'Message2'
		,max(case when padv.VariableID = 39819 then padv.VariableValue else null end) as 'Message3'
		,max(case when padv.VariableID = 39764 then padv.VariableValue else null end) as 'Message4'
		,max(case when padv.VariableID = 39798 then padv.VariableValue else null end) as 'Message5'
		into ##EBData
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'Ebureau'
		group by
			pad.SubjectID

----

SELECT
    C.[Customer ID],
	CONCAT(C.[Customer ID],'-',L.[Sequence Number]) as 'CustomerLease',
	L.[Submitted Date],
    L.[LeaseQueuekey],
    L.[Funded Date],
	L.[Close Date],
    L.[Approval Amount],
    L.[Funded Amount],
    isnull(nullif(L.[Funded Amount],0)/nullif(L.[Approval Amount],0),0) as 'Utilization',
    isnull(nullif(L.[Total Paid],0)/nullif(L.[Funded Amount],0),0)as 'Multiple',
    isnull(Datediff(mm,L.[Funded Date], L.[Close Date]),Datediff(mm,L.[Funded Date], getdate())) as 'Lease Length',
    case when L.[Auto Approved] = 1 then 'Yes' else 'No' end as 'Auto Approved',
    case when L.[Met First Scheduled Payment] in ('Yes','Partially') then 'No' 
		when L.[Met First Scheduled Payment] in ('No') then 'Yes'  else 'NULL' end as 'FPD',
    C.[Age],
	C.[Hire Date],
	datediff(mm,C.[Hire Date],L.[Submitted Date]) as 'Time at job',
    case when datediff(dd,L.[Funded Date],L.[Close Date]) < 90 then 'Yes' else 'No' end as '90 Buyout',
    C.[State],
	ProviderData.[LastNameMatch],
	ProviderData.[SocialMatch],
	ProviderData.[EmailMatch],
	ProviderData.[CompanyMatch],
	ProviderData.[FirstNameMatch],
	ProviderData.[AddressMatch],
	ProviderData.[WorkPhoneMatch],
	ProviderData.[dobMatch],
	ProviderData.[HomePhone2Match],
	ProviderData.[Message1],
	ProviderData.[Message2],
	ProviderData.[Message3],
	ProviderData.[Message4],
	ProviderData.[Message5],
	ProviderData.[CrestScore],
	ProviderData.[EBScore]
FROM
    [Crest Business Intelligence].dbo.Customers as C left join
		##EBData as ProviderData on C.[Customer ID]=ProviderData.SubjectID
			left join [Crest Business Intelligence].dbo.Leases as L on C.[Customer ID] = L.[Customer ID]
WHERE
	L.[Submitted Date] between '1-1-2015' and '5-31-2016'
ORDER BY
	C.[Customer ID],L.[Submitted Date]

*/