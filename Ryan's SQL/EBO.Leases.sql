select
	R.[State],
	count([Lease ID]) as 'Total Leases',
	count(case when (DATEDIFF(dd,[Funded Date],[Close Date]) between 91 and 364) and (([Total Paid]/[Total Note]) between 0.26 and 0.8) and (LeaseQueueKey = 5) then 1 else null end) as 'EBO Leases'
from Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
where [Funded Date] between '1-1-15' and '12-31-15'
group by R.[State]
order by R.[State] asc
