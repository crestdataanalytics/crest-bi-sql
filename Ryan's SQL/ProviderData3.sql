/*	
	DROP TABLE ##ProviderData
	select 
		pad.SubjectID
		,max(case when padv.VariableID = 8020 then padv.VariableValue else null end) as 'DR Score'
		,max(case when padv.VariableID = 8031 then padv.VariableValue else null end) as 'Account Closure Reason'
		,max(case when padv.VariableID = 8001 then padv.VariableValue else null end) as 'Days Since First Closure'
		,max(case when padv.VariableID = 8026 then padv.VariableValue else null end) as 'Days since last DR inquiry'
		,max(case when padv.VariableID = 7981 then padv.VariableValue else null end) as 'Account Close Amount'
		,max(case when padv.VariableID = 8118 then padv.VariableValue else null end) as '5 year account closure'
		,max(case when padv.VariableID = 8141 then padv.VariableValue else null end) as '3 year account closure'
		,max(case when padv.VariableID = 8175 then padv.VariableValue else null end) as '1 year account closure'
		,max(case when padv.VariableID = 8117 then padv.VariableValue else null end) as '60 day account closure'
		,max(case when padv.VariableID = 8140 then padv.VariableValue else null end) as '30 day account closure'
		,max(case when padv.VariableID = 8123 then padv.VariableValue else null end) as '2 year DR inquiry'
		,max(case when padv.VariableID = 8146 then padv.VariableValue else null end) as '180 day DR inquiry'
		,max(case when padv.VariableID = 8056 then padv.VariableValue else null end) as '60 day DR inquiry'
		,max(case when padv.VariableID = 7851 then padv.VariableValue else null end) as '30 day DR inquiry'
		,max(case when padv.VariableID = 6302 then padv.VariableValue else null end) as '180 day CR inquiry'
		,max(case when padv.VariableID = 6454 then padv.VariableValue else null end) as '90 day CR inquiry'
		,max(case when padv.VariableID = 6475 then padv.VariableValue else null end) as '60 day CR inquiry'
		,max(case when padv.VariableID = 6399 then padv.VariableValue else null end) as '30 day CR inquiry'
		,max(case when padv.VariableID = 6323 then padv.VariableValue else null end) as '7 day CR inquiry'
		,max(case when padv.VariableID = 6301 then padv.VariableValue else null end) as '3 day CR inquiry'
		,max(case when padv.VariableID = 6346 then padv.VariableValue else null end) as '180 day ach return'
		,max(case when padv.VariableID = 6359 then padv.VariableValue else null end) as '90 day ach return'
		,max(case when padv.VariableID = 6373 then padv.VariableValue else null end) as '60 day ach return'
		,max(case when padv.VariableID = 6869 then padv.VariableValue else null end) as '30 day ach return'
		,max(case when padv.VariableID = 6386 then padv.VariableValue else null end) as '14 day ach return'
		,max(case when padv.VariableID = 6269 then padv.VariableValue else null end) as '7 day ach return'
		,max(case when padv.VariableID = 6892 then padv.VariableValue else null end) as '3 day ach return'
		,max(case when padv.VariableID = 6403 then padv.VariableValue else null end) as '60 day nsf return'
		,max(case when padv.VariableID = 6306 then padv.VariableValue else null end) as '30 day nsf return'
		,max(case when padv.VariableID = 7147 then padv.VariableValue else null end) as '14 day nsf return'
		,max(case when padv.VariableID = 6367 then padv.VariableValue else null end) as '7 day nsf return'
		,max(case when padv.VariableID = 6487 then padv.VariableValue else null end) as '60 day vendor inquiry'
		,max(case when padv.VariableID = 7523 then padv.VariableValue else null end) as '30 day vendor inquiry'
		,max(case when padv.VariableID = 6437 then padv.VariableValue else null end) as '14 day vendor inquiry'
		,max(case when padv.VariableID = 6593 then padv.VariableValue else null end) as '7 day vendor inquiry'
		into ##ProviderData
		from 
		[ProviderAnalysis].dbo.DocumentInfos as pad 
			inner join [ProviderAnalysis].dbo.DocumentVariables as padv on pad.DocumentID = padv.DocumentID
			inner join [ProviderAnalysis].dbo.Variables as pav on padv.VariableID = pav.VariableID
			inner join [ProviderAnalysis].dbo.Providers as pap on pad.providerID = pap.providerId
		Where 
		    pap.ProviderName = 'DataX'
			and GenerationDate > '1-1-2015'
		group by
			pad.SubjectID
*/

/*

	select 
		pad.[Customer ID],
		L.[Retailer ID],
		CONVERT(CHAR(7),L.[Submitted Date],120) as 'Funded Date'
		into ##CustomerRepeat
	from 
		[Crest Business Intelligence].dbo.Customers as pad
		join [Crest Business Intelligence].dbo.Leases as L on pad.[Customer ID] = L.[Customer ID]
	Where 
		[LeaseQueueKey] in (4,5,6,12,16)

*/


SELECT
    C.[Customer ID],
	CONCAT(C.[Customer ID],'-',L.[Sequence Number]) as 'CustomerLease',
	L.[Retailer ID],
	case when (Select count(CR.[Customer ID]) from ##CustomerRepeat as CR where C.[Customer ID] = CR.[Customer ID] ) > 1 then 'Yes' else 'No' end as 'Repeat Cust',
    L.[LeaseQueuekey],
    L.[Funded Date],
    L.[Approval Amount],
    L.[Funded Amount],
    isnull(nullif(L.[Funded Amount],0)/nullif(L.[Approval Amount],0),0) as 'Utilization',
    isnull(nullif(L.[Total Paid],0)/nullif(L.[Funded Amount],0),0) as 'Multiple',
    isnull(Datediff(mm,L.[Funded Date], L.[Close Date]),Datediff(mm,L.[Funded Date], getdate())) as 'Lease Length',
    case when L.[Auto Approved] = 1 then 'Yes' else 'No' end as 'Auto Approved',
    case when L.[Met First Scheduled Payment] in ('Yes','Partially') then 'No' else 'Yes' end as 'FPD',
	C.[Monthly Income] as 'Stated Income',
    C.[Age],
	C.[Zip],
	C.[State] as 'Customer State',
    case when datediff(dd,L.[Funded Date],L.[Close Date]) < 90 then 'Yes' else 'No' end as '90 Buyout',
	[Submitted Date],
	ProviderData.*
FROM
    [Crest Business Intelligence].dbo.Customers as C left join
		##ProviderData as ProviderData on C.[Customer ID]=ProviderData.SubjectID
			join [Crest Business Intelligence].dbo.Leases as L on C.[Customer ID] = L.[Customer ID]
where
	[Submitted Date] > '1-1-2015'
ORDER BY
    [Lease Length] asc,
    [LeaseQueueKey] asc
