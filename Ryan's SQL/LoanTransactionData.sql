SELECT
	sum(L.[FundedAmount]) as 'Funded Amount',
	sum(case when [ReturnDate] is null then LP.[Amount] else 0 end) as 'Amount Paid'
from Loans as L join LoanPayments as LP on L.ID = LP.LoanID
where
	L.[FundedDate] between '1-1-15' and '1-31-15' and
	LP.PaymentDate between '1-1-15' and '5-31-16'

SELECT
	sum(L.[FundedAmount]) as 'Funded Amount'
from vw_loans_reporting as L
where
	L.[FundedDate] between '1-1-15' and '2-1-15'
