SELECT
	CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	count([Lease ID]) as 'Leases',
	sum(datediff(dd,[Funded Date],getdate()))/count([Lease ID]) as 'Avg. Days Since Funded',
	sum([Funded Amount]) as 'Funded'
FROM Leases
WHERE
	LeaseQueueKey = 6 and [Close Date] is null and [Days Late] between 1 and 30
GROUP BY
	CONVERT(CHAR(7),[Funded Date],120)
ORDER BY
	CONVERT(CHAR(7),[Funded Date],120) asc