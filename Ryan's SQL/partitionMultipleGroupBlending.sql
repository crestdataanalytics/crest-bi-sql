alter table blended1
add region varchar(50)

update blended1
set multGroup = 
	(case when [Multiple] = 0 then '0'
	when [Multiple] > 0 and [Multiple] <= 0.5 then '0-0.5'
	when [Multiple] > 0.5 and [Multiple] <= 0.8 then '0.5-0.8'
	when [Multiple] > 0.8 and [Multiple] <= 1 then '0.8-1'
	when [Multiple] > 1 and [Multiple] <= 1.1 then '1-1.1'
	when [Multiple] > 1.1 and [Multiple] <= 1.2 then '1.1-1.2'
	when [Multiple] > 1.2 and [Multiple] <= 1.3 then '1.2-1.3'
	when [Multiple] > 1.3 and [Multiple] <= 1.45 then '1.3-1.45'
	when [Multiple] > 1.45 and [Multiple] <= 1.65 then '1.45-1.65'
	when [Multiple] > 1.65 and [Multiple] <= 1.9 then '1.65-1.9'
	when [Multiple] > 1.9 then '1.9+'
	else 'na' end)