/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
	   [Lease ID]
      ,[Retailer Name]
	  ,L.[Retailer ID]
      ,L.[Customer ID]
      ,[Submitted Date]
      ,[Funded Date]
      ,[Close Date]
      ,[LeaseQueueKey]
      ,[Sequence Number]
      ,[Approval Amount]
      ,[Auto Approved]
      ,[Funded Amount]
      ,[Payments Charged]
      ,[Payments Returned]
      ,[Payments Cleared]
      ,[Refunds Issued]
      ,[Met First Scheduled Payment]
      ,[Amount Refunded]
      ,[Total Paid]
      ,[Fees Applied]
      ,[Days Late]
	  ,[Age]
	  ,C.[State]
	  ,[zip]
	  ,[monthly income]
	  ,[hire date]
  FROM Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
	join Customers as C on L.[Customer ID] = C.[Customer ID]
  where [funded date] between '5-31-2014' and '5-31-2015'