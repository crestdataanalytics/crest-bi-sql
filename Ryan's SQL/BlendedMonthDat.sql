select
	r.CustomerLease,
	p.[Pay Month1],
	r.[Funded Amount],
	p.[Amount Paid],
	p.[avg  Total Paid],
	isnull(nullif(p.[avg  total paid],0)*1.0/nullif(r.[funded amount],0),0) as 'Month Multiple',
	r.[Repeat Cust],
	r.[Funded Date],
	r.[Approval Amount],
	r.[Utilization],
	r.Multiple,
	r.[Lease Length],
	r.[Auto Approved],
	r.FPD,
	r.[90 Buyout],
	r.[Bank Account Closed],
	r.region,
	r.LeaseQueuekey,
	r.[Stated Income],
	r.Age,
	r.[Product Category]
from 
	payMonthData as p join blendedRainMaker as r on p.[display ID] = r.[CustomerLease]
order by
	r.CustomerLease,
	p.[Pay Month1]