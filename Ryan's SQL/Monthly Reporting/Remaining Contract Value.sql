SELECT
	sum	([Total paid]-[total note]) as 'RCV'
FROM
	[Crest Business Intelligence].dbo.Leases
WHERE
	LeaseQueueKey in (9,12,13,14,20,22)
	and
	[Close Date] between '6/1/2016' and '6/30/2016'