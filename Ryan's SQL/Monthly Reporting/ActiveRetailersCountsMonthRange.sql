WITH dt AS (
SELECT DISTINCT DATEADD(mm,DATEDIFF(mm,0,[Funded Date]),0) AS [Funded Date]
  FROM leases
)
SELECT CONVERT(CHAR(7),dt.[funded Date],120) as Vintage
     , COUNT(DISTINCT case when r.[retailer group name] <> '' then r.[Retailer Group Name] else null end)
		+ COUNT(DISTINCT case when r.[retailer group name] ='' then r.[Retailer ID] else null end)AS RetailerCount
	 , CASE WHEN r.[State] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN r.[State] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN r.[State] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN r.[State] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT' END AS 'Region'
  FROM leases as s join retailers as r on s.[retailer ID] = r.[Retailer ID]
 INNER JOIN dt
    ON s.[Funded Date] >= DATEADD(MM, -2, dt.[Funded Date])
   AND s.[Funded Date] < DATEADD(MM, 1, dt.[Funded Date])
 GROUP BY CONVERT(CHAR(7),dt.[funded Date],120)
	 , CASE WHEN r.[State] in ('WA','OR','CA','NV','ID','UT','AZ','AK','HI','MT','CO','NM') THEN 'WEST'
		WHEN r.[State] in ('ND','SD','NE','KS','OK','TX','IA','MO','AR','LA','MS') THEN 'CENTRAL'
		WHEN r.[State] in ('OH','KY','WV','MD','VA','DE','TN','NC','SC','AL','GA') THEN 'MID-EASTERN'
		WHEN r.[State] in ('FL','IL','IN','MI','PA','NY','CT','MA','RI','NH','ME') THEN 'EAST(NE+FL)'
		ELSE 'NCT' END 
 ORDER BY 1, 2