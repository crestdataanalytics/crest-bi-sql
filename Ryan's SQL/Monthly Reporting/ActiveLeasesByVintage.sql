select
	--CONVERT(CHAR(7),[funded Date],120) as Vintage,
	region,
	count([Lease ID]) as 'Leases'
from
	Leases as l join retailers as r on l.[retailer id] = r.[retailer id]
where
	[Funded Amount] > 0 and [Funded Date] <= '7-31-16' and ((leasequeuekey in (4,6,16)) or (leasequeuekey in (5,12,13,14,20,21,22) and [close date] > '7-31-16'))
group by
	region
