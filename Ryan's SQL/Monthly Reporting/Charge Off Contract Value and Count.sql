select 
sum([Total Note])-sum([Total Paid]) as 'Remaining Value',
count([Lease ID]) as 'Leases Charged Off'
from [Crest Business Intelligence].dbo.Leases
where leasequeuekey in(12)
and [Funded Date] < '7/1/2016'
and [close date] between '6/1/2016' and '6/30/2016'
