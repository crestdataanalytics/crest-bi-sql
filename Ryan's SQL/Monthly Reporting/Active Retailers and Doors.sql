-- Active Retailers with > 1 location
select 
	distinct [Retailer Group Name]
from (
		select
			   [Retailer Group Name],	
			   count(*) over(partition by [Retailer Group Name]) as dc
			   from Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
			   where [Funded Date] > '1-1-2016'
			   ) as T
where dc > 1

/*
-- Active Retailers w/1 location
select 
	distinct [Retailer Name]
from 
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
where 
	[Funded Date] > '1-1-2016' and [Retailer Group Name] is null
*/

/*
-- Active Doors
select 
	distinct R.[Retailer ID]
from 
	Retailers as R join Leases as L on R.[Retailer ID] = L.[Retailer ID]
where 
	[Funded Date] between '12
*/