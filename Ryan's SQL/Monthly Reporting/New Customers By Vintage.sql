;WITH FirstDate As
(   -- Gets the first order date for each active customer
    SELECT   [Customer ID], 
             MIN([Funded Date])        as FirstOrderDate,
             YEAR(MIN([Funded Date]))  as Yr,
             MONTH(MIN([Funded Date])) as Mnth
    FROM     Leases
    GROUP BY [Customer ID]
)
-- Groups the first order dates into Year and Month
SELECT   Yr,
         Mnth,
         COUNT([Customer ID]) As cnt
FROM     FirstDate
GROUP BY Yr, Mnth
ORDER BY Yr, Mnth