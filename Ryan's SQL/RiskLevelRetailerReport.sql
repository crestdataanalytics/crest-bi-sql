If OBJECT_ID('tempdb..##TempRetailer') is not null
drop table ##TempRetailer

select
	R.[Retailer Name],
	R.[Retailer ID],
	R.[State],
	sum(case when datediff(dd,[Funded Date],getdate()) < 90 then [Funded Amount] else 0 end) as '3 mo. Funded',	
	isnull(nullif(sum(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 then [Funded Amount] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 and [Met First Scheduled Payment] in ('No','Yes','Partially') then [Funded Amount] else 0 end),0),0) as '3 mo. FPD $',
	isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) >= 1 and datediff(mm,[Funded Date],getdate()) < 4 then 1 else null end),0)*1.0/	
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0) as 'Quarterly FPD % Increase',
	isnull(nullif(count(case when [LeaseQueueKey] = 6 and datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/	
		nullif(count(case when [Close Date] is null and datediff(mm,[Funded Date],getdate())  <= 3 then 1 else null end),0),0) as '3 mo. Bounced %',
	isnull(nullif(count(case when [LeaseQueueKey] = 12 and datediff(mm,[Funded Date],getdate()) between 5 and 8 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(mm,[Funded Date],getdate()) between 5 and 8 then 1 else null end),0),0) as '3 mo. Charge-Off %',
	isnull(nullif(count(case when [LeaseQueueKey] = 5 and datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0)*1.0/	
		nullif(count(case when datediff(dd,[Close Date],getdate()) <= 90 then 1 else null end),0),0) as '3 mo. Paid %',
	isnull(nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(dd,[Close Date],getdate()) < 90 then [Funded Amount] else 0 end),0),0) as '3 mo. Closing Multiple',
	isnull(nullif(sum(case when [Close Date] is not null then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when [Close Date] is not null then [Funded Amount] else 0 end),0),0) as 'Overall Closing Multiple',
	isnull(nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Total Paid] else 0 end),0)*1.0/	
		nullif(sum(case when datediff(mm,[Funded Date],getdate()) between 12 and 24 then [Funded Amount] else 0 end),0),0) as 'One Year Multiple',
	count(case when datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end) as '3 mo Volume',	
	count(case when datediff(mm,[Funded Date],getdate()) <= 6 then 1 else null end) as '6 mo Volume',	
	count(case when datediff(mm,[Funded Date],getdate()) <= 12 then 1 else null end) as '12 mo Volume',	
	isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0) as 'Quarterly Volume Increase',
	isnull(nullif(count(case when datediff(mm,[Submitted Date],getdate()) <= 3 and [LeaseQueueKey] not in (7,11) then 1 else null end),0)*1.0/	
		 nullif(count(case when datediff(mm,[Submitted Date],getdate()) <=3 then 1 else null end),0),0) as '3 mo. Approval Rate',
	(isnull(nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) <=3 then 1 else null end),0)*1.0/
		nullif(count(case when [Met First Scheduled Payment] = 'No' and datediff(mm,[Funded Date],getdate()) between 4 and 7 then 1 else null end),0),0))-
	 (isnull(nullif(count(case when datediff(mm,[Funded Date],getdate()) <= 3 then 1 else null end),0)*1.0/
		 nullif(count(case when datediff(mm,[Funded Date],getdate()) between 4 and 7  then 1 else null end),0),0)) as 'Quarterly FPD vs Volume %',
	sum(case when datediff(mm,[Funded Date],getdate()) >= 12 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],getdate()) = 11 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],getdate()) = 10 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],getdate()) = 9 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],getdate()) = 8 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],getdate()) = 7 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],getdate()) = 6 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],getdate()) = 5 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],getdate()) = 4 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],getdate()) = 3 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],getdate()) = 2 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],getdate()) = 1 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],getdate()) = 0 then coalesce(([Total Paid])/nullif(([Funded Amount]),0),0) - 0.06
		else 0
		end)/count([Lease ID]) as 'Actual vs. Target'
	into ##TempRetailer
from
	CFREPORTS.[Crest Business Intelligence].dbo.Retailers as R join CFREPORTS.[Crest Business Intelligence].dbo.Leases as L on R.[Retailer ID] = L.[Retailer ID]
where
	R.[Current Status] in ('Active') 
group by
	R.[Retailer Name], R.[Retailer ID], R.[State]
order by
	[One Year Multiple] asc,
	r.[Retailer Name]

If OBJECT_ID('tempdb..##RetailerMax') is not null
drop table ##RetailerMax

select
##TempRetailer.[Retailer ID] as RetailerID,
(
				(case 
					when [3 mo. FPD $] >=.60 then 0 
					when [3 mo. FPD $] >=.40 then 1
					when [3 mo. FPD $] >=.30 then 2
					when [3 mo. FPD $] >=.20 then 3
					when [3 mo. FPD $] >=.10 then 4
					else 5
				end*3
				)
				+  
				(
				case 
				when [3 mo. Charge-Off %] >= .30 Then 0
				when [3 mo. Charge-Off %] >= .25 Then 1
				when [3 mo. Charge-Off %] >= .20 Then 2
				when [3 mo. Charge-Off %] >= .15 Then 3
				when [3 mo. Charge-Off %] >= .10 Then 4
				when [3 mo. Charge-Off %] >= .00 Then 5
				else 5
				End*3
				)
				+
				(
				case 
				when [3 mo. Paid %] >= .30 Then 5
				when [3 mo. Paid %] >= .25 Then 4
				when [3 mo. Paid %] >= .20 Then 3
				when [3 mo. Paid %] >= .15 Then 2
				when [3 mo. Paid %] >= .10 Then 1
				when [3 mo. Paid %] >= .00 Then 0
				else 5
				End*3
				)
				+
				(
				case
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.30 Then 5
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple])>= 1.25 Then 4
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.15 Then 3
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= 1.00 Then 2
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .70 Then 1
				when coalesce(nullif([One Year Multiple],0),[Overall Closing Multiple]) >= .00 Then 0
				else 0
				End*4
				)
				+
				(
				case
				when [Actual vs. Target] >= .15 Then 5
				when [Actual vs. Target] >= 0.05 Then 4
				when [Actual vs. Target] >= 0 Then 3
				when [Actual vs. Target] >= -.05 Then 2
				when [Actual vs. Target] >= -.10 Then 1
				when [Actual vs. Target] >= -.15 Then 0
				else 0
				End*3
				)
				+
				(
				case
				when [3 mo. Bounced %] >= .45 Then 0
				when [3 mo. Bounced %] >= .30 Then 1
				when [3 mo. Bounced %] >= .20 Then 2
				when [3 mo. Bounced %] >= .10 Then 3
				when [3 mo. Bounced %] >= .05 Then 4
				when [3 mo. Bounced %] >= 0 Then 5
				else 5
				End*2
				)
				+
				(
				case 
				when [Quarterly FPD vs Volume %] >= .3 Then 0
				when [Quarterly FPD vs Volume %] >= 0.2 Then 1
				when [Quarterly FPD vs Volume %] >= 0 Then 2
				when [Quarterly FPD vs Volume %] >= -.2 Then 3
				when [Quarterly FPD vs Volume %] >= -.4 Then 4
				when [Quarterly FPD vs Volume %] >= -.8 Then 5
				else 5
				End*2
				)
			)/100.00 'Max Approval Score'
			into ##RetailerMax
			from ##TempRetailer

declare @sDate as date, @sDate2 as date
set @sDate = Convert(date, dateadd(day, -365, getdate()))
set @sDate2 = Convert(date, getdate())

select
	RiskLevel = 
	case when sum([Funded Amount]) > 200000  then 'High Risk'
		when sum([Funded Amount]) > 100000 and RM.[Max Approval Score] < 0.3 then 'High Risk'
		when sum([Funded Amount]) > 100000 and RM.[Max Approval Score] < 0.5 and RM.[Max Approval Score] >= 0.3 then 'Medium Risk'
		when sum([Funded Amount]) > 10000 and sum([Funded Amount]) <= 100000 and RM.[Max Approval Score] < 0.5 and RM.[Max Approval Score] >= 0.3 then 'Low Risk'
		when sum([Funded Amount]) > 10000 and sum([Funded Amount]) <= 100000 and RM.[Max Approval Score] < 0.3 and RM.[Max Approval Score] >= 0.15 then 'Medium Risk'
		when sum([Funded Amount]) > 10000 and sum([Funded Amount]) <= 100000 and RM.[Max Approval Score] < 0.15 then 'High Risk'
		when sum([Funded Amount]) <= 10000 and RM.[Max Approval Score] < 0.5 and RM.[Max Approval Score] >= 0.3 then 'Low Risk'
		when sum([Funded Amount]) <= 10000 and RM.[Max Approval Score] < 0.3 then 'Medium Risk' else '----------' end,
	R.Region,
	R.Category,
	R.[Retailer Name],
	R.[Retailer ID],
	R.[Current Status],
	convert(date,R.[Creation Date]) as 'Creation Date',
	R.[Ranking (10 month)],
	RM.[Max Approval Score]*10 as 'Score',
	--CONVERT(CHAR(7),[Funded Date],120) as 'Vintage',
	sum(case when datediff(mm,[Funded Date],getdate()) >= 12 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 1.25
		when datediff(mm,[Funded Date],getdate()) = 11 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 1.21
		when datediff(mm,[Funded Date],getdate()) = 10 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 1.15
		when datediff(mm,[Funded Date],getdate()) = 9 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 1.09
		when datediff(mm,[Funded Date],getdate()) = 8 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 1.02
		when datediff(mm,[Funded Date],getdate()) = 7 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.95
		when datediff(mm,[Funded Date],getdate()) = 6 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.88
		when datediff(mm,[Funded Date],getdate()) = 5 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.79
		when datediff(mm,[Funded Date],getdate()) = 4 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.71
		when datediff(mm,[Funded Date],getdate()) = 3 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.62
		when datediff(mm,[Funded Date],getdate()) = 2 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.44
		when datediff(mm,[Funded Date],getdate()) = 1 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.25
		when datediff(mm,[Funded Date],getdate()) = 0 then nullif(isnull(([Total Paid]),0)/isnull(([Funded Amount]),0),0) - 0.06
		end)*1.0/count(L.[Lease ID]) as 'Actual vs. Target',
	sum([Funded Amount]) as 'Funded',
	sum([Total Paid]) as 'Paid',
	sum([Funded Amount])/count([Lease ID]) as 'Avg. Funded',
	count([Lease ID]) as 'Leases',
		(select
			avg([Monthly Income]) as AI
		From Customers as C join Leases as LL on C.[Customer ID] = LL.[Customer ID]
			join Retailers as RR on RR.[Retailer ID] = LL.[Retailer ID]
		where ([Submitted Date] between @sDate and @sDate2) and (RR.[Retailer ID] = R.[Retailer ID])
		group by RR.[Retailer ID]) as 'Avg. Inc.',
	nullif(isnull(sum([Total Paid]),0)/isnull(sum([Funded Amount]),0),0) as 'Multiple',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then 1 else null end)*1.0/count([Lease ID]) as 'SAC %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) between 90 and 364 then 1 else null end)*1.0/count([Lease ID]) as 'EBO %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) >= 365 then 1 else null end)*1.0/count([Lease ID]) as 'PIF %',
	count(case when LeaseQueueKey in (9,12,13,14,20,21,22) then 1 else null end)*1.0/count([Lease ID]) as 'Charge-Off %',
	count(case when LeaseQueueKey in (4) then 1 else null end)*1.0/count([Lease ID]) as 'Open %',
	count(case when LeaseQueueKey in (6) then 1 else null end)*1.0/count([Lease ID]) as 'Bounced %',
	count(case when LeaseQueueKey not in (4,5,6,9,12,13,14,20,21,22) then 1 else null end)*1.0/count([Lease ID]) as 'Other %'
	
from
	Leases as L left join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
	left outer join ##RetailerMax as RM on R.[Retailer ID] = RM.RetailerID
where
	([Funded Date] between @sDate and @sDate2) and [Funded Amount]>0
	and RM.[Max Approval Score] < 0.5
	--and R.[Retailer Group Name] = 'MBA'
group by
	R.Region,
	R.Category,
	R.[Retailer Name],
	R.[Current Status],
	R.[Creation Date],
	R.[Retailer ID],
	R.[Ranking (10 month)],
	RM.[Max Approval Score]
--having sum([Funded Amount])>10000
order by
	RM.[Max Approval Score] desc