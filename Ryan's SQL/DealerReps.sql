select
	D.Name,
	D.ID,
	concat(U.FirstName,' ',U.LastName) as 'Rep',
	D.RiskScore,
	case when D.CancelDate is not null then 'No' else 'Yes' end as 'Active'
from
	Dealers as D left outer join DealerReps as DR on D.ID = DR.DealerID
		left outer join Users as U on DR.UserID = U.ID
--where
	--D.Name like '%Flint%'
		