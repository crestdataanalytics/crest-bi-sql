select
	R.[Retailer Name],
	count([Customer ID]) 'Rpt Customers'
From Leases as L
left join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
where
	[Submitted Date] > '2015-01-01'
	and [Submitted Date] < '2015-12-31'
	and [Sequence Number] > 1
	and [LeaseQueueKey] not in (7,11)
Group by	
	 R.[Retailer Name]