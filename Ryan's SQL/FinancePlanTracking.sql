SELECT
	CONVERT(CHAR(7),L.[Funded Date],120) as 'Vintage',
	R.Category,
	concat('ID:',FP.[FinancePlanKey],'|Discount:',FP.[Retailer Discount %],'|90Day:',FP.[90 Day Payoff %],'|Note:',FP.[Total Note %]) as 'Plan',
	count([Funded Amount]) as 'Leases',
	sum([Total Paid])/sum([Funded Amount]) as 'Multiple'
FROM
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
		join [Finance Plans] as FP on FP.FinancePlanKey = L.FinancePlanKey
WHERE
	[Funded Date] between '12-31-14' and '6-30-15'
GROUP BY
	CONVERT(CHAR(7),L.[Funded Date],120),
	R.Category,
	FP.[FinancePlanKey],
	FP.[Retailer Discount %],
	FP.[90 Day Payoff %],
	FP.[Total Note %]