select
	CONVERT(CHAR(7),[funded Date],120) as Vintage,
	[Funded Amount],
	[Total Paid],
	[Leasequeuekey]
from
	Leases
where
	[Funded Date] >= '1-1-16' 
	and [Funded Date] < '2-1-16'

select
	CONVERT(CHAR(7),[funded Date],120) as Vintage,
	sum([Funded Amount]) as 'Funded $',
	sum([Total Paid]) as 'Paid $',
	sum([Funded Amount])/count([Lease ID]) as 'Avg. Funded',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then 1 else null end)*1.0/count([Lease ID]) as 'SAC %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) between 90 and 364 then 1 else null end)*1.0/count([Lease ID]) as 'EBO %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) >= 365 then 1 else null end)*1.0/count([Lease ID]) as 'PIF %',
	count(case when LeaseQueueKey = 12 then 1 else null end)*1.0/count([Lease ID]) as 'Charge-Off %',
	count(case when LeaseQueueKey in (4,6) then 1 else null end)*1.0/count([Lease ID]) as 'Open %',
	count(case when LeaseQueueKey not in (4,5,6,12) then 1 else null end)*1.0/count([Lease ID]) as 'Other %'
from
	Leases
where
	[Funded Date] >= '1-1-16' 
	and [Funded Date] < '2-1-16'
group by
	CONVERT(CHAR(7),[funded Date],120)