SELECT TOP 50
	R.[Retailer Name],
	count([Lease ID]) as 'Leases',
	sum([Funded Amount]) as 'Funded $',
	sum([Funded Amount])/count([Lease ID]) as 'Avg. Funded',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then 1 else null end)*1.0/count([Lease ID]) as 'SAC %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) between 90 and 364 then 1 else null end)*1.0/count([Lease ID]) as 'EBO %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) >= 365 then 1 else null end)*1.0/count([Lease ID]) as 'PIF %',
	count(case when LeaseQueueKey = 12 then 1 else null end)*1.0/count([Lease ID]) as 'Charge-Off %',
	count(case when LeaseQueueKey not in (5,12) then 1 else null end)*1.0/count([Lease ID]) as 'Other %'
FROM
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
WHERE
	[Funded Date] between '5-1-14' and '4-30-15' and [Funded Amount] > 0
GROUP BY
	R.[Retailer Name]
ORDER BY
	sum([Funded Amount]) desc

SELECT TOP 50
	R.[Retailer Name],
	count([Lease ID]) as 'Leases',
	sum([Funded Amount]) as 'Funded $',
	sum([Funded Amount])/count([Lease ID]) as 'Avg. Funded',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) < 90 then 1 else null end)*1.0/count([Lease ID]) as 'SAC %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) between 90 and 364 then 1 else null end)*1.0/count([Lease ID]) as 'EBO %',
	count(case when LeaseQueueKey = 5 and datediff(dd,[Funded Date],[Close Date]) >= 365 then 1 else null end)*1.0/count([Lease ID]) as 'PIF %',
	count(case when LeaseQueueKey = 12 then 1 else null end)*1.0/count([Lease ID]) as 'Charge-Off %',
	count(case when LeaseQueueKey in (4,6) then 1 else null end)*1.0/count([Lease ID]) as 'Open %',
	count(case when LeaseQueueKey not in (5,12,4,6) then 1 else null end)*1.0/count([Lease ID]) as 'Other %'
FROM
	Leases as L join Retailers as R on L.[Retailer ID] = R.[Retailer ID]
WHERE
	[Funded Date] between '9-1-15' and '2-28-16' and [Funded Amount] > 0
GROUP BY
	R.[Retailer Name]
ORDER BY
	sum([Funded Amount]) desc